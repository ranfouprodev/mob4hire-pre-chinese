<?php

class Users extends Zend_Db_Table_Abstract  
{
    protected $_name = 'users';
    protected  $_referenceMap=array(
								'ProfileContactInfo'=>array(
									'columns'=>array('id'),
									'refTableClass' => 'ProfileContactInfo',
									'refColumns' => array('id')
									),
								'ProfileMobster'=>array(
									'columns'=>array('id'),
									'refTableClass' => 'ProfileMobster',
									'refColumns' => array('id')
									),
								'ProfileDeveloper'=>array(
									'columns'=>array('id'),
									'refTableClass' => 'ProfileDeveloper',
									'refColumns' => array('id')
									),
								'ProfileResearcher'=>array(
									'columns'=>array('id'),
									'refTableClass' => 'ProfileResearcher',
									'refColumns' => array('id')
									)
									
								);   
    public static function computePasswordHash($password)
    {
        return hash('sha256', '21' . $password . 'eoka3b');
    }


	public function count()
	{
		$cache = CacheFactory::getCache('users');	
		$countValue = 0;
		if ( !$countValue = $cache->get('count') ) {		
			$select=$this->select();
			$select->from($this, array ('COUNT(id) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->set('count', $countValue);
		}
		return $countValue+2044;
	}

	
	public function getUserForSearchUpdate($id){
		
		$tableInfo = array("u" => 'users');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
				->from($tableInfo,array('id', 'username', 'first_name','last_name'))
				->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id', array('p.idcountry','personal_description','company','personal_description_other','city','position','state','web'))
				->joinLeft(array('c'=>'countries'),'c.idcountry=p.idcountry', array('c.country as country','c.iso as iso'))
				->joinLeft(array('pt'=>'profile_tester'),'pt.id=u.id', array('pt.experience'))
				->where("u.id=?",$id);
		
		//echo $select->__toString();
		
		$row=$this->fetchRow($select);
		if (!$row) {
			throw new Exception("Could not find row $idtest");
		}
		
		return $row;
	}
	
	public function listUsersForSearch($where='1',$orderBy='', $pageNumber=1, $resultsPerPage=0)
	{
		$tableInfo = array("u" => 'users');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo,array("u.id as iduser","u.username","u.registered_on as registered_on","u.last_login as last_login"))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('c'=>'countries'),'p.idcountry=c.idcountry',array('c.country as country'))
			->joinLeft(array('r'=>'ratings'),'u.username=r.id',array('(r.total_value/r.total_votes) as rating','r.total_votes as total_votes'))
			->order(explode(",",$orderBy))
			->where($where);
		
		if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);	

		//echo $select->__toString();		
				
 		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	
	public function getUserAsContact($iduser)
	{
		$tableInfo = array("u" => 'users');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo,array("u.id as iduser","u.username","u.registered_on as registered_on","u.last_login as last_login"))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('c'=>'countries'),'p.idcountry=c.idcountry',array('c.country as country'))
			->joinLeft(array('r'=>'ratings'),'u.username=r.id',array('(r.total_value/r.total_votes) as rating','r.total_votes as total_votes'))
			->where("u.id=?",$iduser);

		//echo $select->__toString();		
				
 		$rows= $this->fetchRow($select);
		return $rows;
	}
	
	public function getUserByCode($code)
	{
		$userrow=$this->fetchRow("code = '$code'");
		return $userrow;
	}

	public function getUserByEmail($email)
	{
		$userrow=$this->fetchRow("email = '$email'");
		return $userrow;
	}

	public function getUser($id, $username='')
	{
		$id=(int)$id; // Make sure id is an integer
		if ($username)
		{
			$userrow=$this->fetchRow("username = '$username'");
		}else{
			$userrow=$this->fetchRow('id = ' . $id);
		}
		
		if($userrow){
			return $userrow->toArray();
		}else{
			return false;
		}
		
	}


     /**
     * Adds user to database, sends confirmation email
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @throws Zend_Db_Statement_Exception
     * @return int The id of the new user     
     */    
    public function add($username, $password, $email)
    {
        $activationCode = sha1(uniqid('xyz', true)); 
        
        $newUser = array(
            'username' => $username,
            'password' => $this->computePasswordHash($password),
            'email' => $email,
            'active' => 0,
            'code' => $activationCode,
            'registered_on' => new Zend_Db_Expr('NOW()')        
        );
                
        $userId = $this->insert($newUser);
 
	// send activation email
	$mailer = new Mailer();        
	$languageCode = (string) Zend_Registry::get('Zend_Translate')->getAdapter()->getLocale();               
	$activationLink = Zend_Registry::get('configuration')->general->url . '/index/activate/' . $userId . '/' . $activationCode;
	$mailer->sendRegistrationMail($email, $username, $activationLink, $languageCode);        
 
    
        return $userId;        
    }
    
	 /**
     * Adds user to database, sends confirmation email
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @throws Zend_Db_Statement_Exception
     * @return int The id of the new user     
     */    
    public function addFB($idfacebook,$username, $firstname, $lastname, $email)
    {
        $activationCode = sha1(uniqid('xyz', true)); 
        
        $newUser = array(
            'username' => $username,
            'email' => $email,
            'active' => 1,
			'first_name' => $firstname,
			'last_name' => $lastname, 
			'idfacebook' => $idfacebook,
            'code' => $activationCode,
            'registered_on' => new Zend_Db_Expr('NOW()')        
        );
                
        $userId = $this->insert($newUser);
 
	// send activation email
	$mailer = new Mailer();        
	$languageCode = (string) Zend_Registry::get('Zend_Translate')->getAdapter()->getLocale();               
	$activationLink = Zend_Registry::get('configuration')->general->url . '/user/activate/' . $userId . '/' . $activationCode;
	$mailer->sendRegistrationMail($email, $username, $activationLink, $languageCode);        
 
    
        return $userId;        
    }
	
	
	
    public function editProfile($userId, $profileData)
    {
        $userRowset = $this->find($userId);
        $user = $userRowset->current();
        if (!$user)
        {
            throw new Zend_Db_Table_Exception('User with id '.$userId.' is not present in the database');
        }
        
        foreach ($profileData as $k => $v)
        {
            if (in_array($k, $this->_cols))
            {
                if ($k == $this->_primary)
                {
                    throw new Zend_Db_Table_Exception('Id of user cannot be changed');
                }
                // special case - hash have to be computed for password - don't change password if empty
                if ($k == 'password' && $v)
                {
                    $user->password = $this->computePasswordHash($v);
                }
                else
                {
                    $user->{$k} = $v;
                }
            }            
        }
        
        $user->save();
        
        return $this;              
    }
	
	public function hasUsername($username)
	{
		$select=$this->select();
 		$select->from($this)
			->where("username=?",$username);
 		$rows= $this->fetchAll($select);
		$result = $rows[0];
		
		if($result){
			return true;	
		}else{
			return false;
		}
		
		
	}
	
	/*
	 * searchType=1 for id, 2 for username, 3 for email
	 */
	public function userExists($id=0, $target, $searchType)
	{
		if($searchType==1)
		{
			$whereClause="id = $id";
		}
		elseif($searchType==2)
		{
			$whereClause="username='$target'";
		}
		else
		{
			$whereClause="email='$target'";
		}
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array("count" =>'COUNT(*)');
		$statement = $select->from($this, $columns)
			->where($whereClause);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function listUsers($whereClause='1', $orderBy='registered_on desc',  $pageNumber=1, $resultsPerPage=0)
	{
		$rows = array();

		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to developers table to get developer username
		$select->distinct()
			->from($this)
			->order(explode(",",$orderBy))
			->order('users.id desc') // second order by newest (multiple files on the same date should order)
			->join(array('p'=>'profile_contactinfo'), 'users.id=p.id')
			->where($whereClause);
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
		
		//echo Zend_Debug::Dump($select->assemble());

		$rows= $this->fetchAll($select);

		return $rows;
	}
	
	public function getUserID($username)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
 		$tableInfo = array("u" => 'users');
		$columns = array("id" =>'u.id');
		$select->from($tableInfo, $columns)
			->where("u.username=?",$username);
 		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['id'];
	}
	
	public function countNewUsers($startDate, $endDate)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("u" => 'users');
		$columns = array("total_testers" =>'COUNT(u.id)');
		$statement = $select->from($tableInfo, $columns)
			->where("u.registered_on >= ?", $startDate)
			->where("u.registered_on <= ? ", $endDate);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['total_testers'];
	}
	
	public function averageDailyNewUsers($startDate, $endDate)
	{
		$cat=$this->countNewUsers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/86400;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function averageWeeklyNewUsers($startDate, $endDate)
	{
		$cat=$this->countNewUsers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/604800;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function getUserTimeStamp($id)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("u" => 'users');
		$columns = array("time" =>'u.registered_on');
		$statement = $select->from($tableInfo, $columns)
			->where("u.id = ?", $id);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['time'];
	}
	
	public function getUserEmail($id)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("u" => 'users');
		$columns = array("email" =>'u.email');
		$statement = $select->from($tableInfo, $columns)
			->where("u.id = ?", $id);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['email'];
	}
	
	/*
	 * This will look to both the developer and the tester profiles to see if the user
	 * has allowed for a can send project message.
	 */
	public function canSendProjectMessages($id)
	{
		$profiletester = new ProfileMobster();
		$profiledeveloper = new ProfileDeveloper();
		$result = $profiletester->canSendProjectMessages($id)+$profiledeveloper->canSendProjectMessages($id);
		return $result;
	}
	
	public function countMobPro()
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$statement = $select->from($this, array('COUNT(*) AS total'))
			->where("pro = 1");
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['total'];
	}
	
}