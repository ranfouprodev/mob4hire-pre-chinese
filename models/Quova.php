<?php
class Quova
{
	static private $service='http://api.quova.com/';
	static private $ver='v1/';
	static private $method='ipinfo/';
	static private $format='flatxml';
/*	static private $apikey='100.kfy88kpan3ehhxezn9fv';
	static private $secret='8FStbu2Y';*/
	static private $apikey='100.58zb6zsjeyn6r9nuyx5f';
	static private $secret='AWCq64gH';

	private $sig;
	
	protected  $_params=array();
	
	function __construct()
	{
		$timestamp = gmdate('U');
		$this->sig = md5(self::$apikey . self::$secret . $timestamp);
	}
	
	public function ipinfo($ipaddress)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$service . self::$ver. self::$method. $ipaddress . '?apikey=' .self::$apikey . '&sig='.$this->sig. '&format=' . self::$format);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// make sure we don't make more than 2 calls to the api per second (as per terms of the license)
		usleep(500000);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
		// close curl
		curl_close($ch);

		// return XML data
		if ($headers['http_code'] != '200') {
			return false;
		} else {
			return simplexml_load_string($data);
		}
	}
}