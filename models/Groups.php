<?php

class Groups extends Zend_Db_Table_Abstract  
{
	protected $_name = 'groups';

	public static $access=array(100=>"Membership Pending",102=>"Membership Rejected",201=>"Member",202=>"Moderator");
	
	public static $status=array(100=>"Active", 101=>"Inactive");  
	
	public static $visibility=array(1=>"Public", 2=>"Private");
	
	public function listGroups($where='1', $sort='members desc',  $pageNumber=1, $resultsPerPage=0)
	{
		$tableInfo = array("g" => 'groups');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->join(array('u'=>'users'), 'u.id=g.idowner',  array("u.username as ownername","u.email as owneremail "))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('gu'=>'groups_user'),'gu.idgroup=g.idgroup',array('count(gu.iduser) as members'))
			->group('g.idgroup');
		
		if($where)
			$select->where($where);

		if($sort)
			$select->order($sort);	
			
		if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);	

		//echo $select->__toString();				
 		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function listOpenGroupsForUser($iduser){
		
		$tableInfo = array("g" => 'groups');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->joinLeft(array('gu'=>'groups_user'),'gu.idgroup=g.idgroup')
			->where('gu.access > 199 ')
			->where('g.status = 100 ')
			->where("gu.iduser=$iduser or g.idowner=$iduser")
			->group('g.idgroup');
		
		//echo $select->__toString();				
 		$rows= $this->fetchAll($select);
		return $rows;
		
	}
	
		
	
	public function listUsersForGroup($idgroup,$access='>199'){
		
			$tableInfo = array("gu" => 'groups_user');
			$select=$this->select();
 			$select->setIntegrityCheck(false);
			$select->distinct()
				->from($tableInfo,array('iduser'))
				->where('gu.access '.$access)
				->where('gu.idgroup=?',$idgroup);
				
			//echo $select->__toString();				
 			$rows= $this->fetchAll($select);
			return $rows;
				
	}
	
	public function isAdmin($user, $idgroup){
		
		$row = $this->fetchRow('idowner = ' . $user . ' and idgroup ='.$idgroup);
		
		if (!$row) {
			return false;
		}
		return true;
		
		
	}
	
	
	public function inGroup($user, $idgroup){
		
		if(!($return = $this->isAdmin($user,$idgroup))){
			
			
			$tableInfo = array("g" => 'groups');
			$select=$this->select();
 			$select->setIntegrityCheck(false);
			$select->distinct()
				->from($tableInfo)
				->joinLeft(array('gu'=>'groups_user'),'gu.idgroup=g.idgroup')
				->where('g.idgroup=?',$idgroup)
				->where('gu.access > 199')
				->where("status=100")
				->where('gu.iduser=?',$user);
				
			
			// echo $select->__toString();		
			$row = $this->fetchRow($select);	
			
			if($row)
				$return = true;
		}
		
		return $return; 
	}
	
	
	public function count($mode,$target)
	{

		switch($mode){
			case "self":
				$where = "g.idowner=".$target;
				break;
			default:
				$where = "1";
		}
		
		
		$tableInfo = array("g" => 'groups');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo,'count(g.idgroup) as count')
			->where("status=100");
		
		if($where)
			$select->where($where);

		//echo $select->__toString();	
			
		$rows= $this->fetchAll($select);
			
 		$row =$rows->current();
		$countValue = $row['count'];
		
		return $countValue;
	}
	
	
	public function getGroup($id,$status='')
	{
		$tableInfo = array("g" => 'groups');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->join(array('u'=>'users'), 'u.id=g.idowner',  array("u.username as ownername","u.email as owneremail"))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('gu'=>'groups_user'),'gu.idgroup=g.idgroup',array('count(gu.iduser) as members'))
			->group('g.idgroup')
			->where("g.idgroup=?",$id);
		
		if(!$status)
			 $select->where("status=100");
			
		//echo $select->__toString();			
 		$rows = $this->fetchRow($select);
		return $rows;
	}
	
	
	
	public function add($owner, $groupData) {

        $groupData['created_on'] = new Zend_Db_Expr('NOW()');
        $groupData['status'] = "100";
        $groupData['idowner'] = $owner;

        $group = array(); 
		foreach ($groupData as $name=>$value)
			{
				if (in_array($name, $this->_getCols()))
				{
					if ($name == $this->_primary)
					{
						throw new Zend_Db_Table_Exception('Id of group cannot be changed');
					}
					else
					{
						$group[$name]=$value;						
					}
				}
			}
        
        try {
       	    $id = $this->insert($group);
        }
        catch(Exception $e)
        {
            throw new Exception("Error inserting group");
        }
		
		return $id;
    }

    public function close( $id )
    {
    	$id=(int)$id; // Make sure id is an integer
		$row = $this->fetchRow('idgroup = ' . $id);
		
		if($row){
			$row->status="101";
			$row->save();	
		}
    }
    
	public function resume( $id )
    {
    	$id=(int)$id; // Make sure id is an integer
		$row = $this->fetchRow('idgroup = ' . $id);
		
		if($row){
			$row->status="100";
			$row->save();	
		}
    }
    
    public function getAccessStatus($idgroup, $iduser){
    	
    	$tableInfo = array("gu" => 'groups_user');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->where('gu.iduser=?',$iduser)
			->where('gu.idgroup=?',$idgroup);
			
		// echo $select->__toString();				
 		$row = $this->fetchRow($select);
 		if($row)
 			return $row->access; 
 		
    }
    
    
 	public function removeFromGroup( $idgroup, $iduser)
    {
		$db=Zend_Registry::get('db');
		$stmt = 'DELETE from groups_user WHERE idgroup='.$idgroup.' and iduser='.$iduser;
		$db->query($stmt);	
    }
    
	public function addToGroup( $idgroup, $iduser, $access)
    {
		$db=Zend_Registry::get('db');
		$stmt = 'INSERT into groups_user set idgroup='.$idgroup.' and iduser='.$iduser.' and access='.$access;
		$db->query($stmt);	
    }
	
}