<?php

/*
 * Generic Comment System 
 */
class Comments extends Zend_Db_Table_Abstract
{
    protected $_name = 'comments';
    protected $_primary = 'idcomment';


	public function getComment($id)
	{
		$id=(int)$id; // Make sure id is an integer
		$row = $this->fetchRow('idcomment = ' . $id . ' and status =1');
		
		if (!$row) {
			throw new Exception("Could not find row $id");
		}
		return $row->toArray();
	}

	public function getComments($category)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('d'=>'users'), 'd.id=comments.iduser',  array("d.username"))
			->where("category=? and status =1",$category)
			->order(array("posted desc"));
		$rows= $this->fetchAll($select);
		return $rows;		
	}


	public function addComment($category, $commenterId, $message) {

		$data = array (
			'category'=>$category,
			'iduser'=>$commenterId,
			'message'=>$message,
			'posted'=>new Zend_Db_Expr('NOW()'),
			'status'=>'1');

		try {
			$idfile = $this->insert($data);
		}
		catch(Exception $e)
		{
			throw new Exception("Error inserting comment");
		}
		return $idfile;
	}

    public function removeComment( $idfile)
    {
	
		$id=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idcomment = ' . $idfile);
		
		if($row){
			$row->status="0";
			$row->save();	
		}

    }

  

}
