<?php
require_once 'Lib/Cache/CacheFactory.php';
// require_once '/Tera-Wurfl/TeraWurfl.php';
// include 'Mobi/Mtld/DA/Api.php';
require_once 'Lib/DeviceAtlas/DeviceAtlasCloudClient.php';

class Devices extends Zend_Db_Table_Abstract  
{
	protected $_name = 'deviceatlas';

	public function getDevice($id)
	{
		$select=$this->select();
		$select->distinct()
			->from($this,array('vendor','model'))
			->order(array('vendor'));
 		$rows= $this->fetchRow($select);
		return $rows;
	}
	
	
	public function listVendors($platform='')
	{
		$cache = CacheFactory::getCache('devices');
		$cacheKey = 'listVendors_' . $platform;
		if ( !$array = $cache->get($cacheKey) ) {
			$select=$this->select();
	 		$select->distinct()
				->from($this,array('vendor'))
				->order(array('vendor'));
			if ($platform) {
				$select->where($platform);
			}
	 		$rows= $this->fetchAll($select);
			$array=array();
			foreach($rows as $row)
			{
				$array[$row['vendor']]=$row['vendor'];
			}
			$cache->set('vendors', $array);
		}
		return $array;
	}
	
	public function listModels($vendor, $platform='', $idproject=0)
	{
		if($idproject==0)
		{
			$cache = CacheFactory::getCache('devices');
			$cacheKey = 'listModels_' . $vendor;
			if ( !$array = $cache->get($cacheKey) ) {
				$select=$this->select();
		 		$select->distinct()
					->from($this, array('id', 'model'))
					->where('vendor=?', $vendor)
					->order(array('model'));
				if ($platform) {
					$select->where($platform);
				}
		 		$rows= $this->fetchAll($select);
				$array=array();
				foreach($rows as $row)
				{
					$array[$row['id']]=$row['model'];
				}
				$cache->set('models', $array);
			}
			return $array;
		}
		else
		{
			$select=$this->select();
			$select->setIntegrityCheck(false);
			$select->from($this, array('id', 'model'))
				->join(array('supporteddevices'), 'supporteddevices.tid=deviceatlas.id',  array())
				->where("supporteddevices.idproject = ?",$idproject)
				->where('deviceatlas.vendor=?', $vendor)
				->order(array('deviceatlas.model'));
			if ($platform) 
			{
				$select->where($platform);
			}
			$rows= $this->fetchAll($select);
			
			if(count($rows)==0)
			{
				return $this->listModels($vendor, $platform);
			}
			$array=array();
			foreach($rows as $row)
			{
				$array[$row['id']]=$row['model'];
			}
			return $array;
		}
		
	}
	
	public function checkPlatform($iddevice, $platform)
	{
			$select=$this->select();
	 		$select->distinct()
				->from($this, array('id', 'model'))
				->where('id=?', $iddevice)
				->where($platform);
			return $this->fetchAll($select);
	}
	


    /*
    Local DeviceAtlas.json file update required

    public function identifyDevice($useragent)
    {
        $s = microtime(true);

        $tree='';


        $cache = CacheFactory::getCache('devices');
        $cacheKey = 'tree';

        $siteRootDir = Zend_Registry::get('siteRootDir');

        //if ( !$tree = $cache->get($cacheKey) ) {
        $tree = Mobi_Mtld_DA_Api::getTreeFromFile($siteRootDir."/library/Mobi/json/DeviceAtlas.json");
        //	$cache->set('tree',$tree);
        //}


        $properties = Mobi_Mtld_DA_Api::getProperties($tree, $useragent);
        if($properties && isset($properties['id']) && isset($properties['mobileDevice']))
        {
            //device is a mobile. Add it in to the table if not already there
            if (!$this->fetchRow("id=$properties[id]")) {

                $clean = array();
                foreach ($properties as $k => $v)
                {
                    $replacePoints = str_replace(".","_",$k);		 // Thanks. I forgot about this.
                    if (in_array($replacePoints, $this->_cols))
                    {
                        $clean[$replacePoints] = $v;
                    }
                }

                $this->addNewDevice($clean);
            }
            return $properties;
        }
        else
            return false;
    }*/

    /**
     *
     * DeviceAtlas Cloud Client. API access is set in DeviceAtlas/DeviceAtlasCloudClient.php file
     *
     * @param $useragent
     * @return array|bool
     */
    public function identifyDevice($useragent)
    {

        $s = microtime(true);

        $result = DeviceAtlasCloudClient::getDeviceData($useragent);

		$properties = 0;
        // get the properties
        if (isset($result[DeviceAtlasCloudClient::KEY_PROPERTIES])) {
            $properties = $result[DeviceAtlasCloudClient::KEY_PROPERTIES];
        }

        if($properties && isset($properties['id']) && isset($properties['mobileDevice']))
        {
		    //device is a mobile. Add it in to the table if not already there
            if (empty($this->fetchRow($this->select()->where('id= ?',$properties['id'])))) {

                $clean = array();
                foreach ($properties as $k => $v)
                {
                    $replacePoints = str_replace(".","_",$k);		 // Thanks. I forgot about this.
                    if (in_array($replacePoints, $this->_cols))
                    {
                        $clean[$replacePoints] = $v;
                    }
                }

                $this->addNewDevice($clean);
            }
            return $properties;
        }
        else
            return false;
    }


	public function addNewDevice($properties)
	{
		$this->insert($properties);
	}
	
	public function getDeviceByVendorAndModel($vendor, $model)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('id'))
			->where("vendor = ?",$vendor)
			->where("model = ?",$model);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['id'];
	}

}