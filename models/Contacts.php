<?php

class Contacts extends Zend_Db_Table_Abstract  
{
	protected $_name = 'contacts';
    
	public function listContactsByUser($id,  $pageNumber=1, $resultsPerPage=0)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('u'=>'users'), 'u.id=contacts.contact',  array("u.id as iduser","u.username","u.registered_on as registered_on","u.last_login as last_login"))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('c'=>'countries'),'p.idcountry=c.idcountry',array('c.country as country'))
			->joinLeft(array('r'=>'ratings'),'u.username=r.id',array('(r.total_value/r.total_votes) as rating','r.total_votes as total_votes'))
			->where("owner=?",$id)
			->order(array("u.username"));
		
		if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);	
			
 		$rows= $this->fetchAll($select);
		return $rows;
	}
	
		
	public function count($id)
	{

		$select=$this->select();
		$select->from($this, array ('COUNT(owner) as count'))
				->where("owner=?",$id);
		$rows= $this->fetchAll($select);
		$row=$rows->current();
			$countValue = $row['count'];
			
		
		return $countValue;
	}
	
	
	public function hasContact($owner,$id)
	{
		$row = $this->fetchRow('owner = ' . $owner . ' and contact ='.$id);
		
		if (!$row) {
			return false;
		}
		return true;
	}
	
	
	
	public function add($owner, $contact) {

        $data = array (
        'owner'=>$owner,
        'contact'=>$contact,
		);

        try {
            $idfile = $this->insert($data);
        }
        catch(Exception $e)
        {
            throw new Exception("Error inserting contact");
        }
		
		return $idfile;
    }

    public function remove( $owner, $contact)
    {
		$this->delete(array('owner = ?' => $owner, 'contact=?'=>$contact));
    }
	
	
}