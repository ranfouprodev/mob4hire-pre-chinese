<?php
class BantamLive
{
	static private $url='https://richard%40mob4hire.com:Anathema42@mob4hire.bantamlive.com/api/contacts';
	static private $auth='richard%40mob4hire.com:Anathema42';
	
	public function listContacts()
	{
		return $this->call();
	}
	/*
	** Valid strings for method are POST, GET, PUT
	*/
	private function call($method='GET', $id='', $params='')
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$url.$id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($method=='POST')
			curl_setopt($ch, CURLOPT_POST, 1); // set POST method $data = curl_exec($ch);
		elseif($method=='PUT')
			curl_setopt($ch, CURLOPT_PUT, 1); // set PUT method $data = curl_exec($ch);
		elseif($method=='DELETE')
			curl_setopt($ch, CURLOPT_DELETE, 1); // set DELETE method $data = curl_exec($ch);
		if($params) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, "url=index%3Dbooks&field-keywords=PHP+MYSQL");
		}
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
		// close curl
		curl_close($ch);

		// return JSon data
		if ($headers['http_code'] != '200') {
			return false;
		} else {
			$response=Zend_Json::decode($data);
			return $response['result'];
		}
	}
}