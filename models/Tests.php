<?php

class Tests extends Zend_Db_Table_Abstract  
{
	protected $_name = 'tests';
	
	public static $status=array('0'=>'Awaiting bids', // This should never occur now I don't think
					'50'=>'Bid Invited',
					'75'=>'Bid Rejected',
					'100'=>'Bid Submitted',
					'200'=>'Bid Accepted (Awaiting escrow)',
					'250'=>'In progress',
					'300'=>'Test report submitted',
					'350'=>'Test report rejected',
					'400'=>'Escrow released',
					'499'=>'Test cancelled'); // This is used to get the text relating to test status. Much better than declaring a similar array all over the place which makes stuff really hard to maintain
	
	public static $errors=array('-1'=>'The test id is invalid or it is not in status 100',
						'-2'=>'Invalid tester, the tester does not match',
						'-3'=>'Insufficient funds for action',
						'-4'=>'Project has reached its maximum number of testers',
						'-5'=>'Authentication Error invalid user logged in',
						'-6'=>'Something has gone horribly wrong here',
						'-7'=>'The project is either closed for bidding or you do not qualify for this project',
						'-8'=>'General Error');
	
	protected static $COMMISSION_RATE = 19.0;
	protected static $MINCOMMISSION = 10.0;
	protected static $SALESTAX = 5.0;


	public function init()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		Tests::$status[0] = $translate->_('model_tests_status_bids');
		Tests::$status[50] = $translate->_('model_tests_status_bidInvited');
		Tests::$status[75] = $translate->_('model_tests_status_bidRejected');
		Tests::$status[100] = $translate->_('model_tests_status_bidSubmitted');
		Tests::$status[200] = $translate->_('model_tests_status_bidAccepted');
		Tests::$status[250] = $translate->_('model_tests_status_progress');
		Tests::$status[300] = $translate->_('model_tests_status_reportSubmitted');
		Tests::$status[350] = $translate->_('model_tests_status_reportRejected');
		Tests::$status[400] = $translate->_('model_tests_status_escrowReleased');
		Tests::$status[499] = $translate->_('model_tests_status_cancelled');
	}



	public function getTest($idtest)
	{
		$idtest=(int) $idtest;
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to projects table to get project name
		$select->distinct()
				->from($this,array('idtest', 'Status', 'bidAmount','commission', 'salestax', 'escrowAmount', 'idproject','lasttimestamp'))
				->joinInner(array('u'=>'users'),'u.id=tests.idtester',array('u.id as idtester','username', 'email'))
				->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id', array('p.image as Avatar'))
				->joinLeft(array('da'=>'deviceatlas'), 'da.id=tests.iddevice',array('da.vendor AS vendor','da.model AS model'))
				->joinLeft(array('d'=>'devices'),'d.iddevice=tests.iddevice')
				->joinLeft(array('m'=>'makes'),'m.idmake=d.idmake')
				->joinLeft(array('n'=>'networks'),'n.idnetwork=tests.idnetwork')
				->joinLeft(array('c'=>'countries'),'c.idcountry=n.idcountry')
				->joinLeft(array('tr'=>'transactions'), 'tr.idtest=tests.idtest and tr.Status=tests.Status', array('Comments'))
				->joinInner(array('prj'=>'projects'),'prj.idproject=tests.idproject', array('prj.Name as projectname', 'prj.iddeveloper', 'prj.maxtests', 'prj.StartDate', 'prj.Status AS projectstatus'))
				->joinInner(array('dev'=>'users'),'dev.id=prj.iddeveloper', array ('dev.username as devname', 'dev.email as devemail')) 
				->where("tests.idtest=?",$idtest);
		
		//echo $select->__toString();
		
		$row=$this->fetchRow($select);
		if (!$row) {
			throw new Exception("Could not find row $idtest");
		}
		return $row;	
	}
	
	public function count()
	{
		$select=$this->select();
		$select->from($this, array ('COUNT(idtest) as count'));
		$rows= $this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function listTestsByTester($idtester, $orderBy='idtest desc',  $pageNumber=1, $resultsPerPage=0)
	{
		
		
		$cache = CacheFactory::getCache('tests');
		$rows = array();
		
		
		$cacheKey = 'list_' . $idtester . $orderBy . $pageNumber . '_' . $resultsPerPage;
		$cacheKey = preg_replace('/[^A-Za-z0-9]/', '_', $cacheKey);
		
		$timer = Timer::start();
		if ( !$rows = $cache->get($cacheKey ) ) {
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to projects table to get project name
		$select->distinct()
			->from($this, array('idtest', 'Status', 'bidAmount','lasttimestamp'))
			->order(explode(",",$orderBy))
			->join(array('p'=>'projects'), 'p.idproject=tests.idproject',  array('p.idproject', 'p.Name', 'p.Duration', 'p.StartDate','p.projecttype','p.Status AS projectStatus'))
			->join(array('t'=>'transactions'), 't.idtest=tests.idtest AND t.Status=tests.Status',array("t.Timestamp"))
			->joinLeft(array('n'=>'networks'),'tests.idnetwork=n.idnetwork', array("n.network AS networkName"))
			->joinLeft(array('d'=>'deviceatlas'),'tests.iddevice=d.id',array('d.model AS model','d.vendor AS vendor'))
			->where("tests.idtester=?",$idtester)
			->where("tests.Status=250");
		//if($status)
			//$select->where("tests.Status=?",$status);
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
//		print_r($select->__toString());

		$rows= $this->fetchAll($select);
		$cache->setE($cacheKey, $rows, Timer::end($timer) );
		
		}
		
		return $rows;
	}

	public function listTestsByTesterAndProject($idproject, $idtester, $status='', $orderBy='idtest desc',  $pageNumber=1, $resultsPerPage=0)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to projects table to get project name
		$select->distinct()
			->from($this, array('idtest', 'Status', 'bidAmount','lasttimestamp'))
			->order(explode(",",$orderBy))
			->join(array('p'=>'projects'), 'p.idproject=tests.idproject',  array('p.Name', 'p.Duration'))
			->join(array('t'=>'transactions'), 't.idtest=tests.idtest AND t.Status=tests.Status',array("t.Timestamp"))
			->joinLeft(array('n'=>'networks'),'tests.idnetwork=n.idnetwork', array("n.network AS networkName"))
			->joinLeft(array('d'=>'deviceatlas'),'tests.iddevice=d.id',array('d.model AS model','d.vendor AS vendor'))
			->where("tests.idtester=?",$idtester)
			->where("p.idproject=?",$idproject);
		if($status)
			$select->where("tests.Status=?",$status);
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
	//	print_r($select->__toString());
		$rows= $this->fetchAll($select);
		return $rows;
	}


	public function getStatusByProject($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to projects table to get project name
		$select->distinct()
			->from($this, array('Status', 'count(idtest) as count'))
			->group('Status')
			->where("tests.idproject=?",$idproject);
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function getTestsByProject($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('idtest', 'Status', 'bidAmount'))
				->joinInner(array('t'=>'transactions'), 't.idtest=tests.idtest', array('max(t.Timestamp) as Timestamp' ))
				->joinInner(array('u'=>'users'),'u.id=t.idtester',array('u.id as idtester','username'))
				->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone', 'p.city AS city', 'p.company AS company'))
				->joinLeft(array('da'=>'deviceatlas'), 'da.id=tests.iddevice')
				->joinLeft(array('d'=>'devices'),'d.iddevice=tests.iddevice')
				->joinLeft(array('m'=>'makes'),'m.idmake=d.idmake')
				->joinLeft(array('n'=>'networks'),'n.idnetwork=tests.idnetwork')
				->joinLeft(array('c'=>'countries'),'c.idcountry=n.idcountry')
				->joinLeft(array('ci'=>'profile_contactinfo'), 'ci.id=u.id', array())
				->joinLeft(array('tc'=>'countries'), 'tc.idcountry=ci.idcountry',array('country as testercountry'))
				->joinLeft(array('wm'=>'wall_message'), 'wm.idtest=tests.idtest', array('wm.idmessage as idmessage', 'wm.message as message', '(SELECT COUNT(idmessage) FROM wall_message WHERE wall_message.idtest=tests.idtest AND isDeleted=0) AS totalMessages'))
				->where("tests.idproject=?",$idproject)
				->group('t.idtest')
				->order(array('tests.Status', 'Timestamp desc'));
		//print_r($select->__toString());
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function getTestsByProjectID($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('transactions.idtest AS idtest', 'transactions.bidAmount AS bidAmount', 'transactions.Timestamp AS Timestamp', 
			'transactions.Status AS status', 'users.username AS testername', 'tc.country AS testercountry', 'users.email AS testeremail', 
			'countries.country AS testercountry', 'networks.network AS testernetwork', 'deviceatlas.vendor AS make', 'deviceatlas.model AS device') )
				->joinInner(array('transactions'),'tests.idtest=transactions.idtest AND transactions.Status=tests.Status')
				->joinInner(array('users'),'tests.idtester=users.id ')
				->joinLeft(array('profile_contactinfo'),'profile_contactinfo.id=users.id ')
				->joinLeft(array('tc' => 'countries'), 'tc.idcountry=profile_contactinfo.idcountry ')
				->joinLeft(array('deviceatlas'),'deviceatlas.id=tests.iddevice')
				->joinLeft(array('networks'),'networks.idnetwork=tests.idnetwork')
				->joinLeft(array('countries'),'countries.idcountry=networks.idcountry')
				->joinLeft(array('wm'=>'wall_message'), 'wm.idtest=tests.idtest', array('wm.idmessage as idmessage', 'wm.message as message',  '(SELECT COUNT(idmessage) FROM wall_message WHERE wall_message.idtest=tests.idtest AND isDeleted=0) AS totalMessages'))
				->where("tests.idproject=?",$idproject)
				->order(array('tests.Status DESC', 'Timestamp desc'));
		$rows= $this->fetchAll($select);
		return $rows;
	}

	public function getActiveTestersByProject($idproject){
		
		$alltesters = $this->getTestsByProject($idproject);
		
		$activetesters = array(); 
		
		foreach($alltesters as $tester){
			
			if($tester['Status'] == '250'){
				array_push($activetesters,$tester);
			}
				
		}
		
		return $activetesters;
		
	}

	public function isActiveTesterOnProject($idproject, $idtester){
		
		$alltesters = $this->getActiveTestersByProject($idproject);
		
		foreach($alltesters as $tester){
			
			if($tester['idtester'] == $idtester){
				return true;
			}
				
		}
		
		return false;
		
	}

	public function updateTest($idtest, $testData)
	{
		$testRowset = $this->find($idtest);
		$test = $testRowset->current();
		if (!$test)
		{
			throw new Zend_Db_Table_Exception('test is not present in the database');
		}
        
		foreach ($testData as $k => $v)
		{
			$comments='';
			if (in_array($k, $this->_cols))
			{
				if ($k == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of test cannot be changed');
				}
				else if ($k=='Comments') {
					$comments=$v; // pass through comments to transactions only if we are setting them
				}
				
				$test->{$k} = $v;	
			}            
		}
		$timestamp=time();
		$test->lasttimestamp=time();
		$test->save();
			
		$transactions=new Transactions();
		$transactions->add($test->idtest, $test->idtester, $test->Status, $test->bidAmount, $timestamp, $comments);
        
		return $this;              
	}

	public function getBidSumForRange($startDate, $endDate)
	{
		//SELECT SUM(tests.escrowamount) FROM  tests JOIN transactions ON transactions.idtest=tests.idtest AND transactions.Status=400
		//WHERE transactions.Timestamp >= 1237757691 AND  transactions.Timestamp <= 1277757691;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('SUM(tests.bidamount) AS total', 'COUNT(tests.idtest) AS number', 'AVG(tests.bidamount) AS average'))
			->join(array('transactions'),'tests.idtest=transactions.idtest AND transactions.Status=400' ,array())
			->where("transactions.Timestamp >= ?", strtotime($startDate))
			->where("transactions.Timestamp <= ? ", strtotime($endDate));
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row;
	}
	
	public function countTestsForProject($idproject)
	{
		$select=$this->select();
		$columns = array("count" =>'COUNT(idtest)');
		$statement = $select->from($this, $columns)
			->where("idproject = ?", $idproject)
			->where("Status >= ? ", 200)
			->where("Status <= ?",400);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	/**
	 * This function will change the status of a test that has been set as test case submitted to 
	 * a status of in progress once again.  This function should only be called by an admin view as no
	 * developer should have this functionality for the near future.  Please verify that it is an admin calling
	 * this function in the view.
	 */
	public function adminResetTest($idtest, $message)
	{
		//For this function we must first verify that the test has status 300
		//Next we change the status to 250 and send an email to both the tester and developer that this
		//has been completed
		$row=$this->getTest($idtest);
		if($row['Status']==300)
		{
			//the test is in the right status to be reset to in progress
			$testData=array('Status'=>250, 'Comments'=>$message);
			$this->updateTest($idtest,$testData);
			$idtester=$row['idtester'];
			$bidAmount=$row['bidAmount'];
			$mailer = new Mailer(); 
			$mailer->sendResetTestCase($idtest, $message);
		}
		else
		{
			//Failed
			return -1;
		}
		
		
	}
	
	/**
	 * This function will change the status of a test that has been set as test case submitted to 
	 * a status of in progress once again.  This function should only be called by an admin view as no
	 * developer should have this functionality for the near future.  Please verify that it is an admin calling
	 * this function in the view.
	 */
	public function adminCancelTest($idtest, $message='', $sendEmail=1)
	{
		//This is a simple one for what ever reason the test is cancelled.  All I need to check is that
		//The test bid was accepted seriously
		$row = $this->getTest($idtest);
		$accounts = new Accounts();
		$projects = new Projects();
		
		if(($row['Status']>199)&&($row['Status']<400))
		{
			//This means that the test is currently set to either bid accepted, in progress, test result submitted
			$idtester=$row['idtester'];
			$idproject=$row['idproject'];
			$projectData=$projects->getProject($idproject);
			$iddeveloper=$projectData['iddeveloper'];
			$bidAmount=$row['bidAmount'];
			$commission=$row['commission'];
			$salestax=$row['salestax'];
			$returnAmount=$bidAmount+$commission+$salestax;	
			$accounts->moneyIn($iddeveloper, $returnAmount, $idtest,4);
			$testData=array('Status'=>499, 'Comments'=>$message);
			$this->updateTest($idtest,$testData);
			if($sendEmail==1)
			{
				$mailer = new Mailer(); 
				$mailer->sendCancelTestCase($idtest, $message);
			}
			return $idtest;		
		}
		else
		{
			return -1;
		}
	}

	/*
	 * This hasn't been tested yet
	 */
	public function developerCancelTest($idtest, $iddeveloper, $message='')
	{
		$check=$this->developerCanCancelTest($idtest, $iddeveloper);
		if($check==1)
		{
			//Developer can cancel the test
			$this->adminCancelTest($idtest, $message);
		}
		else
		{
			return -1;
		}
	}
	/*
	 * This will need be called with the iddeveloper in current user
	 */
	public function developerCanCancelTest($idtest, $iddeveloper)
	{
		$MaxOverDueDays=14;
		$now=strtotime("now");
		
		$row = $this->getTest($idtest);
		$projects = new Projects();
		$projectData=$projects->getProject($row->idproject);
		$overDueTime=($projectData['duration']+ $MaxOverDueDays)*86400+$row['lasttimestamp'];
		if(($iddeveloper==$projectData['iddeveloper'])&&($row['Status']>199)&&($row['Status']<400)&&($overDueTime<$now))
		{
			//This mean that the test has been accepted, escrow hasn't been release, the developer is
			//logged in and the time past duration is more than 14 days
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public function developerCanRejectTest($idtest, $iddeveloper)
	{
		$row = $this->getTest($idtest);
		$projects = new Projects();
		$projectData=$projects->getProject($row->idproject);

		if(($iddeveloper==$projectData['iddeveloper'])&&($row['Status']==300))
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	/*
	 * This function should only be callable by the developer of the project.  I know this will ahve to change
	 * when we go to the roles tables.  But for now this should generate the correct email in the system.
	 */
	public function developerRejectTest($idtest, $iddeveloper, $message='')
	{
		$row = $this->getTest($idtest);
		$projects = new Projects();
		$projectData=$projects->getProject($row->idproject);
		
		if(($check=$this->developerCanRejectTest($idtest, $iddeveloper))==1)
		{
			//The test has been submitted and the developer is logged in
			$testData=array('Status'=>250, 'Comments'=>$message);
			$this->updateTest($idtest,$testData);
			$bidAmount=$row['bidAmount'];
			$idtester=$row['idtester'];
			$mailer = new Mailer(); 
			$mailer->sendDeclineTestCase($idtest, $message);		
			
			return $idtest;
		}
		else
		{
			return $check;
		}
		
	}
	
	/*
	 * This function will check to see if the person can actually accept the bid for this project.
	 * If the user is allowed it will return a value of 1 if not it will return a negative value.
	 * -1 invalid test - the test id provided doesn't match or isn't in status 100
	 * -2 Invalid tester - the tester doesn't make sense
	 * -3 Developer doesn't have funds to run the test
	 * -4 Project has reached its maximum number of testers
	 * -5 Authentication Error - the developer isn't logged in.
	 */
	public function canAcceptBid($idtest, $developer)
	{
		//This function is currently not being used.  That needs to be rectified
			
		//Model's used in this function
		$accounts = new Accounts();
		$projects = new Projects();
		
		
		$db=Zend_Registry::get('db');
		$qryTest=$db->query("SELECT * FROM tests WHERE idtest='$idtest' AND tests.Status=100");
		// Make sure idtest is in the database and it has the correct Status
		if ($qryTest && $test=$qryTest->fetch()) 
		{
			$idproject=$test['idproject'];
			// join in general profile to get country later
			$qryProject=$db->query("SELECT projects.*, profile_contactinfo.idcountry FROM projects INNER JOIN profile_contactinfo ON profile_contactinfo.id=projects.iddeveloper WHERE idproject='$idproject'"); 
			// Make sure we are the owner of this project and it has the right status
			if($test['Status']>=200 || !$qryProject || !($project=$qryProject->fetch())|| $project['iddeveloper']!=$developer) 
			{
				//Authentication error
				return -5;
			}
			else 
			{
				$idtester=$test['idtester'];
				$iddeveloper=$project['iddeveloper'];
				$qryTester=$db->query("SELECT * FROM users WHERE users.id=$idtester");	
				if ($qryTester && ($tester=$qryTester->fetch())) 
				{
					
					$escrowAmount=$test['escrowAmount'];
					$balance=$accounts->getBalance($iddeveloper);
					if($balance >= $escrowAmount)
					{
						if($project['maxtests']<=$this->countTestsForProject($idproject))
						{
							//This means that a project has reached its maximum number of testers
							return -4;
						}
						else
						{
							return 1;
						}
					}
					else
					{
						//not enough funds
						return -3;
					}
				}
				else 
				{
					//Invalid tester
					return -2;
				}
			}
		}
		else 
		{
			//invalid test
			return -1;
		}	
	}
	
	/*
		Developer accepts bid
		Sets status in test to 200 then 250 and makes an entry in the transactions table of the same status
		It has 3 parameters, two of which must be in here
		$idtest - the id number for the test
		$message - the message you want to send to the tester
		$developer - the id of the developer of the project.  This is here to ensure that the correct person is 
		calling this function.  So always call it with $this->view->currentUser->id call
	*/
	public function acceptBid($idtest, $developer, $message='')
	{
		$check=$this->canAcceptBid($idtest, $developer);
		if($check==1)
		{
			$row = $this->getTest($idtest);
			
			$accounts = new Accounts();
			$projects = new Projects();
				
			$idtester=$row['idtester'];
			$idproject=$row['idproject'];			
			$commission=$row['commission'];
			$escrowAmount=$row['escrowAmount'];
			$bidAmount=$row['bidAmount'];
			$idtester=$row['idtester'];
			$iddeveloper=$developer;
			$salestax=$row['salestax'];
			
			$balance=$accounts->getBalance($iddeveloper);	
			try 
			{
				//Take the escrow amount out of the developer's account
				$description = "Escrow for test:".$idtest;
				$accounts->moneyOut($iddeveloper, $escrowAmount, $description, $idtest,2);

				//Set the status of the test as in progress
				$testData = array('Status'=>250, 'Comments'=>$message);
				$this->updateTest($idtest, $testData);
				if($row['maxtests']<=$this->countTestsForProject($idproject))
				{
					//This means that a project has reached its maximum number of testers
					$projects->closeBidding($idproject);
				}
				$mailer = new Mailer(); 
				$mailer->sendEscrowFundsBid($idtest, $message);
				return $idtest;
			}
			catch (Exception $e)
			{
				// something has gone horribly wrong.
				return -6;
			}
		}
		else
		{
			return $check;
		}
		
	}
	
	
	/*
	 * This function will check to see if the person can actually reject the bid for this project.
	 * If the user is allowed it will return a value of 1 if not it will return a negative value.
	 * -1 invalid test - the test id provided doesn't match or isn't in status 100
	 * -2 Invalid tester - the tester doesn't make sense
	 * -3 
	 * -4 
	 * -5 Authentication Error - the developer isn't logged in.
	 */
	public function canRejectBid($idtest, $developer)
	{
		$db=Zend_Registry::get('db');
		$qryTest=$db->query("SELECT * FROM tests WHERE idtest='$idtest' AND tests.Status=100");
		// Make sure idtest is in the database and it has the correct Status
		if ($qryTest && $test=$qryTest->fetch()) 
		{
			$idproject=$test['idproject'];
			// join in general profile to get country later
			$qryProject=$db->query("SELECT * FROM projects WHERE idproject='$idproject'"); 
			// Make sure we are the owner of this project and it has the right status
			if($test['Status']>=200 || !$qryProject || !($project=$qryProject->fetch())|| $project['iddeveloper']!=$developer) 
			{
				//Authentication error
				return -5;
			}
			else
			{
				$bidAmount=$test['bidAmount'];
				$idtester=$test['idtester'];
				$iddeveloper=$project['iddeveloper'];
				$qryTester=$db->query("SELECT * FROM users WHERE users.id=$idtester");	
				if ($qryTester && ($tester=$qryTester->fetch())) 
				{
					return 1;
				}
				else 
				{
					//Invalid tester
					return -2;
				}
			}
		}
		else
		{
			//invalid test
			return -1;
		}
	
	
	}
	
	
	/*
		Developer rejects bid
		Sets status in test to 75 and makes an entry in the transactions table of the same status
		It has 3 parameters, two of which must be in here
		$idtest - the id number for the test
		$message - not sure what it is for
		$developer - the id of the developer of the project.  This is here to ensure that the correct person is 
		calling this function.  So always call it with $this->view->currentUser->id call
	*/
	public function rejectBid($idtest, $developer, $message='')
	{
		
		$check=$this->canRejectBid($idtest, $developer);
		if($check==1)
		{
			$db=Zend_Registry::get('db');
			$qryTest=$db->query("SELECT * FROM tests WHERE idtest='$idtest' AND tests.Status=100");
			$test=$qryTest->fetch();
			$idproject=$test['idproject'];
			$bidAmount=$test['bidAmount'];
			$idtester=$test['idtester'];
			try 
			{
				$testData = array('Status'=>75, 'Comments'=>$message);
				$this->updateTest($idtest, $testData);
				$mailer = new Mailer(); 
				$mailer->sendRejectBid($idtest, $message);
				return $idtest;
			}
			catch (Exception $e)
			{
				// something has gone horribly wrong.
				return -6;
			}
		}
		else
		{
			return $check;
		}
	}
	
	/*
	 * This will ensure that the developer is logged in and that the test is indeed in status 300
	 * If the user is allowed it will return a value of 1 if not it will return a negative value.
	 * -1 invalid test - the test id provided doesn't match or isn't in status 100
	 * -2 Invalid tester - the tester doesn't make sense
	 * -3 
	 * -4 
	 * -5 Authentication Error - the developer isn't logged in.
	 */
	public function canAcceptTestPayTester($idtest, $developer)
	{	
		$db=Zend_Registry::get('db');
		$qryTest=$db->query("SELECT * FROM tests WHERE idtest='$idtest' AND (tests.Status=300 OR tests.Status=250)");
		// Make sure idtest is in the database and it has the correct Status
		if ($qryTest && $test=$qryTest->fetch()) 
		{
			$idproject=$test['idproject'];
			$qryProject=$db->query("SELECT * FROM projects  WHERE idproject='$idproject'"); 
			// Make sure we are the owner of this project and it has the right status
			if($test['Status']>300 || !$qryProject || !($project=$qryProject->fetch())|| $project['iddeveloper']!=$developer) 
			{
				//Authentication error
				return -5;
			}
			else
			{
				$idtester=$test['idtester'];
				$qryTester=$db->query("SELECT * FROM users WHERE users.id=$idtester");	
				if ($qryTester && ($tester=$qryTester->fetch())) 
				{

					return 1;

				}
				else 
				{
					//Invalid tester
					return -2;
				}
				
			}
		}
		else
		{
			//invalid test
			return -1;
		}
		
	}
	/*
	 * It has 3 parameters, two of which must be in here
		$idtest - the id number for the test
		$message - not sure what it is for
		$developer - the id of the developer of the project.  This is here to ensure that the correct person is 
		calling this function.  So always call it with $this->view->currentUser->id call
	 */
	public function acceptTestPayTester($idtest, $developer, $message= '')
	{
		$check=$this->canAcceptTestPayTester($idtest, $developer);
		if($check==1)
		{
			//Model's used in this function
			$accounts = new Accounts();;
			
			$db=Zend_Registry::get('db');
			$qryTest=$db->query("SELECT * FROM tests WHERE idtest='$idtest' AND (tests.Status=300 OR tests.Status=250)");
			$test=$qryTest->fetch();
			
			$idproject=$test['idproject'];
			$bidAmount=$test['bidAmount'];
			$idtester=$test['idtester'];
			$iddeveloper=$developer;
			$commission=$test['commission'];
			$salestax=$test['salestax'];
			try 
			{
				$accounts->moneyIn($idtester, $bidAmount, $idtest,3);
				$testData = array('Status'=>400, 'Comments'=>$message);
				$this->updateTest($idtest, $testData);
				$mailer = new Mailer();
				$mailer->sendAcceptTestCase($idtest, $message);
				return $idtest;
			}
			catch (Exception $e)
			{
				// something has gone horribly wrong.
				return -6;
			}
		}
		else
		{
			return $check;
		}		
	}
	
	/*
	 * This will check to see if the test being requested is allowed to be submitted and check to see that the 
	 * user submitting is the user of the test.
	 */
	public function canSubmitTest($idtest, $idtester)
	{
		$row = $this->getTest($idtest);
		if($row['Status']==250)
		{
			if($row['idtester']==$idtester)
			{
				return 1;
			}
			else
			{
				//Invalid Tester
				return -2;
			}
		}
		else
		{
			//Invalid test
			return -1;
		}
		
	}
	
	public function submitTest($idtest, $idtester, $message='')
	{
		$check=$this->canSubmitTest($idtest, $idtester);
		if($check==1)
		{
			try
			{
				$testData = array('Status'=>300, 'Comments'=>$message);
				$this->updateTest($idtest, $testData);
				$mailer = new Mailer();
				$mailer->sendTestCaseSubmitted($idtest, $message);

				return $idtest;
			}
			catch (Exception $e)
			{
				// something has gone horribly wrong.
				return -6;
			}
			
			
		}
		else
		{
			return $check;
		}
	}
	
	
	public function submitBid($idproject, $idtester, $bidAmount, $idnetwork, $iddevice, $message='')
	{
		$projects = new Projects();
		if($bidAmount<0)
		{
			$bidAmount=0;
		}
		if($bidAmount>99999)
		{
			$bidAmount=99999;
		}
		if($projects->canApplyForProject($idtester, $idproject))
		{
			
			$db=Zend_Registry::get('db');
			
			$qryProject=$db->query("SELECT projects.*, profile_contactinfo.idcountry FROM projects INNER JOIN profile_contactinfo ON profile_contactinfo.id=projects.iddeveloper WHERE idproject='$idproject'"); 
			$project=$qryProject->fetch();
			$iddeveloper=$project['iddeveloper'];
			$qrycom=$db->query("SELECT * FROM commission WHERE id=$iddeveloper");
			if ($qrycom && ($reccom=$qrycom->fetch())) 
			{
				$mincommission=$reccom['mincommission'];
				$commission_rate=$reccom['commission_rate'];
			}
			else 
			{
				$mincommission=self::$MINCOMMISSION;
				$commission_rate=self::$COMMISSION_RATE;						
			}
			$commission=$bidAmount*$commission_rate/100.0;
			if ($commission < $mincommission && $project['package']=='1') 
			{
				$commission=$mincommission;
			}
			$salestax=0;
			if ($project['idcountry']==2) 
			{
				$salestax=$commission * self::$SALESTAX/100;
			}
	
			$escrowAmount=$bidAmount + $commission + $salestax;
			$timestamp = time();
			$data=array( 'idproject'=>$idproject, 
				'iddevice'=>$iddevice, 
				'idnetwork'=>$idnetwork, 
				'idtester'=>$idtester, 
				'bidAmount'=>$bidAmount, 
				'Status'=>100, 
				'escrowAmount'=>$escrowAmount,
				'salestax'=>$salestax,
				'commission'=>$commission,
				'Comments'=>$message,
				'lasttimestamp'=>$timestamp);
			try
			{
				$this->insert($data);
				$idtest=$this->getAdapter()->lastInsertId();
				$transactions = new Transactions();
				$transactions->add($idtest,$idtester, 100, $bidAmount, $timestamp, $message);
				$mailer = new Mailer(); 
				$mailer->sendBidSubmitted($idtest, $message);
				return 1;
				
			}
			catch (Exception $e)
			{
				// something has gone horribly wrong.
				return -6;
			}	
		}
		else
		{
			return -1;
		}
	}

	public function cansubmitNewBid($idtest, $idtester)
	{
		$row = $this->getTest($idtest);
		if($row && $row['Status']==75)
		{
			$projects=new Projects();
			if($projects->canApplyForProject($idtester, $row['idproject']))
			{
				if($row['idtester']==$idtester)
				{
					return 1;
				}
				else
				{
					//Invalid Tester
					return -2;
				}
			}
			else {
			// can't apply for project who knows why?
				return -7;
			}
		}
		else
		{
			//Invalid test
			return -1;
		}	
	}
	
	public function submitNewBid($idtest, $idtester, $bidAmount, $message='')
	{
		if (($check=$this->canSubmitNewBid($idtest, $idtester))==1)
		{
			if($bidAmount<1)
			{
				$bidAmount=1;
			}
			if($bidAmount>99999)
			{
				$bidAmount=99999;
			}
			$test=$this->getTest($idtest);
			$idproject=$test['idproject'];
			$db=Zend_Registry::get('db');
			
			$qryProject=$db->query("SELECT projects.*, profile_contactinfo.idcountry FROM projects INNER JOIN profile_contactinfo ON profile_contactinfo.id=projects.iddeveloper WHERE idproject='$idproject'"); 
			$project=$qryProject->fetch();
			$iddeveloper=$project['iddeveloper'];
			$qrycom=$db->query("SELECT * FROM commission WHERE id=$iddeveloper");
			if ($qrycom && ($reccom=$qrycom->fetch())) 
			{
				$mincommission=$reccom['mincommission'];
				$commission_rate=$reccom['commission_rate'];
			}
			else 
			{
				$mincommission=self::$MINCOMMISSION;
				$commission_rate=self::$COMMISSION_RATE;						
			}
			$commission=$bidAmount*$commission_rate/100.0;
			if ($commission < $mincommission && $project['package']=='1') 
			{
				$commission=$mincommission;
			}
			$salestax=0;
			if ($project['idcountry']==2) 
			{
				$salestax=$commission * self::$SALESTAX/100;
			}
	
			$escrowAmount=$bidAmount + $commission + $salestax;
			$data=array(
						'bidAmount'=>$bidAmount, 
						'Status'=>100, 
						'escrowAmount'=>$escrowAmount,
						'salestax'=>$salestax,
						'commission'=>$commission,
						'Comments'=>$message);
			$this->updateTest($idtest, $data);
			$mailer = new Mailer(); 
			$mailer->sendBidSubmitted($idtest, $message);
			return $idtest;
		}
		else {
			return $check;
		}
	}
	
	public function getEscrowFundsForProject($idproject)
	{
		//SELECT SUM(bidAmount)+SUM(salesTax)+SUM(commission) AS count FROM tests
		//WHERE idproject=513 AND status>249 AND status<400;


		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array("count" =>new Zend_Db_Expr('SUM(bidAmount)+SUM(salesTax)+SUM(commission)'));
		if($idproject==0)
		{
			$statement = $select->from($this, $columns)
				->where("status > 249")
				->where("status < 400");
		}
		else
		{
			$statement = $select->from($this, $columns)
				->where("idproject = ?",$idproject)
				->where("status > 249")
				->where("status < 400");
		}


		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countTestsByStatusByProject($status,$idproject,$timestamp, $idtester=0)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns=array("count"=>'COUNT(idtest)');
		if($idtester==0)
		{
			$statement = $select->from($this, $columns)
				->where("idproject = ?",$idproject)
				->where("Status = ?",$status)
				->where("lasttimestamp > ?",$timestamp);
			
		}
		else
		{
			$statement = $select->from($this, $columns)
				->where("idproject = ?",$idproject)
				->where("Status = ?",$status)
				->where("lasttimestamp > ?",$timestamp)
				->where("idtester = ?",$idtester);
		}
		
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function listProjectsForTestCanceling($orderBy='p.StartDate DESC')
	{		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'tests');
		$columns=array("idproject"=>'DISTINCT(t.idproject)');
		$select->from($tableInfo, $columns)
			->join(array('p'=>'projects'),'t.idproject=p.idproject',array('p.Name AS Name','p.StartDate','p.Status','p.iddeveloper'))
			->join(array('u'=>'users'),'u.id=p.iddeveloper',array('u.username AS username'))
			->where("p.Status > 200")
			->where("p.Status < 401")
			->where("t.Status > 200")
			->where("t.Status < 400")
			->order($orderBy);
		$rows=$this->fetchAll($select);
		return $rows;
	}
	
	public function listTestsByProjectID($idproject, $whereClause='1')
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns=array('idtest', 'bidAmount', 'lasttimestamp', 'Status');
		$select->from($this, $columns)
				->joinInner(array('users'),'tests.idtester=users.id',array('users.username AS testername','users.email AS testeremail'))
				->joinLeft(array('profile_contactinfo'),'profile_contactinfo.id=users.id')
				->joinLeft(array('tc' => 'countries'), 'tc.idcountry=profile_contactinfo.idcountry', array('countries.country AS testercountry','countries.country AS country'))
				->joinLeft(array('deviceatlas'),'deviceatlas.id=tests.iddevice',array('deviceatlas.vendor AS make','deviceatlas.model AS device'))
				->joinLeft(array('networks'),'networks.idnetwork=tests.idnetwork', array('networks.network AS testernetwork'))
				->joinLeft(array('countries'),'countries.idcountry=networks.idcountry')
				->joinLeft(array('wm'=>'wall_message'), 'wm.idtest=tests.idtest', array('wm.idmessage as idmessage', 'wm.message as message',  '(SELECT COUNT(idmessage) FROM wall_message WHERE wall_message.idtest=tests.idtest AND isDeleted=0) AS totalMessages'))
				->where("tests.idproject=?",$idproject)
				->where($whereClause)
				->order(array('tests.Status DESC', 'Timestamp desc'));
		$rows= $this->fetchAll($select);
		return $rows;
	}

	public function checkIfBid($idproject, $idtester)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->from($this, array('COUNT(*) AS total'))
			->where("Status > 99 AND Status < 401")
			->where("idproject = ?",$idproject)
			->where("idtester = ?",$idtester);
		$rows= $this->fetchAll($select);
		$row=$rows->current();
		if($row['total']>0)
			return TRUE;
		else
			return FALSE;
	}
}
