<?php
class App {
// These arrays should really be static but I can't get them to work that way
var $_errorStrings=array("App name field is required",
						 "Short description field is required",
						 "Category field is required",
						 "Platform field is required",
						 "Version field is required",
						 "Application's name must contain from 3 to 30 alpanumeric, '_' or space characters",
						 "Version syntax must follow this format '00', '00.00' or '00.00.00'",
						 "Test end date must be set up two or more days ahead.",
						 "Duration field is required.",
						 "Duration must be between 1 and 90", 
						 "App Name is not available",
						 "You must select at least one supported handset",
						 "You must upload a generic executable",
						 "You must upload a test case",
						 "Terms and conditions field is required",
						 "Invalid value for Number of Testers"
);

var $allowedfiletypes=array('0'=>array('.sis','.sisx','.jar','.jad','.zip','.cab','.exe','.alx','.cod','.swf'),
							'1'=>array('.sis','.sisx','.jar','.jad','.zip','.cab','.exe','.alx','.cod','.swf'),
							'2'=>array('.jpg','.jpeg','.png','.gif'),
							'3'=>array('.jpg','.jpeg','.png','.gif'),
							'4'=>array('.txt','.zip','.doc','.pdf'),
							'5'=>array('.txt','.zip','.doc','.pdf'));
var $mode="launch";
var $_makes;
var $_devices;
var $_supportedDevices; // 2D Array of supported devices by parent
var $_supportedNetworks; // 2D Array of supported carriers by country
var $_filterDevices;
var $testerlist=array();
//						String		Application name which availability was previously checked by mean
var $appName;	//					of another O2 Litmus API

var $version;
//						String		Short application description
var $shortDesc;

//						String		Long application description
var $longDesc;

var $instructions; //	String		Instructions for the application

//						String		CSV String containing categories names list
var $categories=array(); //

//						String		CSV String containing Operating Systems names list
var $os=array();

//						String		CSV String containing make and model list. Example:
var $makemodel;

//            			String      CSV String containing O2 Litmus's enablers names list
var $enablers=array();

//            			String      Terms & Conditions
var $tc;

//            			long         Number of testers that also means limit of downloads
var $maxTesters;

var	$testenddate; // long             Timestamp with the final date of the test

var $duration;
var $invitemessage; // message to be sent to testers
var $executables;// String           String in containing the whole executable's URLs of the application

var $uploads=array(array(array()));
var $ideveloper;
var $developername,$devUsername;
var $image, $screenshot, $support, $testcase;
var $idproject,$appId;
var $timestamp; // Timestamp when project created
var $submittedtestcase=array(); // Array of filenames comprising testcase
var $day,$month,$year;
var $testsinprogress=0;
var $newbidsreceived=0;
var $status;

function App() { // default constructor
}

function load($idproject) {
	$projects=new Projects();
	if (!$record=$projects->getProject($idproject)->toArray()) {
		return 0;
	}	
	else {
		$this->idproject=$record['idproject'];
		$this->iddeveloper=$record['iddeveloper'];
		$this->appId=$record['appID'];
		$this->appName=$record['Name'];
		$this->version=$record['version'];
		$this->shortDesc=$record['description'];
		$this->longDesc=$record['longDesc'];
		$this->instructions=$record['instructions'];
		$this->categories=explode(",", $record['categories']);
		$this->os=explode(",",$record['os']);
		$this->enablers=explode(",", $record['enablers']);
		$this->tc=$record['tc'];
		$this->maxTesters=$record['maxtests'];
		$this->testenddate=$record['testenddate'];
		$this->StartDate=$record['StartDate'];
		$this->duration=$record['Duration'];
		$this->developername=$record['first_name']." ".$record['last_name'];
		$this->devUsername=$record['username'];
		$this->testcase=$record['testcase'];
		$this->timestamp=$record['timestamp']; // Timestamp when project created
		$this->duration=$record['Duration'];
		$this->invitemessage=$record['invitemessage'];
		$this->inviteonly=$record['inviteonly'];
		$this->flatfee=$record['flatfee'];
		$this->budget=$record['budget'];
		
		if($record['ImagePath']) {
			$this->image=$record['ImagePath'];
		}
		else {
			$this->image="/images/app_default.gif";
		}
		$this->uploads=unserialize($record['uploads']);
		$this->status=$record['Status'];

		// Load supported handsets list into app object

		$supportedDevices=$projects->getSupportedDevices($idproject);
		foreach($supportedDevices as $record2)
		{
			$this->addSupportedDevice($record2['vendor'], $record2['tid']);
		}

		// Load supported networks list into app object

		$supportedNetworks=$projects->getSupportedNetworks($idproject);
		foreach($supportedNetworks as $record3)
		{
			$this->addSupportedNetwork($record3['idcountry'], $record3['idnetwork']);
		}

		$this->testsinprogress=0;
		$this->newbidsreceived=0;
		$status=$projects->getStatus($idproject);
		foreach($status as $record4)
		{
			if($record4['Status']==100) {
				$this->newbidsreceived=$record4['count'];					
			}
			else if ($record4['Status']>=200 && $record4['Status']<499) {
				$this->testsinprogress+=$record4['count'];
			}
		}
	}
	return 1;
}

function insert() {
	// Write new project to database
	$projects= new Projects();
	if(!$idproject=$projects->add($this->toArray())) {
		return 0;
	}
	else {
		if (isset($this->uploads[0][0][0])) {
			$genericexecutable=$this->uploads[0][0][0];
		}
		else {
			$genericexecutable="";
		}
		$i=0;
		$supportedDevices=$this->getSupportedDevices();
		$projects->addSupportedDevices($idproject, $supportedDevices, $this->uploads); // Pass the device array and the file uploads
		$supportedNetworks=$this->getSupportedNetworks();
		$projects->addSupportedNetworks($idproject, $supportedNetworks);
	}
	return 1;
}

function endtesting() {
	$db=Zend_Registry::get('db');
	if (!$db->query($debug="UPDATE projects SET Status=499 WHERE appID='$this->appId'")) {
		return 0;
	}
	else {
		return 1;
	}	
}

function escapeStuff() {
	$this->appName=mysql_escape_string($this->appName);
	$this->version=mysql_escape_string($this->version);
	$this->shortDesc=mysql_escape_string($this->shortDesc);
	$this->longDesc=mysql_escape_string($this->longDesc);
	$this->instructions=mysql_escape_string($this->instructions);
	$this->tc=mysql_escape_string($this->tc);
	$this->invitemessage=mysql_escape_string($this->invitemessage);
}

function getSupportedDevices() {
	return $this->_supportedDevices;
}

function addSupportedDevice($parent, $tid) {
	// Do not allow the same device to be added twice
	if ($this->isSupportedDevice($parent,$tid))
		return 0;
	if (!isset($this->_supportedDevices[$parent])) {
		// add parent array if does not exist
		$this->_supportedDevices[$parent]=array(); 
	}
	array_push($this->_supportedDevices[$parent], $tid);
	return 1;
}

function removeSupportedDevice($parent,$tid) {
	$index=array_search($tid, $this->_supportedDevices[$parent]);
	array_splice($this->_supportedDevices[$parent],$index,1);
}

function isSupportedDevice($parent, $tid) {
	if (!$parent || !isset($this->_supportedDevices[$parent]))
		return false; // if array key is not found then device is not supported
	return in_array($tid, $this->_supportedDevices[$parent]);
}

function getFilterDevices() {
	return $this->_filterDevices;
}

function addFilterDevice($parent, $tid) {
	// Do not allow the same device to be added twice
	if ($this->isFilterDevice($parent,$tid))
		return 0;
	if (!isset($this->_filterDevices[$parent])) {
		// add parent array if does not exist
		$this->_filterDevices[$parent]=array(); 
	}
	array_push($this->_filterDevices[$parent], $tid);
	return 1;
}


function removeFilterDevice($parent,$tid) {
	$index=array_search($tid, $this->_filterDevices[$parent]);
	array_splice($this->_filterDevices[$parent],$index,1);
}

function isFilterDevice($parent, $tid) {
	if (!isset($this->_filterDevices[$parent]))
		return false; // if array key is not found then device is not supported
	return in_array($tid, $this->_filterDevices[$parent]);	
	
}

function getMake($parent) {
	if (!$parent)
		return 0;
	else
		return $this->_makes[$parent];

}

function getMakes() { // returns an array of makes
	return $this->_makes;
}

function getDevice($parent, $tid) {
	if (!$parent || !$tid)
		return 0;
	else	{
		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT * FROM deviceatlas WHERE id=$tid");
		$record=$query->fetch();
		return $record['model'];
	}
}

function getDevices($parent) {
	return $this->_devices[$parent];
}

function getParent($tid) {
// return parent for given tid
	foreach($this->_devices as $parent=>$array) {
		foreach($array as $device) {
			if ($device==$tid)
				return $parent;
		}
		
	}
	return false;
}

function getSupportedNetworks() {
	return $this->_supportedNetworks;
}

function addSupportedNetwork($idcountry, $idnetwork) {
	// Do not allow the same netowrk to be added twice
	if ($this->isSupportedNetwork($idcountry,$idnetwork))
		return 0;
	if (!isset($this->_supportedNetworks[$idcountry])) {
		// add parent array if does not exist
		$this->_supportedNetworks[$idcountry]=array(); 
	}
	array_push($this->_supportedNetworks[$idcountry], $idnetwork);
	return 1;
}

function removeSupportedNetwork($idcountry,$idnetwork) {
	$index=array_search($idnetwork, $this->_supportedNetworks[$idcountry]);
	array_splice($this->_supportedNetworks[$idcountry],$index,1);
}

function isSupportedNetwork($idcountry,$idnetwork) {
	if (!isset($this->_supportedNetworks[$idcountry]))
		return false; // if array key is not found then network is not supported
	return in_array($idnetwork, $this->_supportedNetworks[$idcountry]);	
}

function getCountry($idcountry) {
	if (!$idcountry)
		return 0;
	else	{
		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT * FROM countries WHERE idcountry=$idcountry");
		$record=$query->fetch();
		return $record['country'];
	}
}

function getNetwork($idnetwork) {
	if (!$idnetwork)
		return 0;
	else	{
		$db=Zend_Registry::get('db');
		$query=$db->query($debug="SELECT * FROM networks WHERE idnetwork=$idnetwork");
		$record=$query->fetch();
		return $record['network'];
	}
}

	
function getTesterList($handsetprefmatch, $random, $username) {
	$db=Zend_Registry::get('db');

	$whereclause="1";
	$testerlist=array();
	if ($handsetprefmatch=='true') {
		$filterDevices=$this->getSupportedDevices();
	}
	else {
		$filterDevices=$this->getFilterDevices();
	}
	if ($filterDevices) {
		$whereclause=" regdevices.deviceatlasid in (";
		$devicesadded=0;
		foreach($filterDevices as $idmake=>$array) {
			foreach($array as $iddevice)
			{
				$devicesadded=1;
				$whereclause.=$iddevice.",";
			}
		}
		if (!$devicesadded) {
			$whereclause="1";
		}
		else {
			$whereclause=rtrim($whereclause,','); // remove trailing comma
			$whereclause.=')';
		}
	}
	else "whereclause=1";
	
	$query=$db->query($debug="SELECT regdevices.idtester, regdevices.deviceatlasid, regdevices.idregdevice, username, vendor, model, Count(idtest) as testscompleted from regdevices ".
					   "inner join users on users.id=regdevices.idtester inner join deviceatlas on regdevices.deviceatlasid=deviceatlas.id left join tests on tests.idtester=users.id AND tests.Status>=400 AND tests.Status!=499 WHERE ".
						"$whereclause AND users.username!='$username' group by users.id");
	while($query && $record=$query->fetch()) {
		if (1) // ($record['Avatar']=="")
		{
			$record['Avatar']='/images/image-not-found.gif';
		}
		array_push($testerlist,$record);
	}
	if ($random=='true') {
		shuffle($testerlist);
	}
//	return $debug;
	return $testerlist;
}
	
function inviteTesters() {
	$db=Zend_Registry::get('db');
	if (isset($_POST['testerselection'])) {	
		foreach($_POST['testerselection'] as $index=>$value) {
			$qrytester=$db->query($debug="SELECT users.id, deviceatlas.id as deviceid FROM regdevices INNER JOIN users ON users.id=regdevices.idtester INNER JOIN deviceatlas ON deviceatlas.id=regdevices.deviceatlasid WHERE regdevices.idregdevice=$value"); 
			$record=$qrytester->fetch();
			$db->query($debug="REPLACE INTO invitedtesters (appName, idtester, deviceatlasid) ".
					"VALUES ('$this->appName', '$record[id]', '$record[deviceid]')");
//			echo $debug;
		}
	}
//	return $debug;
}

function testAvailability() { // eventually this should use a method of the Project object to test whether desired project name is unique
	//todo
	
	return 1;
}

function toArray() {
	$timestamp=time();
	$uploads=serialize($this->uploads);
	$os=implode(",",$this->os);
	$categories=implode(",",$this->categories);
	$enablers=implode(",",$this->enablers);
	$startdate=date("y-m-d", $this->testenddate);
	$this->escapeStuff();
	return Array(
				'Name'=>$this->appName,
				'description'=>$this->shortDesc, 
				'longDesc'=>$this->longDesc,
				'instructions'=>$this->instructions, 
				'tc'=>$this->tc, 
				'iddeveloper'=>$this->iddeveloper,
				'version'=>$this->version, 
				'testenddate'=>$this->testenddate, 
				'maxtests'=>$this->maxTesters, 
				'ImagePath'=>$this->image, 
				'screenshot'=>$this->screenshot, 
				'support'=>$this->support, 
				'testcase'=>$this->testcase, 
				'Status'=> 0, 
				'timestamp'=>$timestamp,
				'Duration'=> $this->duration, 
				'invitemessage'=>$this->invitemessage, 
				'uploads'=>$uploads, 
				'categories'=>$categories, 
				'enablers'=>$enablers, 
				'os'=>$os); 
}

function json_code ($json) { 

      //remove curly brackets to beware from regex errors

      $json = substr($json, strpos($json,'{')+1, strlen($json));
      $json = substr($json, 0, strrpos($json,'}'));
      $json = preg_replace('/(^|,)([\\s\\t]*)([^:]*) (([\\s\\t]*)):(([\\s\\t]*))/s', '$1"$3"$4:', trim($json));

      return json_decode('{'.$json.'}', true);

}


}
?>
