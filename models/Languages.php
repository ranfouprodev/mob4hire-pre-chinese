<?php
require_once 'Lib/Cache/CacheFactory.php';

class Languages extends Zend_Db_Table_Abstract  
{
	protected $_name = 'languages';
    
	public function listLanguages()
	{
		$cache = CacheFactory::getCache('languages');
		if ( !$array = $cache->get('list') ) {
			$select=$this->select();
	 		$select->distinct()
				->from($this,array('idlang','name'))
				->order(array("idlang"));
	 		$rows= $this->fetchAll($select);
			
			$array=array();
			foreach($rows as $row)
			{
				$array[$row['idlang']]=$row['name'];
			}
			$cache->set('list', $array);
		}
		return $array;
	}
}