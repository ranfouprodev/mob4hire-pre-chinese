<?php

class ProfileDeveloper extends Zend_Db_Table_Abstract
{
	
	public static  $platforms = array('','iphone'=>'IPhone','blackberry' => 'Blackberry','android' => 'Android','palm' => 'Palm','flash' => 'Flash','Windows' => 'Windows Mobile','j2me' => 'J2ME','symbian' => 'Symbian','mob2' => 'Mobile Web','other' => 'Other');
	
	 public static $revenueTypes = array(
		 1=>'Original Mobile Application Development',
		 2=>'Branded Mobile Application Development',
		 3=>'Content Development',
		 4=>'Mobile 2.0 Web Development',
		 5=>'Porting Applications From Various Platforms',
		 6=>'Text Messaging/SMS',
		 7=>'MMS',
		 8=>'Content Aggregator',
		 9=>'SMS/MMS Aggregator',
		 10=>'Mobile Application Aggregator',
		 11=>'Application Storefront',
		 12=>'Aggregate Content For Licensing And/Or Direct Sales',
		 13=>'Offer Services To Other Developers (please specify)',
		 14=>'Other (please specify)');
	 
	protected $_name='profile_developer';
	protected $_dependentTables = array('Users');
	
	public function count()
	{
		$cache = CacheFactory::getCache('developerprofiles');	
		$countValue = 0;
		if ( !$countValue = $cache->get('count') ) {		
			$select=$this->select();
			$select->from($this, array ('COUNT(id) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->set('count', $countValue);
		}
		return $countValue;
	}
	
	
	public function countCountries()
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('developerprofiles');
		$countValue = 0;
		if ( !$countValue = $cache->get('country_count') ) {		
		
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// fetch only the carriers where the device has been registered
			$select->distinct()
				->from($this, array())
				->join(array('u'=>'profile_contactinfo'), 'u.id = profile_developer.id',  array("u.idcountry"));
				//->where('regdevices.iddevice!=0 and status=1');
			$rows= $this->fetchAll($select);
			$countValue = count($rows) - 1; // the default is idcountry = 0 for unknown. That shouldn't be counted
			$cache->setE('country_count', $countValue, Timer::stop($timer));
		}
		return $countValue;
	}
	
	
	// A profile should be created when the user is the first time. 
	// this creates an empty record
	public function add($id)
	{
        
		$newUser = array(
			'id' => $id,
			'notify_email' =>'1,2,3'
		);
                
		$profileId = $this->insert($newUser);
     
		return $profileId;        
	}
	
	
	/*
	 Can be called with either id or Username
	*/
	public function getProfile($id, $username='')
	{
		$id=(int)$id; // Make sure id is an integer
		if ($username)
		{
			$users=new Users();
			$userrow=$users->fetchRow("username = '$username'");
			$id=$userrow['id'];

		}
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		if(!$row){
			throw new Exception("Could not find row $id");
		}else{
			$userrow=$row->findDependentRowset('Users')->toArray();
			$userrow[0]['password']=''; // Don't send the password
			return array_merge($row->toArray(), $userrow[0]);
		}
		
		
		/*if (!$row) {
			$users=new Users();
			$userrow=$users->fetchRow('id = ' . $id);
			if(!$userrow)
			{
				throw new Exception("Could not find row $id");
			}
			return $userrow->toArray();
		}
		else {
			$userrow=$row->findDependentRowset('Users')->toArray();
			$userrow[0]['password']=''; // Don't send the password
			return array_merge($row->toArray(), $userrow[0]); // I don't know why this is a 2 dimensional array but this seems to work
		}*/
	}

	/* 
		Can be used to update only the profile_d table. It does not affect anything in the users table
	*/
	public function updateProfile($userId, $profileData)
	{
		$profileRowset = $this->find($userId);
		// no user in the db. 
		if(!$profileRowset->count())
		{
			$this->add($userId);
			$profileRowset = $this->find($userId);
		}
	
		$profile = $profileRowset->current();

		foreach ($profileData as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of user cannot be changed');
				}
				else
				{
					if(is_array($value))
						$profile->{$name}=implode(',',$value);						
					else
						$profile->{$name}=$value;					
				}
			}
		}
		$profile->save();  // update profile if exists
		return $this;
	}
	
	/*
		This method is used in /app/profile/ and updates the users table as well as the profile table. Need to check this is still working
		after the addition of  'Which statement best describes you'. I suspect it might not be TODO:
	*/
	public function editProfile($userId, $profileData)
	{
		$this->updateProfile($userId, $profileData); // update profile data
		$users=new Users();
		$users->editProfile($userId, $profileData); // update Users table
		return $this;
	}
	
	public function canSendProjectMessages($id)
	{		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('COUNT(id) AS count'))
			->where("notify_email LIKE '%1%'")
			->where("id = ?",$id);
			
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		//This will either return 0 or 1.  If the count is 1 we can send an email to the user
		return $row['count'];
/*		try{
			$profileInfo = $this->getProfile($id);
		
			$email = explode(',',$profileInfo['notify_email']);
		
			return isset($email['1']);
		}catch(Exception $e){
			return true; // default send emails;
		}
*/
		//return 1;
	}
	
	public function hasProfile($id){ 
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		
		if(!$row){
			return false;
		}
		else{
			return true;
		}
	
	}
	
	/**
	 * This function counts the number of new Developers who sign up with the site over a 
	 * specified period of time.
	 *
	 */
	public function countNewDevelopers($startDate, $endDate)
	{
		//TODO need to write a test script for this function
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("pd" => 'profile_developer');
		$columns = array("count" =>'COUNT(pd.id)');
		$statement = $select->from($tableInfo, $columns)
			->join(array("u" => 'users'), 'u.id = pd.id', array())
			->where("u.registered_on >= ?", $startDate)
			->where("u.registered_on <= ? ", $endDate);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function averageDailyNewDevelopers($startDate, $endDate)
	{
		//TODO need to write a test script for this
		$cat=$this->countNewDevelopers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/86400;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function averageWeeklyNewDevelopers($startDate, $endDate)
	{
		//TODO need to write a test script for this
		$cat=$this->countNewDevelopers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/604800;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
}