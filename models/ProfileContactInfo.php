<?php

class ProfileContactInfo extends Zend_Db_Table_Abstract
{

	
	public static $descriptionList = array(
			1 => 'I\'m a mobile enthusiast and would like to make money testing and doing surveys.',
			2 => 'I\'m a quality assurance professional who wants to make extra money freelancing.',
			3 => 'I\'m a software developer.',
			4 => 'We\'re a professional mobile testing company.',
			5 => 'We\'re a professional porting company.',
			6 => 'I\'m a market researcher.',
			7 => 'I work for a Developer Network or an APP store.',
			8 => 'I work at a carrier or handset manufacturer.',
			9 => 'I work for an O/S, middleware or platform software vendor.',
			10 => 'None of these seem to suit who I am! I would describe me as:');
	
	/*public static $timeZoneList = array(
			""=>"--",
			"0.0" => "(GMT) Western Europe Time, London, Lisbon, Casablanca",
		      "1.0" => "(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris",
		      "2.0" => "(GMT +2:00) Kaliningrad, South Africa",
		      "3.0" => "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg",
		      "3.5" => "(GMT +3:30) Tehran",
		      "4.0" => "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi",
		      "4.5" => "(GMT +4:30) Kabul",
		      "5.0" => "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent",
		      "5.5" => "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi",
		      "5.75" => "(GMT +5:45) Kathmandu",
		      "6.0" => "(GMT +6:00) Almaty, Dhaka, Colombo",
		      "7.0" => "(GMT +7:00) Bangkok, Hanoi, Jakarta",
		      "8.0" => "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong",
		      "9.0" => "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
		      "9.5" => "(GMT +9:30) Adelaide, Darwin",
		      "10.0" => "(GMT +10:00) Eastern Australia, Guam, Vladivostok",
		      "11.0" => "(GMT +11:00) Magadan, Solomon Islands, New Caledonia",
		      "12.0" => "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka",
			
			"-12.0" => "(GMT -12:00) Eniwetok, Kwajalein",
			  "-11.0" => "(GMT -11:00) Midway Island, Samoa",
		      "-10.0" => "(GMT -10:00) Hawaii",
		      "-9.0" => "(GMT -9:00) Alaska",
		      "-8.0" => "(GMT -8:00) Pacific Time (US & Canada)",
		      "-7.0" => "(GMT -7:00) Mountain Time (US & Canada)",
		      "-6.0" => "(GMT -6:00) Central Time (US & Canada), Mexico City",
		      "-5.0" => "(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima",
		      "-4.0" => "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz",
		      "-3.5" => "(GMT -3:30) Newfoundland",
		      "-3.0" => "(GMT -3:00) Brazil, Buenos Aires, Georgetown",
		      "-2.0" => "(GMT -2:00) Mid-Atlantic",
		      "-1.0" => "(GMT -1:00 hour) Azores, Cape Verde Islands",	      
		);*/

	public static $timeZoneList = array(
		""=>"--",
		"0.0" => "(GMT)",
		"1.0" => "(GMT +1:00)",
		"2.0" => "(GMT +2:00)",
		"3.0" => "(GMT +3:00)",
		"3.5" => "(GMT +3:30)",
		"4.0" => "(GMT +4:00)",
		"4.5" => "(GMT +4:30)",
		"5.0" => "(GMT +5:00)",
		"5.5" => "(GMT +5:30)",
		"5.75" => "(GMT +5:45)",
		"6.0" => "(GMT +6:00)",
		"7.0" => "(GMT +7:00)",
		"8.0" => "(GMT +8:00)",
		"9.0" => "(GMT +9:00)",
		"9.5" => "(GMT +9:30)",
		"10.0" => "(GMT +10:00)",
		"11.0" => "(GMT +11:00)",
		"12.0" => "(GMT +12:00)",

		"-12.0" => "(GMT -12:00)",
		"-11.0" => "(GMT -11:00)",
		"-10.0" => "(GMT -10:00)",
		"-9.0" => "(GMT -9:00)",
		"-8.0" => "(GMT -8:00)",
		"-7.0" => "(GMT -7:00)",
		"-6.0" => "(GMT -6:00)",
		"-5.0" => "(GMT -5:00)",
		"-4.0" => "(GMT -4:00)",
		"-3.5" => "(GMT -3:30)",
		"-3.0" => "(GMT -3:00)",
		"-2.0" => "(GMT -2:00)",
		"-1.0" => "(GMT -1:00)",
	);


	public function init()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		ProfileContactInfo::$descriptionList[1] = $translate->_('form_profileContactInfo_personal_descriptionOpt1');
		ProfileContactInfo::$descriptionList[2] = $translate->_('form_profileContactInfo_personal_descriptionOpt2');
		ProfileContactInfo::$descriptionList[3] = $translate->_('form_profileContactInfo_personal_descriptionOpt3');
		ProfileContactInfo::$descriptionList[4] = $translate->_('form_profileContactInfo_personal_descriptionOpt4');
		ProfileContactInfo::$descriptionList[5] = $translate->_('form_profileContactInfo_personal_descriptionOpt5');
		ProfileContactInfo::$descriptionList[6] = $translate->_('form_profileContactInfo_personal_descriptionOpt6');
		ProfileContactInfo::$descriptionList[7] = $translate->_('form_profileContactInfo_personal_descriptionOpt7');
		ProfileContactInfo::$descriptionList[8] = $translate->_('form_profileContactInfo_personal_descriptionOpt8');
		ProfileContactInfo::$descriptionList[9] = $translate->_('form_profileContactInfo_personal_descriptionOpt9');
		ProfileContactInfo::$descriptionList[10] = $translate->_('form_profileContactInfo_personal_descriptionOpt10');
	}
	
	protected $_name='profile_contactinfo';
	protected $_dependentTables = array('Users');
	
	public function countCountries()
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('developerprofiles');
		$countValue = 0;
		if ( !$countValue = $cache->get('country_count') ) {		
		
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// fetch only the carriers where the device has been registered
			$select->distinct()
				->from($this, array('idcountry'));
				//->where('regdevices.iddevice!=0 and status=1');
			$rows= $this->fetchAll($select);
			$countValue = count($rows) - 1; // the default is idcountry = 0 for unknown. That shouldn't be counted
			$cache->setE('country_count', $countValue, Timer::stop($timer));
		}
		return $countValue;
	}
	
	
	
	// A profile should be created when the user is the first time. 
	// this creates an empty record
	public function add($id)
	{
		$newUser = array(
			'id' => $id,
			'image' => '/images/image-not-found-black.gif',
		);
                
		$profileId = $this->insert($newUser);
     
		return $profileId;        
	}
	
	
	/*
	 Can be called with either id or Username
	*/
	public function getProfile($id, $username='')
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('developerprofiles');
		$tag = "profile" . $id;
		
		if ( !$profileValue = $cache->get($tag) ) {
		
		$id=(int)$id; // Make sure id is an integer
		if ($username)
		{
			$users=new Users();
			$userrow=$users->fetchRow("username = '$username'");
			$id=$userrow['id'];

		}
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->joinLeft(array('c'=>'countries'), 'c.idcountry=profile_contactinfo.idcountry',  array("c.country"))
			->joinLeft(array('l'=>'languages'), "l.idlang=profile_contactinfo.language1", array("l.name as language"))
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		if (!$row) {
			$this->add($id);
			$row =  $this->fetchRow($select);		
		}
		
		$userrow=$row->findDependentRowset('Users')->toArray();
		$userrow[0]['password']=''; // Don't send the password
		
		$profileValue = array_merge($row->toArray(), $userrow[0]);
		
		$cache->setE($tag, $profileValue, Timer::stop($timer));
		}
		
		return $profileValue; 		
	}

	/* 
		Can be used to update only the profile_contact_info table. It does not affect anything in the users table
	*/
	public function updateProfile($userId, $profileData)
	{
		$profileRowset = $this->find($userId);
		// no user in the db. 
		if(!$profileRowset->count())
		{
			$this->add($userId);
			$profileRowset = $this->find($userId);
		}
		
		if(($profile = $profileRowset->current())) {
			foreach ($profileData as $name=>$value)
			{
				if (in_array($name, $this->_cols))
				{
					if ($name == $this->_primary)
					{
						throw new Zend_Db_Table_Exception('Id of user cannot be changed');
					}
					else
					{
						$profile->{$name}=$value;						
					}
				}
			}
			$profile->save();  // update profile if exists
		}
		return $this;
	}
	
	/*
		This method is used in /app/profile/ and updates the users table as well as the profile table. Need to check this is still working
		after the addition of  'Which statement best describes you'. I suspect it might not be TODO:
	*/
	public function editProfile($userId, $profileData)
	{
		$this->updateProfile($userId, $profileData); // update profile data
		$users=new Users();
		$users->editProfile($userId, $profileData); // update Users table
		return $this;
	}
	
}