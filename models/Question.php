<?php
class Question extends Zend_Db_Table_Abstract  
{
    protected $_name = 'question';

    protected  $_referenceMap=array(
								'SurveyQuestion'=>array(
									'columns'=>array('idquestion'),
									'refTableClass' => 'SurveyQuestion',
									'refColumns' => array('idquestion')
									),
								'QuestionAnswer'=>array(
									'columns'=>array('idquestion'),
									'refTableClass' => 'QuestionAnswer',
									'refColumns' => array('idquestion')
									),
								'Response'=>array(
									'columns'=>array('idquestion'),
									'refTableClass' => 'Response',
									'refColumns' => array('idquestion')
									)   
									
								);   

	public static $type = array(1=>'Select',
								2=>'Text');
								
	public static $mandatory = array(0=>'Not Required',
								1=>'Answer Required');

	public static $multiselect = array(0=>'Only One Answer May Be Selected',
								1=>'The User may select several answers to this question');
								
	public static $custom = array(0=>'Not Custom', 1=>'Custom');
	/*
	 * THis will add a new question to the database.  The inputs would need to be the question text, and
	 * a list of possible answers in an array.
	 * So it is assumed that the answers are already inserted into the answers, the format of answerArray
	 * is just a rowset of answers.
	 * table prior to adding a new question.
	 */
	public function addNewQuestion($text, $answerArray, $type=1, $mandatory=1, $multiselect=0, $custom=1)
	{
		$data = array('text'=>$text,'type'=>$type, 'mandatory'=>$mandatory, 'multiselect'=>$multiselect, 'custom'=>$custom);
		$this->insert($data);
		$id=$this->getAdapter()->lastInsertId();
		$qa = new QuestionAnswer();
		foreach($answerArray as $ans)
		{
			$data = array('idquestion'=>$id, 'idanswer'=>$ans['idanswer']);
			$qa->insert($data);
		}
		return $id;
	}
	
	public function listQuestions()
	{
		$select=$this->select();
		$select->from($this);
		$rows= $this->fetchAll($select);
		return $rows;
		
	}
	
	public function listQuestionsNotInSurvey($idsurvey)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('text','idquestion','type','mandatory','multiselect'))
			->where("question.idquestion NOT IN (SELECT survey_question.idquestion FROM survey_question WHERE idsurvey=$idsurvey AND active=1)")
			->where("custom = 0");
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function getQuestion($idquestion)
	{
		$rowset = $this->find($idquestion);
		$row = $rowset->current();
		return $row;
	}
}