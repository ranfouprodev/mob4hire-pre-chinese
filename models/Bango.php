<?php
class Bango
{
	private $_username;
	private $_password;
	private $_bango;
	
	private $_bangoSoapClient;
	
	function Bango($username, $password, $bango)
	{
		$this->_username=$username;
		$this->_password=$password;
		$this->_bango=$bango;
		$this->_bangoSoapClient=new Zend_Soap_Client('https://webservices.bango.com/userinformation/service.asmx?WSDL');
	}
	
	function NetworkFromIP($ip)
	{
		return $this->_bangoSoapClient->NetworkFromIP($this->_username, $this->_password, $ip, $this->_bango);
	}
}