<?php

class GroupInvites extends Zend_Db_Table_Abstract  
{
	
	protected $_name = 'groups_user';
	
	
	/*
	 * Adds a user to the group 
	 */
	public function addUserToGroup($idgroup, $iduser){
		
		if(!$this->hasPendingInvite($idgroup, $iduser)){	
		
			$data = array("idgroup"=>$idgroup, 
			"iduser"=>$iduser,"access"=>201,
			"updated"=> new Zend_Db_Expr('NOW()'));
			
			 try {
	       	    $id = $this->insert($data);
	        }
	        catch(Exception $e)
	        {
	            throw new Exception("Error inserting group");
	        }
			
			return $id;
		}else{
			return $this->updateStatus($idgroup,$iduser,201);
		}
		
	}
	
	public function sendInviteToGroup($idgroup, $iduser){
		
		if(!$this->hasPendingInvite($idgroup, $iduser)){	
		
			$data = array("idgroup"=>$idgroup, 
			"iduser"=>$iduser,"access"=>100,
			"updated"=> new Zend_Db_Expr('NOW()'));
			
			 try {
	       	    $id = $this->insert($data);
	        }
	        catch(Exception $e)
	        {
	            throw new Exception("Error inserting group");
	        }
			
	        // Send mail 
	        $mailer = new Mailer(); 
	        $mailer->sendJoinGroupRequest($iduser ,$idgroup);
	        
	        
			return $id;
		}
		
	}
	
	
	public function rejectInvite($idgroup, $iduser){
		
		return $this->updateStatus($idgroup,$iduser,102);
			
	}
	
	public function acceptInvite($idgroup, $iduser){
		
		return $this->updateStatus($idgroup,$iduser,201);
	}
	
	/*
	 * This will actually return any status
	 */
	public function hasPendingInvite($idgroup, $iduser){
		$tableInfo = array("gu" => 'groups_user');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->where('gu.iduser=?',$iduser)
			->where('gu.idgroup=?',$idgroup);
			
		//echo $select->__toString();				
 		$rows = $this->fetchAll($select);
 		
 		if($row = $rows->current())
 			return true;
 			
 		return false;
	}
	
	
	public function updateStatus($idgroup, $iduser, $status){
		
		$tableInfo = array("gu" => 'groups_user');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->where('gu.iduser=?',$iduser)
			->where('gu.idgroup=?',$idgroup);
			
		//echo $select->__toString();				
 		$rows = $this->fetchAll($select);
 		
 		if($row = $rows->current()){
 			$row->updated = new Zend_Db_Expr('NOW()');
 			$row->access = $status;
 			$row->save(); 
 		}
 		
		return true;
	}
	
	public function listInvitesForGroup($idgroup){
		
		$tableInfo = array("gu" => 'groups_user');
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($tableInfo)
			->where('gu.status = 100')
			->where('gu.idgroup=?',$idgroup);
				
			//echo $select->__toString();				
 			$rows= $this->fetchAll($select);
			return $rows;
	}
	
	
}