<?php

class Cron 
{

	public function run()
	{		
		$now=date ("Y-m-d", time()); // we could use mysql NOW() but I don't know what its timezone is
		$message="";
		$tempArray=$this->openNewProjects($now);
		$count=count($tempArray);
		$message = $message."The system automatically started $count projects who's starting date has arrived. The projects automatically opened for bidding are:<br/>";
		for($i=0 ; $i<$count; $i++)
		{
			$message = $message."".$tempArray[$i]."<br/>";
		}
		$tempArray=$this->closeOldProjects($now);
		$count=count($tempArray);
		$message = $message."The system has automatically closed the bidding on $count projects who's bidding end date as arrived. <br/>";
		for($i=0 ; $i<$count; $i++)
		{
			$message = $message."".$tempArray[$i]."<br/>";
		}
		$tempArray=$this->overdueTestReminder($now);
		$message = $message."The system generated ".$tempArray[0]." reminder emails on ".$tempArray[1]." projects with tests overdue. Here is the list of emails sent out:<br/>";
		for ($i = 2; $i < count($tempArray); $i++)
		{
			$message = $message."".$tempArray[$i]."<br/>";
		}
		$mailer=new Mailer();
		$mailer->sendToAdmin("Daily Admin Report", $message);
		
		$this->writeProjectRSS();
		
		// Sending out the daily email messages for the groups
		$this->sendDailyGroupMessages(); 
		
		// Update the search indexes
		$this->indexSearch();
		
	}
	
	
	public function indexSearch(){ 
		$usersearch = new Search_UserSearch(); 
		$usersearch->dailyIndex();
	}
	
	public function openNewProjects($now)
	{
		$projects=new Projects();
		$mailer = new Mailer();
		$select=$projects->select();
		$select->where("Status = 200");
		$select->where("StartDate <= '$now' AND testenddate >='$now'");
		$rows=$projects->fetchAll($select);
		$count=0;
		$rSet=array();
		foreach($rows as $row)
		{
			$row->Status=201;
			$row->save();
			$rSet[$count]=$row->Name;
			$idproject=$row->idproject;
			$count++;
			$mailer->sendProjectBiddingOpen($idproject);
		}
		return $rSet;
	}
	
	public function closeOldProjects($now)
	{
		$projects=new Projects();
		$mailer = new Mailer();
		$select=$projects->select();
		$select->where("Status = 201");
		$select->where("testenddate < '$now'");
		$rows=$projects->fetchAll($select);
		$count=0;
		$rSet=array();
		foreach($rows as $row)
		{
			$row->Status=300;
			$row->save();
			$rSet[$count]=$row->Name;
			$idproject=$row->idproject;
			$count++;
			$mailer->sendProjectBiddingClosed($idproject);
		}
		return $rSet;
	}
	

	/*
	 * This will check all in progress tests that are overdue and less than one month over due.  It will generate
	 * email reminders for those users who are active.  Lastly it returns an array of results.  The first two
	 * indecies in the array are reserved, 0=>the total emails sent, 1=>the total number of projects emails were
	 * generated for.  The remainder of the array is a bunch of strings with messages to be sent to the 
	 * admin detailing what users and what projects they are being emailed for. 
	 */
	public function overdueTestReminder($now)
	{
		if (!is_numeric($now))
		{
			$tempNow=strtotime($now);
			$now=$tempNow;
		}
		$tests= new Tests();
		$select=$tests->select();
		$select->setIntegrityCheck(false);
 		$tableInfo = array("t" => 'tests');
		$columns = array('t.idtest','t.idtester','t.idproject','t.Status', 't.lasttimestamp AS daTime');
		$select->from($tableInfo, $columns)
			->join(array('u'=>'users'),'u.id=t.idtester',array('u.id AS iduser','u.username AS username', 'u.email AS email'))
			->join(array('pr'=>'projects'),'pr.idproject=t.idproject', array('pr.Name as projectName', 'pr.iddeveloper', 'pr.maxtests','pr.duration AS projectDuration'))
			->where("t.status = ?",250)	
			->where("(t.lasttimestamp+pr.duration*86400+172800) <= ? ",$now)
			->where("(t.lasttimestamp+pr.duration*86400+1209600) > ? ",$now)//This makes sure that the projet isn't more than 14 days late
			->order('t.lasttimestamp desc');
		$rows=$tests->fetchAll($select);
		$mailer=new Mailer();	
		$count=0;	
		$users = new Users();
		$lastProject=0;
		$lastProjectCount=0;
		$rSet = array(0=>$count, 1=>$lastProjectCount); 
		$index=2;
		foreach($rows as $row)
		{
			$checkActive=$users->getUser($row['iduser']);
			$emailAddress=$row['email'];
			$name=$row['username'];
			$projectName=$row['projectName'];
			$idproject=$row['idproject'];
			$idtest=$row['idtest'];
			$timestamp=$row['daTime']+$row['projectDuration']*86400;
			if($checkActive['active']==1)
			{
				$daysTemp=$now-$timestamp;
				$daysOverdue=number_format($daysTemp/86400,1);
				$mailer->sendOverdueTestResults($emailAddress, $name, $projectName, $idproject, $idtest, $daysOverdue);
				$count++;
				$rSet[$index]="<b>".$name."</b> is currently $daysOverdue days overdue with their work on the ".$projectName." project.";
				$index++;
				if($idproject!=$lastProject)
				{
					$lastProject=$idproject;
					$lastProjectCount++;
				}
			}
		}
		$rSet[0]=$count;
		$rSet[1]=$lastProjectCount;
		return $rSet;
		
	}
	
	/*
	 * This will send an email to the developer about new bids and overdue test submitting
	 * on a project they are working on.  It will do this reminder once a day from 3 to 14 days.
	 */
	public function overdueDeveloperReminder($now)
	{
		if (!is_numeric($now))
		{
			$tempNow=strtotime($now);
			$now=$tempNow;
		}
	}
//
//  There are two alternative versions of writeProjectRSS here. The preferable one uses Zend_Feed_Writer but this does not exist in the older version of Zend framework on production servers
//  The second seems to have a problem writing the dates correctly 
	public function writeProjectRSS()
	{
		$projects=new Projects();
		$select=$projects->select();
		$select->setIntegrityCheck(false);
		$select->from(array('p'=>'projects'))
			->join(array('d'=>'users'), 'd.id=p.iddeveloper',  array("d.username"))
			->where("p.Status = 201")
			->order("p.StartDate desc");
		$rows=$projects->fetchAll($select);

		$writer=new Zend_Feed_Writer_Feed();
		
		$writer->setTitle('Mob4Hire: Open Projects'); 
		$writer->setLink('http://www.mob4hire.com/rss/projects.xml'); 
		$writer->setDescription('Open projects on www.mob4hire.com');
		$writer->setDateModified(time()); 

		foreach($rows as $project)
		{
			$shite=0;
			$entry = $writer->createEntry();
			if ($project['Name']) {
				$entry->setTitle($project['Name']);
			}
			else {
				$shite=1;
			}
			$entry->setLink("http://www.mob4hire.com/the-mob/view/idproject/".$project['idproject']); 
			
			if($project['description']) {
				$entry->setDescription($project['description']);
			}
			else {
				$shite=1;
			}
				if($timestamp=strtotime($project['StartDate'])) {
				$entry->setDateCreated($timestamp); 
			}
			if($project['username']) {
				$entry->addAuthor($project['username']);
			}
			else {
				$shite=1;
			}
			if(!$shite)
			{
				try {
					$writer->addEntry($entry); // manual addition post-creation
				}
				catch (Exception $e){print_r($e);}
			}
		}
		try {
			$fh = fopen(Zend_Registry::get('siteRootDir')."/public/rss/projects.xml", "w");
			fwrite($fh, $writer->export('rss'));
			fclose($fh);
		}
		catch (Exception $e){print_r($e);}

	}

	
	/*
	 * This function will simply compile a list of users who are supposed to receive an email.
	 * It is going to be a bit tricky as I need to figure out who needs an email before I generate the
	 * emails.
	 */
	public function dailyDigestEmailList($now)
	{
		if (!is_numeric($now))
		{
			$tempNow=strtotime($now);
			$now=$tempNow;
		}
		$yesterday=$now-86400;
		
		
	}
	
	
	/*
	 * Sends out the group messages that have added/changed in the last 24 hours
	 */
	public function sendDailyGroupMessages($now)
	{
		$timestamp = time() - (24 * 60 * 60); // 24h ago
		
		$groups = new Groups(); 
		$groupList = $groups->listGroups("g.type=1 and status=100","members desc",1,0);
			
		$walls = new Wall(); 
		
		$mailer = new Mail(); 
		
		foreach($groupList as $group){
			$posts = (int) $walls->countThreads($group->idgroup,6,$timestamp);
			if($posts > 0){
				// we should probably render the template once and batch send it out, but for now we will have to work around it	
				$userList = $groups->listUsersForGroup($group->idgroup);
				foreach($userList as $user){
					 $mailer->sendGroupDailyDigest($user->iduser, $group->idgroup, $timestamp);
				}
				// and send one to the owner
					$mailer->sendGroupDailyDigest($group->idowner, $group->idgroup, $timestamp);
			}
		}
		
		
	}
		
	
	/*
	 * This will figure out what parts of the email to send people.  It will get info on
	 * 1. The user's wall messages in the past day
	 * 2. Any bids that developers have accepted of the users in the past day
	 * 3. Any test results submitted to the users projects in the past day
	 * 4. Any new bids for the users project in the past day
	 */
	public function dailyDigestEmail($iduser, $now)
	{
		//I need the thing to be a timestamp not a date as most of the tables use timestamps
		if (!is_numeric($now))
		{
			$tempNow=strtotime($now);
			$now=$tempNow;
		}
		$yesterday=$now-86400;
		$emailArray=array();
		$emailIndex=0;
		
		//Get the projects that the dude is a developer for
		$projects=new Projects();
		$select=$projects->select();
		$select->setIntegrityCheck(false);			
		$tableInfo = array("p" => 'projects');
		$columns = array("idproject" =>'p.idproject',"projectName" => 'p.Name',);
		$select->from($tableInfo, $columns)
			->where("p.iddeveloper = ?", $iduser)
			->where("p.Status>200")
			->where("p.Status<401");
		$developerProjectList=$projects->fetchAll($select);	
		$check=0;	
		foreach($developerProjectList as $row)
		{
			if($check==0)
			{
				$emailArray[$emailIndex]="<h3>Your Projects</h3><br/>";
				$emailIndex++;
				$check=1;
			}
			//Here we count all the new bids and all the new tests for this project
			$idproject=$row['idproject'];
			$tempArray=$projects->getDeveloperProjectUpdateNotices($idproject,$yesterday);
			if($tempArray['newCheck']>0)
			{
				$name=$row['projectName'];
				$tempMessage='<a href="http://www.mob4hire.com/app/developer/showproject/idproject/'.$idproject.'">'.$name.'</a>';
				$tempMessage=$tempMessage." has received: ".$tempArray['message']."<br/>";
				$emailArray[$emailIndex]=$tempMessage;
				$emailIndex++;
			}
		}
		//Now lets figure out what tests the user is working on
		$check=0;
		$tests = new Tests();
		$select=$tests->select();
		$select->setIntegrityCheck(false);			
		$tableInfo = array("t" => 'tests');
		$columns = array('DISTINCT(t.idproject) AS idproject');
		$select->from($tableInfo, $columns)
			->joinInner(array('prj'=>'projects'),'prj.idproject=t.idproject', array('prj.Name as projectName'))
			->where("t.idtester = ?", $iduser)
			->where("t.Status>200")
			->where("t.Status<400");
		$testList=$tests->fetchAll($select);	
		foreach($testList as $row)
		{
			if($check==0)
			{
				$emailArray[$emailIndex]="<h3>Tests You Are Doing</h3>";
				$emailIndex++;
				$check=1;
			}
			$idproject=$row['idproject'];
			$name=$row['projectName'];
			$tempArray=$projects->getTesterProjectUpdateNotices($idproject,$iduser, $yesterday);
			if($tempArray['newCheck']>0)
			{
				$name=$row['projectName'];
				$tempMessage="Your work on the project $name";
				$tempMessage=$tempMessage." has received: ".$tempArray['message']."<br/>";
				$emailArray[$emailIndex]=$tempMessage;
				$emailIndex++;
			}
		}
		//Now we need to figure out what else to add?
		return $emailArray;
	}//End dailyDigestEmail
}