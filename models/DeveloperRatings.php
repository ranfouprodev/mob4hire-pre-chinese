<?php
/* The ratings table is used to keep a cumulative rating for each user. 
*  There are also testerratings and developerratings tables which are 
*  used to hold the rating for each test for testers and developers respectively */ 
class DeveloperRatings extends Zend_Db_Table_Abstract  
{
	protected $_name = 'developerratings';

	public function getRatingsUser($id,$limit=0) 
	{
               $select=$this->select(); 
               $select->setIntegrityCheck(false); 
                $select->distinct() 
                        ->from($this) 
                        ->join(array('t'=>'tests'), 't.idtest=developerratings.id',  array("t.idproject", "t.comments")) 
                        ->join(array('p'=>'projects'),'p.idproject=t.idproject',array("p.name"))
			->join(array('u'=>'users'),'u.id=t.idtester',array('u.username'))
                        ->where("p.iddeveloper=?",$id) 
                        ->order(array("t.idtest"));
				if($limit)
					$select->limit($limit);
                        
                $rows= $this->fetchAll($select); 
                return $rows; 
	}

	public function addRating($idtest, $value)
	{
		$rating=array('id'=>$idtest,
					'total_votes'=>'1', // in this table this will always be one but we will leave it this way to maintain compatibility
					'total_value'=>$value);

		try {
			$this->insert($rating);
			return false;
		}
		catch (Exception $e) {
			// we tried to add a duplicate, update instead
			$rowset= $this->find($idtest);
			if ($rowset->count()) {
				$rating=$rowset->current();
				$oldrating=$rating->total_value;
				$rating->total_value=$value;
				$rating->save();
				return  $oldrating;
			}
		}
	}
}