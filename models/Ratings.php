<?php
/* The ratings table is used to keep a cumulative rating for each user. 
*  There are also testerratings and developerratings tables which are 
*  used to hold the rating for each test for testers and developers respectively */ 
class Ratings extends Zend_Db_Table_Abstract  
{
	protected $_name = 'ratings';
	
	/* where type = 1 for tester, 2 for developer */
	
	public function addRating($username, $type, $idtest, $value)
	{
		// Do not add a zero star rating. In this case the user did not click on the stars and no rating was given
		if ($value) 
		{
			$rowset= $this->find($username);
			if (!$rowset->count()) {
				$this->insert(array('id'=>$username));
				$rowset = $this->find($username);
			}
		
			$rating=$rowset->current();
			$totalvotes=$rating->total_votes;
			$totalvalue=$rating->total_value;

			if ($type==1)
				$table=new TesterRatings();
			else if ($type=2)
				$table=new DeveloperRatings();
			
			if ($oldrating=$table->addRating($idtest, $value)) {
				// There was already a record which we updated we need to adjust ratings accordingly
				$totalvalue-=$oldrating; // total_votes will remain the same
			}
			else {	
				$totalvotes++;
			}
			
			$totalvalue+=$value;
		
			$rating->total_votes=$totalvotes;
			$rating->total_value=$totalvalue;
		
			$rating->save();	
		}
	}
	
	
}