<?php
class SurveyQuestion extends Zend_Db_Table_Abstract  
{
	/*
	 * The Order field will be used to specifying which questions come first in a survey.  Because order
	 * can be null, we will go highest number order first.  We suggest making the first number in a survery
	 * 10000 and decrease by 10 for each subsequent question.  This way there is room to play with it.
	 */
	protected $_name = 'survey_question';

	protected  $_referenceMap=array(
								'Branch'=>array(
									'columns'=>array('idsurvey_question'),
									'refTableClass' => 'Branch',
									'refColumns' => array('idsurvey_question')
									)   
								);
    
	protected $_dependentTables = array('Survey', 'Question');
	
	
	/*
	 * This function is going to add a question to an existing survey. This will only add a question to the 
	 * survey that is already defined in the database. 
	 * The inputs into this function would be:
	 * -idsurvey for the survey it is associated with
	 * -idquestion is the question that is already in the database that is to be added to the survey
	 * -order is the order value associated to the question.  The higher the order number the more likely it
	 * will be first in a survey
	 * It will return true if add was sucessful
	 */
	public function addQuestionToSurvey($idsurvey, $idquestion, $order=0)
	{
		if($order!=0)
			$data = array('idsurvey'=>$idsurvey, 'idquestion'=>$idquestion, 'order'=>$order, 'active'=>1);
		else
			$data = array('idsurvey'=>$idsurvey, 'idquestion'=>$idquestion, 'active'=>1);
		if($this->insert($data))
			return true;
		else
			return false;

	}
	
	public function getSurveyQuestion($idsurvey_question)
	{
		$select=$this->select();
		$columns=array('idquestion','idsurvey_question','idsurvey','order','active');
		$statement = $select->from($this, $columns)
			->where("idsurvey_question = ?",$idsurvey_question);
		$row=$this->fetchRow($select);
		return $row;
	}
	
	public function updateSurveyQuestion($idsurvey_question, $data)
	{
		$rowset = $this->find($idsurvey_question);
		$row = $rowset->current();
		
		foreach ($data as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of role cannot be changed');
				}
				else
				{
					$row->{$name}=$value;						
				}
			}
		}
		$row->save();
		
	}
	/*
	 * This function will return a list of questions based on a specified survey.  It will
	 * be joined with the question table, to give more info on it.
	 */
	public function getQuestionsBySurvey($idsurvey, $custom=0)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idsurvey_question','idquestion','order'))
			->join(array('question'=>'question'),'survey_question.idquestion=question.idquestion',array('text', 'custom', 'mandatory','type','title'))
			->joinLeft(array('question_answer'=>'question_answer'),'question.idquestion=question_answer.idquestion',array())
			->joinLeft(array('answer'=>'answer'),'answer.idanswer=question_answer.idanswer',array('answergroup'))
			->where("survey_question.idsurvey = ?",$idsurvey)
			->where("question.custom >= ?",$custom)
			->where("active=1")
			->group('idquestion')
			->order("survey_question.order DESC")
			->order("survey_question.idquestion");
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function removeQuestionFromSurvey($idsurvey_question)
	{
		$data=array('active'=>0);
		$this->updateSurveyQuestion($idsurvey_question, $data);
	}
	
	/*
	 * This is used to simple find the idsurvey_question for a given survey and question
	 * I am using it only to find the correct branching info to transfer the current
	 * mobExperience survey for branch table
	 */
	public function getIDSurveyQuestionByIdSurveyAndIDQuestion($idsurvey,$idquestion)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$statement = $select->from($this)
			->where("idquestion = ?",$idquestion)
			->where("idsurvey = ?",$idsurvey);
		$rows= $this->fetchAll($select);
		$row=$rows->current();
		return $row['idsurvey_question'];
	}

}