<?php
require_once 'Lib/Cache/CacheFactory.php';

class Testers extends Zend_Db_Table_Abstract  
{
	protected $_name = 'testers';
	
	
	public function count()
	{
		$cache = CacheFactory::getCache('testers');	
		$countValue = 0;
		if ( !$countValue = $cache->get('count') ) {		
			$select=$this->select();
			$select->from($this, array ('COUNT(idtester) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->set('count', $countValue);
		}
		return $countValue;
	}
    
	public function proCount()
	{
		$cache = CacheFactory::getCache('testers');
		$countValue = 0;
		if ( !$countValue = $cache->get('proCount') ) {
			$select=$this->select();
			$select->from($this, array ('COUNT(idtester) as count'))
				->where('pro = ?', 1);
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->set('proCount', $countValue);
		}
		return $countValue;
	}
 	
}