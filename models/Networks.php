<?php
require_once 'Lib/Cache/CacheFactory.php';
require_once 'Lib/Util/Timer.php';

class Networks extends Zend_Db_Table_Abstract  
{
	protected $_name = 'networks';
    
	public function listNetworks($idcountry)
	{
		$cache = CacheFactory::getCache('networks');
		$cacheKey = 'listNetworks_' . $idcountry;
		$countValue = 0;
		$timer = Timer::start();
		if ( !$array = $cache->get($cacheKey) ) {
			$select=$this->select();
	 		$select->distinct()
				->from($this, array('idnetwork', 'network'))
				->where('idcountry=?', $idcountry)
				->order(array('network'));
			fb($select->__toString());
	 		$rows= $this->fetchAll($select);
			$array=array();
			foreach($rows as $row)
			{
				$array[$row['idnetwork']]=$row['network'];
			}
			$elapsed = Timer::end($timer);
			$cache->setE('lists', $array, $elapsed );
		}
		return $array;
	}
	
	public function listAllNetworks()
	{
		$cache = CacheFactory::getCache('networks');
		$cacheKey = 'listAllNetworks';
		$countValue = 0;
		$timer = Timer::start();
		if ( !$array = $cache->get($cacheKey) ) {
			$select=$this->select();
	 		$select->distinct()
				->from($this, array('idnetwork', 'network'))
				->order(array('network'));
	 		$rows= $this->fetchAll($select);
			$array=array();
			foreach($rows as $row)
			{
				$array[$row['idnetwork']]=$row['network'];
			}
			$elapsed = Timer::end($timer);
			$cache->setE('lists', $array, $elapsed );
		}
		return $array;
	}
	
	public function countNetworks()
	{
		//TODO need to write some view to test this yet
		//SELECT COUNT(n.idnetwork) AS 'count' FROM networks AS n;
		$select=$this->select();
		$tableInfo = array("n" => 'networks');
		$columns = array("count" =>'COUNT(n.idnetwork)');
		$statement = $select->from($tableInfo, $columns);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	public function listNetworksByCountry($idcountry)
	{
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("n" => 'networks');
		$columns = array("id" =>'n.idnetwork', "name"=>'n.network', "country"=>'c.country');
		$statement = $select->from($tableInfo, $columns)
			->joinLeft(array("c"=>'countries'), 'n.idcountry=c.idcountry',  array())
			->where("n.idcountry = ?", $idcountry);
		$rows=$this->fetchAll($select);
		return $rows;

	}
}