<?php

class PPLog2 extends Zend_Db_Table_Abstract  
{
	protected $_name = 'pplog2';
		// These are the only acceptable values for transactioncode. If it aint her, it don't get in
	public static $transactioncodes=array(
										0 => 'Paypal deposit',
										1 => 'Paypal withdrawal',
										2 => 'escrow in - pay for test',
										3 => 'escrow out - get paid for test',
										4 => 'escrow out - escrow returned to developer',
										5 => 'premium commision fee',
										6 => 'managed services fee',
										7 => 'error adjustment payment' , 
										8 => 'transfer funds', 
										9 => 'manual deposit', 
										10 => 'manual withdrawal'
									);
									
									
	public function startPayPalWithdraw($transactionid, $amount, $userid)
	{
		//TODO need to add some code here
		$user = new Users();
		$email=$user->getUserEmail($userid);
		$insertArray = array('transactionid'=>$transactionid, 'receiver_email'=>getUserEmail($id), 'transactioncode'=>1);
		$this->insert($insertArray);
	}
	
	public function paypalDeposit($transactionid, $amount, $userid)
	{
		$user = new Users();
		$email=$user->getUserEmail($userid);
		$insertArray = array('transactionid'=>$transactionid, 'receiver_email'=>getUserEmail($id), 'transactioncode'=>0);
		$this->insert($insertArray);
	}
    
}