<?php
class RandomSurvey extends Zend_Db_Table_Abstract  
{
	protected $_name = 'random_survey';
	
	public function getAllInfo()
	{
		$select=$this->select();
		$columns=array('idquestion','mini','maxi');
		$select->from($this,$columns);
		$rows= $this->fetchAll($select);
		return $rows;
	}
    								
}