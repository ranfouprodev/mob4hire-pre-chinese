<?php

class ProfileResearcher extends Zend_Db_Table_Abstract
{
	
	protected $_name='profile_researcher';
	protected $_dependentTables = array('Users');
	
	// A profile should be created when the user is the first time. 
	// this creates an empty record
	public function add($id)
    {
        
        $newUser = array(
            'id' => $id,
			'notify_email' =>'1,2,3'
        );
                
        $profileId = $this->insert($newUser);
     
        return $profileId;        
    }
	
	
	/*
	 Can be called with either id or Username
	*/
	public function getProfile($id, $username='')
	{
		$id=(int)$id; // Make sure id is an integer
		if ($username)
		{
			$users=new Users();
			$userrow=$users->fetchRow("username = '$username'");
			$id=$userrow['id'];

		}
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		if(!$row){
			throw new Exception("Could not find row $id");
		}
		else{
			$userrow=$row->findDependentRowset('Users')->toArray();
			$userrow[0]['password']=''; // Don't send the password
			return array_merge($row->toArray(), $userrow[0]);
		}
		
		
		/*if (!$row) {
			$users=new Users();
			$userrow=$users->fetchRow('id = ' . $id);
			if(!$userrow)
			{
				throw new Exception("Could not find row $id");
			}
			return $userrow->toArray();
		}
		else {
			$userrow=$row->findDependentRowset('Users')->toArray();
			$userrow[0]['password']=''; // Don't send the password
			return array_merge($row->toArray(), $userrow[0]); // I don't know why this is a 2 dimensional array but this seems to work
		}*/
	}

	/* 
		Can be used to update only the profile_d table. It does not affect anything in the users table
	*/
	public function updateProfile($userId, $profileData)
	{
		$profileRowset = $this->find($userId);
		// no user in the db. 
		if(!$profileRowset->count())
		{
			$this->add($userId);
			$profileRowset = $this->find($userId);
		}
	
		$profile = $profileRowset->current();

		foreach ($profileData as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of user cannot be changed');
				}
				else
				{
					if(is_array($value))
						$profile->{$name}=implode(',',$value);						
					else
						$profile->{$name}=$value;					
				}
			}
		}
		$profile->save();  // update profile if exists
		return $this;
	}
	
	/*
		This method is used in /app/profile/ and updates the users table as well as the profile table. Need to check this is still working
		after the addition of  'Which statement best describes you'. I suspect it might not be TODO:
	*/
	public function editProfile($userId, $profileData)
	{
		$this->updateProfile($userId, $profileData); // update profile data
		$users=new Users();
		$users->editProfile($userId, $profileData); // update Users table
		return $this;
	}
	
		public function hasProfile($id){ 
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		
		if(!$row){
			return false;
		}
		else{
			return true;
		}
	
	}
	
	public function countAllResearchers()
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("pr" => 'profile_researcher');
		$columns = array("total" =>'COUNT(pr.id)');
		$statement = $select->from($tableInfo, $columns);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['total'];
	}
	
	public function countNewResearchers($startDate, $endDate)
	{
		//TODO need to write a test script for this function
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("pr" => 'profile_researcher');
		$columns = array("count" =>'COUNT(pr.id)');
		$statement = $select->from($tableInfo, $columns)
			->join(array("u" => 'users'), 'u.id = pr.id', array())
			->where("u.registered_on >= ?", $startDate)
			->where("u.registered_on <= ? ", $endDate);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function averageDailyNewResearchers($startDate, $endDate)
	{
		//TODO need to write a test script for this
		$cat=$this->countNewResearchers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/86400;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function averageWeeklyNewResearchers($startDate, $endDate)
	{
		//TODO need to write a test script for this
		$cat=$this->countNewResearchers($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/604800;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
}