<?php

class WallFile extends Zend_Db_Table_Abstract  
{
	protected $_name = 'wall_file';

	public function add($idmessage, $idfiles) 
	{
		$data = array (
			'idmessage'=>$idmessage,
			'idfiles'=>$idfiles,
			'isDeleted'=>0);
		try 
		{
			$this->insert($data);
			return 1;
		}
		catch(Exception $e)
		{
			throw new Exception("Error inserting file :".$e);
			return -1;
		}
		
	}
    
	public function countFilesForMessage($idmessage)
	{
		$columns = array('COUNT(*) AS count');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, $columns)
			->where("idmessage =?",$idmessage)
			->where("isDeleted =?",0);
		$rows= $this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
    
	/*
	* This just returns a row set of the idfiles from the wall_file table.  It is typically used for deleting 
	* purposes but I can't be certain what else.  I assume it should link to ProjectFiles but I am not certain.
	*/
	public function getFilesForPost($idmessage)
	{
		$columns = array('idfiles', 'idmessage', 'isDeleted');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, $columns)
			->where("idmessage =?",$idmessage)
			->where("isDeleted =?",0);
		$rows= $this->fetchAll($select);
		return $rows;
	}
    
	/*
	* This is just here to get the individual file for a wall post.  It is in this class just to keep
	* everything in the same place.  I know it is just linking to another function but don't shoot me
	*/
	public function getFile($idfiles)
	{
		$projectFiles = new ProjectFiles();
		return $projectFiles->getFile($id);
	}
}