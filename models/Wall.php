<?php

class Wall extends Zend_Db_Table_Abstract  
{
	protected $_name = 'wall_message';

	
	/*
	 * Messages status as of 201. Will help with understanding when messages are read
	 * 
	 * Status 102 is set so that the view knows to show this as a new message. Being as we are passing
	 * the data structure through to the view we cannot update the status directly without modifying the passed object
	 * 
	 * 
	 */
	public static $status=array(100=>"Unread", 101=>"Read", 102=>"Show As New", 
			200=>"Deleted");
	
	public static $postType=array(1=>"Project Public Wall", 2=>"Private Test Wall", 
			3=>"Personal Private Wall",6=>"Group Public Wall");
	
	/*
	 * Basically a simple get message so I can find out some information from the system.  What sucks
	 * is that I was having issues showing it and I was afraid at this point to change the deletePost 
	 * return values.
	 */
	public function getMessage($idmessage)
	{
		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		return $row;
	}
    
	/*
	* The function has several parameters which there are various issues in using this function.
	* There a some fields that must be added and others that only one will be added.
	* idposter - this is the user id of the user posting the message, this may not be 0
	* message - this is the text portion of a message
	* idtarget - this is the int(10) unsigned id value associated with the target of the post
	* postType - this will tell the system what kind of post to a wall is it supposed to be
	* 			  the values are 1 -> Project Post
	* 							 2 -> Test Post
	* 							 3 -> Personal User Post
	* So when this function is called add the 
	* $this->view->currentUser->id
	* is in the function call in the controller for the idposter
	* 
	 * PLEASE NOTE THIS FUNCTION DOESN'T CHECK IF THE POSTER IS ALLOWED TO MAKE THIS POST
	*
	*/	
	public function postOnWall($idposter, $message, $idtarget, $postType, $idparent=0, $sendMail=1)
	{
		$message=strip_tags($message);
		$id=-1;
		if($postType==1)
		{
			//This is a project wall post
			//Check to make sure that both idtest and idreceiver are 0
			$data = array('idposter'=>$idposter, 'timestamp'=>time(), 'message' =>$message, 'idproject'=>$idtarget, 'postType'=>$postType);
		}
		elseif($postType==2)
		{
			//This is a test wall post
			$tests = new Tests();
			$testRow = $tests->getTest($idtarget);
			$idproject=$testRow['idproject'];
			$data = array('idposter'=>$idposter, 'timestamp'=>time(), 'message' =>$message, 'idtest'=>$idtarget, 'idproject'=>$idproject, 'postType'=>$postType);
		}
		elseif($postType==3)
		{
			$data = array('idposter'=>$idposter, 'timestamp'=>time(), 'message' =>$message, 'idreceiver'=>$idtarget, 'postType'=>$postType);
		}
		elseif($postType==6)
		{
			$data = array('idposter'=>$idposter, 'timestamp'=>time(), 'message' =>$message, 'idgroup'=>$idtarget, 'postType'=>$postType);
		}
		
		else
		{
			//Something was wrong here @todo throw exception, that's why we have them
			return -1;
		}
			       
		$id = (int) $this->insert($data);
		
		//$id=$this->getAdapter()->lastInsertId();
        
		if($idparent==0)
		{
			$data = array('idparent'=>$id);
			$idparent = $id;
		}
		else
		{
			//$data = array('idparent'=>$idparent);
			//Now we need to check if the parent is valid
			$rowset = $this->find($idparent);
			$row = $rowset->current();
			if($row)
			{
				//The parent exists
				$data = array('idparent'=>$idparent);
				$idparent = $idparent;
			}
			else
			{
				//Nope parent doesn't exist so this is a new parent
				$data = array('idparent'=>$id);
				$idparent = $id;
			}
		}
        
		$this->update($data,'idmessage='.$id);
	                
		if($id>0 && $sendMail)
		{
			$mailer = new Mailer();
			$mailer->sendNewWallPostEmail($idposter, $idtarget, $postType, $message,$idparent);
		}
		
		
		// Reset the status if necessary
		$wallstatus = new WallMessageStatus();
		$wallstatus->changeThreadStatus($idparent, '100');
		
		// dump the wall cache
		$cache = CacheFactory::getCache('wall');
		$cacheKey = 'getWall_' . $idtarget . $postType . $idposter;
		$cacheKey = preg_replace('/[^A-Za-z0-9]/', '_', $cacheKey);
		$cache->remove($cacheKey);
		
		return $id;
	}
	
	/*
	 * Basic update function I've seen elsewhere in the site.  I think it is a nice generica thing to use
	 */
	public function updateWall($idmessage, $data)
	{
		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		
		foreach ($data as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of role cannot be changed');
				}
				else
				{
					$row->{$name}=$value;						
				}
			}
		}
		$row->save();
		return $this; // to allow chaining of methods
	}
	
	/*
	 * This function is used to edit an existing wall message in the system.  The one who posted it should be
	 * the only one who is allowed to edit it.  As such this function should be called with the 
	 * $this->view->currentUser->id
	 * as the $iduser in this function
	 */
	public function editMessage($idmessage, $iduser, $message)
	{
		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		if($row['idposter']==$iduser)
		{
			//Allow the edit
			if($message)
			{
				//The message isn't null so go ahead
				$this->updateWall($idmessage, array('message'=>$message));
				return $row['idparent'];
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return -1;
		}
	}
	
	
	/*
	 * This function will set the delete flag in a post to 1.  First the user must be verified to have
	 * access to the post in question.  The user must either be the project developer for the project
	 * wall or test wall, the receiver of the private wall post or the poster of the message itself
	 * So when this function is called add the 
	 * $this->view->currentUser->id
	 * is in the function call in the controller
	 * This function returns either the idproject, idreceiver, or idtest if sucessful.  This will allow
	 * the view to get the correct new wall.
	 */
	public function deletePost($idmessage, $iduser, $check=0)
	{
		if($check==0)
		{
			$check = $this->canDelete($idmessage, $iduser);
		}
		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		$data = array('isDeleted'=>1);
		
		if($check==1)
		{
			if($row['hasFile']>0)
			{
				//The post has several files associated with it.  These need to be deleted from the database
				//for size constraints
				$wallFile = new WallFile();
				$files=$wallFile->getFilesForPost($idmessage);
				foreach($files as $file)
				{
					$idfiles=$file['idfiles'];
					$this->deleteFileFromPost($idmessage, $idfiles, $iduser, 1);
					
				}
			}
			if($row['idparent']==$idmessage)
			{
				$db=Zend_Registry::get('db');
				$stmt = 'UPDATE wall_message SET isDeleted=1 WHERE idparent='.$row['idparent'];
				$db->query($stmt);
				
			}
			else
			{
				$result = $this->updateWall($idmessage, $data);
			}
			if($row['idreceiver'])
			{
				//This was private wall post
				return $row['idreceiver'];
			}
			elseif($row['idtest'])
			{
				//This was a test wall post
				return $row['idtest'];
			}
			else
			{
				//Project wall post
				return $row['idproject'];
			}
			
		}
		else
		{
			return -1;
		}
	}
	
	public function canDelete($idmessage, $iduser)
	{
		// what's the matter with good old true and false here or 0 and 1 -1 and 1? wtf?
		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		
		$check=-1;
		if(!$row)
		{
			//No entry for the idmessage
			return -1;
		}
		elseif($row['idposter']==$iduser)
		{
			//OK go ahead and do the delete
			$check=1;
		}
		elseif($row['idreceiver'])
		{
			if($row['idreceiver']==$iduser)
			{
				//OK go ahead and do the delete
				$check=1;
			}
			else
			{
				//The user is neither the poster or receiver for a receiver private message so they can't delete
				return -1;
			}
		}
		elseif($row['idtest'])
		{
			$tests = new Tests();
			$testRow = $tests->getTest($row['idtest']);
			$projects = new Projects();
			$projectRow = $projects->getProject($testRow['idproject']);
			if($projectRow['iddeveloper']==$iduser)
			{
				//OK go ahead and do the delete
				$check=1;
			}
			else
			{
				//The user is neither the poster of the project developer
				return -1;
			}
		}
		elseif($row['idproject'])
		{
			$projects = new Projects();
			$projectRow = $projects->getProject($row['idproject']);
			if($projectRow['iddeveloper']==$iduser)
			{
				//OK go ahead and do the delete
				$check=1;
			}
			else
			{
				//The user is neither the poster of the project developer
				return -1;
			}
		}
		else
		{
			//The user has no ability to delete the post
			return -1;
		}
		
		return $check;
	}
	

	/*
	 * This will add a file to a wall post.  If will first check to make sure that the person asking
	 * to do this is the poster of the wall post.  So when this function is called add the 
	 * $this->view->currentUser->id
	 * is in the function call in the controller
	 */
	public function addFileToPost($idmessage,$iduser, $idfile)
	{

		$rowset = $this->find($idmessage);
		$row = $rowset->current();
		if($row)
		{
			if($row['idposter']==$iduser)
			{
				//OK go ahead and add the file
				$projectFiles = new ProjectFiles();
				$category='wal'.$idmessage;
				
				// 0 permissions mean this file is open to anyone. Probably not what we want. 
				$permissions=0; // we need to make this public so that ProjectController::downloadAction will work
				

				$this->updateWall($idmessage, array('hasFile'=>1));
				$wallFile = new WallFile();
				$wallFile->add($idmessage, $idfiles);
				
				return $idfiles;
			}
			else
			{
				//Something went horribly wrong
				return -1;
			}
			
		}
		else
		{
			return -1;
		}
		
	}
    
	/*
	* This Function should be called with the $this->view->currentUser->id to have some validation in the 
	* function.  If the user id is invalid the user can't delete the post.  It should follow the same rules 
	* as the canDelete option which is called with this function
	*/
	public function deleteFileFromPost($idmessage, $idfiles, $iduser, $check=0)
	{
		if($check==0)
		{
			$check=$this->canDelete($idmessage, $iduser);
		}
		if($check==1)
		{
			try 
			{
				//The file can be deleted woohoo
				$wallFile = new WallFile();
				$count = $wallFile->countFilesForMessage($idmessage);
				if($count==1)
				{
					//This means we are deleting the last file for a particular message
					$this->updateWall($idmessage, array('hasFile'=>0));
				}
				$db=Zend_Registry::get('db');
				$db->query("UPDATE wall_file SET isDeleted=1 WHERE idmessage=$idmessage AND idfiles=$idfiles");
				$projectFiles = new ProjectFiles();
				$projectFiles->removeFileFromProject($idfiles);
			}
			catch(Exception $e)
			{
				throw new Exception("Error deleting file :".$e);
				$check=-1;
			}
		}

		return $check;
	}
	
	/*
	 * Changed to handle message status for the user. 
	 */
	public function getWallThread($iduser, $idmessage)
	{

        $tableInfo = array("w" => 'wall_message');
        $columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.idgroup AS idgroup','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
        $select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from($tableInfo, $columns)
            ->joinLeft(array('u'=>'users'), 'w.idposter=u.id', array('u.username AS postername'))
            ->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
            ->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
            ->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
            ->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'))
            ->joinLeft(array('gr'=>'groups'),'gr.idgroup=w.idgroup',array('gr.Name AS groupName'))
            ->where("w.isDeleted=?",0)
            ->where("w.idparent=?",$idmessage)
            ->order("w.idmessage");

		//echo $select->__toString();	
			
		$rows= $this->fetchAll($select);


		$this->setMessageStatus($iduser, $idmessage, '101');
		
		return $rows;

	}
	
	/*
	* 	I am sick of not being able to render a wall like this so I implemented my own interface. Sorry Dean
	* 
	* 
	* 	$timestamp = messages since this time
	* 
	*/
	public function getWall($idtarget, $postType, $idlogin=0,  $pageNumber=1, $resultsPerPage=0, $timestamp=0, $where='')
	{
		$cache = CacheFactory::getCache('wall');
		$rows = array();
				
		$cacheKey = 'getWall_' . $idtarget . $postType . $idlogin . $pageNumber . $resultsPerPage;
		$cacheKey = preg_replace('/[^A-Za-z0-9]/', '_', $cacheKey);
		
		$timer = Timer::start();
		if ( !$rows = $cache->get($cacheKey ) ) {
			switch ($postType) {
			case 1:
				$rows = $this->getProjectWall($idtarget, $pageNumber, $resultsPerPage, $timestamp,$where,$idlogin);
				break;
			case 2:
				$rows = $this->getTestWall($idtarget, $pageNumber, $resultsPerPage, $timestamp,$where,$idlogin);
				break;
			case 3:
				$rows = $this->getUserWall($idtarget, $idlogin, $pageNumber, $resultsPerPage, $timestamp,$where);
				break;
			case 6:
				// 4 unified - 5 single thread
				$rows = $this->getGroupWall($idtarget, $idlogin, $pageNumber, $resultsPerPage, $timestamp);
								
				
				break;
			}
	
			$cache->setE($cacheKey, $rows, Timer::end($timer) );
		}
		
		
		
		return $rows; 
		
	}





	/*
	 * Returns all the messages for a user, in descending time. 
	 * 
	 * Messages are grouped by thread (i.e. only one returned), so multiple new messages in a thread will 
	 * be ignored. 
	 * 
	 */
	public function getUnifiedWall($iduser, $pageNumber=1, $resultsPerPage=0, $timestamp=0, $where='')
	{

		if($timestamp == 0){
			$timestamp = strtotime("-180 day");
		}


		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'w.idposter=u.id', array('u.username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'))
			->where("w.isDeleted=?",0)
			->where($this->getWhereStatementForAllUserMessages($iduser))
			->where("w.timestamp > ?",$timestamp)
 			->order("timestamp DESC")
			->group('w.idparent');
			
		if($where)
			$select->where($where);	
		
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
			
		fb($select->__toString());	
		$rows = $this->fetchAll($select);
		
		
		return $rows;
	}
	
	public function getGroupWall($idgroup,$iduser, $pageNumber=1, $resultsPerPage=0, $timestamp=0)
	{
		
		$whereInGroup = "w.idgroup IN (SELECT idgroup FROM groups_user WHERE iduser=$iduser)";
		$whereInGroup .= " OR w.idgroup IN (SELECT idgroup FROM groups WHERE idowner=$iduser)";
		
			
		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'w.idposter=u.id', array('u.username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->joinLeft(array('gr'=>'groups'),'gr.idgroup=w.idgroup',array('gr.Name AS groupName'))
			->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'))
			->where("w.isDeleted=?",0)
			->where("w.idgroup=?",$idgroup)
			->where($whereInGroup)
			->where("w.timestamp > ?",$timestamp)
 			->order("timestamp DESC")
			->group('w.idparent');
		
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
			
		//echo $select->__toString();	
		$rows = $this->fetchAll($select);
		
		
		return $rows;
	}
	
	
	/*
	* Odds are this post will never be used as the project wall gets you everything you need just there
	* but I figure I should add it just in case I am wrong again.
	* 
	* iduser is for checking for new messages
	* 
	*/
	public function getTestWall($idtest,  $pageNumber=1, $resultsPerPage=0, $timestamp=0, $where='', $iduser=0)
	{
		$rows=$this->getTestWallOrder($idtest);
		if($rows)
		{
			$orderClause='FIELD(w.idparent ';
			foreach($rows as $row)
			{
				$orderClause=$orderClause.','.$row['idparent'];
			}
			$orderClause=$orderClause.')';
		}
		else
		{
			$orderClause='w.idparent';
		}
		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'idposter=u.id', array('username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->where("w.isDeleted=?",0)
			->where("w.idtest=?",$idtest)
			->where("w.timestamp > ?",$timestamp)
			->order(array($orderClause, 'w.timestamp'))
			->group('w.idparent');
			
		if($iduser != 0)
			$select->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'));	

		if($where)
			$select->where($where);	
			
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
			
		$rows2= $this->fetchAll($select);
		
		//echo $select->__toString();	
		
		return $rows2;
	}
    
	private function getTestWallOrder($idtest)
	{
		$stmt = 'SELECT idparent, MAX(timestamp) FROM wall_message WHERE idtest='.$idtest.' AND isDeleted=0 GROUP BY idparent ORDER BY MAX(timestamp) DESC';
		$db=Zend_Registry::get('db');
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
	}
    
	public function getProjectWall($idproject,  $pageNumber=1, $resultsPerPage=0, $timestamp=0, $where='', $iduser=0)
	{
		$rows=$this->getProjectWallOrder($idproject);
		if($rows)
		{
			$orderClause='FIELD(w.idparent ';
			foreach($rows as $row)
			{
				$orderClause=$orderClause.','.$row['idparent'];
			}
			$orderClause=$orderClause.')';
		}
		else
		{
			$orderClause='w.idparent';
		}
		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'idposter=u.id', array('username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->where("w.isDeleted=?",0)
			->where("w.idproject=?",$idproject)
			->where("ISNULL(w.idtest)")
			->where("w.timestamp > ?",$timestamp)
			->order(array($orderClause, 'w.timestamp'))
			->group('w.idparent');

		if($iduser != 0)
			$select->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'));	

		if($where)
			$select->where($where);	

			
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);
		fb($select->__toString());
		$rows2= $this->fetchAll($select);
		return $rows2;
	}
    
	private function getProjectWallOrder($idproject)
	{
		$stmt = 'SELECT idparent, MAX(timestamp) FROM wall_message WHERE idproject='.$idproject.' AND isDeleted=0 AND ISNULL(idtest) GROUP BY idparent ORDER BY MAX(timestamp) DESC';
		$db=Zend_Registry::get('db');
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
    
	}
    
	public function getUserWall($iduser, $idlogin=0,  $pageNumber=1, $resultsPerPage=0, $timestamp=0, $where='')
	{
		
		$rows=$this->getUserWallOrder($iduser, $idlogin);
		if($rows)
		{
			$quickCheck=0;
			$orderClause='FIELD(w.idparent ';
			foreach($rows as $row)
			{
				if($quickCheck==0)
				{
					$whereClause='w.idparent IN ('.$row['idparent'];
					$quickCheck=1;
				}
				else
				{
					$whereClause=$whereClause.','.$row['idparent'];
				}
				$orderClause=$orderClause.','.$row['idparent'];
			}
			$orderClause=$orderClause.')';
			$whereClause=$whereClause.')';
		}
		else
		{
			$orderClause='w.idparent';
			//This should now have the query look for isDeleted=0 AND isDeleted=1
			//This is imposible but so is getting here, if there are no rows we've 
			//No wall entries
			$whereClause='w.isDeleted=1';
		}
		
		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'w.idposter=u.id', array('u.username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->where("w.isDeleted=?",0)
			->where("w.timestamp > ?",$timestamp)
			->where($whereClause)
			->order(array($orderClause, 'w.timestamp'))
			->group('w.idparent');
			
		if($iduser != 0)
			$select->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser = '.$iduser,array('s.status AS status'));	

		if($where)
			$select->where($where);	
			
		if($resultsPerPage)
			$select->limitPage($pageNumber, $resultsPerPage);

		//echo $select->__toString();
			
		$rows2= $this->fetchAll($select);
		return $rows2;

	}
    
	private function getUserWallOrder($iduser, $idlogin=0)
	{
		if($idlogin==0 || $iduser==$idlogin)
		{
			$stmt = 'SELECT idparent, MAX(timestamp) FROM wall_message WHERE (idreceiver='.$iduser.' OR (idposter='.$iduser.' AND ISNULL(idproject))) AND isDeleted=0 GROUP BY idparent ORDER BY MAX(timestamp) DESC';
		}
		else
		{
			$stmt = 'SELECT idparent, MAX(timestamp) FROM wall_message WHERE ((idreceiver='.$iduser.' AND idposter='.$idlogin.') OR (idreceiver='.$idlogin.' AND idposter='.$iduser.')) AND isDeleted=0 GROUP BY idparent ORDER BY MAX(timestamp) DESC';
		}
		
		$db=Zend_Registry::get('db');
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
	}
    
	/*
	* PLEASE DO NOT DELETE THIS METHOD I NEED IT TO FIX THINGS LATER
	*/
	public function fixCommentPost($idposter, $message, $idtarget, $timestamp, $postType, $idparent=0)
	{
		$id=-1;
		if($postType==1)
		{
			//This is a project wall post
			//Check to make sure that both idtest and idreceiver are 0
			$data = array('idposter'=>$idposter, 'timestamp'=>$timestamp, 'message' =>$message, 'idproject'=>$idtarget);
		}
		elseif($postType==2)
		{
			//This is a test wall post
			$tests = new Tests();
			$testRow = $tests->getTest($idtarget);
			$idproject=$testRow['idproject'];
			$data = array('idposter'=>$idposter, 'timestamp'=>$timestamp, 'message' =>$message, 'idtest'=>$idtarget, 'idproject'=>$idproject);
		}
		elseif($postType==3)
		{
			$data = array('idposter'=>$idposter, 'timestamp'=>$timestamp, 'message' =>$message, 'idreceiver'=>$idtarget);
		}
		else
		{
			//Something was wrong here
			return -1;
		}
        
		$result = $this->insert($data);
		$id=$this->getAdapter()->lastInsertId();
		
		if($idparent==0)
		{
			$data = array('idparent'=>$id);
		}
		else
		{
			//$data = array('idparent'=>$idparent);
			//Now we need to check if the parent is valid
			$rowset = $this->find($idparent);
			$row = $rowset->current();
			if($row)
			{
				//The parent exists
				$data = array('idparent'=>$idparent);
			}
			else
			{
				//Nope parent doesn't exist so this is a new parent
				$data = array('idparent'=>$id);
			}
		}
		
		$this->updateWall($id, $data);                
		
		return $id;
	}
    
    
    
	public function showFullUserWall($iduser)
	{
		$stmt="SELECT idreceiver, idtest, idproject, MAX(timestamp) FROM wall_message WHERE isDeleted=0 AND (idtest IN (SELECT idtest FROM tests JOIN projects ON tests.idproject=projects.idproject WHERE projects.iddeveloper=$iduser) OR idtest IN (SELECT idtest FROM tests WHERE idtester=$iduser) OR idproject IN (SELECT idproject FROM projects WHERE iddeveloper=$iduser) OR (idproject IN (SELECT idproject FROM tests WHERE idtester=$iduser) AND ISNULL(idtest)) OR idreceiver=$iduser OR idparent IN (SELECT idparent FROM wall_message WHERE idposter=$iduser AND NOT ISNULL(idreceiver))) GROUP BY idreceiver, idtest, idproject ORDER BY MAX(timestamp) DESC";
		$db=Zend_Registry::get('db');
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
	}
	
	
	public function getUserProjectsAndTests($iduser, $timestamp=0)
	{		
		$db=Zend_Registry::get('db');
		$dogs=$this->userDashBoardOrder($iduser, $timestamp);
		if($dogs)
		{
			$orderClause='FIELD(wm1.idproject ';
			foreach($dogs as $dog)
			{
				$orderClause=$orderClause.','.$dog['idproject'];
			}
			$orderClause=$orderClause.')';
		}
		else
		{
			$orderClause='(wm1.idproject)';
		}
		$users  = new Users();
		$userRow=$users->getUser($iduser);
		if($userRow['last_login'])
		{
			$last=$userRow['last_login'];
			$lastLogin = strtotime($last);
			if($timestamp>$lastLogin)
			{
				$lastLogin=$timestamp;
			}
			
		}
		else
		{
			$lastLogin=0;
		}
		
		$stmt="SELECT wm1.idtest AS idtest, wm1.idproject AS idproject, MAX(wm1.timestamp) AS timestamp, p.Name AS projectname
		(SELECT COUNT(w.idmessage) FROM wall_message w WHERE w.idtest=wm1.idtest OR (w.idproject=wm1.idproject AND ISNULL(wm1.idtest) AND ISNULL(w.idtest)) AND w.timestamp>$timestamp AND isDeleted=0) AS totalMessages
				 FROM wall_message wm1 LEFT JOIN projects p ON wm1.idproject=p.idproject WHERE wm1.isDeleted=0 AND wm1.timestamp>$timestamp AND (wm1.idtest IN (SELECT idtest FROM tests JOIN projects ON tests.idproject=projects.idproject WHERE projects.iddeveloper=$iduser) OR wm1.idtest IN (SELECT idtest FROM tests WHERE idtester=$iduser) OR wm1.idproject IN (SELECT idproject FROM projects WHERE iddeveloper=$iduser) OR (wm1.idproject IN (SELECT idproject FROM tests WHERE idtester=$iduser) AND ISNULL(idtest))) GROUP BY wm1.idproject, wm1.idtest ORDER BY ".$orderClause.", wm1.idtest";
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
	}
	
	private function userDashBoardOrder($iduser, $timestamp=0)
	{
		$stmt="SELECT idproject, MAX(timestamp) AS timestamp FROM wall_message WHERE isDeleted=0 AND timestamp>$timestamp AND (idproject IN (SELECT idproject FROM projects WHERE iddeveloper=$iduser) OR (idproject IN (SELECT idproject FROM tests WHERE idtester=$iduser))) GROUP BY idproject ORDER BY MAX(timestamp) DESC";
		$db=Zend_Registry::get('db');
		$query=$db->query($stmt);
		$rows=$query->fetchAll();
		return $rows;
		
	}
	
	public function hasAccess($iduser, $idtarget, $postType)
	{
		$db=Zend_Registry::get('db');
		if($postType==1)
		{
			//It is a project wall I need to know if the user is a developer or tester for that project
			$stmt="SELECT * FROM tests WHERE idproject=$idtarget AND idtester=$iduser AND Status IN (250,300, 350)";
			$query=$db->query($stmt);
			if($query->fetchAll())
			{
				return 1;
			}
			else
			{
				$stmt="SELECT * FROM projects WHERE idproject=".$idtarget." AND iddeveloper=".$iduser;
				$query2=$db->query($stmt);
				if($query2->fetchAll())
				{
					return 1;
				}
			}
			
		}
		elseif($postType==2)
		{
			//This is a test wall I need to know if the user is a developer for the project or the tester
			$tests = new Tests();
			if($testData=$tests->getTest($idtarget))
			{
				$projects = new Projects();
				if($projectData=$projects->getProject($testData['idproject']))
				{
					if($projectData['iddeveloper']==$iduser||$testData['idtester']==$iduser)
					{
						return 1;
					}
				}
			}			
		}
		elseif($postType==3)
		{
			//It is a user wall only the user has access this is easy all we need check is to see if the
			//iduser is the same as idtarget
			if($iduser==$idtarget)
			{
				return 1;
			}

		}
		elseif($postType==4)
		{
			//It is a user wall only the user has access this is easy all we need check is to see if the 
			//iduser is the same as idtarget
			//if($iduser==$idtarget)
			//{
				return 1;
			//}
		}
		elseif($postType==5)
		{
			// This is a message thread. todo check if they can read it
				return 1;
			
		}
		elseif($postType==6)
		{
			$groups = new Groups();
			if($groups->inGroup($iduser,$idtarget))
				return 1; 
			
		}
		
		return 0;
	}
	
	
	private function getWhereStatementForAllUserMessages($iduser){
		$where = "w.idreceiver=$iduser OR ";
		$where .= "w.idposter=$iduser OR ";
		$where .= "(w.idproject IN (SELECT idproject FROM tests WHERE idtester=$iduser AND tests.Status IN (250,300, 350)) AND ISNULL(w.idtest))OR ";
		$where .= "w.idproject IN (SELECT idproject FROM projects WHERE iddeveloper=$iduser ) OR ";
		$where .= "w.idtest IN (SELECT idtest FROM tests WHERE idtester=$iduser)";
		
		$groups = new Groups(); 
		
		$groupList = $groups->listOpenGroupsForUser($iduser);
		
		if(isset($groupList) && sizeof($groupList)>0){
			$where .= " OR w.idgroup IN (";
			foreach($groupList as $group){
					$where .= $group->idgroup;
					$where .= ',';
			}
			$where = substr($where,0,-1);
			$where .= ")";
		}

		//echo $where;
		
		return $where; 
	}
	
	
	
	public function setMessageStatus($iduser, $idparent,$status){
		$wallstatus = new WallMessageStatus(); 
		$wallstatus->addOrUpdateStatus($iduser, $idparent, $status);
	}
	
	
	/*
	 * 4.5.2 db required for this
	 */
	public function countUnread($iduser)
	{

		$maxage = strtotime("-10 day");
		
		$cache = CacheFactory::getCache('wall');
		$countValue = 0;
		
		$cacheKey = 'countUnread_' . $iduser .'_'.$maxage;
		
		$timer = Timer::start();
		if ( !$countValue = $cache->get($cacheKey) ) {
				
				$tableInfo = array("w" => 'wall_message');
				$columns = array('w.idparent');
				$select=$this->select();
				$select->setIntegrityCheck(false);
				$select->from($tableInfo, $columns)
					->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser='.$iduser)
					->where("w.isDeleted=?",0)
					->where($this->getWhereStatementForAllUserMessages($iduser))
					->where("status IS NULL or status = 100")
					->where("timestamp > ?",$maxage)
					->group('w.idparent');		
				// echo $select->__toString();	
				$rows= $this->fetchAll($select);
			
				//$row=$rows->current();
				$countValue = $rows->count(); 
				
				$cache->setE($cacheKey, $countValue, Timer::end($timer) );
		} else {
			$elapsed = Timer::end($timer);
		}
		
		return $countValue;
	}
	
	public function countThreads($idtarget, $postType, $timestamp=0,$iduser=0,$where='')
	{
		if($postType==1)
		{
			//It is a project wall
			$whereClause="idproject=$idtarget AND ISNULL(idtest)";
		}
		elseif($postType==2)
		{
			//This is a test wall
			$whereClause="idtest=$idtarget";
		}
		elseif($postType==3)
		{
			
			$user = Zend_Registry::get('defSession')->currentUser->id;
			
			if($user == $idtarget){
				$whereClause="( idreceiver=$idtarget OR idposter=$idtarget AND ISNULL(idproject))";
				
			}else{
				$whereClause="( (idreceiver=$idtarget AND idposter=$user AND ISNULL(idproject)) OR 
							(idreceiver=$user AND idposter=$idtarget AND ISNULL(idproject))	)";	
			}
			
		}
		elseif($postType==4)
		{
			//This is a test wall
			$whereClause=$this->getWhereStatementForAllUserMessages($idtarget);
		}
		elseif($postType==6)
		{
			$whereClause="idgroup=$idtarget";
		}
		
		$tableInfo = array("w" => 'wall_message');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array('idmessage');
		$select->from($tableInfo, $columns)
			->where("isDeleted = ?", 0)
			->where($whereClause)
			->where("timestamp > ?",$timestamp)
			->group('w.idparent');
		
		if($iduser){
			$select->joinLeft(array('s'=>'wall_message_status'),'w.idparent=s.idparent and s.iduser='.$iduser,array('s.status AS status'));
		}	
				
		if($where)
			$select->where($where);
					
		//echo $select->__toString();				
		$rows=$this->fetchAll($select);
		return $rows->count(); 
		
	}
	
	public function countPosts($idtarget, $postType, $timestamp=0, $user=0)
	{
		if($postType==1)
		{
			//It is a project wall
			$whereClause="idproject=$idtarget AND ISNULL(idtest)";
		}
		elseif($postType==2)
		{
			//This is a test wall
			$whereClause="idtest=$idtarget";
		}
		elseif($postType==3)
		{
			if($user==$idtarget)
			{
				//The user logged in is the page we are viewing.  So it is everything
				$whereClause="(idreceiver=$idtarget)";
		
			}
			elseif($user>0)
			{
				//The logged in user is view someone else's page
				$whereClause="(idposter=$idtarget AND idreceiver=$user)";
			}
			else
			{
				$whereClause="(idreceiver=$idtarget OR (idposter=$idtarget AND ISNULL(idproject)))";
			}
		}
		
		elseif($postType==4)
		{
			//This is a test wall
			$whereClause=$this->getWhereStatementForAllUserMessages($idtarget);
		}
		
		
		$select=$this->select();
		$tableInfo = array("w" => 'wall_message');
		$columns = array('COUNT(w.idmessage) as count');
		$statement = $select->from($tableInfo, $columns)
			->where("isDeleted = ?", 0)
			->where($whereClause)
			->where("timestamp > ?",$timestamp);
		if($user) {
			$statement->where("idposter!=?",$user);
		}
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	public function countTestPostsByProject($idproject, $timestamp=0, $user=0)
	{

		$whereClause="(idproject=$idproject AND NOT ISNULL(idtest))";

		$select=$this->select();
		$columns = array("count" =>'COUNT(idmessage)');
		$statement = $select->from($this, $columns)
			->where("isDeleted = ?", 0)
			->where($whereClause)
			->where("timestamp > ?",$timestamp);
		if($user) {
			$statement->where("idposter!=?",$user);
		}
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	public function countTestPostsByProjectAndTester($idproject, $idtester, $timestamp=0)
	{

		$whereClause="(wall_message.idproject=$idproject AND NOT ISNULL(wall_message.idtest)) AND test.idtester=$idtester AND wall_message.idposter!=$idtester";

		$select=$this->select();
		$columns = array("count" =>'COUNT(idmessage)');
		$statement = $select->from($this, $columns)
			->join(array('test'=>'tests'),'wall_message.idtest=test.idtest',array())
			->where("isDeleted = ?", 0)
			->where($whereClause)
			->where("timestamp > ?",$timestamp);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	/*
	 * This one was written for Richard as he asked for one to just count new posts but not for the user
	 * who wrote them.  It is here now.
	 */
	public function countNewNotMyPosts($idtarget, $postType,$iduser, $timestamp=0)
	{
		if($postType==1)
		{
			//It is a project wall
			$whereClause="idproject=$idtarget AND ISNULL(idtest) AND idposter<>$iduser";
		}
		elseif($postType==2)
		{
			//This is a test wall
			$whereClause="idtest=$idtarget AND idposter<>$iduser";
		}
		else
		{
			$whereClause="idreceiver=$idtarget";
		}
		$select=$this->select();
		$columns = array("count" =>'COUNT(idmessage)');
		$statement = $select->from($this, $columns)
			->where("isDeleted = ?", 0)
			->where($whereClause)
			->where("timestamp > ?",$timestamp);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	public function getTestWallForAdmin($idtest)
	{
		$rows=$this->getTestWallOrder($idtest);
		if($rows)
		{
			$orderClause='FIELD(w.idparent ';
			foreach($rows as $row)
			{
				$orderClause=$orderClause.','.$row['idparent'];
			}
			$orderClause=$orderClause.')';
		}
		else
		{
			$orderClause='w.idparent';
		}
		$tableInfo = array("w" => 'wall_message');
		$columns = array('w.idmessage AS idmessage','w.idparent AS idparent','w.timestamp AS timestamp','w.idposter AS idposter','w.idreceiver AS idreceiver','w.idtest AS idtest','w.idproject AS idproject','w.message AS message', 'w.hasFile AS hasFile','w.isDeleted AS isDeleted','w.postType AS postType');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($tableInfo, $columns)
			->joinLeft(array('u'=>'users'), 'idposter=u.id', array('username AS postername'))
			->joinLeft(array('p'=>'profile_contactinfo'),'p.id=u.id',array('p.image as Avatar', 'p.time_zone'))
			->joinLeft(array('u1'=>'users'), 'w.idreceiver=u1.id', array('u1.username AS receivername'))
			->joinLeft(array('pr'=>'projects'),'pr.idproject=w.idproject',array('pr.Name AS projectName'))
			->where("w.idtest=?",$idtest)
			->order(array($orderClause, 'w.timestamp'));
			
		$rows2= $this->fetchAll($select);
		return $rows2;
	}
	
	
	
}