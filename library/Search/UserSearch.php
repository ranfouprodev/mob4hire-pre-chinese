<?php
require_once 'Zend/Search/Lucene.php';

class Search_UserSearch extends Search_SearchBase implements Search_SearchInterface{
	
	public function __construct() {
		parent::__construct( "searchuser" );
	}
		
	
	public function dailyIndex(){

		// Recreate the full search index
		$userTable = new Users(); 
		
		$select=$userTable->select();
		$select->setIntegrityCheck(false);
		
		$tableInfo = array("u" => 'users');
		$columns = array('u.id as iduser');
		
		$yesterday = strtotime('-1 days');
		
		$select->from($tableInfo, $columns)
				->where("last_login > ".$yesterday);
		//$select->limit(500);
		
		$users = $userTable->fetchAll($select);
		
		foreach($users as $userid){
			$this->add($userid->iduser);
		}
		
		$this->optimize();
		
	}
	
	public function fullIndex(){

        Zend_Registry::get('logger')->debug("Full Index");

		// Recreate the full search index
		$userTable = new Users(); 
		
		$select=$userTable->select();
		$select->setIntegrityCheck(false);
		
		$tableInfo = array("u" => 'users');
		$columns = array('u.id as iduser');
		
		//$select->from($tableInfo, $columns);

        $yesterday = strtotime('-180 days');

        $select->from($tableInfo, $columns)
            ->where("last_login > ".$yesterday);

		
		$users = $userTable->fetchAll($select);
		
		foreach($users as $userid){
			$this->add($userid->iduser);
		}
		
		$this->optimize();
		
	}
	
	public function add($id){
		$userTable = new Users(); 
		$user = $userTable->getUserForSearchUpdate($id);
			
		$regdevices=new Regdevices();
		$handsets=$regdevices->listRegdevices('regdevices.idtester='.$id.' and not isnull(d.model)');
			
		
		$doc = new Zend_Search_Lucene_Document(); 
		
		$doc->addField(Zend_Search_Lucene_Field::Binary('iduser',$user->id));
		$doc->addField(Zend_Search_Lucene_Field::Text('name',$this->removeNonUTF($user->username)));
		
		if(isset($user->company))
			$doc->addField(Zend_Search_Lucene_Field::Text('company',$this->removeNonUTF($user->company)));
		
		if(isset($user->personal_description_other))
			$doc->addField(Zend_Search_Lucene_Field::Text('description',$this->removeNonUTF($user->personal_description_other)));
			
		if(isset($user->country)){
			$doc->addField(Zend_Search_Lucene_Field::Text('country',$user->iso . " " . $user->country));
		}
		
		$regdevices=new Regdevices();
		$handsets=$regdevices->listRegdevices('regdevices.idtester='.$id.' and not isnull(d.model)');
		
		foreach($handsets as $handset){
			if(isset($handset->osSymbian))
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"Symbian"));
			
			if(isset($handset->osAndroid))
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"Android"));

			if(isset($handset->osRim)){
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"RIM Blackberry"));
			}
			
			if(isset($handset->osWindows)){
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"Windows Phone 7"));
			}
			
			if(isset($handset->osOsx)){
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"iOS OSX"));
			}
			
			if(isset($handset->osLinux)){
				$doc->addField(Zend_Search_Lucene_Field::Text('os',"Linux Maemo"));
			}
			
			$doc->addField(Zend_Search_Lucene_Field::Text('network',$handset->network));
			
			$model = $handset->network ." ".$handset->vendor ." ".$handset->model.",";
			
			$doc->addField(Zend_Search_Lucene_Field::Text('handset',$this->removeNonUTF($model)));
			
		}
	
		
		$this->searchIndex->addDocument($doc);
		
	}
	
	public function delete($index){
		
	}
	
	
	
	protected function removeNonUTF($output){
		 return preg_replace('/[^(\x20-\x7F)]*/','', $output);
	}
}
?>