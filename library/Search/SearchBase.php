<?php
require_once 'Zend/Search/Lucene.php';

class Search_SearchBase {
	
	protected $searchIndex;
	
	public function __construct($name) {
		try{
			$this->searchIndex = Zend_Search_Lucene::open('/../search_cache/'.$name);
		}catch(Exception $e){
			$this->searchIndex = Zend_Search_Lucene::create('../search_cache/'.$name);
		}
	}
	
	public function search($searchString, $limit = '50'){
	
		Zend_Search_Lucene::setResultSetLimit($limit);
		
		$hits = $this->searchIndex->find($searchString);
		
		/*foreach($hits as $hit){
			echo $hit->iduser .":";
			echo $hit->score ."<br/>";
		}*/
		
		return $hits;
		
	}
	
	
	protected function removeNonUTF($output){
		 return preg_replace('/[^(\x20-\x7F)]*/','', $output);
	}
	

	public function count(){
		return $this->searchIndex->numDocs(); 
	}
	
	public function optimize(){
		return $this->searchIndex->optimize();
	}
	
	public function getIndex(){
		return $this->searchIndex;
	}
	
	
	
}
?>