<?php
require_once 'Zend/Search/Lucene.php';

class Search_ProjectSearch extends Search_SearchBase implements Search_SearchInterface{
	
	public function __construct() {
		parent::__construct( "projectuser" );
	}
		
	
	public function fullIndex(){

		// Recreate the full search index
		$projectTable = new Projects(); 
		
		$select=$projectTable->select();
		$select->setIntegrityCheck(false);
		
		$tableInfo = array("p" => 'projects');
		$columns = array('p.idproject as idproject');
		
		$select->from($tableInfo, $columns);
		//$select->limit(500);
		
		$projects = $projectTable->fetchAll($select);
		
		foreach($projects as $idproject){
			$this->add($idproject->idproject);
		}
		
		$this->optimize();
		
	}
	
	public function add($id){
		$projectTable = new Projects(); 
		$project = $projectTable->getProject($id);
					
		$doc = new Zend_Search_Lucene_Document(); 
		
		$doc->addField(Zend_Search_Lucene_Field::UnIndexed('idproject',$project->idproject));
		$doc->addField(Zend_Search_Lucene_Field::Text('name',$this->removeNonUTF($project->Name)));
		
		if(isset($project->description))
			$doc->addField(Zend_Search_Lucene_Field::Text('description',$this->removeNonUTF($project->description)));
		
		if(isset($user->longDesc))
			$doc->addField(Zend_Search_Lucene_Field::Text('long',$this->removeNonUTF($project->longDesc)));
			
		if(isset($user->username)){
			$doc->addField(Zend_Search_Lucene_Field::Text('developer',$this->removeNonUTF($project->username)));
		}
		
		
		$devices = $projectTable->getSupportedDevices($id);
		
		
		foreach($devices as $device){
			if(isset($device->platform)){
				switch($device->platform){
					case "osRIM":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"RIM Blackberry"));
						break;
					case "osSymbian":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"Symbian"));
						break;
					case "osAndroid":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"Android"));
						break;
					case "osWindows":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"Windows Phone 7"));
						break;
					case "osOsx":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"iOS OSX"));
						break;
					case "osLinux":
						$doc->addField(Zend_Search_Lucene_Field::Text('os',"Linux Maemo"));
						break;
				}	
			}
				
			if(isset($device->vendor)){
				$doc->addField(Zend_Search_Lucene_Field::Text('handset',$device->vendor));	
			}
			
			if(isset($device->model)){
				$doc->addField(Zend_Search_Lucene_Field::Text('handset',$device->vendor.' '.$device->model));	
			}
		}	
			

		$networks = $projectTable->getSupportedNetworks($id);

		foreach($networks as $network){
			if(isset($network->network)){
					$doc->addField(Zend_Search_Lucene_Field::Text('network',$network->network));	
				}
				
			if(isset($network->country)){
					$doc->addField(Zend_Search_Lucene_Field::Keyword('country',$network->country));	
				}
				
		}
	
		
		$this->searchIndex->addDocument($doc);
		
	}
	
	public function delete($index){
		
	}
	
	
	
}
?>