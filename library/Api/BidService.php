<?php
require_once "ServiceBase.php";
require_once "BidServiceInterface.php";
require_once "Dto/BidDto.php";

class Api_BidService extends Api_ServiceBase implements Api_BidServiceInterface {
	
	public function __construct( $url, $authId, $apiKey ) {
		parent::__construct( $url, $authId, $apiKey, "bids" );
		$this->setLogLevel(3);
	}
	
	/**
	 * Retrieve a count of all bids (optionally filtered by a project)
	 * @param object $project [optional].  Defaults to -1 which means no filtering.
	 * @return The count of projects
	 */
	public function countBids($project = -1 ) {
		$params = array();
        $params["project"] = $project;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/count");
			return (int)$response;
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return 0;
	}
	
	/**
     * This function will return a list of bids.  Use $start and $count for
     * pagination.
     *
     * @param int $project		The project to filter bids on. Default is -1 which does no filtering on project
     * @param int $user			The user to get bids for.  Default is -1 which does no filtering on user
     * @param int $status		The status to filter bids on.  Default is -1 which does no filtering on status
     * @param int $start		The starting item to retrieve (ie. 11)
     * @param int $count		The number of items to retrieve (ie. 10)
     * @param string $sortField	The field to sort the results on. Defaults to "id"
     * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
     *
     * @return  				An array containing BidDto objects
     */
    public function listBids($project = -1, $user = -1, $status = -1, $start = 0, $count = 10, $sortField = "id", $direction = "Asc") {
    	$params = array();
		$params["project"] = $project;
		$params["user"] = $user;
		$params["status"] = $status;
        $params["start"] = $start;
        $params["count"] = $count;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        
        $this->setParams($params);
        
		$response = NULL;
		$result = array();
		try {
	        $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/list");
		        $xmlIterator = $response->getIterator();
	        $i = 0;
	        foreach ($xmlIterator->bid as $bidXml) {
	            $result[$i++] = $this->bidFromXml( $bidXml );
	        }
		} catch ( Exception $err ) {
			$this->logError($err->getMessage());
		}
		
        return $result;
    }
	
	/**
	 * Retrieve a bid by it's ID
	 * @param int $bidId		The ID of the Bid to retrieve
	 * @return 					A BidDto or null if the ID wasn't found.
	 */
	public function getById( $bidId ) {
		$params = array();
        $params["id"] = $bidId;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/getById");
			$user = $this->userFromXml( $response->getIterator() );
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return $user;
	}
	
	/**
	 * Create a new bid.
	 * @param BidDto $bid			The bid to create.  
	 * @return						A BidDto object containing the created bid. (including database ID) 
	 */
	public function createProject( $bidDto ) {
		if ( !is_object($bidDto) || !($bidDto instanceof Api_BidDto ) ) {
    		throw new Exception("project must not be null and must be an instance of Api_BidDto" );
    	}
		$project = null;
		$params = array();
        $params["_body"] = $this->xmlFromBid( $bidDto );
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		try {
            $response = $this->sendRequest('PUT', "/" . $this->getServicePrefix() . "api/v1/create");
            $bid = $this->bidFromXml( $response->getIterator() );
        }
        catch(Exception $err) {
        	print $err->getMessage() ."\n";
        }
		
		return $bid;
	}
	
	/**
	 * Delete an existing project
	 * @param int $bidId			The ID of the bid to delete
	 * @return 						A boolean.  False indicates failure.
	 */
	public function deleteBid( $bidId ) {
		$params = array();
        $params["id"] = $bidId;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
		$response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() .  "/api/v1/delete");
			if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
				return FALSE;
			}
			return TRUE;
        }
        catch(Exception $err) {
        	print "\nException: " . $err->getMessage() . "\n";
        }
		
		return FALSE;
	}
	
	private function bidFromXml( $xml ) {
		$bid = new Api_BidDto();
		$bid->id = intval( $xml->id );
		$bid->userId = intval($xml->userId);
		$bid->projectId = intval($xml->projectId);
		$bid->amount = (float)$xml->amount;
		$bid->status = (int)$xml->status;
		$bid->created = new DateTime();
		$bid->created->setTimestamp((int)$xml->created);
		return $bid;
	}
	
	private function xmlFromBid( $bid ) {
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$xml .= "<bid><id>" . $bid->id . "</id>";
		$xml .= "<userId>" . $bid->userId . "</userId>";
		$xml .= "<projectId>" . $bid->projectId . "</projectId>";
		$xml .= "<amount>" . $bid->amount . "</amount>";
		$xml .= "<status>" . $bid->status . "</status>";
		$xml .= "<created>" . $bid->created->getTimestamp() . "</created>";
		$xml .= "</bid>";
		return $xml; 
	}
}
?>