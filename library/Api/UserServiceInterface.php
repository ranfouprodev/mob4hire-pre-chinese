<?php


interface Api_UserServiceInterface {
	
	/**
	 * This function will return a list of users.  Use $start and $count for
	 * pagination.
	 * 
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing UserDto objects
	 */
	public function listUsers( $start = 0, $count = 10, $sortField = "id", $direction="Asc" );
	
	/**
	 * Retrieve a user by their username.
	 * @param string $username	The username of the user to retrieve	
	 * @return 					A UserDto object containing the user or NULL if the user was not found.
	 */
	public function getByUsername( $username );
	
	/**
     * Retrieve a user by their email address.
     * @param string $email		The email address of the user to retrieve
     * @return 					A UserDto object containing the user or NULL if the user was not found.
     */
	public function getByEmail( $email );
	
	/**
     * Retrieve a user by their database id.
     * @param integer $id		The database ID of the user to retrieve
     * @return 					A UserDto object containing the user or NULL if the user was not found.
     */
    public function getById($id);
	
	public function getBatchUsers( $ids );
	
	/**
	 * Create a new User object.
	 * @param UserDto 	$userDto The user to create. Leave id empty
	 * @return 			True if successful otherwise false
	 */

	public function create( $userDto );
	public function update( $userDto );
	public function delete( $id );
	public function authenticate( $username, $password );
	
}
?>