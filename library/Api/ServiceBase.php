<?php

require_once 'Zend/Rest/DeviceAtlasCloudClient.php';

class Api_ServiceBase extends Zend_Rest_client {
	
	protected $authId;
	protected $apiKey;
	protected $_responseType = 'xml';
    protected $_responseTypes = array('php', 'xml', 'json');
	protected $_params = array();
	protected $_debugging = false;
	protected $_logLevel = 3 ; // Only show INFO and ERROR messages
	protected $_servicePrefix = "projecs";
	
	public function __construct( $url, $authId, $apiKey, $servicePrefix ) {
		$this->authId = $authId;
		$this->apiKey = $apiKey;
		$this->setUri($url);
		$client = self::getHttpClient();
		$client->setHeaders('Accept-Charset', 'ISO-8859-1,utf-8');
		$client->setEncType("application/xml");
	}
	
	public function OverrideServicePrefix( $prefix ) {
		$this->logDebug("Setting service Prefix to " . $prefix );
		$this->_servicePrefix = $prefix;
	}
	
	public function getServicePrefix() {
		return $this->_servicePrefix;
	}
	
	public function setDebugging( $debugging ) {
		$this->_debugging = $debugging;
	}
	
	public function isDebug() {
		return $this->_debugging;
	}
	
	public function setParams($params)
    {
    	// Validate mandatory parameters to endpoint
    	if (!isset($params['apikey']) || !isset($params['authid'])) {
            throw new Exception('APIKey or AuthID Missing');
        }

        foreach ($params as $key => $value) {
            switch (strtolower($key)) {
                case 'type':
                    $this->setResponseType($value);
                    break;
                default:
                    $this->_params[$key] = $value;
                    break;
            }
        }

        return $this;
    }

    public function getParams()
    {
    	return $this->_params;
    }


    public function setResponseType($responseType)
    {
        if (!in_array(strtolower($responseType), $this->_responseTypes)) {
            throw new Exception('Invalid Response Type');
        }

        $this->_responseType = strtolower($responseType);
        return $this;
    }

    public function getResponseType()
    {
        return $this->_responseType;
    }

	public function sendRequest($requestType, $path) {
        $requestType = ucfirst(strtolower($requestType));

		$this->logDebug( "Path: " . $path . "\n" );
		$this->logDebug( "Params: " . http_build_query($this->getParams() ) . "\n" );
		
        try {
            $requestMethod = 'rest' . $requestType;
            $response = $this->{$requestMethod}($path, $this->getParams());
            return $this->formatResponse($response);
        } catch (Zend_Http_Client_Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
	
	public function formatResponse(Zend_Http_Response $response) {
		$this->logDebug("Response type:" . $this->getResponseType() );
		
		try {
	        if ('json' === $this->getResponseType()) {
	            return $response->getBody();
	        } elseif ('php' === $this->getResponseType()) {
	            return unserialize($response->getBody());
	        } else {
	            return new Zend_Rest_Client_Result($response->getBody());
	        }
		} catch ( Exception $err ) {
			//$this->logError($err->getMessage());
			return $response->getBody();
		}
    }
	
	/**
     * Perform a POST or PUT
     *
     * Performs a POST or PUT request. Any data provided is set in the HTTP
     * client. String data is pushed in as raw POST data; array or object data
     * is pushed in as POST parameters.
     *
     * SGG NOTE: This is a kludge to fix a problem with the way Zend handles
     * PUT methods.
     * 
     * @param mixed $method
     * @param mixed $data
     * @return Zend_Http_Response
     */
    protected function _performPost($method, $data = null) {
		
        $client = self::getHttpClient();
        if (is_string($data)) {
            $client->setRawData($data);
        } elseif (is_array($data) || is_object($data)) {
        	// See if we have a body member in the array
			if ( is_array($data)) {
				if ( !is_null($data["_body"]) ) {
					$client->setRawData($data["_body"]);
					// Remove it from the array
					unset($data["_body"]); 
					// Set the content type
					$client->setHeaders("Content-Type", "application/xml");
					$client->setParameterGet((array) $data);
				}
			} else {
            	$client->setParameterPost((array) $data);
			}
        }
        return $client->request($method);
    }
	
	public function getAuthId() { return $this->authId; }
	public function getApiKey() { return $this->apiKey; } 
	
	public function setLogLevel( $logLevel ) {
		$this->_logLevel = $logLevel;
	}
	
	public function logDebug( $message ) {
		if ( $this->_logLevel >= 2 ) {
			error_log( "[DEBUG] " . $message );
		}	
	}
	
	public function logWarning( $message ) {
		if ( $this->_logLevel >= 1 ) {
			error_log( "[WARN] " . $message );
		}
	}
	
	public function logError( $message ) {
		error_log( "[ERROR] " . $message );
	}
	
	public function logInfo( $message ) {
		error_log( "[INFO] " . $message );
	}
}
?>