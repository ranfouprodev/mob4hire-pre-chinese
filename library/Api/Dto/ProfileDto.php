<?php 
class Api_ProfileDto {
	
	/**
	 * The database ID of this profile
	 * @var integer
	 */
    public $id;
    
	/**
	 * The user ID of the user that owns this profile
	 * @var integer
	 */
    public $userId;
    
	/**
	 * The user's first name
	 * @var string
	 */
    public $firstName;
    
	/**
	 * The user's last name
	 * @var string
	 */
    public $lastName;
    
	/**
	 * The user's display name
	 * @var string
	 */
    public $displayName;
    
	/**
	 * The URL to an image to use on public profiles
	 * @var string
	 */
    public $displayImage;
    
	/**
	 * Notification types for messages
	 * @var integer
	 */
    public $notificationType;
    
	/**
	 * The user's country
	 * @var string
	 */
    public $country;
    
	/**
	 * The user's zipcode 
	 * @var string
	 */
    public $zipCode;
    
	/**
	 * A string containing the user's area of interest
	 * @var string
	 */
    public $areaofInterest;
    
	/**
	 * The user's telephone number
	 * @var string
	 */
    public $telephone;
    
	/**
	 * The user's resume
	 * @var string
	 */
    public $resume;
    
	/**
	 * A URL to the user's homepage
	 * @var string
	 */
    public $homepage;
    
	/**
	 * The date that this profile was created (in seconds since the Jan 1, 1970)
	 * @var long
	 */
	public $created;
	
	/**
	 * The date that this profile was last modified (in seconds since Jan 1, 1970
	 * @var long
	 */
	public $modified;
	
	/**
	 * The type of profile that this represents.  Currently this can be 0 for tester
	 * and 1 for developer
	 * @var integer
	 */
	public $profileType;
		
    public function __construct() {
    }
}
?>
