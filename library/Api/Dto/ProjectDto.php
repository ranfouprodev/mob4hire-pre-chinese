<?php


class Api_ProjectDto {
	
	/**
	 * The database ID of the project
	 * @var integer
	 */
	public $id;

	/**
	 * The ID of the product that this project belongs to
	 * @var integer
	 */
	public $productId;
		
	/**
	 * The name of the project
	 * @var string
	 */
	public $name;
	
	/**
	 * A description of the project
	 * @var string
	 */
	public $description;
	
	/**
	 * A DeveloperDto object containing the profile of the developer that
	 * owns this project
	 * @var DeveloperDto
	 */
	public $owner;
	
	/**
	 * The date that bidding closes for this project
	 * @var date
	 */
	public $bidCloseDate; 

	/**
	 * The number of days after bidding closes to complete the project
	 * @var integer
	 */
	public $daysToComplete;
		
	/**
	 * The type of application.
	 * @var string
	 */
	public $appType;
	
	/**
	 * Requirements to complete this project
	 * @var string
	 */	
	public $requirements;

	/**
	 * An array of requirements to be eligible for this project.  This may
	 * include things like Handset Types, Carriers, Geographic Location etc..
	 * @var array of Strings
	 */
	public $requirementsList;	

	/**
	 * The type of project.  Types are either "Open" or "Closed"
	 * @var string
	 */
	public $type;

	/**
	 * The current status of the project
	 * @var integer
	 */
	public $status;		

	/**
	 * The type of budget. ie. Small, Medium or Large
	 * @var string
	 */
	public $budget;
		
	public function getStatusText() {
		switch ( $this->status ) {
			case 0:
				return "Created";
			case 1:
				return "Waiting for Bids";
			case 2:
				return "In Progress";
			case 3:
				return "Complete";
		} 
	}
	
	public function getTimeRemainingToBid() {
		
		$timestamp = $this->bidCloseDate;
		$granularity = 3;
		
        $seconds = $timestamp - time();
		if ( $seconds < 0 ) {
			return "NONE";
		}
        $units = array(
            '1 year|:count years' => 31536000,
            '1 week|:count weeks' => 604800,
            '1 day|:count days' => 86400,
            '1 hour|:count hours' => 3600,
            '1 min|:count min' => 60,
            '1 sec|:count sec' => 1);
        $output = '';
        foreach ($units as $key => $value) {
            $key = explode('|', $key);
            if ($seconds >= $value) {
                $count = floor($seconds / $value);
                $output .= ($output ? ' ' : '');
                $output .= ($count == 1) ? $key[0] : str_replace(':count', $count, $key[1]);
                $seconds %= $value;
                $granularity--;
            }
            if ($granularity == 0) {
                break;
            }
        }

        return $output ? $output : '0 sec';
    }
}
?>