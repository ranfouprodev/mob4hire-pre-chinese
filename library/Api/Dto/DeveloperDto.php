<?php


class Api_DeveloperDto extends Api_ProfileDto {
	
	/**
	 * The name of the Developer's company
	 * @var string
	 */
	public $company;
	
	/**
	 * The position this user holds in the user's company
	 * @var string
	 */
	public $position;
	
	/**
	 * The platform that the user's company primarily develops for
	 * @var string
	 */
	public $platform;
	
	/**
	 * The number of employees at the user's company
	 * @var integer
	 */
	public $employeeCount;
	
	/**
	 * The type of software created at the user's company
	 * @var string
	 */
	public $softwareType;
}
?>