<?php


class Api_TesterDto extends Api_ProfileDto {
	
	/**
	 * @var string
	 */
	public $softwareBackground;
	
	/**
	 * @var string
	 */
	public $currentOccupation;
	
	/**
	 * @var string
	 */
	public $educationLevel;
	
	/**
	 * @var string
	 */
	public $notes;
	
	/**
	 * A flag indicating whether or not the profile information can be shown
	 * to other users.
	 * @var boolean
	 */
	public $public;
	
	/**
	 * A flag indicating the type of notifications to receive when new
	 * tests are available that match this user's profile
	 * @var integer
	 */
	public $testNotifications;
	
	/**
	 * A flag indicating the type of notifications to receive when another
	 * user sends a message
	 * @var integer
	 */
	public $messageNotifications;
}
?>