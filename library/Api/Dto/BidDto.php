<?php


class Api_BidDto {
	
		
	/**
	 * The database ID of this bid.
	 * @var integer
	 */
	public $id;
	
	/**
	 * The ID of the user that made this bid
	 * @var integer
	 */
	public $userId;
	
	/**
	 * The ID of the project that this bid was made on
	 * @var integer
	 */
	public $projectId;
	
	/**
	 * The amount of the bid
	 * @var float
	 */
	public $amount;
	
	/**
	 * The status of the bid
	 * @var integer
	 */
	public $status;
	
	/**
	 * The date that the bid was created
	 * @var date
	 */
	public $created;
	
}
?>