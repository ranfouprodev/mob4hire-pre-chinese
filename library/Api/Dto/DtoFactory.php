<?php

require_once "ProjectDto.php";
require_once "UserDto.php";
require_once "ProfileDto.php";
require_once "DeveloperDto.php";
require_once "TesterDto.php";

class Api_DtoFactory {
	
	const TESTER_DTO = "TesterDto";
	const DEVELOPER_DTO = "DeveloperDto";
	const USER_DTO = "UserDto";
	const PROJECT_DTO = "ProjectDto";
	
	private static $_instance;
	
	public static function getInstance(){

     	if(!isset(self::$_instance)){
         	$object= __CLASS__;
         	self::$_instance=new $object;
     	}
     	return self::$_instance;
   	}
	
	private function __construct() {
	}
	
	public function createDtoFromXml( $clazz, $xml ) {
		
		if ( $clazz == Api_DtoFactory::TESTER_DTO ) {
			return $this->parseTester( $xml );
		} else if ( $clazz == Api_DtoFactory::DEVELOPER_DTO ) {
			return $this->parseDeveloper( $xml );
		} else if ( $clazz == Api_DtoFactory::USER_DTO ) {
			return $this->parseUser( $xml );
		} else if ( $clazz == Api_DtoFactory::PROJECT_DTO ) {
			return $this->parseProject( $xml );
		} else {
			print "No parser!!";
			throw new Exception("No parser for class: " . $clazz . " was found!" );
		}
	}
	
	public function createXmlFromDto( $clazz, $dto ) {
		if ( $clazz == Api_DtoFactory::TESTER_DTO ) {
			return $this->createTesterXml( $dto );
		} else if ( $clazz == Api_DtoFactory::DEVELOPER_DTO ) {
			return $this->createDeveloperXml( $dto );
		} else if ( $clazz == Api_DtoFactory::USER_DTO ) {
			return $this->createUserXml( $dto );
		} else if ( $clazz == Api_DtoFactory::PROJECT_DTO ) {
			return $this->createProjectXml( $dto );
		} else {
			throw new Exception("No parser for class: " . $clazz . " was found!" );
		}		
	}
	
	private function parseProfile( $dto, $xml ) {
		
		$dto->id = $xml->id;
		$dto->userId = $xml->userId;
		$dto->firstName = $xml->firstName;
		$dto->lastName = $xml->lastName;
		$dto->displayName = $xml->displayName;
		$dto->displayImage = $xml->displayImage;
		$dto->notificationType = $xml->notificationType;
		$dto->country = $xml->country;
		$dto->zipCode = $xml->zipCode;
		$dto->areaofInterest = $xml->areaofInterest;
		$dto->telephone = $xml->telephone;
		$dto->resume = $xml->resume;
		$dto->homepage = $xml->homepage;
		$dto->created = (int)$xml->created;
		$dto->modified = (int)$xml->modified;
		$dto->profileType = $xml->profileType;
	}
	
	private function parseTester( $xml ) {
		$tester = new Api_TesterDto();
		$this->parseProfile($tester, $xml );
		$tester->softwareBackground = $xml->softwareBackground;
		$tester->currentOccupation = $xml->currentOccupation;
		$tester->educationLevel = $xml->exucationLevel;
		$tester->notes = $xml->notes;
		//$tester->public $xml->public;
		$tester->testNotifications = (int)$xml->testNotifications;
		$tester->messageNotifications = (int)$xml->messageNotifications;
		
		return $tester;
	}
	
	private function createTesterXml( $testerDto ) {

	}
	
	private function parseDeveloper( $xml ) {
		$developer = new Api_TesterDto();
		$this->parseProfile($developer, $xml );
		
		$developer->company = $xml->company;
		$developer->position = $xml->position;
		$developer->platform = $xml->platform;
		$developer->employeeCount = (int)$xml->employeeCount;
		$developer->softwareType = $xml->softwareType;
		
		return $developer;		
	}
	
	private function createDeveloperXml( $developerDto, $addHeader = TRUE ) {
		
	}
	
	private function parseUser( $xml ) {
		$user = new Api_UserDto();
		$user->id = intval( $xml->id );
		$user->username = $xml->username;
		$user->password = $xml->password;
		$user->email = $xml->email;
		$user->emailValidated = $xml->emailValidated;
		$user->emailValidationString = (boolean)$xml->emailValidationString;
		return $user;		
	}
	
	private function createUserXml( $userDto ) {
		$xml = "";
		if ( $addHeader ) {
			$xml .= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		}
		$xml .= "<user><id>" . $user->id . "</id>";
		$xml .= "<username>" . $user->username . "</username>";
		$xml .= "<password>" . $user->password . "</password>";
		$xml .= "<email>" . $user->email . "</email>";
		$xml .= "<emailValidated>" . ($user->emailValidated?"true":"false") . "</emailValidated>";
		$xml .= "<emailValidationString>" . $user->emailValidationString . "</emailValidationString></user>";
		return $xml; 		
	}
	
	private function parseProject( $xml ) {
		$project = new Api_ProjectDto();
		$project->id = (int)$xml->id;
		$project->productId = (int)$xml->productId;
		$project->name = $xml->name;
		$project->description = $xml->description;
		$project->owner = $this->parseDeveloper( $xml->owner );
		$project->bidCloseDate = (int)$xml->bidCloseDate;
		$project->daysToComplete = (int)$xml->daysToComplete;
		$project->appType = $xml->appType;
		$project->requirements = $xml->requirements;
		$project->requirementsList = explode(",", $xml->requirementsList );
		$project->type = $xml->type;
		$project->status = $xml->status;
		$project->budget = $xml->budget;
		
		return $project;		
	}
	
	private function createProjectXml( $projectDto, $addHeader = TRUE ) {
		$xml = "";
		if ( $addHeader ) {
			$xml .= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		}
		$xml .= "<project><id>" . $project->id . "</id>";
		$xml .= "<name>" . $project->name . "</name>";
		$xml .= "<description>" . $project->description . "</description>";
		$xml .= $this->createDeveloperXml( $projectDto->owner, false );
		$xml .= "<bidCloseDate>" . $project->bidCloseDate->getTimestamp() . "</bidCloseDate>";
		$xml .= "<daysToComplete>" . $project->daysToComplete . "</daysToComplete>";
		$xml .= "<appType>" . $project->appType . "</appType>";
		$xml .= "<requirements>" . $project->requirements . "</requirements>";
		$xml .= "<requirementsList>" . implode(",", $project->requirementsList ) . "</requirementsList>";
		$xml .= "<type>" . $project->type . "</type>";
		$xml .= "<status>" . $project->status . "</status>";
		$xml .= "<budget>" . $project->budget . "</budget>";
		$xml .= "</project>";
		return $xml; 		
	}
}
?>