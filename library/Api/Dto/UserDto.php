<?php

/**
 * A Data Transfer Object for User. This is used by the services tier to push and pull data from
 * the services.
 */
class Api_UserDto {
	
	/**
	 * The database ID of this user.
	 * @var integer
	 */
	public $id;
	
	/**
	 * The User's Username
	 * @var string
	 */
	public $username;
	
	/**
	 * The User's password.  Hashed if it's from the service and un-hashed (clear text) if we're sending
	 * to the service to update.
	 * 
	 * @var string
	 */
	public $password;
	
	/**
	 * The User's email address.
	 * @var string
	 */
	public $email;
	
	/**
	 * A flag indicating whether or not this email address has been validated.
	 * @var boolean
	 */
	public $emailValidated;
	
	/**
	 * The email validation string that we'll use to validate this email address
	 * @var string
	 */
	public $emailValidationString;
	
	public function __construct( ) {
	}
}
?>