<?php


interface Api_BidServiceInterface {
	
	/**
     * This function will return a list of bids.  Use $start and $count for
     * pagination.
     *
     * @param int $project		The project to filter bids on. Default is -1 which does no filtering on project
     * @param int $user			The user to get bids for.  Default is -1 which does no filtering on user
     * @param int $status		The status to filter bids on.  Default is -1 which does no filtering on status
     * @param int $start		The starting item to retrieve (ie. 11)
     * @param int $count		The number of items to retrieve (ie. 10)
     * @param string $sortField	The field to sort the results on. Defaults to "id"
     * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
     *
     * @return  				An array containing UserDto objects
     */
    public function listBids($project = -1, $user = -1, $status = -1, $start = 0, $count = 10, $sortField = "id", $direction = "Asc");
	
	/**
	 * Retrieve a bid by it's ID
	 * @param int $bidId		The ID of the Bid to retrieve
	 * @return 					A BidDto or null if the ID wasn't found.
	 */
	public function getById( $bidId );
	
	/**
	 * Create a new bid.
	 * @param BidDto $bid			The bid to create.  
	 * @return						A BidDto object containing the created bid. (including database ID) 
	 */
	public function createProject( $bidDto );
	
	/**
	 * Delete an existing project
	 * @param int $bidId			The ID of the bid to delete
	 * @return 						A boolean.  False indicates failure.
	 */
	public function deleteBid( $bidId );
}
?>