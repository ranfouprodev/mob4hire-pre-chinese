<?php
require_once("UserService.php");
require_once("TestProjectService.php");
require_once("BidService.php");

class Api_ServiceFactory {
	
	private static $_instance;
	
	private $apiKey;
	private $authId;
	private $host;
	private $port;
	private $environment;
	private $serviceCache;
	
	const PROJECT_SERVICE = "TestProject";
	const USER_SERVICE = "User";
	const APIKEY_SERVICE = "ApiKey";
	const BID_SERVICE = "Bid";
	
	private static $instance;
	
	public static function getInstance(){
     	if(!isset(self::$instance)){
         	$object= __CLASS__;
         	self::$instance=new $object;
     	}
     	return self::$instance;
   	}

	private function __construct() {
		
		$this->environment = "dev"; //getenv("SERVER_ENV");
		if ( $this->environment == "dev" ) {
			$this->host = "http://localhost";
			$this->port = 8080;
			$this->apiKey = "ZWI1NjVlNmIxMDA3MGYzNmY0N2FlNjVmYTM0OGM5ZDg=";
			$this->authId = 1;
		} else {
			throw new Exception("Not sure why it's getting here!! Environment : " . $this->environment );
			$this->host = "http://domU-12-31-39-03-4A-56.compute-1.internal";
			$this->port = 80;
			$this->apiKey = "ZWI1NjVlNmIxMDA3MGYzNmY0N2FlNjVmYTM0OGM5ZDg=";
			$this->authId = 1;
		}
		
	}
	
	public function getService( $serviceName ) {
		$service = NULL;
		if ( isset( $this->serviceCache[$serviceName] ) ) {
			return $this->serviceCache[$serviceName];
		}
		if ( Api_ServiceFactory::PROJECT_SERVICE == $serviceName ) {
			$service = new Api_TestProjectService( $this->host . ":" . $this->port, $this->authId, $this->apiKey );
			if ( $this->environment == "dev" ) {
				$service->OverrideServicePrefix("TestProject");
			}
		} elseif ( Api_ServiceFactory::USER_SERVICE == $serviceName ) {
			$port = $this->port;
			if ( $this->environment == "dev" ) {
				$port = 8085;
			}
			$service = new APi_UserService( $this->host . ":" . $port, $this->authId, $this->apiKey );
		} 

		// Cache the service so we don't have to go create it again
		if ( !is_null($service) ) {
			$this->serviceCache[$serviceName] = $service;
		}
				
		return $service;
	}
	
}
?>