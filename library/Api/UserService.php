<?php 
require_once "ServiceBase.php";
require_once "UserServiceInterface.php";
require_once "Dto/DtoFactory.php";

/**
 * A wrapper for the User Service.
 */
class Api_UserService extends Api_ServiceBase implements Api_UserServiceInterface {

	public function __construct( $url, $authId, $apiKey ) {
		parent::__construct( $url, $authId, $apiKey, "APIAuth" );
	}
	
    /**
     * This function will return a list of users.  Use $start and $count for
     * pagination.
     *
     * @param int $start		The starting item to retrieve (ie. 11)
     * @param int $count		The number of items to retrieve (ie. 10)
     * @param string $sortField	The field to sort the results on. Defaults to "id"
     * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
     *
     * @return  				An array containing UserDto objects
     */
    public function listUsers($start = 0, $count = 10, $sortField = "id", $direction = "Asc") {
    
        $params = array();
        $params["start"] = $start;
        $params["count"] = $count;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        
        $this->setParams($params);
        
        $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/list");
        $xmlIterator = $response->getIterator();
        $result = array();
        $i = 0;
        foreach ($xmlIterator->user as $userXml) {
            $result[$i++] = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $userXml );
        }
        
        return $result;
    }
    
    /**
     * Retrieve a user by their username.
     * @param string $username	The username of the user to retrieve
     * @return 					A UserDto object containing the user or NULL if the user was not found.
     */
    public function getByUsername($username) {
        $params = array();
        $params["username"] = $username;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
        
        try {
            $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/getByUsername");
            $user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        
        }
        
        return $user;
    }
    
    /**
     * Retrieve a user by their email address.
     * @param string $email		The email address of the user to retrieve
     * @return 					A UserDto object containing the user or NULL if the user was not found.
     */
    public function getByEmail($email) {
        $params = array();
        $params["email"] = $email;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
        
        try {
            $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/getByEmail");
 			$user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        
        }
        
        return $user;
    }
    
    /**
     * Retrieve a user by their database id.
     * @param integer $id		The database ID of the user to retrieve
     * @return 					A UserDto object containing the user or NULL if the user was not found.
     */
    public function getById($id) {
        $params = array();
        $params["id"] = $id;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
        
        try {
            $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/getById");
			$user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        
        }
        
        return $user;
    }

    
	/**
	 * Retrieve a list of users specific to the ids passed
	 * @param string $ids		A comma seperated list of database IDs to retrieve
	 * @return 					An array of UserDto objects or null if nothing was found
	 */
    public function getBatchUsers($ids) {
        $params = array();
        $params["ids"] = $ids;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);

        $result = NULL;
        
        try {
            $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/retrieve");
            $result = array();
            $xmlIterator = $response->getIterator();
			
            $i = 0;
            foreach ($xmlIterator->user as $userXml) {
				$user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $userXml );
                $result[$i++] = $user;
            }
        }
        catch(Exception $err) {
        
        }
        
        return $result;
    }
    
	/**
	 * Create a new User object.
	 * @param UserDto 	$userDto The user to create. Leave id empty
	 * @return 			UserDto object containing the created user with the database id
	 */
    public function create($userDto) {
    	
    	if ( !is_object($userDto) ) {
    		return null;
    	}
		$user = null;
		$params = array();
        $params["_body"] = $this->xmlFromUser( $userDto );
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		try {
            $response = $this->sendRequest('PUT', "/APIAuth/api/v1/user/create");
            $user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        	print $err->getMessage() ."\n";
        }
		
		return $user;
    }
    
	/**
	 * Update a user from a UserDto object.
	 * @param UserDto $userDto		The UserDto containing the user data.  The Id must
	 *                              be the database ID of the user to change.
	 * @return 						The updated UserDto representing the user.
	 */
    public function update($userDto) {
        if ( !is_object($userDto) ) {
    		return null;
    	}
		$user = null;
		$params = array();
        $params["_body"] = $this->xmlFromUser( $userDto );
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		try {
            $response = $this->sendRequest('POST', "/APIAuth/api/v1/user/update");
            $user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        	print $err->getMessage() ."\n";
        }
		
		return $user;
    }
    
	/**
	 * Delete a User by it's database id.
	 * @param integer $id			The database ID of the user to change.
	 * @return 						Boolean true or false.
	 */
    public function delete($id) {
    	
		// For this to work properly with the Zend client we have to encode the
		// parameters directly into the path. (I think).
		
    	$params = array();
        $params["id"] = $id;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
		$response = NULL;
        try {
            $this->sendRequest('GET', "/APIAuth/api/v1/user/delete");
        }
        catch(Exception $err) {
        	print "\nException: " . $err->getMessage() . "\n";
			var_dump($response);
        }
    }
    
	/**
	 * Authenticate a user by username and cleartext password.  
	 * @param object $username			The username of the user
	 * @param object $password			The cleartext password of the user
	 * @return 							A UserDto for the user or null if either the
	 *                                  the username didn't match a user or the password
	 *                                  wasn't valid.
	 */
    public function authenticate($username, $password) {
        $params = array();
        $params["username"] = $username;
		$params["password"] = $password;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
        
        try {
            $response = $this->sendRequest('GET', "/APIAuth/api/v1/user/authenticate");
			$user = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::USER_DTO, $response );
        }
        catch(Exception $err) {
        
        }
        
        return $user;
    }
}
?>
