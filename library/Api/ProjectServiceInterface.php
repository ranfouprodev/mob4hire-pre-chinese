<?php


interface Api_ProjectServiceInterface {
	
	/**
	 * This function will return a list of projects for the specified product and
	 * optionally with the specified status.
	 * 
	 * @param int $product		The product to list projects for. Defaults to 1
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjects( $product=1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" );

	/**
	 * This function will return a list of projects for the specified owner and
	 * optionally with the specified status.
	 * 
	 * @param int $ownerId		The owner to get projects for.
	 * @param int $product		The product to list projects for. Defaults to 1
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjectsByOwner( $ownerId, $productId = 1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" );
	
	/**
	 * This function will return a list of projects that have the specified user as a member.
	 * 
	 * @param int $memberId		The user id of the member.
	 * @param int $product		The product to list projects for. Defaults to 1	 
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjectsWithMember( $memberId, $productId = 1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" );
	
	
	/**
	 * Retrieve a project by it's ID
	 * @param int $projectId	The project id 
	 * @return 					A ProjectDto or null if the ID wasn't found.
	 */
	public function getById( $projectId );
	
	/**
	 * Retrieve a list of Users that belong to a project
	 * @param int $projectId	The ID of the project to get users from
	 * @return 					An array of UserDto objects that represent the members of
	 *                          a project.
	 */
	public function getUsers( $projectId );
	
	/**
	 * Create a new project.
	 * @param ProjectDto $project	The project to create.  
	 * @return						A ProjectDto object containing the created project. (including database ID) 
	 */
	public function createProject( $project );
	
	/**
	 * Update an existing project
	 * @param ProjectDto $project	A ProjectDto object (with valid id) that contains the changes to update with.
	 * @return 						A ProjectDto object containing the updated values.
	 */
	public function updateProject( $project );
	
	/**
	 * Delete an existing project
	 * @param int $projectId		The database ID of the project to delete
	 * @return 						A boolean.  False indicates failure.
	 */
	public function deleteProject( $projectId );
	
	/**
	 * Get a property value from a project.
	 * @param object $projectId		The ID of the project to get the property from
	 * @param object $propertyName	The name of the property to retrieve.
	 * @return 						A string containing the property value.
	 */
	public function getProjectProperty( $projectId, $propertyName );
	
	/**
	 * Set a property value for a project
	 * @param int $projectId		The ID of the project to set the property on
	 * @param string $propertyName	The name of the property to set.
	 * @param string $propertyValue	The value to set the property to.
	 * @return 						Nothing.
	 */
	public function setProjectProperty( $projectId, $propertyName, $propertyValue );
	
	/**
	 * Add a user to a project
	 * @param int $projectId		The ID of the project to add the user to
	 * @param UserDto $user			A UserDto object representing the user to add to the project
	 * @return 						boolean. False indicates failure
	 */
	public function addUser( $projectId, $user );
	
	/**
	 * Remove a user from a project	The ID of the project to remove the user from
	 * @param int $projectId		The ID of the project to remove the user from
	 * @param object $user			A UserDto object representing the user to remove from the project
	 * @return 						boolean. False indicates failure.
	 */
	public function removeUser( $projectId, $user );
}
?>