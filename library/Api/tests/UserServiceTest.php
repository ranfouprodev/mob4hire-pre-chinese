<?php
require_once "PHPUnit/Framework.php";
require_once "../UserService.php";
require_once "../Dto/UserDto.php";

class UserServiceTest extends PHPUnit_Framework_TestCase {
	
	private $apiKey = "ZWI1NjVlNmIxMDA3MGYzNmY0N2FlNjVmYTM0OGM5ZDg=";

	public function testList() {
		
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		
		$users = $userService->listUsers( 0, 100, "username", "asc" );
		if ( count( $users ) < 1 ) {
			$this->fail( "Was expecting users!" );
		}
	}

	public function testGetUserByUsername() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		
		$user = $userService->getByUsername( "mob4hire" );
		if ( is_null($user)) {
			$this->fail("Was expecting a user back!");
		}
		$user = $userService->getByUsername( "113BobWasHere229");
		if ( !is_null($user)) {
			$this->fail("Was not expecting a user back!");
		}
	}
	
	public function testGetUserByEmail() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		
		$user = $userService->getByEmail( "admin@mob4hire.com" );
		if ( is_null($user)) {
			$this->fail("Was expecting a user back!");
		}
		$user = $userService->getByEmail( "113BobWasHere229@mob4hire.com");
		if ( !is_null($user)) {
			$this->fail("Was not expecting a user back!");
		}
	}


	public function testGetUserById() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		
		$user = $userService->getById( 1 );
		if ( is_null($user)) {
			$this->fail("Was expecting a user back!");
		}
		$user = $userService->getById( 1102202 );
		if ( !is_null($user)) {
			$this->fail("Was not expecting a user back!");
		}
	}
	
	/*		
	public function testCreateUser() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		$user = new Api_UserDto();
		$user->username="APHP Test 1234";
		$user->password="password";
		$user->email="me@localhost.com";
		$user->emailValidated = false;
		
		$user = $userService->create( $user );
		if ( is_null($user) ) {
			$this->fail("Was expecting a user back from the create method!");
		}
	}
	
	public function testUpdateUser() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );

		$user = $userService->getByUsername("APHP Test 1234");
		if ( is_null($user)) {
			$this->fail("Failed to lookup user created in previous test");
		}
		$user->emailValidationString="Test";
		$user = $userService->update( $user );
		if ( is_null($user) ) {
			$this->fail("Was expecting a user back from the update method!");
		}
	}	
	*/
	
	public function testGetBatchUsers() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );
		
		$users = $userService->getBatchUsers( "1,2" );
		if ( is_null($users)) {
			$this->fail("Was expecting a user back " );
		} else {
			if ( count($users)!=2 ) {
				$this->fail("Was expecting 2 users but got " . count($users) );
			}
		}
	}
	
	/*
	public function testDeleteUser() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );

		$user = $userService->getByUsername("APHP Test 1234");
		if ( is_null($user)) {
			$this->fail("Failed to lookup user created in previous test");
		}
		$userService->delete( $user->id );

	}		
	*/
	public function testAuthenticateUser() {
		$userService = new Api_UserService( "http://localhost:8085", 1, $this->apiKey );

		$user = $userService->authenticate("mob4hire", "password");
		if ( is_null($user)) {
			$this->fail("Failed to authenticate user");
		}
		$user = $userService->authenticate( "mob4hire", "pass" );
		if ( !is_null($user) ) {
			$this->fail("Authenticated with invalid password!!!");
		}
	}
	
}
?>