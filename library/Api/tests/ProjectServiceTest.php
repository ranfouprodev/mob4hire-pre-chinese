<?php
require_once "PHPUnit/Framework.php";
require_once "../ServiceFactory.php";
require_once "../Dto/ProjectDto.php";
require_once "../Dto/UserDto.php";

class ProjectServiceTest extends PHPUnit_Framework_TestCase {
	
	public function testListProjects() {
		
		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );
		$projects = $projectService->listProjects( 1, -1, 0, 100, "name", "asc" );
		if ( count( $projects  ) < 1 ) {
			$this->fail( "Was expecting projects!" );
		}
	}

	public function testListProjectsByOwner() {

		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );
		$projects = $projectService->listProjectsByOwner( 2 );
		if ( count( $projects  ) < 1 ) {
			$this->fail( "Was expecting projects but got " . count( $projects ) );
		}
	}

	public function testListProjectsByMember() {

		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );
		$projects = $projectService->listProjectsWithMember( 2 );
		if ( count( $projects  ) != 1 ) {
			$this->fail( "Was expecting a single project!" );
		}

		$projects = $projectService->listProjectsWithMember( 1 );
		if ( count( $projects  ) != 2 ) {
			$this->fail( "Was expecting two projects!" );
		}
	}

	public function testGetProjectById() {

		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );

		$project = $projectService->getById( 1 );
		if ( is_null( $project ) ) {
			$this->fail( "Was expecting a project but got nothing!" );
		}
	}

	public function testGetProjectUsers() {
		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );

		$users = $projectService->getProjectUsers( 2 );
		if ( count( $users ) < 1 ) {
			$this->fail( "Was expecting an array of users" );
		}
		if ( !($users[0] instanceof Api_TesterDto ) ) {
			var_dump($users[0]);
			$this->fail("Was expecting to see an array of userDto objects" );
		}
		
	}

	public function testGetProperty() {

		$projectService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::PROJECT_SERVICE );
		$value = $projectService->getProjectProperty(1, "min_bid");
		if ( is_null($value)) {
			$this->fail("Was expecting a value for property 'min_bid' in project 1" );
		}		
	}
}
?>