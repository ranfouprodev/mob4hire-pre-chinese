<?php
require_once "PHPUnit/Framework.php";
require_once "../ServiceFactory.php";
require_once "../Dto/BidDto.php";

class BidServiceTest extends PHPUnit_Framework_TestCase {

	public function testListBids() {
		
		$bidService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::$BID_SERVICE );
		$bids = $bidService->listBids();
		if ( count( $bids  ) < 1 ) {
			$this->fail( "Was expecting bids!" );
		}
	}	
	
	public function testBidCount() {
		$bidService = Api_ServiceFactory::getInstance()->getService( Api_ServiceFactory::$BID_SERVICE );
		$count = $bidService->countBids();
		if ( $count != 3 ) {
			$this->fail("Was expecting a count of 3 and got " . $count );
		}
		$count = $bidService->countBids(1);
		if ( $count != 2 ) {
			$this->fail("Was expecting to see 2 bids for project 1 and got " . $count );
		}
	}
}
?>