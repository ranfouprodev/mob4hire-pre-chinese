<?php
require_once "ServiceBase.php";
require_once "ProjectServiceInterface.php";
require_once "Dto/DtoFactory.php";

class Api_TestProjectService extends Api_ServiceBase {
	
	public function __construct( $url, $authId, $apiKey ) {
		parent::__construct( $url, $authId, $apiKey, "testprojects" );
	}
	
	/**
	 * This function will return a list of projects for the specified product and
	 * optionally with the specified status.
	 * 
	 * @param int $product		The product to list projects for. Defaults to 1
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjects( $product=1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" ) {
		
		// See if the order direction was included in the sortField
		if ( $pos = strpos( strtolower($sortField)," desc" ) ) {
			$direction = substr($sortField, $pos + 1);
			$sortField = substr($sortField, 0, $pos );
		} else if ( $pos = strpos( strtolower($sortField), " asc" ) ) {
			$direction = substr($sortField, $pos + 1);
			$sortField = substr($sortField, 0, $pos );
		}
		$params = array();
		$params["product"] = $product;
		$params["status"] = $status;
        $params["start"] = $start;
        $params["count"] = $count;
		$params["sort"] = $sortField;
		$params["direction"] = $direction;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        
        $this->setParams($params);
        $result = array();
		$response = NULL;
		try {
	        $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/list");
			if ( $response->getStatus() != 200 ) {
				var_dump($response);
			}
	        $xmlIterator = $response->getIterator();
	        $i = 0;
			$owners = array();
	        foreach ($xmlIterator->project as $projectXml) {
	        	$result[$i++] = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $projectXml );
	        }
		
		} catch ( Exception $err ) {
			$this->logError($err->getMessage());
		}
		
        return $result;
	}

	/**
	 * This function will return a list of projects for the specified owner and
	 * optionally with the specified status.
	 * 
	 * @param int $ownerId		The owner to get projects for.
	 * @param int $product		The product to list projects for. Defaults to 1
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjectsByOwner( $ownerId, $productId = 1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" ) {
		$params = array();
		$params["product"] = $productId;
		$params["status"] = $status;
        $params["start"] = $start;
        $params["count"] = $count;
		$params["sort"] = $sortField;
		$params["direction"] = $direction;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        
        $this->setParams($params);
        
        $result = array();
		$response = NULL;
		
		try {
	        $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/developer/" . $ownerId . "/list");
			$xmlIterator = $response->getIterator();
	        $i = 0;
	        foreach ($xmlIterator->project as $projectXml) {
	            $result[$i++] = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $projectXml );
	        }
		} catch ( Exception $err ) {
			$this->logError($err->getMessage());
		}
        
        return $result;
	}
	
	/**
	 * This function will return a list of projects that have the specified user as a member.
	 * 
	 * @param int $memberId		The user id of the member.
	 * @param int $product		The product to list projects for. Defaults to 1	 
	 * @param int $status		The status to filter projects on.  Defaults to -1 which does no filtering.
	 * @param int $start		The starting item to retrieve (ie. 11)
	 * @param int $count		The number of items to retrieve (ie. 10)
	 * @param string $sortField	The field to sort the results on. Defaults to "id"
	 * @param string $direction The direction of the sort. Can be either "Asc" or "Desc".  Defaults to "Asc"
	 * 
	 * @return  				An array containing ProjectDto objects
	 */
	public function listProjectsWithMember( $memberId, $productId = 1, $status=-1, $start = 0, $count = 10, $sortField = "id", $direction="Asc" ) {
		$params = array();
		$params["product"] = $productId;
		$params["status"] = $status;
        $params["start"] = $start;
        $params["count"] = $count;
		$params["sort"] = $sortField;
		$params["direction"] = $direction;		
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        
        $this->setParams($params);
        $response = null;
		$result = array();
				
		try {
	        $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/tester/" . $memberId . "/list");
	        $xmlIterator = $response->getIterator();
	        $result = array();
	        $i = 0;
	        foreach ($xmlIterator->project as $projectXml) {
	            $result[$i++] = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $projectXml );
	        }
		} catch ( Exception $err ) {
			$this->logError($err->getMessage());
		}
        return $result;
	}
	
	
	/**
	 * Retrieve a project by it's ID
	 * @param int $projectId	The project id 
	 * @return 					A ProjectDto or null if the ID wasn't found.
	 */
	public function getById( $projectId ) {
		$params = array();
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/" . $projectId );
			$user =  Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $response->getIterator() );
	    } catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return $user;
	}
	
	/**
	 * Retrieve a list of Users that belong to a project
	 * @param int $projectId	The ID of the project to get users from
	 * @return 					An array of UserDto objects that represent the members of
	 *                          a project.
	 */
	public function getProjectUsers( $projectId, $start=0, $count=10, $sort="id", $direction="asc" ) {
		
		$params = array();
		$params["start"] = $start;
		$params["count"] = $count;
		$params["sort"] = $sort;
		$params["direction"] = $direction;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);

		$result = array();		
		$response = NULL;
		try {
			$response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/" . $projectId . "/tester/list");
	        $xmlIterator = $response->getIterator();
	        $ids = array();
			$i = 0;
	        foreach ($xmlIterator->tester as $tester ) {
	        	$result[$i++] = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::TESTER_DTO, $tester );
			}
	
		} catch ( Exception $err ) {
			$this->logError( $err->getMessage() );
		}
		
		return $result;
	}
	
	/**
	 * Create a new project.
	 * @param ProjectDto $project	The project to create.  
	 * @return						A ProjectDto object containing the created project. (including database ID) 
	 */
	public function createProject( $projectDto ) {
		if ( !is_object($projectDto) || !($projectDto instanceof Api_ProjectDto ) ) {
    		throw new Exception("project must not be null and must be an instance of Api_ProjectDto" );
    	}
		$project = null;
		$params = array();
        $params["_body"] = $this->xmlFromProject( $projectDto );
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		try {
            $response = $this->sendRequest('PUT', "api/v1/create");
            $project = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $response->getIterator() ); 
        }
        catch(Exception $err) {
        	print $err->getMessage() ."\n";
        }
		
		return $project;
	}
	
	/**
	 * Update an existing project
	 * @param ProjectDto $project	A ProjectDto object (with valid id) that contains the changes to update with.
	 * @return 						A ProjectDto object containing the updated values.
	 */
	public function updateProject( $project ) {
		if ( !is_object($projectDto) || !($projectDto instanceof Api_ProjectDto ) ) {
    		throw new Exception("project must not be null and must be an instance of Api_ProjectDto" );
    	}
		$project = null;
		$params = array();
        $params["_body"] = $this->xmlFromProject( $projectDto );
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		try {
            $response = $this->sendRequest('POST', "/api/v1/update");
            $project = Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::PROJECT_DTO, $response->getIterator() ); 
        }
        catch(Exception $err) {
        	print $err->getMessage() ."\n";
        }
		
		return $user;
	}
	
	/**
	 * Delete an existing project
	 * @param int $projectId		The database ID of the project to delete
	 * @return 						A boolean.  False indicates failure.
	 */
	public function deleteProject( $projectId ) {
		$params = array();
        $params["id"] = $projectId;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
        
        $user = NULL;
		$response = NULL;
        try {
            $response = $this->sendRequest('GET', "/api/v1/delete");
			if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
				return FASE;
			}
			return TRUE;
        }
        catch(Exception $err) {
        	print "\nException: " . $err->getMessage() . "\n";
        }
		
		return FALSE;
	}
	
	/**
	 * Get a property value from a project.
	 * @param object $projectId		The ID of the project to get the property from
	 * @param object $propertyName	The name of the property to retrieve.
	 * @return 						A string containing the property value or NULL if the property wasn't found.
	 */
	public function getProjectProperty( $projectId, $propertyName ) {
		$params = array();
        $params["id"] = $projectId;
		$params["name"] = $propertyName;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/getById/getProperty");
			return $response->getIterator()->value;
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return NULL;
	}
	
	/**
	 * Set a property value for a project
	 * @param int $projectId		The ID of the project to set the property on
	 * @param string $propertyName	The name of the property to set.
	 * @param string $propertyValue	The value to set the property to.
	 * @return 						Nothing.
	 */
	public function setProjectProperty( $projectId, $propertyName, $propertyValue ) {
		$params = array();
        $params["id"] = $projectId;
		$params["name"] = $propertyName;
		$parans["value"] = $propertyName;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/getById/setProperty");
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
	}
	
	/**
	 * Add a user to a project
	 * @param int $projectId		The ID of the project to add the user to
	 * @param UserDto $user			A UserDto object representing the user to add to the project or a User ID of
	 * 								the user to add.
	 * @return 						boolean. False indicates failure
	 */
	public function addUser( $projectId, $user ) {
		$params = array();
        $params["project"] = $projectId;
		if ( is_object($user) ) {
			$params["userid"] = $user->id;
		} else {
			$params["userid"] = $user;
		}
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
		$response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/getProjectUsers/add");
			if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
				return FALSE;
			}
			return TRUE;
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
		
		return FALSE;
	}
	
	/**
	 * Remove a user from a project	The ID of the project to remove the user from
	 * @param int $projectId		The ID of the project to remove the user from
	 * @param object $user			A UserDto object representing the user to remove from the project
	 * @return 						boolean. False indicates failure.
	 */
	public function removeUser( $projectId, $user ) {
		$params = array();
        $params["project"] = $projectId;
		if ( is_object($user) ) {
			$params["userid"] = $user->id;
		} else {
			$params["userid"] = $user;
		}
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
		$response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/getProjectUsers/remove");
			if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
				return FALSE;
			}
			return TRUE;
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
		
		return FALSE;		
	}
	
		/**
	 * Retrieve a count of all products (optionally filtered by a product)
	 * @param int $product [optional].  Defaults to -1 which means no filtering.
	 * @return The count of projects
	 */
	public function countProjects($product = -1 ) {
		$params = array();
        $params["product"] = $product;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);
		
        $user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/project/count");
			return (int)$response->getIterator()->result;
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return 0;
	}
	
	public function getTesterProfile( $userId, $productId ) {
		$params = array();
        $params["product"] = $productId;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);		
		
		$user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/profile/tester/user/" . $userId );
			if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
				return NULL;
			}
			return Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::TESTER_DTO, $response->getIterator() ); 
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return NULL;
	}
	
	public function getDeveloperProfile( $productId, $userId ) {
		$params = array();
        $params["product"] = $productId;
        $params["authid"] = $this->getAuthId();
        $params["apikey"] = $this->getApiKey();
        $this->setParams($params);		
		
		$user = NULL;
        $response = NULL;
        try {
            $response = $this->sendRequest('GET', "/" . $this->getServicePrefix() . "/api/v1/profile/developer/user/" . $userId );
			if ( is_object($response) ) {
				if ( $response->getStatus() == 404 || $response->getStatus() == 500 || $response->getStatus() == 403 ) {
					return NULL;
				}
				return Api_DtoFactory::getInstance()->createDtoFromXml( Api_DtoFactory::DEVELOPER_DTO, $response->getIterator() );
			} else {
				return NULL;
			}
        }
        catch(Exception $err) {
        	$this->logError($err->getMessage());
        }
        
        return NULL;		
	}
	
	public function getProfile( $productId, $userId ) {
		
	}
}
?>