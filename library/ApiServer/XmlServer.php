<?php
/*
 * For handling different versions. Routing is handled at the bootstrap? 
 */
class ApiServer_XmlServer extends ApiServer_XmlServerBase {

    public function getVersion($apikey) {
        $this->authenticate($apikey);

        return ApiServer_XmlResponse::Generate( array ('version'=>'1.0'));
    }
}

?>
