<?php

class ApiServer_XmlServerUser extends ApiServer_XmlServer {

	/*
	 * Test Code Only. Having an open mailer is at best problematic. 
	 */
	public function email($to, $subject, $message,  $apikey ){
		
		$this->authenticate($apikey);
		
		$mailConfig = Zend_Registry::get('configuration')->mail;
		
		if ($mailConfig->amazon)
		{
			$transport = new Zend_Mail_Transport_AmazonSes($mailConfig->amazonconfig->toArray());
		}
		elseif ($mailConfig->smtp)
		{
			$transport = new Zend_Mail_Transport_Smtp($mailConfig->host, $mailConfig->smtpconfig->toArray());
		}
		else
		{
			$transport = new Zend_Mail_Transport_Sendmail();
		}
		
		$to = 'telesigntest1@mob4hire.com';
		
		Zend_Mail::setDefaultTransport($transport);
		
		try{
			$mailer = new Zend_Mail('UTF-8');
			$mailer->addTo($to, $to);
			$mailer->setSubject($subject);
			$mailer->setBodyHtml($message);
			$mailer->setBodyText(strip_tags($message));
				
			$fromAddress=='' ? $mailer->setFrom(Zend_Registry::get('configuration')->mail->from) : $mailer->setFrom($fromAddress);
			
			$mailer->send(); 
			
			return ApiServer_XmlResponse::Generate( array ('result'=>'Success'), true);
		
		}
		catch(Exception $e)
		{
			 return ApiServer_XmlResponse::Generate( array ('error'=>'Unable to send message'), false);
		}
		
	}
	
	
	public function login($username, $password, $apikey) {
		$this->authenticate($apikey);
        
		if ($username && $password) {
            $auth = Zend_Auth::getInstance();

            $db = Zend_Registry::get('db');
            $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
            $authAdapter->setIdentity($username)->setCredential(Users::computePasswordHash($password));
            $authResult = $auth->authenticate($authAdapter);
            if ($authResult->isValid()) {
                //valid username and password
                $userInfo = $authAdapter->getResultRowObject();
                if ($userInfo->active) {
                    //save userinfo in session
                    $userInfo->password = '';
                    Zend_Registry::get('defSession')->currentUser = $userInfo;

                    // Write last_login info to db
                    $users = new Users();
                    $now = new Zend_Date();

                    $userData = array ('last_login'=>$now->toString('YYYY-MM-dd HH:mm:ss'));
                    $users->editProfile($userInfo->id, $userData);

                    return ApiServer_XmlResponse::Generate( array ('profile'=>$this->getCurrentProfile()), true);


                } else {
                    return ApiServer_XmlResponse::Generate( array ('error'=>'Not Verified'), false);
                }
            } else {
                return ApiServer_XmlResponse::Generate( array ('error'=>'Invalid Credentials'), false);
            }
        } else
            return ApiServer_XmlResponse::Generate( array ('error'=>'Invalid Credentials'), false);


    }
	
	public function logout($apikey){
		$this->authenticate($apikey);
        
		// get the profile of the user logged in
		$this->getLoggedInUser(); 	
		
		$auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        Zend_Registry::get('defSession')->currentUser = null;
	
        return ApiServer_XmlResponse::Generate( array ('message'=>'none'), true);		
	}
	
	public function view($apikey, $userid){
		$this->authenticate($apikey);
		
		$users = new Users(); 
		
		$user = $users->getUser($userid);
		
		if(!$user){
			return ApiServer_XmlResponse::Generate( array ('error'=>'Unknown User Id'), false);
		}else{
			
			$userXml = array(
			'id' => $user['id'],
			'username' => $user['username'],
			'registered' => $user['registered_on'], 
			'lastlogin' => $user['last_login']			
			);
		}
		
		return ApiServer_XmlResponse::Generate( array ('profile'=>$userXml), true);
	}

	public function register($apikey, $username, $password, $firstname, $lastname, $email, $mobile ){
		$this->authenticate($apikey);
		
		$usersTable = new Users();
		
	      $badRegistration = false;
	       
		    try {
	            $newUserId = $usersTable->add($username, $password, $email);
	        }
	        catch(Zend_Db_Statement_Exception $e) {
	            $message = $e->getMessage();
	            $badRegistration = true;
	
	            if (strpos($message, $email) !== false) {
	                //email already registered
					return ApiServer_XmlResponse::Generate( array ('error'=>'That email is already registered'), false);
	            } else if (strpos($message, $username) !== false) {
	                //username already registered
					return ApiServer_XmlResponse::Generate( array ('error'=>'That username is already registered'), false);
	            } else {
					return ApiServer_XmlResponse::Generate( array ('error'=>'Unable to register at this time'), false);
	            }
	
	        }
	
	
	        if (!$badRegistration) {
	            $profileFields = array ('first_name'=>$firstname, 'last_name'=>$lastname );
	            $usersTable->editProfile($newUserId, $profileFields);
	
	
				$contactInfoTable = new ProfileContactInfo(); 
				$contactInfoTable->add($newUserId);
				
				$contactFields = array ('mobile'=>$mobile );
	            $contactInfoTable->editProfile($newUserId, $contactFields);
	
			    return ApiServer_XmlResponse::Generate( array ('id'=>$newUserId), true);
	
	        }

		return ApiServer_XmlResponse::Generate( array ('error'=>'Unable to register at this time'), false);
		
	}
	
	/*
	 * Only works for logged in users. Authentication check occurs too
	 */
	protected function getCurrentProfile(){
		// get the profile of the user logged in
		$user = $this->getLoggedInUser(); 
		
		// Get the balance
		$accounts= new Accounts();
		$balance = $accounts->getBalance($user->id);
		
		// Get the rating 
		$ratings=new Ratings();
		$row=$ratings->fetchRow("id='$user->username'");
		if ($row && $row->total_votes) {
			$rating=$row->total_value/$row->total_votes; // do not divide by zero!
		}
		else {
			$rating=0;
		}
		
		// show mobPro statu
		$users=new Users();
		$userrow=$users->fetchRow("username='$user->username'");
		$pro='false';
		if($userrow && $userrow->pro) {
			$pro='true';
		}

		//print_r($user);
		$update = new ProfileContactInfo();
		$profile = $update->getProfile( $user->id);
		if(!$profile){
			$update->add($this->view->currentUser->id);
		}
		if(isset($profile['image']))
			$profImg = $profile['image'];
		else
			$profImg = Zend_Registry::get('configuration')->general->url . "/images/image-not-found.gif" ;
				
		
		$userXml = array(
			'id' => $user->id,
			'image' =>$profImg,
			'username' => $user->username,
			'firstname' => $user->first_name,
			'lastname' => $user->last_name,
			'registered' => $user->registered_on, 
			'lastlogin' => $user->last_login,
			'email' => $user->email, 
	//		'messages' => $this->getUnreadMessageCount($user->id),
		  'balance' => $balance,
		  'rating' => $rating,
		  'mobpro' => $pro
			
		);
		
		return $userXml;
		
	}
	

				

}

?>
