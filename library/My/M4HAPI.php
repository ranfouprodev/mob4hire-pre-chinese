<?php
/**
* See the documentation of this api at http://trac.m4hdev.com/
*  To do: make this wrapper generic so it can be applied to any service
*/

class My_M4HAPI
{
		private $baseUrl='http://api.mob4hire.com/'; // Public url
//		private $baseUrl='http://domU-12-31-39-03-4A-56.compute-1.internal/';		   // Private url
		private $authid='1';
		private $apikey='ZWY3ODE3MGUyZWNiN2Q0OWE2M2UwZmUwMzU4YzAwZWU='; // This is not good practice! What about when this is called by the external api.
		private $service;
	
		public function __construct($service, $authid='', $apikey='')
		{
			$this->service=$service;
			if($authid)
				$this->authid=$authid;
			if($apikey)
				$this->apikey=$apikey;
		}
		
		// APIAuth methods
		
		public function getUser($id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/user/'.$id.'/', array('authid'=>$this->authid,'apikey'=>$this->apikey));
			$response=$curl->exec();
			if (!$response)
				return false;
			else
				return $response;
		}
		
		public function createUser($username, $password, $email, $activationCode)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/user/' , array('authid'=>$this->authid,'apikey'=>$this->apikey), $this->formatUserXMLRequest($username,$password,$email, $activationCode),'PUT');
			$response=$curl->exec();
			if (!$response)
				return false;
			else
				return $response['id'];
		}
		
		public function updateUser($username, $password, $email, $activationCode,  $emailValidated, $id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/user/' , array('authid'=>$this->authid,'apikey'=>$this->apikey), $this->formatUserXMLRequest($username,$password,$email, $activationCode,  $emailValidated, $id), 'POST');
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}
		}
		
		public function deleteUser($id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/user/'.$id.'/', array('authid'=>$this->authid,'apikey'=>$this->apikey),'','DELETE');
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}
		}
		
		public function authenticateUser($username, $password)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/user/authenticate/'.$username.'/' , array('password'=>$password,  'authid'=>$this->authid, 'apikey'=>$this->apikey));
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}
		}

		public function getUserByColumnValue($table, $column, $value)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/'.$table.'/'.$column.'/'.$value.'/', array('authid'=>$this->authid, 'apikey'=>$this->apikey));
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}

		}
		
		protected function formatUserXMLRequest($username, $password, $email,  $activationCode='',  $emailValidated='', $id='')
		{
			$xml='<user>';
			if ($id)
				$xml.="<id>$id</id>";
			if($activationCode)
				$xml.="<emailValidationString>$activationCode</emailValidationString>";
			if($emailValidated)
				$xml.="<emailValidated>$emailValidated</emailValidated>";
			if($username)
				$xml.="<username>$username</username>";
			if($password)
				$xml.="<password>$password</password>";
			if($email)
				$xml.="<email>$email</email>";
			$xml.='</user>';
			return $xml;			
		}

		// projects methods
		
		public function getProject($id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/project/'.$id.'/', array('authid'=>$this->authid,'apikey'=>$this->apikey));
			$response=$curl->exec();
			if (!$response)
				return false;
			else
				return $response;
		}
		
		public function createProject($productId, $name, $description, $ownerId, $bidCloseDate, $daysToComplete, $appType, $requirements, $requirementsList, $type, $status, $budget)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/project/' , array('authid'=>$this->authid,'apikey'=>$this->apikey), $this->formatProjectXMLRequest($productId, $name, $description, $ownerId, $bidCloseDate, $daysToComplete, $appType, $requirements, $requirementsList, $type, $status, $budget),'PUT');
			$response=$curl->exec();
			if (!$response)
				return false;
			else
				return $response['id'];
		}
		
		public function updateProject($productId, $name, $description, $ownerId, $bidCloseDate, $daysToComplete, $appType, $requirements, $requirementsList, $type, $status, $budget, $id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/project/' , array('authid'=>$this->authid,'apikey'=>$this->apikey), $this->formatProjectXMLRequest($username,$password,$email, $activationCode,  $emailValidated, $id), 'POST');
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}
		}
		
		public function deleteProject($id)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/project/'.$id.'/', array('authid'=>$this->authid,'apikey'=>$this->apikey),'','DELETE');
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}
		}
		

		public function getProjectByColumnValue($table, $column, $value)
		{
			$curl= new My_CurlWrapper($this->baseUrl.$this->service.'/'.$table.'/'.$column.'/'.$value.'/', array('authid'=>$this->authid, 'apikey'=>$this->apikey));
			$response=$curl->exec();
			if (!$response)
			{
				return false;
			}
			else 
			{
				return $response;
			}

		}
		
		protected function formatProjectXMLRequest($productId, $name, $description, $ownerId, $bidCloseDate, $daysToComplete, $appType, $requirements, $requirementsList, $type, $status, $budget, $id='')
		{
			$xml='<project>';
			if ($id)
				$xml.="<id>$id</id>";
			if($productId)
				$xml.="<productId>$productId</productId>";
			if($name)
				$xml.="<name>$name</name>";
			if($description)
				$xml.="<description>$description</description>";
			if($ownerId)
				$xml.="<ownerId>$ownerId</ownerId>";
			if($bidCloseDate)
				$xml.="<bidCloseDate>$bidCloseDate</bidCloseDate>";
			if($daysToComplete)
				$xml.="<daysToComplete>$daysToComplete</daysToComplete>";
			if($appType)
				$xml.="<appType>$appType</appType>";
			if($requirements)
				$xml.="<requirements>$requirements</requirements>";
			if($requirementsList)
				$xml.="<requirementsList>$requirementsList</requirementsList>";
			if($type)
				$xml.="<type>$type</type>";
			if($status)
				$xml.="<status>$status</status>";
			if($budget)
				$xml.="<budget>$budget</budget>";
			$xml.='</project>';
			return $xml;			
		}

}