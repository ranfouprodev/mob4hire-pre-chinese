<?php
/**
 * My_Form
 *
 *
 * Zend form implemntation defining default behaviour of forms.
 *
 */
class My_Form extends ZendX_JQuery_Form
{

	
	public $elementDecorators = array(
								'ViewHelper',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
							
	public $fileDecorators = array(
								'File',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
	public $buttonDecorators = array('ViewHelper', array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_input')));
	public $submitDecorators = array(
								'ViewHelper',
								array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'button')),
							);
	public $jQueryDecorators = array(
								array('UiWidgetElement', array('tag' => '')), // it necessary to include for jquery elements
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);

	public function __construct($options = null)
	{
		$this->addElementPrefixPath('My_Form_Decorator', 'My/Form/Decorator/', 'decorator');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		parent::__construct($options);
	}

/*	public function loadDefaultDecorators()	
	{
		$this->addDecorator('FormElements')
			->addDecorator('Form');

		$this->setElementDecorators(array('MobForm'));

 
	}
*/
}

