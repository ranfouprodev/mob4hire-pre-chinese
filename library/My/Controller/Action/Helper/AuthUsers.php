<?php

require_once 'Zend/Controller/Action/Helper/Abstract.php';


class My_Controller_Action_Helper_AuthUsers extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * Constructor
     *     
     * @return void
     */
    public function __construct()
    {
    }

    public function preDispatch()
    {
        $actionController = $this->getActionController();
        $actionController->view->isLoggedIn = $this->isLoggedIn();
        $actionController->view->username = $this->getUsername();
    }
    
    public function isLoggedIn()
    {
        $auth = Zend_Auth::getInstance();
        return $auth->hasIdentity();
    }
    
    public function getUsername()
    {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        if (!$identity)
        {
            return null;            
        }
        
        return $identity;
    }
	
	
	public function getFacebookUser()
	{
		$fb = new Facebook('3417a1664e1f729761c8b6014fc7f61f','1edbf13ee84f91669fcbdf41ba653d67');
		
		if(!$fb){
			return "No facebook object";
		}
		
		$fb_user=$fb->get_loggedin_user();
		 if (!$fb_user)
        {
            return null;            
        }
        
        return $fb_user;

	}
	
	
}
