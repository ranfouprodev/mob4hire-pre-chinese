<?php
/**
 * My_Form
 *
 *
 * Zend form implementation defining default behaviour of forms.
 *
 */
class My_VanillaSurveyForm extends Zend_Form
{
	protected $survey;
	protected $iduser; 
	protected $prev;
	/*
	/* This will create a survey form  object from the underliying survey model.
	*/
	public function __construct($survey, $iduser, $options = null)
	{
		$this->survey=$survey;
		$this->iduser = $iduser;
		$this->prev=$options; // This is used to feed in the entries from the previous page
		parent::__construct($options);
	}

	public function init()
	{	
		if ($question=$this->survey->getQuestion())
		{
			$this->setAttribs(array('method'=>'post', 'action'=>'/survey/vanilla/'.$this->survey->getId().'/'.$this->iduser.'/'.$this->survey->getCount(), 'enctype'=>'multipart/form-data'));		
			$title=new My_Form_Element_myXhtml('title');
			$title->setContent("<h2>".$question['title']."</h2>");
			
			$counter = new My_Form_Element_myXhtml('counter');
			$counter->setContent('<div class="center-wrapper"><h3 >'.(int)(($this->survey->getCount()+1)*100/$this->survey->getTotal()).'% done</h3></div>');

			switch ($question['type'])
			{
				case 8; // maybe we still have to do dates this way or the validators and stuff don't work
					$element =  new My_Form_Element_DateSelects($question['idquestion']);
					$validatorDate = new Zend_Validate_Date();
					$validatorDate->setMessages(
							array(
							Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
							Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
							)
						);               
					$element->addValidator($validatorDate)->setStartEndYear(1933, date("Y"))->setReverseYears(true);
					break;				
				default:
					$element=new My_Form_Element_JQM($question['idquestion']);
					$element->setQuestion($question, $this->prev); // pass previous question for 
			}
			$element->setDecorators(array(
						'ViewHelper',
						'Label',
						new Zend_Form_Decorator_HtmlTag(array('tag' => 'div', 'data-role'=>'fieldcontain'))
				))
				//->setLabel($question['text'])
				->setAttrib('data-theme', 'm');
				
			if($question['mandatory']) {
				$element->setRequired(true);
			}

			$label = new My_Form_Element_myXhtml('question');
			$label->setContent('<h4 class="center-wrapper">'.$question['text'].'</h4>');
			
			
			$submit = new Zend_Form_Element_Submit('submit');
			$submit->setDecorators(array(
						'ViewHelper'
				));
			$submit->setAttrib('data-theme', 'm');

			$this->addElement($title);
			$this->addElement($label);
			$this->addElement($element);
			$this->addElement($submit);
			$this->addElement($counter);

		}
		else {
			
			// We can have multiple tests per project but only one user per survey. Hopefully this won't cause confusion in the future. 
			// #famouslastwords
			// yes, I see what you are saying now. will bear this in mind
		
			//$tests = new Tests();
			
			//$testList = $tests->listTestsByTesterAndProject($this->survey->idproject, $this->iduser,'250');
			
			//if(isset($testList) && $test = $testList->current()){
				// submit the results
				//echo "Submitting results for:".$test->idtest." user ".$this->iduser;
			//	$tests->submitTest($test->idtest, $this->iduser);
			//}
			
			$title = new My_Form_Element_myXhtml('title');
			$title->setContent('<h2>Thank you! You have completed this survey</h2>
<p>The project manager has been informed that you have completed the survey and should
release the funds to you in the next 24h</p><p><a href="/survey/vanilla/reset/'.$this->survey->getId().'/'.$this->iduser.'/">Reset</a> ');
			
			$scriptComplete = new My_Form_Element_myXhtml('script');
			$scriptComplete->setContent('<script type="text/javascript">Mob4Hire.surveyComplete();</script>');  
			
			$this->addElement($title);
			$this->addElement($scriptComplete);

			//if($this->survey->getId()==2) {
			  // This is the profile survey
			//  $pm=new ProfileMobster();
			//  $pm->importFromSurvey($this->iduser);
		    //	}
		}
	}

}

