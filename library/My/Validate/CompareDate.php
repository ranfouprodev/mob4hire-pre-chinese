<?php
require_once 'Zend/Validate/Abstract.php';
require_once 'Zend/Date.php';

class My_Validate_CompareDate extends Zend_Validate_Abstract
{
	const ERROR_MSG = 'errorMsg';
	const DATE_IN_PAST= 'dateInPast';
	
	protected $_messageTemplates = array(
		self::ERROR_MSG => "Start date needs to be before end date.",
		self::DATE_IN_PAST => "Project listing can not start in the past"
	);
    
	protected $fieldToMatch = '';
    
	public function __construct($fieldToMatch=null)
	{
		if($fieldToMatch){
			$this->fieldToMatch = (string)$fieldToMatch;               
		}
	}
	
    public function isValid($value, $context = null)
    {
	$date1=new Zend_Date($value, Zend_Date::ISO_8601);

	if($this->fieldToMatch && isset($context[$this->fieldToMatch])) // we are matching another field in the form
	{
		$date2=new Zend_Date($context[$this->fieldToMatch], Zend_Date::ISO_8601);

		if(!$date1->isLater($date2))
		{
			$this->_error(self::ERROR_MSG);
			return false;      
		}
	}
	else // we are matching with today's date
	{
		$today = new Zend_Date();
		if($date1->compare($today, Zend_Date::DATES)==-1)
		{
			$this->_error(self::DATE_IN_PAST);
			return false;      
		}
	}
       return true;	
    }
}
?>