<?php
require_once 'Zend/Validate/Abstract.php';   

class My_Validate_CurrentPasswordConfirmation extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'passwordConfirmationNotMatch';
    
    protected $_messageTemplates = array(
        self::NOT_MATCH => "Password doesn't match the account"
    );
        
    public function __construct(){}
	
    public function isValid($value, $context = null)
    {
    	
        $valueString = (string) $value;
        $this->_setValue($valueString);
		
		$users = new Users(); 
		$user = $users->getUser(Zend_Registry::get('defSession')->currentUser->id);
		        
        if ($user['password'] !=  Users::computePasswordHash($valueString))
        {
            $this->_error(self::NOT_MATCH);
            return false;      
        }

        return true;
    }
}