<?php
/**
 * My_Form
 *
 *
 * Zend form implemntation defining default behaviour of forms.
 *
 */
class My_SurveyForm extends Zend_Form
{

	
	public $elementDecorators = array(
								'ViewHelper',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
							
	public $fileDecorators = array(
								'File',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
	public $buttonDecorators = array('ViewHelper', array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_input')));
	public $submitDecorators = array(
								'ViewHelper',
								array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'button')),
							);
	public $jQueryDecorators = array(
								array('UiWidgetElement', array('tag' => '')), // it necessary to include for jquery elements
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);

	protected $idsurvey;
	protected $iduser;
	
	/*
	/* This will create a survey form  object from the underliying survey model.
	*/
	public function __construct($idsurvey, $iduser, $options = null)
	{
		$this->addElementPrefixPath('My_Form_Decorator', 'My/Form/Decorator/', 'decorator');
		$this->setAttrib('enctype', 'multipart/form-data');
		$this->idsurvey=$idsurvey;
		$this->iduser=$iduser;
		
		parent::__construct($options);
	}

	public function init()
	{
		$survey=new Survey($this->idsurvey, $this->iduser);
		
		if ($question=$survey->getQuestion())
		{
			$title = new My_Form_Element_myXhtml('title');
			$title->setContent('<h1>'.$survey->getTitle().'</h1>');

			switch ($question['type'])
			{
				case 1:
					$element=new Zend_Form_Element_Select($question['idquestion']);
					$element->setLabel($question['text']);
			
					foreach($question['answer'] as $id=>$answer)
					{
						$element->addMultiOption($id,$answer);
					}
					break;
				case 2:
					$element=new Zend_Form_Element_Text($question['idquestion']);
					$element->setLabel($question['text']);
					break;
				// add new question types here if required
				// maybe the first of these should be a static text type
			}
		
			$submit = new Zend_Form_Element_Submit('submit');
			$submit->setLabel('Submit')
				->setDecorators($this->submitDecorators);
	
			$this->addElement($title);
			$this->addElement($element);
			$this->addElement($submit);
		}
		else {
			$title = new My_Form_Element_myXhtml('title');
			$title->setContent('<h1>Thank you. You have completed this survey</h1>');
			$this->addElement($title);
		}
	}
/*	public function loadDefaultDecorators()	
	{
		$this->addDecorator('FormElements')
			->addDecorator('Form');

		$this->setElementDecorators(array('MobForm'));

 
	}
*/
}

