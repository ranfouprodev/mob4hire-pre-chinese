<?php
require_once 'Zend/Form/Element.php';

class My_Form_Element_JQM extends Zend_Form_Element_Xhtml 
{
    public $helper = 'jQM';    
    public $options = array();

    public function setQuestion($question, $prev=null)
    {
	$this->options=$question; // This can then be accessed in the view helper
	$this->options['prev']=$prev;
    }    
}
