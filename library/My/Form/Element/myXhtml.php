<?php
require_once 'Zend/Form/Element.php';

class My_Form_Element_myXhtml extends Zend_Form_Element_Xhtml
{
   private $_content;

    /**
     * Setter for the element content
     *
     * @param string
     */
   public function setContent($content)
   {
      $this->_content = $content;
   }

    /**
     * Render form element
     *
     * @param  Zend_View_Interface $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        if($view !== null) {
            $this->setView($view);
        }
        return $this->_content;
    }   
}

?>