<?php
require_once 'Zend/Form/Element.php';

class My_Form_Element_ModelSelect extends Zend_Form_Element_Xhtml 
{
	
	public $helper='modelSelect';
	
	public function setVendor($idvendor)
	{
		$this->options=array('vendor'=>$idvendor);
	}
}
