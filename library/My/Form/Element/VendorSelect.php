<?php
require_once 'Zend/Form/Element.php';

class My_Form_Element_VendorSelect extends Zend_Form_Element_Xhtml 
{
    /**
     * Use formSelect view helper by default
     * @var string
     */
    public $helper = 'vendorSelect';        

}
