<?php
require_once 'Zend/Form/Element.php';

class My_Form_Element_NetworkSelect extends Zend_Form_Element_Xhtml 
{
    /**
     * Use formSelect view helper by default
     * @var string
     */
	public $helper = 'networkSelect';        

	// Can't figure out how to get this to work using a view helper try this instead
	
	private $_country;
	
	public function setCountry($idcountry)
	{
		$this->options=array('country'=>$idcountry);
	}
	
}
