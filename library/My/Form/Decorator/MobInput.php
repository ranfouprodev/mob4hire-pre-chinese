<?php 
class My_Form_Decorator_MobInput extends Zend_Form_Decorator_Abstract {
   
    
    public function buildInput() {
        $element = $this->getElement();
        $helper = $element->helper;
				$messages = $element->getMessages();
				$input_str = '<div class="form_input';
				if(!empty($messages)) {
					$input_str .= ' form_error_box';
				}
				$input_str .= '">';
        $input_str .= $element->getView()->$helper($element->getName(), $element->getValue(), $element->getAttribs(), $element->options);
        if (!empty($messages)) {
	        $input_str .= '<div class="form_errors">'.$element->getView()->formErrors($messages).'</div>';
        }
				$input_str .= '</div>';
				return $input_str;
    }
    
    public function buildErrors() {
        $element = $this->getElement();
        $messages = $element->getMessages();
        if ( empty($messages)) {
            return '';
        }
        return '<div class="form_errors"><p>'.$element->getView()->formErrors($messages).'</p></div>';
    }
    
        
    public function render($content) {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }
        
        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
       	//$label = $this->buildLabel();
        $input = $this->buildInput();
        //$errors = $this->buildErrors();
        //$desc = $this->buildDescription();
        
        $output = $input;
        
        switch ($placement) {
            case (self::PREPEND):
                return $output.$separator.$content;
            case (self::APPEND):
            default:
                return $content.$separator.$output;
        }
    }
}



?>
