<?php 
class My_Form_Decorator_MobFormTable extends Zend_Form_Decorator_Abstract {
    public function buildLabel() {
        $element = $this->getElement();
        $label = $element->getLabel();
        
		if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label = '* '.$label;
        }
		
				$renderedLabel = '<div class="form_label">'. $element->getView()->formLabel($element->getFullyQualifiedName(), trim($label),array("escape"=>false));
				
        
        if ($desc = $element->getDescription()) {
			    $renderedLabel .= '<div class="form_description"><p>'.$desc.'</p></div>';
        }
				$renderedLabel .= '</div>';
		
				$renderedLabel .= '<div class="form_helptip">';
        if ($element->helpText) {
            $renderedLabel .= '<span class="whats-this"><a href="javascript:void(0);">
			<img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>'
			. $element->helpText .'</p></span>';
        } else {
					$renderedLabel .= '&nbsp;';
				}

				$renderedLabel .= '</div></td>';

        
        // Render the Label and Helper
        return $renderedLabel;
        
    }
    
    public function buildInput() {
        $element = $this->getElement();
        $helper = $element->helper;
				$messages = $element->getMessages();
				$input_str = '<div class="form_input';
				if(!empty($messages)) {
					$input_str .= ' form_error_box';
				}
				$input_str .= '">';
        $input_str .= $element->getView()->$helper($element->getName(), $element->getValue(), $element->getAttribs(), $element->options);
        if (!empty($messages)) {
	        $input_str .= '<div class="form_errors">'.$element->getView()->formErrors($messages).'</div>';
        }
				$input_str .= '</div>';
				return $input_str;
    }
    
    public function buildErrors() {
        $element = $this->getElement();
        $messages = $element->getMessages();
        if ( empty($messages)) {
            return '';
        }
        return '<div class="form_errors"><p>'.$element->getView()->formErrors($messages).'</p></div>';
    }
    
    public function buildDescription() {
        $element = $this->getElement();
        $desc = $element->getDescription();
        if ( empty($desc)) {
            return '';
        }
        return '<div class="form_description"><p>'.$desc.'</p></div>';
    }
    
    public function render($content) {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }
        
        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label = $this->buildLabel();
        $input = $this->buildInput();
        //$errors = $this->buildErrors();
        //$desc = $this->buildDescription();
        
        //$output = '<div class="form_element">'.$label.$input.$errors.$desc.'</div>';
        $output = '<tr><td class="label">'.$label.'</td><td>'.$input.'</td><td>&nbsp;</td></tr>';
        
        switch ($placement) {
            case (self::PREPEND):
                return $output.$separator.$content;
            case (self::APPEND):
            default:
                return $content.$separator.$output;
        }
    }
}



?>
