<?php 
class My_Form_Decorator_MobLabel extends Zend_Form_Decorator_Abstract {
    
	public function buildLabel() {
        $element = $this->getElement();
        $label = $element->getLabel();
        
		if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label = '* '.$label;
        }
		
		$renderedLabel = '<div class="form_label">'. $element->getView()->formLabel($element->getFullyQualifiedName(), trim($label),array("escape"=>false));
				
        
        if ($desc = $element->getDescription()) {
			    $renderedLabel .= '<div class="form_description"><p>'.$desc.'</p></div>';
        }
				$renderedLabel .= '</div>';
				$renderedLabel .= '<div class="form_helptip">';
        if ($element->helpText) {
            $renderedLabel .= '<span class="whats-this"><a href="javascript:void(0);">
			<img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>'
			. $element->helpText .'</p></span>';
        } else {
					$renderedLabel .= '&nbsp;';
				}

				$renderedLabel .= '</div>';
        
        // Render the Label and Helper
        return $renderedLabel;
        
    }
    
    
    public function render($content) {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }
        
        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label = $this->buildLabel();
       
	    $output = $label;
        
        switch ($placement) {
            case (self::PREPEND):
                return $output.$separator.$content;
            case (self::APPEND):
            default:
                return $content.$separator.$output;
        }
    }
}



?>
