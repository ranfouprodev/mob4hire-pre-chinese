<?php
/**
* Wrapper for curl used like this:
* $url='http://ec2-174-129-118-227.compute-1.amazonaws.com/APIAuth/user/authenticate/mob4hire/';
* $httpparams=array('authid'=>'1', 'apikey'=>'ZWY3ODE3MGUyZWNiN2Q0OWE2M2UwZmUwMzU4YzAwZWU%3d');
* $xmlbody= '<user><username>richard<password><email>richard@mob4hire.com</email></user>' ;
* $httpverb='GET';
* $curlwrapper= new My_Curl_Wrapper($url, $httpparams, $xmlbody, $httpverb);
*/

class My_CurlWrapper 
{
	private $curlopt=array();
	private $ch;
	
	public function __construct($url, $httpparams='', $xmlbody='', $httpverb='GET')
	{
		$this->ch = curl_init(); 
		$fields_string='';
		if ($httpparams)
		{
			// Append the http request parameters to the url	
			$fields_string='?';
			foreach($httpparams as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }  // url encoding removed here  because of problems with upper case letters in escape values 
			$fields_string=rtrim($fields_string,'&');  
	
		}
		echo $url.$fields_string."<br/>";
		echo $xmlbody;
		curl_setopt($this->ch, CURLOPT_URL,$url.$fields_string);  
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		if ($httpverb=='PUT' || $httpverb=='POST')
		{
			// If we are sending XML we need to set a custom request type
			curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $httpverb);
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml','Content-Length: '.strlen($xmlbody)));
			curl_setopt($this->ch, CURLOPT_POSTFIELDS, $xmlbody);
		}
		else if ($httpverb=='DELETE')
		{
			curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $httpverb);
		}
	}
	
	public function exec() 
	{
		$response=curl_exec($this->ch);
		$status=curl_getinfo($this->ch,CURLINFO_HTTP_CODE);
		echo $response;
		if ($status != 200)
		{
			return false;
		}
		else 
		{
			return $this->toArray(simplexml_load_string($response));
		}
	}
	
	private function toArray($obj) 
	{
		print_r($obj);
		$array=Array();
		$children = $obj->children(); 
		foreach ($children as $elementName => $node) 
		{ 
			$array[$elementName]=(string)$node;
		}
		return $array;
	}
		
}