<?php

class My_Auth_Adapter implements Zend_Auth_Adapter_Interface {
	/**
	* Username
	*
	* @var string
	*/
	protected $username = null;

	/**
	* Password
	*
	* @var string
	*/
	protected $password = null;

	protected $user;
	
	/**
	* Class constructor
	*
	* The constructor sets the username and password
	*
	* @param string $username
	* @param string $password
	*/
	public function __construct($username, $password) {
		$this->username = $username;
		$this->password = $password;
	}

	public function setIdentity($name)
	{
		$this->username = $name;
		return $this;
	}

	public function setCredential($pass)
	{
		$this->password = $pass;
		return $this;
	}

	public function getResultRowObject()
	{
		return $this->user;
	}
	
	/**
	* Authenticate
	*
	* Authenticate the username and password
	*
	* @return Zend_Auth_Result
	*/
	public function authenticate() {
		// Try to fetch the user using the model
		$users = new Users();
		$this->user = $users->authenticate($this->username, $this->password);

		// Initialize return values
		$code = Zend_Auth_Result::FAILURE;
		$identity = null;
		$messages = array();

		// Do we have a valid user?
		if ($this->user) {
			$code = Zend_Auth_Result::SUCCESS;
			$identity = $this->user['username'];
		} else {
			$messages[] = 'Authentication error';
		}

		return new Zend_Auth_Result($code, $identity, $messages);
	}
}
