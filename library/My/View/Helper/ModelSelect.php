<?php


class My_View_Helper_ModelSelect extends Zend_View_Helper_Abstract
{
	public function modelSelect($name, $value = null, $attribs = null, $options = null)
	{
		$devices=new Devices();
		$modellist=$devices->listModels($options['vendor']);

		$model = new Zend_Form_Element_Select($name);
		$model->setLabel('Model');
		foreach($modellist as $id=>$modelname) {
				$model->addMultiOption($id , $modelname);
		}
		return $model;
	}

}

?>