<?php
require_once 'Zend/View/Helper/FormElement.php';

class My_View_Helper_FormDateSelects extends Zend_View_Helper_FormElement
{
    public function formDateSelects($name, $value = null, $attribs = null,
    $options = null, $listsep = "<br />\n")
    {
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, id, value, attribs, options, listsep, disable

        // now start building the XHTML.
        $disabled = '';
        if (true === $disable) {
            $disabled = ' disabled="disabled"';
        }

        $elementNamesArray = $this->getDayMonthYearFieldNames($name);
        $valueDay = $valueMonth = $valueYear = null;

        if ($value !== null)
        {
            $valueExploded = explode('-', $value);
            if (!isset($valueExploded[2]))
            $value = null;
            else
            {
                $valueDay = (int) $valueExploded[2];
                $valueMonth = (int) $valueExploded[1];
                $valueYear = (int) $valueExploded[0];
            }
        }

        // Build the surrounding day element first.
        $xhtml = '<select'
        . ' name="' . $this->view->escape($elementNamesArray['day']) . '"'
        . ' id="' . $this->view->escape($id . '_day') . '"'
        . $disabled
        . $this->_htmlAttribs($attribs)
        . ">\n    ";

        // build the list of options
        $list = array();
        if ($options['showEmpty'])
        {
            $list[] = '<option value="-"> </option>';
        }
        for ($i = 1; $i <= 31; $i++)
        {
            $list[] = '<option'
            . ' value="' . $i . '"'
            . ($valueDay === $i ? ' selected="selected"' : '')
            . '>' . $i . '</option>';
        }

        // add the options to the xhtml and close the select
        $xhtml .= implode("\n    ", $list) . "\n</select>";

        // Build the month next
        $xhtml .= ' <select'
        . ' name="' . $this->view->escape($elementNamesArray['month']) . '"'
        . ' id="' . $this->view->escape($id . '_month') . '"'
        . $disabled
        . $this->_htmlAttribs($attribs)
        . ">\n    ";

        // build the list of options
        $list = array();
        if ($options['showEmpty'])
        {
            $list[] = '<option value="-"> </option>';
        }
        $month = array(" ","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
        for ($i = 1; $i <= 12; $i++)
        {
            $list[] = '<option'
            . ' value="' . $i . '"'
            . ($valueMonth === $i ? ' selected="selected"' : '')
            . '>' . $month[$i] . '</option>';
        }

        // add the options to the xhtml and close the select
        $xhtml .= implode("\n    ", $list) . "\n</select>";

         
        // Build the years next
        $xhtml .= ' <select'
        . ' name="' . $this->view->escape($elementNamesArray['year']) . '"'
        . ' id="' . $this->view->escape($id . '_year') . '"'
        . $disabled
        . $this->_htmlAttribs($attribs)
        . ">\n    ";

        // build the list of options
        $list = array();
        if ($options['showEmpty'])
        {
            $list[] = '<option value="-"> </option>';
        }


        if ($options['reverseYears'])
        {
            for ($i = $options['endYear']; $i >= $options['startYear']; $i--)
            {
                $list[] = '<option'
                . ' value="' . $i . '"'
                . ($valueYear === $i ? ' selected="selected"' : '')
                . '>' . $i . '</option>';
            }
        }
        else
        {
            for ($i = $options['startYear']; $i >= $options['endYear']; $i++)
            {
                $list[] = '<option'
                . ' value="' . $i . '"'
                . ($valueYear === $i ? ' selected="selected"' : '')
                . '>' . $i . '</option>';
            }            
        }

         
        // add the options to the xhtml and close the select
        $xhtml .= implode("\n    ", $list) . "\n</select>";

        return $xhtml;
    }


    /**
     * Makes day, month and year names from given element name. Special case is array notation.
     *
     * Given a value such as foo[bar][baz], the generated names will be
     * foo[bar][baz_day], foo[bar][baz_month] and foo[bar][baz_year]
     *
     * @param  string $value
     * @return array
     */
    protected function getDayMonthYearFieldNames($value)
    {
        if (empty($value) || !is_string($value)) {
            return $value;
        }

        $ret = array(
                'day' => $value . '_day',
                'month' => $value . '_month',
                'year' => $value . '_year'
                );

                if (strstr($value, '['))
                {
                    $endPos = strlen($value) - 1;
                    if (']' != $value[$endPos]) {
                        return $ret;
                    }

                    $start = strrpos($value, '[') + 1;
                    $name = substr($value, $start, $endPos - $start);
                    $arrayName = substr($value, 0, $start-1);
                    $ret = array(
                    'day' => $arrayName . '[' . $name . '_day' . ']',
                    'month' => $arrayName . '[' . $name . '_month'  . ']',
                    'year' => $arrayName . '[' . $name . '_year' . ']'
                    );
                }
               return $ret;
    }
}