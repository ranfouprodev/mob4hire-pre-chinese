<?php 
class My_View_Helper_FormUploadify extends Zend_View_Helper_FormText {
    protected $_uploadify;
    
    public function FormUploadify($name, $value = null, $attribs = null) {
        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

        
        // Append the scripts
        $defaultScript = '/js/uploadify/jquery.uploadify.v2.1.0.min.js';
        $swfScript = '/js/uploadify/swfobject.js';
        
        $this->view->headScript()->appendFile($defaultScript);
        $this->view->headScript()->appendFile($swfScript);

        
        $script = "$(document).ready(function() {
			$('#".$this->view->escape($name)."').uploadify({
				'uploader'  : '/js/uploadify/uploadify.swf',
				'script'    : '/ajax/index/uploadimage/',
				'cancelImg' : '/js/uploadify/cancel.png',
				'fileDesc'  : 'Image Files',
				'fileExt'   : '*.jpg;*.jpeg;*.gif;*.png',
				'auto'      : true,
				'multi'		: false, 
				'sizeLimit' : 307200,
				'fileDataName' : 'projImg',
				'folder'    : '/',
				 onError: function (event, queueID, fileObj, errorObj){
						alert('Error uploading file. Check file size (under 300kb) and try again');
					},
				onComplete: function(event, queueID, fileObj, response, data) {
     				$('#uploads').append(response);
					}
				});
			});";
	$this->view->headScript()->appendScript($script, $type = 'text/javascript');

	$xhtml = '<input type="FILE" name="' . $this->view->escape($name) . '"'
		. ' id="' . $this->view->escape($id) . '"></input>';


	$xhtml .= '<div id="uploads"></div>';

        return $xhtml;
    }
}
?>
