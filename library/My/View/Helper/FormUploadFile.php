<?php 
class My_View_Helper_FormUploadFile extends Zend_View_Helper_FormText {

    public function FormUploadFile($name, $value = null, $attribs = null) {

        $translate = Zend_Registry::get('Zend_Translate');

        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable
   
	    $xhtml = '<div class="dropzone"  name="' . $this->view->escape($name) . '"'. ' id="' . $this->view->escape($id) . '">';
        $xhtml .= '<div class="dz-default dz-message"><span><h3>'.$translate->_('app_profile_index_image_drop').'</h3></span></div>';
        $xhtml .= '</div>';

        return $xhtml;
    }
}
?>
