<?php


class My_View_Helper_VendorSelect extends Zend_View_Helper_Abstract
{
	public function vendorSelect($name)
	{
		$devices=new Devices();	
		$vendorlist=$devices->listVendors();

		$vendor = new Zend_Form_Element_Select($name);
		$vendor->setLabel('Handset Vendor');
		foreach($vendorlist as $id=>$vendorname) {
				$vendor->addMultiOption($id, $vendorname);
		}
		return $vendor;
	}

}

?>