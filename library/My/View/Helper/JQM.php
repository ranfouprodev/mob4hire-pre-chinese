<?php


class My_View_Helper_JQM extends Zend_View_Helper_Abstract
{
	public function jQM($name, $value = null, $attribs = null, $options = null)
	{
		switch ($options['type'])
		{
			case 1: // SELECT
				$element= ($options['multiselect']) ? new Zend_Form_Element_MultiCheckbox($name) : new Zend_Form_Element_Select($name);
				foreach($options['answer'] as $id=>$answer)
				{
					$element->addMultiOption($id,$answer);
				}
	                    
				break;
			case 2: // Free text
				$element=new Zend_Form_Element_Text($name);
				break;
			case 3: // Yes/No
				$element=new My_Form_Element_YesNo($name);
				break;
			case 4: // Country
				$element=new My_Form_Element_CountrySelect($name);
				break;
			case 5: // Network
				fb(print_r($options,true));
				
				if($options['prev']) {
					if ($options['prev']['idquestion']==$name-1)
					{
						$country=$options['prev']['text'];
					}
					else
						$country=1;
				}
				else
					$country=1; // We will assume us if we don't have country available - todo: something smarter
				$element=new My_Form_Element_NetworkSelect($name);
				$element->setCountry($country);
				break;
			case 6: // Vendor
				$element=new My_Form_Element_VendorSelect($name);
				break;	
			case 7: // Model
				if($options['prev']) {
					if ($options['prev']['idquestion']==$name-1)
					{
						$vendor=$options['prev']['text'];
					}
					else
						$vendor='Apple';
				}
				else
					$vendor='Apple';
				$element=new My_Form_Element_ModelSelect($name);
				$element->setVendor($vendor);
				break;

			case 8: // Date
				$element =  new My_Form_Element_DateSelects($name);
				$validatorDate = new Zend_Validate_Date();
				$validatorDate->setMessages(
	                                array(
	                                    Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
	                                    Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
	                                )
	                            );               
				$element->addValidator($validatorDate)->setStartEndYear(1933, date("Y"))->setReverseYears(true);

				break;
			
		}

		return $element;
	}

}

?>