<?php


class My_View_Helper_CountrySelect extends Zend_View_Helper_Abstract
{
	public function countrySelect($name)
	{
		$countries=new Countries();
		$countrylist=$countries->listCountries(); 

		$country = new Zend_Form_Element_Select($name);
		$country->setLabel('Country');
		foreach($countrylist as $id=>$countryname) {
				$country->addMultiOption(substr($id,7), $countryname);
		}
		return $country;
	}

}

?>