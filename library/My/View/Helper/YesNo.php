<?php


class My_View_Helper_YesNo extends Zend_View_Helper_Abstract
{
	public function yesNo($name)
	{
		$return="<fieldset data-role='controlgroup' data-type='horizontal'>";
		$return.="<input type='radio' name='$name' id='yes' value='on'>";
		$return.="<label for='yes'>Yes</label>";
		$return.="<input type='radio' name='$name' id='no' value='off'>";
		$return.="<label for='no'>No</label>";
		$return.="</fieldset>";
		return $return;
	}

}

?>