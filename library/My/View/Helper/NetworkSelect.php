<?php


class My_View_Helper_NetworkSelect extends Zend_View_Helper_Abstract
{
	public function networkSelect($name, $value = null, $attribs = null, $options = null)
	{
		$networks=new Networks();
		$networklist=$networks->listNetworks($options['country']); 

		$network = new Zend_Form_Element_Select($name);
		$network->setLabel('Network');
		foreach($networklist as $id=>$networkname) {
				$network->addMultiOption($id , $networkname);
		}
		return $network;
	}

}

?>