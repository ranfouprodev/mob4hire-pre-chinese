<?php
/**
 * My_Form
 *
 *
 * Zend form implemntation defining default behaviour of forms.
 *
 */
class My_TouchSurveyForm extends Zend_Form
{
	protected $survey;
	
	/*
	/* This will create a survey form  object from the underliying survey model.
	*/
	public function __construct($survey, $iduser, $options = null)
	{
		$this->survey=$survey;
		$this->setAttribs(array('method'=>'post', 'action'=>'/survey/index/touchandgo/idsurvey/'.$survey->getId(), 'enctype'=>'multipart/form-data'));		
		parent::__construct($options);
	}

	public function init()
	{
		
		if ($question=$this->survey->getQuestion())
		{
			switch ($question['type'])
			{
				case 1:
					$element=new Zend_Form_Element_Select($question['idquestion']);
			
					foreach($question['answer'] as $id=>$answer)
					{
						$element->addMultiOption($id,$answer);
					}
					break;
				case 2:
					$element=new Zend_Form_Element_Text($question['idquestion']);
					break;
				// add new question types here if required
				// maybe the first of these should be a static text type
			}
			$element->setDecorators(array(
						'ViewHelper',
						'Label',
						new Zend_Form_Decorator_HtmlTag(array('tag' => 'li')) //wrap elements in <li>'s
				))
				->setLabel($question['text']);

			$separator=new My_Form_Element_myXhtml('separator');
			$separator->setContent('<li class="sep"></li>');
/*
			$submit = new Zend_Form_Element_Submit('submit');
			$submit->setDecorators(array(
						'ViewHelper',
						new Zend_Form_Decorator_HtmlTag(array('tag' => 'li')) //wrap elements in <li>'s
				));
//				->setAttribs(array(/*'class'=>'submit', 'onClick'=>'submitPage(this.form)'));
*/
			$submit=new My_Form_Element_myXhtml('submit');
			$submit->setContent('<a class="submit whiteButton">Next</a>');
			$this->addElement($element);
			$this->addElement($separator);
			$this->addElement($submit);
		}
		else {
			$title = new My_Form_Element_myXhtml('title');
			$title->setContent('<h1>Thank you. You have completed this survey</h1>');
			$this->addElement($title);
		}
	}
	
	public function loadDefaultDecorators()
	{
		$this->addDecorator('FormElements')
			->addDecorator('HtmlTag', array('tag' => 'ul', 'class'=>'form')) //this adds a <ul> inside the <form>
			->addDecorator('Form');
			//->setAttribs(array(/*'class'=>'submit',*/ 'onSubmit'=>'submitPage(this.form)'));
	}
}

