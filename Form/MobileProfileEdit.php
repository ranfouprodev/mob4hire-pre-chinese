<?php
class Form_MobileProfileEdit extends ZendX_JQuery_Form
{
	public function init()
	{
		$this->setAction('/mobile/profile/edit')->setMethod('post')->setAttrib('id','frmProfileContactInfo');

		$translator = $this->getTranslator();

		$filterTrim = new Zend_Filter_StringTrim();

		// Not included/relevant
		//set radio buttons for which state ment best describes you
		$personalDescription = new Zend_Form_Element_Select('personal_description');
		$personalDescription->setLabel('Which Statement Best Describes You?');
		$personalDescription->addMultiOptions(array(1 => 'I\'m a mobile enthusiast and would like to make money testing and doing surveys.',2 => 'I\'m a quality assurance professional who wants to make extra money freelancing.',3 => 'I\'m a software developer.',4 => 'We\'re a professional mobile testing company.',5 => 'We\'re a professional porting company.',6 => 'I\'m a market researcher.',7 => 'I work for a Developer Network or an APP store.',8 => 'I work at a carrier or handset manufacturer.',9 => 'I work for an O/S, middleware or platform software vendor.',10 => 'None of these seem to suit who I am! I would describe me as:'));
		$personalDescription->setvalue(1);
		$personalDescription->setAttrib('data-theme', 'a');
		$personalDescription->setRequired(true);

		$pField = new Zend_Form_Element_Text('personal_description_other');
		$pField->setLabel($translator->translate('form_mobile_profile_edit_summary'))
			->addFilter($filterTrim);

		$firstName = new Zend_Form_Element_Text('first_name');
		$firstName->setLabel($translator->translate('form_mobile_profile_edit_firstName'))
				->addFilter($filterTrim)
				->setRequired(true);
		
		$lastName = new Zend_Form_Element_Text('last_name');
		$lastName->setLabel($translator->translate('form_mobile_profile_edit_lastName'))->addFilter($filterTrim)
				->setRequired(true);


		$language1 = new Zend_Form_Element_Select('language1');
		$language1->setLabel($translator->translate('form_mobile_profile_edit_primaryLanguage'))
		->setRequired(true);
		$language1->helpText=$translator->translate('form_mobile_profile_edit_primaryLanguage_help');

		$languagesList = new Languages();//get Languages
		$language1->addMultiOptions($languagesList->listLanguages());


		$language2 = new Zend_Form_Element_Select('language2');
		$language2->setLabel($translator->translate('form_mobile_profile_edit_secondaryLanguage'));
		$language2->addMultiOptions($languagesList->listLanguages());


		$company = new Zend_Form_Element_Text('company');
		$company->setLabel($translator->translate('form_mobile_profile_edit_company'))->addFilter($filterTrim);

		$position = new Zend_Form_Element_Text('position');
		$position->setLabel($translator->translate('form_mobile_profile_edit_position'))->addFilter($filterTrim);
	
		$country = new Zend_Form_Element_Select('idcountry');
		$country->setLabel($translator->translate('form_mobile_profile_edit_country'))
		->setRequired(true);

		$countryLst = new Countries();//get countries from Countries model
		$countrylist=$countryLst->listCountries();
		foreach($countrylist as $id=>$countryname) {
			$country->addMultiOption(substr($id,7), $countryname);
		}

		$timeZones = new Zend_Form_Element_Select('time_zone');
		$timeZones->setLabel($translator->translate('form_mobile_profile_edit_timezone'))
		->setRequired(true);

		$timeZoneList = ProfileContactInfo::$timeZoneList;

		$timeZones->addMultiOptions($timeZoneList)
		->setvalue("");



		$address1 = new Zend_Form_Element_Text('address1');
		$address1->setLabel($translator->translate('form_mobile_profile_edit_streetAddress'))->addFilter($filterTrim);

		$address2 = new Zend_Form_Element_Text('address2');
		$address2->addFilter($filterTrim);
		
		$address2->setLabel(' ');
	
		$city = new Zend_Form_Element_Text('city');
		$city->setLabel($translator->translate('form_mobile_profile_edit_city'))->addFilter($filterTrim)
		->setRequired(true);
	
		$state = new Zend_Form_Element_Text('state');
		$state->setLabel($translator->translate('form_mobile_profile_edit_state'))->addFilter($filterTrim);
	
		$zip = new Zend_Form_Element_Text('zip');
		$zip->setLabel($translator->translate('form_mobile_profile_edit_postal'))->addFilter($filterTrim);

	
		$cell = new Zend_Form_Element_Text('mobile');
		$cell->setLabel($translator->translate('form_mobile_profile_edit_mobileNumber'))
		->setRequired(true)
		->addFilter($filterTrim);


		$homepage = new Zend_Form_Element_Text('web');
		$homepage->setLabel($translator->translate('form_mobile_profile_edit_website'))->addFilter($filterTrim);

		$skype = new Zend_Form_Element_Text('skype');
		$skype->setLabel($translator->translate('form_mobile_profile_edit_skype'))->addFilter($filterTrim);

		$save = new Zend_Form_Element_Submit('submit');
		$save->setLabel($translator->translate('form_mobile_profile_edit_submit'));

		 
		//$this->addElement($personalDescription);
		$this->addElement($pField);

		$this->addElement($firstName);
		$this->addElement($lastName);
		$this->addElement($language1);
		$this->addElement($language2);
		$this->addElement($company);
		$this->addElement($position);
		$this->addElement($country);
		$this->addElement($timeZones);
		$this->addElement($address1);
		$this->addElement($address2);
		$this->addElement($city);
		$this->addElement($state);
		$this->addElement($zip);
		//$this->addElement($tel);
		$this->addElement($cell);
		$this->addElement($homepage);
		$this->addElement($skype);
		//$this->addElement($interest);
		//$this->addElement($resume);


		$this->addDisplayGroup(
		array(	'first_name',
																		'last_name', 
																		'username',
																		'language1', 
																		'language2',
																		'company',
																		'position',
																		'idcountry',
																		'time_zone',
																		'address1',
																		'address2',
																		'city',
																		'state',
																		'zip',
																		'tel',
																		'mobile',
																		'web',
																		'skype'),
														'General Profile Settings',
		array('legend' => $translator->translate('form_mobile_profile_edit_generalProfileSettings'))
		);

		
	/*$validatorNotEmpty = new Zend_Validate_NotEmpty();
	$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));

	
	$notifyEmail = new Zend_Form_Element_MultiCheckbox('notify_email');
	$notifyEmail->setLabel('By Email')
				->setAttrib('data-theme', 'm')
				->removeDecorator('HtmlTag')
				->removeDecorator('DtDdWrapper');
	$notifyEmail->setDescription('What type of messages do you wish to receive on email?');
	$notifyEmail->addMultiOptions(array(1 => 'Everytime a project is submitted that I qualify for.',2 => 'Daily digest of emails.',3 => 'When a message has been posted on a project that I\'m working on.',4 => 'When a survey is available.',5 => 'Mob4Hire Newsletters.'));
	$notifyEmail->setValue(array(1,2,3,4,5));
	
	$notifySMS = new Zend_Form_Element_MultiCheckbox('notify_sms');
	$notifySMS->setLabel('By SMS')->setAttrib('data-theme', 'm');
	$notifySMS->setDescription('What type of messages do you wish to receive on SMS?');
	$notifySMS->addMultiOptions(array(1 => 'Everytime a project is submitted that I qualify for.',2 => 'When a message has been posted on a project that I\'m working on.',3 => 'When a survey is available.',4 => 'As a final reminder for things I should be taking care of!'));
	
	$this->addElement($notifyEmail);
	$this->addElement($notifySMS);
	
	$this->addDisplayGroup(array('notify_email', 'notify_sms'),
                                       'notifications',
                                       array('legend' => 'Notifications','data-role'=>'controlgroup'));
	
	// begin Heading
	$devicesHeading = new My_Form_Element_myXhtml('devicesHeading');
	$devicesHeading_str = '<span class="mobblack">';
	$devicesHeading_str .= '<h3>MANDATORY</h3>In order to apply for and work on projects in Mob4Hire, you must have at least ONE device identified. That\'s how we connect you with the mobile developers and market researchers who need your help. <br /><br />Below are your current devices. You can add as many as you wish. If you have more than 10 handsets, please <a href="mailto:support@mob4hire.com?subject=I%20would%20like%20to%20become%20a%20MobPro!"><u>contact us</u></a> to become a MobPro!</span><br/>';
	$devicesHeading->setContent($devicesHeading_str);
	$this->addElement($devicesHeading);
	
	
	$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element clearfix">';
		$heading1_str .= '<div class="form_label"><label>3. Manually identify your handset</label>';
		$heading1_str .= '<div class="form_description">';
		$heading1_str .= '<p>If we can\'t identify your phone, try to find it yourself in our database.</p>';
		$heading1_str .= '</div></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>Select the network carriers on which you want to test. Add as many as you want.</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);

		$countries=new Countries();
		//$countrylist=array_merge(array(0=>'Any'),$countries->listCountries()); 
		$countrylist=$countries->listCountries(); 
		
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country')
				->setAttrib('data-theme', 'm');
		foreach($countrylist as $id=>$countryname) {
				$country->addMultiOption(substr($id,7), $countryname);
		}

		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->setAttrib('data-theme', 'm')
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically


		$devices=new Devices();	
		$vendors=$devices->listVendors();
		
		$manufacturer = new Zend_Form_Element_Select('vendor');	
		$manufacturer->setLabel('Handset')
				->setAttrib('data-theme', 'm')
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically


		//$modelList = $devices->listModels(current($vendors));	
		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->setAttrib('data-theme', 'm')
		//		->addMultiOptions($modelList)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addmodel = new Zend_Form_Element_Button('addmodel');
		$addmodel->setLabel('    Add    ')
				->setAttrib('data-theme', 'm');
		
		$endbtn=new My_Form_Element_myXhtml('end_button');
		$endbtn->setContent('</div></div>');
		
		
		$handsettable=new My_Form_Element_myXhtml('ajaxdiv');
		$handsettable->setContent('<div id="ajaxtable"></div>');
		
		$devicesHeading2 = new My_Form_Element_myXhtml('devicesHeading2');
		$devicesHeading2_str = '<br /><em>Manually entered handsets show with an asterisk to the right and are to be reviewed by the Mob4Hire Staff.</em><br /><br /><span class="mobblack">';
		$devicesHeading2_str .= '<p><h2 class="profile-mobster">Pick one of these four ways to identify and add a device to your account.</h2></p></span>';
		$devicesHeading2->setContent($devicesHeading2_str);
		$this->addElement($devicesHeading2);
			
		
		$step1 = new My_Form_Element_myXhtml('step1');
		$step1_str = '	<div class="form_element clearfix">';
		$step1_str .='		<div class="form_label"><label>1. Mobile website</label>';
		$step1_str .=' 		<div class="form_description">';
		$step1_str .= '			<p>Login to the mobile website using the same Username and Password you use to login to this website.</p>';
		$step1_str .= '		</div>';
		$step1_str .='		</div>';
		$step1_str .= '	<div class="form_helptip">';
		$step1_str .= '		<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$step1_str .= '			<p style="display: none;">We work really hard to ensure the device database is always up to date with the latest devices. In some instances we won\'t have your device. By clicking the SMS link and identifying your device we can improve the database for everyone</p>';
		$step1_str .= '		</span>';
		$step1_str .= '	</div>';
		$step1_str .= '	<div class="form_input">On your mobile handset, open the web browser and surf to: <span class="mobred">http://m.mob4hire.com</span> Then, click login ... once you enter your username and password, we\'ll identify your handset. DO NOT USE WIFI TO DO THIS. You have to be using your wireless network operator.'.
						'</div>';	
		$step1_str .= '</div>';

		$step1->setContent($step1_str);
		$this->addElement($step1);

		
		$manual = new My_Form_Element_myXhtml('manualsms');
		$manual_str = '<div class="form_element clearfix">';
		$manual_str .= '	<div class="form_label"><label>2. Send an SMS to your Phone</label>';
		$manual_str .=' 		<div class="form_description">';
		$manual_str .= '			<p>Enter your mobile number here, including international area codes, and we can send you an sms to identify your device.</p>';
		$manual_str .= '		</div>'.
					'	</div>';
		$manual_str .= '	<div class="form_helptip">';
		$manual_str .= '		<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$manual_str .= '			<p style="display: none;">Use this method if you cannot find your phone on the list. By entering your mobile phone number, we will send you an sms to your device to identify it with our system. Devices will automatically be added into your account. The following are examples of international phone numbers, with area code: N. America: 13035556666. UK: 447737013261.</p>';
		$manual_str .= '		</span>';
		$manual_str .= '	</div>';
		$manual_str .= '	<div class="form_input">';		
		$manual_str .= '		<input type="text" value="" id="manualsms" name="manualsms">';
		$manual_str .= '		<button type="button" id="sendbutton" name="sendbutton">Send SMS</button>';
		$manual_str .= '		<br/><div id="smsresult">'.
						'</div>';
		$manual_str .= '</div>';
				
		$manual->setContent($manual_str);
		
		$heading2 = new My_Form_Element_myXhtml('heading2');
		$heading2_str = '<div class="form_element clearfix">';
		$heading2_str .= '<div class="form_label"><label>4. Can\'t find your handset?</label>';
		$heading2_str .= '<div class="form_description">';
		$heading2_str .= '<p>Sorry about that! Just enter this information, and we will make sure it gets included.</p>';
		$heading2_str .= '</div></div>';
		$heading2_str .= '<div class="form_helptip">';
		$heading2_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading2_str .= '<p>Select the network carriers on which you want to test. Add as many as you want.</p>';
		$heading2_str .= '</span>';
		$heading2_str .= '</div>';
		$heading2_str .= '<div class="form_input">';
		$heading2->setContent($heading2_str);
		
		$country2 = new Zend_Form_Element_Select('country2');
		$country2->setLabel('Country')
				->setAttrib('data-theme', 'm');
		foreach($countrylist as $id=>$countryname) {
				$country2->addMultiOption(substr($id,7), $countryname);
		}
		
		$carrier2 = new Zend_Form_Element_Select('network2');
		$carrier2->setLabel('Network')
				->setAttrib('data-theme', 'm')
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$manualHandset = new Zend_Form_Element_Text('manualHandset');
		$manualHandset->addFilter($filterTrim)->setLabel('Handset')->setAttrib('data-theme', 'm');
		$manualModel = new Zend_Form_Element_Text('manualModel');
		$manualModel->addFilter($filterTrim)->setLabel('Model')->setAttrib('data-theme', 'm');
				
		$addmodel2 = new Zend_Form_Element_Button('addmodel2');
		$addmodel2->setLabel('    Add    ')
				->setAttrib('data-theme', 'm');
		
		$endbtn2=new My_Form_Element_myXhtml('end_button2');
		$endbtn2->setContent('</div></div>');
		
		
		$this->addElement($handsettable);
		$this->addElement($heading1);
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($heading2);
		$this->addElement($country2);
		$this->addElement($carrier2);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		$this->addElement($addmodel2);
		$this->addElement($manualHandset);
		$this->addElement($manualModel);
		$this->addElement($endbtn);
		$this->addElement($endbtn2);
		
		$this->addElement($manual);
		
		$this->addDisplayGroup(array('devicesHeading','ajaxdiv','devicesHeading2','step1'),
                                       'Top',
                                       array('legend' => 'Add Devices To Your Account'));
		
		$this->addDisplayGroup(array('manualsms'),
				       'sms',
				       array('legend' => ''));
		
		$this->addDisplayGroup(array('heading1', 'country', 'network','vendor','model', 'platform', 'addmodel','end_button'),
                                       'Add Devices To Your Account',
                                       array('legend' => ''));
		
		$this->addDisplayGroup(array('heading2', 'country2', 'network2','manualHandset','manualModel','addmodel2','end_button2'),
				       'cant find it',
                                       array('legend' => ''));
		
	


	
	$browser = new Zend_Form_Element_MultiCheckbox('browser');
	$browser->setLabel('What type of web browser do you have on your computer?')->setAttrib('data-theme', 'm');
	$browser->setAttrib('data-role', 'controlgroup');
	$browser->setDescription('Do you wish to get paid to test projects and surveys?');
	$browser->addMultiOptions(array(1 => 'I don\'t have a computer with an internet connection.',2 => 'Internet Explorer',3 => 'Opera',4 => 'Firefox',5 => 'Safari',6 => 'Chrome',7 => 'My browser is not listed.'));
	$browser->setvalue(1);
	
	$testType = new Zend_Form_Element_MultiCheckbox('test_type');
	$testType->addMultiOptions(array('Functional'=>'Functional', 'Usability'=>'Usability'))
			->setAttrib('data-theme', 'm')
			->setRequired(true)
			->setLabel('Type of projects you\'d like to work on')
			->setDescription('Select one or both');
	$testType->setValue(1);
	
		
	$apps = new Zend_Form_Element_MultiCheckbox('apps');
	$apps->setLabel('Test Categories')->setAttrib('data-theme', 'm');
	$apps->setDescription('Select them all if you want to.');
	$apps->addMultiOptions(array(1 => 'News',2 => 'Medical',3 => 'Reference',4 => 'Productivity',5 => 'Navigation',6 => 'Health and Fitness',7 => 'Education',8 => 'Weather',9 => 'Business',10 => 'Music',11 => 'Finance',12 => 'Sports',13 => 'Travel',14 => 'Utilities',15 => 'Games',16 => 'Social Networking',17 => 'Books',18 => 'Entertainment',19 => 'Lifestyle'));
	

	$this->addElement($browser);
	$this->addElement($testType);
	$this->addElement($apps);

	$hack=new My_Form_Element_myXhtml('hack'); // fix for IE7
	$hack->setContent("<br/><br/><br/>");
	$this->addElement($hack);
	
	$this->addDisplayGroup(array('handsetPicker','browser','test_type','apps'),
                                       'Devices And Technologies',
                                       array('legend' => 'Devices And Technologies'));
	
	$experience = new My_Form_Element_TinyMce('experience');
		$experience->addFilter($filterTrim)
				// ->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				 ->setAttrib('data-theme', 'm')
				 ->setRequired(true)
				 ->setLabel('Experience Description')
				 ->setAttrib('rows', '7')
				 ->setAttrib('cols', '50')
				 ->setDescription('Your areas of expertise ... a small resume to apply for the job.');
					 
	
			
	$education = new Zend_Form_Element_Select('education');
	$education->setLabel('Highest Level Of Education')->setAttrib('data-theme', 'm'); 
	$education->addMultiOptions(array(1 => 'Less than high school.',2 => 'High school graduate.',3 => 'Some college or university.',4 => 'College graduate with a 2 year degree.',5 => 'College graduate with a 4 year degree.',6 => 'Advanced degree.'));
	
	$previousTesting = new Zend_Form_Element_Select('previous_testing');
	$previousTesting->setLabel('Have You Tested Software Before?')->setAttrib('data-theme', 'm');
	$previousTesting->addMultiOptions(array(1 => 'Not really.',2 => 'A little.',3 => 'Lots.',4 => 'I used to work in Q/A.',5 => 'I work in Q/A right now.'));
	
	$nda = new Zend_Form_Element_Radio('nda');
	$nda->setLabel('Would you consider signing an NDA when doing a project?')->setAttrib('data-theme', 'm');
	$nda->setDescription('Some projects require a non-disclosure agreement to be signed.');
	$nda->addMultiOptions(array(1 => 'Yes', 0 => 'No'));
	$nda->setvalue(1);
	
	$this->addElement($experience);
	$this->addElement($education);
	$this->addElement($previousTesting);
	$this->addElement($nda);
	
	$this->addDisplayGroup(array('experience', 'education','previous_testing','nda'),
                                       'skills and experience',
                                       array('legend' => 'Skills And Experience'));
	
	$whoSummary = new My_Form_Element_myXhtml('who_summary');
	$whoSummary->setContent('These demographics are used when developers or market researchers want to target a "focus group" ... i.e. someone who is part of their potential customer base they want to learn more about regarding use of their app, product or service.<br />The more you complete, the more you\'ll be selected for highly valuable and targeted surveys.<br />This information is PRIVATE. It can never be used to individually identify you. We don\'t share it one a 1:1 basis with anyone.');
	
	$gender = new Zend_Form_Element_Select('gender');
	$gender->setLabel('Gender')->setAttrib('data-theme', 'm');
	$gender->addMultiOptions(array(1 => 'Rather not say.',2 => 'Male',3 => 'Female'));

	
	$bdate =  new My_Form_Element_DateSelects('bdate');
        $validatorDate = new Zend_Validate_Date();
        $validatorDate->setMessages(
            array(
                Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
    $bdate->setLabel('Date Of Birth')->addValidator($validatorDate)->setAttrib('data-theme', 'm');
	$bdate->helpText='In addition to gender, age is one of the most requested attributes in a survey. Make sure you fill this one out.';
    $bdate->setStartEndYear(1933, date("Y"))->setReverseYears(true);
	
	$maritalStatus = new Zend_Form_Element_Select('martial_status'); // Yes, I know. The same typo exists in the database
	$maritalStatus->setLabel('Marital Status')->setAttrib('data-theme', 'm');
	$maritalStatus->addMultiOptions(array(1 => 'Rather not say.',2 => 'Single / Never Married',3 => 'Married',4 => 'Divorced',5 => 'Widowed.',6 => 'Separated.'));
	
	$children = new Zend_Form_Element_Select('children');
	$children->setLabel('How many children under the age of 18 live at home?')->setAttrib('data-theme', 'm');
	$children->addMultiOptions(array(1 => 'Rather not say',2 => '0',3 => '1',4 => '2',5 => '3',6 => '4',7 => '5 or more.'));
	
	$annualIncome = new Zend_Form_Element_Select('annual_income');
	$annualIncome->setLabel('What is your total annual household income?')->setAttrib('data-theme', 'm');
	$annualIncome->addMultiOptions(array(1 => 'Rather not say',2 => '$0 - $10,000',3 => '$10,001 - $23,000',4 => '$23,001 - $46,000',5 => '$46,001 - $69,000',6 => '$69,001 - $115,000',7 => '$115,001 - $161,000',8 => '$161,001 or more'));
				 
	
	$this->addElement($whoSummary);
	$this->addElement($gender);
	$this->addElement($bdate);
	$this->addElement($maritalStatus);
	$this->addElement($children);
	$this->addElement($annualIncome);
	
	$this->addDisplayGroup(array('who_summary','gender', 'bdate','martial_status','children','annual_income'),
                                       'who are you',
                                       array('legend' => 'Who Are You?'));
	
	$mobLife = new Zend_Form_Element_Select('mob_life');
	$mobLife->setLabel('Describe your mobile lifestyle.')->setAttrib('data-theme', 'm');
	$mobLife->addMultiOptions(array(1 => 'Rather not say',2 => 'Rarely use my mobile handset.',3 => 'It\'s part of my day.',4 => 'I get nervous when I don\'t have my phone.',5 => 'I love mobile!',6 => 'I\'m the first person to try everything new.',7 => 'It\'s an integral part of both my job and my home life.'));
	
	$phoneUse = new Zend_Form_Element_MultiCheckbox('phone_use');
	$phoneUse->setLabel('Other than talking ...')->setAttrib('data-theme', 'm');
	$phoneUse->setDescription('... what other activities do you do on your mobile phone?');
	$phoneUse->addMultiOptions(array(1 => 'Web browsing',2 => 'Game playing',3 => 'Email',4 => 'Instant messaging',5 => 'Photo/photo sharing',6 => 'Music and Video Stream',7 => 'Text messaging/SMS',8 => 'MMS messaging',9 => 'Photography',10 => 'Check weather',11 => 'Check time (clock)',12 => 'Navigation / maps',13 => 'Video calls',14 => 'Watch television',15 => 'Social networking like Facebook',16 => 'Books',17 => 'Organizing a calendar',18 => 'Other (please specify)'));
	$phoneUseOther = new Zend_Form_Element_Text('phone_use_other');
	$phoneUseOther->addFilter($filterTrim)->setLabel('&nbsp;')->setAttrib('data-theme', 'm');
	
	$this->addElement($mobLife);
	$this->addElement($phoneUse);
	$this->addElement($phoneUseOther);
	
	$this->addDisplayGroup(array('mob_life','phone_use','phone_use_other'),
                                       'mob life',
                                       array('legend' => 'Mob Life'));
	
                                       
                                       */

		

	$this->addElement($save);

	}
}
