<?php
class Form_ProfileContactInfo extends My_Form
{
	public function init()
	{
		$translator = $this->getTranslator();
		$this->setAction('/app/profile/index')->setMethod('post')->setAttrib('id','frmProfileContactInfo');
		$filterTrim = new Zend_Filter_StringTrim();
		
		//set radio buttons for which state ment best describes you
		$personalDescription = new Zend_Form_Element_Radio('personal_description');
		$personalDescription->setLabel($translator->translate('form_profileContactInfo_personal_description'))->setDecorators(array('MobForm'));
		$personalDescription->helpText=$translator->translate('form_profileContactInfo_personal_descriptionHelp');
		$personalDescription->addMultiOptions(array(1 => $translator->translate('form_profileContactInfo_personal_descriptionOpt1') ,2 => $translator->translate('form_profileContactInfo_personal_descriptionOpt2'),3 => $translator->translate('form_profileContactInfo_personal_descriptionOpt3'),4 => $translator->translate('form_profileContactInfo_personal_descriptionOpt4'),5 => $translator->translate('form_profileContactInfo_personal_descriptionOpt5'),6 => $translator->translate('form_profileContactInfo_personal_descriptionOpt6'),7 => $translator->translate('form_profileContactInfo_personal_descriptionOpt7'),8 => $translator->translate('form_profileContactInfo_personal_descriptionOpt8'),9 => $translator->translate('form_profileContactInfo_personal_descriptionOpt9'),10 => $translator->translate('form_profileContactInfo_personal_descriptionOpt10')));
		$personalDescription->setvalue(1);
		$personalDescription->setRequired(true);
		
		$pField = new Zend_Form_Element_Text('personal_description_other');
		$pField->setLabel('&nbsp;')->addFilter($filterTrim)->setDecorators(array('MobForm'));
		
		$firstName = new Zend_Form_Element_Text('first_name');
		$firstName->setLabel($translator->translate('form_profileContactInfo_firstName'))->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setRequired(true);
		$firstName->helpText = $translator->translate('form_profileContactInfo_firstNameHelp');
   		
    
		$lastName = new Zend_Form_Element_Text('last_name');        
		$lastName->setLabel($translator->translate('form_profileContactInfo_lastName'))->addFilter($filterTrim)
				->setRequired(true)
				->setDecorators(array('MobForm'));
		$lastName->helpText = $translator->translate('form_profileContactInfo_lastNameHelp');
   
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel($translator->translate('form_profileContactInfo_email'))
			->setDecorators(array('MobForm'))
			->setRequired(true);
		$email->helpText =$translator->translate('form_profileContactInfo_emailHelp');
		
		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' appears to be an IP address, but IP addresses are not allowed"),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' appears to be a DNS hostname but cannot match TLD against known list"),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' appears to be a DNS hostname but contains a dash (-) in an invalid position"),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' appears to be a DNS hostname but cannot match against hostname schema for TLD '%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' appears to be a DNS hostname but cannot extract TLD part"),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' does not match the expected structure for a DNS hostname"),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' does not appear to be a valid local network name"),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' appears to be a local network name but local network names are not allowed")
			)
		);
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));
		
		$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' is not a valid email address"),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' is not a valid hostname for email address '%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' does not appear to have a valid MX record for the email address '%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' not matched against dot-atom format"),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' not matched against quoted-string format"),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' is not a valid local part for email address '%value%'")
			)
		);
		
		$users = new Users();
		$validatorUniqueEmail = new My_Validate_DbUnique($users, 'email', Zend_Registry::get('defSession')->currentUser->id);
		$validatorUniqueEmail->setMessage(__('This email address is already registered, please choose another one.'));
		$email->addValidator($validatorNotEmpty, true)->setRequired(true)
			->addFilter($filterTrim)
			->addValidator($validatorEmail)
			->addValidator($validatorUniqueEmail)
			->setDecorators(array('MobForm'))
			->addDecorator(array('ajaxDiv' => 'HtmlTag'), array('tag'=>'div', 'placement'=>'append', 'id'=>'email_help', 'class'=>'errors'));

		$language1 = new Zend_Form_Element_Select('language1');
		$language1->setLabel($translator->translate('form_profileContactInfo_language1'))
			->setRequired(true)
			->setDecorators(array('MobForm'));
		$language1->helpText=$translator->translate('form_profileContactInfo_language1Help');
	
		$languagesList = new Languages();//get Languages
		$language1->addMultiOptions($languagesList->listLanguages());
	
		
		$language2 = new Zend_Form_Element_Select('language2');
		$language2->setLabel($translator->translate('form_profileContactInfo_language2'))
			->setDecorators(array('MobForm'));
		$language2->helpText=$translator->translate('form_profileContactInfo_language2Help');
		$language2->addMultiOptions($languagesList->listLanguages());
		
		
		$company = new Zend_Form_Element_Text('company');
		$company->setLabel($translator->translate('form_profileContactInfo_company'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$company->helpText=$translator->translate('form_profileContactInfo_companyHelp');
		
		$position = new Zend_Form_Element_Text('position');        
		$position->setLabel($translator->translate('form_profileContactInfo_position'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$position->helpText=$translator->translate('form_profileContactInfo_positionHelp');

		$country = new Zend_Form_Element_Select('idcountry');
		$country->setLabel($translator->translate('form_profileContactInfo_country'))->setDecorators(array('MobForm'))
				->setRequired(true);
		$country->helpText=$translator->translate('form_profileContactInfo_countryHelp');
		$countryLst = new Countries();//get countries from Countries model
		$countrylist=$countryLst->listCountries();
		foreach($countrylist as $id=>$countryname) {
				$country->addMultiOption(substr($id,7), $countryname);
		}
		
		$timeZones = new Zend_Form_Element_Select('time_zone');
		$timeZones->setLabel($translator->translate('form_profileContactInfo_timeZone'))
				->setDecorators(array('MobForm'))
				->setRequired(true);
				
		$timeZones->helpText=$translator->translate('form_profileContactInfo_timeZoneHelp');	
		$timeZoneList = ProfileContactInfo::$timeZoneList;
		
		$timeZones->addMultiOptions($timeZoneList)
				   ->setvalue("");
		


		$address1 = new Zend_Form_Element_Text('address1');
		$address1->setLabel($translator->translate('form_profileContactInfo_address1'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$address1->helpText=$translator->translate('form_profileContactInfo_address1Help');
		
		$address2 = new Zend_Form_Element_Text('address2');
		$address2->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$address2->setLabel('&nbsp');
		$address2->helpText=$translator->translate('form_profileContactInfo_address2Help');

		$city = new Zend_Form_Element_Text('city');
		$city->setLabel($translator->translate('form_profileContactInfo_city'))->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setRequired(true);
		$city->helpText=$translator->translate('form_profileContactInfo_cityHelp');
		
		$state = new Zend_Form_Element_Text('state');
		$state->setLabel($translator->translate('form_profileContactInfo_state'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$state->helpText=$translator->translate('form_profileContactInfo_stateHelp');

		$zip = new Zend_Form_Element_Text('zip');
		$zip->setLabel($translator->translate('form_profileContactInfo_zip'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$zip->helpText=$translator->translate('form_profileContactInfo_zipHelp');
		
		//$tel = new Zend_Form_Element_Text('tel');
		//$tel->setLabel('Main Landline Telephone')->addFilter($filterTrim)->setDecorators(array('MobForm'));
		//$tel->helpText='If you don\'t have a main landline telephone number, just leave this field blank.';
		//
		$cell = new Zend_Form_Element_Text('mobile');
		$cell->setLabel($translator->translate('form_profileContactInfo_mobile'))
			->setRequired(true)
			->addFilter($filterTrim)
			->setDecorators(array('MobForm'));
		$cell->helpText=$translator->translate('form_profileContactInfo_mobileHelp');
		
		
		$homepage = new Zend_Form_Element_Text('web');
		$homepage->setLabel($translator->translate('form_profileContactInfo_web'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$homepage->helpText=$translator->translate('form_profileContactInfo_webHelp');
		
		$skype = new Zend_Form_Element_Text('skype');
		$skype->setLabel($translator->translate('form_profileContactInfo_twitter'))->addFilter($filterTrim)->setDecorators(array('MobForm'));
		$skype->helpText=$translator->translate('form_profileContactInfo_twitterHelp');
    
		//$interest = new Zend_Form_Element_Select('areaofinterest');
		//$interest->setLabel('Area of Interest');
		//$interest->addMultiOptions(array('games' => 'Games','messaging' => 'Messaging','music' => 'Music'))->setDecorators(array('MobForm'));
    
		//$resume =  new Zend_Form_Element_Textarea('resume');
		//$resume->setLabel('Area of Expertise / Resume')->setDecorators(array('MobForm'));
    
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$cancel = new Zend_Form_Element_Submit('cancel');
		$cancel->setDecorators($this->submitDecorators);
		$cancel->setAttrib('class', 'text_button');
		$cancel->setLabel($translator->translate('form_profileContactInfo_cancel'));


		$save = new Zend_Form_Element_Submit('submitbtn');
		$save->setAttrib('class', 'text_button_red');
		$save->setLabel($translator->translate('form_profileContactInfo_save'));
				
		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
    
		$this->addElement($personalDescription);
		$this->addElement($pField);
		
		$this->addElement($firstName);
		$this->addElement($lastName);
		$this->addElement($email);		
		$this->addElement($language1);
		$this->addElement($language2);
		$this->addElement($company);
		$this->addElement($position);
		$this->addElement($country);
		$this->addElement($timeZones);
		$this->addElement($address1);
		$this->addElement($address2);
		$this->addElement($city);
		$this->addElement($state);
		$this->addElement($zip);
		//$this->addElement($tel);
		$this->addElement($cell);
		$this->addElement($homepage);
		$this->addElement($skype);
		//$this->addElement($interest);
		//$this->addElement($resume);
		

		/*$this->addDisplayGroup(array('telephone', 'idcountry','city', 'postcode', 'areaofinterest', 'resume' , 'website'),
                                       'Contact & Additional Information',
                                       array('legend' => 'Contact & Additional Information'));*/
		
		//$this->addElement($gNotify);
		/*$this->addDisplayGroup(array('notify' ),
                                       'General Notiification Setting',
                                       array('legend' => 'General Notiification Setting'));*/
		
		
		$this->addDisplayGroup(
														array(	'first_name',
																		'last_name', 
																		'email', 
																		'username',								
																		'language1', 
																		'language2',
																		'company',
																		'position',
																		'idcountry',
																		'time_zone',
																		'address1',
																		'address2',
																		'city',
																		'state',
																		'zip',
																		'tel',
																		'mobile',
																		'web',
																		'skype'),
														'General Profile Settings',
														array('legend' => $translator->translate('form_profileContactInfo_legendProfileSettings')
)
													);
		
	
		$this->addElement($start_row);
		$this->addElement($save);
		//$this->addElement($cancel);
		$this->addElement($end_row);
	}
}
