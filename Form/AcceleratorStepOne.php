<?php
class Form_AcceleratorStepOne extends My_Form
{
    public function init()
    {
        $filterTrim = new Zend_Filter_StringTrim();
        
		$start_div_form_element = '<div class="form_element clearfix">';
		$start_div_helptip = '<div class="form_helptip">';
		$close_div = '</div>';

		$start_form_label = '<div class="form_label"><label>';
		
		
		$end_form_label = '</label></div>';

		$start_span_whats_this = '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>';
		$close_span = '</p></span>';

	
		$start_table_label = $start_div_form_element . $start_form_label;
		$helptip = $end_form_label. $start_div_helptip. $start_span_whats_this;
		    
		$surveyReview = new My_Form_Element_myXhtml('review');
		$projReview_str = $start_table_label;
		$projReview_str .='Sample Survey';
		$projReview_str .='<div class="form_description">Review the survey by clicking on the link to the right. Some values will be populated automatically in your survey and may appear blank in the demonstration</div>';
		$projReview_str .= $helptip;
		$projReview_str .='We hope you have a great experience with the Mob4hire community.';
		$projReview_str .= $close_span. $close_div;
		$projReview_str .='<div class="form_input"><a href="http://survey.mob4hire.com/index.php?sid=73225&lang=en" target="_blank">Mob4Hire MobAccelerator Demonstration Survey</a></div>';
		$projReview_str .=  $close_div;
		$surveyReview->setContent($projReview_str);
		
		
		
        $svyPkg = new Zend_Form_Element_Radio('svyPkg');
		$svyPkg->addMultiOptions(array(1=>'Basic ', 2=>'Premium ', 3=>'Managed'));
		$svyPkg->setLabel('<span class="mobblack">MOB</span><span class="mobred">ACCELERATOR</span>');
		$svyPkg->setDecorators(array('MobForm'));	
		$svyPkg->setDescription('Choose a package that suits your needs.');
		$svyPkg->setValue(1)->setRequired(true);
		$svyPkg->helpText = '<img src="/images/banner-product-info-accelerator.gif"/>';
        
		$lineBreak = new My_Form_Element_myXhtml('lineBreak');
		$lineBreak->setContent('<hr />');
		
		$inviteType = new Zend_Form_Element_Radio('inviteType');
		$inviteType->addMultiOptions(array(0=>'Open (anybody that qualifies)', 1=>'Closed to specific mobsters I\'ll select. (Requires Premium or Managed Project)'))
					->setDecorators(array('MobForm'))
					->setLabel('Type Of Invitation')
					->setDescription('Who gets to participate in your project? (Select a premium or managed project to be able to choose \"Closed\")');
		$inviteType->setValue(0)->setRequired(true);
		$inviteType->helpText="An Open invitation means that anybody who qualifies for the project can apply to participate. To \"qualify,\" it means they must match the criteria you select in Step 2: Panel Selection. A Closed invitation assumes you\'ve built a list of Mobsters in your Contact list ... those are the people you can hand-pick to do the test. Closed Invitations are useful for regression testing, or if you\'ve built trust and rapport with Mobsters who have done work for you before , or maybe you want to ask them different questions.";
		
		$floatValidator = new Zend_Validate_Float();
		$floatValidator->setMessage("This must be a valid amount");
		
		$svyAmount = new Zend_Form_Element_Text('svyAmount');
		$svyAmount->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel('Fee Per Respondent ($USD)')
				->setDescription('As a guideline, it takes about 1/2 hour to complete the MOBACCELERATOR survey, and potentially an hour to investigate the app store, download, and install. So, you\'ll be paying your tester for 1.5 hours of work ... this could go from $20 all the way to $300. Make sure you include the cost of your app, if it\'s paid')
				->setRequired(true)
				->addValidator($floatValidator);
		$svyAmount->addFilter($filterTrim);
		$svyAmount->helpText='This is the amount you are willing to pay for each completed survey. When offering this amount, in addition to the time it takes to complete the survey, think about the amount of time it will take the respondent to do all the tasks required (such as trying a mobile app or mobile website) and the data the mobile user may incur. You should also take into consideration the jurisdiction and target demographic you\'re running the survey when setting your price. Obviously, if the only task is just to complete a survey, it will cost you less than if it\'s "Try this app for seven days and then take a survey." The higher the price, the higher the chances you\'ll get good completes.';
		
		$lineBreak2 = new My_Form_Element_myXhtml('linebreak2');
		$lineBreak2->setContent('<hr />');
		
		
		$projName = new Zend_Form_Element_Text('projName');
		$projName->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Survey Name')
				 ->setAttrib('size', '50')
				 ->setRequired(true)
				 ->setDescription('Choose a unique project name.');			 
		$projName->helpText = 'The name of your project is how it\'s identified throughout the system. It cannot be changed once your project goes live. Use your app name, your company name, the phase of the moon of whatever it needs to be to attract Mobster\'s attention.';
        
		$projImg = new My_Form_Element_ImageUpload('projImg');
		$projImg->setDecorators(array('MobForm'))
				->setLabel('Survey Image')
				->setDescription('Upload an image that will be associated with the survey. (100x100 .png, .jpg and .gif only)');
		$projImg->helpText='Uploading an image to a survey helps to market it. You can upload files of the following types: jpg, jpeg, png and gif. Maximum file size for each file is 300Kb. The dimensions for the graphic are 120 x 120.';
		$projImg->setRequired(false);
		
		
		
		$projDesc = new Zend_Form_Element_Text('projDesc');
		$projDesc->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setRequired(true)
				 ->setLabel('Short Description')
				 ->setAttrib('size', '50')
				 ->setAttrib('maxLength', 140)
				 ->setDescription('The elevator pitch of your project.');
		$projDesc->helpText='A short description of your project. It should be compelling, descriptive and to the point. Make it short and snappy; it\'s used for project listings and Twitter / SMS promotions.';				 
				 
				 
		$projDescFull = new My_Form_Element_TinyMce('$projDetail');
		$projDescFull->addFilter($filterTrim)
				//	->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setDecorators(array('MobForm'))
					->setRequired(true)
					->setLabel('Description')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50')
					->setDescription('Give enough information to describe the survey so someone can decide whether they want to participate.');
		$projDescFull->helpText='This description is shown publicly to anybody that wants to read it. It\'s an overview of your project, including what you\'re trying to accomplish and a summary of what you want the Mobster to do. This ISN\'T your full survey script ... you\'ll take care of that in step 3 "Resources".   Does your project description have enough information AND is it compelling enough for Mobsters to want to complete this project? Obviously, if the survey is just a survey (without installing an app or running a mobile website), then these instructions will be pretty darn short.';
        
		$tags = new Zend_Form_Element_Text('tags');
		$tags->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setRequired(false)
			->setLabel('Tags')
			->setAttrib('size', '50')
			->setDescription('Enter tags separated by commas.');
		$tags->helpText='Tags are useful for Mobsters to find your project through search results.';
		
		$lineBreak3 = new My_Form_Element_myXhtml('linebreak3');
		$lineBreak3->setContent('<hr />');
		
		$validatorDate = new Zend_Validate_Date();
		$validatorDate->setMessages(array(Zend_Validate_Date::INVALID=>__("'%value%' does not appear to be a valid date"), Zend_Validate_Date::FALSEFORMAT=>__("'%value%' does not fit given date format")));
		
		
		$sDate = new ZendX_JQuery_Form_Element_DatePicker('sDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$sDate->addValidator($validatorDate)
			// ->setDecorators($this->jQueryDecorators)
			->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
						)
			  ->setRequired(true)
			  ->setLabel('Listing Start Date')
			  ->setAttrib("readonly","readonly")
			  ->setDescription("When do you want the project to start?");
		$sDate->setValue(date("Y-m-d"));	
		$sDate->helpText='This is the date the project will start being displayed to Mobsters. If you want to start right away and set it to today’s date (or before), please note that Mob4Hire will still need to approve the project before it gets listed.';  
	
		/*
		* Project Duration
		*/
		$projDur = new Zend_Form_Element_Text('projDur');
		$projDur->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setRequired(true)
				->setValue('30')
				->addValidator(new Zend_Validate_Int())
				->setLabel('Survey Duration (days)')
				->setDescription('The number of days the Mobsters have to complete the survey.');
		$projDur->helpText="This is the amount of days the survey will remain live. The survey is OVER when this duration expires, or when you get the # of responses you wish (which you pick in step 2).";

		$estTime = new Zend_Form_Element_Text('estTime');
		$estTime->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel('Estimated Time To Complete The Survey')->setRequired(true);
		$estTime->helpText='This is an estimate of the amount of time it will take to complete the survey AND the amount of time it will take the Mobster to complete any other actions before taking the survey (such as installing a mobile app, or reviewing a mobile website).';
			
			

		/*
		* Estimated Data Usage
		*/
		$estData = new Zend_Form_Element_Text('estData');
		$estData->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setLabel('Estimated Data Usage For The Survey')
			->setDescription('How much data (kb) will be used during the test?')->setRequired(false);
		$estData->helpText='This only applies to surveys which require other actions (such as running a mobile app or viewing a mobile website). Since data plans vary SO much from person to person, carrier to carrier and country to country, you need to tell the Mobster how much data they can expect will be transferred during the project.  This helps them estimate how much the data will cost so they can see if it makes sense for them to participate. This includes app download, as well as data Tx and Rx. Examples: "100 Kbytes,"  "2 Mbytes," "More than 5 Mbytes; unlimited data plan is required."  NOTE: If your mobile app is data intensive, and you haven\'t offered enough money for a paid complete, you will find yourself with zero responses!';
		
		$mobInvite = new Zend_Form_Element_Textarea('mobInvite');
		$mobInvite->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel('Mobster Invitation')
				->setOptions(array('cols'=>'50'))
				->setDescription('This is added to the email that is sent to Mobsters who have elected to complete the survey.')->setRequired(false);
		$mobInvite->helpText="In addition to the default text of our Mob4Hire email, this paragraph is added before we send it to Mobsters who have elected to complete the survey.";
		
								
		$this->addElement($svyPkg);
		$this->addElement($surveyReview);
	
		$this->addElement($lineBreak);
		$this->addElement($inviteType);
		$this->addElement($svyAmount);
		$this->addElement($lineBreak2);
		$this->addElement($projName);
		$this->addElement($projImg);
		$this->addElement($projDesc);
		$this->addElement($projDescFull);
		$this->addElement($tags);
		$this->addElement($lineBreak3);
		$this->addElement($sDate);
		$this->addElement($projDur);
		$this->addElement($estTime);
		$this->addElement($estData);
		$this->addElement($mobInvite);
		
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		// TODO this should be mobexperience
		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		
		$this->addElement($start_row);
		//$this->addElement($mobtest_logo);
		$this->addElement($next);
		$this->addElement($end_row);
	
    }    
} //end class

?>