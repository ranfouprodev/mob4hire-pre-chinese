<?php
class Form_ChangePassword extends My_Form
{
	public function init()
	{

		$translator = $this->getTranslator();

		$this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		 $filterTrim = new Zend_Filter_StringTrim();
		 
		
		$validatorIdentical = new My_Validate_PasswordConfirmation('confirmpassword') ;
		$validatorConfirm = new My_Validate_CurrentPasswordConfirmation() ;
		 
				         
		$oldpassword = new Zend_Form_Element_Password('currentpassword');
		$oldpassword->setLabel($translator->translate('form_changePassword_current'))
				->addFilter($filterTrim)
				->setRequired(true)
				->setDecorators(array('MobForm'))
				->addValidator($validatorConfirm)
			    ->addValidator('NotEmpty');
	
	
		$confirmPassword = new Zend_Form_Element_Password('confirmpassword');
		$confirmPassword->setLabel($translator->translate('form_changePassword_confirm'))
						->addFilter($filterTrim)
						->setRequired(true)
				 ->setDecorators(array('MobForm'));
	
		
		
		$password = new Zend_Form_Element_Password('password');
		$password->setLabel($translator->translate('form_changePassword_newPassword'))
				->addFilter($filterTrim)
				->setRequired(true)
				->setDecorators(array('MobForm'))
			    ->addValidator('NotEmpty') 
		        ->addValidator($validatorIdentical);
				 

		if (!isset(Zend_Registry::get('defSession')->resetPass))
			$this->addElement($oldpassword);
			
		$this->addElement($password);
		$this->addElement($confirmPassword);
	 
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$submit = new Zend_Form_Element_Submit('submitBtn');
		$submit->setAttrib('class', 'text_button_red');
		$submit->setLabel($translator->translate('form_button_submit'));

	 
		$this->addElement($start_row);
		$this->addElement($submit);
		$this->addElement($end_row);
		 
		 
		 
		
 	}
}
