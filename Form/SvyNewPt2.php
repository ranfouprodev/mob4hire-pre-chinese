<?php
class Form_SvyNewPt2
{ 
    public $title ="<h3>Survey Details</h3>";
    public $summary ="";
    
    public function getForm(){
        
        $filterTrim = new Zend_Filter_StringTrim();
        
        $form = new Zend_Form;
        $form->setAction('/app/survey/rsrchrnew')->setMethod('post')->setAttrib('id','frmrschrnewPt2');
        
        $name = new Zend_Form_Element_Text('name');        
        $name->setLabel('Survey Name:')->addFilter($filterTrim);
        $name->setDescription('Name displayed for this project');
        $desc =  new Zend_Form_Element_Textarea('description');
        $desc->setLabel('Description:');
        
        
        $sdate =  new My_Form_Element_DateSelects('sdate');
        $validatorDate = new Zend_Validate_Date();
        $validatorDate->setMessages(
            array(
                Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
        $sdate->setLabel('Start Date:')->addValidator($validatorDate);
        $sdate->setStartEndYear(date("Y"), date("Y")+1)->setReverseYears(true);
        
        $edate =  new My_Form_Element_DateSelects('edate');
        $validatorDate = new Zend_Validate_Date();
        $validatorDate->setMessages(
            array(
                Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
        $edate->setLabel('End Date:')->addValidator($validatorDate);
        $edate->setStartEndYear(date("Y"), date("Y")+1)->setReverseYears(true);
 
        
        //pass back unix timestamp
        
        $form->addElement($name);
        $form->addElement($desc);
        $form->addElement($sdate);
        $form->addElement($edate);
        
        return $form;  
    }//end getForm        
} //end class

?>