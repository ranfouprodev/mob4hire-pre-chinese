<?php
class Form_MobileContact extends My_Form
{
	public function init()
	{
		$this->setAction('/mobile/services')
			->setMethod('post')
			->setAttrib('id', 'lcontact');

		$filterTrim = new Zend_Filter_StringTrim(); 
		
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));

		$fullName = new Zend_Form_Element_Text('fullname');        
		$fullName->setLabel('What is your full name?')
				->addFilter($filterTrim)
				->addValidator($validatorNotEmpty)
				->setRequired(true);

		$company = new Zend_Form_Element_Text('company');        
		$company->setLabel('Company')
				->addFilter($filterTrim);

							
		$countries=new Countries();
		$countrylist=$countries->listCountries('idcountry asc'); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('What country are you in?')
				->setAttrib('data-theme', 'm');
		
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$country->addMultiOption($id, $countryname);
				else
					$country->addMultiOption(substr($id,7), $countryname);
		}
		
		
		$email = new Zend_Form_Element_Text('email');
		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' appears to be an IP address, but IP addresses are not allowed"),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' appears to be a DNS hostname but cannot match TLD against known list"),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' appears to be a DNS hostname but contains a dash (-) in an invalid position"),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' appears to be a DNS hostname but cannot match against hostname schema for TLD '%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' appears to be a DNS hostname but cannot extract TLD part"),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' does not match the expected structure for a DNS hostname"),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' does not appear to be a valid local network name"),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' appears to be a local network name but local network names are not allowed")
			)
		);
 
		$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' is not a valid email address"),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' is not a valid hostname for email address '%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' does not appear to have a valid MX record for the email address '%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' not matched against dot-atom format"),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' not matched against quoted-string format"),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' is not a valid local part for email address '%value%'")
			)
		);
		$email->addValidator($validatorNotEmpty, true)
			->setRequired(true)
			->setLabel('Email Address')
			->addFilter($filterTrim)
			->addValidator($validatorEmail);
			
		$description = new Zend_Form_Element_Textarea('description');
		$description->addFilter($filterTrim)
					->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setRequired(true)
					->setLabel('Please briefly describe your project')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50');
		
		$product = new Zend_Form_Element_Hidden('product');
		$product->setValue('Experience');
				

		$submit = new Zend_Form_Element_Submit('login');
		$submit->setLabel('Submit  ');
		$submit->setAttrib('rel','external');
		$submit->setAttrib('data-inline','true');
		$submit->setAttrib('data-icon','arrow-r');
		$submit->setAttrib('data-iconpos','right');
		$submit->setAttrib('data-theme', 'm');
		$submit->setAttrib('style', 'float:right');

		$this->addElement($fullName);
		$this->addElement($company);
		$this->addElement($email);
		$this->addElement($country);
		$this->addElement($description);
		$this->addElement($product);
	
		$this->addElement($submit);
 	}
}
