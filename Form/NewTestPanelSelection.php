<?php
class Form_NewTestPanelSelection extends My_Form
{
	public function init()
	{
		$translator = $this->getTranslator();

        $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();

		$saveDraft= new Zend_Form_Element_Submit('saveDraft');
		$saveDraft->setAttrib('class', 'save-draft')->setLabel('');
		$this->addElement($saveDraft);
	
		/*
		 * This field is added to select specific mobsters on closed tests
		 */
		$projects = new Projects(); 
		$project = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
		
		if(isset($project) && $project['inviteonly'] == 1){
			$to=new Zend_Form_Element_Text('contactsSelect');  
			$to->setLabel('Invite Specific Mobsters')
				->setDecorators(array('MobForm'))
				->setAttrib('size','40')
				->setAttrib('rel','#contactpop')	
				->setDescription('Specific Mobsters can be selected by clicking the box to the right and choosing from your contacts list. Only users on your contact list can be added');
			$to->helpText = 'Since you have specified a close project. Please choose those testers that you would like to participate in your project';
	
			$this->addElement($to);
	
		}

		$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element clearfix">';
		$heading1_str .= '<div class="form_label"><label>' . $translator->translate('form_newTestPanelSelection_heading1Label') . '</label></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>' . $translator->translate('form_newTestPanelSelection_heading1Help') . '</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);

		$countries=new Countries();
		$countrylist=array_merge(array(0=> $translator->translate('form_newTestPanelSelection_any')),$countries->listCountries()); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel($translator->translate('form_newTestPanelSelection_heading1Country'))
				->setDecorators(array('MobForm'));
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$country->addMultiOption($id, $countryname);
				else
					$country->addMultiOption(substr($id,7), $countryname);
		}

		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel($translator->translate('form_newTestPanelSelection_heading1Network'))
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=> $translator->translate('form_newTestPanelSelection_any')))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addcarrier = new Zend_Form_Element_Button('addcarrier');
		$addcarrier->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);

		$networktable=new My_Form_Element_myXhtml('networktable');
		$networktable_str = '</div>';
		$networktable_str .= '</div>';
		$networktable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$networktable_str .= '<div id="networktable"></div>';
		$networktable_str .= '</div>';
		$networktable_str .= '</div>';
		$networktable->setContent($networktable_str);

		$heading2 = new My_Form_Element_myXhtml('heading2');
		$heading2_str = '<div class="form_element clearfix">';
		$heading2_str .= '<div class="form_label"><label>' . $translator->translate('form_newTestPanelSelection_heading2Label') . '</label></div>';
		$heading2_str .= '<div class="form_helptip">';
		$heading2_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading2_str .= '<p>' . $translator->translate('form_newTestPanelSelection_heading2Help') . '</p>';
		$heading2_str .= '</span>';
		$heading2_str .= '</div>';
		$heading2_str .= '<div class="form_input">';
		$heading2->setContent($heading2_str);

		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel($translator->translate('form_newTestPanelSelection_heading2Platform'))
				->setdecorators(array('MobForm'))
				->addMultiOptions(array(''=> $translator->translate('form_newTestPanelSelection_any'), 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(''=> $translator->translate('form_newTestPanelSelection_any')), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel($translator->translate('form_newTestPanelSelection_heading2Handset'))
				->setDecorators(array('MobForm'))
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel($translator->translate('form_newTestPanelSelection_heading2Model'))
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=> $translator->translate('form_newTestPanelSelection_any')))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addmodel = new Zend_Form_Element_Button('addmodel');
		$addmodel->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);
		
		
		$handsettable=new My_Form_Element_myXhtml('handsettable');
		$handsettable_str = '</div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$handsettable_str .= '<div id="handsettable"></div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '</div>';
		
		$handsettable->setContent($handsettable_str);
				
		
		
		

		$region = new Zend_Form_Element_Textarea('region');
		$region->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel('Other Search Criteria')
		    ->setOptions(array('cols' => '50'));
		$region->setValue('Looking for testers in San Francisco, Chicago, New York City and in Uzbekistan.');
		$region->helpText = 'Use this field to target a specific region like a city, state, province, territory or ...?';

 		
		/*
		 * Other Selection Criteria: Why do we have two of these?
		 */
		$region = new My_Form_Element_TinyMce('$region');	
		$region->addFilter($filterTrim)
			//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
			->setDecorators(array('MobForm'))
			->setLabel($translator->translate('form_newTestPanelSelection_regionLabel'))
			->setAttrib('rows', '10')
			->setAttrib('cols', '50')
			->setDescription($translator->translate('form_newTestPanelSelection_regionDesc'))
			->setValue('None');
		$region->helpText=$translator->translate('form_newTestPanelSelection_regionHelp');



		/*
		$heading4 = new My_Form_Element_myXhtml('heading4');
		$heading4_str = '<div class="form_element">';
		$heading4_str .= '<div class="form_label"><label>Advanced Search</label></div>';
		$heading4_str .= '<div class="form_helptip">';
		$heading4_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading4_str .= '<p>Use other demographic and psychographic data to futher define your selection criteria.</p>';
		$heading4_str .= '</span>';
		$heading4_str .= '</div>';
		$heading4_str .= '<div class="form_input">';
		$heading4_str .= '</div>';
		$heading4_str .= '</div>';
		$heading4->setContent($heading4_str);*/

		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');
		
		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$mobtest_logo = new My_Form_Element_myXhtml('mobtest_logo');
		$mobtest_logo->setContent('<div id="mobtest-logo"><span>MobTest Real world mobile testing.</span></div>');

		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('')
				->setDecorators($this->submitDecorators);

		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')
			->setDecorators($this->submitDecorators);

				
		$this->addElement($heading1);
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($addcarrier);
		$this->addElement($networktable);
		$this->addElement($heading2);
		$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		$this->addElement($handsettable);
		$this->addElement($region);
		//$this->addElement($heading4);
		$this->addElement($start_row);
		$this->addElement($mobtest_logo);
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row); 
        }
}
?>