<?php
class Form_AcceleratorStepTwo extends My_Form
{
    public function init()
    {
                $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
  
		/*
		 * This field is added to select specific mobsters on closed tests
		 */
		$projects = new Projects(); 
		$project = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
		
		if(isset($project) && $project['inviteonly'] == 1){
			$to=new Zend_Form_Element_Text('contactsSelect');  
			$to->setLabel('Invite Specific Mobsters')
				->setDecorators(array('MobForm'))
				->setAttrib('size','70')
				->setAttrib('rel','#contactpop')	
				->setDescription('Specific Mobsters can be selected by clicking the box to the right and choosing from your contacts list. Only users on your contact list can be added');
		    $to->helpText = 'Since you have specified a close project. Please choose those testers that you would like to participate in your project';
	
			$this->addElement($to);
	
		}else{
			
			      
	        $amtMobsters = new Zend_Form_Element_Text('amtMobsters');
			$amtMobsters	->addFilter($filterTrim)
	  						->setDecorators(array('MobForm'))
							->setLabel('How Many Mobsters?')
							->setAttrib('size','10')
							->setRequired(true)
							->setDescription('Total number of respondents required for the survey.')
							->addValidator(new Zend_Validate_Int());
			$amtMobsters->helpText = 'Enter the total number of respondants you wish to reach with the survey. The Mob4Hire survey engine will manage the panel members in the community in order to ensure you get exactly the number of respondents you need. By entering the number of completed responses, you also cap the cost of the survey to a maximum amount, so there are no surprises in our billing.';
			$amtMobsters->setValue('15');
				
			$this->addElement($amtMobsters);
		}



		$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element clearfix">';
		$heading1_str .= '<div class="form_label"><label>Network Carriers</label></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>Add as many network carriers as you want for people to participate in the survey. If you select "ANY", then everybody qualifies for the survey.</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);

		$countries=new Countries();
		$countrylist=array_merge(array(0=>'Any'),$countries->listCountries()); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country')
				->setDecorators(array('MobForm'));
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$country->addMultiOption($id, $countryname);
				else
					$country->addMultiOption(substr($id,7), $countryname);
		}
		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addcarrier = new Zend_Form_Element_Button('addcarrier');
		$addcarrier->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);

		$networktable=new My_Form_Element_myXhtml('networktable');
		$networktable_str = '</div>';
		$networktable_str .= '</div>';
		$networktable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$networktable_str .= '<div id="networktable"></div>';
		$networktable_str .= '</div>';
		$networktable_str .= '</div>';
		$networktable->setContent($networktable_str);

		$heading2 = new My_Form_Element_myXhtml('heading2');
		$heading2_str = '<div class="form_element clearfix">';
		$heading2_str .= '<div class="form_label"><label>Platforms & Handsets</label></div>';
		$heading2_str .= '<div class="form_helptip">';
		$heading2_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading2_str .= '<p>Add as many handsets as you want. Only those panel members who have those handsets will qualify for the survey. If you select "ANY", then everybody qualifies for the survey.</p>';
		$heading2_str .= '</span>';
		$heading2_str .= '</div>';
		$heading2_str .= '<div class="form_input">';
		$heading2->setContent($heading2_str);

		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel('Platform')
				->setdecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any', 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(0=>'Any'), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel('Handset')
				->setDecorators(array('MobForm'))
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addmodel = new Zend_Form_Element_Button('addmodel');
		$addmodel->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);
		
		
		$handsettable=new My_Form_Element_myXhtml('handsettable');
		$handsettable_str = '</div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$handsettable_str .= '<div id="handsettable"></div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '</div>';
		
		$handsettable->setContent($handsettable_str);
		
		$region = new My_Form_Element_TinyMce('$region');	
		$region->addFilter($filterTrim)
			//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
			->setDecorators(array('MobForm'))
			->setLabel('Other Selection Criteria')
			->setAttrib('rows', '10')
			->setAttrib('cols', '50')
			->setDescription('You can specify any other demographic or geographic constraints here. ')
			->setValue('None');
		$region->helpText='Use this free form field to target a specific region like a city or state or select Mobsters by demographic such as age or income level';


		
		$this->addElement($heading1);
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($addcarrier);
		$this->addElement($networktable);
		$this->addElement($heading2);
		$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		$this->addElement($handsettable);
		$this->addElement($region);
		
		
		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('                       ')
				->setDecorators($this->submitDecorators);
				
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		
		$this->addElement($start_row);
	//	$this->addElement($mobtest_logo);
		
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);
		
    }    
} //end class

?>