<?php
class Form_MobileChangePassword extends My_Form
{
	public function init()
	{
		 $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		 $filterTrim = new Zend_Filter_StringTrim();
		 
		 $this->setAction('/mobile/profile/changepassword')
			->setMethod('post')
			->setAttrib('id', 'changepassword');
		
		$validatorIdentical = new My_Validate_PasswordConfirmation('confirmpassword') ;
		$validatorConfirm = new My_Validate_CurrentPasswordConfirmation() ;
		 
				         
		$oldpassword = new Zend_Form_Element_Password('currentpassword');
		$oldpassword->setLabel('Current Password')
				->addFilter($filterTrim)
				->setRequired(true)
				->addValidator($validatorConfirm)
			    ->addValidator('NotEmpty');
	
	
		$confirmPassword = new Zend_Form_Element_Password('confirmpassword');
		$confirmPassword->setLabel('Confirm')
						->addFilter($filterTrim)
						->setRequired(true);
	
		
		
		$password = new Zend_Form_Element_Password('password');
		$password->setLabel('New Password')
				->addFilter($filterTrim)
				->setRequired(true)
			    ->addValidator('NotEmpty') 
		        ->addValidator($validatorIdentical);
				 

		if (!isset(Zend_Registry::get('defSession')->resetPass))
			$this->addElement($oldpassword);
			
		$this->addElement($password);
		$this->addElement($confirmPassword);
	 
				
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Change  ');
		$submit->setAttrib('rel','external');
		$submit->setAttrib('data-inline','true');
		$submit->setAttrib('data-icon','arrow-r');
		$submit->setAttrib('data-iconpos','right');
		$submit->setAttrib('data-theme', 'm');
		$submit->setAttrib('style', 'float:right');
		
	 
		$this->addElement($submit);
		 
		 
		 
		
 	}
}
