<?php
class Form_Login extends My_Form
{
	public function init()
	{
		$this->addPrefixPath('My_Decorator','My/Decorator/','decorator');
		$this->setAction('/index/login')->setMethod('post')->setAttrib('id', 'login');
		$translator = $this->getTranslator();
 
		$username = new Zend_Form_Element_Text('username');
		$username->setLabel($translator->translate('form_login_username'))
				 ->setDecorators(array('MobForm'));        
		
	
		$password = new Zend_Form_Element_Password('password');
		$password->setLabel($translator->translate('form_login_password'))->setDecorators(array('MobForm'));
		if(strpos(Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),'app' )==1) // we were redirected here because not logged in
		{
			$this->addElement('hidden', 'return', array(
								'value' => Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(), 
								'name'=>'return'
						)); // used to direct the user where they wanted to go after auth failure		
		}
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('class', 'text_button');
        $submit->setLabel($translator->translate('form_login_submit'));
		
		$forgot = new My_Form_Element_myXhtml('forgot');
		$forgot->setDecorators(array('MobForm'))
				->setContent('		<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input"><div class="forgotpassword"><a href="/index/forgotpassword">' . $translator->translate('form_login_forgot') . '</a></div></div"></div></div>');
		$this->addElement($username);		 
		$this->addElement($password);
		$this->addElement($submit);
		
		$this->addElement($forgot);
 	}
}
