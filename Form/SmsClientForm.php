<?php
class Form_SmsClientForm extends My_Form
{
	public function init()
	{
		$this->setAction('/admin/sms/client')->setMethod('POST')->setAttrib('id','frmSmsUserForm');
		
		$filterTrim = new Zend_Filter_StringTrim();
		
		/*
		 * Create Email Address
		 */
		$email = new Zend_Form_Element_Text('email');    
		$email->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Client Email')
				 ->setAttrib('size', '60')
				 ->setRequired(true)
				 ->setDescription('Email address for the SMS API key');			 
 		
        
        $goButton = new Zend_Form_Element_Submit('button');
        $goButton->setLabel('Add');
		
        $this->addElement($email);
        $this->addElement($goButton);
           
	}
}