<?php
class Form_SvyNewPt1
{
    public $title = "<h3>Select Survey Type</h3>";
    public $summary ="<p>Select from our three pre-formatted surveys.<br />(Coming soon...create your own survey)</p>";
    
    public function getForm(){
        
        $form = new Zend_Form;
        $form->setAction('/app/survey/rsrchrnew')->setMethod('post')->setAttrib('id','frmrschrnewPt1');
        
        $survey_type = new Zend_Form_Element_Radio('survey_type');
        $survey_type->addMultiOptions(array(1 => '1. MobViral $90 blah blah blah...',2 => '2. MobExperience $100 blah blah blah...',3 => '3. The one Stephen talked about at meeting $900 blah blah blah...'));
        $survey_type->setvalue(1);
        
        $form->addElement($survey_type);
        
        return $form;    
    }//end getForm     
} //end class

?>