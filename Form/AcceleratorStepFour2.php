<?php
class Form_AcceleratorStepFour2 extends My_Form
{
	public function init()
	{
		//$this->setAction('/app/profile/index')->setMethod('post')->setAttrib('id','frmProfileContactInfo');
		$filterTrim = new Zend_Filter_StringTrim();

		$start_div_form_element = '<div class="form_element clearfix">';
		$start_div_helptip = '<div class="form_helptip">';
		$close_div = '</div>';

		$start_form_label = '<div class="form_label"><label>';
		$end_form_label = '</label></div>';

		$start_span_whats_this = '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>';
		$close_span = '</p></span>';

		$start_td = '<td>';
		$start_label_td = '<td class="label">';
		$end_td = '</td>';

		$start_table_label = $start_label_td . $start_div_form_element . $start_form_label;
		$helptip = $end_form_label. $start_div_helptip. $start_span_whats_this;
		$close_label_tags = $close_span . $close_div . $close_div . $end_td;
	    
		$projReview = new My_Form_Element_myXhtml('projReview');
		$projReview_str = $start_table_label;
		$projReview_str .='Submit Project';
		$projReview_str .= $helptip;
		$projReview_str .='We hope you have a great experience with the Mob4hire community.';
		$projReview_str .= $close_label_tags;
		$projReview->setContent($projReview_str);
	
		$projReview2 = new My_Form_Element_myXhtml('projReview2');
		$review_desc = $start_td;
		$review_desc .= '<span class="mobblack">Please note: As of April, 2010, only Managed Survey projects are available. Six things happen when you click [SUBMIT]: </span>';
		$review_desc .= '<ol class="submit-project">';
		

		$review_desc .= '<li>We\'ll review the project and give you a quote for both managing the project as well as the testers flat fee transactions.</li>';
		$review_desc .= '<li>If you agree, we will charge you the amount of your quote. Any amount submitted to be paid to testers will be held in escrow, pending project results to ensure your 100% satisfaction.</li>';
		$review_desc .= '<li>We will load all potential panel members into the Mob4Hire survey engine, and finalize cover letters, emails and such</li>';
		$review_desc .= '<li>We will keep the survey open until the # of Mobsters you\'ve requested have completed the survey, or until the end of the survey period (we\'ll refund any unused tester flat fee transaction escrow funds).</li>';
		$review_desc .= '<li>Results will be returned to you in Excel spreadsheet format</li>';
		$review_desc .= '<li>All the panel members will be automatically entered into your contact list. You\'ll need to rate them, too, if you want.</li>';
		
		
		
		$review_desc .= '</ol>';
		$review_desc .= $end_td;
		$projReview2->setContent($review_desc);
			
		$submit_project_start_td = new My_Form_Element_myXhtml('submit_project_start_td');
		$submit_project_start_td->setContent('<td>');
	    
		$submitProject = new Zend_Form_Element_Submit('submitProject');
		$submitProject->setLabel('SUBMIT PROJECT TO MOB4HIRE');//->setDecorators($this->buttonDecorators);
	    
		$submit_project_end_td = new My_Form_Element_myXhtml('submit_project_end_td');
		$submit_project_end_td->setContent('</td>');
	    
		$start_table = new My_Form_Element_myXhtml('start_table');
		$start_table->setContent('<table>');
		$end_table = new My_Form_Element_myXhtml('end_table');
		$end_table->setContent('</table>');
		
		$start_review_row = new My_Form_Element_myXhtml('start_review_row');
		$start_review_row->setContent('<tr>');
		$end_review_row = new My_Form_Element_myXhtml('end_review_row');
		$end_review_row->setContent('</tr>');
	    
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('')
				->setDecorators($this->submitDecorators);
		
		$paymethod = new Zend_Form_Element_Hidden('paymethodhidden');
		$paymethod->setValue(1);
		
		$amount= new Zend_Form_Element_Hidden('amount');
		$paymethod->setValue(0);
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel  ('')
				->setDecorators($this->submitDecorators);
	
		$this->addElement($start_table);
		$this->addElement($start_review_row);
		$this->addElement($projReview);
		$this->addElement($projReview2);
		$this->addElement($end_review_row);
		$this->addElement($end_table);
	    
		$this->addElement($start_row);
		$this->addElement($mobtest_logo);
		$this->addElement($paymethod);
		$this->addElement($amount);
		$this->addElement($submit);
		$this->addElement($previous);
		$this->addElement($end_row);
        
		$back= new Zend_Form_Element_Hidden('return');
		$back->setValue('/app/accelerator/resources') ;
		$this->addElement($back);
	
			
        }
}
