<?php
class Form_ExperienceStepThreeA extends My_Form
{
    public function init()
    {
          $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
 /*       
       	$surveyName = new Zend_Form_Element_Text('surveyname');
       	$surveyName->setDecorators(array('MobForm'))
       			->setLabel('New Survey Name')
				->setDescription('Enter the name you wish to be associated with this new survey.');
		$surveyName->helpText='Typically the name entered here would be the same as the project or project name followed by survey.';
*/				
		$applicationName = new Zend_Form_Element_Text('appname');
		$applicationName->setDecorators(array('MobForm'))
       			->setLabel('Application Name')
				->setDescription('What is the name of the application being tested');
		$applicationName->helpText='The application name provided here will replace the APPLICATION NAME tags inside of each question inside of the MobExperience Survey. This field is mandatory.';
		
		$handsetName = new Zend_Form_Element_Text('handsetname');
		$handsetName->setDecorators(array('MobForm'))
       			->setLabel('Handset Name')
				->setDescription('What handset is the experience on.');
		$handsetName->helpText='If the survey will be taking place on several handsets it is recommended you just enter "your handset".  The information entered here will replace the {HANDSET MANUFACTURER & MODEL} tag in the MobExperience Survey.';
		
		$surveyID = new Zend_Form_Element_Hidden('idsurvey');
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak_str = '<hr /><br />';
		$lineBreak_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$lineBreak_str .= 'Use the following section to provide the links to the resources that you want the surveyors to evaluate';
		$lineBreak_str .= '</div>';	
		$lineBreak->setContent($lineBreak_str);
		
		$genFiles = new My_Form_Element_FileUpload('genFiles');
		$genFiles->setDecorators(array('MobForm'))
				->setLabel('Generic Mobile App Files')
				->setDescription('Upload files that will be used by everyone\'s handset in the project.');
		$genFiles->helpText='Optional. This would be your mobile app. Or, mobile ad campaign. Or, whatever you want to install on the Mobsters handsets for them to try out before they complete a survey.';
		
		$divgenFiles=new My_Form_Element_myXhtml('divgenFiles');
		$divgenFiles_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divgenFiles_str .= '<div id="divgenFiles"></div>';
		$divgenFiles_str .= '</div>';
		$divgenFiles_str .= '</div>';	
		$divgenFiles->setContent($divgenFiles_str);
		
		$mobURL= new Zend_Form_Element_Text('mobURL');
		$mobURL->setLabel('Mobile Website')
				->setAttrib('size','50')
				->setDescription('Or, do you need a web resource that the Mobsters can access. Use this field if you want to test your mobile web site')
				->setDecorators(array('MobForm'));
		$mobURL->helpText='Optional. Handsets and browsers vary so much that you\'ll need to test your website on many different versions. Enter the URL of your mobile website here.';
		
		$appStoreURL= new Zend_Form_Element_Text('appStoreURL');
		$appStoreURL->setLabel('App Store URL')
				->setAttrib('size','50')
				->setDescription('Or, have the mobile user download it from an app store.')
				->setDecorators(array('MobForm'));
		$appStoreURL->helpText='If you already have an application in market that you wish to survey, you can direct mobile users to app stores. Remember: if you wish them to download a paid app, make sure to include the cost in the fee that you\'re willing to give for completing the survey.';
		
		
		$screenShots = new My_Form_Element_FileUpload('screenShots');
		$screenShots->setLabel('Screenshots')
				->setDecorators(array('MobForm'))
				->setDescription('Upload screenshots, other images or graphics.');	
		$screenShots->helpText='Your survey may be easier to explain if you include graphics. Or, maybe you want do some A-B testing, and show several screen samples for the mobile user to compare and comment on.';	
			
		
		$divscreenShots=new My_Form_Element_myXhtml('divscreenShots');
		$divscreenShots_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divscreenShots_str .= '<div id="divscreenShots"></div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots->setContent($divscreenShots_str);
		
		$documentation = new My_Form_Element_FileUpload('documentation');
		$documentation->setLabel('Other Documentation')
				->setDecorators(array('MobForm'))
				->setDescription('Upload supporting documentation, user guides or help.');	
		$documentation->helpText='Are there any other documents you need reviewed?';		
			
		$divdocumentation=new My_Form_Element_myXhtml('divdocumentation');
		$divdocumentation_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divdocumentation_str .= '<div id="divdocumentation"></div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation->setContent($divdocumentation_str);
	
//		$this->addElement($surveyName);
		$this->addElement($applicationName);
		$this->addElement($handsetName);
		$this->addElement($surveyID);
		
		$this->addElement($lineBreak);
		$this->addElement($genFiles);
		$this->addElement($divgenFiles);
		$this->addElement($mobURL);
		$this->addElement($appStoreURL);
	
		$this->addElement($screenShots);
		$this->addElement($divscreenShots);		
		$this->addElement($documentation);
		$this->addElement($divdocumentation);
	    
		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('                       ')
				->setDecorators($this->submitDecorators);
				
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		
		$this->addElement($start_row);
		
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);
		
    }    
} //end class

?>