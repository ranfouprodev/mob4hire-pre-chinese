<?php
class Form_ExperienceStepThreeB extends My_Form
{
    public function init()
    {
          $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
        
		$questionList=new My_Form_Element_myXhtml('questionstuff1');
		$questionString = '<div id="questionlistajaxtable"></div>';
		$questionList->setContent($questionString);
		
		$surveyID = new Zend_Form_Element_Hidden('idsurvey');
		
		
		$questionText =new Zend_Form_Element_Textarea('questiontext');
		$questionText->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel('Survey Question')
				->setOptions(array('cols'=>'50'))
				->setOptions(array('rows'=>'5'))
				->setDescription('Enter the text for the question you wish to have attached to the survey.');
		$questionText->helpText='This is the text that will be put into the survey.  If you wish to have your application name appear in the survey enter {APPLICATION NAME} inside this textarea';
		
		$mandatory = new Zend_Form_Element_Select('mandatory');
		$mandatory->setLabel('Required')
			->setDecorators(array('MobForm'))
			->setDescription('Is this question required to be answered for the survey.')
			->addMultiOptions(array(1=>'Yes'))
			->addMultiOptions(array(0=>'No'));
		$mandatory->helpText='If answering this question in your survey is required select Yes, however if the question isnt required to be answered select No.';
		
		$answerGroup = new Zend_Form_Element_Select('answergroup');
		$answerGroup->setLabel('Answer Choice')
			->setDecorators(array('MobForm'))
			->setDescription('What type of answer set do you wish to use?');
		$answerGroup->helpText='Changing the drop down menu will select the answer type you wish and it will also display the possible answers below';
		$answer = new Answer();
		$groups=$answer->listAnswerGroups();
		foreach($groups as $group)
		{
			$answerGroup->addMultiOptions(array($group['answergroup']=>$group['answergroup']));
		}
		
		$answerList=new My_Form_Element_myXhtml('answerstuff1');
		$answerString = '<div id="answerajaxtable"></div>';
		$answerList->setContent($answerString);
		
		$addButton = new Zend_Form_Element_Button('addquestion');
		$addButton->setLabel('Add Question');
		
		$this->addElement($questionList);
		$this->addElement($surveyID);
		$this->addElement($questionText);
		$this->addElement($mandatory);
		$this->addElement($answerGroup);
		$this->addElement($answerList);
		$this->addElement($addButton);
	    
		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('                       ')
				->setDecorators($this->submitDecorators);
				
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		
		$this->addElement($start_row);
		
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);
		
    }    
} //end class

?>