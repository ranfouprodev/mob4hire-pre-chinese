<?php
class Form_RecoverPassword extends My_Form
{
	public function init()
	{
		$translator = $this->getTranslator();

		$this->addPrefixPath('My_Decorator','My/Decorator/','decorator');
 
 		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' " . $translator->translate('form_recoverpassword_IP_ADDRESS_NOT_ALLOWED')),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' " . $translator->translate('form_recoverpassword_UNKNOWN_TLD')),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_DASH')),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME_SCHEMA') . "'%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' " . $translator->translate('form_recoverpassword_UNDECIPHERABLE_TLD')),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME0')),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_LOCAL_NAME')),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' " . $translator->translate('form_recoverpassword_LOCAL_NAME_NOT_ALLOWED'))
			)
		);
 
	 	$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID')),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME') . " '%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' " . $translator->translate('form_recoverpassword_INVALID_MX_RECORD') . " '%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' " . $translator->translate('form_recoverpassword_DOT_ATOM')),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' " . $translator->translate('form_recoverpassword_QUOTED_STRING')),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' " . $translator->translate('form_recoverpassword_INVALID_LOCAL_PART') . " '%value%'")
			)
		);
 
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel($translator->translate('form_recoverpassword_email'))
				->addValidator($validatorEmail)
				->setRequired(true)
				->setAttrib('size','50')
				->setDecorators(array('MobForm'));        
		
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
		
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('class', 'text_button_red');
        $submit->setLabel($translator->translate('form_recoverpassword_submit'));

		
		$this->addElement($email);		 
		$this->addElement($start_row);
		$this->addElement($submit);
		$this->addElement($end_row);
 	}
}