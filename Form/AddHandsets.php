<?php
class Form_AddHandsets extends My_Form
{
	// This form will never be submitted. It is entirely Ajax
	
	public function init()
	{
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country:');
		$countries=new Countries();
		$countrylist=$countries->listCountries();
		foreach($countrylist as $id=>$countryname) {
			$country->addMultiOption(substr($id,7), $countryname);
		}

	
		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Carrier Network:');
	
		$countrycode= new Zend_Form_Element_Text('code');
		$countrycode->setLabel("Number");
		$number=new Zend_Form_Element_Text('number');
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		$manufacturer->setLabel('Manufacturer:');

		$devices=new Devices();	
		$manufacturer->addMultiOptions($devices->listVendors());
	
		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model:');
	
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($countrycode);
		$this->addElement($number);
		$this->addElement($manufacturer);
		$this->addElement($model);	
	

		$add = new Zend_Form_Element_Submit('add');
		$add->setLabel('    Add    ');
	
		$this->addElement($add);
	
	}
}
?>
