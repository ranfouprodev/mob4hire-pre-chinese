<?php
// This is simply a test form for testing custom elements. Don't use it
class Form_Test extends My_Form
{
	public function init()
	{
		$test=new My_Form_Element_CountrySelect('test');
		$this->addElement($test);
	}
}
