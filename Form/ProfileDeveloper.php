<?php
class Form_ProfileDeveloper extends My_Form
{
	public function init(){
	$translator = $this->getTranslator();
	//$this->setAction('/app/profile/developer')->setMethod('post')->setAttrib('id','frmProfileDeveloper');
	$filterTrim = new Zend_Filter_StringTrim();

	$notifyEmail = new Zend_Form_Element_MultiCheckbox('notify_email');
	$notifyEmail->setLabel($translator->translate('form_profileDeveloper_notifyEmailLabel'))->setDecorators(array('MobForm'));
	$notifyEmail->setDescription($translator->translate('form_profileDeveloper_notifyEmailDesc'));
	$notifyEmail->helpText=$translator->translate('form_profileDeveloper_notifyEmailHelp');
	$notifyEmail->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_notifyEmailOpt1'),2 => $translator->translate('form_profileDeveloper_notifyEmailOpt2'),3 => $translator->translate('form_profileDeveloper_notifyEmailOpt3')));
	
	$notifySMS = new Zend_Form_Element_MultiCheckbox('notify_sms');
	$notifySMS->setLabel($translator->translate('form_profileDeveloper_notifySMSLabel')
)->setDecorators(array('MobForm'));
	$notifySMS->setDescription($translator->translate('form_profileDeveloper_notifySMSDesc'));
	$notifySMS->helpText=$translator->translate('form_profileDeveloper_notifySMSHelp');
	$notifySMS->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_notifySMSOpt1'),2 => $translator->translate('form_profileDeveloper_notifySMSOpt2')));
	
	$notifyResearch = new Zend_Form_Element_Radio('notify_research');
	$notifyResearch->setLabel($translator->translate('form_profileDeveloper_notifyResearchLabel')
)->setDecorators(array('MobForm'));
	$notifyResearch->setDescription($translator->translate('form_profileDeveloper_notifyResearchDesc'));
	$notifyResearch->helpText=$translator->translate('form_profileDeveloper_notifyResearchHelp');
	$notifyResearch->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_notifyResearchOpt1'),0 => $translator->translate('form_profileDeveloper_notifyResearchOpt2')));
	$notifyResearch->setvalue(1);
	
	$this->addElement($notifyEmail);
	//$this->addElement($notifySMS);
	//$this->addElement($notifyResearch);
	
	$this->addDisplayGroup(array('notify_email'),//,'notify_sms','notify_research'),
                                       'Notifications',
                                       array('legend' => $translator->translate('form_profileDeveloper_legendNotifications')));
	
	$stDate =  new My_Form_Element_DateSelects('start_date');
        $validatorDate = new Zend_Validate_Date('YYYY-MM-DD');
        $validatorDate->setMessages(
            array(
                 Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
	$stDate->setLabel($translator->translate('form_profileDeveloper_stDateLabel'))->addValidator($validatorDate)->setDecorators(array('MobForm'));
	$stDate->helpText=$translator->translate('form_profileDeveloper_stDateHelp');
        $stDate->setStartEndYear(1957, date("Y"))->setReverseYears(true);
	
	$revenue = new Zend_Form_Element_MultiCheckbox('revenue');
	$revenue->setLabel($translator->translate('form_profileDeveloper_revenueLabel'))->setDecorators(array('MobForm'));
	$revenue->setDescription($translator->translate('form_profileDeveloper_revenueDesc'));
	$revenue->helpText=$translator->translate('form_profileDeveloper_revenueHelp');
	$revenue->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_revenueOpt1'),2 => $translator->translate('form_profileDeveloper_revenueOpt2'),3 => $translator->translate('form_profileDeveloper_revenueOpt3'),4 => $translator->translate('form_profileDeveloper_revenueOpt4'),5 => $translator->translate('form_profileDeveloper_revenueOpt5'),6 => $translator->translate('form_profileDeveloper_revenueOpt6'),7 => $translator->translate('form_profileDeveloper_revenueOpt7'),8 => $translator->translate('form_profileDeveloper_revenueOpt8'),9 => $translator->translate('form_profileDeveloper_revenueOpt9'),10 => $translator->translate('form_profileDeveloper_revenueOpt10'),11 => $translator->translate('form_profileDeveloper_revenueOpt11'),12 => $translator->translate('form_profileDeveloper_revenueOpt12'),13 => $translator->translate('form_profileDeveloper_revenueOpt13'),14 => $translator->translate('form_profileDeveloper_revenueOpt14')));
	
	$platform = new Zend_Form_Element_MultiCheckbox('platform');
	$platform->setLabel($translator->translate('form_profileDeveloper_platformLabel'))->setDecorators(array('MobForm'));
	$platform->setDescription($translator->translate('form_profileDeveloper_platformDesc'));
	$platform->helpText=$translator->translate('form_profileDeveloper_platformHelp');
	$platform->addMultiOptions(array('iphone'=> $translator->translate('form_profileDeveloper_platformOpt1'),'blackberry' => $translator->translate('form_profileDeveloper_platformOpt2'),'android' => $translator->translate('form_profileDeveloper_platformOpt3'),'palm' => $translator->translate('form_profileDeveloper_platformOpt4'),'flash' => $translator->translate('form_profileDeveloper_platformOpt5'),'Windows' => $translator->translate('form_profileDeveloper_platformOpt6'),'j2me' => $translator->translate('form_profileDeveloper_platformOpt7'),'symbian' => $translator->translate('form_profileDeveloper_platformOpt8'),'mob2' => $translator->translate('form_profileDeveloper_platformOpt9'),'other' => $translator->translate('form_profileDeveloper_platformOpt10')));
	
	$apps = new Zend_Form_Element_MultiCheckbox('apps');
	$apps->setLabel($translator->translate('form_profileDeveloper_appsLabel'))->setDecorators(array('MobForm'));
	$apps->helpText=$translator->translate('form_profileDeveloper_appsLabel');
	$apps->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_appsOpt1'),2 => $translator->translate('form_profileDeveloper_appsOpt2'),3 => $translator->translate('form_profileDeveloper_appsOpt3'),4 => $translator->translate('form_profileDeveloper_appsOpt4'),5 => $translator->translate('form_profileDeveloper_appsOpt5'),6 => $translator->translate('form_profileDeveloper_appsOpt6'),7 => $translator->translate('form_profileDeveloper_appsOpt7'),8 => $translator->translate('form_profileDeveloper_appsOpt8'),9 => $translator->translate('form_profileDeveloper_appsOpt9'),10 => $translator->translate('form_profileDeveloper_appsOpt10'),11 => $translator->translate('form_profileDeveloper_appsOpt11'),12 => $translator->translate('form_profileDeveloper_appsOpt12'),13 => $translator->translate('form_profileDeveloper_appsOpt13'),14 => $translator->translate('form_profileDeveloper_appsOpt14'),15 => $translator->translate('form_profileDeveloper_appsOpt15'),16 => $translator->translate('form_profileDeveloper_appsOpt16'),17 => $translator->translate('form_profileDeveloper_appsOpt17'),18 => $translator->translate('form_profileDeveloper_appsOpt18'),19 => $translator->translate('form_profileDeveloper_appsOpt19')));
	
	$numEmployees = new Zend_Form_Element_Select('num_employees');
	$numEmployees->setLabel($translator->translate('form_profileDeveloper_numEmployeesLabel'))->setDecorators(array('MobForm'));
	$numEmployees->helpText=$translator->translate('form_profileDeveloper_numEmployeesHelp');
	$numEmployees->addMultiOptions(array(1 => $translator->translate('form_profileDeveloper_numEmployeesOpt1'),2 => $translator->translate('form_profileDeveloper_numEmployeesOpt2'),3 => '2-5',4 => '6-10',5 => '11-25',6 => '26-100',7 => '101-500',8 => '500+'));
	
	$this->addElement($stDate);
	$this->addElement($revenue);
	$this->addElement($platform);
	$this->addElement($apps);
	$this->addElement($numEmployees);

	$this->addDisplayGroup(array('stDate','revenue','platform','apps','num_employees'),
                                       'about company',
                                       array('legend' => $translator->translate('form_profileDeveloper_legendAboutCompany')));

	
	$start_row = new My_Form_Element_myXhtml('start_single_row');
	$start_row->setContent('<div class="submit_buttons clearfix">');

	$cancel = new Zend_Form_Element_Submit('cancel');
	$cancel->setLabel('')
			->setDecorators($this->submitDecorators);

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setAttrib('class', 'text_button');
		$submit->setLabel($translator->translate('form_login_submit'));
	
	
		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
	
	
	
	
	
	
	/*$this->addDisplayGroup(array('revenue'),
                                       'main revenue',
                                       array('legend' => 'What Is Your Company\'s Main Source Of Revenue? Please select all that apply'));
	
	*/
	//$this->addElement($languages);
	
	/*			    
	$this->addElement($numMobApps);
	$this->addElement($mobApps);
	$this->addElement($mobAppsInDev);
	$this->addElement($annualRevenue);
	$this->addElement($stDate);
	$this->addElement($social);
	$this->addElement($music);
	$this->addElement($news);
	$this->addElement($medical);
	$this->addElement($reference);
	$this->addElement($productivity);
	$this->addElement($navigation);
	$this->addElement($fitness);
	$this->addElement($education);
	$this->addElement($weather);
	$this->addElement($business);
	$this->addElement($finance);
	$this->addElement($sports);
	$this->addElement($travel);
	$this->addElement($util);
	$this->addElement($games);
	$this->addElement($books);
	$this->addElement($ent);
	$this->addElement($lifestyle);
	
	
	$this->addDisplayGroup(array('social','music','news','medical','reference','productivity','navigation','fitness','education','weather','business','finance','sports','travel','util','games','books','ent','lifestyle'),
                                       'Types Offered And Developing',
                                       array('legend' => 'Applications You Currently Offer For Download Or Which You Are Currently Developing. (check all that apply)'));
	
	
				    */
	
						$this->addElement($start_row);
						$this->addElement($submit);
					//	$this->addElement($cancel);
						$this->addElement($end_row);
	
	}
}
?>