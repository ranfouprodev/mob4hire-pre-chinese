<?php
class Form_MobileLogin extends My_Form
{
	public function init()
	{

		$translator = $this->getTranslator();
		$this->setAction('/mobile/user/login')
			->setMethod('post')
			->setAttrib('id', 'login');
		
 
		if(strpos(Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),'survey' )==1) // we were redirected here because not logged in
		{
			$this->addElement('hidden', 'return', array(
								'value' => Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(), 
								'name'=>'return'
						)); // used to direct the user where they wanted to go after auth failure		
		}

		$username = new Zend_Form_Element_Text('username');
		$username->setLabel($translator->translate('form_mobile_login_username'));
		$this->addElement($username);
	
		$password = new Zend_Form_Element_Password('password');
		$password->setLabel($translator->translate('form_mobile_login_password'));
		$this->addElement($password);
 
		$submit = new Zend_Form_Element_Submit('login');
		$submit->setLabel($translator->translate('form_mobile_login_button'));
		$submit->setAttrib('rel','external');
		$submit->setAttrib('data-inline','true');
		$submit->setAttrib('data-icon','arrow-r');
		$submit->setAttrib('data-iconpos','right');
		$submit->setAttrib('data-theme', 'm');
		$submit->setAttrib('style', 'float:right');
		$this->addElement($submit);
 	}
}
