<?php

class Api_IndexController extends Zend_Controller_Action
{
    public function indexAction()
    {

        $this->_helper->Layout->disableLayout();
        $this->_helper->ViewRenderer->setNoRender();

        $params = $this->_request->getParams();
        unset ($params['controller']);
        unset ($params['action']);
        unset ($params['module']);

        $server_prefix = 'ApiServer_XmlServer';

        if (array_key_exists('context', $params)) {
            switch($params['context']) {
                case 'user':
                    $server_class = $server_prefix.'User';
                    break;
                default:
                    throw new Exception('Invalid context');
            }
        }
        else
            $server_class = $server_prefix;

        $server = new ApiServer_XmlHandler();
	    $server->setClass($server_class);
		$server->returnResponse(true);
		
		$responseXML = $server->handle($params);
		
		$responseXML = preg_replace('~\s*(<([^>]*)>[^<]*</\2>|<[^>]*>)\s*~','$1',$responseXML);

		
		if (!array_key_exists('format', $params)) {
		    if ($this->_request->isXmlHttpRequest())
		    $format = 'json';
		else
		    $format = 'xml';
		} else
		    $format = $params['format'];
		
		switch($format) {
		    case 'xml':
		        $this->_response->setHeader('Content-Type', 'text/plain')->setBody($responseXML);
		    break;
		case 'json':
		    $this->_response->setHeader('Content-Type', 'application/json')->setBody(Zend_Json::fromXML($responseXML));
		    break;
	}
	

}





}
