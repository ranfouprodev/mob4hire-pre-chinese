<?php
class Survey_IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_helper->layout->setLayout('mobile_survey');
		$idsurvey=$this->getRequest()->getParam('idsurvey');
		//$idproject=$this->getRequest()->getParam('idproject');

		$this->view->survey=new Survey($idsurvey, $this->view->currentUser->id);
		$this->view->header=$this->view->survey->getTitle();
		$this->view->form=$form=new My_JQMSurveyForm($this->view->survey, $this->view->currentUser->id);
		
		//check accepted user here
		/*$tests = new Tests(); 
		if(!$tests->isActiveTesterOnProject($idproject, $this->view->currentUser->id))
		{
		//popup message or redirect to a page stating the user has not been accepted yet
			$this->_redirect('/app); 
		}*/
		
		
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			//fb(print_r($formData, true));
			if ($form->isValid($formData))
			{
				// get form values
				$formValues = $form->getValues();
				// write response
				$response=new Response();

				// This can be used for more than one result per page
				foreach ($formValues as $id=>$value) 
				{
					if((int)$id)
					{
						if (is_array($value)) {
						    foreach($value as $index=>$answer) {
							  $value[$index]=substr($answer,3);
						    }
						    $text=implode(',', $value);
						    $idanswer=0;
						}
						// Check for a multiple choice answer
						elseif (substr($value,0,3)=='ans')
						{
							// strip  off the first three chars to get the identifier	
							$idanswer=substr($value,3);
							$text=''; 
						}
						else {
							$idanswer=0;
							$text=$value;
						}
						
						$response->insert($prev=
							array(
									'iduser'=>$this->view->currentUser->id,
									'idsurvey'=>$idsurvey,
									'idquestion'=>$id,
									'idanswer'=>$idanswer,
									'text'=>$text,
									'timestamp'=>time()
								)
							);
					}
				}
				$this->view->form=$form=new My_JQMSurveyForm($this->view->survey, $this->view->currentUser->id, $prev); // load next question
			}
		}
	}
}