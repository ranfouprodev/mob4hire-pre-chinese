<?php

class App_InboxController extends Zend_Controller_Action
{
	
	public function init() {
        $wall = new Wall(); 
        $this->view->unread = $wall->countUnread($this->view->currentUser->id);

        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }
	
	
	public function privateAction()
	{
		$this->_helper->layout->setLayout('inbox');
	
		$this->view->idtarget=$this->view->currentUser->id;;
		$this->view->postType=3;
  	    $this->view->showcomment = false; 
		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->page=$page=1;
		$this->view->timestamp=$timestamp=strtotime("-180 day");;
				
	}
 

	/*
	 * Displays all the new messages for all projects/tests/private walls
	 */
	public function newAction(){
		$this->_helper->layout->setLayout('inbox');	
		$this->view->currentPage = "unread";
		
		$this->view->idtarget=$this->view->currentUser->id;;
		$this->view->postType=4;
  	    $this->view->showcomment = false; 
		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->page=$page=1;
		$this->view->timestamp=$timestamp=strtotime("-30 day");;
		$this->view->filter="new";
		
		// update the new messages
		 $wall = new Wall(); 
		$this->view->unread = $wall->countUnread($this->view->currentUser->id); 
	}
	
	/*
	 * Displays all the messages for all projects/tests/private walls
	 */
	public function indexAction(){
		$this->_helper->layout->setLayout('inbox');
	
		$parameters = $this->_getAllParams();

		$this->view->idtarget=$this->view->currentUser->id;;
		$this->view->postType=4;
		$this->view->showcomment = false; 
		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->page=$page=1;
		$this->view->timestamp=$timestamp=strtotime("-180 day");;

		
	}
	
	
	/*
	 * Shows one message thread
	 */
	public function viewAction()
	{
		$this->_helper->layout->setLayout('inbox');
	
		$parameters = $this->_getAllParams();
		if (isset($parameters['id']))
		{
			$this->view->idtarget=$idtarget=$parameters['id'];
			$this->view->postType=$postType=5;
			$this->view->resultsPerPage=1;
			$this->view->page=1;	
					
		}else{
			$this->view->error="That message thread is invalid or been removed.";
		}
		
		
	}//end indexAction
	
	
	
}//end class