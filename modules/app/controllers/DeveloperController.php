<?php

class App_DeveloperController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->view->currentPage = "developer";	
		
		$this->view->headScript()->appendFile('/js/projects/list.js');
		$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = true;var mode = "developer";var navigation = "none";');
	}
	
	public function testresultsAction()
	{	
		$idtest=$this->getRequest()->getParam('idtest');
		$testresults=new TestResults();
		$testresult = $testresults->getTestResult($idtest);
		
		
		/*
		 * New testresults are not being added. That needs to be patched
		 */
		$tests = new Tests(); 
		$test = $tests->getTest($idtest);
		
		if(!isset($test))
			$this->_redirect('/app/developer');
		
		$idproject = $test['idproject'];
		$projects= new Projects();
		$project=$projects->getProject($idproject);
	
		$this->checkDeveloperAccess($project);
				
		$this->view->test = $test; 
		$this->view->idproject = $idproject;	
		$this->view->idtester = $test['idtester']; 
	
		// Make sure the current user can access this test. 
		if(isset($testresult)){
			$this->view->testresults = $testresult;
			
			$bugreports=new BugReports();
			$this->view->bugreports=$bugreports->fetchAll("idtest=$idtest");
		}
	}
	
	public function testservicesAction()
	{
		
	}
	
	public function surveyservicesAction()
	{
		
	}

	public function acceptbidAction()
	{
		$idtest=$this->getRequest()->getParam('idtest');
		$tests = new Tests();

		$rowset=$tests->find($idtest);
		if ($this->view->test=$test=$rowset->current())
		{
			$this->view->balance=$this->view->getBalance($this->view->currentUser->id);
		}
		if ($this->getRequest()->isPost()) // form submitted
		{
			$this->view->result=$tests->acceptBid($idtest, $this->view->currentUser->id, $_POST['comments']);
		}
		else {
			$projects=new Projects();
			$users=new Users();

			$this->view->validated=0;
		
			if(($this->view->errorcode=$tests->canAcceptBid($idtest,  $this->view->currentUser->id))==1 || $this->view->errorcode==-3) // allow insufficient funds through
			{
				if ($test)
				{
					$this->view->project=$projects->getProject($test->idproject);
					$this->view->tester=$users->getUser($test->idtester);
					$this->view->validated=1;
				}
			}
		}
	}

	public function rejectbidAction()
	{
		$idtest=$this->getRequest()->getParam('idtest');
		$tests = new Tests();

		$rowset=$tests->find($idtest);
		$this->view->test=$test=$rowset->current();
		
		if ($this->getRequest()->isPost()) // form submitted
		{		
			$this->view->result=$tests->rejectBid($idtest, $this->view->currentUser->id, $_POST['comments']);
		}
		else {
			$users=new Users();

			$this->view->validated=0;
		
			if(($this->view->errorcode=$tests->canRejectBid($idtest,  $this->view->currentUser->id))) 
			{
				if ($test)
				{
					$this->view->tester=$users->getUser($test->idtester);
					$this->view->validated=1;
				}
			}
		}
	}
	
	public function escrowfundsAction()
	{
		$this->view->idtest=$this->getRequest()->getParam('idtest');
	}

	public function testcasedownloadSubmittedAction()
	{
		$this->view->idproject=$this->getRequest()->getParam('idproject');
		$this->view->idtest=$this->getRequest()->getParam('idtest');
	}
	
	public function downloadsubmittedAction()
	{
		$this->_helper->layout->disableLayout();
	}

	public function paytesterAction()
	{
		$idtest=$this->getRequest()->getParam('idtest');
		$tests = new Tests();

		$rowset=$tests->find($idtest);
		if($this->view->test=$test=$rowset->current()) {
			$users=new Users();
			$this->view->tester=$users->getUser($test->idtester);

		}
		
		if ($this->getRequest()->isPost()) // form submitted
		{		
			if(($this->view->result=$tests->acceptTestPayTester($idtest, $this->view->currentUser->id, $_POST['comments'])) > 0)
			{
				$ratingmodel=new Ratings();
				$rating=(int) $_POST['rating'];
				$ratingmodel->addRating($this->view->tester['username'], 1, $idtest, $rating);			
			}
		}
		else {

			$this->view->validated=0;
		
			if(($this->view->errorcode=$tests->canAcceptTestPayTester($idtest,  $this->view->currentUser->id))) 
			{
				if ($test)
				{
					$this->view->validated=1;
				}
			}
		}
	}

	public function declinetestcaseAction()
	{
		$idtest=$this->getRequest()->getParam('idtest');
		$comments=$this->getRequest()->getParam('comments');
		
		$tests = new Tests();
	
		$rowset=$tests->find($idtest);
		if($this->view->test=$test=$rowset->current()) {
			$users=new Users();
			$this->view->tester=$users->getUser($test->idtester);
		}
		
		if ($this->getRequest()->isPost()) // form submitted
		{		
			if(($this->view->result=$tests->developerRejectTest($idtest, $this->view->currentUser->id, $_POST['comments'])) > 0)
			{
				$ratingmodel=new Ratings();
				$rating=(int) $_POST['rating'];
				$ratingmodel->addRating($this->view->tester['username'], 1, $idtest, $rating);			
			}
		}
		else {

			$this->view->validated=0;
		
			if(($this->view->errorcode=$tests->developerCanRejectTest($idtest,  $this->view->currentUser->id))) 
			{
				if ($test)
				{
					$this->view->validated=1;
				}
			}
		}
	}
	
	public function closebiddingAction()
	{
		$idproject=$this->getRequest()->getParam('idproject');
		$projects= new Projects();
		$project=$projects->getProject($idproject);
		$this->checkDeveloperAccess($project); // this redirects if we are not the developer (or admin)
		if($this->view->submit=isset($_POST['submit']))
			$projects->closeBidding($idproject);
	}
	// These are Ajax actions. Must disable the layout to prevent the while site repeating itself
	public function devicemenuAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

	public function networktableAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

	public function handsettableAction()
	{
		$this->_helper->layout->disableLayout();
	
	}
	
	public function removeinvitedtesterAction()
	{
		$this->_helper->layout->disableLayout();
	
	}
	
	public function handsetfiltertableAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

	public function ajaxtestertableAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

	public function uploadAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

	protected function isActiveTesterOnProject($project)
	{
		$idtester = $this->view->currentUser->id;
		$idproject = $project['idproject'];
		
		$tests = new Tests(); 
		
		return  $tests->isActiveTesterOnProject($idproject, $idtester);
		
		
	}
	
	protected function checkDeveloperAccess($project)
	{
		$idtester = Zend_Registry::get('defSession')->currentUser->id;
		$idproject = $project['idproject'];
		
		if( ($project['iddeveloper']== $idtester && $project['Status']>=200 && $project['Status']<500)  || Zend_Registry::get('defSession')->currentUser->isAdmin ) {
			return true;
		}

		$this->_redirect('/app/developer');
	}

	public function showprojectAction()
	{
		$this->view->headScript()->appendFile('/js/handsets/list3.js');
		$this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
		$this->view->headScript()->appendFile('/js/devices/list.js');

				
		$this->view->idproject=$idproject=$this->getRequest()->getParam('idproject');
		
		$show=$this->getRequest()->getParam('showit');
			$this->view->show=$show;
				
		$projects= new Projects();
		$this->view->project=$project=$projects->getProject($this->view->idproject);
	
		
		// Test to make sure this is the developer or an active tester  and redirect to mobster/
		if($this->isActiveTesterOnProject($project)){
			$this->_redirect('/app/mobster/showproject/idproject/'.$idproject);
		}
		$this->checkDeveloperAccess($project);

		//Here is the extend date form that may need to be added in the view
		$form =  new Form_ExtendDateForm();
		$this->view->extendDateForm=$form;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();
				// write form values to database here
				$projects = new Projects();
				$formValues['Status']=201; // put the project back to 'in progress'
				$projects->updateProject($idproject, $formValues);
				$this->view->project=$project=$projects->getProject($this->view->idproject);
			}
			else 
			{
				$form->populate($formData);
			}
		}
		
		//Getting the supporting documentation needed here
		$projectFile = new ProjectFiles();
		$this->view->uploadPlan =  $projectFile->getFilesForProjectByCategory($idproject, 'uploadPlan');
		$this->view->ndaFile =  $projectFile->getFilesForProjectByCategory($idproject ,'ndaFile');
		$this->view->files = $projectFile->getFilesForProjectByCategory($idproject ,'documentation');
			
		
		//This code here will simply get generate an array who's indices are test status numbers
		//And values are the number of tests that fall into that category.
		$tests = new Tests();
		$rows=$tests->getStatusByProject($idproject);
		$projectInfoArray = array(0=>0, 50=>0, 75=>0, 100=>0, 200=>0, 250=>0, 300=>0, 350=>0, 400=>0, 499=>0);
		foreach($rows as $row)
		{
			$status=$row['Status'];
			$count=$row['count'];
			$projectInfoArray[$status]=$count;
		}

		$this->view->projectArray = $projectInfoArray;
		
		$regdevices=new Regdevices();
		$this->view->totalTesters=$regdevices->getCompatibleUsers($idproject)->count()+1;
		
		/*$this->view->totalTesters = '700';//$projectInfoArray[100]+$projectInfoArray[250]+$projectInfoArray[300]
				+$projectInfoArray[350]+$projectInfoArray[400];*/

		$testerinvites=new TesterInvites();
		$this->view->invitelist=$testerinvites->getInvitedTesters($idproject); // these are the rows of users we have already invited
		$this->view->invitesSent=$this->view->invitelist->count();//amount of mobsters invited already

		$testsTable=new Tests();
		$this->view->testList=$testList=$testsTable->getTestsByProject($idproject);
				
		
		$inviteFilter=new Form_HandsetFilter3();
		//Add code here to limit the counties
		$supportednetworks = new SupportedNetworks();
		$countrylist = array_merge(array(0=>'Any'),$supportednetworks->getSupportedCountries($idproject));
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$inviteFilter->country->addMultiOption($id, $countryname);
				else
					$inviteFilter->country->addMultiOption(substr($id,7), $countryname);
		}
		$supporteddevices = new SupportedDevices();
		$vendors=array_merge(array(0=>'Any'), $supporteddevices->listSupportedVendors($idproject));
		$inviteFilter->vendor->addMultiOptions($vendors);
		$this->view->inviteFilter=$inviteFilter;
	}
	

	/*
	** This table is rendered by shoproject  as an Ajax action. It dynamically shows compatible testers and invited testers 
	**
	*/
	public function invitetableAction()
	{	
		// todo: make sure only the correct user can perform this action
		$this->_helper->layout->disableLayout();
		
		$this->view->idproject=$idproject=$this->getRequest()->getParam('id');
		
		$show=$this->getRequest()->getParam('showit');
		$this->view->show=$show;
			
		if ($this->authenticate($idproject))
		{
			$testerinvites=new TesterInvites();
			$this->view->invitelist=$testerinvites->getInvitedTesters($idproject); // these are the rows of users we have already invited
			$this->view->invitesSent=$this->view->invitelist->count();//amount of mobsters invited already
			
			$regdevices=new Regdevices();
			$this->view->testerlist=$regdevices->getCompatibleUsers($idproject); //get mobsters
			$this->view->invitesRemaining= $this->view->testerlist->count()+1 - $this->view->invitesSent;
		}
	}
	
	public function invitetableajaxAction()
	{
		$this->_helper->layout->disableLayout();
		
		$whereClause=$this->getRequest()->getParam('whereClause');
		$idproject=$this->getRequest()->getParam('id');
		
		$show=$this->getRequest()->getParam('showit');
		$this->view->show=$show;
			
		$this->view->idproject=$idproject;
		
		$testerinvites=new TesterInvites();
		fb('idproject:'.$idproject);
		$this->view->invitelist=$testerinvites->getInvitedTesters($idproject); // these are the rows of users we have already invited
		$this->view->invitesSent=$this->view->invitelist->count();//amount of mobsters invited already
			
		$this->view->whereClause=mysql_real_escape_string(stripslashes($whereClause));
		
		$regdevices=new Regdevices();
		
		$this->view->testerlist=$regdevices->getCompatibleUsers($idproject, '' ,$whereClause);
		
		$this->view->invitesRemaining=$this->view->testerlist->count()+1 - $this->view->invitesSent;
		
		$this->render("invitetable"); // draw the new table after the invites
	}
	
	public function inviteAction()
	{
		// todo: make sure only the correct user can perform this action
		$this->_helper->layout->disableLayout();

		$this->view->idproject=$idproject=$this->getRequest()->getParam('id');
		
		$show=$this->getRequest()->getParam('showit');
			$this->view->show=$show;
			
		if ($this->authenticate($idproject))
		{
			$invites=explode(',', $this->getRequest()->getParam('invites'));

			$mailer = new Mailer();
			$mailer->sendInvitesTemp($invites, $idproject, 'en');

			$testerinvites=new TesterInvites();
			$this->view->invitelist=$invitelist=$testerinvites->getInvitedTesters($idproject); // these are the users we have already invited
			$this->view->invitesSent=$invitesSent=$invitelist->count();
			
			$regdevices=new Regdevices();
			$this->view->testerlist=$regdevices->getCompatibleUsers($idproject); //get mobsters
			$this->view->invitesRemaining=$this->view->testerlist->count()+1 - $this->view->invitesSent;
			$this->render("invitetable"); // draw the new table after the invites
		}
	}
	
	public function authenticate($idproject)
	{
		// in this case we do not want to redirect to /app/developer. We will just return an error message
		// eventually we will build all this into a front controller plugin which will make the code much cleaner
		$id = Zend_Registry::get('defSession')->currentUser->id;
		$projects=new Projects();
		$project=$projects->getProject($idproject);
		if( ($project && $project['iddeveloper']== $id)) {
			return true;
		}
		else {
			echo "Authentication Error";
		}
	
	}
}