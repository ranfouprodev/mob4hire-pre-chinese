<?php

class App_SearchController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_helper->layout->setLayout('index');
		
		$parameters = $this->_getAllParams();
		
		// var_dump($parameters);
		
		if ($this->getRequest()->isPost())
		{
			if ( isset ($parameters['type']) && isset($parameters['search']))
			{
				$type = strtolower($parameters['type']);
				$search = $parameters['search'];
				
				switch($type){
					case "people":
						$this->_redirect('/app/contacts/search/?search='.$search);	
						break;
					case "projects":
						$this->_redirect('/app/project/search/?search='.$search);
						break;
				}
				
			}

		}
	}
   
}
?>