<?php

class App_ProfileController extends Zend_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }

	public function indexAction()
	{
		$this->_helper->layout->setLayout('profile');
//		$this->view->headScript()->appendFile('/js/jquery.js'); // This is already enabled in the layout
		$this->view->headScript()->appendFile('/js/register/form.js');
		$this->view->headLink()->appendStylesheet('/css/rating.css');
		//$this->view->headLink()->appendStylesheet('/css/forms.css');
		
		$form = new Form_ProfileContactInfo;
		$this->view->profileInfo=$form;
		
		// We need to check if there is a profile entry for this user first. 
		// Otherwise the image check will fail. I am not sure this is correct. We are adding a record to the profile table before the validation has been passed. i thought this was how we were checking for completed profile
		$update = new ProfileContactInfo();
		$profile = $update->getProfile($this->view->currentUser->id);
		if(!$profile){
			$update->add($this->view->currentUser->id);
		}
		$this->view->profile=$profile;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
				
			if( isset($profile['idfacebook']) && $formData['email'] =="(via facebook)"){ // Can we have a bit of attention to detail here. More haste, less speed perhaps. The variable is defined one line before. You'd think it would be possible to remember what it is called
					$formData['email'] = $profile['email'];
			}
				
			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();

				

				$update = new ProfileContactInfo();
				$update->editProfile($this->view->currentUser->id, $formValues);
				
					//  Redirect based on the selection information. 
					switch($formValues['personal_description']){
						case 1:
						case 2:
							$this->_redirect('/app/profile/mobster');
							break;

						case 3:
							$this->_redirect('/app/profile/developer');
							break;
						case 6:
							$this->_redirect('/app/profile/researcher');
							break;
						default:
							$this->_redirect('/app/profile/view');			
						break;
					}		

				
			}//end if statement isValid()

			else
			{
				if( isset($profile['idfacebook']) && strpos($formData['email'],'proxymail.facebook.com') ){ // to prevent the facebook proxy email from showing again when validation failed
					$formData['email'] = "(via facebook)";
				}
				$form->populate($formData);
			}

		}// end if ispost()
			
		else
		{

			//populate info fields with model data
			$profileTable = new ProfileContactInfo();
			$profileInfo = $profileTable->getProfile($this->view->currentUser->id);
			
			if( isset($profileInfo['idfacebook']) && strpos($profileInfo['email'],'proxymail.facebook.com') ){
				$profileInfo['email'] = "(via facebook)";
			}
			
			$form->populate($profileInfo);
			
			$this->view->profile = $profileInfo;

		}

	}//end indexAction

	 
	public function developerAction()
	{
		$this->_helper->layout->setLayout('profile');

		// Get (or Add) profile contact info		
  		$update = new ProfileContactInfo();
		$profile = $update->getProfile($this->view->currentUser->id);
		$this->view->profile = $profile;
			
			
		// Check for record	
		$profileTable = new ProfileDeveloper();
		try{
			$profileInfo = $profileTable->getProfile($this->view->currentUser->id);					
			$form = new Form_ProfileDeveloper;
		}catch(Exception $e){
			// no record exists
			$form = new Form_ProfileDeveloperApply;
		}

		$this->view->profileDeveloper=$form;	
	
	
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
						
			if (isset($formData['apply'])) // User wants to start a developer account
			{
				$profileTable->add($this->view->currentUser->id);	
				$form = new Form_ProfileDeveloper;
				$this->view->profileDeveloper=$form;
			}
			else
			{
						
				if ($form->isValid($formData))
				{
					$formValues = $form->getValues();
	
					$update = new ProfileDeveloper();
					
					try{
						$update->updateProfile($this->view->currentUser->id, $formValues);
					}catch(Exception $e){
						echo $e->getMessage().' error';
					}
					
				
	
					
				}//end if statement isValid()
	
				else
				{
					$form->populate($formData);
				}
			}

		}// end if ispost()	
		else
		{

					
			if(isset($profileInfo)){
							
				$profileInfo['notify_email'] = explode(',',$profileInfo['notify_email']);
				$profileInfo['notify_sms'] = explode(',',$profileInfo['notify_sms']);
				$profileInfo['revenue'] = explode(',',$profileInfo['revenue']);
				$profileInfo['platform'] = explode(',',$profileInfo['platform']);
				$profileInfo['apps'] = explode(',',$profileInfo['apps']);
				
				
				$form->populate($profileInfo);
			}
			
			$this->view->profileDeveloper = $form;

		}
	
	}
    
	public function mobsterAction()
	{

		$this->_helper->layout->setLayout('profile');

		// Get (or Add) profile contact info		
  		$update = new ProfileContactInfo();
		$profile = $update->getProfile($this->view->currentUser->id);
		$this->view->profile = $profile;
			
			
		// Check for record	
		$profileTable = new ProfileMobster();
		try{
			$profileInfo = $profileTable->getProfile($this->view->currentUser->id);					
			$form = new Form_ProfileMobster;
		}catch(Exception $e){
			// no record exists
			$form = new Form_ProfileMobsterApply;
		}

		$this->view->profileMobster=$form;	
	
	
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
						
			if (isset($formData['apply'])) // User wants to start a mobster account
			{
				$profileTable->add($this->view->currentUser->id);	
				$form = new Form_ProfileMobster;
				$this->view->profileMobster=$form;
			}
			else
			{
						
				if ($form->isValid($formData))
				{
					$formValues = $form->getValues();
	
					$update = new ProfileMobster();
					
					try{
						$update->updateProfile($this->view->currentUser->id, $formValues);
					}catch(Exception $e){
						echo $e->getMessage().' error';
					}
					
					$this->_redirect('/app/');			
	
					
				}//end if statement isValid()
	
				else
				{
					$form->populate($formData);
				}
			}

		}// end if ispost()	
		else
		{
	
			//populate info fields with model data
			if(isset($profileInfo)){		
				$profileInfo['notify_email'] = explode(',',$profileInfo['notify_email']);
				$profileInfo['notify_sms'] = explode(',',$profileInfo['notify_sms']);
				$profileInfo['test_type'] = explode(',',$profileInfo['test_type']);
				$profileInfo['browser'] = explode(',',$profileInfo['browser']);
				$profileInfo['apps'] = explode(',',$profileInfo['apps']);
				$profileInfo['phone_use'] = explode(',',$profileInfo['phone_use']);
				
				$form->populate($profileInfo);
				
				$this->view->profileMobster = $form;
			}
		}
	}
    
	public function researcherAction()
	{
		$this->_helper->layout->setLayout('profile');
		$this->view->headLink()->appendStylesheet('/css/forms.css');

		// Get (or Add) profile contact info		
  		$update = new ProfileContactInfo();
		$profile = $update->getProfile($this->view->currentUser->id);
		$this->view->profile = $profile;
			
			
		// Check for record	
		$profileTable = new ProfileResearcher();
		try{
			$profileInfo = $profileTable->getProfile($this->view->currentUser->id);					
			$form = new Form_ProfileResearcher;
		}catch(Exception $e){
			// no record exists
			$form = new Form_ProfileResearcherApply;
		}

		$this->view->profileResearcher=$form;	
	
	
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
						
			if (isset($formData['apply'])) // User wants to start a mobster account
			{
				$profileTable->add($this->view->currentUser->id);	
				$form = new Form_ProfileResearcher;
				$this->view->profileResearcher=$form;
			}
			else
			{
						
				if ($form->isValid($formData))
				{
					$formValues = $form->getValues();
	
					$update = new ProfileResearcher();
					
					try{
						$update->updateProfile($this->view->currentUser->id, $formValues);
					}catch(Exception $e){
						echo $e->getMessage().' error';
					}
					
					$this->_redirect('/app/');			
	
					
				}//end if statement isValid()
	
				else
				{
					$form->populate($formData);
				}
			}

		}// end if ispost()	
		else
		{
		
			if(isset($profileInfo)){
							
				$profileInfo['notify_email'] = explode(',',$profileInfo['notify_email']);
				$profileInfo['notify_sms'] = explode(',',$profileInfo['notify_sms']);

				
				
				$form->populate($profileInfo);
			}
			
			$this->view->profileResearcher = $form;

		}
	}
	//
	//  View public profile works with user id or username	
	//
	public function viewAction()
	{
		$this->view->headScript()->appendFile('/js/contactlist/contacts.js');
			
		$this->_helper->layout->setLayout('profile');
		
		$this->view->id = 0;
		
		$users = new Users();
		
		$parameters = $this->_getAllParams();
		if (!isset($parameters['id']) && !isset($parameters['user'])){
			$this->view->id=$this->view->currentUser->id; // If no id sent then show our own profile
		}elseif (isset($parameters['user'])){
			$this->view->user=$parameters['user'];		
			$user = $users->getUser(0,  $this->view->user);
			if($user)
				$this->view->id = $user['id'];
		}else{
			$this->view->id=$parameters['id'];
		}

		if(!isset($this->view->id) || $this->view->id==0){
			$this->_redirect('/app/profile/view');
		}
			
		
		
		// Header 
		$this->view->contact = $users->getUserAsContact($this->view->id);
		$regdevices=new Regdevices();
		$this->view->handsets = $regdevices->getHandsetsByUser($this->view->id);
				
		// Primary contact info
		$profile=new ProfileContactInfo();
		$this->view->profile=$contactInfo=$profile->getProfile($this->view->id,  $this->view->user);
		
		// Mobster
		$testerratings=new TesterRatings();
		$this->view->testerratings=$testerratings->getRatingsUser($this->view->id,5);
		$profileTable = new ProfileMobster();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id,  $this->view->user);					
			$this->view->mobster = $profileInfo;
		}catch(Exception $e){
			
		}
		
		// Researcher
		$profileTable = new ProfileResearcher();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id , $this->view->user); // id is ignored if username is given so we are still ok					
			$this->view->researcher = $profileInfo;
		}catch(Exception $e){
			
		}
				
		// Developer
		$profileTable = new ProfileDeveloper();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id , $this->view->user);					
			$this->view->developer = $profileInfo;
		}catch(Exception $e){
			
		}
		$developerratings=new DeveloperRatings();
		$this->view->developerratings=$developerratings->getRatingsUser($this->view->id,5);
		
		
		// Groups
		$groups = new Groups(); 
		$this->view->groups = $groups->listOpenGroupsForUser($this->view->id);
		
		
	}

	public function ratingAction()
	{
		if (!($this->view->id=$this->_getParam('id', 0)))
		{
			$this->view->id=$this->view->currentUser->id; // If no id sent then show our own profile
		}
		$testerratings=new TesterRatings();
		$this->view->testerratings=$testerratings->getRatingsUser($this->view->id);
		$developerratings=new DeveloperRatings();
		$this->view->developerratings=$developerratings->getRatingsUser($this->view->id);
	}
	
	
	public function sendsmsAction(){
		
		$this->_helper->layout->disableLayout();
		
		$parameters = $this->_getAllParams();//get all passed in params

		if(!isset($parameters['sms']))
		{
			$this->view->result = "Invalid SMS provided";
		}
		
		
		
		$id = $this->view->currentUser->id;
		$sms = $parameters['sms'];
		
		$pattern = '/[^0-9]*/';
		$sms = preg_replace($pattern,'', $sms);
		
		
		$device = new DeviceActivation(); 
		$result = $device->sendActivation($id,$sms);
		
		if(!$result || $result == "0")
			$this->view->result = "Unable to send SMS at this time. Please try again later, or visit mob4hire.com/mobile to verify your account";
		else
			$this->view->result = "SMS has been sent. If you do not receive your SMS within the next four hours visit mob4hire.com/mobile to verify your account";
	}
	
	public function changepasswordAction(){
		$this->view->headLink()->appendStylesheet('/css/forms.css');
		$form = new Form_ChangePassword(); 
		$this->view->form = $form;
		
		
		if ($this->getRequest()->isPost()) {
			
		 	$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
    
				Zend_Registry::get('defSession')->resetPass = NULL;
	        	
				// Update the users password
				$users = new Users(); 
			
				$userData = array('password'=>$formData['password']);
				$users->editProfile($this->view->currentUser->id, $userData);
	
				$this->_redirect('/app/profile');	
	
			}
		}
	}
		

}//end class