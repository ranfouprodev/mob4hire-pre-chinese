<?php

class App_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }

	public function indexAction()
	{
		fb("Entering App-Index Action");

		$this->view->headScript()->appendFile('/js/notifications/notification.js');
		$this->view->headScript()->appendScript("$(document).ready(function(){showNotifications();});", $type = 'text/javascript', $attrs = array());
		
		// Check if the contactinfo profile is completed.
		$userid = $this->view->currentUser->id;
		$profiles = new ProfileContactInfo();
		$profile = $profiles->getProfile($userid);

		$notifications = new Notifications();

		if( !isset($profile['time_zone'] ) ){
			$message = "<a href='/app/profile/'>".$this->view->translate('app_index_profile_empty')."</a>";
			$post = true;

		$notificationList = $notifications->getNotificationsForUser($userid);
			foreach($notificationList as $n){
				if($n['message'] == $message)
				$post = false;
			}


			if($post)
			$notifications->addNotification($userid,$message);
		}
			
			
		// Add changes to the payments as temporary
		$expiryDate = strtotime("2011-12-31");
		// Automatically expire notification
		if( time() < $expiryDate){
			$message = "<a href='/the-mob/testers-faq'>      We've made changes to our payment policies. Click here for details</a>";
			$notifications->addNotification($userid,$message);
		}
			

		//Append the scripts for the wall
		$this->view->lastLogin=$lastLogin=strtotime(Zend_Registry::get('defSession')->currentUser->last_login);

			
		// Just for checking if there is projects available
		$projecttable = new Projects();
		$this->view->hasProjects=$projecttable->getProjectsByDeveloper($this->view->currentUser->id, 399,1,1)->count();

		$teststable= new Tests();
		$this->view->hasTests=$teststable->listTestsByTester($this->view->currentUser->id,'idproject desc',1,1)->count();


	}

	 
	public function accountDetailsAction()
	{
		$this->_helper->layout->setLayout('profile');
		$accounts= new Accounts();
		$this->view->account=$accounts->listAccounts($this->view->currentUser->id);
	}

	public function depositFundsAction()
	{
		$this->_helper->layout->setLayout('profile');
		$PAYPAL_PERCENTAGE = 3.9;
		$PAYPAL_ADDITIONAL = 0.3;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();


			if (isset($formData['previous'])) // The form was not submitted: the back button was pressed
			{
				$this->_redirect($formData['return']); // what the fuck is this? Is this post value ever set?
				// because it could come from either survey or experience we would like to route to the right page

				//$this->_redirect('/app/project/resources'); // may be survey or experience.
			}
			else if (isset($formData['paymethodhidden'])) // Did we come from project launch? or are we just adding arbitrary funds?
			{
				// Update the project status to Under Review
				// This action is not necessarily instigated by the project launch. I thin maybe this breaks if funds are added ad-hoc
				$projects = new Projects();
				$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
				$projects->updateProject($projectData['idproject'], array('Status'=>101)); // Switch this back before committing!

				// Send the status email to administrators
				$mail = new Mailer();
				$mail->sendDraftModerationEmail($projectData);

					
				// Remove the session variables
				Zend_Registry::get('defSession')->project = NULL;

					
				$isPaypal = ((isset($formData['paymethodhidden']) && $formData['paymethodhidden']==1)?true:false);


				if(!$isPaypal){
					$this->_redirect('/app/project/');
				}
			}
			$amount= number_format($this->_getParam('amount', 0),2);
			if ($amount>0)
			{
				$paypalFee=($amount*$PAYPAL_PERCENTAGE/100.0)+$PAYPAL_ADDITIONAL;
				$this->view->depositAmount = number_format($amount + $paypalFee, 2);
				$this->view->amount=$amount;
				$this->view->paypalFee=number_format($paypalFee, 2);
			}
			else
			{
				$this->_redirect('/app/developer');
			}
		}
		else { //amount can be passed in in the url from escrowfunds or acceptbid

			$amount= number_format($this->_getParam('amount', 0),2);
			if ($amount>0)
			{
				$paypalFee=($amount*$PAYPAL_PERCENTAGE/100.0)+$PAYPAL_ADDITIONAL;
				$this->view->depositAmount = number_format($amount + $paypalFee, 2);
				$this->view->amount=$amount;
				$this->view->paypalFee=number_format($paypalFee, 2);
			}
			else
			{
				$this->view->amount=NULL; // if no value is passed in allow manual entry
			}
		}
	}

	public function ppcancelAction()
	{

	}
	public function ppreturnAction()
	{

	}
	public function withdrawFundsAction()
	{
		$accounts= new Accounts();
		$this->view->balance = $accounts->getBalance($id=$this->view->currentUser->id);
		$this->_helper->layout->setLayout('profile');
			
		$params=$this->_request->getParams();
		if (isset($params['submit'])) // form was submitted
		{
			$amount = (float)$params['amount'];
			$instructions=$params['instructions'];
			$account=$params['act'];
				
			if ($amount >= 50)
			{
				$acocunts= new Accounts();
				if ($accounts->withdrawFunds($id, $amount, '', $instructions, $account)!=-1)
				{
					// successful

					$message = $this->view->translate->_('app_index_withdrawFunds_success');
					$this->view->message=$message;
						
					$mailer = new Mailer();
					$mailer->emailPaypalConfirmation($id, $amount, $account);
				}
				else
				{
					// insufficient funds
					$this->view->message=$this->view->translate->_('app_index_withdrawFunds_nsfError');
				}
			}
			else
			{
				$this->view->message=$this->view->translate->_('app_index_withdrawFunds_minError');
			}
		}
	}


}