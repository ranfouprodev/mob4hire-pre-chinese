<?php

class App_TestController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->view->headScript()->appendFile('/js/wall.js');
 	}
	
	public function chargeuserAction()
	{

	}
/*
	public function indexAction()
	{
		$db=Zend_Registry::get('db');

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$parameters = $this->_getAllParams();

		if ( isset ($parameters['deviceatlasid']))
		{
			$deviceatlasid=$parameters['deviceatlasid'];
			if ( isset ($parameters['iddevice']))
			{
				$iddevice=$parameters['iddevice'];
				//both deviceatlasid and iddevice are set. do the update
				$db->query("UPDATE regdevices SET deviceatlasid=$deviceatlasid WHERE iddevice=$iddevice AND isnull(deviceatlasid)");
				echo "[{deviceatlasid: $deviceatlasid , iddevice: $iddevice}]";
			}

		}
	}
*/
/*
	public function registerdevicesAction()
	{
		$parameters = $this->_getAllParams();

		if ( isset ($parameters['page']))
		{
			$this->view->page=$parameters['page'];
		}
		else
		{
			$this->view->page=1;
		}
 
	}
*/
/*	
	public function indexAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$parameters = $this->_getAllParams();

		if ( isset ($parameters['page']))
		{
			$page=$parameters['page'];
		}
		else
		{
			$page=1;
		}
 		// Throw all the UAs from regdevices at DeviceAtlas (a few at a time)
		$regdevices= new RegDevices();
		$devices=$regdevices->listRegdevices("regdevices.http_user_agent!='' and isnull(regdevices.deviceatlasid)","regdevices.idregdevice",$page,100);
		if($devices->count()) {
			$json='[';
			foreach ($devices as $device) {
					$json.="{useragent: '$device->http_user_agent'";
					$properties=$regdevices->identifyDevice($device->idregdevice);
					if (!$properties)
						$json.=",id: 'Unknown'},";
					else
						$json.=", id:'".((isset($properties['id']))?$properties['id']: 'Unknown')."'},";
			}
			$json = substr($json, 0, -1); // remove final comma		
			$json.= "]";
			echo $json;
		}
		else
			echo "[{useragent:'DONE'}]";
	}
*/
/*
	public function indexAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$parameters = $this->_getAllParams();

		if ( isset ($parameters['page']))
		{
			$page=$parameters['page'];
		}
		else
		{
			$page=1;
		}
 		$devicetable = new Devices();
		// Throw all the UAs from TeraWurfl at DeviceAtlas (a few at a time)
		$terawurfl= new TeraWurfl();
		$devices=$terawurfl->getDevicesWithLImits($page, 100);
		if($devices->count()) {
			$json='[';
			foreach ($devices as $device) {
				if($device->actual_device_root) {
					$json.="{useragent: '$device->user_agent'";
					$properties=$devicetable->identifyDevice($device->user_agent);
					if (!$properties)
						$json.=",id: 'Unknown'},";
					else
						$json.=", id:'".((isset($properties['id']))?$properties['id']: 'Unknown')."'},";
				}
			}
			$json = substr($json, 0, -1); // remove final comma		
			$json.= "]";
			echo $json;
		}
		else
			echo "[{useragent:'DONE'}]";
	}

/*	
	public function indexAction()
	{
		$parameters = $this->_getAllParams();

		if ( isset ($parameters['page']))
		{
			$page=$parameters['page'];
		}
		else
		{
			$page=1;
		}
 		$devicetable = new Devices();
		// Throw all the UAs from TeraWurfl at DeviceAtlas (a few at a time)
		$terawurfl= new TeraWurfl();
		$devices=$terawurfl-> getDevicesWithLImits($page, 100);
		if($devices->count()) {
			foreach ($devices as $device) {
				if($device->actual_device_root) {
					echo $device->deviceID.' ';
					$properties=$devicetable->identifyDevice($device->user_agent);
					echo ((isset($properties['id']))?$properties['id']: 'Unknown').'<br/>';
				}
			}
			$this->_redirect('/app/test/index/page/'.++$page); // redirect to self to fetch next 100 results
		}
		else
			echo "DONE";
	}
	
/*	public function dirlistAction()
	{
		$projectFiles = new ProjectFiles();
		$projecttable=new Projects();
		$projects=$projecttable->fetchAll();
		foreach ($projects as $project)
		{
			echo "<br/>".$project->Name."<br/>";
			//get name of downloads subdirectory
//			$dd=rawurlencode($project->Name);
			$dd=$project->Name;
			echo "------".$dd."<br/>";
			try {
				$iterator = new DirectoryIterator('/var/www/html/mob4hireV2/downloads/'.$dd);
				foreach ($iterator as $fileinfo) {
					if ($fileinfo->isDir() && !$fileinfo->isDot()) {
						if ($fileinfo->getFilename()=='generic')
						{
							// This is a generic executable
							try{
								$iterator2=new DirectoryIterator('/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename());
								foreach ($iterator2 as $fileinfo2)
								{
									if(!$fileinfo2->isDir() && !$fileinfo2->isDot())
									{
										$filePath='/var/www/html/mob4hireV2/downloads/'.$dd.'/generic/'.$fileinfo2->getFilename();
										echo $filePath.'<br/>';
										$fileData = file_get_contents($filePath);
										$fileSize = filesize($filePath);
										$extension= substr(strtolower(strrchr($fileinfo2->getFilename(),'.')),1);
										$mimeType = $this->getMimeType(substr($extension,1));
										echo "idproject:$project->idproject mimeType:$mimeType fileSize:$fileSize category:genFiles<br/>";
										$fileid=$projectFiles->addFileToProject($project->idproject, $fileinfo2->getFilename(), $mimeType,$fileSize,$fileData, "genFiles", "0");						
									}
								}
							}
							catch (Exception $e){echo $e->getMessage();};
						}
						else if(!$fileinfo->isDot())
						{
							// this is a testxxxx directory it may contain specific binaries but screw them
							try{
								$iterator2=new DirectoryIterator('/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename());
								foreach ($iterator2 as $fileinfo2)
								{
									if($fileinfo2->isDir() && !$fileinfo2->isDot())
									{
										try {
	//										echo '<br/>/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename().'<br/>';
											$iterator3=new DirectoryIterator('/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename());
											foreach($iterator3 as $fileinfo3)
											{
												if(!$fileinfo3->isDir() && !$fileinfo3->isDot())
												{
													$filePath='/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename().'/'.$fileinfo3->getFilename();
													echo $filePath.'<br/>';
													$fileData = file_get_contents($filePath);
													$fileSize = filesize($filePath);
													$extension= substr(strtolower(strrchr($fileinfo3->getFilename(),'.')),1);
													$mimeType = $this->getMimeType(substr($extension,1));
													$category='tsc'.substr($fileinfo->getFilename(),4);
													echo "idproject:$project->idproject mimeType:$mimeType fileSize:$fileSize category:$category<br/>";
													$fileid=$projectFiles->addFileToProject($project->idproject, $fileinfo3->getFilename(), $mimeType,$fileSize,$fileData, $category, "0");
												}
											}
										}
										catch (Exception $e){echo $e->getMessage();};
									}
								}
							}
							catch (Exception $e){echo $e->getMessage();};
							
						}
					   
					}
					else if(!$fileinfo->isDot())
					{
						// These are test case files
						$filePath= '/var/www/html/mob4hireV2/downloads/'.$dd.'/'.$fileinfo->getFilename();
						echo $filePath.'<br/>';
						$fileData = file_get_contents($filePath);
						$fileSize = filesize($filePath);
						$extension= substr(strtolower(strrchr($fileinfo->getFilename(),'.')),1);
						$mimeType = $this->getMimeType(substr($extension,1));
						echo "idproject:$project->idproject mimeType:$mimeType fileSize:$fileSize category:uploadPlan<br/>";
						$fileid=$projectFiles->addFileToProject($project->idproject, $fileinfo->getFilename(), $mimeType,$fileSize,$fileData, "uploadPlan", "0");						
					}
				}
			}
			catch (Exception $e){echo $e->getMessage();};
		}
	}

protected function getMimeType($ext){
	

		
		$mimetypes = array(
		 "doc" => "application/msword", 
         "pdf" => "application/pdf", 
         "bmp" => "image/bmp", 
         "gif" => "image/gif", 
         "jpeg" => "image/jpeg", 
         "jpg" => "image/jpeg", 
         "jpe" => "image/jpeg", 
         "png" => "image/png", 
         "txt" => "text/plain", 
     	);
		
		
	  if (isset($mimetypes[$ext])) { 
         return $mimetypes[$ext]; 
    
      } else { 
         return 'application/octet-stream'; 
      }
		
	}
*/	
	/*
	 * displays sample email (also send it)
	 */
	public function emailAction(){
		$mailer = new Mailer(); 
		
		$timestamp = time() - (24 * 60 * 60); // 24h ago
		
		//$email = $mailer->sendJoinGroupRequest(61835,10);
		$email = $mailer->sendGroupDailyDigest(102063,10,$timestamp);
		
		$this->view->email = $email; 
		
	}
	
} //end class