<?php

/*
 * Ajax module for the generic comment classes
 */
class App_WallController extends Zend_Controller_Action
{

	protected $s3;

	public function init() {
		$this->s3 = Zend_Registry::get('S3');
		$this->imagebucket = Zend_Registry::get('configuration')->S3->imagebucket;
		$this->securebucket = Zend_Registry::get('configuration')->S3->securebucket;
		$this->imageurl = Zend_Registry::get('configuration')->S3->imageurl;

		$this->session = new Zend_Session_Namespace('User');
		$this->view->translate = Zend_Registry::get('Zend_Translate');
	}



	public function indexAction()
	{
		$this->_redirect("/app/inbox");		
	}

	
	// Just returns the number of unread messages, for dynamic updates
	public function getunreadAction()
	{
		$this->_helper->layout->disableLayout();
		
		$wall = new Wall(); 
		$this->view->unread = $wall->countUnread($this->view->currentUser->id); 
		
	}
	
	/*
	 * This will get a wall based on what parameter is set.  If idtest is set it will get a test wall
	 * if idproject is set it will get a project wall
	 * if iduser is set it will get a user wall
	 */
	public function getwallAction()
	{
		$parameters = $this->_getAllParams();

		if (isset($parameters['idtarget']) && isset($parameters['postType']))
		{
			$this->view->idtarget=$idtarget=$parameters['idtarget'];
			$this->view->postType=$postType=$parameters['postType'];

			// Add the comment box at the top
			if(isset($parameters['showcomment'])){
				$showcomment = strtolower($parameters['showcomment']);
				if($showcomment == "true")
					$this->view->showcomment = true;
				else 
					 $this->view->showcomment = false; 
			}

			
			// Get the filter parameter
			// Used to add special filters for the message type not covered elsewhere
			if(isset($parameters['filter'])){
				$filter = $parameters['filter'];

				if($filter == "new")
					$this->view->filter = "s.status is null or s.status = 100"; 
			}
			
				
			$this->view->resultsPerPage=$resultsPerPage=4;
			$this->view->page=$page=1;
			$this->view->timestamp=$timestamp=0;

			if(isset($parameters['resultsPerPage']))
				$this->view->resultsPerPage=$resultsPerPage = $parameters['resultsPerPage'];
				
			if(isset($parameters['page']))
				$this->view->page=$page = $parameters['page'];

			if(isset($parameters['timestamp']))
				$this->view->timestamp = $timestamp = $parameters['timestamp'];
			
			$options = array(); 
			if(isset($parameters['layout']))
				$options['layout'] = $parameters['layout'];		
			$this->options = $options;	
			

			//fb($idtarget, 'idtarget');
			//fb($postType, 'postType');
			$wall=new Wall();

			if(!$this->hasAccess($idtarget,$postType))
			{
				$this->_redirect("/app/wall/error");
			}
				
							
				
		}else{
			$this->_redirect("/app/wall/error");
		}


	}


	/*
	 * This will need two parameters in it to make sure that we return the correct view
	 * idmessage - the message id that you want to delete
	 * postType - this will let me get back to the correct view.
	 * 			  the values are 1 -> Project Post
	 * 							 2 -> Test Post
	 * 							 3 -> Personal User Post
	 */
	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
		if (isset($parameters['idmessage']) && isset($parameters['postType']))
		{
				
			$idmessage=$parameters['idmessage'];
			$iduser=Zend_Registry::get('defSession')->currentUser->id;
			$wall = new Wall();
			if($wall->canDelete($idmessage, $iduser))
			{
				$this->view->idtarget=$idtarget = $wall->deletePost($idmessage, $iduser);
				$this->view->postType=$postType = $parameters['postType'];
				if($this->hasAccess($idtarget,$postType))
				{
					if($postType==1)
					{
						//Project wall
						$wallData =$wall->getProjectWall($idtarget);
						$this->view->wall=$wallData;
					}
					elseif($postType==2)
					{
						//Test Wall
						$wallData =$wall->getTestWall($idtarget);
						$this->view->wall=$wallData;
					}
					elseif($postType==3)
					{
						//User Wall
						$wallData =$wall->getUserWall($iduser, $this->view->currentUser->id);
						$this->view->wall=$wallData;
					}
					else
					{
						$this->view->error = "Invalid type of wall postType";
						$this->_redirect("/app/wall/error");
					}
				}
				else
				{
					$this->_redirect("/app/wall/error");
				}

			}
			else
			{
				$this->view->error = "The user doesn't have access to delete this message";
				$this->_redirect("/app/wall/error");
			}

		}
	}

	protected function canDelete($idmessage)
	{
		$iduser=Zend_Registry::get('defSession')->currentUser->id;
		$wall = new Wall();
		$canDelete = $wall->canDelete($idmessage, $iduser);
		return $canDelete;
	}

	/*
	 * This has several parameter that must be in it
	 * message - this is the text portion of a message
	 * idtarget - this is the int(10) unsigned id value associated with the target of the post
	 * postType - this will tell the system what kind of post to a wall is it supposed to be
	 * 			  the values are 1 -> Project Post
	 * 							 2 -> Test Post
	 * 							 3 -> Personal User Post
	 * The last parameter is only needed if this add is a reply to a thread that would be the
	 * idparent
	 */
	public function addAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();


		if (isset($parameters['idtarget']) && isset($parameters['message']) && isset($parameters['postType']))
		{
			$wall = new Wall();
			$this->view->idtarget=$idtarget = $parameters['idtarget'];
			$message = $parameters['message'];
			$idposter =  Zend_Registry::get('defSession')->currentUser->id;
			$this->view->postType=$postType = $parameters['postType'];


			$idparent=0;
			if($this->hasAccess($idtarget,$postType))
			{
				if(isset($parameters['idparent']))
				{
					$idparent=$parameters['idparent'];
				}

				if((isset($parameters['id']) && $id=(int)$parameters['id'])) // id has a value if we are updating the post after uploading stuff
				{
					$idparent =$wall->editMessage($id, $idposter, $message);
				}
				else
				{
					$messageId = $wall->postOnWall($idposter, $message, $idtarget, $postType, $idparent);
					$idparent = $messageId;
				}

				$wallData =$wall->getWallThread($idposter,$idparent);
				$this->view->wall=$wallData;

			}
			else
			{
				$this->view->error = "The user doesn't have access to add a message";
				$this->_redirect("/app/wall/error");
			}

				
		}
	}

	public function preaddAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
		if ( isset ($parameters['idtarget']) && isset($parameters['message']) && isset($parameters['postType']))
		{
			$wall = new Wall();
			$this->view->idtarget=$idtarget = $parameters['idtarget'];
			$message = $parameters['message'];
			$idposter =  Zend_Registry::get('defSession')->currentUser->id;
			$this->view->postType = $postType = $parameters['postType'];
			$idparent=0;
				
			if(isset($parameters['idparent']))
			{
				$idparent=$parameters['idparent'];
			}
			
			if($this->hasAccess($idtarget,$postType))
			{
				$id=$wall->postOnWall($idposter, $message, $idtarget, $postType, $idparent);

				$this->view->idmessage=$id;
				$this->view->idparent=$idparent;
				
			}
			else
			{
				$this->view->error = "The user doesn't have access here so go away";
				$this->_redirect("/app/wall/error");
			}
				
				
		}
	}

	public function uploadAction()
	{
		$this->_helper->layout->disableLayout();

		//todo: check mime types to avoid fake extensions
		$allowedfiletypes=array(".pdf",".doc",".xls",".docx",".xlsx",".rtf",".txt",".zip",".rar",".jpg",".jpeg",".gif",".png"); // This validation has already been done on the client side but better safe than sorry

		$parameters = $this->_getAllParams();//get all passed in params

		try{

			if(($filecontrolname=$this->getRequest()->getParam('file')))
			{
				$extension= strtolower(strrchr($_FILES[$filecontrolname]['name'],'.'));

				//if (in_array($extension, $allowedfiletypes))
				//{
					$randName = time().mt_rand(100, 999).$extension;

					try{
						$mimeType = $this->getMimeType(substr($extension,1));
					}catch(Exception $e){
						$mimeType = substr($extension,1);
					}

					$fileData = file_get_contents($_FILES[$filecontrolname]['tmp_name']);
					$fileSize = filesize($_FILES[$filecontrolname]['tmp_name']);

					$baseFilename = basename( $_FILES[$filecontrolname]['name']);

					$projectFiles = new ProjectFiles();

					$idproject = 0;

					$this->s3->putObject($this->securebucket.'/'.$idproject.'/'.$randName, $fileData,
						array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
							Zend_Service_Amazon_S3::S3_ACL_PRIVATE));

					$fileurl = $this->imageurl.$this->securebucket."/".$idproject."/".$randName;
					$fileid=$projectFiles->addFileToProject($idproject, $randName, $mimeType,$fileSize,'', $filecontrolname, "2");
					$projectFiles->updateProjectFile($fileid, array("physical_filename"=>$fileurl));

					$idmessage=substr($filecontrolname, 3); // strip off the wal from the file control name

					$wall=new Wall();
					$wall->updateWall($idmessage, array('hasFile'=>1));

				$wallFile = new WallFile();
					$wallFile->add($idmessage, $fileid);

				//}
				//else
				//{
				//	$this->view->error=" That filetype is not supported at this time";
				//}
					
				$this->view->filecontrolname=$filecontrolname;

				$files = new ProjectFiles();
				$this->view->projectFiles=$files->getFilesForProjectByCategory(0, $filecontrolname);
					
			}
		}catch(Exception $e){
			echo "exception:".$e->getMessage();
			$this->view->error = "We are unable to upload that file at this time";
		}
	}

	public function removefileAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params

		try{

			if(isset($parameters['id']))
			{
				$files = new ProjectFiles();
				$wall = new Wall();

				$projectFile=$files->getFile($parameters['id']);

				$fcn=$projectFile['category'];

				// make sure this is a wall file and that we have permission to delete it
				if(substr($fcn,0,3)=='wal' && $wall->canDelete(substr($fcn,3), $this->view->currentUser->id))
				{ // Todo: Check the logged in user is allowed to do this
					$files->removeFileFromProject($projectFile['idfiles']);
				}else{
					$this->view->error = "You do not have sufficient privileges to modify that file";
				}


				if(isset($fcn) && isset($projectFile))
				{
					$this->view->projectFiles=$files->getFilesForProjectByCategory(0 ,$fcn); // does not belong to a project send 0
					$this->view->filecontrolname=$fcn;
				}

			}
				
		}catch(Exception $e){
			$this->view->error = "We are unable to delete the file at this time";
		}
	}

	protected function getMimeType($ext)
	{
		$mimetypes = array(
			"doc" => "application/msword", 
			"pdf" => "application/pdf", 
			"bmp" => "image/bmp", 
			"gif" => "image/gif", 
			"jpeg" => "image/jpeg", 
			"jpg" => "image/jpeg", 
			"jpe" => "image/jpeg", 
			"png" => "image/png", 
			"txt" => "text/plain", 
		);


		if (isset($mimetypes[$ext])) {
			return $mimetypes[$ext];

		} else {
			return 'application/octet-stream';
		}

	}

	protected function hasAccess($idtarget,$postType)
	{
		//todo: write an access control function to determine whether the logged in user has permissions to perform this action
		// This shit can not go live until it is done
		$iduser=$this->view->currentUser->id;
		$wall = new Wall();
		$result=$wall->hasAccess($iduser, $idtarget, $postType);
		if($result==0)
		{
			//Maybe do some redirect now
			$this->view->error = "The user doesn't have access to this page";
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function errorAction()
	{

	}

	/*
	 * Testing below here
	 */

	public function v2Action(){
		$this->_helper->layout->disableLayout();
		$projects = new Projects();
		$this->view->project = $projects->getProject(524);
		$this->view->lastLogin=$lastLogin=strtotime(Zend_Registry::get('defSession')->currentUser->last_login);
	
	}

}//end class