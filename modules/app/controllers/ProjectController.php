<?php
class App_ProjectController extends Zend_Controller_Action
{

	protected $s3;

	public function init() {
		$this->s3 = Zend_Registry::get('S3');
		$this->imagebucket = Zend_Registry::get('configuration')->S3->imagebucket;
		$this->securebucket = Zend_Registry::get('configuration')->S3->securebucket;

	    $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
	}




	/***
	 *
	* This method should duplicate the /project page. List the projects table.
	*
	*
	*/
	public function indexAction()
	{
		$profiledeveloper=new ProfileDeveloper();
		if($profiledeveloper->hasProfile(Zend_Registry::get('defSession')->currentUser->id)) {
			// developer
			$this->_redirect('/app/developer'); // redirect to developer project list
		}
		else { // mobster
			$this->_redirect('/app/project/compatible'); // redirect to compatible projects list
		}
	}

	// we will use this for the projects I can bid on thing
	public function compatibleAction()
	{
		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "compatible";var navigation = "none";');

	}

	public function mobsterAction()
	{
		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "tester";var navigation = "none";');


	}

	public function draftAction()
	{
		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "draft";var navigation = "none";');

	}

	public function developerAction()
	{
		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "developer";var navigation = "none";');


	}


	public function searchAction()
	{

		$parameters = $this->_getAllParams();

		if(isset($parameters['nolayout']))
		$this->_helper->layout->disableLayout();

		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->page=$page=1;
			
		if(isset($parameters['search']))
		$search = $parameters['search'];

		if(isset($parameters['resultsPerPage']))
		$this->view->resultsPerPage=$resultsPerPage = $parameters['resultsPerPage'];

		if(isset($parameters['page']))
		$this->view->page=$page = $parameters['page'];

		$options = array('showcomments' =>false,'search'=>$search);

		$this->view->options = $options;

		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "custom";var navigation = "top";var search = "'.$search.'"');

	}



	public function allAction()
	{
		$this->view->headScript()->appendFile('/js/projects/list.js');
		//$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "all";var navigation = "top";');


	}
	/*
	 * Used to remove a project from draft stage
	*/
	public function deleteAction(){

		$parameters = $this->_getAllParams();//get all passed in params


		$hasDeleted = false;

		if (isset ($parameters['id'])){
			 
			$idproject = $parameters['id'];
				
			$projects = new Projects();
			$project = $projects->getProject($idproject);

			$canDelete = false;
				
			if(!isset($project)){
				$canDelete = false;
			}if(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canDelete = true;
			}elseif ($project['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id && $project['Status']=="100"){
				$canDelete = true;
			}

			if($canDelete){
				$projects->updateProject($idproject,array('Status'=>501));
				$hasDeleted = true;
			}
				
		}

		if(!$hasDeleted){
			$this->_redirect('/app/project/', array ('exit'=>true));
		}

	}


	/***
	 *
	* The project controller will handle the display and details of the project. The index action serves
	* to display the project details and has different views for not logged in (public), logged in (tester) and
	* logged in (developer of the project).
	*
	*/
	public function viewAction()
	{
		$parameters = $this->_getAllParams();//get all passed in params

		//looking for parameter called idproject as in eg: ?idproject=1
		//if no params passed in then exit page...
		if (! isset ($parameters['idproject']))
		{
			$this->_redirect('/app/project/', array ('exit'=>true));
		}

		$idproject = $parameters['idproject'];
		$this->view->idproject=$idproject;
		$pj = new Projects();
		$pd = $pj->getProject($idproject);

		if (! isset ($pd))
		{
			$this->_redirect('/app/project/', array ('exit'=>true));
		}

		if(($pd['Status'] <= 200 || $pd['Status'] > 401) && (Zend_Registry::get('defSession')->currentUser->id !=$pd['iddeveloper']) && (!Zend_Registry::get('defSession')->currentUser->isAdmin))
		{
			$this->_redirect('/app/project/', array ('exit'=>true));
		}

		// Not sure if this is still valid
		if($pd['projecttype'] =='survey'){
			// reroute
			$this->_redirect('/app/survey/view/idproject/'.$idproject);
		}


	  
		if ( isset ($parameters['popup'])) // When the page is to pop up in a new window, turn off the layout
		{
			 
			$this->_helper->layout->setLayout('popup');
			$this->view->headLink()->appendStylesheet('/css/rating.css');// We have to add style sheets back in since these are included by the layout...
			$this->view->headLink()->appendStylesheet('/css/app/master.css');
			$this->view->headLink()->appendStylesheet('/css/forms.css');
			$this->view->headScript()->appendFile('/js/application.js');
		}

		//ok we have a param called idproject
		$projectId = $parameters['idproject']; //get the value and store it in $projectId variable

		$this->view->projectTable=$projectTable = new Projects(); //create new instance of Projects table

		$this->view->proj = $projectTable->getProject($projectId); //pass this row back to the view based on parameter passed in as idproject
		//didn't we already do this earlier?

		$form = new Form_Comments;

		$this->view->comments = $form;
	}



	/*
	 * will move all the db project files to s3.
	*/
	public function syncprojectsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$pf = new ProjectFiles();
		$projectFiles = $pf->getAllFilesByType("projImg");

		$response = $this->_response;
		$body = "Moving Project Files<br/>";

		foreach($projectFiles as $projectFile){
			$body .= '--'.$projectFile['idfiles'].' Project:'.$projectFile['idproject'].'<br/>';

			
			$fileid = $projectFile['idfiles'];
			$filedata = $projectFile['filedata'];
			$idproject = $projectFile['idproject'];
			$filename = $projectFile['filename'];
				
			$location = $projectFile['physical_filename'];

			// Load the file from the secure drive and port to the open images drive
			if(isset($location)  &&  strpos($location,"s3.amazonaws.com".$this->securebucket) )
			{
				$file = substr($location,strpos($location,".com/")+5);
					
				$fileData = $this->s3->getObject($file);
				
				$randName = time().mt_rand(100, 999);
				
				$extension = str_replace(".","",strtolower(strrchr($filename, '.')));
					
				$tempName = 'temp'.$randName.".".$extension;

					
				$this->s3->putObject($this->imagebucket.'/'.$tempName, $fileData,
				array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
				Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));
				
				$newimageurl = "http://s3.amazonaws.com/".$this->imagebucket."/".$tempName;
				
				$imageType = exif_imagetype($newimageurl);
				if($imageType == false || ($imageType != IMAGETYPE_GIF && $imageType != IMAGETYPE_JPEG && $imageType != IMAGETYPE_PNG))
				{
					throw new Exception('Only png, gif or jpeg images are allowed.');
				}
				
				
				// Resize the image
			switch(image_type_to_extension($imageType,false)){
				case "gif":
					$image = imagecreatefromgif($newimageurl);
					break;
				case "jpeg":
					$image = imagecreatefromjpeg($newimageurl);
					break;
				case "png":
					$image = imagecreatefrompng($newimageurl);
					break;
			}
				
			list($w, $h) = getimagesize($newimageurl);
				
				if($w > $h){
					$ratio = 100 / $w;
					$width = 100;
					$height = (int) ($h * $ratio);
				}else{
					$ratio = 100 / $h;
					$height = 100;
					$width = (int) ($w * $ratio);
				}
				
				
				$newimage = imagecreatetruecolor($width, $height);
				
				$tempDir = "$_SERVER[DOCUMENT_ROOT]/uploads/";
				
				
				imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $w, $h);
				
				imagejpeg($newimage,$tempDir.$randName ,100);
				
				$newData = file_get_contents($tempDir.$randName);
				
				imagedestroy($image);
				
				$randName .= '.jpg';
				
				$this->s3->putObject($this->imagebucket.'/'.$randName, $fileData,
				array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
				Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));
				
				$imageurl = "http://s3.amazonaws.com/".$$this->imagebucket."/".$randName;
				
				$pf->updateProjectFile($fileid, array("filedata"=>'',"physical_filename"=>$imageurl));
				
				// update the project
				$projects = new Projects();
					
				$projectData = array(
								'ImagePath'=>$imageurl,
				);
					
				$projects->updateProject($idproject,$projectData );
				
				
				$this->s3->removeObject($this->imagebucket.'/'.$tempName, $fileData,
				array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
				Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));
				
			}
				
		}

		$response->setBody($body);
	}


	/*
	 * will move all the db project files to s3.
	*/
	public function syncAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$projects = new ProjectFiles();
		$projectFiles = $projects->getAllFilesStoredInDb();


		$response = $this->_response;
		$body = "Moving Project Files<br/>";

		foreach($projectFiles as $projectFile){
			
			if($projectFile['category']!='projImg')
			{
				$body .= '--'.$projectFile['idfiles'].' Project:'.$projectFile['idproject'].'<br/>';
	
				$fileid = $projectFile['idfiles'];
				$filedata = $projectFile['filedata'];
				$idproject = $projectFile['idproject'];
				$filename = $projectFile['filename'];

	
				$this->s3->putObject($this->securebucket.'/'.$idproject.'/'.$filename, $filedata,
				array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
				Zend_Service_Amazon_S3::S3_ACL_PRIVATE));
	
				$fileurl = "http://s3.amazonaws.com/".$this->securebucket."/".$idproject."/".$filename;
					
	
				$projects->updateProjectFile($fileid, array("filedata"=>'',"physical_filename"=>$fileurl));
			}
				
		}

		$response->setBody($body);
	}

	/*
	 * Syncs all the user profiles with the s3 storage
	*/
	public function syncprofileAction(){

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$response = $this->_response;
		$body = "Moving Profile Images<br/>";

		$projects = new ProjectFiles();

		$users = new Users();
		$userList = $users->listUsersForSearch("p.image IS NOT NULL && p.image like '%/content/image%'");

		foreach($userList as $user){
			$fileid = trim(strrchr($user['Avatar'], '/'),'/');
				
			$userid = $user['iduser'];
				
			$projectFile = $projects->getFile($fileid);
				
			$filename = $projectFile['filename'];
			$filedata = $projectFile['filedata'];


			$this->s3->putObject($this->imagebucket.'/'.$filename, $filedata,
			array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
			Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));
				
			$imageurl = "http://s3.amazonaws.com/".$this->imagebucket."/".$filename;
				
			$projects->updateProjectFile($fileid, array("filedata"=>'',"physical_filename"=>$imageurl));
				
			// Write the image URL to the database here
			$profile= new ProfileContactInfo();
			$profile->updateProfile($userid, array('image'=>$imageurl));
				
			$body .= "User:".$user['username'].' Image:'.$user['Avatar'].' id:'.$fileid.' to:'.$imageurl.'<br/>';
				
		}

		$response->setBody($body);
	}



	// Verification for downloading app files
	public function downloadAction() {

		// Disable view and layout rendering
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$parameters = $this->_getAllParams();

		if ( isset ($parameters['id']))
		{

			$projects = new ProjectFiles();
			$response = $this->_response;
			$idfile = $parameters['id'];
				
			try{
					
					
				$file = $projects->getFile($idfile);
				$idproject = $file['idproject'];

				$test = new Tests();
				$iduser = Zend_Registry::get('defSession')->currentUser->id;
					
				//check permission to download the file
				switch($file['permissions'])
				{
					case "1":
						$project = new Projects();
						$projectData = $project->getProject($file['idproject']);
						if($projectData['iddeveloper'] == $iduser){
							// if this is the owner of the project
							$canDownload = true;
						}elseif($test->isActiveTesterOnProject($idproject, $iduser)){
							$canDownload = true;
						}
						else{
							$canDownload = false;
						}
						break;
					case "2":

						$idproject = 0;
						$idmessage=substr($file['category'], 3);
						// Wall posting
						$wall = new Wall();
						$wallData = $wall->getMessage($idmessage);

						if($wall->canDelete($idmessage,$iduser)){
							$canDownload = true;
						}else{
							$canDownload = false;
						}

						break;
					default:
						$canDownload = true;
				}

				if($canDownload){

					$mimeType = $file['mime_type'];

					$name = $file['filename'];

					$location = $file['physical_filename'];

					$bits = $this->s3->getObject($this->securebucket.'/'.$idproject.'/'.$name);

					// Couldn't find it on AWS, trying URL
					if (strlen($bits) == 0 && isset($location)) {
							$bits = @file_get_contents($location);
					}

					// Finally, trying the database.
					if (strlen($bits) == 0){
						$bits = $file['filedata'];
					}

					if (strlen($bits) == 0) {
						$response->setBody($this->view->translate->_('app_project_download_unavailable'));
					}
					else {
                        $response->setHeader('cache-control', 'no-transform,public,max-age=300,s-maxage=900');
						$response->setHeader('Content-type', 'application/octet-stream', true);
						$response->setHeader('Content-Disposition', 'attachment; filename='.$name, true);
						$response->setBody($bits);
					}
				}else{
					$response->setBody($this->view->translate->_('app_project_download_access'));
				}

			}catch(Exception $e){
				$response->setBody($this->view->translate->_('app_project_download_error') );
			}

		}

	}

	public function clearAction(){

		Zend_Registry::get('defSession')->project = NULL;
		if ( isset ($parameters['idproject'])) {
			$idproject = $parameters['idproject'];
			$this->_redirect('/app/project/definition/' . $idproject);
		}

	}


	public function definitionAction()
	{

		// Only a developer can perform this action
		$this->checkDeveloperRole();

		// This is to check if we are editing an existing project
		$parameters = $this->_getAllParams();
		if ( isset ($parameters['idproject']))
		{
			$idproject = $parameters['idproject'];
			$pj = new Projects();
			$pd = $pj->getProject($idproject);

			if($pd['projecttype'] =='survey'){
				// reroute
				$this->_redirect('/app/survey/definition/'.$idproject);
			}elseif($pd['projecttype'] =='experience'){
				// reroute
				$this->_redirect('/app/experience/definition/'.$idproject);
			}elseif($pd['projecttype'] =='accelerator'){
				// reroute
				$this->_redirect('/app/accelerator/definition/'.$idproject);
			}


			// make sure it is the owner
			$canEdit = false;

			if(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canEdit = true;
			}elseif ($pd['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id && $pd['Status'] == 100) {
				$canEdit = true;
			}

			if(!$canEdit)
			{
				$this->_redirect('/app/project/view/'.$idproject);
			}
				
			Zend_Registry::get('defSession')->project = new stdClass ();
			Zend_Registry::get('defSession')->project->idproject = $pd['idproject'];
			Zend_Registry::get('defSession')->project->projecttype = 'test';
		}

		$form = new Form_NewTestDefinition;

		$this->view->definition = $form;

		if ($this->getRequest()->isPost())
		{
			if(isset(Zend_Registry::get('defSession')->tempimage)){
				$projects = new Projects();
				// We will write a record to the projects table even when validation is failed
				if (!(isset(Zend_Registry::get('defSession')->project)) || !(isset(Zend_Registry::get('defSession')->project->idproject)))
				{
					$projectData=array(
									'projecttype'=>'test',
									'iddeveloper'=>$this->view->currentUser->id,
									'Status'=>100 // Draft
					);
					Zend_Registry::get('defSession')->project->idproject = $projects->add($projectData); // write project and store its id in the session
				}
					
				$tempImage = Zend_Registry::get('defSession')->tempimage;

				$imageUrl = $tempImage['imageurl'];

				$projectFiles = new ProjectFiles();

				$fileid = $projectFiles->addFileToProject(Zend_Registry::get('defSession')->project->idproject, $tempImage['basefilename'], $tempImage['mimetype'], $fileSize, '', "projImg", "0");
				$projectFiles->updateProjectFile($fileid, array("physical_filename"=>$imageUrl));

					
				$projectData = array (
						'ImagePath'=>$imageUrl,
				);
					
				Zend_Registry::get('defSession')->tempimage=NULL; // Unset the temporary image so that we don't get duplicates
				$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
					



			}
				
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData))
			{

				$formValues = $form->getValues();
				// write form values to database here
				$projects = new Projects();

				if ($formValues['testType']) {
					$testType=implode(',', $formValues['testType']);
				}
				else {
					$testType=NULL;
				}

				if ($formValues['testCategory']) {
					$testCategory=implode(',', $formValues['testCategory']);
				}
				else {
					$testCategory=NULL;
				}
					
				// maybe instead of the following clumsy operation, we should rename the form elements to match the database
				$projectData = array (
					'projecttype'=>'test',
					'iddeveloper'=>$this->view->currentUser->id,
					'package'=>$formValues['testPkg'],
					'Name'=>$formValues['projName'],
					'description'=>$formValues['projDesc'],
					'longDesc'=>$formValues['projDetail'],
					'inviteonly'=>$formValues['inviteType'],
					'invitemessage'=>$formValues['mobInvite'],
					'flatfee'=>$formValues['transType'],
					'budget'=>$formValues['bidAmount'],
					'StartDate'=>$formValues['sDate'],
					'testenddate'=>$formValues['eDate'],
					'Duration'=>$formValues['projDur'],
					'maxtests'=>$formValues['amtMobsters'],
					'testType'=>$testType,
					'categories'=>$testCategory,
					'testClass'=>$formValues['testClass'],
					'estTime'=>$formValues['estTime'],
					'estData'=>$formValues['estData'],
					'tags'=>$formValues['tags'],
					'Status'=>100 // Draft
				);

				if (isset(Zend_Registry::get('defSession')->project) && Zend_Registry::get('defSession')->project->idproject)
				{
					$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
				}
				else
				{
					Zend_Registry::get('defSession')->project->idproject = $projects->add($projectData); // write project and store its id in the session
				}
				Zend_Registry::get('defSession')->project->projecttype = 'test';
					
				// redirect to next step
				if(isset($formData['next'])) {
					$this->_redirect('/app/project/selectpanel');
				}
				elseif(isset($formData['saveDraft'])) {
					Zend_Registry::get('defSession')->project = NULL;
					$this->_redirect('/app/project/draft');
				}

			}
		}
		else
		{
			// if the idproject session variable is set then populate the form
			//if(Zend_Registry::get('defSession')->project && property_exists(Zend_Registry::get('defSession')->project, 'idproject'))
			if (Zend_Registry::get('defSession')->project && isset (Zend_Registry::get('defSession')->project->idproject) && Zend_Registry::get('defSession')->project->projecttype =='test')
			{
				$projects = new Projects();
				$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
				$formData = array (
					'testPkg'=>$projectData['package'],
					'projName'=>$projectData['Name'],
					'projDesc'=>$projectData['description'],
					'projDetail'=>$projectData['longDesc'],
					'inviteType'=>$projectData['inviteonly'],
					'mobInvite'=>$projectData['invitemessage'],
					'transType'=>$projectData['flatfee'],
					'amtMobsters'=>$projectData['maxtests'],
					'bidAmount'=>$projectData['budget'],
					'sDate'=>$projectData['StartDate'],
					'eDate'=>$projectData['testenddate'],
					'projDur'=>$projectData['Duration'],
					'testType'=>explode(',', $projectData['testType']),
					'testCategory'=>explode(',', $projectData['categories']),
					'testClass'=>$projectData['testClass'],
					'estTime'=>$projectData['estTime'],
					'estData'=>$projectData['estData'],
					'otherReq'=>$projectData['otherReq'],
					'tags'=>$projectData['tags'],
				);
				$form->populate($formData);
			}
			else
			{
				// We need to create the session variable here to store the image
				Zend_Registry::get('defSession')->project = new stdClass ();
				Zend_Registry::get('defSession')->project->projecttype = 'test';
			}
		}

	}

	public function selectpanelAction()
	{
		if(!isset(Zend_Registry::get('defSession')->project->idproject)){
			$this->_redirect('/app/project/definition');
		}

		$this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
		$this->view->headScript()->appendFile('/js/devices/list.js');
		$this->view->headScript()->appendFile('/js/networks/addnetworksproject.js');
		$this->view->headScript()->appendFile('/js/handsets/addhandsetsproject.js');

		$form = new Form_NewTestPanelSelection;
		$this->view->selectpanel = $form;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();
				// write form values to database here
				$projects = new Projects();
				 
				// maybe instead of the following clumsy operation, we should rename the form elements to match the database
				$projectData = array (
					'region'=>$formValues['region'],
				);
				if (Zend_Registry::get('defSession')->project->idproject)
				{
					$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
				}

				// Check and add invited mobsters.
				$invites = new TesterInvites();
				if(isset($formValues['contactsSelect']))
				{
					$userlist=explode(',',$formValues['contactsSelect']);
					if(sizeof($userlist) > 0)
					$invites->addInvitesByName(Zend_Registry::get('defSession')->project->idproject,$userlist);
				}
				if ( isset ($formData['next']))
				$this->_redirect('/app/project/resources');
				elseif ( isset ($formData['previous']))
				$this->_redirect('/app/project/definition');
				elseif(isset($formData['saveDraft'])) {
					Zend_Registry::get('defSession')->project = NULL;
					$this->_redirect('/app/project/draft');
				}
			}
		}
		else
		{
			// if the idproject session variable is set then populate the form
			if (Zend_Registry::get('defSession')->project->idproject)
			{
				$projects = new Projects();
				$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
				$formData = array (
					'amtMobsters'=>$projectData['maxtests'],
					'region'=>$projectData['region'],
				);

				$invites = new TesterInvites();
				$inviteList = $invites->getInvites(Zend_Registry::get('defSession')->project->idproject);


				if(sizeof($inviteList)>0){
					$inviteStr =" ";
					foreach ($inviteList as $invite){
						$inviteStr .= $invite['username'].',';
					}
					$formData['contactsSelect'] = substr($inviteStr,0,-1);
				}
				$form->populate($formData);
			}
		}

	}

	public function resourcesAction()
	{
		if(!isset(Zend_Registry::get('defSession')->project->idproject)){
			$this->_redirect('/app/project/definition');
		}
		$this->view->headScript()->appendFile('/js/files/list.js');

		$form = new Form_NewTestResources;
		$this->view->resources = $form;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();
				// write form values to database here
				$projects = new Projects();
				// maybe instead of the following clumsy operation, we should rename the form elements to match the database
				$projectData = array (
					'instructions'=>$formValues['reportInstructions'],
					'nda'=>$formValues['nda'],
					'weburl'=>$formValues['mobURL'],
				);
				if (Zend_Registry::get('defSession')->project->idproject)
				{
					$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
				}
				if ( isset ($formData['next']))
				$this->_redirect('/app/project/estimates');
				elseif ( isset ($formData['previous']))
				$this->_redirect('/app/project/selectpanel');
				elseif(isset($formData['saveDraft'])) {
					Zend_Registry::get('defSession')->project = NULL;
					$this->_redirect('/app/project/draft');
				}
			}
		}
		else
		{
			// if the idproject session variable is set then populate the form
			if (Zend_Registry::get('defSession')->project->idproject)
			{
				$projects = new Projects();
				$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
				$formData = array (
					'reportInstructions'=>$projectData['instructions'],
					'nda'=>$projectData['nda'],
					'mobURL'=>$projectData['weburl'],
				); //todo: Save all the upload paths from the session here
				$form->populate($formData);
			}
		}

	}

	public function estimatesAction()
	{
		if(!isset(Zend_Registry::get('defSession')->project->idproject)){
			$this->_redirect('/app/project/definition');
		}

		$form = new Form_NewTestEstimates;
		// $this->view->headScript()->appendFile('/js/estimatesPage.js');
		$this->view->estimates = $form;

		//estimates table
		$profiletable = new ProfileContactInfo();
		$profile = $profiletable->getProfile($this->view->currentUser->id);
		$invData = array (
			"billto"=>$profile['username'],
			"est#"=>$profile['id'].'-'.Zend_Registry::get('defSession')->project->idproject,
			"billToName"=>$profile['first_name']." ".$profile['last_name'],
			"mob"=>"Mob4hire Inc.",
			"billtoAddr"=>"",
			"mobAddr"=>"",
			"billtoCity"=>$profile['city'],
			"mobCity"=>"Calgary, Alberta",
			"billtoCountry"=>$profile['country'].' '.$profile['postcode'],
			"mobCountry"=>"Canada T2E 6M7",
			"billtoInfo"=>"Att: ".$profile['first_name']." ".$profile['last_name'],
			"mobInfo"=>"Inquiries: accounting@mob4hire.com");

		$projects = new Projects();
		$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
		$this->view->projectData = $projectData;

		$subtotal = 0;
		if($projectData['package'] == 2){
			$premiumfee = Zend_Registry::get('configuration')->m4h->premiumTestFee;
			$subtotal += $premiumfee;
		}

		if($projectData['flatfee'] == 1) // Fixed Fee
		{
			$this->view->fixedfee = $fixedFee = $projectData['maxtests'] * $projectData['budget'];
			$subtotal += $fixedFee;
				
			$this->view->commission = $commission = $fixedFee * 0.19;
			$subtotal += $commission;
		}

		if($projectData['package'] == 3){
			$subtotal=0;
		}

		$this->view->subtotal = $subtotal;



		$this->view->estimate = $invData;
		$form2 = new Form_NewTestEstimatesPt2;
		$form2->setAction('/app/index/deposit-funds')->setMethod('post');
		$this->view->estimatesPt2 = $form2;

		$testTp = array (1=>"Basic", 2=>"Premium", 3=>"Managed");
		$this->view->testType = $testTp[$this->view->projectData['package']];

		//This is really nasty, but I do not know how else to do it without a major retooling
		$form->getElement('projReview2')->setContent("<td><span class='mobblack'>".$this->view->projectReview($this->view->projectData)."</span></td>");


		// Form is redirected to the paypal deposit page. We will never get here
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				// write form values to database here
				print_r($formData);

				if ( isset ($formData['next']))
				{
					// need to unset the session variables here. Probably would be better if they were all in an array so they could be unset as one todo
					// $this->_redirect('/app/project/estimates'); // Not sure what to do here: need to redirect to paypal if paypal selected, confirmation message otherwise

					 
					// If the account balance requires funds go to paypal if not back to dashboard
					//$this->_forward('/app/index/depositfunds');

				}
				elseif ( isset ($formData['previous']))
				$this->_redirect('/app/project/resources');
				elseif(isset($formData['saveDraft'])) {
					Zend_Registry::get('defSession')->project = NULL;
					$this->_redirect('/app/project/draft');
				}
			}
		}

	}

	public function errorAction(){

		$flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$errors = $flashMessenger->getMessages();


		if( in_array('permission',$errors)){
				
			$this->view->errorTitle = "A Developer Profile is Required";
			$this->view->error = "You will need a completed developer profile before submitting any projects. Click <a href='/app/profile/developer'>HERE</a> to complete your profile";
				
		}elseif( in_array('permission_researcher',$errors)){
				
			$this->view->errorTitle = "A Researcher Profile is Required";
			$this->view->error = "You will need a completed researcher profile before submitting any projects. Click <a href='/app/profile/researcher'>HERE</a> to complete your profile";
				
		}else{
			$this->view->errorTitle = "We are unable to complete your request at this time";
			$this->view->error = "If this error persists please email support@mob4hire.com with the details.";
				
		}

	}

	protected function checkDeveloperRole(){

		// redirect if the user doesn't have a developer profile set
		$profileTable = new ProfileDeveloper();
		if(!$profileTable->hasProfile($this->view->currentUser->id)){
				
			$flashMessenger = $this->_helper->getHelper('FlashMessenger');
			$flashMessenger->addMessage('permission');
			$this->_redirect('/app/project/error');
		}

	}

	public function newprojectAction()
	{
		$this->_helper->layout->setLayout('app');
		$this->_helper->layout()->getView()->headTitle('| Project Management');
		$parameters = $this->_getAllParams();

		$this->view->deflt = isset($_GET['product']) ?$_GET['product']:"Experience";

		$form = new Form_RegisteredManagedServices();
		$this->view->form = $form;
		$this->view->done=false;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
				
			if ($form->isValid($formData))
			{
				$mailer = new Mailer();
				$mailer->sendGetQuoteEmails($formData,$this->view->currentUser->id);
				$this->view->done=true;
				//$this->_redirect('/app');
			}
		}
	}

	public function managedconfirmAction()
	{
		$this->_helper->layout->setLayout('app');
		$this->_helper->layout()->getView()->headTitle('| Project Management');;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			$check=0;
				
			$message = "Details:<br/>";

			if($formData['description'])
			{
				$description=$formData['description'];
			}
			else
			{
				$check=4;
			}

			$users = new Users();
			$contactInfo = new ProfileContactInfo();
			$userData = $users->getUser($this->view->currentUser->id);
			$contactData=$contactInfo->getProfile($this->view->currentUser->id);
			$name=$userData['first_name'].' '.$userData['last_name'];
			$username=$userData['username'];
			$email=$userData['email'];
			$country=$contactData['country'];
			$company=$contactData['company'];
				
				
			$enddate=$formData['enddate'];
			$service=$formData['service'];
				
			$title = "$username requests a new managed service project";
				
			$message=$message."<b>$name</b> from <b>$company</b> in <b>$country</b> has requested <b>$service</b> be preformed.<br/><b>Notes: </b><br/>$description<br/>".
			 "Project End Date: $enddate<br/> Please email them at $email";
			if($check==0)
			{
				$mailer = new Mailer();
				$mailer->sendToAdmin($title,$message);

				echo "<h2>Thank you for your submission, someone from our managed services team will contact you shortly.</h2>";
			}
			else
			{
				$this->_redirect("/app/project/newproject/check/$check");
			}
		}//End if
	}



}
