<?php

class App_MobsterController extends Zend_Controller_Action
{
	
	public function indexAction()
	{		
		$this->view->currentPage = "mobster";
		$this->view->headScript()->appendFile('/js/projects/list.js');
		$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = true;var mode = "tester";var navigation = "none";');
	
		
	}
	
	public function openprojectsAction()
	{
		$this->view->headScript()->appendFile('/js/addons/jquery.bigtarget.1.0.1.js');
	}
	
	public function submitbidAction()
	{
		$this->view->idproject=$idproject=$this->_getParam('idproject', 0);
		$projecttable=new Projects();

		$gidtester=$this->view->currentUser->id;
		if (isset($_POST['submit']) && isset($idproject)) { // form has been submitted
			$errors=array();
			$validated=true; // assume validation passed	
			
			$this->view->app=$app=new App();
			$app->load($idproject);
		
				
			// Check to see if the user has a mobster profile completed. 
			$profileTable = new ProfileMobster();
			if(!$profileTable->hasProfile($this->view->currentUser->id)){
				array_push($errors,4);
				$validated=false;	
				
			}
	
			// This only applies if the project requires specific handsets (I think now all projects will require a handset. If any was selected, then they selected it from the list of their own handsets)
			if(isset($_POST['supportedhandsets']))
				$iddevice=$_POST['supportedhandsets'];
			else{
				array_push($errors,1);
				$validated=false;
			}
	
			if(isset($_POST['supportednetworks'])){
				$idnetwork=$_POST['supportednetworks'];
					
			}
			else{
					array_push($errors,3);
					$validated=false;
			}

			if (!$app->flatfee) {
			
				$bidamount=(int) $_POST['bidAmount'];
				if ($bidamount < 1) {
					array_push($errors,2);
					$validated=false;
				}
			}
			else {
				$bidamount=$app->budget;
			}
			$comments=mysql_escape_string($_POST['comments']);
	
			$this->view->errors=$errors;


			
			if($validated){	
				
				$exe='';
				$tests=new Tests();
				if(($this->view->errorcode=$tests->submitBid($idproject, $gidtester, $bidamount, $idnetwork, $iddevice, $comments))==1) {						
					$this->view->bidSubmitted=true;
				}
				else {
					//something went wrong
					$this->view->errors=array('5');
					$validated=false;
				}
			}

		}
		
		if (isset($idproject) && !isset($this->view->bidSubmitted)) {
			// This is either the first time through or we failed validation	
		
			$this->view->app=$app=new App();
		
			if($this->view->canApply=$projecttable->canApplyForProject($this->view->currentUser->id, $this->view->idproject)) {

				if ($app->load($idproject)) { // all this stuff can be got from getProject. We need to get rid of this App object
					$compatibledevices=array();
					$compatiblenetworks=array();
					// get all compatible devices and add them to this array for selection in form
					$regdevices=new Regdevices();
					$devices=$regdevices->getHandsetsByUser($this->view->currentUser->id);  // get handsets
					foreach($devices as $device)
					{
						if($projecttable->iscompatibleDevice($device, $idproject)) {
							if (!in_array($device['vendor']." ".$device['model'], $compatibledevices)) {
								$compatibledevices[$device['id']]=$device['vendor']." ".$device['model'];
							}
							if (!in_array($device['country']." ".$device['network'], $compatiblenetworks)) {	
								$compatiblenetworks[$device['idnetwork']]=$device['country']." ".$device['network'];
							}
						}
					}
					$this->view->compatibledevices=$compatibledevices;
					$this->view->compatiblenetworks=$compatiblenetworks;
				}
			}
		}
	}

	public function submitnewbidAction()
	{
		$idtest=$this->_getParam('idtest', 0);
		$tests = new Tests();

		$rowset=$tests->find($idtest);

		if($this->view->test=$test=$rowset->current()) {
			$users=new Users();
			$this->view->tester=$users->getUser($test->idtester);
		}
		
		if ($this->getRequest()->isPost()) // form submitted
		{		
			$this->view->result=$tests->submitNewBid($idtest, $this->view->currentUser->id, $_POST['bidamount'], $_POST['comments']);
		}
		else {

			$this->view->validated=0;
		
			if(($this->view->errorcode=$tests->canSubmitNewBid($idtest,  $this->view->currentUser->id))) 
			{
				$projects= new Projects();
				$this->view->project=$projects->getProject($test->idproject);

				if ($test)
				{
					$this->view->validated=1;
				}
			}
		}	
	}
	
	public function downloadAction()
	{
		$this->_helper->layout->disableLayout();
	}

	public function downloadAppAction()
	{
		$this->view->idtest=$this->_getParam('idtest', 0);
		
		$tests=new Tests();
		$this->view->test=$test=$tests->getTest($this->view->idtest);
		
		$projectfiles=new ProjectFiles();
		$this->view->files=$projectfiles->getBinaryByDevice($test->idproject, $test->iddevice);
		$this->isActiveTesterOnProject($test->idproject);
		
	}
	
	public function downloadtestcaseAction()
	{
		
		$this->view->idproject=$idproject=$this->_getParam('idproject', 0);
		$this->isActiveTesterOnProject($idproject);
		$projects=new Projects();
		$this->view->project=$project=$projects->getProject($idproject);
		// Ok: We're in. Get all the Project Files for this project
		$this->view->projectfiles=new ProjectFiles();
				
	}

	public function submittestresultsAction()
	{
		
		$this->view->headScript()->appendFile('/js/files/list.js');
		$tests=new Tests();
		$this->view->idtest=$idtest=$this->_getParam('idtest', 0);
		$this->view->test=$test=$tests->getTest($this->view->idtest);
		
		$this->view->idproject=$idproject=$test->idproject;
		$idtester = $this->view->currentUser->id;
	
		
		if ($this->getRequest()->isPost())
		{	// Test cases have been submitted and messages need to be sent
			$formData = $this->getRequest()->getPost();

			if (isset($formData['cancel'])) // The form was not submitted: the back button was pressed
			{		
				$this->_redirect('/app/mobster/');
			}
			
			$this->view->complete = true;
			// Add the transaction
			if($tests->submitTest($idtest, $this->view->currentUser->id, $_POST['comments'])>0) {
				$ratingmodel=new Ratings();
				$rating=(int) $formData['rating'];
				$ratingmodel->addRating($test['devname'], 2, $idtest, $rating);
			}
						
		}
		else 
		{
			if ($tests->canSubmitTest($idtest, $this->view->currentUser->id)==1) {
				$form = new Form_SubmitTestCase($idtest); 
				$this->view->form = $form;
			}
			else {
				$this->_redirect('/app/project/view/idproject/'.$idproject); 			
			}
		}
		
	}

	
	public function uploadtestcaseAction()
	{
		$this->_helper->layout->disableLayout();
	}


	public function isActiveTesterOnProject($idproject)
	{
		$idtester = $this->view->currentUser->id;
		
		$tests = new Tests(); 
		if(!$tests->isActiveTesterOnProject($idproject, $idtester)){
			$this->_redirect('/app/project/view/idproject/'.$idproject); 
		}
	
	}
	
	public function showprojectAction()
	{
		$this->view->idproject=$idproject=$this->_getParam('idproject', 0);
		$projects= new Projects();
		$this->view->project=$project=$projects->getProject($this->view->idproject);
		$this->view->projectfiles=new ProjectFiles();
	
		// Test to make sure this tester can access the project
		$this->isActiveTesterOnProject($idproject);
		
			
		$iduser = $this->view->currentUser->id;
	
		$tests=new Tests();
		$testlist =$tests->listTestsByTesterAndProject($idproject,$iduser,'','t.Timestamp desc');
		
		$this->view->test = $testlist[0]->toArray();
			

		
		// Add the wall for the project
//		$this->view->headScript()->appendFile('/js/jquery.class.min.js');
		$this->view->headLink()->appendStylesheet('/css/app/wall.css');
//		$this->view->headScript()->appendFile('/js/wall/wall.js');
		$this->view->headScript()->appendScript("$(document).ready(function(){var wall = new Wall({'container':'wall-container','resultsPerPage':4,'type':1,'target':".$idproject.",'comment':true});});", $type = 'text/javascript', $attrs = array());
	
	}


 }