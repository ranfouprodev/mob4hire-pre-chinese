<?php

class Zend_View_Helper_GetMimeType
{
	function getMimeType($ext) 
	{
		$mimetypes = array(
			"doc" => "application/msword", 
			"pdf" => "application/pdf", 
			"bmp" => "image/bmp", 
			"gif" => "image/gif", 
			"jpeg" => "image/jpeg", 
			"jpg" => "image/jpeg", 
			"jpe" => "image/jpeg", 
			"png" => "image/png", 
			"txt" => "text/plain", 
		);
		
		
		if (isset($mimetypes[$ext])) { 
			return $mimetypes[$ext]; 
		} else { 
			return 'application/octet-stream'; 
		}
	}
}