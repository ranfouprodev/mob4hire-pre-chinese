<?php

class Zend_View_Helper_ShowNewLines
{
	public function showNewLines($string)
	{
		$string=str_replace("\r\n", "<br />",$string);
		$string=str_replace("\n", "<br />",$string);
		return $string;
	}
} 	