<?php

class Zend_View_Helper_FormatContactLink
{

	function formatContactLink($id) 
	{ 
		// Check if the contact is connected 
		$userid =  Zend_Registry::get('defSession')->currentUser->id;
		
		$contact = new Contacts();
		$hasContact = $contact->hasContact($userid, $id);
		
		if($hasContact)
		{
			$link = '<div id="divAddContactsLink"><p><a onclick="removeContact('.$id.')"><img src="/images/buttons/remove_from_contacts_button.gif"/ alt="Remove from Contacts" /></a></p></div>';
	
		}
		else
		{
			$link = '<div id="divAddContactsLink"><p><a onclick="addContact('.$id.')"><img src="/images/buttons/add_to_contacts_button.gif"/ alt="Add To Contacts" /></a></p></div>';			
		}
		
		return $link;
	}
}