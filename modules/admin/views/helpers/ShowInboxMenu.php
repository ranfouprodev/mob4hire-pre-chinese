<?php

class Zend_View_Helper_showInboxMenu
{
	public function showInboxMenu()
	{
		return
		"
			<li><a href='/app/inbox'>Received</a></li>
			<li><a href='/app/inbox/sent'>Sent</a></li>
			<li><a href='/app/inbox/notifcations'>Notifcations</a></li>
			<li><a href='/app/inbox/compose'>Compose</a></li>
			";
	}
} 	