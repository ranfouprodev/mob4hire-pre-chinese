<?php

class Admin_ProjectController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headScript()->appendFile('/js/addons/jquery.bigtarget.1.0.1.js');
	}

	public function uploadfileAction()
	{
		$form = $this->getUploadForm();
		$this->view->form = $form;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
						
			$projectFiles = new ProjectFiles();
			
			if(isset($formData['category']) && $formData['category'] != "" )
				$projectFileList = $projectFiles->getFilesForProjectByCategory($formData['projectid'],$formData['category']);
			else
				$projectFileList = $projectFiles->getAllFilesForProject($formData['projectid']);

			$this->view->filecontrolname = "admin";
			$this->view->projectFiles = $projectFileList;
			
		}
	}

	private function getUploadForm()
	{
		$form = new Zend_Form();
	
		$username = new Zend_Form_Element_Text('projectid');
		$username->setLabel('Project Id');        
		$form->addElement($username);
	
		$password = new Zend_Form_Element_Text('category');
		$password->setLabel('Category');
		$form->addElement($password);
 
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Submit');
		$form->addElement($submit);
 
		return $form;
	}

	public function approveAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();//get all passed in params
	
		if(isset($parameters['id'])){
			$projectTable = new Projects();
			$projectId = $parameters['id'];
			$projectData = $projectTable->getProject($projectId);		
			
			if($projectData['Status'] == 101){	
				// Check if this is a premium project. If so deduct the premium fee here
				$accounts=new Accounts();
				$premiumfee= Zend_Registry::get('configuration')->m4h->premiumTestFee;
				
				if($projectData['package']!=2)
				{
					$startDate = strtotime($projectData['StartDate']);
					$now = strtotime("now");
				
					$mail = new Mailer(); 
					$mail->sendProjectApprovedEmail($projectData);
				
					// If required send invites
					$mail->sendTesterInvites($projectData);
				
					if($startDate > $now){
						$status = 200; //open
					}else{
						$status = 201;
					}
				
					$projectTable->updateProject($projectId, array('Status'=>$status));
					$this->view->message = "Notification has been sent to the user";
				}
				else 
				{	
					$check=$accounts->moneyOut($projectData['iddeveloper'], $premiumfee, "Premium Test Fee: Project ".$projectData['idproject'],0,5);
					if($check>0)
					{
						//All good
						$startDate = strtotime($projectData['StartDate']);
						$now = strtotime("now");
					
						$mail = new Mailer(); 
						$mail->sendProjectApprovedEmail($projectData);
					
						// If required send invites
						$mail->sendTesterInvites($projectData);
					
						if($startDate > $now){
							$status = 200; //open
						}else{
							$status = 201;
						}
					
						$projectTable->updateProject($projectId, array('Status'=>$status));
						$this->view->message = "Notification has been sent to the user";
					}
					else
					{
						$this->view->message="This is a premium project and the developer does not have the necessary funds.";
					}
				}
			}else{
				$this->view->message = "This project has already been approved or not under moderation";
			}
		}
		
	}
	
	public function emailAction(){ 
	
		$this->_helper->layout->setLayout('admin');

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			$idproject = $formData['projectid'];
			
			$emailTargets = $formData['emails'];
			
			echo var_dump($emailTargets);
			
			
		}else{

			$parameters = $this->_getAllParams();//get all passed in params
	
			if(isset($parameters['id'])){
				$projectTable = new Projects(); //create new instance of Projects table
				$projectId = $parameters['id']; //get the value and store it in $projectId variable
				$projectData = $projectTable->getProject($projectId);
							
				$this->view->project = $projectData;
	
				$users = new Users(); 
				$userList =  array();
				
				// Find the users that match the required networks
				$supportednetworks = new SupportedNetworks();
				$networkSupported = $supportednetworks->getSupportedNetworks($projectId);
			
				foreach($networkSupported as $network){
					$idcountry = $network['idcountry'];
					$usersInCountry = $users->listUsers('idcountry='.$idcountry);
					if($usersInCountry)
						$userList = array_merge($userList, $usersInCountry->toArray());
					//$userList = $users->listUsers('idcountry='.$idcountry);
					
				}
				
				$this->view->users = $userList;
			}
		
	
		}			
		
		
		

	
	}
	
	public function testapprovedemailAction(){
	
		$parameters = $this->_getAllParams();
	
		$projectId=$parameters['projectId'];
	
	
		$projectTable = new Projects();
		$projectData = $projectTable->getProject($projectId);
	
		 $startDate = strtotime($projectData['StartDate']);
		 $now = strtotime("now");
				
		 $this->view->projid = $projectId;
		 $this->view->startdate = $startDate;
		 $this->view->now = $now;
		 $this->view->projectdata = $projectData;
				
				
		$mail = new Mailer(); 
		$mail->sendProjectApprovedEmail($projectData, "nosend");
				
		// If required send invites
		$mail->sendTesterInvites($projectData, "nosend");
	}
	
	
	
	
	public function editAction()
	{
		$this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
		$this->view->headScript()->appendFile('/js/devices/list.js');
		//  $this->view->headScript()->appendFile('/js/networks/addnetworksproject.js');
		//  $this->view->headScript()->appendFile('/js/handsets/addhandsetsproject.js');
		
		
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();//get all passed in params

		$formComments = new Form_Comments;
		$this->view->comments = $formComments;
 	  
		$projectTable = new Projects(); //create new instance of Projects table
		$projectId = $parameters['id']; //get the value and store it in $projectId variable
	    
		$form = new Form_AdminProject($projectId);
		$this->view->form = $form;
	 
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			$idproject = $formData['projectid'];

			echo "Project:".$idproject;

			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();
 
				// maybe instead of the following clumsy operation, we should rename the form elements to match the database
				$projectData = array (
					'projecttype'=>$formValues['projecttype'],
					'package'=>$formValues['testPkg'],
					'Name'=>$formValues['projName'],
					'ImagePath'=>Zend_Registry::get('defSession')->project->ImagePath,
					'description'=>$formValues['projDesc'],
					'longDesc'=>$formValues['projDetail'],
					'inviteonly'=>$formValues['inviteType'],
					'invitemessage'=>$formValues['mobInvite'],
					'flatfee'=>$formValues['transType'],
					'budget'=>$formValues['bidAmount'],
					'StartDate'=>$formValues['sDate'],
					'testenddate'=>$formValues['eDate'],
					'Duration'=>$formValues['projDur'],
					'maxtests'=>$formValues['amtMobsters'],
					'testType'=>implode(',', $formValues['testType']),
					'categories'=>implode(',', $formValues['testCategory']),
					'testClass'=>$formValues['testClass'],
					'estTime'=>$formValues['estTime'],
					'estData'=>$formValues['estData'],
					'tags'=>$formValues['tags'],
					'Status'=>$formValues['status'],
					'region'=>$formValues['region'],
					'instructions'=>$formValues['reportInstructions'],
					'nda'=>$formValues['nda'],
				);


				$projectTable->updateProject($projectId, $projectData);
				
				$this->view->updatestatus="Updated ".$idproject;
				//$this->_redirect('/admin/project/', array ('exit'=>true));	

			}else{
				$errors = $form->getErrors(); 
				echo var_dump($errors);			
			}
				
		}else{       
			$projectData = $projectTable->getProject($projectId); //pass this row back to the view based on parameter passed in as idproject
	
			$this->view->proj = $projectData;
	
			 $formData = array (
				 	'projecttype'=>$projectData['projecttype'],
					'status'=>$projectData['Status'],
					'testPkg'=>$projectData['package'],
					'projName'=>$projectData['Name'],
					'projDesc'=>$projectData['description'],
					'projDetail'=>$projectData['longDesc'],
					'inviteType'=>$projectData['inviteonly'],
					'mobInvite'=>$projectData['invitemessage'],
					'transType'=>$projectData['flatfee'],
					'maxtests'=>$projectData['maxtests'],
					'bidAmount'=>$projectData['budget'],
					'sDate'=>$projectData['StartDate'],
					'eDate'=>$projectData['testenddate'],
					'projDur'=>$projectData['Duration'],
					'testType'=>explode(',', $projectData['testType']),
					'testCategory'=>explode(',', $projectData['categories']),
					'testClass'=>$projectData['testClass'],
					'estTime'=>$projectData['estTime'],
					'estData'=>$projectData['estData'],
					'otherReq'=>$projectData['otherReq'],
					'tags'=>$projectData['tags'],
					'amtMobsters'=>$projectData['maxtests'],
					'region'=>$projectData['region'],
					'reportInstructions'=>$projectData['instructions'],
					'nda'=>$projectData['nda'],
			);
			$form->populate($formData);
		}
	}
	
	
	
	public function viewAction()
	{
		
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();//get all passed in params


		//ok we have a param called idproject
		$projectId = $parameters['idproject']; //get the value and store it in $projectId variable

		$projectTable = new Projects(); //create new instance of Projects table

		$this->view->proj = $projectTable->getProject($projectId); //pass this row back to the view based on parameter passed in as idproject

		$form = new Form_Comments;
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$this->view->comments = $form;
		$test = new Tests();
		$this->view->testList=$test->getTestsByProjectID($projectId);
		$this->view->idproject=$projectId;


	}
	
	
	public function listprojectsajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$orderBy= $this->getRequest()->getParam('orderBy');
		$this->view->orderBy = $orderBy;
		$this->view->mode=$mode=$this->getRequest()->getParam('mode');
		if($mode=='moderation')
		{
			$whereClause="projects.Status = 101";
			$this->view->title="Waiting Moderation";
		}
		else
		{
			$whereClause="1";
			$this->view->title="All Projects";

		}
		$this->view->$whereClause=$whereClause;
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly
		$this->view->numberOfPages=((int)($this->view->projectCount/$rowsPerPage))+1; // Nb, Projects have already been counted for output in header

		$projects=new Projects();
		$this->view->projectList=$projects->listProjects($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	}

	public function openAction()
	{
		
		$this->_helper->layout->setLayout('admin');
		$project = new Projects();
		$this->view->message="";
		if(isset($_POST['statusFlag']))
		{
			$idproject=$_POST['projectToOpen'];
			$project->openBidding($idproject);
			$blah = $project->getProject($idproject);
			$this->view->message="PROJECT ".$blah['Name']." RE-OPENED";
		}
		$this->view->projectList=$project->simpleListByStatus(300,1,0);
	}
	
	public function closeAction()
	{
		
		$this->_helper->layout->setLayout('admin');
		$project = new Projects();
		$this->view->message="";
		if(isset($_POST['statusFlag']))
		{
			$idproject=$_POST['projectToOpen'];
			$project->closeBidding($idproject);
			$blah = $project->getProject($idproject);
			$this->view->message="PROJECT ".$blah['Name']." CLOSED";
		}
		$this->view->projectList=$project->simpleListByStatus(201,1,0);
	}
	
	public function listprojectsAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$project = new Projects();
		$status=201;
		if(isset($_POST['statusFlag']))
		{
			$status=$_POST['statusSelect'];
			
		}
		$this->view->selected=$status;
		$this->view->projectList=$project->listProjectsByStatus($status,1,0);
	}
	
	public function showtestreportAction()	
	{
		$this->_helper->layout->setLayout('admin');
		$idtest = $this->_getParam('idtest', 0);
		$this->view->idproject=$this->_getParam('idproject',0);
		$testResult = new TestResults();
		$this->view->result = $testResult->getTestResult($idtest);	
		
	}
	
	public function confirmemailsAction()
	{
		$this->_helper->layout->setLayout('admin');
		if(isset($_POST['confirmed']))
		{
			if(isset($_POST['projectID']))
			{
				$idproject=$_POST['projectID'];
	  			$regdevices=new Regdevices();
				$testerinvites=new TesterInvites();
				$invitelist=$testerinvites->getInvitedTesters($idproject); // these are the users we have already invited				

				$testersinvited=array();
				foreach ($invitelist as $tester)
				{
					array_push($testersinvited, $tester->userid);
				}

	  			$users=$regdevices->getCompatibleUsers($idproject, 500);
				// this returns a rowset. We need to change this to an array of ids for the mailer
				$userids=array();
				foreach ($users as $user) {
					if (!in_array($tester->userid, $testersinvited)) {
						array_push($userids, $user['userid']);
					}
				}
	  			$this->view->idproject=$idproject;
	  			$mailer = new Mailer();
	  			$mailer->sendInvitesTemp($userids, $idproject, 'en');
				//IF YOU WANT TO TEST UNCOMMENT OUT THE ABOVE MAILER LINE
			}
			$this->view->mouse=1;
			
		}
		else
		{
			if(isset($_POST['projectID']))
			{
				$idproject=$_POST['projectID'];
	  			$regdevices=new Regdevices();
	  			$this->view->users=$regdevices->getCompatibleUsers($idproject, 500);
	  			$this->view->idproject=$idproject;
			}
			$this->view->mouse=2;
		}
	}
	
	public function viewmessagesAction()
	{
		$this->_helper->layout->setLayout('admin');
		$message = new Messages();
		$user = new Users();
		$username1=$this->_getParam('user1',0);
		$username2=$this->_getParam('user2',0);
		$this->view->tester=$username1;
		$this->view->developer=$username2;
		$this->view->testerID=$user->getUserID($username1);
		$this->view->items=$message->getMessagesBetweenUsers($username1, $username2);
	}
	

	public function canceltestAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->db=Zend_Registry::get('db');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$tests= new Tests();
		$parameters = $this->_getAllParams();
		$this->view->show=0;
		if(isset($parameters['idtest']))
		{
			$this->view->show=2;
			$idtest=$parameters['idtest'];
			$this->view->message="Cancel test $idtest";
			$this->view->test=$tests->getTest($idtest);
			$wall = new Wall();
			$this->view->wall=$wall->getTestWallForAdmin($idtest);
		}
		elseif(isset($_POST['cancelTest']))
		{
			$message='';
			if(isset($parameters['messages']))
			{
				$message=$parameters['messages'];
			}

			if(isset($parameters['sendEmail']))
			{
				$check=(int)$parameters['sendEmail'];
			}
			else
			{
				$check=0;
			}
			$idtest=$parameters['test'];
			$temp=$tests->adminCancelTest($idtest, $message, $check);
			if($temp>0)
			{
				if($check==1)
				{
					$this->view->message= "OK Done - Email sent to tester and developer";
				}
				else
				{
					$this->view->message= "OK Done";
				}
				
			}
			else
			{
				$this->view->message= "FAILURE - Please talk to you friendly admin for help.";
			}
			$this->view->show=3;
			
		}
		elseif (isset($parameters['idproject']))
		{
			$whereClause="tests.Status>200 AND tests.Status < 400";
			$projects = new Projects();
			
			$idproject=$parameters['idproject'];
			$data=$projects->getProject($idproject);
			$this->view->projectName=$data['Name'];
			$this->view->testList=$tests->listTestsByProjectID($idproject, $whereClause);
			$this->view->show=1;
		}
		else
		{
			$orderBy='';
			if(isset($parameters['orderBy']))
			{
				$orderBy=$parameters['orderBy'];
			}
			$this->view->projectList=$tests->listProjectsForTestCanceling($orderBy);
		}
	}
	
	public function projectsInDraftAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$projects = new Projects();
		$this->view->projectList=$projects->listProjectsByStatus(101,1,0);
	}
}