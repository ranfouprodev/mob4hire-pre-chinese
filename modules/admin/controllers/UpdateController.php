<?php

class Admin_UpdateController extends Zend_Controller_Action
{
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 
	}
	
	public function fixratingsAction()
	{
		$db=Zend_Registry::get('db'); // create a connection to the database. We wont bother with the Zend_Db_Select for this
		
		//remove all dud records with zero rating and the odd double ratings for same test
		$db->query("DELETE FROM testerratings WHERE (total_votes>0 AND total_value=0) OR total_votes>1");
		$db->query("DELETE FROM developerratings WHERE (total_votes>0 AND total_value=0) OR total_votes>1");
		$query1=$db->query("SELECT ratings.* FROM ratings WHERE total_votes>0 ORDER BY id");
		
		while ($row1=$query1->fetch())
		{
			// get related records from tester ratings
			$totalvotes=0;
			$totalvalue=0;
			$query2=$db->query("SELECT * FROM tests INNER JOIN testerratings ON testerratings.id=tests.idtest INNER JOIN users ON users.id=tests.idtester WHERE users.username='$row1[id]'");
			while($row2=$query2->fetch())
			{
				if($row2['total_value'])
				{
					$totalvotes++;
					$totalvalue+=$row2['total_value'];
				}
			}
			// get related records from developer ratings

			$query3=$db->query("SELECT * FROM tests INNER JOIN developerratings ON tests.idtest=developerratings.id INNER JOIN projects ON projects.idproject=tests.idproject INNER JOIN users ON users.id=projects.iddeveloper WHERE users.username='$row1[id]'");
			while($row3=$query3->fetch())
			{
				if($row3['total_value'])
				{
					$totalvotes++;
					$totalvalue+=$row3['total_value'];			
				}
			
			}
			if($totalvotes!=$row1['total_votes'] || $totalvalue!=$row1['total_value']) { 
				echo "$row1[id] : $row1[total_votes] : $row1[total_value] ----- $totalvotes : $totalvalue<br/>";
				$db->query($debug="UPDATE ratings SET total_votes=$totalvotes, total_value=$totalvalue WHERE id='$row1[id]'"); // fix ratings to make ratings match the other tables
			}
		}
	}
	
	public function populateaccountsAction()
	{
		$db=Zend_Registry::get('db'); // create a connection to the database. We wont bother with the Zend_Db_Select for this
		
		// first pull all the paypal transactions out of the present accounts table and put them in accounts2

		$query=$db->query("SELECT * FROM accounts order by transactionid");
		echo '<table class="current_handsets">';
		while ($row=$query->fetch()) {
			$debitprofit=false; // used to flag when we need to write an extra debit from the profit account
			$transactioncode=255; // just used to indicate that no valid transaction code has been assigned yet
			// try to categorize the transaction based on the transactiontype field
			$transactiontype=$row['transactiontype'];
			
			// Paypal deposit // 0
			// Paypal withdrawal // 1
			// 'escrow in - pay for test', // 2
			// 'escrow out - get paid for test', // 3
			// 'escrow out - escrow returned to developer', // 4
			// 'premium commission fee', // 5
			// 'managed services fee', // 6
			// 'error adjustment payment' , // 7
			// 'transfer funds', // 8
			// 'manual deposit', // 9
			// 'manual withdrawal' // 10

			$match = strstr($transactiontype, 'Paypal deposit:');
			
			if ($match!=FALSE) {
				// try to parse the paypal txid from the transactiontype string
				$txid=trim(substr(strstr($transactiontype, ':'), 1));
				// txid is valid if it has 16 characters (this is not safe as a general rule but works for our data)
				if (strlen($txid)==17)
				{
					// this is a valid paypal deposit
					$transactioncode=0;
					// todo write the record to the pplog table
				}
				else
				{
					// this is a manual deposit (needs matching with a cheque or something. The money was magically added
					$transactioncode=9;
					// todo write the record to the manual_payments table

				}
			}
			else 
			{
				$match = strstr($transactiontype, 'Paypal withdrawal:');
				if ($match!=FALSE || ($match=strstr($transactiontype, 'Paypal refund:'))) {
					// try to parse the paypal txid from the transactiontype string
					$txid=trim(substr(strstr($transactiontype, ':'), 1));
					// txid is valid if it has 16 characters (this is not safe as a general rule but works for our data)
					if (strlen($txid)==17)
					{
						// this is a valid paypal withdrawal
						$transactioncode=1;
						// todo write the record to the pplog table

					}
					else
					{
						// There are some special cases here for the brucechrumka cockups
						// I will try to correct them here. I hope I don't find too many of these special cases
						if($row['transactionid']==10592)
						{
							//  This was an error correction and should be flagged as such
							$row['transactiontype']="Error correction";
							$transactioncode=7;
						}
						elseif($row['transactionid']==10594)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer to brucechrumka";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10595)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer from brighthouselabs";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10694)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer to brucechrumka";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10695)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer from CitySonic";
							$transactioncode=8;
						}

						else
						{
							// this is a manual withdrawal. Presumably we sent a cheque. What happened to the South African thing?
							$transactioncode=10;
							// todo write the record to the manual_payments table
						}
					}
				}
				else 
				{
					// escrow ins and outs
					// ins
					$match = strstr($transactiontype, 'Escrow for test:');
					if ($match!=FALSE) {
						$transactioncode=2;
						// todo write the record to the escrow table
					}
					else 
					{
						//outs
						$match = strstr($transactiontype, 'Payment for test');
						if ($match!=FALSE) {
							$transactioncode=3;
							// todo write the record to the escrow table
						}
						else {
							$match = strstr($transactiontype, 'Escrow returned (Test Cancelled)');
							if ($match!=FALSE) {
								$transactioncode=4;
								// todo write the record to the escrow table
							}
							else
							{
								$match = strstr($transactiontype, 'Escrow Returned (Test Canceled)');
								if ($match!=FALSE) {
									$transactioncode=4;
									// todo write the record to the escrow table
								}
								else
								{
									// Payment for survey needs to come from profit I think
									$match = strstr($transactiontype, 'Payment for Survey:');
									if ($match!=FALSE) {
										$transactioncode=8;
										$debitprofit=true;
										// todo make debit from profit
									}
									else 
									{
										$match = strstr($transactiontype, 'Developer Bonus');
										if ($match!=FALSE) {
											$transactioncode=8;
											$debitprofit=true;
											// todo make debit from profit
										}
										else
										{
											$match = strstr($transactiontype, 'Premium Test Fee:');
											if ($match!=FALSE) {
												$transactioncode=5;
												// todo make credit to profit
											}
											else
											{
												// there are a few odds and ends left all internal transfers from profit account
												$transactioncode=8;
												$debitprofit=true;

											}
										}
									}
								}
							}
						}
					}
				}
			}
/*			if ($transactioncode==255)
			{
				echo "<tr class='row_even'>";
				echo "<td><p>$row[transactionid]</p></td><td><p>$row[iduser]</p></td><td><p>$row[amount]</p></td><td><p>$row[transactiontype]</p></td><td><p>$transactioncode</p></td><td><p>$row[usertype]</p></td><td><p>$row[Timestamp]</p></td><td><p>$row[newBalance]</p></td>";
				echo "</tr>";
			}*/
			try {
				$db->query($debug="INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) VALUES ($row[iduser], $row[amount], '$row[transactiontype]', $transactioncode, $row[Timestamp], $row[newBalance])");
				$newtid=$db->lastInsertId(); // get primary key of the record just inserted
			
				switch ($transactioncode)
				{
					case 0: // paypal deposit
						$pplquery=$db->query($debug=	"INSERT INTO pplog2 (item_name, item_number, payment_status, mc_gross, mc_currency, txn_id, receiver_email, payer_email, custom, Timestamp, transactionid, transactioncode) ".
													"SELECT pplog.*, '$row[Timestamp]' , '$newtid' , '0' FROM pplog WHERE txn_id='$txid' AND payment_status='Completed'");
						$pplogid=$db->lastInsertId(); // get primary key of the record just inserted
						
						if (!$pplogid) { 
							// There was no corresponding paypal log for this payment. It must have been manual. Simulate one
							// We do not know for sure what the correct mc_gross was. Maybe we can calculate this
							// By analysing the data in the paypal log I think the formula used to calculate the paypal fee was 2.9% + 30c
							// I think we changed this at some point however because now it is 3.9% + 30c.
							// We do not know the payer email for these transactions although I expect we can get it from Paypal.
							$ppfee=($row['amount']*102.9/100)+0.3;
							$mc_gross=$row['amount']+$ppfee;
							$pplquery2=$db->query($debug="INSERT INTO pplog2 (item_name, item_number, payment_status, mc_gross, mc_currency, txn_id, receiver_email, payer_email, custom, Timestamp, transactionid, transactioncode) ".
														"VALUES ('$row[transactiontype]', '', 'Completed', '$mc_gross', 'USD', '$txid','paypal@mob4hire.com' ,'','','$row[Timestamp]' , '$newtid', '0')");
						}
						break;
					case 1: // paypal withdrawal
						// write a record to pplog2 linked to this record
						// I think paypal withdrawals will be flintstoned for some time to come. We should reflect whether or not the payment has been made in the payment_status field and write the txid when we have it
						$mc_gross=-1*$row['amount'] ; // don't have negative numbers in pplog2. We don't have the payee email address here but when we write to this table for real we should add it
						$pplquery=$db->query($debug=	"INSERT INTO pplog2 (item_name, item_number, payment_status, mc_gross, mc_currency, txn_id, receiver_email, payer_email, custom, Timestamp, transactionid, transactioncode) ".
													"VALUES ('$row[transactiontype]', '', 'Completed', '$mc_gross', 'USD', '$txid','' ,'paypal@mob4hire.com','','$row[Timestamp]' , '$newtid', '1')");

						break;
					case 2:
						// write a record to escrow linked to this record (funds in)
						// we need to pull some stuff from the tests (and projects) table to add into this table
						// watch out for constraint violations here: missing tests, developers, testers.... oh dear
						$testquery=$db->query($debug="SELECT tests.*, projects.iddeveloper FROM tests INNER JOIN projects ON projects.idproject=tests.idproject WHERE idtest=$row[idtest]");
						$test=$testquery->fetch();
						$accounts2=new Accounts2();
						$newBalance=$accounts2->getBalance(2)+$test['escrowAmount'];

						$escrowquery=$db->query($debug=	"INSERT INTO escrow (idtest, idtester, iddeveloper, bidAmount, status, commission, salestax, timestamp, transactionid) ".
														"VALUES ($row[idtest], $test[idtester], $test[iddeveloper], $test[bidAmount], 2, $test[commission], $test[salestax], $row[Timestamp], $newtid)");
						$accountsquery=$db->query($debug="INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (2, $test[escrowAmount], '$row[transactiontype]',2,$row[Timestamp], $newBalance)");
						break;
					case 3:
						// write a record to escrow linked to this record (tester paid)
						// this should aways be an update since a record should already exist when the funds went into escrow. make sure this is so!
						$db->query($debug="INSERT INTO escrow (idtest, idtester, iddeveloper, bidAmount, status, commission, salestax, timestamp, transactionid) ".
														"VALUES ($row[idtest], $test[idtester], $test[iddeveloper], $test[bidAmount], 3, $test[commission], $test[salestax], $row[Timestamp], $newtid)");
						// The tester was paid. We should credit the profit account
						$testquery=$db->query($debug="SELECT tests.*, projects.iddeveloper FROM tests INNER JOIN projects ON projects.idproject=tests.idproject WHERE idtest=$row[idtest]");
						$test=$testquery->fetch();
					
						$accounts2=new Accounts2();
						$newBalance=$accounts2->getBalance(1)+$test['commission'];
						
						$profitquery=$db->query($debug=	"INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (1, $test[commission], 'mob4hire commission idtest:$row[idtest]',3,$row[Timestamp], $newBalance)");
						$escrowBalance=$accounts2->getBalance(2)-$test['escrowAmount'];;
						$escrowquery=$db->query($debug="INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (2, -1.00*$test[escrowAmount], '$row[transactiontype]',3,$row[Timestamp], $escrowBalance)");
						break;
					case 4:
						// write a record to escrow linked to this record (developer refunded)
						// this should aways be an update since a record should already exist when the funds went into escrow. make sure this is so!
						$db->query($debug="INSERT INTO escrow (idtest, idtester, iddeveloper, bidAmount, status, commission, salestax, timestamp, transactionid) ".
														"VALUES ($row[idtest], $test[idtester], $test[iddeveloper], $test[bidAmount], 4, $test[commission], $test[salestax], $row[Timestamp], $newtid)");
						$testquery=$db->query($debug="SELECT tests.*, projects.iddeveloper FROM tests INNER JOIN projects ON projects.idproject=tests.idproject WHERE idtest=$row[idtest]");
						$test=$testquery->fetch();
						$accounts2=new Accounts2();
						$escrowBalance=$accounts2->getBalance(2)-$test['escrowAmount'];
						$escrowquery=$db->query($debug="INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (2, -1.00*$test[escrowAmount], '$row[transactiontype]',4,$row[Timestamp], $escrowBalance)");


						break;
					case 5:
						// make a credit to profit account (premium test fee)
						$accounts2=new Accounts2();
						$newBalance=$accounts2->getBalance(1)-$row['amount']; // NB $row['amount'] is negative. This is why we are subtracting it to credit the profit account
						
						$profitquery=$db->query($debug=	"INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (1, -1.00*$row[amount], '$row[transactiontype]',5,$row[Timestamp], $newBalance)");
						break;
					case 6:
						// make a credit to profit account (managed test fee). We don't have any of these yet. Need to sort this out
						break;
					case 7:
						// error adjustment payment (nothing to do)
						break;
					case 8:
						// where this was a transfer between users there should already be two records (credit and debit) in accounts. Where the payment is from profit write another record to debit profit
						if ($debitprofit) {
							$accounts2=new Accounts2();
							$newBalance=$accounts2->getBalance(1)-$row['amount'];
							$profitquery=$db->query($debug="INSERT INTO accounts2 (iduser, amount, transactiontype, transactioncode, Timestamp, newBalance) ".
														"VALUES (1, -1.00*$row[amount], '$row[transactiontype]', 8 ,$row[Timestamp], $newBalance)");
						}
						break;
					case 9:
						// write a record to manual_payments (deposit)
					case 10:
						// write a record to manual_payments (withdrawal)
						$manquery=$db->query($debug="INSERT INTO manual_payments (Timestamp, transactionid, amount, note, transactioncode) ".
													"VALUES ($row[Timestamp], $newtid, $row[amount], '$row[transactiontype]', $transactioncode)");

						break;
				}
			}
			catch (Exception $e)
			{
				echo "<p>$debug</p>";
				echo "<p>".$e->getMessage()."</p>";
			}

			
		}
		echo "</table>";
	}

	public function fixtimestampsAction()
	{
		$this->_helper->layout->setLayout('admin');
		$reg = new Regdevices();
		$reg->fixTimestamps();
	}
	
	public function fixtestsbiddataAction()
	{
		$this->_helper->layout->setLayout('admin');
		$tests = new Tests();
		$this->view->count=$tests->fixOldBidData();
	}
	
	public function populatewallAction()
	{
		$this->_helper->layout->setLayout('admin');
		
		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT * FROM comments WHERE status=1 AND NOT ISNULL(iduser)");
		$count=0;
		while ($row=$query->fetch())
		{
			$category=$row['category'];
			//$message=$row['message'];
			$message=strip_tags($row['message']);
			$idposter=$row['iduser'];
			$tempDate=$row['posted'];
			$timestamp=strtotime($tempDate);
			$text="";
			if(substr($category,0,3)=='usr')
			{
				//This is a user private wall post
				
				$text=substr($category,3);
				$idtarget=(int)$text;
				$wall = new Wall();
				$result=$wall->fixCommentPost($idposter, $message, $idtarget, $timestamp, 3);
				//echo 'User Wall ID: '.$text.'<br/>';
			}
			elseif(substr($category,0,4)=='proP')
			{
				//Project wall post
				$text=substr($category,10);
				$idtarget=(int)$text;
				$wall = new Wall();
				$result=$wall->fixCommentPost($idposter, $message, $idtarget, $timestamp, 1);
				//echo 'Project Wall ID: '.$text.'<br/>';
			}
			else
			{
				$count++;
			}
			
			if($result==-1)
			{
				echo 'Error adding comment ID: '.$row['idcomment'].'<br/>';
			}
			
			
			
			
		}
		
		echo 'Number of public wall posts '.$count;
		$query2=$db->query("SELECT * FROM transactions INNER JOIN tests ON transactions.idtest=tests.idtest INNER JOIN projects ON tests.idproject=projects.idproject WHERE NOT ISNULL(transactions.Comments)");
		$wall = new Wall();
		$count=0;
		while ($row=$query2->fetch())
		{
			if($row['Comments'])
			{
				$message=$row['Comments'];
				$idposter=$row['idtester'];
				$idtarget=$row['idtest'];
				$timestamp=$row['Timestamp'];
				
				$result=$wall->fixCommentPost($idposter, $message, $idtarget, $timestamp, 2);
				if($result==-1)
				{
					echo 'Error adding transaction ID: '.$row['idtransaction'].'<br/>';
				}
			}
			else
			{
				$count++;
				echo 'X'.$row['Comments'];
			}
			
		}
		echo '<br/>NULL COMMENT COUNT: '.$count.'<br/>';	
	}
	
	public function populateinboxAction()
	{
		$this->_helper->layout->setLayout('admin'); //render this output in the admin module		
		$start = "Thank You for Submitting Your Project"; //pick a popular subject-- should reduce amount of results and local memory used
		
		$msg = new migmsg(); //migmsg = the model to the current messages table		
		$results = $msg->getMessages($start);
		echo "<h1>Records that don't match '{$start}' = {$results->count()}</h1>";  //results now contains all records that dont match the start subject string

		
		$expressions = array('/Mob4Hire - You have been invited to participate in a project/', //array of regular expressions
		'Escrow & NDA',
		'Thank You for Submitting Your Project',
		'Your Mob4Hire bid has been rejected',
		'Your Mob4Hire bid has been accepted',
		'You have a bid on your project at Mob4hire',
		'Your Mob4Hire Project has been Completed',
		'NDA & Escrow',
		'Your test on project',
		'You have a bid on your project',
		'Your bid on project',
		'has submitted test case for project:',
		'Thank you for testing! Your funds have been released',
		'Thank You for Submitting Your Project',
		'Your project is in moderation:',
		'Your project has been approved:',
		'Your bid on project',
		'has been accepted and funds have been escrowed',
		'Your test on project',
		'Paypal withdrawal request',
		'New Project For Moderation',
		'Mob4Hire - You have been invited to participate in a project');		
		//$matched = array(); //store records we dont want-- so we can double check that we have the right ones
		$users = new Users();
		$wall = new Wall();
		foreach ($results  as $row) //do the following for each record in the results set
		{				
			if($row['message']&&$row['subject']&&$row['owner']&&$row['sender'])
			{
				$idtarget = $row['owner'];
				$check=0;
				$message=strip_tags($row['message']);
				$idposter=$row['sender'];
				$timestamp=$row['posted'];
				if($users->getUser($idtarget)&&$users->getUser($idposter))
				{
					$check=1;
					$subjectMatch = strstr($row['subject'], 'RE:');
					if($subjectMatch==FALSE)
					{
						for ($i=0; $i<count($expressions); $i++) //for each regular expression
						{	
							$subjectMatch = strstr($row['subject'], $expressions[$i]);
							if($subjectMatch!=FALSE)
							{
								$check=0;
							}
							//if(preg_match($expressions[$i], $row['subject'], $match))	;				
	//						if(preg_match($expressions[$i], $row['subject'])||preg_match($expressions[$i], $row['message'])) // check against preg_match	
	//						{
	//							$check=0;					
	//							//array_push ($matched,$row);  //add this row to an array of matches
	//							//$row->delete(); //remove this row from our results set				
	//						}
						
						}
					}

					
				}
				

				
				if($check==1)
				{
					//The message made it all the way through the system here
					$wall->fixCommentPost($idposter, $message, $idtarget, $timestamp, 3);
				}
			}
		}
	}
	
	
	public function fixaccountsAction()
	{
		$this->_helper->layout->setLayout('admin'); //render this output in the admin module		
		
		//I am thinking that I was wrong with how I handled the migration so I was thinking of adding the 
		//transactionCode field to the accounts table there by making it more like my accounts2 table
		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT * FROM accounts WHERE ISNULL(transactioncode) ORDER BY transactionid");
		while ($row=$query->fetch())
		{
			$transactiontype=$row['transactiontype'];
			$match = strstr($transactiontype, 'Paypal deposit:');
			
			if ($match!=FALSE) 
			{
				// try to parse the paypal txid from the transactiontype string
				$txid=trim(substr(strstr($transactiontype, ':'), 1));
				// txid is valid if it has 16 characters (this is not safe as a general rule but works for our data)
				if (strlen($txid)==17)
				{
					// this is a valid paypal deposit
					$transactioncode=0;
					// todo write the record to the pplog table
				}
				else
				{
					// this is a manual deposit (needs matching with a cheque or something. The money was magically added
					$transactioncode=9;
				}
			}
			else 
			{
				$match = strstr($transactiontype, 'Paypal withdrawal:');
				if ($match!=FALSE || ($match=strstr($transactiontype, 'Paypal refund:'))) 
				{
					// try to parse the paypal txid from the transactiontype string
					$txid=trim(substr(strstr($transactiontype, ':'), 1));
					// txid is valid if it has 16 characters (this is not safe as a general rule but works for our data)
					if (strlen($txid)==17)
					{
						// this is a valid paypal withdrawal
						$transactioncode=1;

					}
					else
					{
						// There are some special cases here for the brucechrumka cockups
						// I will try to correct them here. I hope I don't find too many of these special cases
						if($row['transactionid']==10592)
						{
							//  This was an error correction and should be flagged as such
							$row['transactiontype']="Error correction";
							$transactioncode=7;
						}
						elseif($row['transactionid']==10594)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer to brucechrumka";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10595)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer from brighthouselabs";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10694)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer to brucechrumka";
							$transactioncode=8;
						}
						elseif($row['transactionid']==10695)
						{
							//  This was a transfer of funds and should be flagged as such
							$row['transactiontype']="Internal transfer from CitySonic";
							$transactioncode=8;
						}
						else
						{
							$transactioncode=10;
						}
					}
				}
				else 
				{
					$match = strstr($transactiontype, 'Escrow for test:');
					if ($match!=FALSE) 
					{
						$transactioncode=2;
					}
					else 
					{
						$match = strstr($transactiontype, 'Payment for test');
						if ($match!=FALSE) 
						{
							$transactioncode=3;
						}
						else {
							$match = strstr($transactiontype, 'Escrow returned (Test Cancelled)');
							if ($match!=FALSE) 
							{
								$transactioncode=4;
							}
							else
							{
								$match = strstr($transactiontype, 'Escrow Returned (Test Canceled)');
								if ($match!=FALSE) 
								{
									$transactioncode=4;
								}
								else
								{
									$match = strstr($transactiontype, 'Payment for Survey:');
									if ($match!=FALSE) 
									{
										$transactioncode=8;
									}
									else 
									{
										$match = strstr($transactiontype, 'Developer Bonus');
										if ($match!=FALSE) 
										{
											$transactioncode=8;
											$debitprofit=true;
											// todo make debit from profit
										}
										else
										{
											$match = strstr($transactiontype, 'Premium Test Fee:');
											if ($match!=FALSE) 
											{
												$transactioncode=5;
											}
											else
											{
												$transactioncode=8;

											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			$transactionid=$row['transactionid'];
			
			$db->query("UPDATE accounts SET transactioncode=$transactioncode WHERE transactionid=$transactionid");
		
		}
	}
	
	public function fixtesttimestampAction()
	{
		$this->_helper->layout->setLayout('admin');
		
		$db=Zend_Registry::get('db');
		
		$query1=$db->query("CREATE TEMPORARY TABLE temp (SELECT idtest, max(Timestamp) as Timestamp  FROM transactions GROUP BY idtest ORDER BY idtest)");
		$query2=$db->query("UPDATE tests t1, temp t2 SET t1.lasttimestamp=t2.Timestamp WHERE t2.idtest=t1.idtest;");

	}
	
	public function identifydeviceAction()
	{
		$this->_helper->layout->setLayout('admin');
		$reg = new Regdevices();
		$count=0;
		$total=0;
		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT idregdevice FROM regdevices WHERE ISNULL(deviceatlasid) AND NOT ISNULL(http_user_agent)");
		while ($row=$query->fetch())
		{
			$idregdevice=$row['idregdevice'];
			if($reg->identifyDevice($idregdevice))
			{
				$count++;
			}
			$total++;
		}
		$this->view->count=$count;
		$this->view->total=$total;
	}

	public function resendregistrationemailAction()
	{
		$this->_helper->layout->setLayout('admin');

		$db=Zend_Registry::get('db');
		$query=$db->query("SELECT id, email, username, code  FROM users WHERE active=0 AND registered_on >='2010-10-01'");
		while ($row=$query->fetch())
		{	
			// send activation email
			$mailer = new Mailer();        
			$languageCode = 'en';               
			$activationLink = Zend_Registry::get('configuration')->general->url . '/user/activate/' . $row['id'] . '/' . $row['code'];
			$mailer->sendRegistrationMail($row['email'], $row['username'], $activationLink, $languageCode);        
		}
	}
	
	/*
	 * This action is needed on the live site to clean up a problem the occured with illegal wall_message posts.
	 * Once it has finished running it can just be deleted.
	 */
	public function scrubwallAction()
	{
		$this->_helper->layout->setLayout('admin');
		$sql = "SELECT idmessage, idtest, idproject, postType, idparent FROM wall_message WHERE ISNULL(idreceiver) AND NOT ISNULL(idproject) ORDER BY idparent, idmessage";
		$db=Zend_Registry::get('db');
		$query=$db->query($sql);
		$idparent=0;
		$parentPostType=0;
		$deleteArray = array();
		while ($row=$query->fetch())
		{
			if($idparent!=$row['idparent'])
			{
				$idparent=$row['idparent'];
				$parentPostType=$row['postType'];
			}
			else
			{
				if($parentPostType != $row['postType'])
				{
					$deleteArray[]=$row['idmessage'];
				}
			}
		}
		$wall = new Wall();
		foreach($deleteArray as $index=>$row)
		{
			$where = $wall->getAdapter()->quoteInto('idmessage = ?', $row);
			$wall->delete($where);
			echo 'Deleted idmessage '.$row.'<br/>';
		}
	}
}