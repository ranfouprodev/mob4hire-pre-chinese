<?php

class Admin_CommunityController extends Zend_Controller_Action
{
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 
	}//End indexAction
	
	public function index2Action()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 $db=Zend_Registry::get('db');
 		 $query=$db->query("SELECT l.idlang AS idlang, a.text, a.idanswer AS idanswer FROM answer a JOIN languages l ON l.name=a.text");
 		 while ($result = $query->fetch())
 		 {
 		 	$idlang=$result['idlang'];
 		 	$idanswer=$result['idanswer'];
 		 	
 		 	echo "UPDATE `answer` SET `value`=$idlang WHERE `idanswer`=$idanswer;<br/>";
 		 
 		 }
 		 
	}//End indexAction

	public function listmanualidAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$reg=new Regdevices();
		$parameters = $this->_getAllParams();
		$whereClause="NOT ISNULL(manualid)";
		$db=Zend_Registry::get('db'); 
		$query=$db->query("SELECT idregdevice, manualid, users.username AS username FROM regdevices LEFT JOIN users ON regdevices.idtester=users.id WHERE NOT ISNULL(manualid)");
		$this->view->blah=$query->fetchAll();
		if(isset($parameters['message']))
		{
			$this->view->message="Updated regdevice ID:".$parameters['message'];
		}
	}
	public function adddeviceAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		if(isset($parameters['insert']))
		{
			$useragent=$parameters['queryArea'];
			$devices = new Devices();
			$check = $devices->identifyDevice($useragent);
			if($check)
				echo "<h1>Device Added</h1>";
			else
				echo "<h1>Failed adding the user agent</h1>";
		}		
	}
	
	public function assigndeviceAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headScript()->appendFile('/js/devices/list.js');
		$this->view->form=new Form_HandsetAssign();
		$this->view->form->populate($this->_request->getPost());
		$parameters = $this->_getAllParams();
		$reg = new Regdevices();
		if(isset($parameters['idregdevice']))
		{
			$idregdevice=(int)$parameters['idregdevice'];
			$regData=$reg->getRegdevice($idregdevice);
			$this->view->message=$regData['manualid'];
			$this->view->idreg=$idregdevice;
		}
		else
		{
			$this->view->message="nothing entered in idregdevice";
		}
		if(isset($parameters['model'])&&isset($parameters['vendor'])&&isset($parameters['idreg']))
		{
			$model=$parameters['model'];
			$idregdevice=$parameters['idreg'];
			$vendor=$parameters['vendor'];
			$device = new Devices();
			$id=$device->getDeviceByVendorAndModel($vendor, $model);
			
			$data=array('deviceatlasid'=>$id, 'manualid'=>null);
			$reg->updateDevice($idregdevice, $data);
			$this->view->message="Updated ".$idregdevice;
			$this->view->idreg=$idregdevice;
			$this->_redirect('/admin/community/listmanualid/message/'.$idregdevice);
		}
	}
	
	/*
	 * This action is the big do all action for an admin to make changes to an accounts
	 */
	public function paymentAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		if(isset($parameters['cancelCheck']))
		{
			//Check to see if the cancel button has been pressed it will reset the process
			$this->view->check=1;
			$this->view->message="Enter the user information, amount and select transaction";
			$this->view->buttonMessage="Commit";
			$this->view->iduser="";
			$this->view->amount="";
			$this->view->sub="";
		}
		else
		{
			//This means that the other button was pressed which is the do something button
			$accounts = new Accounts();
			$users = new Users();
			$this->view->iduser="";
			$this->view->amount="";
			$this->view->check=1;
			$this->view->message="Enter the user information, amount and select transaction";
			$this->view->buttonMessage="Commit";
			$userName='';
			$userCheck=$users->getUser($this->view->currentUser->id);
			if(isset($parameters['comments1']))
			{
				//This is the additional comments for the mail message
				$comments=$parameters['comments1'];
			}
			if(isset($parameters['comments']))
			{
				//This is the additional comments for the mail message
				$comments=$parameters['comments'];
			}
			else
			{
				$comments='';
			}
			if(isset($parameters['sub1']))
			{
				//sub1 was a hidden hodling container for the subject of the message
				$sub=$parameters['sub1'];
			}
			else
			{
				$sub='';
			}
			
			if(isset($parameters['sub']))
			{
				//sub is the text box for the subject of the message
				$sub=$parameters['sub'];
			}
			$this->view->sub=$sub;
			$this->view->comments=$comments;
			if(isset($parameters['check']))
			{
				$check=$parameters['check'];
				$this->view->check=$check;	
				if($check==1)
				{
					//This means that the user has entered the information to be added now we need a confirmation
					if(isset($parameters['iduser'])&&isset($parameters['amount'])&&isset($parameters['codeSelected']))
					{

						$userName=$parameters['iduser'];
						$iduser=0;
						$transactionCode=$parameters['codeSelected'];
						$this->view->codeSelected=$transactionCode;
						if($transactionCode==-1)
						{
							//The transaction code isn't selected
							$this->view->check=0;
							$this->view->message="Please select a transaction code for the system accounting";
							$this->view->buttonMessage="Try Again";
							$this->view->iduser="";
							$this->view->amount="";
							$this->view->sub="";
						}
						elseif($row1=$users->getUser($iduser, $userName))
						{
							//Here we make sure that the user entered was correct
							$iduser=$row1['id'];
							$userName=$row1['username'];
							if (is_numeric($parameters['amount']))
							 {
							 	$amount=(double)$parameters['amount'];
							 	if($amount>5000||$amount<0)
							 	{
							 		//The amount entered must be less than $5000.  This is merely a security issue
								 	$this->view->check=0;
									$this->view->message="The amount entered must be less than $5000 and greater than $0";
									$this->view->buttonMessage="Try Again";
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
							 	}
							 	else
							 	{
							 		//This mean that the user, amount, code are all in order now lets check other things
									$this->view->iduser=$iduser;
									$this->view->amount=$amount;
									$this->view->check=2;
									$this->view->buttonMessage="Confirm";
							 		if($transactionCode==1)
									{
										//Paypal withdraw
										if($accounts->getBalance($iduser)>=$amount)
										{
											$this->view->message='Please confirm making a paypall withdraw of $'.$amount.' from '.$userName.' account';
										}
										else
										{
											//Not enough money to add the funds so we should print out an error message
											$this->view->check=0;
											$this->view->message="$userName does not have the funds to make this paypal withdraw";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										}
									}
									elseif($transactionCode==6)
									{
										//Managed Services Fee
										if($accounts->getBalance($iduser)>=$amount)
										{
											$this->view->message="Please confirm charging $ $amount to $userName account for a Managed service fee";
										}
										else
										{
											//Not enough money to add the funds so we should print out an error message
											$this->view->check=0;
											$this->view->message="$userName doesn't have the funds to make this managed services fee payment";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										
										}
									}
									elseif($transactionCode==7)
									{
										//Error Adjustment Payment
										$this->view->message='Please confirm error adjustment payment by adding $'.$amount.' to '.$userName.' account';
									}
									elseif($transactionCode==8)
									{
										//Transfer payment now we need to check if the user who is paying exists
										if(isset($parameters['iduserout']))
										{

											$userNameOut=$parameters['iduserout'];
											$iduserOut=0;
											if($outDude=$users->getUser($iduserOut, $userNameOut))
											{
												$iduserOut=$outDude['id'];
												$userNameOut=$outDude['username'];
												$this->view->iduserout=$iduserOut;
												if($accounts->getBalance($iduserOut)>=$amount)
												{
													$this->view->message="Please confirm transfering out $$amount from $userNameOut to $userName";
												}
												else
												{
													//The dude paying doesn't have the funds
													$this->view->check=0;
													$this->view->message="$userNameOut does not have the funds to make this funds transfer";
													$this->view->buttonMessage="Try Again";
													$this->view->iduser="";
													$this->view->sub="";
													$this->view->amount="";
												}
											}
											else
											{
												//The second user doesn't exist so reset
												$this->view->check=0;
												$this->view->message="The user paying for the transfer is not in the database";
												$this->view->buttonMessage="Try Again";
												$this->view->iduser="";
												$this->view->sub="";
												$this->view->amount="";
											}
										}
										else
										{
											//Reset no secondary user selected
											$this->view->check=0;
											$this->view->message="A second user must be selected for a transfer action";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										}
									}
									elseif($transactionCode==9)
									{
										//Manual Deposit
										$this->view->message='Please confirm the manual deposit by adding $'.$amount.' to '.$userName.' account';
									}
									elseif($transactionCode==10)
									{
										//Manual Withdraw
										if($accounts->getBalance($iduser)>=$amount)
										{
											$this->view->message="Please confirm the manual withdraw by removing $ $amount from $userName account";
										}
										else
										{
											//Not enough money to remove the funds so we should print out an error message
											$this->view->check=0;
											$this->view->message="$userName doesn't have the funds to make this manual withdraw";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										}
									}
									elseif($transactionCode==11)
									{
										//Mobster Bonus
										$this->view->message="Please confirm adding a mobster bonus of $ $amount to $userName account";
									}
							 		elseif($transactionCode==12)
									{
										//Error Adjustment Withdraw
										if($accounts->getBalance($iduser)>=$amount)
										{
											$this->view->message='Please confirm error adjustment withdraw by removing $'.$amount.' from '.$userName.' account';
										}
										else
										{
											//Not enough money to remove the funds so we should print out an error message
											$this->view->check=0;
											$this->view->message="$userName doesn't have the funds to make this error adjustment withdraw";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										}
									}//End if on the transaction codes
								 	else
									{
									 	//Invalid transaction code entered
									 	$this->view->check=0;
										$this->view->message="Invalid transactionCode entered";
										$this->view->buttonMessage="Try Again";
										$this->view->iduser="";
										$this->view->sub="";
										$this->view->amount="";
									}
							 	}//End else part of if($amount>5000||$amount<0)	
							 	
							 }//End if (is_numeric($parameters['amount']))
							 else
							 {
							 	//Non-numeric value entered in the field
							 	$this->view->check=0;
								$this->view->message="The amount entered must be numeric";
								$this->view->buttonMessage="Try Again";
								$this->view->iduser="";
								$this->view->sub="";
								$this->view->amount="";
							 }
						}//End elseif($row1=$users->getUser($iduser, $userName))
						else
						{
							//This means that the user entered is incorrect and we must reset
							$this->view->check=0;
							$this->view->message="Invalid User the user doesn't exist in the database";
							$this->view->buttonMessage="Try Again";
							$this->view->iduser="";
							$this->view->sub="";
							$this->view->amount="";
						}
						
					}//End if(isset($parameters['iduser'])&&isset($parameters['amount'])&&isset($parameters['codeSelected']))
					else
					{
						//Error occured reset
						$this->view->check=1;
						$this->view->message="Enter the user information, amount and select transaction";
						$this->view->buttonMessage="Commit";
						$this->view->iduser="";
						$this->view->amount="";
						$this->view->sub="";
					}	
				}//End if($check==1)
				elseif($check==2)
				{
					if(isset($parameters['id'])&&isset($parameters['pay'])&&isset($parameters['code']))
					{
						$iduser=$parameters['id'];
						$amount=(double)$parameters['pay'];
						if($users->getUser($iduser))
						{
							//Check to ensure that the user entered is valid
							$this->view->iduser=$iduser;
							$this->view->amount=$amount;
							$row=$users->getUser($iduser);
							$userName=$row['username'];
							$transactionCode=$parameters['code'];
							$this->view->codeSelected=$transactionCode;
							if($transactionCode==1)
							{
								//Paypal withdraw
								if($transactionid=$accounts->withdrawFunds($iduser, $amount, $sub, $comments)!=-1)
								{
									//Money was taken out just fine
									$this->view->check=0;
									$this->view->buttonMessage="Make another transaction";
									$this->view->message='$ '.$amount.' has been taken from '.$row['username'].' account.';
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";	
								}
								else
								{
									$this->view->check=0;
									$this->view->buttonMessage="Try Again";
									$this->view->message="Error occured, please try again";
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
								}	
							}							
							elseif($transactionCode==7||$transactionCode==9||$transactionCode==11)
							{
								//Error Adjustment payment or manual deposit or mobster bonus
								if($transactionid=$accounts->moneyIn($iduser, $amount, 0,$transactionCode, $sub)!=-1)
								{
									//Money added OK
									$this->view->check=0;
									$this->view->buttonMessage="Make another transaction";
									$this->view->message='$ '.$amount.' has been added to '.$row['username'].' account.';
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
									$mailer = new Mailer();
									//$mailer->sendAdminManualDeposit($this->view->currentUser->id, $iduser, $amount, $transactionid, $comments);
								}
								else
								{
									$this->view->check=0;
									$this->view->buttonMessage="Try Again";
									$this->view->message="Error occured adding funds.  Please try again";
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
								}
							}
							elseif($transactionCode==10||$transactionCode==6||$transactionCode==12)
							{
								//Manual Withdraw or managed service fee or error withdraw
								if($transactionid=$accounts->moneyOut($iduser, $amount, $sub, 0,$transactionCode)!=-1)
								{
									//moneyOut($id, $amount, $description, $idtest,$code=-1)
									//Money was taken out just fine
									$this->view->check=0;
									$this->view->buttonMessage="Make another transaction";
									$this->view->message='$ '.$amount.' has been taken from '.$row['username'].' account.';
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
									$mailer = new Mailer();
									//$mailer->sendAdminManualDeposit($this->view->currentUser->id, $iduser, $amount, $transactionid, $comments);
								}
								else
								{
									$this->view->check=0;
									$this->view->buttonMessage="Try Again";
									$this->view->message="Error occured, please try again";
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
								}
							}
							elseif($transactionCode==8)
							{
								//Transfer funds action
								if(isset($parameters['idout']))
								{
									$iduserout=$parameters['idout'];
									if($outDude=$users->getUser($iduserout))
									{
										$iduserout=$outDude['id'];
										$userNameOut=$outDude['username'];
										$descr="xfer funds to $userName ".$sub;
										if($transactionid=$accounts->moneyOut($iduserout, $amount, $descr, 0,$transactionCode)!=-1)
										{
											//We have taken the money out of the first user's account now lets add it to the other guy
											$descr="xfer funds from $userNameOut ".$sub;
											if($transactionid=$accounts->moneyIn($iduser, $amount, 0,$transactionCode, $descr)!=-1)
											{
												//We added the money too the first dude
												$this->view->buttonMessage="Make another transaction";
												$this->view->check=0;
												$userName=$row['username'];
												$this->view->message="$$amount was taken from $userNameOut account and added to $userName account";
												$this->view->iduser="";
												$this->view->amount="";
												$this->view->sub="";
											}
											elseif($transactionid=$accounts->moneyIn($iduserout, $amount, 0,7, "Cancel Tranfer")!=-1)
											{
												//We returned the money to the payer as an error adjustment payment
												$this->view->check=0;
												$this->view->buttonMessage="Try Again";
												$this->view->message="The reciever of the tranfer was in error so $$amount was returned to $userNameOut";
												$this->view->iduser="";
												$this->view->amount="";
												$this->view->sub="";
											}
											else
											{
												//Something seriously has gone wrong
												$this->view->check=0;
												$this->view->buttonMessage="Try Again";
												$this->view->message="Error occured, please try again";
												$this->view->iduser="";
												$this->view->amount="";
												$this->view->sub="";
											}
										}
										else
										{
											// dude paying cannot afford it
											$this->view->check=0;
											$this->view->message="Error taking money out of $userNameOut account";
											$this->view->buttonMessage="Try Again";
											$this->view->iduser="";
											$this->view->sub="";
											$this->view->amount="";
										}
									}
									else
									{
										//Second dude error
										$this->view->check=0;
										$this->view->message="The user paying for the transfer is not in the database";
										$this->view->buttonMessage="Try Again";
										$this->view->iduser="";
										$this->view->sub="";
										$this->view->amount="";
									}
									
								}
								else
								{
									$this->view->check=0;
									$this->view->buttonMessage="Try Again";
									$this->view->message="Error occured, please try again";
									$this->view->iduser="";
									$this->view->amount="";
									$this->view->sub="";
								}
							}//End elseif($transactionCode==8)
						}//End if($users->getUser($iduser))
						else
						{
							//The user isn't valid so we are going to reset the test
							$this->view->check=0;
							$this->view->message="Invalid User would you like to try again?";
							$this->view->buttonMessage="Try Again";
							$this->view->iduser="";
							$this->view->amount="";
							$this->view->sub="";
						}
						
					}//End if(isset($parameters['id'])&&isset($parameters['pay'])&&isset($parameters['code']))
					else
					{
						//This means that either the id, amount or transaction code buggered up
						//So best thing to do is reset the whole damn thing
						$this->view->check=1;
						$this->view->message="Enter the user information, amount and select transaction";
						$this->view->buttonMessage="Commit";
						$this->view->iduser="";
						$this->view->sub="";
						$this->view->amount="";
					}
				}//End if($check==2)
				else
				{
					//So check isn't 1 or 2 so best course of action here is to reset
					$this->view->check=1;
					$this->view->message="Enter the user information, amount and select transaction";
					$this->view->buttonMessage="Commit";
					$this->view->iduser="";
					$this->view->amount="";
					$this->view->sub="";
				
				}
				
			}//End if(isset($parameters['check']))
		}//End else part of if(isset($parameters['cancelCheck']))
	}//End paymentAction

	public function accountDetailsAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		$accounts= new Accounts();
		$users = new Users();
		$queryCheck=1;
		$iduser=0;
		if(isset($parameters['email']))
		{
			$email=stripslashes($parameters['email']);
			$target=$email;
			$queryCheck=3;
		}
		elseif(isset($parameters['iduser']))
		{
			
			$username=stripslashes($parameters['iduser']);
			if($username != '')
			{
				$target=$username;
				$iduser=0;
				$queryCheck=2;
			}
			else
			{
				$iduser=$this->view->currentUser->id;
				$target=$iduser;
			}
		}
		else
		{
			$iduser=$this->view->currentUser->id;
			$target=$iduser;
			$username='';
		}
		if($users->userExists($iduser, $target, $queryCheck)>0)
		{
			//The user actually exists
			if($queryCheck==3)
			{
				//email grab
				$userData=$users->getUserByEmail($email);
			}
			else
			{
				$userData=$users->getUser($iduser, $username);
			}
			$iduser=$userData['id'];
			$this->view->username=$userData['username'];
			$this->view->iduser=$iduser;
			$this->view->account=$accounts->listAccounts($iduser);
			$this->view->showStuff=1;
		}
		else
		{
			$iduser=0;
			$this->view->username="";
			$this->view->iduser=$iduser;
			$this->view->showStuff=0;
			$this->view->message="invalid user information entered";
		}	
		
	}
	
	public function editTransactionAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		$accounts= new Accounts();
		$users = new Users();
		$this->view->check=0;
		if(isset($parameters['transactionid']))
		{
			$transactionid=$parameters['transactionid'];
		}
		elseif(isset($parameters['transid']))
		{
			$transactionid=$parameters['transid'];
			$type=$parameters['description'];
			$data=array('transactiontype'=>$type);
			$accounts->updateAccount($transactionid, $data);
			$iduser=$parameters['iduser'];
			$this->view->check=1;
		}
		else
		{
			$this->_redirect('/admin');
		}
		$transData=$accounts->getTransaction($transactionid);
		$this->view->transInfo=$transData;
		$this->view->iduser=$transData['iduser'];
		$userData=$users->getUser($transData['iduser']);
		$this->view->username=$userData['username'];
	}
	
	public function handsetsAction()
	{
		$this->_helper->layout->setLayout('admin');
	    /*
	     * Platform doesn't seem to be working. I'll remove it for now. 
	     * 
	     */
		$this->view->headScript()->appendFile('/js/jquery.js');
		$this->view->headScript()->appendFile('/js/handsets/list2.js');
		$this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
		$this->view->headScript()->appendFile('/js/devices/list.js');
		$this->view->headLink()->appendStylesheet('/css/rating.css');
		$this->view->form=new Form_HandsetFilter2();
		$this->view->form->populate($this->_request->getPost());
		
		// If no filters set return all devices
		$idproject=364;
		$this->view->idproject=$idproject;
		$idproject=$this->getRequest()->getParam('idproject');
		$countryFilter=$this->getRequest()->getParam('country');
		$networkFilter=$this->getRequest()->getParam('network');
		$userFilter=$this->getRequest()->getParam('userlike');
		$vendorFilter=$this->getRequest()->getParam('vendor');
		$modelFilter=$this->getRequest()->getParam('model');
		
		if(!$countryFilter && !$networkFilter   && !$vendorFilter && !$modelFilter  && !$userFilter){ //&& !$platformFilter) {
			$this->view->whereClause='n.idcountry>0 AND regdevices.idnetwork>0 AND d.vendor>0 AND d.id>0';		
		}
		else {
			$firstQuerySet=false;			
			if($countryFilter){
				$countryFilterString = "n.idcountry='$countryFilter' ";	
				$firstQuerySet=true;
			}
			else {
				$countryFilterString = "";
			}
			$networkFilterString = "";			
			if($networkFilter){
				if ($firstQuerySet) {
					$networkFilterString.="AND ";
				} 
			
				$networkFilterString .="regdevices.idnetwork='$networkFilter' ";
				$firstQuerySet=true;
			}
			$vendorFilterString = "";
			
			if($vendorFilter){
				if ($firstQuerySet) {
					$vendorFilterString.="AND ";
				}
				$vendorFilterString .= "d.vendor='$vendorFilter' ";
				$firstQuerySet=true;
			}

			$modelFilterString="";			
			if($modelFilter){
				if ($firstQuerySet) {
					$modelFilterString.="AND " ;
				}
				$modelFilterString .= "d.id='$modelFilter' ";
				$firstQuerySet=true;
			}
			$userFilterString="";			
			if($userFilter){
				if ($firstQuerySet) {
					$userFilterString.="AND " ;
				}
				$userFilterString .= "u.username LIKE '%".$userFilter."%' ";
				$firstQuerySet=true;
			}
		
			$this->view->whereClause=$countryFilterString.$networkFilterString.$vendorFilterString.$modelFilterString.$userFilterString;//.$platformFilterString
		
		}
		
	}
	
	
	public function listusersajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$whereClause=$this->getRequest()->getParam('whereClause');
		$idproject=$this->getRequest()->getParam('idproject');
		$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$regdevices=new Regdevices();
		$this->view->userList=$regdevices->getCompatibleUsers($idproject, 100, $whereClause);
		$this->view->totalUsers=count($this->view->userList);
	}
}//End Class