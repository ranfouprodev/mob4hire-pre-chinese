<?php

class Admin_SurveyController extends Zend_Controller_Action
{
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 
	}
	
	public function addNewQuestionAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$answer = new Answer();
		$question = new Question();
		$parameters = $this->_getAllParams();
		$this->view->answerGroups=$answer->listAnswerGroups();
		$this->view->answers=array();
		if(isset($_POST['viewAnswer']))
		{
			$group = $parameters['answerGroup'];
			$this->view->answers=$answer->getAnswersByGroup($group);
			$this->view->groupSelected=$group;
		}
		elseif(isset($_POST['addQuestion']))
		{
			$group = $parameters['answerGroup'];
			$answerArray=$answer->getAnswersByGroup($group);
			
			if(isset($parameters['question_text']))
			{
				$text=$parameters['question_text'];
			}
			$type=$parameters['questionType'];
			if($type==2)
			{
				$answerArray=array();
			}
			$mandatory=$parameters['mandatory'];
			$multiselect=$parameters['multi'];
			$this->view->id=$question->addNewQuestion($text, $answerArray, $type, $mandatory, $multiselect);
		}
		elseif(isset($_POST['startOver']))
		{
			
		}
	}
	
	public function createSurveyAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		
		//The various models we will use in this action
		$parameters = $this->_getAllParams();
		$survey = new Survey();
		$question = new Question();
		$answer = new Answer();
		$questionAnswer = new QuestionAnswer();
		$surveyQuestion = new SurveyQuestion();
		$this->view->surveyTitles=$survey->listSurveys();
		$this->view->message="Create A New Survey Page";
		if(isset($_POST['createSurvey']))
		{
			try 
			{
				$title=$parameters['newSurveyName'];

				if(isset($parameters['loadMobExp']))
					$loadMobExp=$parameters['loadMobExp'];
				elseif(isset($_POST['loadMobExp']))
					$loadMobExp=$_POST['loadMobExp'];
				else
					$loadMobExp=0;
				
				$idproject=(int)$parameters['newProjectID'];
				$newID=$survey->createNewSurvey($title, $idproject);	
				if($loadMobExp>0)
				{
					$questions=$surveyQuestion->getQuestionsBySurvey($loadMobExp);
					foreach($questions as $q)
					{
						$surveyQuestion->addQuestionToSurvey($newID, $q['idquestion'], $q['order']);
					}
					$branch = new Branch();
					$branch->copyAnotherSurveyBranch($newID,$loadMobExp);
					$token = new Token();
					if(isset($parameters['appName']))
					{
						$appName=$parameters['appName'];
						$data = array('idsurvey'=>$newID,'token'=>'APPLICATION NAME','value'=>$appName);
						$token->insert($data);
					}
					if(isset($parameters['handset']))
					{	
						$handset=$parameters['handset'];
						$data = array('idsurvey'=>$newID,'token'=>'HANDSET MANUFACTURER & MODEL','value'=>$handset);
						$token->insert($data);
					}
				}
				$this->view->check=TRUE;
				$this->view->surveyID=$newID;
			}
			catch(Exception $e)
			{

				$this->view->message="An error occured adding the survey";
			}
		}
	}
	
	public function editSurveyAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		
		//The various models we will use in this action
		$parameters = $this->_getAllParams();
		$survey = new Survey();
		$question = new Question();
		$answer = new Answer();
		$questionAnswer = new QuestionAnswer();
		$surveyQuestion = new SurveyQuestion();
		$this->view->answerGroups=$answer->listAnswerGroups();
		$this->view->answers=array();
		
		//These are basic view variables that will be used throughout
		$this->view->surveyStep=0;
		$this->view->surveyID=-1;
		$idsurvey=-1;
		$this->view->surveyName="";
		if(isset($parameters['surveyName']))
		{
			$surveyName=$parameters['surveyName'];
			$this->view->surveyName=$surveyName;
		}
		if(isset($parameters['survey_id']))
		{
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
			$this->view->surveyQuestionList = $surveyQuestion->getQuestionsBySurvey($idsurvey);	
		}
		else
		{
			$this->view->surveys=$survey->listSurveys();
		}
		if(isset($parameters['loadNewSurvey']))
		{
			$survey_id=$parameters['loadNewSurvey'];
			$surveyInfo=$survey->getSurvey($survey_id);
			$this->view->idproject=$surveyInfo['idproject'];
			$this->view->surveyID=$survey_id;
			$this->view->surveyName=$surveyInfo['title'];
			$this->view->surveyStep=1;
			$this->view->newID=$survey_id;
			$this->view->surveyQuestionList = $surveyQuestion->getQuestionsBySurvey($survey_id);	
		}
		elseif(isset($_POST['loadSurvey']))
		{
			//Somebody clicked the create survey button
			$survey_id=$parameters['surveySelect'];
			$surveyInfo=$survey->getSurvey($survey_id);
			$this->view->idproject=$surveyInfo['idproject'];
			$this->view->surveyID=$survey_id;
			$this->view->surveyName=$surveyInfo['title'];
			$this->view->surveyStep=1;
			$this->view->newID=$survey_id;
			$this->view->surveyQuestionList = $surveyQuestion->getQuestionsBySurvey($survey_id);	
		}
		elseif(isset($parameters['addNewQuestion']))
		{
			$this->view->surveyStep=2;
			$idsurvey=$parameters['addNewQuestion'];
			$this->view->surveyID=$idsurvey;
			$this->view->questionList=$question->listQuestionsNotInSurvey($idsurvey);
			
		}
		elseif(isset($parameters['viewQuestion']))
		{
			$idquestion=(int)$parameters['viewQuestion'];
			$this->view->questionInfo=$question->getQuestion($idquestion);
			$this->view->answerList=$questionAnswer->getAnswersByQuestionID($idquestion);
			$this->view->surveyStep=3;
			$idsurvey=$parameters['idsurvey'];
			$this->view->surveyID=$idsurvey;
		}
		elseif(isset($_POST['addQuestionToSurvey']))
		{
			$idquestion=(int)$parameters['questionID'];
			if($surveyQuestion->addQuestionToSurvey($idsurvey, $idquestion))
			{
				$this->view->surveyQuestionList=$surveyQuestion->getQuestionsBySurvey($idsurvey);	
				$this->view->surveyStep=1;
			}
			else
			{
				$this->view->surveyStep=2;
				$this->view->questionList=$question->listQuestionsNotInSurvey($idsurvey);
			}
		}
		elseif(isset($_POST['backToQuestionList']))
		{
			$this->view->surveyStep=2;
			$this->view->questionList=$question->listQuestionsNotInSurvey($idsurvey);
		}
		elseif(isset($_POST['backToSurveyQuestionList']))
		{
			$this->view->surveyStep=1;
		}
		elseif(isset($parameters['removeQuestion']))
		{
			$this->view->surveyStep=1;
			$idsurvey_question=$parameters['removeQuestion'];
			$surveyQuestion->removeQuestionFromSurvey($idsurvey_question);
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
			$this->view->surveyQuestionList=$surveyQuestion->getQuestionsBySurvey($idsurvey);
		}
		elseif(isset($parameters['addCustomQuestion']))
		{
			$this->view->surveyStep=4;
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
		}
		elseif(isset($_POST['viewAnswer']))
		{
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
			$group = $parameters['answergroup'];
			$this->view->answers=$answer->getAnswersByGroup($group);
			$this->view->groupSelected=$group;
			$this->view->surveyStep=4;
		}
		elseif(isset($_POST['addNewCustomQuestion']))
		{
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
			$group = $parameters['answergroup'];
			$answerArray=$answer->getAnswersByGroup($group);
			
			if(isset($parameters['question_text']))
			{
				$text=$parameters['question_text'];
			}
			$type=$parameters['questionType'];
			if($type==2)
			{
				$answerArray=array();
			}
			$mandatory=$parameters['mandatory'];
			$multiselect=$parameters['multi'];
			$custom=$parameters['custom'];
			$idquestion=$question->addNewQuestion($text, $answerArray, $type, $mandatory, $multiselect, $custom);
			if($surveyQuestion->addQuestionToSurvey($idsurvey, $idquestion))
			{
				$this->view->surveyQuestionList=$surveyQuestion->getQuestionsBySurvey($idsurvey);	
				$this->view->surveyStep=1;
			}
			else
			{
				$this->view->surveyStep=2;
				$this->view->questionList=$question->listQuestionsNotInSurvey($idsurvey);
			}
		}
		elseif (isset($parameters['branchQuestion']))
		{
			$this->view->surveyStep=5;
			$idsurvey_question=$parameters['branchQuestion'];
			$idsurvey=$parameters['survey_id'];
			$this->view->surveyID=$idsurvey;
			$tempInfo=$surveyQuestion->getSurveyQuestion($idsurvey_question);
			$idquestion=$tempInfo['idquestion'];
			$questionInfo=$question->getQuestion($idquestion);
			$answerInfo=$questionAnswer->getAnswersByQuestionID($idquestion);
			$this->view->questionInfo=$questionInfo;
			$this->view->answerInfo=$answerInfo;
		}
	}
	
	public function surveycsvAction()
	{
		$this->_helper->layout->disableLayout();
		header("Expires: 0");
		header("Cache-control: private");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Description: File Transfer");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=survey.csv");	

		$parameters = $this->_getAllParams();
		if(isset($parameters['idsurvey']))
		{
			$idsurvey=$parameters['idsurvey'];
		}
		else
		{
			$idsurvey=1;
		}
		
		$surveyAnalysis = new SurveyAnalysis();
		$surveyArray=$surveyAnalysis->getSurveyCSVArray($idsurvey);
		$out = '';

		// Add all values in the table to $out.
		$questionArray=$surveyArray[0];
		$header=FALSE;
		foreach($surveyArray as $result)
		{
			if(!$header)
			{
				foreach ($result as $index=>$row) {
				$out .='"'.$row.'"'.',';
				}
				$out .="\n";
				$header=TRUE;
			}
			else
			{
				$out .='"'.$result[0].'"'.',';
				foreach($questionArray as $q)
				{
					if(array_key_exists($q, $result))
					{
						$out .='"'.$result[$q].'"'.',';
					}
					elseif($q == 0)
					{
						
					}
					else
					{
						$out .='"NOT ANSWERED",';
					}
					
				}
				$out .="\n";
			}
			
		}
		echo $out;
	}
	
	public function surveyResultsAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		$surveyAnalysis = new SurveyAnalysis();
		$survey = new Survey();
		$this->view->surveys=$survey->listSurveysWithLinkedProject();
		$this->view->step=0;
		if(isset($_POST['loadSurvey']))
		{
			$survey_id=$parameters['surveySelect'];
			$idsurvey=$survey_id;
			$this->view->ALI=$surveyAnalysis->getSurveyALI($idsurvey);
			$this->view->PLI=$surveyAnalysis->getSurveyPLI($idsurvey);
			$this->view->RLI=$surveyAnalysis->getSurveyRLI($idsurvey);
			$this->view->ALI1=$surveyAnalysis->getPositiveResponse($idsurvey, 4);
			$this->view->ALI2=$surveyAnalysis->getPositiveResponse($idsurvey, 7);
			$this->view->ALI3=$surveyAnalysis->getPositiveResponse($idsurvey, 8);
			$this->view->PLI1=$surveyAnalysis->getPositiveResponse($idsurvey, 9);
			$this->view->PLI2=$surveyAnalysis->getPositiveResponse($idsurvey, 10);
			$this->view->RLI1=$surveyAnalysis->getInversePositiveResponse($idsurvey, 11);
			$this->view->RLI2=$surveyAnalysis->getInversePositiveResponse($idsurvey, 12);
			$this->view->idsurvey=$idsurvey;
			$this->view->step=1;
		}
		elseif(isset($parameters['resetIt']))
		{
			$this->view->step=0;
		}
		
	}
	
	public function randomSurveyAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		$survey = new Survey();
		$this->view->surveys=$survey->listSurveysWithLinkedProject();
		$response = new Response();
		$this->view->step=0;
		if(isset($_POST['loadSurvey']))
		{
			$survey_id=$parameters['surveySelect'];
			$idsurvey=$survey_id;
			$surveyInfo=$survey->getSurvey($survey_id);
			$idproject=$surveyInfo['idproject'];
			$randomSurvey = new RandomSurvey();
			$surveyAnswers = $randomSurvey->getAllInfo();
			$count=0;
			$db=Zend_Registry::get('db'); 		
			$query=$db->query("SELECT DISTINCT(idtester) AS idtester FROM tests WHERE idproject=$idproject AND Status>101");
			while ($row=$query->fetch())
			{
				$iduser=$row['idtester'];
				$timestamp=time();
				foreach($surveyAnswers as $surv)
				{
					$idquestion=$surv['idquestion'];
					$idanswer=mt_rand($surv['mini'], $surv['maxi']);
					if($surv['mini']==0)
					{
						$text="Random text for IDUSER: $iduser and IDSURVEY: $idsurvey and IDQUESTION: $idquestion";
						$data=array('iduser'=>$iduser,'idsurvey'=>$idsurvey,'idquestion'=>$idquestion,
							'idanswer'=>0,'text'=>$text,'timestamp'=>$timestamp);
					}
					else
					{
						$data=array('iduser'=>$iduser,'idsurvey'=>$idsurvey,'idquestion'=>$idquestion,
							'idanswer'=>$idanswer,'timestamp'=>$timestamp);
					}
					$response->insert($data);
				}
				$count++;
			}
			$this->view->message="<h1>Done, Added $count records</h1>";
			$this->view->step=1;
		}
		
	}
 }