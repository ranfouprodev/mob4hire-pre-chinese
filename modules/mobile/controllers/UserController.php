<?php
require_once 'Zend/Controller/Action.php';

class Mobile_UserController extends Zend_Controller_Action
{
	/**
	* Session namespace object for user data
	*
	* @var Zend_Session_Namespace
	*/
	protected $session = NULL;

	public function init()
	{
		$this->session = new Zend_Session_Namespace('User');
		$this->view->translate = Zend_Registry::get('Zend_Translate');
		
		
		$this->_helper->layout->setLayout('mobile');
	}

	public function indexAction()
	{
		$this->_forward('register');
	}
 
	public function loginAction()
	{

		if ($this->_helper->authUsers->isLoggedIn())
		{
			$this->_redirect($this->view->url(array(
                              'module'=>'mobile',
                              'controller'=>'app',
                              'action'=>'index'
                              ))
                   , array('exit'=>true));
		}
 
		$this->view->loginForm = new Form_MobileLogin(); 
 
		if ($this->getRequest()->isPost())
		{            
			if ($this->view->loginForm->isValid($_POST))
			{
				$username = $this->view->loginForm->getValue('username');
				$password = $this->view->loginForm->getValue('password');
				$auth = Zend_Auth::getInstance();
 
				$db = Zend_Registry::get('db');
				$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');               
				$authAdapter->setIdentity($username)->setCredential(Users::computePasswordHash($password));
				$authResult = $auth->authenticate($authAdapter);                                
				if ($authResult->isValid())
				{
					//valid username and password
					$userInfo = $authAdapter->getResultRowObject();
						//save userinfo in session
					$userInfo->password = '';
					Zend_Registry::get('defSession')->currentUser = $userInfo;
					// Write last_login info to db
					$users = new Users(); 
					$now = new Zend_Date(); 
						
					$userData = array('last_login' =>  $now->toString('YYYY-MM-dd HH:mm:ss') );
					$users->editProfile($userInfo->id, $userData);

					$regdevices= new Regdevices();
					$deviceprops=$regdevices->registerDevice($_SERVER['HTTP_USER_AGENT'], $userInfo->id, ((isset($_SERVER['HTTP_X_WAP_PROFILE']))?$_SERVER['HTTP_X_WAP_PROFILE'] : ''),  $ip=$this->getIP());
					// registered under $this->mobile already? 
					//Zend_Registry::get('defSession')->currentUser->device=$deviceprops;
					
					// Not sure where this is used
					$countries=new Countries();
					Zend_Registry::get('defSession')->currentUser->idcountry=$countries->getCountryByIP($ip);


					// if we requested a page but failed auth redirect there
					if(isset($_POST['return']))
					{
						$return=$_POST['return'];
						$this->_redirect($return, array ('exit'=>true));
					}
					else
					{
						//redirect to home                        
						$this->_redirect('/mobile/app/', array('exit'=>true));                        
					}
				}
				else
				{
                    $this->view->loginError = true;
				}
			}
		}
	}
	
	public function logoutAction()
	{
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		Zend_Registry::get('defSession')->currentUser = null;
		$this->_redirect($this->_helper->url('index', 'index'), array('exit'=>true));                
	}	
	
	public function forgotpasswordAction()
	{
		$parameters = $this->_getAllParams();
     	
		$users = new Users(); 
			// Check if the token is in the parameter		
		if ( isset ($parameters['cc'])){

			$code = $parameters['cc'];
			// find the user with that password
			$user = $users->getUserByCode($code);
			
			if(!$user)
				$this->view->tokenError = true; 
			else{
				
				$date = new Zend_Date(); 
				$last = new Zend_Date(strtotime($user['last_login'])); 	
				
				$age = ($date->sub($last))/60;
				
				if($age < (60*12) ){
				
					// Log the user in and forward them to the change password page
					$auth = Zend_Auth::getInstance();
			
			        $db = Zend_Registry::get('db');
			        $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'username');
			        $authAdapter->setIdentity($user['username'])->setCredential($user['username']);
			        $authResult = $auth->authenticate($authAdapter);
				
			        Zend_Registry::get('defSession')->currentUser = $user;
					Zend_Registry::get('defSession')->resetPass = true;
					
					$this->_redirect('/mobile/profile/changepassword', array ('exit'=>true));	
			        		
				}else{
					$form = new Form_MobileRecoverPassword(); 
				
					$this->view->form = $form; 
					$this->view->tokenExpired = true;
				}	
			}
  
		}else{

			$form = new Form_MobileRecoverPassword(); 
			
			$this->view->form = $form; 
			
			if ($this->getRequest()->isPost()) {
				
			 	$formData = $this->getRequest()->getPost();
	
		        if ($form->isValid($formData))
	            {
	            	$email = $formData['email'];
					
					// Find the user related to that account
					$user = $users->getUserByEmail($email);
					
					if(!$user)
						$this->view->emailError = true;
					else{						
							// Generate the token and add it to the activation field in the user db
							// We are reusing that field so I don't have to update the schema. 
							$activationCode = sha1(uniqid('xyz', true)); 
							
							// update the account
							$now = new Zend_Date(); 
							
							$userData = array('code'=>$activationCode , 'last_login' =>  $now->toString('YYYY-MM-dd HH:mm:ss') );
							$users->editProfile($user['id'], $userData);
							
							// Mail out the password
							$mailer = new Mailer(); 
							$mailer->sendForgotPassword($user, $activationCode);
						
							$this->view->emailSent = true;
					}	
				}    
			}
		}
	}
 
	public function activateAction()
	{        
		$parameters = $this->_getAllParams();
		if (isset($parameters['userId']) && isset($parameters['activationCode']))
		{
			// activate the user if the activation code is valid. 
			$usersTable = new Users();
			$rows = $usersTable->find($parameters['userId']);
			$user = $rows->current();
			if ($user)
			{                
				if (!$user->active && $user->code == $parameters['activationCode'])
				{
					// activate the user
					$user->active = 1; 
					$user->save();
					$this->view->activatedOK = true;
				}
				else if ($user->active)
				{
					$this->view->userAlreadyActive = true;
				}
			}                        
		}
		else if (isset($this->session->userJustRegistered))
		{
			$this->view->username = $this->session->userJustRegistered;
 
			// use 'user/justRegistered.phtml' template.
			$this->_redirect('/mobile/user/justregistered', array('exit'=>true));  
		}
	}
	
    
	public function registerAction()
	{
		$usersTable = new Users();
		$this->view->registerForm = new Form_MobileRegister(); 

		
		if ($this->getRequest()->isPost())
		{
			if ($this->view->registerForm->isValid($_POST))
			{
				$values = $this->view->registerForm->getValues();
				$badRegistration = false;
				try
				{
					$newUserId = $usersTable->add($values['username'], $values['password'], $values['email']);
						
					// We need to check if there is a profile entry for this user first. 
					$update = new ProfileContactInfo();
					$profile = $update->getProfile($newUserId);
					if(!$profile){
						$update->add($newUserId);
					}

				}
				catch (Zend_Db_Statement_Exception $e)
				{
					// race condition can occur between validator and actual DB operation
					// so check if username or email is already registered
					// NOTE: this should be very rare case, because we check this in validators just before inserting the row
					$message = $e->getMessage();
 
					if (strpos($message, $values['email']) !== false)
					{
						//email already registered
						$badRegistration = true;
						$this->view->globalPageError = 'email';
					}
					else if (strpos($message, $values['username']) !== false)
					{
						//username already registered
						$badRegistration = true;
						$this->view->globalPageError = 'username';
					}
					else
					{
						// other error, rethrow the exception
						throw $e;
					}
 
				}
 
 
				if (!$badRegistration)
				{
					$profileFields = array(
						'first_name' => $values['firstname'],
						'last_name' => $values['lastname'],
						);
 
					$usersTable->editProfile($newUserId, $profileFields);
					// log the user in straight away!
					$auth = Zend_Auth::getInstance();
 
					$db = Zend_Registry::get('db');
					$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');               
					$authAdapter->setIdentity($values['username'])->setCredential(Users::computePasswordHash($values['password']));
					$authResult = $auth->authenticate($authAdapter);                                
					if ($authResult->isValid())
					{
						//valid username and password
						$userInfo = $authAdapter->getResultRowObject();
						//save userinfo in session
						$userInfo->password = '';
						Zend_Registry::get('defSession')->currentUser = $userInfo;

						$countries=new Countries();
						Zend_Registry::get('defSession')->currentUser->idcountry=$countries->getCountryByIP($ip);
							
						//redirect to home                        
						$this->session->userJustRegistered = $values['username'];
						$this->_redirect('/mobile/user/activate', array('exit'=>true));                        
					}
				}
			}
		}

	}
	
	 public function justregisteredAction(){
	 	
	 	
	 }
	
	
	 public function identifyAction(){

		$deviceprops=Zend_Registry::get('defSession')->mobile;
		
	 	if(!empty($deviceprops) && isset($deviceprops['id']) && isset($deviceprops['mobileDevice'])){
		 	$this->view->device = $device = $deviceprops;
		}

         $regdevices = new Regdevices();

		if ($this->getRequest()->isPost())
		{

            $formData =  $this->getRequest()->getPost();
			
			$idcountry = $formData['country'];
			$idnetwork = $formData['network'];
			$vendor = $formData['vendor'];
			
			if(isset($formData['model'])){
				$model = $formData['model'];

				if(!$regdevices->hasDevice($this->view->currentUser->id,$formData['model']))
					$regdevices->insert(array('idtester'=>$this->view->currentUser->id, 'idnetwork'=>$formData['network'], 'deviceatlasid'=>$formData['model']));
	
			}			
			$form = $this->getDeviceForm($idcountry,$idnetwork,$vendor,$model);
		}
		else
		{
			if (isset(Zend_Registry::get('defSession')->currentUser->idcountry) && Zend_Registry::get('defSession')->currentUser->idcountry)
			{
				$idcountry=Zend_Registry::get('defSession')->currentUser->idcountry;
				$idnetwork=0; // We cannot identify the network at the moment
			}
			else {
				$idcountry = 1; // default country
				$idnetwork = 1; 
			}
			
			if(isset($device)){
				$vendor = $device['vendor'];
				$model = $device['id'];
				$form = $this->getDeviceForm($idcountry,$idnetwork,$vendor,$model);
			}else{
				$form = $this->getDeviceForm($idcountry,$idnetwork,1,1);
			}
	
		}
	 
	
		// Get list of currently registered handsets
	
		$handsets = $regdevices->getHandsetsByUser(Zend_Registry::get('defSession')->currentUser->id);	
		$this->view->handsets = $handsets;

		$this->view->form = $form;
		
	 }
 
 
 	private function getDeviceForm($idcountry='', $idnetwork='', $vendor='', $modelName=''){
		$form = new Zend_Form();
				
		$form->setAction('/mobile/user/identify')
			->setMethod('post')
			->setAttrib('rel', 'external');
		
		
		$countries=new Countries();
		$countrylist=$countries->listCountries("idcountry asc"); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country')
				->setAttrib('data-theme', 'a')
				->setAttrib('onChange','this.form.submit()'); // This should work on most smartphones. Not sure about old shitty phones
		
		foreach($countrylist as $id=>$countryname) {
				$country->addMultiOption(substr($id,7), $countryname);
		}

		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->setAttrib('data-theme', 'm')
				->addMultiOptions(array(0=>'Select Country First'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		if($idcountry){
			$country->setValue(array($idcountry));
			$networks = new Networks(); 
			$networkList = $networks->listNetworks($idcountry)	;
			$carrier->setMultiOptions($networkList);
		}

		if($idnetwork)
			$carrier->setValue(array($idnetwork));


		
		$devices=new Devices();	
		$vendors=$devices->listVendors();
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		$manufacturer->setLabel('Handset')
					->addMultiOptions($vendors)
					->setAttrib('data-theme', 'm')	
					->setRegisterInArrayValidator(false)
					->setAttrib('onChange','this.form.submit()');
		
		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->setAttrib('data-theme', 'm')
				->addMultiOptions(array(0=>'Select Model First'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		if($vendor){
			$manufacturer->setValue($vendor);
			$modelList = $devices->listModels($vendor);
			$model->setMultiOptions($modelList);
		}

		if($modelName)
			$model->setValue($modelName);
			
		$addmodel = new Zend_Form_Element_Submit('add');
		$addmodel->setLabel('Add')->setAttrib('data-theme', 'm');
		
	
		
		$form->addElement($country);
		$form->addElement($carrier);
			
		$form->addElement($manufacturer);
		$form->addElement($model);
		$form->addElement($addmodel);
		
		return $form;
		
 	}
 
     
	
	public function checkusernameajaxAction()
	{
		$this->_helper->layout->disableLayout();
 
		$username = $this->getRequest()->getParam('username');
		$usersTable = new Users();
		$select = $usersTable->select();
		$select->where('username = ?', $username);
		$rows = $usersTable->fetchAll($select);
		if ($rows->count())
			$valid = false;
		else
			$valid = true;
 
		$this->_helper->viewRenderer->setNoRender();
		$data = array('valid'=>$valid);
		$json = Zend_Json::encode($data);
		echo $json;
	}
 
	public function checkemailajaxAction()
	{
		$this->_helper->layout->disableLayout();
 
		$email = $this->getRequest()->getParam('email');
		$usersTable = new Users();
		$select = $usersTable->select();
		$select->where('email = ?', $email);
		$rows = $usersTable->fetchAll($select);
		if ($rows->count())
			$valid = false;
		else
			$valid = true;
		
		$this->_helper->viewRenderer->setNoRender();
		$data = array('valid'=>$valid);
		$json = Zend_Json::encode($data);
		echo $json;
	}
	
	private function getIP()
	{
		$headers = apache_request_headers();
		if (array_key_exists('X-Forwarded-For', $headers)){
			$hostname=$headers['X-Forwarded-For'] ;
		} else {
			$hostname=$_SERVER["REMOTE_ADDR"];
		}
		return $hostname;
	}
}
