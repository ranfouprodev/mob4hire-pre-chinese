<?php
require_once 'Zend/Controller/Action.php';

class Mobile_ProfileController extends Zend_Controller_Action
{
	/**
	* Session namespace object for user data
	*
	* @var Zend_Session_Namespace
	*/
	protected $session = NULL;

	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate');
		$this->_helper->layout->setLayout('mobile');
	    $this->view->headScript()->appendFile('/js/contactlist/contacts.js');

        $this->view->submenu = $this->view->render('partial/profile_submenu.phtml');
	}

	public function indexAction()
	{
		$this->_redirect("/mobile/profile/view");		
	}
 
	public function changepasswordAction(){
	
		$form = new Form_MobileChangePassword(); 
		$this->view->form = $form;
		
		
		if ($this->getRequest()->isPost()) {
			
		 	$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
    
				Zend_Registry::get('defSession')->resetPass = NULL;
	        	
				// Update the users password
				$users = new Users(); 
			
				$userData = array('password'=>$formData['password']);
				$users->editProfile($this->view->currentUser->id, $userData);
	
				$this->_redirect('/mobile/app');	
	
			}
		}
	}
	
	public function editAction(){ 
		
		$form = new Form_MobileProfileEdit;
		$this->view->profileInfo=$form;
		
		// We need to check if there is a profile entry for this user first. 
		$update = new ProfileContactInfo();
		$profile = $update->getProfile($this->view->currentUser->id);
		if(!$profile){
			$update->add($this->view->currentUser->id);
		}
		
		$this->view->profile=$profile;

		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
				
			if( isset($profile['idfacebook']) && $formData['email'] =="(via facebook)"){ // Can we have a bit of attention to detail here. More haste, less speed perhaps. The variable is defined one line before. You'd think it would be possible to remember what it is called
					$formData['email'] = $profile['email'];
			}
				
			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();

				$update = new ProfileContactInfo();
				$update->editProfile($this->view->currentUser->id, $formValues);
				
			}//end if statement isValid()

			else
			{
				if( isset($profile['idfacebook']) && strpos($formData['email'],'proxymail.facebook.com') ){ // to prevent the facebook proxy email from showing again when validation failed
					$formData['email'] = "(via facebook)";
				}
				$form->populate($formData);
			}

		}// end if ispost()
			
		else
		{

			//populate info fields with model data
			$profileTable = new ProfileContactInfo();
			$profileInfo = $profileTable->getProfile($this->view->currentUser->id);
			
			if( isset($profileInfo['idfacebook']) && strpos($profileInfo['email'],'proxymail.facebook.com') ){
				$profileInfo['email'] = "(via facebook)";
			}
			
			$form->populate($profileInfo);
			
			$this->view->profile = $profileInfo;

		}
		
		
	}
	
	public function viewAction(){ 
		
		$this->view->headScript()->appendFile('/js/contactlist/contacts.js');

		
		$this->view->id = 0;
		
		$users = new Users();
		
		$parameters = $this->_getAllParams();
		if (!isset($parameters['id']) && !isset($parameters['user'])){
			$this->view->id=$this->view->currentUser->id; // If no id sent then show our own profile
			
		}elseif (isset($parameters['user'])){
			$this->view->user=$parameters['user'];		
			$user = $users->getUser(0,  $this->view->user);
			
			if($user){
				$this->view->id = $user['id'];
			}
			$this->_helper->layout->setLayout('mobile');
		}else{
			$this->view->id=$parameters['id'];
			$this->_helper->layout->setLayout('mobile');
		}

		if(!isset($this->view->id) || $this->view->id==0){
			$this->_redirect('/mobile/profile/view');
		}
			
		
		
		// Header 
		$this->view->contact = $users->getUserAsContact($this->view->id);
		$regdevices=new Regdevices();
		$this->view->handsets = $regdevices->getHandsetsByUser($this->view->id);
				
		// Primary contact info
		$profile=new ProfileContactInfo();
		$this->view->profile=$contactInfo=$profile->getProfile($this->view->id,  $this->view->user);
		
		// Mobster
		$testerratings=new TesterRatings();
		$this->view->testerratings=$testerratings->getRatingsUser($this->view->id,5);
		$profileTable = new ProfileMobster();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id,  $this->view->user);					
			$this->view->mobster = $profileInfo;
		}catch(Exception $e){
			
		}
		
		// Researcher
		$profileTable = new ProfileResearcher();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id , $this->view->user); // id is ignored if username is given so we are still ok					
			$this->view->researcher = $profileInfo;
		}catch(Exception $e){
			
		}
				
		// Developer
		$profileTable = new ProfileDeveloper();
		try{
			$profileInfo = $profileTable->getProfile($this->view->id , $this->view->user);					
			$this->view->developer = $profileInfo;
		}catch(Exception $e){
			
		}
		$developerratings=new DeveloperRatings();
		$this->view->developerratings=$developerratings->getRatingsUser($this->view->id,5);
		
		
		// Groups
		$groups = new Groups(); 
		$this->view->groups = $groups->listOpenGroupsForUser($this->view->id);
		
		
	}
	
	public function contactsAction()
	{
	
		$parameters = $this->_getAllParams();
		
		$this->view->resultsPerPage=10;
		$this->view->page=1;
			
		$options = array('showcomment'=>false,'selector'=>'none','layout'=>'compact');
					
		$this->view->options = $options;

 	}
	
 	public function accountAction(){ 
		$accounts= new Accounts();
		$this->view->account=$accounts->listAccounts($this->view->currentUser->id);
 	}
 	
}
