<?php

class Mobile_GroupController extends Zend_Controller_Action
{
	
	public function init() {
        
        $this->view->headScript()->appendFile('/js/groups/mobile_groups.js');
        $this->view->headScript()->appendFile('/js/wall/wall.js');
        $this->_helper->layout->setLayout('mobile');
    }
	
	public function indexAction()
	{
		$this->view->resultsPerPage=10;
		$this->view->page=1;
			
		$options = array(); 
		$options['showcomment'] = true;
		$options['layout'] = 'compact';
			
		$this->view->options = $options;
		$this->view->mode = "all";
		
	}
	
	public function selfAction()
	{
		$this->view->resultsPerPage=10;
		$this->view->page=1;
			
		$options = array(); 
		$options['showcomment'] = true;
		$options['layout'] = 'compact';
			
		$this->view->options = $options;
		$this->view->mode = "self";
		
	}
	
	
/*
	 * Group View Page
	 */
	public function viewAction()
	{
		$parameters = $this->_getAllParams();
		
		if(!isset($parameters['id'])){
				$this->_redirect('/mobile/group/index');
		}
			
		$groups = new Groups(); 
		$group = $groups->getGroup($parameters['id']);
		
		$this->view->group = $group; 
	}
	
	
 
}//end class