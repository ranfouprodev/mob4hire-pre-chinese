<?php
// This should eventually be moved into the model!

class Zend_View_Helper_GetProjectImage
{
	function getProjectImage($project,$height=100,$width=100) 
	{
		$projectFiles = new ProjectFiles();
		$currentImages = $projectFiles->getFilesForProjectByCategory($project->idproject,"projImg");
		
				
		if(!isset($currentImages) || sizeof($currentImages)==0){
			 return '<img class="project-icon" src="/images/icons/no_project_image.gif" height="'.$height.'" width="'.$width.'" alt="' . $project->Name . '" />';			
		}
		else
		{
			$currentImage = $currentImages[0];
			 return '<img class="project-icon" src="/content/image/'.$currentImage['idfiles'].'" height="'.$height.'" width="'.$width.'" alt="' . $project->Name . '" />';			
		}
		
		
		
	}
}