<?php
/*
 * Shows all the posts, from projects, tests and private posts in order
 */
class Zend_View_Helper_ShowMobileUnifiedWall
{
	public $view;
	static public $wallType=array(1=>'prj', 2=>'tst', 3=>'usr');
	static public $wallLabel=array(1=>'Public', 2=>'Private', 3=>'User');
	// idlogin is only required when we want to display a converstation between two users
	// open can take one of three values: 0=closed; 1=open; 2=open if new posts
	function ShowMobileUnifiedWall( $pageNumber=1, $resultsPerPage=0, $timestamp=0) 
	{
		$wallDto = new Wall();
		
		$walls = $wallDto->getUnifiedWall(Zend_Registry::get('defSession')->currentUser->id, $pageNumber, $resultsPerPage, $timestamp);
		
		$return='<div class="clear"></div>';
		
		foreach($walls as $wall){			
			
			
			$return.='<div id="wallpost-thread">';
			$return.='<p class="about">';
			if($wall->idproject <> ''){
				$idtarget = $wall->idproject;
				$postType = 1;
				$return.='<img src="/images/icons/message_project.png"/>  New project message from '.$wall->projectName . ' posted ' .$this->view->showElapsedTime($wall->timestamp);
			}elseif($wall->idtest <> ''){
				$idtarget = $wall->idtest;
				$postType = 2;
				$return.='<img src="/images/icons/message_test.png"/>  New private project message from the project '.$wall->projectName . ' posted ' .$this->view->showElapsedTime($wall->timestamp);
			}else{
				$idtarget = Zend_Registry::get('defSession')->currentUser->id;
				$postType = 3;
				$return.='<img src="/images/icons/message_private.png"/>  New private message posted ' .$this->view->showElapsedTime($wall->timestamp);
			}
			$return.='<div class="test-msg-wall" id="'.self::$wallType[$postType].$idtarget.'">';
			$return.=$this->view->partial('wall/_layout.phtml', array('wall'=>$wallDto->getWallThread($wall->idparent),'postType'=>$postType,'idtarget'=>$idtarget,'edit'=>false));
			//$return.=" showing thread:".$wall->idmessage;
			$return.='</div></div>';	
		}
		
		
		return $return;
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}