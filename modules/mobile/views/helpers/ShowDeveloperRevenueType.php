<?php

class Zend_View_Helper_ShowDeveloperRevenueType
{
	public function showDeveloperRevenueType($string)
	{

	$translate = Zend_Registry::get('Zend_Translate');

		if($string){
      $platform = explode(",",$string);
    
      $return = "";
	   foreach ($platform as $a){
		 $return .= ProfileDeveloper::$revenueTypes[$a];
		 $return .=",";
	   }
	    $return = substr($return,0,-1);
			
		return $return;
	}
    else {
		return $translate->_('mobile_profile_view_empty');
    }
    
    
	}
} 	