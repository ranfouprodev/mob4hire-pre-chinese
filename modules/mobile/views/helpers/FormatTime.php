<?php

class Zend_View_Helper_FormatTime
{
	function formatTime($mysqldate) 
	{
		$timestamp=strtotime($mysqldate);
		$formatteddate=date("G:i:s", $timestamp);
		return $formatteddate;
	}
}