<?php

class Zend_View_Helper_CanEditFile
{
	/*
	 * Determines if a user can edit the file
	 */
	function canEditFile($projectFile) 
	{
				
		try{
					
		$project = new Projects(); 
		$projectData = $project->getProject($projectFile['idproject']);
				
		if(Zend_Registry::get('defSession')->currentUser->isAdmin){
			$canEdit = true; // Admin access
		}elseif($projectData['Status'] == 100 && $projectData['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id){
			// Users Can only edit the files in draft
			$canEdit = true;
		}else{
			$canEdit = false;
		}
	
		}catch(Exception $e){
			$canEdit = false;
		}
			
		return $canEdit;
			
		
	}
}