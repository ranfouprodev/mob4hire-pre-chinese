<?php

class Zend_View_Helper_CanViewFile
{
	/*
	 * Determines if a user can edit the file
	 */
	function canViewFile($projectFile) 
	{
	/*			
		try{
					
		$project = new Projects(); 
		$projectData = $project->getProject($projectFile['idproject']);
				
		$tests = new Tests(); 
		$idproject = $projectFile['idproject'];
		
		$iduser = Zend_Registry::get('defSession')->currentUser->id;
		
		if(Zend_Registry::get('defSession')->currentUser->isAdmin){
			$canView = true; // Admin access
		}elseif ($projectData['iddeveloper'] == $iduser){
			// Users Can only edit the files in draft
			$canView = true;
		}elseif ($tests->isActiveTesterOnProject($idproject, $iduser)	){
			$canView = true;			
		}else{
			$canView = false;
		}
	
		}catch(Exception $e){
			$canView = false;
		}
			
		return $canView;
	*/	
		// This view helper is currentlly preventing anybody from being able to see project files before their bid has been accepted.
		// This surely makes it nearly impossible for them to make a sensible bid.
		return true;
	}
}