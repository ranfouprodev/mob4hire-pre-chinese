<?php

class Rss_IndexController extends Zend_Controller_Action
{
    public function indexAction()
    {
 		$this->_helper->layout->disableLayout();
		$this->getHelper('viewRenderer')->setNoRender(true); 
		
		 $feed = $this->getProjectFeed();
   		 $feed->send();
    }
	
	
	public function projectsAction()
    {
 		$this->_helper->layout->disableLayout();
		$this->getHelper('viewRenderer')->setNoRender(true); 
		
		// $feed = $this->getProjectFeed();
   		// $feed->send();
    }
	
	
	
	private function getProjectFeed()
   {
    $title = 'Mob4Hire Latest Projects';
    $feedUri = '/rss/';

    //link from which feed is available
    $link = 'http://' . $_SERVER['HTTP_HOST'] . $feedUri;

	$projects=new Projects();
	$projectList  = $projects->listProjects('projects.Status > 199 and projects.Status < 299', 'StartDate desc ', 1, 20);
	//$projectList  = $projects->listProjects('1', 'idproject desc ', 1, 20);
   
   
	$entries = array();
    foreach ($projectList as $project){
		$entry = array('title' => ($project['Name'])?$project['Name']:" ",
          'link'    => ('http://' . $_SERVER['HTTP_HOST'] . '/the-mob/view/idproject/' . $project['idproject'])?"url":"none",
		  'description' => ($project['description'])?$project['description']:" ",
		     	  'pubDate'=> date(DATE_RFC822,strtotime($project['StartDate'])),
           );	
		   
	     array_push($entries, $entry);
	}
        
	 //create array according to structure defined in Zend_Feed documentation
    $rss = array('title' => $title,
                     'link'  => $link,
					 'image' =>'http://' . $_SERVER['HTTP_HOST'] . '/images/logo_big.png',
                     'description' => 'Keep Up To Date With Mob4Hire Projects',
                     'language' => 'en-us',
                     'charset' => 'utf-8',
                     'entries' => $entries,
					 'copyright' => '(c)2010 Mob4Hire Inc',
					 'lastUpdate'=>strtotime(time()),		 
              );	
		
		
		
    
    $feed = Zend_Feed::importArray($rss, 'rss');
    return $feed;
	}

	
	
	
}