<?php
class Zend_View_Helper_ShowRatingBar
{
	/*
	This is used to print a star rating without going to the expense of querying the database each time.
	*/
	function showRatingBar($rating, $pro=0)
	{
		$width=number_format($rating * 20, 2);
		$return= "
			<div class='ratingblock'>
				<div id='unit_long'>
					<ul id='unit_ul' class='unit-rating' style='width: 100px;'>
						<li class='current-rating' style='width: ".$width."px;'>Currently $rating/5</li>
					</ul>
				</div>";
		if($pro)
		{
			$return.="<img class='pro' src='/images/Certified-Tester-Icon-2.gif' />";
		}

		$return.="</div>";
		
		return $return;
	}
}