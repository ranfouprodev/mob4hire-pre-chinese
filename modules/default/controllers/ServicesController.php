<?php

class ServicesController extends Zend_Controller_Action 
{
	public function indexAction()
	{
        
	}
	
	public function managed2Action()
	{
		$this->_helper->layout->setLayout('services_wide');
		$this->_helper->layout()->getView()->headTitle('| Project Management');        
		
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			
			$title = "New managed service request";
			$message = "Details:<br/>";
			
			foreach ($formData as $k => $v)
			{
				if($k != "submit"){
					$message .= $k . " - " .$v;
					 $message .= "<br/>";
				}
			}
			
			$mailer = new Mailer(); 
			$mailer->sendToAdmin($title,$message);
			
			$this->_redirect('/services/what-is-mob4hire');
		}
	}
	
	
	public function managedAction()
	{
		$this->view->headLink()->appendStylesheet('/css/forms.css');
		$this->_helper->layout->setLayout('services_wide');
		
		$this->_helper->layout()->getView()->headTitle('| Project Management');
		
		$parameters = $this->_getAllParams();
		
		$this->view->deflt = isset($_GET['product']) ?$_GET['product']:"Experience";
		
		$form = new Form_ManagedServices();
		$this->view->form = $form;
		$this->view->done=false;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
			$mailer = new Mailer(); 
			$mailer->sendGetQuoteEmails($formData);	
			$this->view->done=true;
			}
		}
	}
	
	public function managedconfirmAction()
	{
		$this->_helper->layout->setLayout('services_wide');
		$this->_helper->layout()->getView()->headTitle('| Project Management');;
		/*require_once('Lib/Captcha/recaptchalib.php');
		$privatekey = "6LeUKb4SAAAAAGvKRTqijA81f18MvNdVSjlZ4LkL";
		$resp = recaptcha_check_answer ($privatekey,
		                              $_SERVER["REMOTE_ADDR"],
		                              $_POST["recaptcha_challenge_field"],
		                              $_POST["recaptcha_response_field"]);
		if (!$resp->is_valid) 
		{
			// What happens when the CAPTCHA was entered incorrectly
			$this->_redirect('/services/managed/check/1');
		} 
		else
		{*/
			if ($this->getRequest()->isPost())
			{
				
				/*
				 * Using a Zend_Form and Validator would have saved you a lot of time on this one. Where
				 * possible we should make use of both of those.
				 */
				
				$formData = $this->getRequest()->getPost();
				$check=0;
				$title = "New managed service request";
				$message = "Details:<br/>";
				if($formData['fullname'])
				{
					$name=$formData['fullname'];
				}
				else
				{
					$check=2;
				}
				if($formData['email'])
				{
					$email=$formData['email'];
				}
				else
				{
					$check=3;
				}
				if($formData['description'])
				{
					$description=$formData['description'];
				}
				else
				{
					$check=4;
				}
				$country=$formData['country'];
				//$enddate=$formData['enddate'];
				$service=$formData['service'];
				$company=$formData['company'];
				
				$message=$message."<b>$name</b> from <b>$company</b> in <b>$country</b> has requested <b>$service</b> be preformed.<br/><b>Notes: </b><br/>$description<br/>".
				"Please email them at $email"; 
				//"Project End Date: $enddate<br/> Please email them at $email";
				if($check==0)
				{
					$mailer = new Mailer(); 
					$mailer->sendToAdmin($title,$message);
					
					echo "<h2>Thank you for your submission, someone from our managed services team will contact you shortly.</h2>";
				}
				else
				{
					$this->_redirect("/services/managed/check/$check");
				}
				
			}
		//}
		
	}
	
	public function acceleratorAction()
	{
  		$this->_helper->layout()->getView()->headTitle(' | App Stores');        

	}

	public function globalMobileResearchAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Wireless Research');        
		
		$parameters = $this->_getAllParams();
		
		if (isset($parameters['status'])) {
			$status=$parameters['status'];
			
			if ($status == 'success') {
				// TODO: Make sure we were referred here by Paypal
				$this->render('success');
			}
			elseif($status == 'cancel') {
				$this->render('surveyError');
			}
			else {
				$this->_helper->layout->setLayout('services_wide');
			}
		}
		else {
			$this->_helper->layout->setLayout('services_wide');
		}
	}
	
	public function downloadgmrAction()
	{
		$response = $this->_response;

		// Disable view and layout rendering
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// Process the file
		$file = "$_SERVER[DOCUMENT_ROOT]/content/gmr/Mob4Hire_BOB_Customer_Loyalty_Mobile_Impact_Summer_Report_2010.pdf";
		$bits = @file_get_contents($file);
		if(strlen($bits) == 0) {
			$response->setBody('Sorry, we could not find requested download file.');
			
		}
		else {
			$response->setHeader('Content-type', 'application/pdf', true);
			$response->setHeader('Content-Description', 'File Transfer', true);
			$response->setHeader('Content-disposition', 'attachment; filename=Mob4Hire_BOB_Customer_Loyalty_Mobile_Impact_Summer_Report_2010.pdf');	
			$response->setBody($bits);
		}		
	}
	
	public function starAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Star Rating');        
	}
 
	public function whatIsMob4hireAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Mobile Developer');        
	}
	
	public function seeHowItWorksAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | android iPhone ');   
	} 

	public function pricingAction()
	{
		$this->_helper->layout->setLayout('services_wide');
	}
	
	public function mobileAppTestingAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | app apps');   
	}

	public function mobileAppResearchAction()
	{
		$this->_helper->layout()->getView()->headTitle('| brands');        
	
	}
	
	public function marketResearchAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Survey Surveys ');        
	
	}

	public function contextualResearchAction()
	{
	
	}
	
	public function mysteryShoppingAction()
	{
	
	}
	
	public function developmentProjectsAction()
	{
	
	}
	
	public function graphicDesignAction()
	{
	
	}
	
	public function translationAction()
	{
	
	}
	
	public function mobtasksAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Microtask Tasks');
		
		$this->_helper->layout->setLayout('services');
		
	}
	
	public function mobsmstestAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | SMS Testing ');
		
		$this->_helper->layout->setLayout('services');
		
	}
	public function mobsimtestAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | SIM Testing ');
		
		$this->_helper->layout->setLayout('services');
		
	}
	public function mobstarcertifiedAction()
	{
		$this->_helper->layout->setLayout('services');
		
	}
	public function mobtestsuiteAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | App Apps ');  
		
		$this->_helper->layout->setLayout('services');
		
		$this->view->headScript()->appendScript('$(document).ready(function(){$("img[rel]").overlay({expose: "#333",effect: "apple"});})', $type = 'text/javascript', $attrs = array());
		
	}
	
	public function mobabtestAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | muiltivariate a b ');
		
		$this->_helper->layout->setLayout('services');
		
	}
	
}