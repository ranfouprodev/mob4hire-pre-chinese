<?php
class PartnersController extends Zend_Controller_Action 
{	
	public function clientsAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Clients'); 
	}
	
	public function partnersAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Partners'); 
	}
	
	public function associatesAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Associates'); 
	}

	public function infostretchAction()
	{
	}
	
	public function perfectoAction()
	{	
	}
	
	public function navteqAction()
	{
	//Class declerations here
		$tester = new ProfileMobster();
		$regdevice = new Regdevices();
		$project = new Projects();
		$contactinfo = new ProfileContactInfo();
		$tests = new Tests();
		$users = new Users();
		$developer = new ProfileDeveloper();
		
		$this->view->totalMobsters=$users->count();
		$this->view->totalTesters=$tester->countAllTesters();
		$this->view->totalCountries=$contactinfo->countCountries();
		$this->view->totalHandSets=$regdevice->count();
		$this->view->totalNetworks=$regdevice->countNetworks();
		$this->view->totalDevelopers=$developer->count();
		$this->view->developerCountries=$developer->countCountries();
	}

	public function nokiaAction()
	{
		//Class declerations here
		$tester = new ProfileMobster();
		$regdevice = new Regdevices();
		$project = new Projects();
		$contactinfo = new ProfileContactInfo();
		$tests = new Tests();
		$users = new Users();
		$developer = new ProfileDeveloper();
		
		$this->view->totalMobsters=$users->count();
		$this->view->totalTesters=$tester->countAllTesters();
		$this->view->totalCountries=$contactinfo->countCountries();
		$this->view->totalHandSets=$regdevice->count();
		$this->view->totalNetworks=$regdevice->countNetworks();
		$this->view->totalDevelopers=$developer->count();
		$this->view->developerCountries=$developer->countCountries();
	}
	
	public function facebookAction()
	{
		//Class declerations here
		$tester = new ProfileMobster();
		$regdevice = new Regdevices();
		$project = new Projects();
		$contactinfo = new ProfileContactInfo();
		$tests = new Tests();
		$users = new Users();
		$developer = new ProfileDeveloper();
		
		$this->view->totalMobsters=$users->count();
		$this->view->totalTesters=$tester->countAllTesters();
		$this->view->totalCountries=$contactinfo->countCountries();
		$this->view->totalHandSets=$regdevice->count();
		$this->view->totalNetworks=$regdevice->countNetworks();
		$this->view->totalDevelopers=$developer->count();
		$this->view->developerCountries=$developer->countCountries();
	}	
  }
