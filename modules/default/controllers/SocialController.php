<?php
class SocialController extends Zend_Controller_Action
{

       private static $config=array(
                        'callbackUrl'=>'http://rmcooke.dyndns.org/social/twittercb',
                        'siteUrl' => 'http://twitter.com/oauth',
                        'consumerKey'=>'kLEjeoqnbtpdcI12Rb59fA',
                        'consumerSecret'=>'WSYxfdM79mVo8p5v1glEhxP4VWHJTjCUbI9wLRwTU'
                );

	public function indexAction()
	{

	}

	public function facebookauthAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		// Get a Facebook authorization for the publish_stream and offline_access
		$url = 'https://graph.facebook.com/oauth/authorize?client_id=' .
		Zend_Registry::get('facebook_client_id') .
			'&redirect_uri=' .
			Zend_Registry::get('facebook_redirect_uri') .
			'&scope=publish_stream,offline_access'.
			',email ';

		// Return URL that should be used for redirection in Javascript
		die ("1|$url");
	}
	
	public function facebookcbAction()
	{
		$request = $this->getRequest();
		$params = $request->getParams();

		if (isset($params['code']))
		{
			// This is the callback from Facebook, get the code parameter
			$code = $params['code'];

			$url = 'https://graph.facebook.com/oauth/access_token';
			$arpost = array(
				'client_id' => Zend_Registry::get('facebook_client_id'),
				'redirect_uri' => Zend_Registry::get('facebook_redirect_uri'),
				'client_secret' => Zend_Registry::get('facebook_client_secret'),
				'code' => $code);

			// Now request the access_token
			$result = $this->requestFacebookAPI_GET($url,$arpost);

			if ($result === FALSE)
			{
				// Redirect to error page
			}
			else
			{
				parse_str($result, $arresult);

				// Use the access_token to retrieve the user's profile
				$url = 'https://graph.facebook.com/me';
				$arpost = array('access_token' => $arresult['access_token']);

				$result = $this->requestFacebookAPI_GET($url,$arpost);

				if ($result === FALSE)
				{
					// Redirect to error page
				}
				else
				{
					$arprofile = json_decode($result,true);

					$data = array(
						'facebook_access_token' => $arresult['access_token'],
						'facebook_name' => $arprofile['name'],
						'facebook_id' =>  $arprofile['id']
					);
					fb(print_r($arprofile,true));
					$this->view->data=$data;
					// Ignore if the user is logged in
					if (!Zend_Registry::get('defSession')->currentUser && $data['facebook_id']) {

						$hasLogin = $this->login($data['facebook_id']);

						if (!$hasLogin) {
							// We have a logged in user but no account created?
							$usersTable = new Users();
							$username = str_replace(' ', '', $data['facebook_name']);
							fb("username".$username);
							// Check if the username exists
							while ($usersTable->hasUsername($username)) {
								$username = str_replace(' ', '', $data['facebook_name']).rand(1000, 9999);
							}

							try {
								$newUserId = $usersTable->addFB($data['facebook_id'], $username, $arprofile['first_name'], $arprofile['last_name'], $arprofile['email']);
							}
							catch(Zend_Db_Statement_Exception $e) {
								// Don't know how to handle this...hmm.
								// Likely the problem here is that the user has not granted us access to his email address
								// We should show a polite message and have him retry
							}

							$this->login($data['facebook_id']);
						}
					}
					$this->_redirect('/app/', array ('exit'=>true));

				}
			}
		}
	}

	private function requestFacebookAPI_GET($url, $arpost) 
	{
		//
		// Create a cURL request to Facebook
		//
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url . "?" . http_build_query($arpost));
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;

	}
	
	private function requestFacebookAPI_POST($url, $arpost) 
	{
		//
		// Create a cURL request to Facebook
		//
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arpost));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = curl_exec($ch);

		curl_close($ch);

		return $result;
	}
   
	protected function login($fb_user) 
	{

		// check to see if account id already exists, if not redirect
		$auth = Zend_Auth::getInstance();

		$db = Zend_Registry::get('db');
		$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'idfacebook', 'idfacebook');
		$authAdapter->setIdentity($fb_user)->setCredential($fb_user);
		$authResult = $auth->authenticate($authAdapter);
		if ($authResult->isValid()) 
		{
			//valid username and password
			$userInfo = $authAdapter->getResultRowObject();
			if ($userInfo->active) 
			{
				//save userinfo in session
				$userInfo->password = '';
				Zend_Registry::get('defSession')->currentUser = $userInfo;
				Zend_Registry::get('defSession')->facebookUser = $fb_user;
				// Write last_login info to db
				$users = new Users(); 
				$now = new Zend_Date(); 
							
				$userData = array('last_login' =>  $now->toString('YYYY-MM-dd HH:mm:ss') );
				$users->editProfile($userInfo->id, $userData);
		
				return true;

			} 
			else 
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function twitterauthAction()
	{
		require_once 'Zend/Oauth/Consumer.php';

		// create oauth object
		$oauth=new Zend_Oauth_Consumer(self::$config);
		// get request token
		try{
			$request_token = $oauth->getRequestToken();
		}
		catch(Exception $e)
		{
			echo 'Error: '.$e->getMessage();
			exit (1);
		}
		// store request token in session
		$_SESSION['request_token']=serialize($request_token);

		// explode request token to extract oauth token
		$exploded_request_token=explode('=',str_replace('&','=',$request_token));
		// get oauth token from exploded request token
		$oauth_token=$exploded_request_token[1];
		// show sign in with twitter button
		echo "<a href='http://twitter.com/oauth/authorize?oauth_token={$oauth_token}'><img src='sign-in-with-twitter-button.png' alt='Twitter button' /></a>";
	}
	public function twittercbAction()
	{
		require_once 'Zend/Oauth/Consumer.php';

		$oauth = new Zend_Oauth_Consumer(self::$config);

		if (isset($_GET['oauth_token']) && isset($_SESSION['request_token'])) {
			// try to get the access token
			try{
				$access = $oauth->getAccessToken($_GET, unserialize($_SESSION['request_token']));
			}	
			catch(Exception $e)
			{
			 echo 'Error: '.$e->getMessage();
			 exit (1);
			}
			$_SESSION['access_token'] = serialize($access);
			$_SESSION['request_token'] = null;
			print_r($_SESSION['access_token']);
		}elseif(!empty($_GET['denied'])){
			echo 'you have denied us access to your twitter crendentials';
		}
		else {
		    echo 'Invalid callback request. Oops. Sorry.';
		}
	}

	public function googleauthAction()
	{
		$google = new Google();
		$plus = $google->getPlusService();
		$client = $google->getClient();

		if (isset($_GET['code'])) {
  			$client->authenticate();
  			$_SESSION['token'] = $client->getAccessToken();
  			header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

		if (isset($_SESSION['token'])) {
  			$client->setAccessToken($_SESSION['token']);
		}

		if ($client->getAccessToken()) {
  			$me = $plus->people->get('me');

 		 	$optParams = array('maxResults' => 100);
  			$activities = $plus->activities->listActivities('me', 'public', $optParams);

  			print "Your Profile: <pre>" . print_r($me, true) . "</pre>";
  			print "Your Activities: <pre>" . print_r($activities, true) . "</pre>";

  			// The access token may have been updated lazily.
  			$_SESSION['token'] = $client->getAccessToken();
		} else {
  			$authUrl = $client->createAuthUrl();
  			print "<a class='login' href='$authUrl'>Connect Me!</a>";
		}
	}
}

