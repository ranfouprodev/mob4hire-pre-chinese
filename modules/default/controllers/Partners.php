<?php
class Partners extends Zend_Controller_Action 
{	
	public function clientsAction()
	{
		$this->_helper->layout()->getView()->headTitle('Mob4Hire | Mobile Crowd Testing and Market Research | Clients');
	}
	
	public function partnersAction()
	{
		$this->_helper->layout()->getView()->headTitle('Mob4Hire | Mobile Crowd Testing and Market Research | Partners'); 
	}
	
	public function associatesAction()
	{
		$this->_helper->layout()->getView()->headTitle('Mob4Hire | Mobile Crowd Testing and Market Research | Associates');
    }
}