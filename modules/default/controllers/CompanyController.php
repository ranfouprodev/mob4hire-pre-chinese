<?php

class CompanyController extends Zend_Controller_Action 
{
	public function indexAction()
	{
		$this->_helper->layout->setLayout('index');      // Override the default behaviour since this page does not have the left menu 
	}
	
	public function awardsAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Awards');
	}
	
	public function contactUsAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Contact Info');
	}
	
	public function investorRelationsAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Investors Investment');
	}
	
	public function ourStoryAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Startup');
	}
	
	public function theTeamAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Management');
	}

	public function careersAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Careers');
	}
}