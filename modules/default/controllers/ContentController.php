<?php

class ContentController extends Zend_Controller_Action 
{

	public function init() {
       $this->s3 = Zend_Registry::get('S3');
       $this->imagebucket = Zend_Registry::get('configuration')->S3->imagebucket;
       $this->securebucket = Zend_Registry::get('configuration')->S3->securebucket;
	}

	
	// for displaying the profile images
	public function imageAction(){

        fb("Entering Image Action");

     // Disable view and layout rendering
        $this->_helper->viewRenderer->setNoRender();
		$this->_helper->viewRenderer->setNoController();
        $this->_helper->layout()->disableLayout();

        $parameters = $this->_getAllParams();

        if ( isset ($parameters['id']))
        {
            
            $projects = new ProjectFiles();
            $response = $this->_response;
		 
		    try{
            	$file = $projects->getFile($parameters['id']);
			
				//check permission to download the file
				if($file['permissions'] == "0" &&  ($file['category']=='profImg' || $file['category']=='projImg'))
				{
					$canDownload = true;
				}
				else{
					$canDownload = false;
				}

				
				if($canDownload){
					$mimeType = $file['mime_type'];

					$name = rawurlencode($file['filename']);

					$location = $file['physical_filename'];

					$bits = $this->s3->getObject($this->imagebucket.'/'.$name);

					// Couldn't find it on AWS, trying URL
					if (strlen($bits) == 0 && isset($location)) {
						$bits = @file_get_contents($location);
					}

					$response->setHeader('cache-control', 'no-transform,public,max-age=300,s-maxage=900');
		            $response->setHeader('Content-type', $mimeType);


		            if (strlen($bits) == 0) {
		            	$tempFile = $_SERVER[DOCUMENT_ROOT]."/images/icons/no_project_image.gif";
 		                $bits = @file_get_contents($tempFile);
				        $response->setHeader('Content-type', "image/gif");
					 	$response->setBody($bits);
		            }
		            else {
		                 $response->setBody($bits);
			        }
				}else{
					    $tempFile = $_SERVER[DOCUMENT_ROOT]."/images/icons/no_project_image.gif";
 		                $bits = @file_get_contents($tempFile);
				        $response->setHeader('Content-type', "image/gif");
						$response->setBody($bits);
				}
				
			}catch(Exception $e){
						$tempFile = $_SERVER[DOCUMENT_ROOT]."/images/icons/no_project_image.gif";
 		                $bits = @file_get_contents($tempFile);
				        $response->setHeader('Content-type', "image/gif");
						$response->setBody($bits);
			}

        }

	}
	
	// Verification for downloading app images
    public function downloadAction() {

        // Disable view and layout rendering
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $parameters = $this->_getAllParams();

        if ( isset ($parameters['id']))
        {
            
            $projects = new ProjectFiles();
            $response = $this->_response;
		 
		    try{
            	$file = $projects->getFile($parameters['id']);
			
				//check permission to download the file
				if($file['permissions'] == "0" && ($file['category']=='projImg' || $file['category']=='profImg'))
				{
					$canDownload = true;
				}
				else{
					$canDownload = false;
				}


				if($canDownload){

					$mimeType = $file['mime_type'];

					$name = rawurlencode($file['filename']);

					$location = $file['physical_filename'];

					$bits = $this->s3->getObject($this->imagebucket.'/'.$name);

					// Couldn't find it on AWS, trying URL
					if (strlen($bits) == 0 && isset($location)) {
						$bits = @file_get_contents($location);
					}

					// Finally, trying the database.
					if (strlen($bits) == 0){
						$bits = $file['filedata'];
					}

					if (strlen($bits) == 0) {
						$response->setBody($this->view->translate->_('app_project_download_unavailable'));
					}
					else {
						$response->setHeader('cache-control', 'no-transform,public,max-age=300,s-maxage=900');
						$response->setHeader('Content-type', $mimeType);
						$response->setBody($bits);
					}
				}else{
					$response->setBody($this->view->translate->_('app_project_download_access'));
				}

			}catch(Exception $e){
				$response->setBody($this->view->translate->_('app_project_download_error') );
			}

        }


    }
	
}