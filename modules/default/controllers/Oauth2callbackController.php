<?php

class Oauth2callbackController extends Zend_Controller_Action 
{
	public function indexAction() {
		 $this->view->layout()->setLayout('social');
                $google = new Google();
                $plus = $google->getPlusService();
                $client = $google->getClient();

                if (isset($_GET['code'])) {
                        $client->authenticate();
                        $_SESSION['token'] = $client->getAccessToken();
                        header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
		}

                if (isset($_SESSION['token'])) {
                        $client->setAccessToken($_SESSION['token']);
                }

                if ($client->getAccessToken()) {
                        $me = $plus->people->get('me');

                        $optParams = array('maxResults' => 100);
                        $activities = $plus->activities->listActivities('me', 'public', $optParams);

                        print "Your Profile: <pre>" . print_r($me, true) . "</pre>";
                        print "Your Activities: <pre>" . print_r($activities, true) . "</pre>";

                        // The access token may have been updated lazily.
                        $_SESSION['token'] = $client->getAccessToken();
                } else {
                        $authUrl = $client->createAuthUrl();
                        print "<a class='login' href='$authUrl'>Connect Me!</a>";
        	}                        
	}


}
