<?php
class Zend_View_Helper_ShowProjects
{
	public $view;

	function showProjects($mode, $orderBy='StartDate desc,Name', $pageNumber=1, $resultsPerPage=4, $options=array()) 
	{	
		
		$projects=new Projects();
	
		// sets the defaults for the options
		if(!array_key_exists("showcomments", $options))
			$options['showcomments'] = false;
			
		if(!array_key_exists("navigation", $options))
			$options['navigation'] = 'none';

		if(!array_key_exists("layout", $options))
			$options['layout'] = 'standard';	
		
		if(array_key_exists("search", $options))
			$search = $options['search'];
			
		$script = 	'var layout="'.$options['layout'].
					'";var resultsPerPage='.$resultsPerPage.
					';var mode = "'.$mode.
					'";var navigation = "'.$options['navigation'].'"';
		if($options['showcomments'])
			$script .=';var showComment = true';
		else
			$script .=';var showComment = false';
			
			
		if(isset($search))
		 $script .= ';var search ="'.$search.'"';
			
		$this->view->headScript()->appendScript($script);		
			
		if(isset(Zend_Registry::get('defSession')->currentUser->id)){
			$iduser=Zend_Registry::get('defSession')->currentUser->id;
			$lastLogin=strtotime(Zend_Registry::get('defSession')->currentUser->last_login);
		}else{
			$iduser=0;
			$lastLogin=0;	
		}
		
		$useQuickQuery=false; // We are going to set this flag when we want to use the new, faster, stripped down query
		
		switch($mode){
			case "tester":
					$whereClause="projects.Status < 499 and projects.idproject IN (SELECT idproject FROM tests WHERE tests.idtester=$iduser AND tests.Status IN (250,300, 350))";
					$totalProjects=$projects->countProjects($whereClause, $iduser);
				break;
			case "developer":
					$whereClause="projects.Status < 499 and projects.iddeveloper='".$iduser."'";
					$totalProjects=$projects->countProjects($whereClause, $iduser);
				break;
			case "compatible":
					$whereClause=$projects->getCompatibleProjects($iduser);
					$totalProjects=$projects->countProjects($whereClause, $iduser);
				break;
			case "open":
					$whereClause="projects.Status = 201 or projects.Status = 202 or projects.Status = 203";	
					$totalProjects=$projects->countProjects($whereClause, $iduser);
				break;
			case "draft":
					$whereClause="projects.Status = 100 AND projects.iddeveloper=".$iduser;
					$totalProjects=$projects->countProjects($whereClause, $iduser);
				break;
			case "search":
					$searchString = $options['search'];
					// run the search
					$search = new Search_ProjectSearch();
					$hits = $search->search($searchString);
			
					$results = array(); 
					foreach ($hits as $hit) {
						$results[] = $hit->idproject;
					}
				
					if(isset($results) && sizeof($results)>1){
						$whereClause = "projects.idproject in (";
						foreach ($results as $result) {
							
							$whereClause .= $result;
							$whereClause .= ',';
						}
						$whereClause = substr($whereClause,0,-1);
						$whereClause .= ")";
			
						$totalProjects = sizeof($results);	
					}
					
				
				break;
			default:
					$whereClause="projects.Status > 199 AND projects.Status < 499 AND (projects.inviteonly=0 OR projects.iddeveloper=$iduser)";
					$totalProjects=$projects->countProjectsQuick($whereClause);
					$useQuickQuery=true; // you can comment out this line to evaluate one query against another
				break;
			
		}
		if ($useQuickQuery)
			$projectList=$projects->listProjectsQuick($whereClause, $orderBy, $pageNumber,$resultsPerPage);	
		else
			$projectList=$projects->listProjects($whereClause, $orderBy, $pageNumber,$resultsPerPage, $iduser);
		 
		// Shows all the projects based on the listprojectsajax
		$return='<div class="project-listing">';
		
		if(count($projectList)==0){
			// Render the no results page
			$return .=$this->view->partial('partial/_project_error.phtml','app', array('mode'=>$mode) );
		}else{
		
			if($options['navigation'] == "top")
				$return .= $this->view->partial('partial/_project_navigation.phtml','app', array("order"=>$orderBy));
			
			$tests = new Tests(); 	
			foreach($projectList as $project){
				//if($iduser != 0)
				$test = $tests->listTestsByTesterAndProject($project['idproject'], $iduser);
					
				$return .=$this->view->partial('partial/_project_default_status.phtml','app', array('options'=>$options,'project'=>$project,'tests'=>$test,'lastLogin'=>$lastLogin) );
			}
			 
			// Add pagination
			if($resultsPerPage != 0){
				$posts = (int)$totalProjects;
				
				$pages = ($posts / $resultsPerPage);
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
		
				$return .= $this->view->partial('partial/_project_pagination.phtml','app', array("order"=>$orderBy,"pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious));
			}
		}
		$return.='</div>';
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}