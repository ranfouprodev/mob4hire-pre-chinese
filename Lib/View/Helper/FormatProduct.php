<?php
// This should eventually be moved into the model! Why?

class Zend_View_Helper_FormatProduct
{
	function formatProduct($name) 
	{
		switch(strtolower($name)){
			case "mobtestsuite":
				$link = '<a href="/services/mobtestsuite" alt="MOBTESTSUITE">MOB<span class="mobred">TESTSUITE</span></a>';
			break;
			
			case "mobaccelerator":
				$link = '<a href="/services/accelerator" alt="MOBACCELERATOR">MOB<span class="mobred">ACCELERATOR</span></a>';
			break;
			case "mobexperience":
				$link = '<a href="/services/mobile-app-research" alt="MOBEXPERIENCE">MOB<span class="mobred">EXPERIENCE</span></a>';
			break;
			
			case "mobstar":
				$link = '<a href="/services/star" alt="MOBSTAR Certified">MOB<span class="mobred">STAR</span> Certified</a>';
			break;
			case "mobtest":
				$link = '<a href="/services/mobile-app-testing" alt="MOBTEST">MOB<span class="mobred">TEST</span></a>';
			break;
			case "mobsimtest":
				$link = '<a href="/services/mobsimtest" alt="MOBSIMTEST">MOB<span class="mobred">SIMTEST</span></a>';
			break;
			case "mobsmstest":
				$link = '<a href="/services/mobsmstest" alt="MOBSMSTEST">MOB<span class="mobred">SMSTEST</span></a>';
			break;
			case "mobsurvey":
				$link = '<a href="/services/market-research" alt="MOBSURVEY">MOB<span class="mobred">SURVEY</span></a>';
			break;
			case "mobabtest":
				$link = '<a href="/services/mobabtest" alt="MOBA/B TEST">MOB<span class="mobred">A/BTEST</span></a>';
			break;
			case "mobtasks":
				$link = '<a href="/services/mobtasks" alt="MOBTASKS">MOB<span class="mobred">TASKS</span></a>';
				break;
			case "mobdevelop":
				$link = '<a href="#" >MOB<span class="mobred">DEVELOP</span></a>';
				break;	
			case "mobmysteryshopping":
				$link = '<a href="#" >MOB<span class="mobred">MYSTERYSHOPPING</span></a>';
				break;
			default:
				$link = '<a href="#" >MOB<span class="mobred">'.strtoupper($name).'</span></a>';
			break;
		}
		
		return $link;
	}
}