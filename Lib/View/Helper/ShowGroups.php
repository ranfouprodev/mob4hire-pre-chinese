<?php
class Zend_View_Helper_ShowGroups
{
	public $view;

	function ShowGroups( $mode, $pageNumber=1, $resultsPerPage=4, $options=array()) 
	{
		// sets the defaults for the options
		if(!array_key_exists("showcomments", $options))
			$options['showcomments'] = false;
			
		
		if(!array_key_exists("layout", $options))
			$options['layout'] = "standard";	
			
		$this->view->headScript()->appendScript('var mode="'.$mode.'";var page='.$pageNumber.';var resultsPerPage = '.$resultsPerPage.';var selector = true;var layout = "'.$options['layout'].'";');	
			
		
			
		// 	Groups	
		$groups = new Groups(); 
		
		
		// add switch for mode here
		switch($mode){
			case "self":
				$where = "g.idowner = ".$this->view->currentUser->id;
				$where .= " or g.idgroup in (select idgroup from groups_user where iduser =".$this->view->currentUser->id.")";
				$order = "members desc";		
				break;
			default:
				$where = "g.type=1 and status=100";
				$order = "members desc";
				break;
		}
		
		$groupList = $groups->listGroups($where,$order,$pageNumber,$resultsPerPage);
		
		$return='<div class="group-listing">';
		
		
		
		if(count($groupList)==0){
			// Render the no results page
			$return .=$this->view->partial('group/_groups_error.phtml','app');
		}else{
			foreach($groupList as $group){	
				$return .=$this->view->partial('group/_layout.phtml','app', array('group'=>$group,'options'=>$options) );
			}
			// Add pagination
			if($resultsPerPage != 0){
				$posts = $groups->count($mode,$this->view->currentUser->id);
				
				$pages = ceil(($posts / $resultsPerPage));
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
		
				$return .= $this->view->partial('group/_groups_pagination.phtml','app', array("pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious));
			}
		}
		
		
		$return.='</div>';
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}