<?php
class Zend_View_Helper_ShowSearchUsers
{
	public $view;

	function ShowSearchUsers( $searchString, $order='', $pageNumber=1, $resultsPerPage=4, $options=array()) 
	{
		// sets the defaults for the options
		if(!array_key_exists("showcomments", $options))
			$options['showcomments'] = false;
			
		if(!array_key_exists("navigation", $options))
			$options['navigation'] = 'none';
			
		if(!array_key_exists("selector", $options))
			$options['selector'] = 'none';
			
		if(!array_key_exists("layout", $options))
			$options['layout'] = "standard";	

		$this->view->headScript()->appendScript('var search="'.$searchString.'";var page='.$pageNumber.';var resultsPerPage = '.$resultsPerPage.';var selector = "'.$options['selector'].'";var showcomment = "'.$options['showcomments'].'";var layout = "'.$options['layout'].'";');	
	
			
		// run the search
		$search = new Search_UserSearch();
		$hits = $search->search($searchString);
		
		$results = array(); 
		foreach ($hits as $hit) {
			$results[] = $hit->iduser;
		}
		
		
		$contactList = array(); 
		
		if(isset($results) && sizeof($results)>1){
			$whereClause = "u.id in (";
			foreach ($results as $result) {
				
				$whereClause .= $result;
				$whereClause .= ',';
			}
			$whereClause = substr($whereClause,0,-1);
			$whereClause .= ")";

			// Contact details	
			$users=new Users();
			$contactList=$users->listUsersForSearch($whereClause, 'MONTH(last_login) desc, rating desc,total_votes desc',$pageNumber, $resultsPerPage);	
			
		}
		
		
		
		// Shows all the projects based on the listprojectsajax
		$return='<div class="contact-listing">';
		
		if(count($contactList)==0){
			// Render the no results page
			$return .=$this->view->partial('contacts/_contacts_error.phtml');
		}else{
		
			//if($options['navigation'] == "top")
			//	$return .= $this->view->partial('partial/_project_navigation.phtml', array("order"=>$orderBy));
		
			foreach($contactList as $contact){	
				// 	Handset Details
				$regdevices=new Regdevices();
				$handsets=$regdevices->getHandsetsByUser($contact->iduser);
				
				$return .=$this->view->partial('contacts/_layout.phtml', array('options'=>$options,'contact'=>$contact,'handsets'=>$handsets) );
			}
			 
			// Add pagination
			if($resultsPerPage != 0){
				$posts = sizeof($results);
				
				$pages = ceil($posts / $resultsPerPage);
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
		
				$return .= $this->view->partial('contacts/_contacts_pagination.phtml', array("pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious));
			}
		}
		$return.='</div>';
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}