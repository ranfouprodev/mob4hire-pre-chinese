<?php
// This should eventually be moved into the model!

class Zend_View_Helper_GetUnreadMessages
{
	function getUnreadMessages($iduser, $idtarget=0, $postType=0, $timestamp=0) 
	{
		$wall = new Wall(); 

		
		switch($postType){
			case 0:
				return $wall->countUnread($iduser);
			case 1:
			case 2:
			case 3:
				return $wall->countNewNotMyPosts($idtarget, $postType,$iduser, $timestamp);		
			default:
				return $wall->countUnread($iduser);
		}
		
		
		
	}
}