<?php

class Zend_View_Helper_RatingBar
{
	function ratingBar($id, $username, $units='',$static='',$tableName='', $showNumberofRatings=false) 
	{
        $translate = Zend_Registry::get('Zend_Translate');

		$ratings=new Ratings();
		$row=$ratings->fetchRow("id='$username'");
		if ($row && $row->total_votes) {
			$rating=$row->total_value/$row->total_votes; // do not divide by zero!
		}
		else {
			$rating=0;
		}
		// show mobPro status next to ratings
		$users=new Users();
		$userrow=$users->fetchRow("username='$username'");
		$pro=0;
		if($userrow && $userrow->pro) {
			$pro=1;
		}
		
		$width=number_format($rating * 20, 2);
			
		$return ="";
		
		if($static != 'static')
			$return .="<a href='/app/profile/rating/id/$id'>";
		
		$return .="
			<div class='ratingblock'>
				<div id='unit_long'>
					<ul id='unit_ul' class='unit-rating' style='width: 100px;'>
						<li class='current-rating' style='width: ".$width."px;'>".$translate->_('app_rating_bar_currently')." $rating/5</li>
					</ul>";
		if($pro)
		{
			$return.="<img class='pro' src='/images/Certified-Tester-Icon-2.gif' />";
		}
			
		if($showNumberofRatings && $row)
		{
			$return.="<label>(".$translate->_('app_rating_bar_total_votes')." ".$row->total_votes.")</label>";
		}
		$return.="</div>
			</div>";
		
		if($static!='static')
			$return .="</a>";
		return $return;

	}
}