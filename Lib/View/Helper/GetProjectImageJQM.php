<?php

class Zend_View_Helper_GetProjectImageJQM
{
	function getProjectImageJQM($project) 
	{
		if($project['ImagePath']){
		    $img_path = $project['ImagePath'];
		}else{
			$img_path="/images/app/role-icon-mobtest-available.png";
		}
		return '<img src=' . $img_path . ' alt="' . $project['Name'] . '" />';
 	}
}