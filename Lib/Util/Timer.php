<?php
class Timer  {
	
	static public function start() {
		$timer = microtime();
		$timer = explode(" ", $timer);
		$timer = $timer[1] + $timer[0];
		
		return $timer;
	}
	
	static public function end( $timer ) {
		$endTimer = microtime();
		$endTimer = explode(" ", $endTimer);
		$endTimer = $endTimer[1] + $endTimer[0];

		return $endTimer - $timer;
	}
	
	static public function stop( $timer ) {
		$endTimer = microtime();
		$endTimer = explode(" ", $endTimer);
		$endTimer = $endTimer[1] + $endTimer[0];

		return $endTimer - $timer;
	}
}