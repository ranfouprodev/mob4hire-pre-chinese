<?php

class Lib_Controller_Plugin_Modularlayout extends Zend_Controller_Plugin_Abstract
{
     public function routeShutdown(Zend_Controller_Request_Abstract $request)
     {
	 try {
		if ($request->getModuleName()=='mobile')
		{
			Zend_Layout::getMvcInstance()->setLayout('mobile'); // Specific layout for mobile site
		}
		elseif ($request->getModuleName()=='jqm')
		{
			Zend_Layout::getMvcInstance()->setLayout('jqm'); // Specific layout for mobile site
		}

		elseif ($request->getModuleName()=='app' )
		{
			if(Zend_Registry::get('defSession')->currentUser)
			{
				if($request->getControllerName()=='index')
					Zend_Layout::getMvcInstance()->setLayout('app');
				else
					Zend_Layout::getMvcInstance()->setLayout($request->getControllerName()); // else per controller layout to handle left menus
			}
			else
				Zend_Layout::getMvcInstance()->setLayout('main');				
		}
		else
			Zend_Layout::getMvcInstance()->setLayout($request->getControllerName()); // else per controller layout to handle left menus
	 }
	 catch (Exception $e) {
		// If setLayout failed, likely there is no specific layout for this controller so use the default one
		Zend_Layout::getMvcInstance()->setLayout('main');
	 }
     }
}