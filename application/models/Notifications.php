<?php

/*
 * Generic Notification System 
 */
class Notifications extends Zend_Db_Table_Abstract
{
    protected $_name = 'notifications';
    protected $_primary = 'idnotification';


	public function getNotification($id)
	{
		$id=(int)$id; // Make sure id is an integer
		$row = $this->fetchRow('idnotification = ' . $id . ' and status =1');
		
		if (!$row) {
			throw new Exception("Could not find row $id");
		}
		return $row->toArray();
	}

	public function getNotificationsForUser($iduser)
	{
			$select=$this->select();
	 		$select->setIntegrityCheck(false);
			$select->distinct()
				->from($this)
				->where("iduser=? and status =1",$iduser)
				->order(array("posted desc"));
	//		echo $select->__toString();
	 		$rows= $this->fetchAll($select);
			return $rows;	
		
				
	}


	public function addNotification( $iduser, $message) {

		$data = array (
				'iduser'=>$iduser,
				'message'=>$message,
				'posted'=>new Zend_Db_Expr('NOW()'),
				'status'=>'1');

		try {
			$idfile = $this->insert($data);
		}
		catch(Exception $e)
		{
			throw new Exception("Error inserting notification");
		}
		
		return $idfile;
	}

	public function removeNotification( $idfile)
	{
	
		$id=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idnotification = ' . $idfile);
		
		if($row){
			$row->status="0";
			$row->save();	
		}

	}

  

}
