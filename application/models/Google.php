<?php

require_once 'google-api-php-client/src/apiClient.php';
require_once 'google-api-php-client/src/contrib/apiPlusService.php';

class Google
{
	private static $applicationName = 'Mob4Hire';
	private static $clientId = '756203085254.apps.googleusercontent.com';
	private static $clientSecret = '9HS-CAlIGv2bGs2FpV_i2FXx';
	private static $redirectURI = 'http://staging.mob4hire.com/oauth2callback';
	private static $developerKey = 'AIzaSyAtC7ANr0-_M3XpKVB7l2L97GYMzAdoPFQ';

	protected $apiClient;

	function __construct() {
		$this->apiClient = new apiClient();
		$this->apiClient->setApplicationName(self::$applicationName);		
		$this->apiClient->setClientId(self::$clientId);
		$this->apiClient->setClientSecret(self::$clientSecret);
		$this->apiClient->setRedirectURI(self::$redirectURI);
		$this->apiClient->setDeveloperKey(self::$developerKey);
	}

	public function getClient() {
		return $this->apiClient;
	}

	public function getPlusService(){
		return new apiPlusService($this->apiClient);
	}
}
