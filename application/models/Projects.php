<?php
require_once 'Lib/Cache/CacheFactory.php';
require_once 'Lib/Util/Timer.php';


class Projects extends Zend_Db_Table_Abstract  
{
	

	protected $_name = 'projects';

	protected  $_referenceMap=array(
								'Survey'=>array(
									'columns'=>array('idsurvey'),
									'refTableClass' => 'Survey',
									'refColumns' => array('idsurvey')
									)
							);
 
	static $status=array(100=>"Draft", 101=>"Under Review", 102=>"Rejected", 
			200=>"Waiting for Project Bid Start Date", 201=>"Waiting for Mobsters to Apply",202=>"Waiting for Mobsters to Apply", 
			300=>"Bidding Closed", 
			400=>"Testing Ended",401=>"Completed",
			500=>"Archived",501=>"Deleted");


	public function init()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		Projects::$status[100] = $translate->_('model_projects_status_draft');
		Projects::$status[101] = $translate->_('model_projects_status_underReview');
		Projects::$status[102] = $translate->_('model_projects_status_rejected');
		Projects::$status[200] = $translate->_('model_projects_status_waitStart');
		Projects::$status[201] = $translate->_('model_projects_status_waitMobster');
		Projects::$status[202] = $translate->_('model_projects_status_waitMobster');
		Projects::$status[300] = $translate->_('model_projects_status_bidClosed');
		Projects::$status[400] = $translate->_('model_projects_status_testEnded');
		Projects::$status[401] = $translate->_('model_projects_status_completed');
		Projects::$status[500] = $translate->_('model_projects_status_archived');
		Projects::$status[501] = $translate->_('model_projects_status_deleted');

		$this->language = Zend_Registry::get("configuration")->general->language;
	}




	public function count()
	{
		$cache = CacheFactory::getCache('projects');
		$countValue = 0;
		$timer = Timer::start();
		if ( !$countValue = $cache->get('count') ) {
			$select=$this->select();
			$select->from($this, array ('COUNT(idproject) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			
			$cache->setE('count', $countValue, Timer::end($timer) );
		} else {
			$elapsed = Timer::end($timer);
		}
		
		return $countValue;
	}
	
	public function listProjects($whereClause='1', $orderBy='StartDate desc',  $pageNumber=1, $resultsPerPage=0, $userid=0)
	{



		$cache = CacheFactory::getCache('projects');
		$rows = array();
		
		$cacheKey = 'listProjects_' . $whereClause . $orderBy . $pageNumber . $resultsPerPage .$userid;
		$timer = Timer::start();
		
		if ( !$rows = $cache->get($cacheKey ) ) {
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// join to developers table to get developer username
			$select->distinct()
				->from($this)
				->order(explode(",",$orderBy))
				->order('projects.idproject desc') // second order by newest (multiple files on the same date should order)
				->join(array('d'=>'users'), 'd.id=projects.iddeveloper',  array("d.username"))
				->joinLeft(array('t'=>'tests'),"t.idproject=projects.idproject and t.idtester='".$userid."' and t.status>=75", array("t.Status as testerStatus","t.idtester","t.idtest"))
				->joinLeft(array('sn'=>'supportednetworks'), 'sn.idproject=projects.idproject' , array('sn.idnetwork', 'sn.idcountry'))
				->joinLeft(array('sd'=>'supporteddevices'), 'sd.idproject=projects.idproject', array('sd.tid', 'sd.vendor', 'sd.platform'))
				->joinLeft(array('ti'=>'tester_invites'), "ti.idproject=projects.idproject and ti.idtester='$userid'", array("ti.idinvite"))
				->where($whereClause)
				->where("sitevisibility='all' OR sitevisibility='".$this->language."'")
//				->where("(projects.inviteonly!=1) OR (projects.inviteonly=1 AND (not isnull(ti.idtester))")
				  // The above was added so that invite only projects only show to those invited
				->group('projects.idproject');
				
			if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);
			
			//echo $select->__toString();	
				
			$rows= $this->fetchAll($select);

			$cache->setE($cacheKey, $rows, Timer::end($timer) );
		}
		return $rows;
	}
	
	public function listProjectsQuick($whereClause='1', $orderBy='StartDate desc',  $pageNumber=1, $resultsPerPage)
	{
		$cache = CacheFactory::getCache('projects');
		$rows = array();
		
		$cacheKey = 'listProjects_' . $whereClause . $orderBy . $pageNumber . $resultsPerPage;
		$timer = Timer::start();
		
		if ( !$rows = $cache->get($cacheKey ) ) {
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// join to developers table to get developer username
			$select->distinct()
				->from($this)
				->order(explode(",",$orderBy))
				->order('projects.idproject desc') // second order by newest (multiple files on the same date should order)
				->join(array('d'=>'users'), 'd.id=projects.iddeveloper',  array("d.username"))
				->where("sitevisibility='all' OR sitevisibility='".$this->language."'")
				->where($whereClause);
//				->where("projects.inviteonly!=1");
				  // The above was added so that invite only projects do not show
			
			if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);
			
			fb($select->__toString());	
				
			$rows= $this->fetchAll($select);

			$cache->setE($cacheKey, $rows, Timer::end($timer) );
		}
		return $rows;
	}
	
	public function countProjects($whereClause='1', $userid=0)
	{
		
		$cache = CacheFactory::getCache('projects');
		$rows = array();
		$cacheKey = 'count_' . $whereClause . $userid;
		$count = 0; 
		
		$timer = Timer::start();
		if ( !$count = $cache->get($cacheKey ) ) {
				$rows = array();
				$timer = Timer::start();
				$select=$this->select();
				$select->setIntegrityCheck(false);
				// join to developers table to get developer username
				$select->from($this,array('projects.idproject'))
						->join(array('d'=>'users'), 'd.id=projects.iddeveloper',  array("d.username"))
						->joinLeft(array('t'=>'tests'),"t.idproject=projects.idproject and t.idtester='".$userid."' and t.status>=75", array("t.Status as testerStatus","t.idtester","t.idtest"))
						->joinLeft(array('sn'=>'supportednetworks'), 'sn.idproject=projects.idproject' , array('sn.idnetwork', 'sn.idcountry'))
						->joinLeft(array('sd'=>'supporteddevices'), 'sd.idproject=projects.idproject', array('sd.tid', 'sd.vendor', 'sd.platform'))
						->joinLeft(array('ti'=>'tester_invites'), "ti.idproject=projects.idproject and ti.idtester='$userid'", array("ti.idinvite"))
						->where($whereClause)
						->where("sitevisibility='all' OR sitevisibility='".$this->language."'")
						->group('projects.idproject');
						
		//		echo $select->__toString();
				$rows= $this->fetchAll($select);
				$count = $rows->count();
				
				$cache->setE($cacheKey, $count, Timer::end($timer) );
		}
		
		return $count;
	}
	
	public function countProjectsQuick($whereClause='1')
	{
		
		$cache = CacheFactory::getCache('projects');
		$rows = array();
		$cacheKey = 'count_' . $whereClause;
		$count = 0; 
		
		$timer = Timer::start();
		if ( !$count = $cache->get($cacheKey ) ) {
				$rows = array();
				$timer = Timer::start();
				$select=$this->select();
				$select->setIntegrityCheck(false);
				// join to developers table to get developer username
				$select->distinct()
					->from($this)
					->where("sitevisibility='all' OR sitevisibility='".$this->language."'")
					->where($whereClause);
						
		//		echo $select->__toString();
				$rows= $this->fetchAll($select);
				$count = $rows->count();
				
				$cache->setE($cacheKey, $count, Timer::end($timer) );
		}
		
		return $count;
	}	
	public function getProject($idproject)
	{
		$cache = CacheFactory::getCache('projects');
		$cacheKey = 'getProject_' . $idproject;
		if ( !$result = $cache->get($cacheKey) ) {	
		
			// This is not the best way of doing this. It would be better to use relationships and return related rows. It will do for now
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// join to developers table to get developer username
			$select->distinct()
				->from($this)
				->join(array('d'=>'users'), 'd.id=projects.iddeveloper',  array("d.username", "d.first_name","d.last_name", "d.email"))
				->where('idproject=?', $idproject);
				
			//echo $select->__toString();	
			$rows = $this->fetchRow($select);
			$result = $rows;
			$cache->set($cacheKey, $result );
		}
		return $result;	
	}

	public function add($projectData)
	{
		$timestamp = time();
		$projectData['timestamp'] = $timestamp;
		// @todo add site origination field
		$idproject=$this->insert($projectData);
		
		$cache = CacheFactory::getCache('projects');
		$cache->clean();
				
		$searchIndex = new Search_ProjectSearch();
		$searchIndex->add($idproject);
		
		return $idproject;
	}

	public function updateProject($idproject, $projectData)
	{
		$projectRowset = $this->find($idproject);
		$project = $projectRowset->current();

		foreach ($projectData as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of user cannot be changed');
				}
				else
				{
					$project->{$name}=$value;						
				}
			}
		}
		$project->save();
		
		$cache = CacheFactory::getCache('projects');
		$cache->clean(); 
		
		return $this; // to allow chaining of methods
	}
	
	public function addSupportedDevices($idproject, $devices, $uploads)
	{
		if (isset($uploads[0][0][0])) 
		{
			$genericexecutable=$uploads[0][0][0];
		}
		else 
		{
			$genericexecutable="";
		}
		
		foreach($devices as $idmake=>$array) 
		{
			$supportedDevices=new SupportedDevices();
			foreach($array as $iddevice) 
			{
				// Does the device have its own executable
				if (!isset($uploads[1]) || !isset($uploads[1][$i][0]) || ($uploads[1][$i][0]=="")) 
				{			
					$executable=$genericexecutable;
					// If generic executable is a null string then this is a validation error - trouble is project has already been written
				}
				else 
				{
					$executable=$uploads[1][$i][0];
				} 
				if (!$supportedDevices->addSupportedDevice($idproject, $iddevice)) 
				{
					return 0;
				}
			$i++;
			}	
		}
	}
	
	public function addSupportedNetworks($idproject, $networks)
	{
		$supportedNetworks=new SupportedNetworks();
		foreach($networks as $idcountry=>$array) 
		{
			foreach($array as $idnetwork) 
			{
				if (!$supportedNetworks->addSupportedNetwork($idproject, $idcountry, $idnetwork)) 
				{
					return 0;
				}
			}
		}	
	}
	
	public function getSupportedDevices($idproject)
	{
		$supporteddevices = new SupportedDevices();
		$result=$supporteddevices->getSupportedDevices($idproject);
		return $result;
	}
	
	public function getSupportedNetworks($idproject)
	{
		$supportednetworks=new SupportedNetworks();
		$result=$supportednetworks->getSupportedNetworks($idproject);
		return $result;
	}
	
	public function getStatus($idproject)
	{
		$tests = new Tests();
		$result = $tests->getStatusByProject($idproject);
		return $result;
	}

	public function getProjectsByDeveloper($id, $status=499,$pageNumber=1, $resultsPerPage=0)
	{
		return $this->listProjects("projects.iddeveloper=$id and projects.status < $status", "projects.idproject desc",$pageNumber, $resultsPerPage);
	}

	/*
		This will return a where clause for use with listProjects function
	*/
	public function getCompatibleProjects($iduser)
	{
		
		$whereClause="((projects.Status = 201 or projects.Status = 202 or projects.Status = 203) and (projects.inviteonly=0 or not isnull(ti.idinvite))) ";
		
		$regdevices= new RegDevices();
		$devices=$regdevices->getHandsetsByUser($iduser);  // get handsets
		if(!$devices->count()) { // user has no devices
			return 0;
		}
		$deviceClause=" AND  (";
		$firstqueryset=0;
		foreach($devices as $device)
		{
			if ($firstqueryset)
				$deviceClause.=' OR ';
			else
				$firstqueryset=1;
			$deviceClause.=$regdevices->isCompatibleDevice($device);
		}
		$deviceClause.=')';
		$whereClause.=$deviceClause;
		return $whereClause;
		
	}
	
	public function canApplyForProject($iduser, $idproject)
	{
		$whereClause=$this->getCompatibleProjects($iduser)." AND projects.idproject=$idproject";
		return $this->listProjects($whereClause, '', 1, 0, $iduser)->current();
	}
	
	public function isCompatibleDevice($device, $idproject)
	{
		$regdevices=new Regdevices();
		$whereClause=$regdevices->isCompatibleDevice($device)." AND projects.idproject=$idproject";
		return $this->listProjects($whereClause)->current();
	}
	
	// used to prevent further bids on the project
	public function closeBidding($idproject)
	{
		$this->updateProject($idproject, array('Status'=>'300')); // Will show as 'in progress' - no further bids will be accepted
	}
	
	public function openBidding($idproject)
	{
		//This will open the bidding up again for a project
		$this->updateProject($idproject, array('Status'=>'201')); 
	}
	
	public function countNewProjects($startDate, $endDate)
	{	
		$count = 0; 
			
		$select=$this->select();
		$tableInfo = array("p" => 'projects');
		$columns = array("count" =>'COUNT(p.idproject)');
		$statement = $select->from($tableInfo, $columns)
			->where("p.StartDate > ?", $startDate)
			->where("p.StartDate < ? ", $endDate);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		
		$count =$row['count'] ;

		
		return $count;
		
	}
	
	
	
	/*
	 * Statistics from here on in 
	 * 
	 */
	public function averageTestsPerProject($startDate, $endDate)
	{
		//This will be in two parts first it will be getting a list of projects in the range.
		//Second we need to count the number of tests for each project id.
		//SELECT COUNT(t.idtest) AS 'count' FROM tests AS t 
		//WHERE t.idproject IN (SELECT p.idproject FROM projects AS p 
		//WHERE p.StartDate > ? AND p.StartDate < ?);
			
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'tests');
		$columns = array("count" =>'COUNT(t.idtest)');
		$whereClause = "t.idproject IN (SELECT p.idproject FROM projects AS p WHERE p.startdate > '"
				.$startDate."' AND p.startDate < '".$endDate."') AND t.status>249 AND t.status<499";
		$statement = $select->from($tableInfo, $columns)
			->where($whereClause);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		$total= $row['count'];
		
		$dog=$this->countNewProjects($startDate, $endDate);
		if($dog==0)
		{
			$dog=1;
		}
		return $total/$dog;
	}
	
	public function averageBidByProject($idproject, $statusFlag)
	{
		//This will return the average bid value from the transactions table by the specified project id
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("tr" => 'transactions');
		$columns = array("average" => 'AVG(tr.BidAmount)');
		$whereClause = "tr.idtest IN (SELECT t.idtest FROM tests AS t WHERE t.idproject = '".$idproject."')";
		$statement = $select->from($tableInfo, $columns)
			->where($whereClause)
			->where("tr.status = ?",$statusFlag);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['average'];
	}
	
	public function averageProjectBid($startDate, $endDate, $statusFlag)
	{
		$select=$this->select();
		$tableInfo = array("p" => 'projects');
		$columns = array("id" =>'p.idproject');
		$statement = $select->from($tableInfo, $columns)
			->where("p.StartDate > ?", $startDate)
			->where("p.StartDate < ? ", $endDate);
	
		$rows=$this->fetchAll($select);
		$total=0;
		
		
		foreach($rows as $row)
		{
			$total=$total +$this->averageBidByProject($row['id'],$statusFlag);
		}
		$dog=$this->countNewProjects($startDate, $endDate);
		if($dog==0)
		{
			$dog=1;
		}
		return $total/$dog;
	}
	
	public function countBidByProject($idproject, $statusFlag, $timestamp=0)
	{
		//This will return the average bid value from the transactions table by the specified project id
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("tr" => 'transactions');
		$columns = array("count" => 'COUNT(tr.BidAmount)');
		$whereClause = "tr.idtest IN (SELECT t.idtest FROM tests AS t WHERE t.idproject = '".$idproject."')";
		$statement = $select->from($tableInfo, $columns)
			->where($whereClause)
			->where("tr.status = ?",$statusFlag)
			->where("tr.Timestamp > ?",$timestamp);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countProjectBid($startDate, $endDate, $statusFlag)
	{
		$select=$this->select();
		$tableInfo = array("p" => 'projects');
		$columns = array("id" =>'p.idproject');
		$statement = $select->from($tableInfo, $columns)
			->where("p.StartDate > ?", $startDate)
			->where("p.StartDate < ? ", $endDate);
	
		$rows=$this->fetchAll($select);
		$total=0;
		
		
		foreach($rows as $row)
		{
			$total=$total +$this->countBidByProject($row['id'],$statusFlag);
		}
		$dog=$this->countNewProjects($startDate, $endDate);
		if($dog==0)
		{
			$dog=1;
		}
		return $total/$dog;
		
	}
	public function listProjectsByStatus($status, $pageNumber, $resultsPerPage)
	{
		$whereClause=1;
		if($status<>-1)
		{
			$whereClause='projects.status ='.$status;
		}
		$rows=$this->listProjects($whereClause,'StartDate desc',  $pageNumber, $resultsPerPage, 0);
		$test = new Tests();
		$trans = new Transactions();
		$resultArray = array();
		foreach($rows as $row)
		{
			$invited=$trans->countTransactionsByStatusByProject(50,$row['idproject']);
			$rejected=$trans->countTransactionsByStatusByProject(75,$row['idproject']);
			$bids=$trans->countTransactionsByStatusByProject(100,$row['idproject']);
			$accepted=$trans->countTransactionsByStatusByProject(200,$row['idproject']);
			$escrowed=$trans->countTransactionsByStatusByProject(400,$row['idproject']);
			$submitted=$trans->countTransactionsByStatusByProject(300,$row['idproject']);
			$cancelled=$trans->countTransactionsByStatusByProject(499,$row['idproject']);
			$failed=0;
			$inprogress=$trans->countTransactionsByStatusByProject(250,$row['idproject']);
			$awaiting=$trans->countTransactionsByStatusByProject(0,$row['idproject']);
			$rejectReport=$trans->countTransactionsByStatusByProject(350,$row['idproject']);
			
			$rSet = array('idproject' => $row['idproject'], 'Name' => $row['Name'], 'StartDate' => $row['StartDate'],
					'Duration' => $row['Duration'],	'Status' => $row['Status'], 'Developer' => $row['username'], 
					'Invited' => $invited, 'Rejected' => $rejected, 'Bids' => $bids, 'Accepted' => $accepted, 
					'Submitted' => $submitted, 'Escrowed' => $escrowed, 'Cancelled' => $cancelled, 
					'Failed' => $failed, 'RejectReport' => $rejectReport, 'InProgress' => $inprogress, 
					'Awaiting' => $awaiting);
			array_push($resultArray,$rSet);
			
		}
		
		return $resultArray;
		
		
	}
	
	public function simpleListByStatus($status, $pageNumber, $resultsPerPage)
	{
		$whereClause=1;
		if($status<>-1)
		{
			$whereClause='projects.status ='.$status;
		}
		$rows=$this->listProjects($whereClause,'StartDate desc',  $pageNumber, $resultsPerPage, 0);
		$test = new Tests();
		$resultArray = array();
		foreach($rows as $row)
		{
			
			$rSet = array('idproject' => $row['idproject'], 'Name' => $row['Name'], 'StartDate' => $row['StartDate'],
					'Duration' => $row['Duration'],	'Status' => $row['Status'], 'Developer' => $row['username']);
			array_push($resultArray,$rSet);
			
		}
		
		return $resultArray;
	}
	
	/*
	 * This function must be called by an admin so please verify this condition in the view before calling this 
	 * function.  What is will do is increase the maximum number of testers in a project.  There is very little
	 * verification in this method as long as the project actually exists.  As this is an admin function it will
	 * probably be called in concert with other such functions in this model
	 */
	public function increaseMaxTesters($idproject, $increase)
	{
		$row=$this->getProject($idproject);
		$maxTesters = (int)$row['maxtests']+$increase;
		
		$projectData = array('maxtests'=>"$maxTesters");
		$this->updateProject($idproject, $projectData);
		return $maxTesters;
	
	}
	
	public function getEscrowFundsForProject($idproject)
	{
		$tests = new Tests();
		return $tests->getEscrowFundsForProject($idproject);
	}
	
	public function getDeveloperProjectUpdateNotices($idproject,$timestamp=0)
	{
		$translate = Zend_Registry::get('Zend_Translate');

		$trans = new Tests();
		$wall = new Wall();
		$newBids=$trans->countTestsByStatusByProject(100,$idproject,$timestamp);
		$testSubmitted=$trans->countTestsByStatusByProject(300,$idproject,$timestamp);
		$newTestWall=$wall->countTestPostsByProject($idproject, $timestamp, Zend_Registry::get('defSession')->currentUser->id);
		$newProjectWall=$wall->countPosts($idproject, 1, $timestamp, Zend_Registry::get('defSession')->currentUser->id);
		$second=0;
		$message="";
		if($newBids>0)
		{
			$message="$newBids new bids";
			$second=1;
		}
		if($testSubmitted>0)
		{
			if($second==1)
			{
				$message=$message.", ";
			}
			else
			{
				$second=1;
			}
			$message=$message."$testSubmitted new test cases submitted";
		}
		if($newTestWall>0)
		{
			if($second==1)
			{
				$message=$message.", ";
			}
			else
			{
				$second=1;
			}
			$message=$message."$newTestWall private test messages";
		}
		if($newProjectWall>0)
		{
			if($second==1)
			{
				$message=$message.", ";
			}
			else
			{
				$second=1;
			}
			$message=$message."$newProjectWall project wall messages";
		}
		if($second==0)
		{
			$message=$translate->_('models_projects_noChange');	
		}
		$returnArray = array('newBids'=>$newBids, 'testSubmitted'=>$testSubmitted, 'newTestWall'=>$newTestWall, 'newProjectWall'=>$newProjectWall,'message'=>$message,'newCheck'=>$second);
		return $returnArray;
	}
	
	public function getTesterProjectUpdateNotices($idproject,$idtester, $timestamp=0)
	{
		
		$wall = new Wall();
		
		$newTestWall=$wall->countTestPostsByProjectAndTester($idproject, $idtester, $timestamp);
		
		$newProjectWall=$wall->countPosts($idproject, 1, $timestamp, $idtester);
		
		
		$second=0;
		$message="";
		if($newTestWall>0)
		{
			if($second==1)
			{
				$message=$message.", ";
			}
			else
			{
				$second=1;
			}
			$message=$message."$newTestWall private test messages";
		}
		if($newProjectWall>0)
		{
			if($second==1)
			{
				$message=$message.", ";
			}
			else
			{
				$second=1;
			}
			$message=$message."$newProjectWall project wall messages";
		}
		if($second==0)
		{
			$message="No Change since last login";	
		}
		$returnArray = array('newTestWall'=>$newTestWall, 'newProjectWall'=>$newProjectWall,'message'=>$message,'newCheck'=>$second);
		return $returnArray;
	}
	
	
	
}