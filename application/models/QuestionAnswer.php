<?php
class QuestionAnswer extends Zend_Db_Table_Abstract  
{
	protected $_name = 'question_answer';
  
	protected $_dependentTables = array('Question', 'Answer');
	
	public function getAnswersByQuestionID($idquestion)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this)
			->join(array('answer'=>'answer'),'question_answer.idanswer=answer.idanswer',array('text','value','answergroup'))
			->where("question_answer.idquestion = ?",$idquestion)
			->order("question_answer.order DESC")
			->order("question_answer.idanswer");
		$rows= $this->fetchAll($select);
		return $rows;
	}
}