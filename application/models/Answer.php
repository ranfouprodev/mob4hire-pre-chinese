<?php
class Answer extends Zend_Db_Table_Abstract  
{
    protected $_name = 'answer';

    protected  $_referenceMap=array(
								'QuestionAnswer'=>array(
									'columns'=>array('idanswer'),
									'refTableClass' => 'QuestionAnswer',
									'refColumns' => array('idanswer')
									),
    
								'Response'=>array(
									'columns'=>array('idanswer'),
									'refTableClass' => 'Response',
									'refColumns' => array('idanswer')
									),
								'Branch'=>array(
									'columns'=>array('idanswer'),
									'refTableClass' => 'Branch',
									'refColumns' => array('idanswer')
									)
																		
								); 
	
	/*
	 * This will add a new answer to the database.  It has two inputs the text that the user will see
	 * prior to answering the quesion the value is used for reporting side to allow statistical 
	 * analysis of the answers.The AnswerGroup is used for linking a certain set of answers together for 
	 * ease of retrevial.
	 */						
	public function addNewAnswer($text, $value='', $answerGroup='') 
	{
		$data = array('text'=>$text, 'value'=>$value, 'answergroup'=>$answerGroup);
		$this->insert($data);
	}
	
	/*
	 * This will get a rowset from the database of all distinct defined answerGroup's in the database.  
	 * It will be a simple SELECT DISTINCT(group) FROM answer WHERE NOT ISNULL(group)
	 */
	public function listAnswerGroups()
	{
		$select=$this->select();
		$select->distinct()
			->from($this, array('answergroup'))
			->where("NOT ISNULL(answergroup)");
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	/*
	 * It will return an array of integers with the integer value for the idanswer for each answer in the
	 * group.  So the return value is $answerArray = array(4,6,81,72)
	 */
	public function getAnswersByGroup($answerGroupName)
	{
		$select=$this->select();
		$select->from($this)
			->where("answergroup = ?",$answerGroupName);
		$rows= $this->fetchAll($select);
		return $rows;
	}
}