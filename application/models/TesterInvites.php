<?php
require_once 'Lib/Cache/CacheFactory.php';

class TesterInvites extends Zend_Db_Table_Abstract  
{
	protected $_name = 'tester_invites';
	
	
	public function count($idproject)
	{
		$countValue = 0;
			$select=$this->select();
			$select->from($this, array ('COUNT(idproject) as count'))
					->where("idproject=? and status=1",$idproject);
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			

		return $countValue;
	}
	
	
	
	public function getInvites($idproject)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('d'=>'users'), 'd.id=tester_invites.idtester',  array("d.username"))
			->where("idproject=? and status=1",$idproject);
 		$rows= $this->fetchAll($select);
		return $rows;		
	}
	
	public function checkInvite($idproject, $idtester)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->from($this, array('COUNT(*) AS total'))
			->where("status = 1")
			->where("idproject = ?",$idproject)
			->where("idtester = ?",$idtester);
 		$rows= $this->fetchAll($select);
		$row=$rows->current();
		if($row['total']>0)
			return TRUE;
		else
			return FALSE;	
	}
	
		
	public function addInvite($idproject,$iduser )
	{
		// check for duplicates 
		
		$row = $this->fetchRow('idproject = ' . $idproject . ' and idtester = '.$iduser.' and status =1');
		
		if (!$row) {
			$invite=array(
						'idproject'=>$idproject,
						'idtester'=>$iduser,
						'status'=>1,
					);
			$id = $this->insert($invite);
		}
	}
	
	
	
	public function addInvitesByName($idproject, $usernames)
	{
		$users=new Users();
		$db=$users->getAdapter();
		foreach($usernames as $username)
		{
			$where=$db->quoteInto('username= ?', $username);
			$row=$users->fetchRow($where);
			if ($row)
			{
				$this->addInvite($idproject,$row->id);
			}
		}
		
	}
 /*
  * This is like getInvites above but links to more tables to return more shit about the users for display
  */
	public function getInvitedTesters($idproject)
	{
		fb('idproject:'.$idproject);
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->from($this)
			->group('u.id')
			->order('rating desc')
			->joinLeft('regdevices', 'regdevices.idtester=tester_invites.idtester',array())
			->joinLeft(array('p'=>'profile_contactinfo'), 'p.id=regdevices.idtester',array('p.time_zone', 'p.city AS city', 'p.company AS company'))
			->joinLeft(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array('n.network'))
			->joinLeft(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array('c.country'))
			->joinLeft(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array('d.vendor', 'd.model'))
			->join(array('u'=>'users'), 'u.id=tester_invites.idtester',  array("u.id as userid", "u.username"))
			->joinLeft(array('r'=>'ratings'),'r.id=u.username', array("(r.total_value/r.total_votes) as rating"))
			->where("idproject=? and tester_invites.status=1",$idproject);
 		fb(print_r($select->__toString(), true));
		$rows= $this->fetchAll($select);
		return $rows;		
	
	}
}