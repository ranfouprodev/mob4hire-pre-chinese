<?php

class ProjectFiles extends Zend_Db_Table_Abstract
{
	public static $cattypes=array(
						"uploadPlan"=>array("Test Plans", "These are the directions to complete the test"),
						"testOutput"=>array("Instructions for Reporting","This is how the results should be displayed"),
						"genFiles"=>array("Application Files","These files are to be installed on your handset"),
						"ndaFile"=>array("NDA Files","This NDAs are required by the developer before work can be started"),
						"screenShots"=>array("Screenshots","&nbsp;"),
						"documentation"=>array("Additional Support Documentaion","These files will help with your testing"),
					);

	protected $_name = 'project_files';
	protected $_primary = 'idfiles';

	public static $permissions=array(0=>"Public", 1=>"Private");
	
	public function getFile($id)
	{
		
		$cache = CacheFactory::getCache('projectfiles');
		
		$cacheKey = 'getFile_' . $id;
		$timer = Timer::start();
		
		if ( !$file = $cache->get($cacheKey ) ) {
		
			$id=(int)$id; // Make sure id is an integer
			$row = $this->fetchRow('idfiles = ' . $id);
			
			if (!$row) {
				throw new Exception("Could not find row $id");
			}
			
			$file = $row->toArray(); 
			$cache->setE($cacheKey, $file, Timer::end($timer) );
		}
		
		
		return $file;
	}
	
	public function getAllFilesStoredInDb(){
		$whereClause = "idproject <> 0 AND OCTET_LENGTH(filedata)>0";
		$sort = 'idfiles ASC';
		$limit = 100;
		$row = $this->fetchAll($whereClause,$sort, $limit,0); 
		return $row->toArray(); 
	}
	
	
	public function getAllFilesByType($fcn){
		$whereClause = "category ='".$fcn."'";
		$sort = 'idfiles ASC';
		$row = $this->fetchAll($whereClause);
		return $row->toArray();
	}
	
	
    public function getAllFilesForProject($idproject)
    {
	        $idproject = (int)$idproject; // Make sure id is an integer
	        $whereClause = "idproject = ".$idproject." and status = 1";
		$row = $this->fetchAll($whereClause);

	        return $row->toArray();
    }

   public function getFilesForProjectByCategory($idproject,$category)
    {
	        $idproject = (int)$idproject; // Make sure id is an integer
	        $whereClause = "idproject = ".$idproject." and category='".$category."' and status = 1";
		$row = $this->fetchAll($whereClause);

	        return $row->toArray();
    }


    /*
     *  Deprecated with new db file storage
     */
	/* public function addFileToProject($idproject, $filename, $physicalFilename,$mimeType, $category, $permissions) {


        $data = array (
        'idproject'=>$idproject,
        'filename'=>$filename,
        'physical_filename'=>$physicalFilename,
        'mime_type'=>$mimeType,
        'category'=>$category,
        'permissions'=>$permissions,
        'status'=>'1');
        try {
            $idfile = $this->insert($data);
        }
        catch(Exception $e)
        {
            throw new Exception("Error inserting file :".$e);
        }
		
		return $idfile;
    }*/
	

	 public function addFileToProject($idproject, $filename,$mimeType, $filesize, $filedata, $category, $permissions) {


        $data = array (
		'idproject'=>$idproject,
		'filename'=>$filename,
		'filedata'=>$filedata,
		'filesize'=>$filesize,
		'mime_type'=>$mimeType,
		'category'=>$category,
		'permissions'=>$permissions,
		'status'=>'1');
        try {
            $idfile = $this->insert($data);
        }
        catch(Exception $e)
        {
            throw new Exception("Error inserting file :".$e);
        }
		
		return $idfile;
    }
	
	

	
	

    public function removeFileFromProject( $idfile)
    {
	
		$id=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idfiles = ' . $idfile);
		
		if($row){
			$row->status="0";
			$row->save();	
		}

    }

    public function changePermission( $idfile, $permission )
    {
	
		$id=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idfiles = ' . $idfile);
		
		if($row){
			$row->permissions=$permission;
			$row->save();	
		}

    }


	public function updateProjectFile($idproject, $projectData)
	{
		$projectRowset = $this->find($idproject);
		$project = $projectRowset->current();

		foreach ($projectData as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of user cannot be changed');
				}
				else
				{
					$project->{$name}=$value;						
				}
			}
		}
		$project->save();
		return $this; // to allow chaining of methods
	}

	public function getBinaryByDevice($idproject, $iddevice)
	{
		$select=$this->select();
		$whereClause = "idproject = ".$idproject." and category like 'dev%".$iddevice."' and status = 1";
		$row = $this->fetchAll($whereClause);
		if(!$row->count()) {
			// check for binaries by platform
			$platforms=array('osSymbian','osWindows', 'osRim','osOsx','osAndroid');
			$deviceatlas= new Devices();
			foreach ($platforms as $platform)
			{
				if ($deviceatlas->checkPlatform($iddevice, $platform)) {
					$select=$this->select();
					$whereClause = "idproject = ".$idproject." and category like 'dev".$platform."%' and status = 1";
					$row = $this->fetchAll($whereClause);
					if($row->count()) {
						return $row->toArray();
					}
				}
			}
			return $this->getFilesForProjectByCategory($idproject,'genFiles'); // If no specific files exists return generic files (maybe we should first check for files by platform , make todo:
		}
			
	        return $row->toArray();
	}
	
	//
	// convert image to 100px x 100px
	// used on already existing blobs
	// needs adapting to work on files just uploaded
	//
	public function resizeImage($idfile, $width=100, $height=100)
	{
		$idfile=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idfiles = ' . $idfile);
		if($row){
			
			$im=imagecreatefromstring($row->filedata);
			$new = imagecreatetruecolor($width, $height);
			$x = imagesx($im);
			fb($x);
			$y = imagesy($im);
			fb($y);
			imagecopyresampled($new, $im, 0, 0, 0, 0, $width, $height, $x, $y);
			
			//prepare image for database
			ob_start();
			imagejpeg($new, '', 80);
			$imgdata = ob_get_contents();
			ob_end_clean();
			fb(strlen($imgdata));
			$row->filedata=$imgdata;
			$row->filesize=strlen($imgdata);
			$row->mime_type='image/jpeg';
			$row->save();
			return $new;
		}
	}
}
