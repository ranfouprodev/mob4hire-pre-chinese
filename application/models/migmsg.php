<?php

class migmsg extends Zend_Db_Table_Abstract
{
	protected $_name = 'messages';
	
	//get all the records where the subject != passed in subject string

	public function getMessages($subject)
	{
		$select=$this->select();
		$select->from($this);
		$select->where('subject NOT LIKE ?',$subject);
			
		$rows= $this->fetchAll($select);
		return $rows;
		
		if (!$rows) {
			throw new Exception("No records match for subject '" + $subject);
		}
		
		return $rows;
	}
}
?>