<?php

class Filter extends Zend_Db_Table_Abstract  
{
	protected $_name = 'users';
	
	public function filterUsers($filterList)
	{
		//The filter array needs to be set with the following set of arrays
		//type => TYPE and value => value
		$countryArray = array();
		$vendorArray = array();
		$modelArray = array();
		$networkArray = array();
		$osArray = array();

		//foreach($filterList as $filter)
		foreach ($filterList as $i => $filter)
		{
			if($filter['type']=='country')
			{
				$tempArray=array("value" => $filter['value']);
				array_push($countryArray, $tempArray);
			}
			if($filter['type']=='network')
			{
				$tempArray=array("value" => $filter['value']);
				array_push($networkArray, $tempArray);
			}
			if($filter['type']=='vendor')
			{
				$tempArray=array("value" => $filter['value']);
				array_push($vendorArray, $tempArray);
			}
			if($filter['type']=='model')
			{
				$tempArray=array("value" => $filter['value']);
				array_push($modelArray, $tempArray);
			}
			if($filter['type']=='os')
			{
				$tempArray=array("value" => $filter['value']);
				array_push($osArray, $tempArray);
			}
		}
		//COUNTRY WHERE CLAUSE
		if(count($countryArray)==0)
		{
			$countryClause='(1';
		}
		else
		{
			$firstqueryset=0;
			$countryClause='(';
			foreach($countryArray as $item)
			{
				if($firstqueryset)
					$countryClause.=' OR ';
				else
					$firstqueryset=1;
				$countryClause.='(networks.idcountry ='.$item['value'].')';
			}
		}
		$countryClause.=')';
		//NETWORKS WHERE CLAUSE
		if(count($networkArray)==0)
		{
			$networkClause='(1';
		}
		else
		{
			$firstqueryset=0;
			$networkClause='(';
			foreach($networkArray as $item)
			{
				if($firstqueryset)
					$networkClause.=' OR ';
				else
					$firstqueryset=1;
				$networkClause.='(regdevices.idnetwork ='.$item['value'].')';
			}
		}
		$networkClause.=')';
		//VENDOR WHERE CLAUSE
		if(count($vendorArray)==0)
		{
			$vendorClause='(1';
		}
		else
		{
			$firstqueryset=0;
			$vendorClause="(";
			foreach($vendorArray as $item)
			{
				if($firstqueryset)
					$vendorClause.=" OR ";
				else
					$firstqueryset=1;
				$vendorClause.="(deviceatlas.vendor ='".$item['value']."')";
			}
		}
		$vendorClause.=")";
		//MODEL WHERE CLAUSE
		if(count($modelArray)==0)
		{
			$modelClause='(1';
		}
		else
		{
			$firstqueryset=0;
			$modelClause="(";
			foreach($modelArray as $item)
			{
				if($firstqueryset)
					$modelClause.=" OR ";
				else
					$firstqueryset=1;
				$modelClause.="(deviceatlas.model ='".$item['value']."')";
			}
		}
		$modelClause.=")";
		//OS WHERE CLAUSE
		//osSymbian, osLinux, osWindows, osRim, osOsx, osAndroid
		if(count($osArray)==0)
		{
			$osClause='(1';
		}
		else
		{
			$firstqueryset=0;
			$osClause="(";
			foreach($osArray as $item)
			{
				if($firstqueryset)
					$osClause.=" OR ";
				else
					$firstqueryset=1;
				$osClause.="(deviceatlas.os".$item['value']."=1)";
			}
		}
		$osClause.=")";
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("users" => 'users');
		$columns = array("id" => 'DISTINCT(users.id)', "username" => 'users.username');
//		SELECT DISTINCT(regdevices.idtester) AS userid, users.username AS username, countries.country AS country
//		FROM regdevices
//		JOIN users ON users.id=regdevices.idtester 
//		JOIN networks ON regdevices.idnetwork=networks.idnetwork
//		JOIN countries ON networks.idcountry=countries.idcountry
//		LEFT JOIN deviceatlas ON regdevices.deviceatlasid=deviceatlas.id
//		WHERE networks.idcountry=192
//		GROUP BY regdevices.idtester;
//		
//		$statement = $select->from($this, array("id" => 'DISTINCT(users.id)', "username" => 'users.username'))
//			->join(array('regdevices'=>'regdevices'), 'users.id=regdevices.idtester',  array())
//			->join(array('networks'=>'networks'), 'regdevices.idnetwork=networks.idnetwork',  array("network" => 'networks.network'))
//			->joinLeft(array('countries'=>'countries'), 'networks.idcountry=countries.idcountry',  array( "country" => 'countries.country'))
//			->joinLeft(array('deviceatlas'=>'deviceatlas'), 'regdevices.deviceatlasid=deviceatlas.id', array("vendor" => 'deviceatlas.vendor', "model" => 'deviceatlas.model'))	
//			->group('users.id')
//			->where($whereClause);
//		echo $select->__toString();	
//		
//		$rows= $this->fetchAll($select);
//		return $rows;	

		$whereClause=$countryClause.' AND '.$networkClause.' AND '.$vendorClause.' AND '.$modelClause.' AND '.$osClause;
		return $whereClause;
	}
	/* Returns a rowset of users matching the filter criteria
	 *
	 * should be called with an array of conditions. each item in the array can itself be an array, in which case an OR subclause will be added
	 * to the where clause
	*/
	public function filterUsers2($filterlist)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false); // not sure why this is needed still. Seems to work
			
		$select	->from($this, array('distinct(users.id)', 'users.username', 'users.email'))
				->joinLeft(array('regdevices'), 'regdevices.idtester=users.id', array())
				->joinLeft(array('networks'), 'networks.idnetwork=regdevices.idnetwork',  array("networks.network","networks.idnetwork"))
				->joinLeft(array('countries'), 'countries.idcountry=networks.idcountry',  array("countries.country", "countries.idcountry"))
				->joinLeft(array('deviceatlas'), 'deviceatlas.id=regdevices.deviceatlasid',  array("deviceatlas.model","deviceatlas.vendor", "deviceatlas.id", "deviceatlas.osAndroid", "deviceatlas.osSymbian", "deviceatlas.osRim", "deviceatlas.osWindows","deviceatlas.osOsx", "deviceatlas.osLinux"));
	
		foreach($filterlist as $index=>$value)
		{
			if (is_array($value))
			{
				// There are multiple possible values for this index
				$firstqueryset=false;
				$whereClause='';
				foreach ($value as $value2) 
				{
					if (!$firstqueryset)
					{
						if($index=='deviceatlas.os')
						{
							$whereClause= "(".$index.$value2."=1";
						}
						else
						{
							$whereClause= "($index='$value2'";
						}
						$firstqueryset=true;
					}
					else
					{
						if($index=='deviceatlas.os')
						{
							$whereClause .= " OR ".$index.$value2."=1";
						}
						else
						{
							$whereClause.=" OR $index='$value2'";
						}
					}
				echo $whereClause;
				}
				$whereClause.=')';
				$select->where($whereClause);
			}
			else
			{
				$select->where("$index='$value'");
			}
		}
		echo $select->__toString();
		$rows= $this->fetchAll($select);
		return $rows;
	}	

	public function getNetworksByCountry($countryArray)
	{
		//The country Array is just a list of simple country id's
		//Example would be array(1,2,5,192)
		$resultArray = array();
		$networks = new Networks();
		if(count($countryArray)==0)
		{
			return $resultArray;
		}
		else
		{
			foreach ($countryArray as &$value) 
			{
				$rows=$networks->listNetworksByCountry($value);
				foreach($rows as $row)
				{
					$tempArray = array("id"=>$row['id'], "name" => $row['name'], "country"=>$row['country']);
					array_push($resultArray, $tempArray);
				}
    			
			}

		}
		return $resultArray;
	}
	
	public function osWhereClause($arrayValues)
	{
		
	}
    	
}