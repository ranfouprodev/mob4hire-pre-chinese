<?php
class Token extends Zend_Db_Table_Abstract  
{
	protected $_name = 'token';
    								
	protected $_dependentTables = array('Survey');
	
	public function getToken($idtoken)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idtoken','idsurvey', 'token','value'))
			->where("idtoken = ?",$idtoken);
		$row=$this->fetchRow($select);
		return $row;
	}

	public function updateToken($idtoken, $data)
	{
		$rowset = $this->find($idtoken);
		$row = $rowset->current();
		
		foreach ($data as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of role cannot be changed');
				}
				else
				{
					$row->{$name}=$value;						
				}
			}
		}
		$row->save();
		return $this;
	}
	
	public function getTokenBySurvey($idsurvey)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idtoken','idsurvey', 'token','value'))
			->where("idsurvey = ?",$idsurvey);
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function countTokenBySurvey($idsurvey,$tokenInfo)
	{
		$select=$this->select();
		$columns = array("count" =>'COUNT(idtoken)');
		$statement = $select->from($this, $columns)
			->where("idsurvey = ?",$idsurvey)
			->where("token = ?",$tokenInfo);	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function updateTokenByTokenAndSurvey($idsurvey,$tokenInfo, $value)
	{
		if($this->countTokenBySurvey($idsurvey, $tokenInfo)>0)
		{
			//There is this info already in the system.
			$idtoken=$this->getTokenID($idsurvey, $tokenInfo);
			$data=array('value'=>$value);
			$this->updateToken($idtoken, $data);
		}
		else
		{
			//We need to insert this token into the system
			$data=array('idsurvey'=>$idsurvey,'token'=>$tokenInfo,'value'=>$value);
			$this->insert($data);
		}
		
	}
	
	public function getTokenID($idsurvey,$tokenInfo)
	{
		$select=$this->select();
		$statement = $select->from($this, array('idtoken'))
			->where("idsurvey = ?",$idsurvey)
			->where("token = ?",$tokenInfo);	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['idtoken'];
	}
}