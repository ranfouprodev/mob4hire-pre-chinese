<?php

class Messages extends Zend_Db_Table_Abstract  
{
	protected $_name = 'messages';

	public function init()
	{
		$this->translate = Zend_Registry::get('Zend_Translate');
	}

	public function listArchivedMessagesByUser($id)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('u'=>'users'), 'u.id=messages.sender',  array("u.username"))
			->where("status=0 and owner=?",$id)
			->order(array("posted desc"));
 		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function archive( $id)
	{
	
		$id=(int)$id; 
		$row = $this->fetchRow('id = ' . $id);
		
		if($row){
			$row->status="0";
			$row->save();	
		}

	}

	public function setViewed( $id)
	{
	
		$id=(int)$id; 
		$row = $this->fetchRow('id = ' . $id);
		
		if($row){
			$row->showed="1";
			$row->save();	
		}

	}
	
	
	public function getMessage($id)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('u'=>'users'), 'u.id=messages.sender',  array("u.username"))
			->where("messages.id=?",$id)
			->order(array("posted desc"));
 		$rows= $this->fetchRow($select);
		return $rows;
	}
	
	
	public function listMessagesByUser($id)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('u'=>'users'), 'u.id=messages.sender',  array("u.username"))
			->where("status=1 and owner=?",$id)
			->order(array("posted desc"));
 		$rows= $this->fetchAll($select);
		return $rows;
	}

	public function listMessagesBySender($id)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->join(array('u'=>'users'), 'u.id=messages.owner',  array("u.username"))
			->where("status=1 and sender=?",$id)
			->order(array("posted desc"));
 		$rows= $this->fetchAll($select);
		return $rows;
	}

	public function sendMessage($owner, $sender,  $message, $subject, $project='')
	{
		$msg=array(
					'owner'=>$owner,
					'sender'=>$sender,
					'showed'=>0,
					'subject'=>$subject,
					'message'=>$message,
					'posted'=>time(),
					'status'=>1,
				);
		$id = $this->insert($msg);
		$wall = new Wall();
		$idtarget=$owner;
		$idposter=$sender;
		$wall->postOnWall($idposter, $message, $idtarget, 3);
		return $id;
	}
/*
*	The following method id sonly called by the Inbox module
*/
	public function sendPrivateMessage($owner, $sender,  $message, $subject, $project='')
	{
		$msg=array(
					'owner'=>$owner,
					'sender'=>$sender,
					'showed'=>0,
					'subject'=>$subject,
					'message'=>$message,
					'posted'=>time(),
					'status'=>1,
				);
		$id = $this->insert($msg);
		$wall = new Wall();
		$idtarget=$owner;
		$idposter=$sender;
		$wall->postOnWall($idposter, $message, $idtarget, 3);
		$this->sendNewEmailNotification($owner);
	}

	public function sendMessageUsers($usernames, $sender, $message, $subject, $project='')
	{
		$users=new Users();
		$db=$users->getAdapter();
		foreach($usernames as $username)
		{
			$where=$db->quoteInto('username= ?', $username);
			$row=$users->fetchRow($where);
			if ($row)
			{
				$this->sendPrivateMessage($row->id, $sender, $message, $subject, $project);
			}
		}
	}
	
	
	
	protected function sendNewEmailNotification($userid)
	{
		$message = "<a href='/app/inbox/'>'.$this->translate->_('notification_new_message').'</a>";
		$post = true;
		
		$notifications = new Notifications(); 
		
		$notificationList = $notifications->getNotificationsForUser($userid);
		if(isset($notificationList)){
			foreach($notificationList as $n){
				if($n['message'] == $message)
					$post = false;
			}
		}

		if($post)
			$notifications->addNotification($userid,$message);
			
		$mailer = new Mailer();
		$mailer->sendMessageNotification($userid);

	}
	
	public function getMessagesBetweenUsers($username1, $username2)
	{
		$select=$this->select();
 		$select->setIntegrityCheck(false);
 		$user = new Users();
 		$userid1=$user->getUserID($username1);
 		$userid2=$user->getUserID($username2);
 		$whereClause1 = '(messages.sender ='.$userid1.' AND messages.owner ='.$userid2.')';
 		$whereClause2 = '(messages.sender ='.$userid2.' AND messages.owner ='.$userid1.')';
		$select->distinct()
			->from($this)
			->where($whereClause1)
			->orWhere($whereClause2)
			->order(array("posted desc"));
 		$rows= $this->fetchAll($select);
		return $rows;
	}
}