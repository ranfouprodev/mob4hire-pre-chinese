<?php

class TestResults extends Zend_Db_Table_Abstract  
{
	protected $_name = 'testresults';
    
	public function getTestResult($idtest)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this)
				->joinInner(array('t'=>'tests'), 't.idtest=testresults.idtest', array('idproject' ))
				->where("testresults.idtest=?",$idtest);
//		print_r($select->__toString());
		$rows= $this->fetchRow($select);
		return $rows;
	}
	
}