<?php
class SurveyAnalysis extends Zend_Db_Table_Abstract  
{
	protected $_name = 'response';

	public function getSurveyQuestionAverage($idsurvey,$idquestion)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$statement = $select->from($this)
			->join(array('answer'=>'answer'),'response.idanswer=answer.idanswer',array("average" =>'AVG(answer.value)'))
			->where("response.idsurvey = ?",$idsurvey)
			->where("response.idquestion = ?",$idquestion)
			->where("NOT ISNULL(answer.value)")
			->group("idsurvey")
			->group("idquestion");
		
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['average'];
	}
	
	/*
	 * This will get the Retention Loyalty Index from a MobExperience survey.
	 * The user will input the idsurvey they want to get this information from and
	 * it will return the RLI for it.  RLI is linked to mob experience survey
	 * questions numbers 11 & 12.  However this number is reverse calculated.
	 */
	public function getSurveyRLI($idsurvey)
	{
		$temp = $this->getSurveyQuestionAverage($idsurvey, 11)+$this->getSurveyQuestionAverage($idsurvey, 12);
		$temp = 20-$temp;
		return $temp/2;
	}
	
	/*
	 * This will get the Advocacy Loyalty Index from a MobExperience survey.
	 * The user will input the idsurvey they want to get this information from and
	 * it will return the ALI for it.  ALI is linked to mob experience survey
	 * questions numbers 4, 7, & 8.
	 */
	public function getSurveyALI($idsurvey)
	{
		$temp =  $this->getSurveyQuestionAverage($idsurvey, 4)+$this->getSurveyQuestionAverage($idsurvey, 7)+$this->getSurveyQuestionAverage($idsurvey, 8);
		return $temp/3;
	}
	
	/*
	 * This will get the Purchasing Loyalty Index from a MobExperience survey.
	 * The user will input the idsurvey they want to get this information from and
	 * it will return the PLI for it.  PLI is linked to mob experience survey
	 * questions numbers 9 & 10.
	 */
	public function getSurveyPLI($idsurvey)
	{
		$temp = $this->getSurveyQuestionAverage($idsurvey, 9)+$this->getSurveyQuestionAverage($idsurvey, 10);
		return $temp/2;
	}
	
	public function getSurveyCSVArray($idsurvey)
	{
		$returnArray = array();
		$questionArray=$this->getSurveyQuestionArray($idsurvey);
		$returnArray[0]=$questionArray;
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array('DISTINCT(iduser) AS iduser');
		$statement = $select->from($this,$columns)
			->where("idsurvey = ?",$idsurvey);
		$rows=$this->fetchAll($select);
		
		foreach($rows as $row)
		{
			$tempArray = array();
			$iduser=$row['iduser'];
			$tempArray=$this->getSurveyOutputByUserAndSurvey($iduser, $idsurvey);
			$returnArray[]=$tempArray;
		}
		$tempArray=array();
		foreach($questionArray as $question)
		{
			if($question!="IDUSER")
			{
				$tempArray[$question]=$this->getSurveyQuestionAverage($idsurvey,$question);
			}
			else
			{
				$tempArray[0]="AVERAGES";
			}
		}
		$returnArray['Averages']=$tempArray;
		
		return $returnArray;
	}
	
	public function getSurveyOutputByUserAndSurvey($iduser, $idsurvey)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns=array('idquestion','text');
		$statement = $select->from($this,$columns)
			->join(array('answer'=>'answer'),'response.idanswer=answer.idanswer',array('value'))
			->where("idsurvey = ?",$idsurvey)
			->where("iduser = ?",$iduser)
			->order("idquestion");
		$rows=$this->fetchAll($select);
		$arr = array();
		$arr[0]=$iduser;
		foreach($rows as $row)
		{
			if($row['value']!=NULL)
				$arr[$row['idquestion']]=$row['value'];
			elseif($row['text']!=NULL)
				$arr[$row['idquestion']]=$row['text'];
			else
				$arr[$row['idquestion']]="----";
		}
		return $arr;
	}
	
	public function getSurveyQuestionArray($idsurvey)
	{
		$select=$this->select();
		$columns=array('DISTINCT(idquestion) AS idquestion');
		$statement = $select->from($this,$columns)
			->where("idsurvey = ?",$idsurvey)
			->order("idquestion");
		$rows=$this->fetchAll($select);
		$arr = array();
		$arr[0]="IDUSER";
		foreach($rows as $row)
		{
			$arr[$row['idquestion']]=$row['idquestion'];
		}
		return $arr;
	}
	
	public function getCountAboveValue($idsurvey,$idquestion, $value)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$statement = $select->from($this)
			->join(array('answer'=>'answer'),'response.idanswer=answer.idanswer',array("sum" =>'COUNT(answer.value)'))
			->where("response.idsurvey = ?",$idsurvey)
			->where("response.idquestion = ?",$idquestion)
			->where("NOT ISNULL(answer.value)")
			->where("answer.value >= ?",$value)
			->group("idsurvey")
			->group("idquestion");
		
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
	
	public function getPositiveResponse($idsurvey, $idquestion)
	{
		$total=$this->getCountAboveValue($idsurvey, $idquestion, 0);
		if($total==0)
		{
			$total=1;
		}
		$count=$this->getCountAboveValue($idsurvey, $idquestion, 6);
		return $count/$total;
	}
	
	public function getInversePositiveResponse($idsurvey, $idquestion)
	{
		$total=$this->getCountAboveValue($idsurvey, $idquestion, 0);
		if($total==0)
		{
			$total=1;
		}
		$count=$this->getCountAboveValue($idsurvey, $idquestion, 5);
		$temp= $count/$total;
		$temp=1-$temp;
		return $temp;
	}

}