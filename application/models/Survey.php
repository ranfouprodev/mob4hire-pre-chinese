<?php
class Survey extends Zend_Db_Table_Abstract  
{
    protected $_name = 'survey';
    
    protected  $_referenceMap=array(
								'SurveyQuestion'=>array(
									'columns'=>array('idsurvey'),
									'refTableClass' => 'SurveyQuestion',
									'refColumns' => array('idsurvey')
									),
								'Token'=>array(
									'columns'=>array('idsurvey'),
									'refTableClass' => 'Token',
									'refColumns' => array('idsurvey')
									),
								'Response'=>array(
									'columns'=>array('idsurvey'),
									'refTableClass' => 'Response',
									'refColumns' => array('idsurvey')
									)							
								);
								
	protected $_dependentTables = array('Projects');


	protected $idsurvey;
	protected $iduser;
	protected $title;
	protected $tokens;
	protected $count;
	protected $total;
	
	/*
	/* This will create a survey object from an existing survey. Maybe we would use this same constructor to create a new survey
	*/
	public function __construct($idsurvey=null, $iduser=null, $options = null)
	{
		
		parent::__construct($options);

		if($idsurvey)
		{
			$this->idsurvey=$idsurvey;	
			$this->iduser=$iduser;
			$survey=$this->find($this->idsurvey)->current();
			$this->title=$survey->title;
			$this->idproject=$survey->idproject;
			
			$token=new Token();
			$this->tokens=$token->fetchAll($token->select()
								->where('idsurvey = ?', $this->idsurvey)
							);
		}

	}

	public function getId()
	{
		return $this->idsurvey;	
	}
	
	public function getTitle()
	{
		return $this->title;
	}

	public function getCount()
	{
		return $this->count;
	}

	public function getTotal()
	{
		return $this->total;
	}
	
	public function updateSurvey($idsurvey, $title, $idproject)
	{
		$data = array('idproject'=>$idproject,'title'=>$title);

		$where = $this->getAdapter()->quoteInto('idsurvey = ?', $idsurvey);

		$this->update($data, $where);
		return $this;
	}
	
	/*
	/* This will simply return the next question each time it is called. Used for rendering the survey
	*/
	public function getQuestion()
	{
		// get this user's existing answers to this survey so we can find the correct next question
		$response=new Response();
		$surveyquestion=new SurveyQuestion();

		$lastrow=$response->fetchRow($response->select()
								->where('idsurvey= ? ', $this->idsurvey)
								->where('iduser= ?', $this->iduser)
								->order('idresponse desc')
							);
		// fetch all the questions for this survey
		$rows=$surveyquestion->fetchAll($surveyquestion->select()
										->where('idsurvey= ? ', $this->idsurvey)
										->order(array('idsurvey_question', 'order DESC'))
									);
		$this->total=$rows->count();
		
		$count=0;
		if($lastrow)
		{
			// this user has already begun the survey. load up the question they should be on
			while (($row=$rows->current()) && ($row->idquestion!=$lastrow->idquestion)) // There must be a smarter way to do this
			{
				$rows->next(); // iterate through all the survey questions until we find the one we are on
				$count++;
			}
			//rows now points to the last question the user answered

			$branch=new Branch();
			$branchrow=$branch->fetchRow($branch->select()
								->where('idsurvey_question= ?', $rows->current()->idsurvey_question)
							);

			if ($branchrow && ($lastrow->idanswer==$branchrow->idanswer)) {
				$rows=$surveyquestion->find($branchrow->branchto);
			}
			else {
				$rows->next(); //move on to the next question (remember to check for branches and end of survey)
			}
		}
		$this->count=$count;
		$row=$rows->current();
		
		if (!$row) return false;  // Survey completed. Tell the caller
		$question=$row->findDependentRowset('Question')->current();
		$question->text=$this->applyTemplateToString($this->tokens, $question->text);
		$return=$question->toArray();
		
		// get the possible answers to this stupid question
		$questionanswer=new QuestionAnswer();;
		$answers=$questionanswer->getAnswersByQuestionID($question->idquestion);
		$answerarray=array();
		foreach($answers as $id=>$answer)
		{
			$answerarray['ans'.$answer['idanswer']]=$answer['text']; // array keys with numerical values get screwed up
		}
		
		$return['answer']=$answerarray;
		
		return $return;
	}
	
	/*
	 * This function will simply create a new survey in the system.  It won't add questions to it
	 * rather it will just add the survey and return the new survey id it made
	 */
	public function createNewSurvey($title, $idproject=1)
	{
		if($idproject==1)
		{
			$data=array('title'=>$title);
		}
		else
		{
			$data=array('idproject'=>$idproject, 'title'=>$title);
		}
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}
	
	public function listSurveys()
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idsurvey', 'title'))
			->joinLeft(array('p'=>'projects'),'p.idproject=survey.idproject', array('p.Name AS Name'));
		
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function listSurveysWithLinkedProject()
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idsurvey', 'title'))
			->joinLeft(array('p'=>'projects'),'p.idproject=survey.idproject', array('p.Name AS Name'))
			->where('NOT ISNULL(survey.idproject)')
			->where('survey.idproject >1');
		
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	public function getSurvey($idsurvey)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idsurvey', 'title','idproject'))
			->where("idsurvey = ?",$idsurvey);
		$row=$this->fetchRow($select);
		return $row;
	}
	
	public function getSurveyByProjectID($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this,array('idsurvey', 'title','idproject'))
			->where("idproject = ?",$idproject);
		
			
		//echo $select->__toString();		
		$rows= $this->fetchAll($select);
		
		$row = $rows->current();
		
		return $row;	
	}
	
	public function countSurveyByProject($idproject)
	{
		$select=$this->select();
		$columns = array("count" =>'COUNT(idsurvey)');
		$statement = $select->from($this, $columns)
			->where("idproject = ?",$idproject);	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}

	// Apply template given array of values to replace and template name

	protected function applyTemplateToString($vars, $templateTxt)
	{
		//replace tags (tags are field names in square brackets)
		foreach ($vars as $token) 
		{
			$templateTxt = str_replace('{'.$token->token.'}', $token->value, $templateTxt);
		}
	
		return $templateTxt;
	}
	
}