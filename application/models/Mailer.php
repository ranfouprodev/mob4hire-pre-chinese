<?php

require_once 'Zend/Mail.php';
require_once 'Zend/Mail/Transport/Smtp.php';
require_once 'Zend/Registry.php';
require_once 'facebook-platform/php/facebook.php';

class Mailer
{
	/**
	* Directory path, where mail templates are located
	*
	* @var string
	*/
	protected $translate = '';
	protected $templatesDir = '';
	protected $sitename = '';
	protected $url = '';
	protected static $styles=array(
								'<p>'=>'<p style="margin:0 0 1em 0;">',
								'<h2>'=>'<h2 style="color:#991c20; font-weight:bold; font-size: 1.2em; margin:0 0 1em 0;">'
							);
	/**
	* Constructor for the class, provide directory path where mail templates are saved
	*
	* @param string $templatesDir Directory path, where mail templates are located
	*/
	public function __construct($templatesDir = 'languages/mailtemplates')
	{
		$this->translate = Zend_Registry::get('Zend_Translate');

		$this->templatesDir = $templatesDir;
		$this->sitename = Zend_Registry::get('configuration')->general->sitename;
		$this->url = Zend_Registry::get('configuration')->general->url;
		$this->language = Zend_Registry::get('configuration')->general->language;

		$mailConfig = Zend_Registry::get('configuration')->mail;
		
		if ($mailConfig->amazon)
		{
			$transport = new Zend_Mail_Transport_AmazonSes($mailConfig->amazonconfig->toArray());
		}
		elseif ($mailConfig->smtp)
		{
			$transport = new Zend_Mail_Transport_Smtp($mailConfig->host, $mailConfig->smtpconfig->toArray());
		}
		else
		{
			$transport = new Zend_Mail_Transport_Sendmail();
		}

		Zend_Mail::setDefaultTransport($transport);
		
		$this->fb = new Facebook('3417a1664e1f729761c8b6014fc7f61f', '1edbf13ee84f91669fcbdf41ba653d67');

	}	
	
	
	public function sendTestEmail($iduser)
	{
		
	     $users = new Users(); 
	     $user = $users->getUser($iduser);

	     $emailAddress = $user['email'];
	     
	     
	    /*
		 * Send test email out 
		 */
		$subject= sprintf('Test Email From Mob4Hire - Please Ignore');
		$templateTxt=$this->applyTemplate(array('name'=>$user['name'], 'content'=>'If you have received this in error please contact support@mob4hire.com'), 'template1');
		$this->send($emailAddress, $user['name'], $subject, $templateTxt);
		
		
		return array("subject"=>$subject,"to"=>$emailAddress,"body"=>$templateTxt); // for testing; 
	}

	/**********************
	* Miscellaneous Emails *
	***********************/
	// On first registration sent to confirm email address
	public function sendRegistrationMail($emailAddress, $name, $activationLink, $languageCode='en') // language code no longer used (or not yet)
	{
		$templateTxt=$this->applyTemplate(array('name'=>$name, 'activation_link'=>$activationLink), 'registration');
		//$subject=sprintf('Confirm your registration in %s', $this->sitename); // really we need some way of templating the subjects
		$this->send($emailAddress, $name, 'email_registration_subject', $templateTxt, 'template1', 1); // nasty hack to allow registration mails through
	}

	// Send Email notification of private message
	public function sendMessageNotification($id) 
	{
		$subject="New Mob4Hire message received";
		// No need for a template here: message is always the same at the moment. later maybe add sender and message text we will require more args
		$body="You have received a new message in the Mob4Hire system. Please log in and visit <a href='http://app.mob4hire.com/app/inbox'> your inbox</a> to view.";
		$users = new Users(); 
		$user = $users->getUser($id);
		$emailAddress = $user['email'];
		// todo: check notification status in profile: this is a bit confusing since we have profiles for developer and mobster both with the same settings
		$this->send($emailAddress, $user['username'], $subject, $body);	
	}
	
	public function emailPaypalConfirmation($id, $amount, $account)
	{
		//lookup email address by id
		$users = new Users(); 
		$user = $users->getUser($id);
		$emailAddress = $user['email'];
		
		$subject="Mob4Hire withdrawl of funds to Paypal";
		$body="<p>The amount of $" . $amount . " is pending to paypal account " . $account . " and should be available within 72 hours.</p><br /> Please note that this may take a bit longer in some circumstances.";
		
		$this->send($emailAddress, $user['username'], $subject, $body);
	}
	

	public function sendForgotPassword($user, $code)
	{
		$templateTxt=$this->applyTemplate(array('username'=>$user->username, 'code'=>$code), 'reset_password');
		$subject='Mob4Hire - Recover Your Password';
		$emailAddress = $user['email'];
		$templateTxt.="";
		$this->send($emailAddress, $user['username'], $subject, $templateTxt);		
	}
	
/*********************
	* Group Messages *
	/*********************/
	// Email Notification on user join request
	public function sendJoinGroupRequest($iduser, $idgroup)
	{
		
		 $groups = new Groups(); 
	     $groupList = $groups->getGroup($idgroup);
	        
	     $users = new Users(); 
	     $admin = $users->getUser($iduser);

	     $group = $groupList->toArray(); 
	     $group['invitename'] = $admin['username'];
	    
		/*
		 * Send join request 
		 */
		$subject= sprintf('Someone Wants to Join Your Group  %s', $group['name']);
		$templateTxt=$this->applyTemplate($group, 'group_request_to_join');
		$this->send($group['owneremail'], $group['ownername'], $subject, $templateTxt);
		
		
		return array("subject"=>$subject,"to"=>$group['owneremail'],"body"=>$templateTxt); // for testing; 
	}
	
	
	// Daily Email of new messages
	public function sendGroupDailyDigest($iduser, $idgroup, $timestamp=0)
	{
		 $groups = new Groups(); 
	     $groupList = $groups->getGroup($idgroup);
	        
	     $users = new Users(); 
	     $user = $users->getUser($iduser);
	     
	     $group = $groupList->toArray(); 
	     $group['timestamp'] = $timestamp;
	     $group['iduser'] = $user['id'];
	     
	     
		/*
		 * Send daily email 
		 */
		$subject= sprintf('Mob4Hire Daily Digest - New Messages From  %s', $group['name']);
		$templateTxt=$this->applyTemplate($group, 'group_daily_digest');
		$this->send($group['owneremail'], $group['ownername'], $subject, $templateTxt);
		
		
		return array("subject"=>$subject,"to"=>$user['email'],"body"=>$templateTxt); // for testing; 
	}
	
	
	/*
	 * API New Client Email
	 */
	public function sendNewAPIClientEmail($email, $name, $key, $secret, $smscode)
	{
		$templateTxt=$this->applyTemplate(array('name'=>$name, 'key'=>$key, 'secret'=>$secret,'smscode'=>$smscode), 'api_welcome');
		
		$subject='Access to Mob4Hire SMS API';
		
		$this->send($email, $name, $subject, $templateTxt);
	}
	
	
	/*********************
	* Developer Messages *
	/*********************/
	// Email Notification on draft submitted
	public function sendDraftModerationEmail($projectData)
	{
		/*
		 *  Email 1 Goes out to the admin. With directions on how to accept the moderation
		 */
		$this->sendAdminDraftModeration($projectData);
		/*
		 * Thank you email 
		 */
		$subject=sprintf('Your project is in moderation: %s', $projectData->Name);
		$templateTxt=$this->applyTemplate($projectData->toArray(), 'user_moderation');
		$this->send($projectData->email, $projectData->username, $subject, $templateTxt);
		// We will always send an email for this so no need to notify methinks
	}

	// Draft is approved. What do we do if the draft is declined?
	public function sendProjectApprovedEmail($projectData)
	{
		$subject=sprintf('Your project has been approved: %s', $projectData->Name);
		$templateTxt=$this->applyTemplate($projectData->toArray(), 'user_projectapproved');
		$this->send($projectData->email, $projectData->username, $subject, $templateTxt);
		// We will always send an email for this so no need to notify methinks
	}
	
	public function sendBidSubmitted($idtest, $comments='')
	{
		$subject="You have a bid on your project, [projectname] from [username] : \$[bidAmount]";	
		$this->sendToDeveloper($idtest, $subject, 'new_bid_submitted', $comments, 'template2');
	}

	public function sendTestCaseSubmitted($idtest, $comments='')
	{
		$subject="[username] has submitted test case for project: '[projectname]' \$[bidAmount]";	
		$this->sendToDeveloper($idtest, $subject, 'test_case_submitted', $comments, 'template2');
	}

	// Adds a notification and a message and then sends mail
	public function sendToDeveloper($idtest, $subject, $template, $comments, $layout='template1')
	{
		// Get the data from the tests table etc
		$tests = new Tests(); 
		$templateData=$tests->getTest($idtest)->toArray();
		$templateData['comments']=$comments;
		$subject=$this->applyTemplateToString($templateData, $subject); // Apply template to subject
		$templateTxt=$this->applyTemplate($templateData, $template); // Apply template to body
		
		//$wall = new Wall();
		//$wall->postOnWall($templateData['idtester'], $comments, $idtarget, 2);
		// Send notification

		if ($comments)
		{
			$wall = new Wall();
			$wall->postOnWall($templateData['idtester'], $comments, $idtest, 2, 0, 0); // postType=2, 0 (no parent), 0 don't send an email
		}
	
		$notifications = new Notifications(); 
		$notifications->addNotification($templateData['iddeveloper'],  $subject);

		// Check for email permissions
		$profiles = new ProfileDeveloper(); 
		$permission = $profiles->canSendProjectMessages($templateData['iddeveloper']);
		if($permission)
		{
			$this->send($templateData['devemail'], $templateData['devname'], $subject, $templateTxt, $layout);
		}
	}
	
	/*
	 * This will send out an email informing a developer his project has been closed for bidding automaticaly
	 */
	public function sendProjectBiddingClosed($idproject)
	{
		$projects = new Projects();
		$users = new Users();
		$profiledeveloper = new ProfileDeveloper();
		$projectData=$projects->getProject($idproject);
		$iddeveloper=$projectData['iddeveloper'];
		
		if($userData=$users->getUser($iddeveloper))  { // there is no need to do this. all the information you require is already in Projects::getProject
			$emailAddress=$userData['email'];
			$name=$userData['username'];
			$projectName=$projectData['Name'];
			$endDate=$projectData['testenddate'];
			$subject="Your project end date has past";
			$templateTxt="The bidding end date for your project $projectName has expired.  The current listed ending date for the project is $endDate.  As a result of this the system has closed the bidding period for this projects.  This is done so that the project won't keep receiving new mobster bids when not required.  If you require more time to complete your project, please use the extend bidding period tools provided in the project management page.";
			$templateTxt.="<br/><br/><a href='http://app.mob4hire.com/app/developer/showproject/idproject/$idproject'>http://www.mob4hire.com/app/developer/showproject/idproject/$idproject</a>";
			if($profiledeveloper->canSendProjectMessages($iddeveloper)>0)
			{
				$this->send($emailAddress, $name, $subject, $templateTxt, 'template2');
			}
		}
		
	}
	
	/*
	 * This will send an automated system message for opening a project
	 */
	public function sendProjectBiddingOpen($idproject)
	{
		$projects = new Projects();
		$users = new Users();
		$profiledeveloper = new ProfileDeveloper();
		$projectData=$projects->getProject($idproject);
		$iddeveloper=$projectData['iddeveloper'];
		if($userData=$users->getUser($iddeveloper))  { // there is no need to do this. all the information you require is already in Projects::getProject
			$emailAddress=$userData['email'];
			$name=$userData['username'];
			$projectName=$projectData['Name'];
			$subject="Your project has been started";
			$templateTxt="Your project $projectName start date has arrived. As a result the system has placed your project into active status, you will begin to receive mobster bids in the very near future.  You can examine you project by logging into the mob4hire site.  Thank you again for testing with Mob4Hire Inc.";
			if($profiledeveloper->canSendProjectMessages($iddeveloper)>0)
			{
				$this->send($emailAddress, $name, $subject, $templateTxt, 'template2');
			}
		}
	}
	
	/*********************
	* Tester Messages       *
	/*********************/
	// Send invites to array of testers
	public function sendInvitesTemp($idusers, $idproject, $languageCode='en')
	{
		$usertable= new Users();
		$projectstable= new Projects();
		$projectData= $projectstable->getProject($idproject);
		
		foreach($idusers as $iduser) {
			$userData=$usertable->getUser($iduser);
			$templateData=array( 
				'user'  => $userData,
				'project' => $projectData,
			);
			$subject='Mob4Hire - You have been invited to participate in a project';
			$templateTxt=$this->applyTemplate($templateData, 'tester_invite');

			//Check to see if the dude has bid already
			$testerInvite = new TesterInvites();
			$tests = new Tests();
			$hasBid = $tests->checkIfBid($idproject, $iduser);
			//$inviteSent = $testerInvite->checkInvite($idproject, $iduser);
			if(!$hasBid)
			{
				$testerInvite->addInvite($idproject,$iduser);
				$this->send($userData['email'], $userData['username'], $subject, $templateTxt, 'template2');
				
			}
		}
	}
	
	public function sendTesterInvites($projectData)
	{
		// Invited testers
		if($projectData['inviteonly'] == 1) {	
			$testerInvites = new TesterInvites(); 
			$users = new Users(); 
			
			$invites = $testerInvites->getInvites($projectData['idproject']);
			
			foreach($invites as $testerName){
				if(($user = $users->getUser(0,$testerName['username'])))
				    $this->sendInvite($user, $projectData); 
			}
		}
		else{
		}
		
		
		// TODO add mail to testers who match the requirements ? How do we work with ANY/Any? 
		
	}

	public function sendInvite($user, $projectData) 
	{
		// here you are working on a user object
		$templateData=array( 
			'user'  => $user,
			'project' => $projectData,
  		 );
		$subject='Mob4Hire - You have been invited to participate in a project';
		$templateTxt=$this->applyTemplate($templateData, 'tester_invite');
		
		$profiles = new ProfileMobster();
		$permission = $profiles->canSendProjectMessages($user['id']); // $user is an array not an object (for some reason)
		
		if($permission)
		{
			$this->send($user['email'], $user['username'], $subject, $templateTxt, 'template2');
		}
		$message = "<a href='/app/project/view/idproject/".$projectData['idproject']."'>You have been invited to participate in a project</a>";
//		$messages = new Messages(); 
//		$messages->sendMessage($user['id'], $projectData['iddeveloper'], $templateTxt, $subject);
		
		$notifications = new Notifications(); 
		$notifications->addNotification($user['id'], $message); 
	}

	public function sendAcceptBid($idtest, $comments='')
	{
		$subject="Your bid on project '[projectname]' has been accepted";
		$this->sendToTester($idtest, $subject, 'bid_accepted', $comments, 'template2');
	}

	public function sendRejectBid($idtest, $comments='')
	{
		$subject="Your bid on project '[projectname]' has been rejected";
		$this->sendToTester($idtest, $subject, 'bid_rejected', $comments, 'template2');
	
	}

	public function sendEscrowFundsBid($idtest, $comments='')
	{
		$subject="Your bid on project '[projectname]' has been accepted and funds have been escrowed";
		$this->sendToTester($idtest, $subject, 'escrow_funds', $comments, 'template2');

	}
	
	// Developer has accepted your test case 
	public function sendAcceptTestCase($idtest, $comments='')
	{
		$subject="Your test on project '[projectname]' has been accepted";
		$this->sendToTester($idtest, $subject, 'accept_testcase', $comments, 'template2');
	}
	
	// Developer has declined your test case
	public function sendDeclineTestCase($idtest, $comments='')
	{
		// email to tester
		$subject="Your test on project '[projectname]' has been rejected";
		$this->sendToTester($idtest, $subject, 'decline_testcase', $comments, 'template2');
		
		// Also send admin email
		$tests = new Tests();
		$testData=$tests->getTest($idtest);
		$message = "Developer:".$testData->devname." has rejected test case from:".$testData->username."<br/>comments:<br/>$comments";
		$this->sendToAdmin("A developer has rejected a test case", $message);

	}
	
	//An admin has reviewed a request to decline a test case and it is now being cancelled
	public function sendCancelTestCase($idtest, $comments='')
	{
		// email to tester
		$subject="Your test on project '[projectname]' has been cancelled";
		$this->sendToTester($idtest, $subject, 'cancel_testcase', $comments, 'template2');
		
		//Also send to developer
		$subject="The requested test on project '[projectname]' has been cancelled";
		$this->sendToDeveloper($idtest, $subject, 'cancel_testcase', $comments, 'template2');
	}
	
	//An admin has reviewed a request to reset a test case and it is now being reset
	public function sendResetTestCase($idtest, $comments='')
	{
		// email to tester
		$subject="Your test on project '[projectname]' has been reset";
		$this->sendToTester($idtest, $subject, 'reset_testcase', $comments, 'template2');
		
		//Also send to developer
		$subject="The requested test on project '[projectname]' has been reset";
		$this->sendToDeveloper($idtest, $subject, 'reset_testcase', $comments, 'template2');
	}

	// Adds a notification and a message and then sends mail
	public function sendToTester($idtest, $subject, $template, $comments, $layout='template1')
	{
		// Get the data from the tests table etc
		$tests = new Tests(); 
		$templateData=$tests->getTest($idtest)->toArray();
		$templateData['comments']=$comments;
		$subject=$this->applyTemplateToString($templateData, $subject); // Apply template to subject
		$templateTxt=$this->applyTemplate($templateData, $template); // Apply template to body
		
		if ($comments)
		{
			$wall = new Wall();
			$wall->postOnWall($templateData['iddeveloper'], $comments, $idtest, 2, 0, 0); // postType=2, 0 (no parent), 0 don't send an email
		}
		
		// Send notification
		$notifications = new Notifications(); 
		$notifications->addNotification($templateData['idtester'], "$subject.");

		// Check for email permissions
		$profiles = new ProfileMobster();
		$permission = $profiles->canSendProjectMessages($templateData['idtester']);
		
		if($permission)
		{
			$this->send($templateData['email'], $templateData['username'], $subject, $templateTxt, $layout);
		}
	}

	/********************
	* Admin Messages     *
	/*******************/
	public function sendAdminWithdrawFunds($id, $account, $amount, $comments='')
	{
		$users=new Users();
		$userData=$users->getUser($id);
		$username=$userData['username'];
		$subject="Paypal withdrawal request, $username - $account: \$$amount";
		$templateTxt="$username has requested that the sum of \$$amount be transferred to the paypal account $account <br/><br/>Addiitonal Comments:<br>$comments";
		$this->sendToAdmin($subject, $templateTxt);	
	}
	
	public function sendAdminWithdrawFunds2($id, $account, $amount, $comments='', $transactionid)
	{
		$users=new Users();
		$userData=$users->getUser($id);
		$username=$userData['username'];
		$subject="Paypal withdrawal request, $username - $account: \$$amount";
		$templateTxt="$username has requested that the sum of \$$amount be transferred to the paypal account $account <br/>The account transaction id is: $transactionid<br/>Addiitonal Comments:<br>$comments";
		$this->sendToAdmin($subject, $templateTxt);	
	}
	
	/*
	 * This is using a new page in the admin/index/manualdeposit.  It needs to be verified that it indeed works
	 */
	public function sendAdminManualDeposit($idAdmin, $iduser, $amount, $transactionid, $comments='')
	{
		$users=new Users();
		$accounts = new Accounts();
		$adminData=$users->getUser($idAdmin);
		$adminName=$adminData['username'];
		$userData=$users->getUser($iduser);
		$username=$userData['username'];
		$newBalance=$accounts->getBalance($iduser);
		$subject="Manual Deposit Report";
		$templateTxt="$adminName has added the sum of \$$amount to the account of $username <br/>The account transaction id is: $transactionid<br/>This brings $username account balance to the /$$newBalance <br/>Additional Comments:<br>$comments";
		$this->sendToAdmin($subject, $templateTxt);	
	}
	
	protected function sendAdminDraftModeration($projectData)
	{
		$templateTxt=$this->applyTemplate($projectData->toArray(), 'moderation');
		$templateTxt.= print_r($projectData->toArray(), true);
		$subject='New Project For Moderation';
		$this->sendToAdmin($subject, $templateTxt);
	}
	
	
	public function sendToAdmin($subject, $templateTxt)
	{
		$emailAddress=Zend_Registry::get('configuration')->admin->email;
		$name='Admin';
		$this->send($emailAddress, $name, $subject, $templateTxt, 'template2');
		
		
		
	}
	
	/********************
	* Helper Functions    *
	********************/
	// Apply template given array of values to replace and template name

	protected function applyTemplateToString($vars, $templateTxt)
	{
		//replace tags (tags are field names in square brackets)
		foreach ($vars as $index=>$value) 
		{
			$templateTxt = str_replace("[$index]", $value, $templateTxt);
		}
	
		return $templateTxt;
	}
	
	// Alternative applyTemplate uses John's templates so requires less work to get running
	protected function applyTemplate($vars, $templatename)
	{
		$vars['sitename']=$this->sitename;
		$vars['url']=$this->url;

		$view = new Zend_View(array('encoding' => 'UTF-8'));
		$view->setScriptPath(Zend_Registry::get('siteRootDir').'/application/layouts/email/'.$this->language);
      		$view->assign($vars);
		$templateTxt=$view->render($templatename.".phtml");
 		return $templateTxt;
	}
	
	
	public function recoverySend($emailAddress, $name, $subject, $templateTxt)
	{
		$this->send($emailAddress, $name, $subject, $templateTxt);
	}

	public function sendOverdueTestResults($emailAddress, $name, $projectName, $idproject, $idtest, $daysOverDue=0)
	{
		$subject="Your test for the  project $projectName is overdue.";
		$templateTxt='<p>Your <a href="http://app.mob4hire.com/app/mobster/showproject/idproject/'.$idproject.'">test</a> on this project is now overdue.  The test is now '.$daysOverDue.' days overdue.  Please submit your test results for this project as soon as possible.</p>
		<p>Thank you for working with mob4hire we appreciate your prompt reply to this task.</p>
		<p>If you have questions please contact support@mob4hire as a forward from this email.</p>';
		$this->send($emailAddress, $name, $subject, $templateTxt);
		
	}
	
	public function sendNewWallPostEmail($idposter, $idtarget, $postType, $message,$idmessage=0)
	{
		$db=Zend_Registry::get('db');

		$users=new Users();
		$userData=$users->getUser($idposter);
		$posterName=$userData['username'];
		
		if($postType==1)
		{
			//This is a project wall post.  We need to send emails to all the testers and the developer
			$projects = new Projects();
			$projectData=$projects->getProject($idtarget);
			$profiledeveloper = new ProfileDeveloper();
			$iddeveloper=$projectData['iddeveloper'];
			$projectName=$projectData['Name'];
			$templateTxt="<p>$message<br/><br/>posted on the  $projectName public project wall by $posterName</p>";
			$subject="New project wall post on project: $projectName";
			
			$stmt="SELECT users.email AS email, users.id, users.username AS username, tests.idtest FROM users 
				JOIN profile_tester ON users.id=profile_tester.id
				JOIN tests ON tests.idtester=users.id 
				JOIN projects ON tests.idproject=projects.idproject
				WHERE profile_tester.notify_email LIKE '%3%' AND tests.idproject=$idtarget AND tests.idtester<>$idposter AND tests.Status>249 AND tests.Status<400";
			
			$query=$db->query($stmt);
			if($rows=$query->fetchAll())
			{
				foreach($rows as $row)
				{
					$emailAddress=$row['email'];
					$name=$row['username'];
					$this->sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage);
				}
			}
			if($profiledeveloper->canSendProjectMessages($iddeveloper)>0 && $iddeveloper!=$idposter)
			{
				//We can send the email to the developer
				$devData=$users->getUser($iddeveloper);
				$emailAddress=$devData['email'];
				$name=$devData['username'];
				$this->sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage);
			}
		}
		elseif($postType==2)
		{
			//This is a test wall post we either have to notify the tester or the developer depending on who
			//Made the post
			$templateTxt="<p>$message<br/><br/>posted by $posterName</p>";
			$subject="New private wall post from $posterName";
			$tests = new Tests();
			$testData=$tests->getTest($idtarget);
			if($testData['idtester']==$idposter)
			{
				//It was the tester who made the post so notify the developer if you can
				$projects = new Projects();
				$projectData=$projects->getProject($testData['idproject']);
				$iddeveloper=$projectData['iddeveloper'];
				$profiledeveloper = new ProfileDeveloper();
				if($profiledeveloper->canSendProjectMessages($iddeveloper)>0)
				{
					//The developer wants the email now
					$devData=$users->getUser($iddeveloper);
					$emailAddress=$devData['email'];
					$name=$devData['username'];
					$this->sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage);
				}
			}
			else
			{
				//The tester didn't make the post so notify the tester of the post
				$profiletester = new ProfileMobster();
				if($profiletester->canSendProjectMessages($testData['idtester'])>0)
				{
					//We can send the email now.
					$devData=$users->getUser($testData['idtester']);
					$emailAddress=$devData['email'];
					$name=$devData['username'];
					$this->sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage);
				}
			}
		}
		elseif($postType==3)
		{
			//This is a private wall post we either have to notify only the receiver 
			//or idtarget here of the post.  However we don't have the correct permissions in the 
			//site yet to hand private wall message post email reminders yet
			$receiverData=$users->getUser($idtarget);
			if($users->canSendProjectMessages($idtarget)>0&&($idposter!=$idtarget))
			{
				//Go ahead and send the message
				$templateTxt="<p>$message<br/><br/>posted by $posterName</p>";
				$subject="New private wall post from $posterName";
				$emailAddress=$receiverData['email'];
				$name=$receiverData['username'];
				$this->sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage);
			}
		}
		else {
			return false;
		}
		
		
	}
	
	protected function sendWall($emailAddress, $name, $subject, $templateTxt, $idtarget, $postType,$idmessage=0)
	{
		// append a link to the wall on to the end of the mail
	/*	$templateTxt.="<br/><p>PLEASE DO NOT REPLY TO THIS MESSAGE</p>".
					"<p>You can reply to it at the following URL</p>".
					"<p><a href='http://app.mob4hire.com/app/wall/idtarget/$idtarget/postType/$postType'>http://www.mob4hire.com/app/wall/idtarget/$idtarget/posttype/$postType</a>";
	*/

		$templateTxt.="<br/><p>PLEASE DO NOT REPLY TO THIS MESSAGE</p>".
					"<p>You can reply to it at the following URL</p>".
					"<p><a href='http://app.mob4hire.com/app/inbox/view/$idmessage'>http://app.mob4hire.com/app/inbox/view/$idmessage</a>";
		
		$this->send($emailAddress, $name, $subject, $templateTxt, 'template2');
		
	}

	// Send email
	protected function send($emailAddress, $name, $subject, $templateTxt, $layout='template1', $reg=0, $fromAddress='')
	{

		$subject=$this->translate->_($subject);

		// apply html template to tart up the mail
		$view = new Zend_View(array('encoding' => 'UTF-8')); 
		$view->setScriptPath(Zend_Registry::get('siteRootDir').'/application/layouts/email/'.$this->language);
		$view->assign(array('content'=>$templateTxt, 'name'=>$name)); // if you need more dynamic content in the template addit here
        	$templateTxt = $view->render($layout.".phtml");

		// apply inline styles
		foreach (self::$styles as $index=>$value) 
		{
			$templateTxt = str_replace("$index", $value, $templateTxt);
		}
		
		$logConfig = Zend_Registry::get('configuration')->log;// get log configuration
		if($logConfig->logging == 1)
		{
			$log = new Zend_Log(new Zend_Log_Writer_Stream($logConfig->path . '/mailerLog.txt'));
			$log->info("Inside the send function.\n\n");
			$log->info("emailAddress=:" . $emailAddress . "\n");
			$log->info("name=:" . $name . "\n");
			$log->info("subject=:" . $subject . "\n");
			$log->info("templateTxt=:" . $templateTxt . "\n");
		}		

	  try 	{
			$mailer = new Zend_Mail();
			$mailer->addTo($emailAddress, $name);
			$mailer->setSubject($subject);
			$mailer->setBodyHtml($templateTxt);
			$mailer->setBodyText(strip_tags($templateTxt));
			
			$fromAddress=='' ? $mailer->setFrom(Zend_Registry::get('configuration')->mail->from) : $mailer->setFrom($fromAddress);
			
			$users = new Users();
			$userRow=$users->getUserByEmail($emailAddress);
			
			if($name=='Admin' || $reg || $userRow['active']==1)
			{
				 $mailer->send(); // remember to uncomment this for production
				 //print_r($mailer);
				 //fb(print_r($mailer, TRUE));
			}
		}

		catch(Exception $e)
		{
			// this is so that local instances won't trigger an error. 
			// How dow we handle errors like this?
			if($logConfig->logging == 1)
				$log->info("send method of mailer threw the following exception: " . $e->getMessage() . "\n");
		}
	}

	public function sendDeviceSMS($sms, $activationLink)
	{
		$text = "Mob4Hire. Open Link to add Mobile to your Account";
		return $this->sendSMS('mob4hire',$sms,$activationLink,$text);
	}

/*	
	protected function sendSMS($from='',$to,$si_url,$si_text='')
	{
		$api = "http://api.clickatell.com";
		$apiID = "3042461";
		$user = "mob4hire";
		$password = "CNTXkmu8"; // I think this password has changed. todo: Check live site
				
		$text = urlencode($si_text.' '.$si_url);
		// auth call
		$url = "$api/http/auth?user=$user&password=$password&api_id=$apiID";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = explode(":",$ret[0]);
		
		
		if ($sess[0] == "OK") {
			$sess_id = trim($sess[1]); // remove any whitespace
			$url = "$api/http/sendmsg?session_id=$sess_id&to=$to&text=$text";
			// do sendmsg call
			$ret = file($url);
			$send = split(":",$ret[0]);
			if ($send[0] == "ID")
				return 1;
			else
				return 0;
			} 
		else {
			//echo var_dump($sess);
			return 0;
		}
	}
*/
	protected function sendSMS($from='',$to,$si_url,$si_text='')
	{
		$api = "https://smpp5.routesms.com:8181";
		$user = "mob4hire";
		$password = "vrjat8i2"; // I think this password has changed. todo: Check live site
				
		$text = rawurlencode($si_text.' '.$si_url);

		$url = "$api/bulksms/sendsms?username=$user&password=$password&message=$text&source=$from&destination=$to&dlr=0&type=0";
		fb($url);
		// do sendmsg call
		$ret = file($url);
		fb($ret);
/*		if ($send[0] == "ID")
			return 1;
		else
			return 0;
		}
*/		return 1;
	}
	
	public function sendGetQuoteEmails($posted,$id=0)
	{
		$template='quoteNoLogin';
		If($id)
		{
			//handle newproject requests
			$contactInfo = new ProfileContactInfo();
			$contactData=$contactInfo->getProfile($id);
			
			$posted['country']=$contactData['country'];
			$posted['email']=$contactData['email'];
			$name=$posted['fullname']=$contactData['first_name']." ".$contactData['last_name'];
			$company= $contactData['company']?$contactData['company']:"(company not specified)";
			
			$subject = "New registered managed service request";
			$message = "<b>$name</b> from <b>$company</b> is looking for a new quote.<br />";
			$template='quoteLogin';
		}
		else
		{
			//non registered -data entered in form need to fix country name passing in country number
			$countries = new Countries();
			$posted['country'] = $countries->getCountryName($posted['country']);
		
			$subject = "New managed service request.";
			$message = "Details:<br/>";
		}
		
		//build aggregate list for project manager to read ie: fullname - blah blah
		foreach ($posted as $elementName => $value)
		{
			if($elementName != "submit")
			{
				$message .= $elementName . " - " .$value;
				$message .= "<br/>";
			}
		}
			
		//send admin an email	
		$this->sendToAdmin($subject,$message);
		
		// send client a product email
		$subject = 'RE: '.$posted['fullname'].' , here�s the Mob4Hire information you requested';
		$templateTxt=$this->applyTemplate($dummy = array(), $template);
		$this->send($posted['email'], $posted['fullname'], $subject, $templateTxt, 'template3',1,'info@mob4hire.com');	
	}
}