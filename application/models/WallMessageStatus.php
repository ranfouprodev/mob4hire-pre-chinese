<?php

class WallMessageStatus extends Zend_Db_Table_Abstract  
{
	protected $_name = 'wall_message_status';

	

	public function changeThreadStatus($parent, $newstatus){
	
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this)
				->where("idparent=?",$parent);
								
		//echo $select->__toString();	
		$rows= $this->fetchAll($select);
		
		foreach($rows as $row){
			$row->status = $newstatus;
			$row->save();  
		}
		
	}
	
	public function addOrUpdateStatus($iduser, $idparent, $newstatus){
		
		// Add or update the status of the wall messages
		if($status = $this->getStatus($iduser, $idparent) == false){
			return $this->addStatus($iduser, $idparent, $newstatus);
		}else{
			if($status != $newstatus){
				$this->updateStatus($iduser, $idparent, $newstatus);
			}
			
		}
		
	}		

	public function getStatus($iduser, $idparent){
		
		$columns = array('status');
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, $columns)
				->where("iduser=?",$iduser)
				->where("idparent=?",$idparent);
								
		// echo $select->__toString();	
		$rows= $this->fetchAll($select);

		$row = $rows->current();
		
		if(!isset($row))
			return false; 
		
		return $row['status'];
	}
	
	public function addStatus($iduser, $idparent, $status){
		
		$data = array (
			'idparent'=>$idparent,
			'iduser'=>$iduser,
			'status'=>$status);
		try {
			$idfile = $this->insert($data);
		}
		catch(Exception $e)
		{
			throw new Exception("Error inserting status:".$e->getMessage());
		}
		return $idfile;
	}
	
	public function updateStatus($iduser, $idparent, $status){
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->where("iduser=?",$iduser)
				->where("idparent=?",$idparent);
								
		// echo $select->__toString();	
		$row = $this->fetchRow($select);
		
		if(isset($row)){
			$row->status = $status;	
			$row->save(); 
		}		
	}
	
	
}