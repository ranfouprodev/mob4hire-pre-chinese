<?php
require_once 'Lib/Cache/CacheFactory.php';

class Accounts extends Zend_Db_Table_Abstract  
{
	protected $_name = 'accounts';
	// These are the only acceptable values for transactioncode. If it aint her, it don't get in
	public static $transactioncodes=array(
										0 => 'Paypal deposit',
										1 => 'Paypal withdrawal',
										2 => 'escrow in - pay for test',
										3 => 'escrow out - get paid for test',
										4 => 'escrow out - escrow returned to developer',
										5 => 'premium commision fee',
										6 => 'managed services fee',
										7 => 'error adjustment payment' , 
										8 => 'transfer funds', 
										9 => 'manual deposit', 
										10 => 'manual withdrawal',
										11 => 'mobster bonus',
										12 => 'error adjustment withdrawal' ,
										13 => 'payment for survey:1',
										14 => 'developer bonus paid by mob4hire'
									);
	/*
		Get balance by user id
	*/
	public function listAccounts($id)
	{
		$select=$this->select();
		$select->from($this)
			->where('accounts.iduser=?', $id)
			->order(array('transactionid desc'));
		$rows= $this->fetchAll($select);
		return $rows;
	}
	
	/*
	 * New function used for admin to get a single transaction from the database.
	 * It will be used in conjunction with an update later to check some of the info
	 * mainly adding a paypal txid
	 */
	public function getTransaction($transactionid)
	{
		$row=$this->fetchRow("transactionid = '$transactionid'");
		return $row;
	}
	
	/*
	 * A basic update method in accounts to allow for updating of accounts
	 */
	public function updateAccount($transactionid, $data)
	{
		$rowset = $this->find($transactionid);
		$row = $rowset->current();
		
		foreach ($data as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of role cannot be changed');
				}
				else
				{
					$row->{$name}=$value;						
				}
			}
		}
		$row->save();
		return $this;
	}
	
	public function getBalance($id)
	{
		$select=$this->select();
		$select->from($this, array('newBalance'))
			->where('accounts.iduser=?', $id)
			->order(array('transactionid desc'));
		$rows= $this->fetchRow($select);
		if (!empty($rows))
			return $rows->newBalance; // return first record
		else
			return 0; // This user hasn't done anything therefore his balance is zero
	}
	
	public function moneyIn($id, $amount, $idtest,$code=-1, $description='')
	{
		$amount=(float)((int)($amount * 100) / 100.0); // convert amount to a currency value
		$timestamp=time();
		$oldBalance=$this->getBalance($id);
		$newBalance=$oldBalance+$amount;
		if($code==0)
		{
			$transactionType='Paypal deposit:99';
		}
		else if($code==3)
		{
			$transactionType='Payment for test '.$idtest;
		}
		else if($code==4)
		{
			$transactionType='Escrow Returned (Test Canceled) Test:'.$idtest;
		}
		else if($code==7)
		{
			$transactionType='Error Payment: '.$description;
		}
		else if($code==8)
		{
			$transactionType='Transfer Funds: '.$description;
		}
		else if($code==9)
		{
			$transactionType='Manual Deposit: '.$description;
		}
		else if($code==11)
		{
			$transactionType='Mobster Bonus: '.$description;	
		}
		if($code==-1)
		{
			if($idtest==0)
			{
				$ret=$this->insert(array('iduser'=>$id, 'amount'=>$amount, 'transactionType'=>"Paypal deposit:99", 'usertype'=>2, 'Timestamp'=>$timestamp, 'newBalance'=>$newBalance,'transactioncode'=>0));
			
			}
			else
			{
				$ret=$this->insert(array('iduser'=>$id, 'amount'=>$amount, 'transactionType'=>"Payment for test ".$idtest, 'usertype'=>1, 'idtest'=>$idtest,'Timestamp'=>$timestamp, 'newBalance'=>$newBalance,'transactioncode'=>3));
			
			}
		}
		else
		{
			$ret=$this->insert(array('iduser'=>$id, 'amount'=>$amount, 'transactionType'=>$transactionType, 'usertype'=>1, 'idtest'=>$idtest,'Timestamp'=>$timestamp, 'newBalance'=>$newBalance,'transactioncode'=>$code));
		}
		return $this->getAdapter()->lastInsertId();
	}
	
	public function withdrawFunds($id, $amount, $txid='', $comments='', $account='')
	{
		$amount=(float)((int)($amount * 100) / 100.0); // convert amount to a currency value
		$timestamp=time();
		$oldBalance=$this->getBalance($id);
		
		if($oldBalance >= $amount)
		{
			// Ok. We have the funds available
			$newBalance=$oldBalance-$amount;
			$this->insert(array('iduser'=>$id, 'amount'=>-1*$amount, 'transactionType'=>"Paypal withdrawal:".$txid, 'usertype'=>1, 'Timestamp'=>$timestamp, 'newBalance'=>$newBalance,'transactioncode'=>1));
			// The paypal txid should be added when the transaction is complete TODO:
			if($txid == '')
			{
				$mailer=new Mailer();
				$mailer->sendAdminWithdrawFunds($id, $account, $amount, $comments);
			}
		}
		else {
			return -1; // This return value indicates the withdrawal was unsuccessful
		}
	}
	public function moneyOut($id, $amount, $description, $idtest,$code=-1)
	{
		$amount=(float)((int)($amount * 100) / 100.0); // convert amount to a currency value
		$timestamp=time();
		$oldBalance=$this->getBalance($id);
		if($code==2)
		{
			$transactionType="Escrow for test:".$idtest;
		}
		elseif($code==5)
		{
			//This is done in the project controller
			$transactionType=$description;
		}
		elseif($code==6)
		{
			$transactionType='Managed Service Fee: '.$description;
		}
		elseif($code==8)
		{
			$transactionType='Transfer Funds: '.$description;
		}
		elseif($code==10)
		{
			$transactionType='Manual Withdraw: '.$description;
		}
		elseif($code==12)
		{
			$transactionType='Error Withdraw: '.$description;
		}
		else 
		{
			$transactionType=$description;
		}
		if($oldBalance >= $amount)
		{
			// Ok. We have the funds available
			$newBalance=$oldBalance-$amount;
			if($code==-1)
			{
				if($idtest==0)
				{
					//This is used for when no test is added in the moneyOut function
					$ret=$this->insert(array('iduser'=>$id, 'amount'=>-1*$amount, 'transactionType'=>$transactionType, 'usertype'=>2, 'Timestamp'=>$timestamp, 'newBalance'=>$newBalance));
				
				}
				else
				{
					$ret=$this->insert(array('iduser'=>$id, 'amount'=>-1*$amount, 'idtest'=>$idtest,'transactionType'=>$transactionType, 'usertype'=>2, 'Timestamp'=>$timestamp, 'newBalance'=>$newBalance));
				}
			}
			else
			{
				$ret=$this->insert(array('iduser'=>$id, 'amount'=>-1*$amount, 'idtest'=>$idtest,'transactionType'=>$transactionType, 'usertype'=>2, 'Timestamp'=>$timestamp, 'newBalance'=>$newBalance,'transactioncode'=>$code));
				
			}
			
			
			return $this->getAdapter()->lastInsertId();
		}
		else 
		{
			return -1; // This return value indicates the withdrawal was unsuccessful
		}
	}
}
