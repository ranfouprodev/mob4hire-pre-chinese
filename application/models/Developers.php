<?php
require_once 'Lib/Cache/CacheFactory.php';
require_once 'Lib/Util/Timer.php';

class Developers extends Zend_Db_Table_Abstract  
{
    protected $_name = 'developers';

    public function count()
    {
		$timer = Timer::start();    	
    	$cache = CacheFactory::getCache('developers');
		$countValue = 0;
		if ( !$countValue = $cache->get('count') ) {    	
			$select=$this->select();
			$select->from($this, array ('COUNT(iddeveloper) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->setE('count', $countValue, Timer::stop($timer));
		}
		return $countValue;
    }
}