<?php

class TeraWurfl extends Zend_Db_Table_Abstract  
{
	protected $_name = 'TeraWurflMerge';

	// This gets record from the TeraWurfl table $resultsPerPage at a time to prevent script timeout when populating the deviceatlas table
	public function getDevicesWithLImits($pageNumber, $resultsPerPage)
	{
		$select=$this->select();	
		$select->from($this)
			->limitPage($pageNumber, $resultsPerPage);
		$rows= $this->fetchAll($select);
		return $rows;
	}
}
