<?php
class SupportedDevices extends Zend_Db_Table_Abstract
{
	protected $_name = 'supporteddevices';
	
	public function getSupportedDevices($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to developers table to get developer username
		$select->distinct()
			->from($this)
			->joinLeft(array('d'=>'deviceatlas'), 'd.id=supporteddevices.tid',  array("d.model"))
			->where('idproject=?', $idproject);
		try {
			$rows= $this->fetchAll($select);
		}
		catch (Exception $e) {
			return false;
		}
		return $rows;
	}
	
	public function addSupportedDevice($idproject, $platform, $vendor, $iddevice, $executable='')
	{
		$data=array(
				'idproject'=>$idproject,
				'platform'=>$platform,
				'vendor'=>$vendor,
				'tid'=>$iddevice,
				'executable'=>$executable);
		try {
			$this->insert($data);
		}
		catch (Exception $e)
		{
		// prevent error messages if we attempt to add duplicates
		}
	}
	
	public function removeSupportedDevice($idproject, $platform, $vendor, $iddevice)
	{
		
		try {
			return $this->delete(array('idproject ='.$idproject, 'platform ="'.$platform.'"', 'vendor ="'.$vendor.'"' , 'tid = '.$iddevice)); 
		}
		catch (Exception $e)
		{
			 return $e;
		}
		
	}
	
	public function listSupportedVendors($idproject)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('platform','vendor'))
			->where('idproject=?', $idproject);
			
		$rows= $this->fetchAll($select);
		if(count($rows)==0)
		{
			$devices = new Devices();
			return $devices->listVendors();
		}
		$array=array();
		foreach($rows as $row)
		{
			if(!$row['vendor']||$row['vendor']=='0')
			{
				if(!$row['platform']||$row['platform']=='0')
				{
					//This means that there is NO vendor OR platform so it means all
					$devices = new Devices();
					return $devices->listVendors();
				}
				else
				{
					$devices = new Devices();
					$tempArray=$devices->listVendors($row['platform']);
					foreach($tempArray as $temp)
					{
						if(!in_array($temp['vendor'], $array))
						{
							$array[$temp]=$temp;
						}
						
					}
				}
				
			}
			elseif(!in_array($row['vendor'], $array))
			{
				$array[$row['vendor']]=$row['vendor'];
			}
			
		}
		return $array;
	}
	

}