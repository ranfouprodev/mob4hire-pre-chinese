<?php

/*
 * SMS tracking db for device id
 */
class DeviceActivation extends Zend_Db_Table_Abstract
{
    protected $_name = 'device_activation';
    protected $_primary = 'idact';

	public static $status=array(100=>"Sent", 101=>"WURFL ID", 102=>"DA ID", 103=>"MANUAL");
	
	public function count($userid)
	{

			$select=$this->select();
			$select->from($this, array ('COUNT(iduser) as count'));
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			return $countValue;
	}

	public function getActivation($code)
	{
		$id=(int)$id; // Make sure id is an integer
		$row = $this->fetchRow('act_code = ' . $id);
		
		if (!$row) {
			throw new Exception("Could not find row $id");
		}
		return $row->toArray();
	}

    public function closeActivation($id, $status)
    {
	
		$id=(int)$idfile; // Make sure id is an integer
		$row = $this->fetchRow('idcomment = ' . $idfile);
		
		if($row){
			$row->status=$status;
			$row->save();	
		}

    }
	
	public function sendActivation($id, $sms){
		
		// Check the user isn't spamming
		$count = $this->count($id);
		
		/*if($count > 10){
			echo "expired";
			return "0";
		}	*/
		
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
		$str = "";
		
		$size = strlen( $chars );
		for( $i = 0; $i < 5; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}

		$activationCode = $str; 
        
        $newActivity = array(
            'iduser' => $id,
            'sms' => $sms,
            'status' => "100",
            'code' => $activationCode,
            'sent' => new Zend_Db_Expr('NOW()')        
        );
		
		$actId = $this->insert($newActivity);
		
		$mailer = new Mailer();        
	//	$activationLink = Zend_Registry::get('configuration')->general->url . '/mobile/'. $activationCode;
		$activationLink = Zend_Registry::get('configuration')->general->url . '/mobile/';
		return	$mailer->sendDeviceSMS($sms, $activationLink);
	
		
	}

  

}
