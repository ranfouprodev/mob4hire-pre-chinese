<?php
require_once 'Lib/Cache/CacheFactory.php';
require_once 'Lib/Util/Timer.php';

class Regdevices extends Zend_Db_Table_Abstract  
{
	protected $_name = 'regdevices';
	private static $_platforms=array("osAndroid", "osSymbian", "osWindows", "osOsx", "osRim", "osLinux");
	
	public function count()
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('regdevices');
		$countValue = 0;
		if ( !$countValue = $cache->get('count') ) {		
			$select=$this->select();
			$select->from($this, array ('COUNT(idregdevice) as count'))
				->where('deviceatlasid!=0 and status=1');
			$rows= $this->fetchAll($select);
			$row=$rows->current();
			$countValue = $row['count'];
			$cache->setE('count', $countValue, Timer::stop($timer));
		}
		return $countValue;
	}
    
	public function carrierCount()
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('regdevices');
		$countValue = 0;
		if ( !$countValue = $cache->get('carrier_count') ) {		
			$select=$this->select();
			// fetch only the carriers where the device has been registered
			$select->distinct()
				->from($this, array ('idnetwork'))
				->where('deviceatlasid!=0 and status=1');
			$rows= $this->fetchAll($select);
			$countValue = count($rows);
			$cache->setE('carrier_count', $countValue, Timer::stop($timer));
		}
		return $countValue;
	}
    
	public function countryCount()
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('regdevices');
		$countValue = 0;
		if ( !$countValue = $cache->get('country_count') ) {		
		
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// fetch only the carriers where the device has been registered
			$select->distinct()
				->from($this, array())
				->join(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array("n.idcountry"))
				->where('regdevices.deviceatlasid!=0 and status=1');
			$rows= $this->fetchAll($select);
			$countValue = count($rows);
			$cache->setE('country_count', $countValue, Timer::stop($timer));
		}
		return $countValue;
	}

	public function distinctDevicesCount($whereClause=1)
	{
		$timer = Timer::start();
		$cache = CacheFactory::getCache('regdevices');
		$countValue = 0;
		if ( !$countValue = $cache->get('count_devices') ) {		
		
		$whereClause .= ' and status=1';
		
			$select=$this->select();
			$select->setIntegrityCheck(false);
/*			$select->distinct()
				->from($this, array ())
				->join(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array("n.network"))
				->join(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array("c.country"))
				->join(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array("d.model","d.vendor"))
				->join(array('u'=>'users'),'u.id=regdevices.idtester', array("u.username"))
				->where(stripslashes($whereClause));*/
			$select->distinct()
				->from($this, array('idregdevice', 'http_user_agent'))
				->joinLeft(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array("n.network","n.idnetwork"))
				->joinLeft(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array("c.country", "c.idcountry"))
				->joinLeft(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array("d.model","d.vendor", "d.id"))
				->join(array('u'=>'users'),'u.id=regdevices.idtester', array("u.username"))
				->joinLeft(array('r'=>'ratings'),'r.id=u.username', array("(r.total_value/r.total_votes) as rating"))
				->where(stripslashes($whereClause));	
				
//			echo $select->__toString();
			$rows= $this->fetchAll($select);
			$countValue = count($rows);
			$cache->setE('count_devices', $countValue, Timer::stop($timer) );
		}
		return $countValue;
		
	}

	public function listRegdevices($whereClause, $orderBy='c.country',  $pageNumber=1, $resultsPerPage=0)
	{
		$cache = CacheFactory::getCache('regdevices');
		$whereClause .= ' and status=1'; //This has to be here or it will mess up the $cacheKey I think
		$cacheKey = 'list_' . $whereClause . '_' . $orderBy . '_' . $pageNumber . '_' . $resultsPerPage;
		$cacheKey = preg_replace('/[^A-Za-z0-9]/', '_', $cacheKey);
		
		$timer = Timer::start();
		if ( !$rows = $cache->get($cacheKey) ) {
			$select=$this->select();
			$select->setIntegrityCheck(false);
			// join to countries , networks, makes, devices tables to get metadata
			$select->distinct()
				->from($this, array('idregdevice', 'http_user_agent', 'IPAddress', 'manualid', 'mac_id'))
				->order(explode(",",$orderBy))
				->joinLeft(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array("n.network","n.idnetwork"))
				->joinLeft(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array("c.country", "c.idcountry"))
				->joinLeft(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array("d.model","d.vendor", "d.id", "d.osAndroid", "d.osSymbian", "d.osRim", "d.osWindows","d.osOsx", "d.osLinux"))
				->join(array('u'=>'users'),'u.id=regdevices.idtester', array("u.username", "u.id as userid", "pro"))
				->joinLeft(array('r'=>'ratings'),'r.id=u.username', array("(r.total_value/r.total_votes) as rating"))
				->where(stripslashes($whereClause));
			if($resultsPerPage)
				$select->limitPage($pageNumber, $resultsPerPage);	
				
//			echo $select->__toString();
			$rows= $this->fetchAll($select);
			$cache->setE($cacheKey, $rows, Timer::stop($timer));
		}
		return $rows;
	}
	
	 public function removeDevice( $iddevice)
	{
	
		$id=(int)$iddevice; // Make sure id is an integer
		$row = $this->fetchRow('idregdevice = ' . $iddevice);
		
		if($row){
			$row->status="0";
			$row->save();	
		}

	}

	public function getHandsetsByUser($id)
	{
		return $this->listRegDevices("regdevices.idtester=$id and not isnull(d.model)", 'd.vendor,d.model');
	}
	
	public function getNetworksByUser($id)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to countries , networks, makes, devices tables to get metadata
		$select->distinct()
			->from($this, array())
			->order('c.country')
			->join(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array("n.network","n.idnetwork"))
			->join(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array("c.country", "c.idcountry"))
			->where("regdevices.idtester=$id");
		$rows= $this->fetchAll($select);
		return $rows;
	}

	public function hasDevice($iddevice, $iduser)
	{
		$row=$this->fetchRow("deviceatlasid=$iddevice AND idtester=$iduser");
		return $row;
	}

	public function identifyDevice($idregdevice)
	{
		// used when we already have the user agent logged
		$rowset=$this->find($idregdevice);

		if(($regdevice=$rowset->current())) {
			$devices=new Devices();
			$deviceprops=$devices->identifyDevice($regdevice->http_user_agent);
			if($deviceprops && (isset($deviceprops['id'])) && (isset($deviceprops['mobileDevice']))) {
				$regdevice->deviceatlasid=$deviceprops['id'];			
				$regdevice->save();
				return $deviceprops;
			}
		}
		return false; // device has not been identified
	}

	public function registerDevice($useragent, $iduser, $uaprof='', $ip='')
	{
		$devices=new Devices();
		$deviceprops=$devices->identifyDevice($useragent);
		if($deviceprops && (isset($deviceprops['id'])) && (isset($deviceprops['mobileDevice'])) && $iduser)
		{
			if (!$this->hasDevice($deviceprops['id'], $iduser)) {
				// get the country based on ip address
				$geoip = new GeoIP(); // This is all a bit ugly since I 'ported' it from PHP 4
				$gi = $geoip->geoip_open("GeoIP.dat", GEOIP_STANDARD);	
				$isocode=$geoip->geoip_country_code_by_addr($gi, $ip);
				$countries= new Countries();
				$country=$countries->getCountryByIso($isocode);
				$deviceprops['added'] = true;
				$timestamp=time();
				$this->insert(array('http_user_agent'=>$useragent, 'idtester'=>$iduser, 'idnetwork'=>'0', 'deviceatlasid'=>$deviceprops['id'],'UAProf'=>$uaprof,  'IPAddress'=>$ip, 'Timestamp'=>$timestamp)); // We don't know the carrier yet!
			}
		}
		return $deviceprops;
	}

	public function addDevice($device, $iduser, $ua='', $ip='')
	{
		if($device && (isset($device['id'])) && (isset($device['mobileDevice'])) && $iduser)
		{
			if (!$this->hasDevice($device['id'], $iduser)) {
				// get the country based on ip address
				$geoip = new GeoIP(); // This is all a bit ugly since I 'ported' it from PHP 4
				$gi = $geoip->geoip_open("GeoIP.dat", GEOIP_STANDARD);
				$isocode=$geoip->geoip_country_code_by_addr($gi, $ip);
				$countries= new Countries();
				$country=$countries->getCountryByIso($isocode);
				$device['added'] = true;
				$timestamp=time();
				$this->insert(array('http_user_agent'=>$ua, 'idtester'=>$iduser, 'idnetwork'=>'0', 'deviceatlasid'=>$device['id'], 'IPAddress'=>$ip, 'Timestamp'=>$timestamp)); // We don't know the carrier yet!
			}
		}
		return $device;
	}
	
	public function getPlatform($device)
	{
		
		foreach (self::$_platforms as $platform)
		{
			if ($device->{$platform})
				return $platform;
		}
		return 0;
	}
	/*
	/ This function returns a whereclause which can be used in conjunction with Projects::listProjects() to limit projects to those compatible with this device
	   Its single argument is an object containing the device data
	*/
	public function isCompatibleDevice($device)
	{
		$platform=$this->getPlatform($device);
		$output="(";
		$output.="	(
						(
							(sd.tid=0 OR isnull(sd.tid)) 
							AND (isnull(sd.vendor))
							AND (isnull(sd.platform))
						) 
					OR 
						(sd.tid='$device->id') 
					OR 	(
							(sd.tid=0 OR isnull(sd.tid))
							AND (sd.vendor='$device->vendor' OR  (isnull(sd.vendor) OR sd.vendor='')) AND (sd.platform='$platform' OR isnull(sd.platform) OR sd.platform='')
						)
					)";
		$output.="AND (((sn.idnetwork=0 OR isnull(sn.idnetwork)) AND (sn.idcountry=0 OR isnull(sn.idcountry))) OR (sn.idnetwork='$device->idnetwork' AND sn.idcountry='$device->idcountry') OR (sn.idnetwork=0 AND sn.idcountry='$device->idcountry'))";
		$output.=")";
		return $output;
	}

	// constructs a whereclause which can be used to get regdevices compatible with this project
	public function getCompatibleHandsets($idproject)
	{
		$whereClause="";
		$projects=new Projects();
		$supportedDevices=$projects->getSupportedDevices($idproject);
		$supportedNetworks=$projects->getSupportedNetworks($idproject);
		// There are no supporteddevices entries; all devices are supported
		if(!$supportedDevices->count()) {
			$deviceClause='1';
		}
		else {
			$firstqueryset=0;
			$deviceClause='(';
			foreach($supportedDevices as $supportedDevice)
			{
				if ($firstqueryset)
					$deviceClause.=' OR ';
				else
					$firstqueryset=1;
				if($supportedDevice->tid) { // a specific device is specified
					$deviceClause.="(d.id='$supportedDevice->tid')";
				}
				else if ($supportedDevice->vendor) { // make is specified
					$deviceClause.="(d.vendor='$supportedDevice->vendor'";
					if ($supportedDevice->platform) { // vendor and platform are selected
						$deviceClause.=" AND d.$supportedDevice->platform";
					}
					$deviceClause.=')';
				}
				else if($supportedDevice->platform) { // only a platform is specified
					$deviceClause.="(d.$supportedDevice->platform)";
				}
				else { // anything goes
					$deviceClause.='(NOT isnull(d.id))'; // In this case the rest of the query is built but clearly once we have been here it will always evaluate to true
				}
			}
			$deviceClause.=')';
		}
		// There are no supportednetworks entries; all networks are supported
		if (!$supportedNetworks->count()) {
			$networkClause='1';
		}
		else {
			$firstqueryset=0;
			$networkClause='(';
			foreach($supportedNetworks as $supportedNetwork)
			{
				if ($firstqueryset)
					$networkClause.=' OR ';
				else
					$firstqueryset=1;
				if($supportedNetwork->idnetwork) { // a specific network is requested
					$networkClause.="(n.idnetwork='$supportedNetwork->idnetwork')";
				}
				elseif($supportedNetwork->idcountry) { // only a country is specified
					$networkClause.="(n.idcountry='$supportedNetwork->idcountry')";
				}
				else { //anything goes
					$networkClause.='(NOT isnull(n.idnetwork))'; // will always be true
				}
			}
			$networkClause.=')';
		}
		$whereClause=$deviceClause. ' AND '. $networkClause;
		return $whereClause;
	}
/*
	public function getCompatibleUsers($idproject, $limit='')
	{
		$whereClause=$this->getCompatibleHandsets($idproject);
		$whereClause .= ' and regdevices.status=1'; // make sure device has not been deleted
		//This is not being used as it wasn't as slick as Richards
		//$whereClause2='regdevices.idtester NOT IN (SELECT tester_invites.idtester FROM tester_invites WHERE tester_invites.idproject='.$idproject.')';
		$whereClause2='isnull(idinvite)';
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to countries , networks, makes, devices tables to get metadata
		$select->from($this, array('distinct(regdevices.idtester)'))
				->group('u.id')
				->order('rating desc')
				->order('u.id desc')
				->joinLeft(array('p'=>'profile_contactinfo'), 'p.id=regdevices.idtester',array('p.time_zone'))
				->joinLeft(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array('n.network'))
				->joinLeft(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array('c.country'))
				->joinLeft(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array('d.vendor', 'd.model'))
				->join(array('u'=>'users'),'u.id=regdevices.idtester', array("u.id as userid", "u.username"))
				->joinLeft(array('r'=>'ratings'),'r.id=u.username', array("(r.total_value/r.total_votes) as rating"))
				->joinLeft(array('ti'=>'tester_invites'), 'ti.idtester=u.id and  ti.idproject='.$idproject, array('idinvite'))
				->where(stripslashes($whereClause))
				->where($whereClause2);
			if($limit)
				$select->limit($limit);	
				
			fb($select->__toString());
			$rows= $this->fetchAll($select);
			return $rows;
	}
*/
	public function getCompatibleUsers($idproject, $limit='', $filterClause='')
	{
		$whereClause=$this->getCompatibleHandsets($idproject);
		if($filterClause!='')
		{
			$newFilter=' AND '.$filterClause;
		}
		else
		{
			$newFilter='';
		}
		$whereClause .= ' and regdevices.status=1'.$newFilter; // make sure device has not been deleted
		//This is not being used as it wasn't as slick as Richards. Oh it was, Dean. It was way slicker
		$notInWhere = " AND u.id NOT IN (SELECT idtester FROM tests WHERE idproject=$idproject AND Status>99 AND Status<401)";
		$whereClause .=$notInWhere;
		//This where clause will get all those who have accepted to recieve emails or those who haven't even
		//filled out a profile_tester entry it the database so they are forced to get emails until they fill it out
		// I don't like this. It pulls users who don't meet the criteria
		$ptWhere=" AND (pt.notify_email LIKE '%3%' OR ISNULL(pt.id))";
		$whereClause .=$ptWhere;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		// join to countries , networks, makes, devices tables to get metadata
		$select->from($this, array('regdevices.idtester'))
				->group('u.id')
				->order('rating desc')
				->order('u.id desc')
				->joinLeft(array('p'=>'profile_contactinfo'), 'p.id=regdevices.idtester',array('p.time_zone', 'p.city AS city', 'p.company AS company'))			
				->joinLeft(array('pt'=>'profile_tester'),'pt.id=p.id',array())
				->joinLeft(array('n'=>'networks'), 'n.idnetwork=regdevices.idnetwork',  array('n.network'))
				->joinLeft(array('c'=>'countries'), 'c.idcountry=n.idcountry',  array('c.country'))
				->joinLeft(array('d'=>'deviceatlas'), 'd.id=regdevices.deviceatlasid',  array('d.vendor', 'd.model'))
				->join(array('u'=>'users'),'u.id=regdevices.idtester', array("u.id as userid", "u.username"))
				->joinLeft(array('r'=>'ratings'),'r.id=u.username', array("(r.total_value/r.total_votes) as rating"))
				->where(stripslashes($whereClause));
				//->where($ptWhere);
				//->where($otherWhere);
			if($limit)
				$select->limit($limit);	
			fb($select->__toString());
			$rows= $this->fetchAll($select);
			return $rows;
	}

	public function countDevicesOverRange($startDate, $endDate)
	{
		//TODO need to write test script for this
		//This function will execute the following SQL statement using the Zend Framework
		//SELECT COUNT(rd.idregdevice) AS 'count' FROM regdevices AS rd 
		//WHERE rd.timestamp > ? AND rd.timestamp < ?;
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("rd" => 'regdevices');
		$columns = array("count" =>'COUNT(rd.idregdevice)');
		$statement = $select->from($tableInfo, $columns)
			->where("rd.timestamp >= ?", strtotime($startDate))
			->where("rd.timestamp <= ? ", strtotime($endDate));
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countNetworks()
	{
		//TODO need to write some view to test this yet
		//SELECT COUNT(DISTINCT(idnetwork)) FROM regdevices;
		
		$select=$this->select();
		$tableInfo = array("n" => 'regdevices');
		$columns = array("count" =>'COUNT(DISTINCT(n.idnetwork))');
		$whereClause='NOT ISNULL(deviceatlasid)';
		$statement = $select->from($tableInfo, $columns)
			->where($whereClause);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
		
	}
	
	public function fixTimestamps()
	{
		$users = new Users();
		$select=$this->select();
		$tableInfo = array("rd" => 'regdevices');
		$columns = array("idtester" =>'rd.idtester', "idregdevice" =>'rd.idregdevice');
		$whereClause='ISNULL(rd.Timestamp) AND rd.idtester>10000';
		$statement = $select->from($tableInfo, $columns)
			->where($whereClause);
		$rows=$this->fetchAll($select);
		
		foreach($rows as $row)
		{
			//TODO add code here to make the timestamp equal to the timestamp I got
			$timestamp = strtotime($users->getUserTimeStamp($row['idtester']));
			$db=Zend_Registry::get('db');
			$stmt = 'UPDATE regdevices SET Timestamp = '.$timestamp.' WHERE idregdevice='.$row['idregdevice'];
			$db->query($stmt);	
		}
		
	}
	
	public function percentageTestersWithHandsets()
	{
		$tester = new ProfileMobster();
		//$devices=$this->distinctUserCount();
		$devices=$this->distinctUserCount();
		$devices=$devices*100;
		$testers=$tester->countAllTesters();
		return $devices/$testers;
		
	}
	
	public function distinctUserCount()
	{
		$select=$this->select();
		$select->from($this, array ('COUNT(DISTINCT(idtester)) as count'));
		$rows= $this->fetchAll($select);
			
		$row=$rows->current();
		return $row['count']+2044;
		
	}

	public function updateDevice($idregdevice, $data)
	{
		$rowset = $this->find($idregdevice);
		$row = $rowset->current();
		
		foreach ($data as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of role cannot be changed');
				}
				else
				{
					$row->{$name}=$value;						
				}
			}
		}
		$row->save();
		return $this; // to allow chaining of methods
	}
	
	public function getRegdevice($idregdevice)
	{
		$rowset = $this->find($idregdevice);
		$row = $rowset->current();
		return $row;
	}
}
