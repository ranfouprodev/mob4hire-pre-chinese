<?php

class ProfileMobster extends Zend_Db_Table_Abstract
{
	
	
	public static $previousTesting = array
	(1=>'Limited or no testing experience',
	2=>'Some testing experience',
	3=>'Experienced tester',
	4=>'Some QA/QC training',
	5=>'Software QA/QC professional.');
	
	protected $_name='profile_tester';
	protected $_dependentTables = array('Users');
	
	// A profile should be created when the user is the first time. 
	// this creates an empty record
	public function add($id)
	{
        
		$newUser = array(
				'id' => $id,
				'notify_email' =>'1,2,3'
				);
                
		$profileId = $this->insert($newUser);
     
		return $profileId;        
	}
	
	public function hasProfile($id){ 
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		
		if(!$row){
			return false;
		}
		else{
			return true;
		}
	
	}
	
	
	
	
	/*
	 Can be called with either id or Username
	*/
	public function getProfile($id, $username='')
	{
		$id=(int)$id; // Make sure id is an integer
		if ($username)
		{
			$users=new Users();
			$userrow=$users->fetchRow("username = '$username'");
			$id=$userrow['id'];
		}
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->distinct()
			->from($this)
			->where('id = ' . $id);
		$row = $this->fetchRow($select);
		
		
		if(!$row){
			throw new Exception("Could not find row $id");
		}
		else{
			$userrow=$row->findDependentRowset('Users')->toArray();
			$userrow[0]['password']=''; // Don't send the password
			return array_merge($row->toArray(), $userrow[0]);
		}
		
	}

	/* 
		Can be used to update only the profile_contact_info table. It does not affect anything in the users table
	*/
	public function updateProfile($userId, $profileData)
	{
		$profileRowset = $this->find($userId);
		// no user in the db. 
		if(!$profileRowset->count())
		{
			$this->add($userId);
			$profileRowset = $this->find($userId);
		}
		
		
		
		$profile = $profileRowset->current();

		foreach ($profileData as $name=>$value)
		{
			if (in_array($name, $this->_cols))
			{
				if ($name == $this->_primary)
				{
					throw new Zend_Db_Table_Exception('Id of user cannot be changed');
				}
				else
				{
					if(is_array($value))
						$profile->{$name}=implode(',',$value);						
					else
						$profile->{$name}=$value;						
					
				}
			}
		}
		$profile->save();  // update profile if exists
		return $this;
	}
	
	/*
		This method is used in /app/profile/ and updates the users table as well as the profile table. Need to check this is still working
		after the addition of  'Which statement best describes you'. I suspect it might not be TODO:
	*/
	public function editProfile($userId, $profileData)
	{
		$this->updateProfile($userId, $profileData); // update profile data
		$users=new Users();
		$users->editProfile($userId, $profileData); // update Users table
		return $this;
	}
	
	public function countNewTesters($startDate, $endDate)
	{
		//This function will execute the following SQL statement using the Zend Framework
		//SELECT COUNT(pt.id)AS 'total_testers' FROM profile_tester AS pt 
		//JOIN users AS u ON pt.id=u.id 
		//WHERE u.registered_on > $startDate AND u.registered_on < $endDate;
		
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("pt" => 'profile_tester');
		$columns = array("total_testers" =>'COUNT(pt.id)');
		$statement = $select->from($tableInfo, $columns)
			->join(array("u" => 'users'), 'u.id = pt.id', array())
			->where("u.registered_on >= ?", $startDate)
			->where("u.registered_on <= ? ", $endDate);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['total_testers'];
	}
	
	public function averageDailyNewTesters($startDate, $endDate)
	{
		$cat=$this->countNewTesters($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/86400;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function averageWeeklyNewTesters($startDate, $endDate)
	{
		$cat=$this->countNewTesters($startDate,$endDate);
		$dog=(strtotime($endDate)-strtotime($startDate))/604800;
		if($dog==0)
		{
			$dog=1;
		}
		return $cat/$dog;
	}
	
	public function countAllTesters()
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("pt" => 'profile_tester');
		$columns = array("total_testers" =>'COUNT(pt.id)');
		$statement = $select->from($tableInfo, $columns);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['total_testers']+2044;
		
	}
	
	/*
	 * This will just check to see if a tester can receive project messages instantly or not
	 */
	public function canSendProjectMessages($id)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$select->from($this, array('COUNT(id) AS count'))
			->where("notify_email LIKE '%3%'")
			->where("id = ?",$id);
			
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		//This will either return 0 or 1.  If the count is 1 we can send an email to the user
		return $row['count'];
	}

	/*
	 * This is a hackish method to import a user survey into his profile
         */
	public function importFromSurvey($id)
	{
		$_SURVEY=2;
		$response=new Response();
		$select=$response->select();
		$select->where('response.idsurvey= ?', $_SURVEY)
		  ->where('response.iduser = ?', $id)
		  ->order('response.idresponse');
		$rows=$response->fetchAll($select);
	  
		$contactinfo=array(); // stuff to go into profile_contactinfo
		$regdevice=array('idtester'=>$id); // stuff to go in regdevices
		$mobsterinfo=array(); // stuff to go into profile_mobster

		foreach($rows as $row)
		{
			// The first few questions relate to the device and carrier and as such should create an entry in rgedevices
			switch($row->idquestion)
			{
				case 55: //country
					$profilecontactinfo['idcountry']=$row['text'];
					break;
				case 56: //network
					$regdevice['idnetwork']=$row['text'];
					break;
				case 57: // Vendor - we don't need this
					break;
				case 58: //device
					$regdevice['deviceatlasid']=$row['text'];
					break;
				case 59: // uuid, imei whatever (why the fuck is this called mac_id?)
					$regdevice['mac_id']=$row['text'];
					break;
				case 66:
					$mobsterinfo['nda']=(int)($row['text']=='on'); // this will be either 0 or 1 
					break;
				case 68: // dob
					$mobsterinfo['bdate']=$row['text'];
					break;
				default:
					// In all other cases the table name is in questiongroup (from answer table)
					if(!$row['idanswer']) {
						$answers=$row['text'];
					}
					else {
						$answers=$row['idanswer'];
					}
					$qa=new QuestionAnswer();
					$qasel=$qa->select();
					$qasel->setIntegrityCheck(false);
					$qasel->from($qa)
						->join(array('a'=>'answer'), 'a.idanswer=question_answer.idanswer', array('a.answergroup as qcol', 'a.value as aval'))
						->where('question_answer.idquestion= ?', $row['idquestion'])
						->where("a.idanswer in ($answers)");
					fb($qasel->__toString());

					$qarows=$qa->fetchAll($qasel);
					$aval='';
					foreach($qarows as $qarow) {
						$qcol=$qarow['qcol'];
						$aval.=$qarow['aval'].',';
					}
					$mobsterinfo[$qcol]=substr($aval,0,-1); // strip off final comma

			}
	    
		}
		fb(print_r($mobsterinfo,true));
		$this->updateProfile($id, $mobsterinfo);
		$pci=new ProfileContactInfo();
		$pci->updateProfile($id, $contactinfo);
		$regdevices=new Regdevices();
		$regdevices->insert($regdevice);
	}
}