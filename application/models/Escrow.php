<?php

class Escrow extends Zend_Db_Table_Abstract  
{
	protected $_name = 'escrow';

	//These are the offical transaction codes for the status in the system.
	public static $transactioncodes=array(
										0 => 'Paypal deposit',
										1 => 'Paypal withdrawal',
										2 => 'escrow in - pay for test',
										3 => 'escrow out - get paid for test',
										4 => 'escrow out - escrow returned to developer',
										5 => 'premium commision fee',
										6 => 'managed services fee',
										7 => 'error adjustment payment' , 
										8 => 'transfer funds', 
										9 => 'manual deposit', 
										10 => 'manual withdrawal'
									);
//	`idtest` int(10) unsigned DEFAULT NULL
//  `idtester` int(10) unsigned DEFAULT NULL
//  `iddeveloper` int(10) unsigned DEFAULT NULL
//  `bidAmount` decimal(7,2) DEFAULT NULL
//  `status` int(10) DEFAULT NULL
//  `commission` decimal(7,2) DEFAULT NULL
//  `salestax` decimal(7,2) DEFAULT NULL
//  `timestamp` int(10) unsigned DEFAULT NULL
//  `idescrow` int(10) unsigned NOT NULL AUTO_INCREMENT
//  `transactionid` int(10) unsigned NOT NULL

	public function escrowIn($idtest, $idtester, $iddeveloper, $bidAmount, $commission, $salestax, $transactionid)
	{
		$timestamp=time();
		$insertArray = array('idtest'=>$idtest, 'idtester'=>$idtester, 'iddeveloper'=>$iddeveloper, 
			'bidAmount'=>$bidAmount, 'status'=>2, 'commission'=>$commission, 'salestax'=>$salestax,
			'timestamp'=>$timestamp, 'transactionid'=>$transactionid);
		$this->insert($insertArray);
	}
	
	public function escrowOutPayTest($idtest, $idtester, $iddeveloper, $bidAmount, $commission, $salestax, $transactionid)
	{
		$timestamp=time();
		$insertArray = array('idtest'=>$idtest, 'idtester'=>$idtester, 'iddeveloper'=>$iddeveloper, 
			'bidAmount'=>$bidAmount, 'status'=>3, 'commission'=>$commission, 'salestax'=>$salestax,
			'timestamp'=>$timestamp, 'transactionid'=>$transactionid);
		$this->insert($insertArray);
	}
	
	public function escrowOutReturnEscrow($idtest, $idtester, $iddeveloper, $bidAmount, $commission, $salestax, $transactionid)
	{
		$timestamp=time();
		$insertArray = array('idtest'=>$idtest, 'idtester'=>$idtester, 'iddeveloper'=>$iddeveloper, 
			'bidAmount'=>$bidAmount, 'status'=>4, 'commission'=>$commission, 'salestax'=>$salestax,
			'timestamp'=>$timestamp, 'transactionid'=>$transactionid);
		$this->insert($insertArray);
	}
	

	public function getFundsInEscrow($idproject=0)
	{
		$in = $this->getEscrowByStatus(2, $idproject);
		$out1 = $this->getEscrowByStatus(3, $idproject);
		$out2 = $this->getEscrowByStatus(4, $idproject);
		$total = $in - $out1 - $out2;
		return $total;
	}
	
	public function getEscrowByStatus($status, $idproject=0)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		
		if($idproject==0)
		{
			$columns = array("sum" =>'sum(bidAmount)+sum(commission)+sum(salesTax)');
			$statement = $select->from($this, $columns)
				->where("status = ?", $status);
		}
		else
		{
			$columns = array("sum" =>'sum(escrow.bidAmount)+sum(escrow.commission)+sum(escrow.salesTax)');
			$statement = $select->from($this, $columns)
				->join(array('tests'=>'tests'), 'escrow.idtest=tests.idtest', array())
				->where("escrow.status = ?", $status)
				->where("tests.idproject = ?",$idproject);
		}
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
	
	public function getBidByStatus($status, $idproject=0)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		
		if($idproject==0)
		{
			$columns = array("sum" =>'sum(bidAmount)');
			$statement = $select->from($this, $columns)
				->where("status = ?", $status);
		}
		else
		{
			$columns = array("sum" =>'sum(escrow.bidAmount)');
			$statement = $select->from($this, $columns)
				->join(array('tests'=>'tests'), 'escrow.idtest=tests.idtest', array())
				->where("escrow.status = ?", $status)
				->where("tests.idproject = ?",$idproject);
		}
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
	
	public function getCommissionByStatus($status)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);	
		if($idproject==0)
		{
			$columns = array("sum" =>'sum(commission)');
			$statement = $select->from($this, $columns)
				->where("status = ?", $status);
		}
		else
		{			
			$columns = array("sum" =>'sum(escrow.commission)');
			$statement = $select->from($this, $columns)
				->join(array('tests'=>'tests'), 'escrow.idtest=tests.idtest', array())
				->where("escrow.status = ?", $status)
				->where("tests.idproject = ?",$idproject);
		}
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
	
	public function getTaxByStatus($status)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);

		if($idproject==0)
		{
			$columns = array("sum" =>'sum(salesTax)');
			$statement = $select->from($this, $columns)
				->where("status = ?", $status);
		}
		else
		{			
			$columns = array("sum" =>'sum(escrow.salesTax)');
			$statement = $select->from($this, $columns)
				->join(array('tests'=>'tests'), 'escrow.idtest=tests.idtest', array())
				->where("escrow.status = ?", $status)
				->where("tests.idproject = ?",$idproject);
		}
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
	
	public function getProfitOverRange($startDate, $endDate)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array("sum" =>'sum(commission)');
		$statement = $select->from($this, $columns)
			->where("status = 3")
			->where("timestamp >= ?", strtotime($startDate))
			->where("timestamp <= ? ", strtotime($endDate));;
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['sum'];
	}
}