<?php

class Transactions extends Zend_Db_Table_Abstract  
{
	protected $_name = 'transactions';
    
	
	public function add($idtest, $idtester, $status, $bidAmount, $timestamp, $comments='')
	{ 
		$nt = array(
			'idtest' => $idtest,
			'idtester' => $idtester,
			'Status' => $status,
			'Timestamp' => time(),
			'BidAmount' => $bidAmount,
			'Timestamp' => $timestamp,
			'Comments' => $comments
		);
                
		$tId = $this->insert($nt);
	}
	public function countTransactionsByStatus($startDate, $endDate, $status)
	{
		//SELECT COUNT(t.idtransaction) AS 'count' FROM transactions AS t 
		//WHERE t.timestamp > ? AND t.timestamp < ? AND t.status=?;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'transactions');
		$columns = array("count" =>'COUNT(t.idtransaction)');
		$statement = $select->from($tableInfo, $columns)
			->where("t.timestamp >= ?", strtotime($startDate))
			->where("t.timestamp <= ? ", strtotime($endDate))
			->where("t.status = ?",$status);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countTransactionsByStatusByProject($status,$idproject,$timestamp=0)
	{
		//SELECT COUNT(t.idtransaction) AS 'count' FROM transactions AS t 
		//WHERE t.timestamp > ? AND t.timestamp < ? AND t.status=?;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'transactions');
		$columns = array("count" =>'COUNT(t.idtransaction)');
		$statement = $select->from($tableInfo, $columns)
			->join(array('test'=>'tests'),'t.idtest=test.idtest',array())
			->where("t.status = ?",$status)
			->where("test.idproject = ?",$idproject)
			->where("t.Timestamp > ?",$timestamp);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countTransactionsByStatusByProjectAndTester($status,$idproject,$idtester, $timestamp=0)
	{
		//SELECT COUNT(t.idtransaction) AS 'count' FROM transactions AS t 
		//WHERE t.timestamp > ? AND t.timestamp < ? AND t.status=?;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'transactions');
		$columns = array("count" =>'COUNT(t.idtransaction)');
		$statement = $select->from($tableInfo, $columns)
			->join(array('test'=>'tests'),'t.idtest=test.idtest',array())
			->where("t.status = ?",$status)
			->where("test.idproject = ?",$idproject)
			->where("test.idtester = ?",$idtester)
			->where("t.Timestamp > ?",$timestamp);
	
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	public function countTesterActivityByStatus($startDate, $endDate, $status)
	{
		//SELECT COUNT(t.idtransaction) AS 'count' FROM transactions AS t 
		//WHERE t.timestamp > ? AND t.timestamp < ? AND t.status=?;
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$tableInfo = array("t" => 'transactions');
		$columns = array("count" =>'COUNT(DISTINCT(t.idtester))');
		if($status==-1)
		{
			$statement = $select->from($tableInfo, $columns)
				->where("t.timestamp >= ?", strtotime($startDate))
				->where("t.timestamp <= ? ", strtotime($endDate));
			
		}
		else
		{
			$statement = $select->from($tableInfo, $columns)
				->where("t.timestamp >= ?", strtotime($startDate))
				->where("t.timestamp <= ? ", strtotime($endDate))
				->where("t.status = ?",$status);
		}
			
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['count'];
	}
	
	/*
	 * This is used only by UpdateController::fixtesttimestampAction
	 * Once used please delete
	 */
	public function getMaxTimestamp($idtest)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array("maxTime" =>'MAX(Timestamp)');
		$statement = $select->from($this, $columns)
			->where("idtest = ?",$idtest);
		$rows=$this->fetchAll($select);
		$row=$rows->current();
		return $row['maxTime'];	
	}
	
	
}