<?php
class Branch extends Zend_Db_Table_Abstract  
{
	protected $_name = 'branch';
    								
	protected $_dependentTables = array('SurveyQuestion');

	/*
	 * How to read the branch entires. For idsurvey_question of what ever it is, if the idanswer is the 
	 * the one in the table row go to idsurvey_question listed in branchto.  If there is not entry in this
	 * table the go to the next question in a survey_question table.
	 */
	
	public function addNewBranch($idsurvey_question,$idanswer,$branchto)
	{
		$data = array('idsurvey_question'=>$idsurvey_question,'idanswer'=>$idanswer,
			'branchto'=>$branchto);
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}
	
	/*
	 * This function is used to copy the branching information from an existing survey
	 * It is used to duplicate the survey into a new survey.
	 */
	public function copyAnotherSurveyBranch($newSurveyID,$oldSurveyID)
	{
		$select=$this->select();
		$select->setIntegrityCheck(false);
		$columns = array('idanswer','branchto');
		$statement = $select->from($this,$columns)
			->join(array('survey_question'=>'survey_question'),'branch.idsurvey_question=survey_question.idsurvey_question',array('idquestion'))
			->where("survey_question.idsurvey = ?",$oldSurveyID);
		
		$rows= $this->fetchAll($select);
		
		$surveyQuestion=new SurveyQuestion();
		foreach($rows as $row)
		{
			$idquestion=$row['idquestion'];
			$idsurvey_question=$surveyQuestion->getIDSurveyQuestionByIdSurveyAndIDQuestion($newSurveyID,$idquestion);
			$idanswer=$row['idanswer'];
			$tempRow=$surveyQuestion->getSurveyQuestion($row['branchto']);
			$tempID=$tempRow['idquestion'];
			$branchto=$surveyQuestion->getIDSurveyQuestionByIdSurveyAndIDQuestion($newSurveyID,$tempID);
			$this->addNewBranch($idsurvey_question,$idanswer,$branchto);
		}
	}
}