<?php

class Lib_Controller_Plugin_Header extends Zend_Controller_Plugin_Abstract
{
    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback allows for proxy or filter behavior.  By altering the
     * request and resetting its dispatched flag (via
     * {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
     * the current action may be skipped.
     *
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
		// Get the dynamic values to output in the header
		$view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view;
        
		$regdevices=new Regdevices();
		$view->handsetCount=$regdevices->count();
		$projects= new Projects();
		$view->projectCount=$projects->count();
		
		// Set the currentPage value so we know which left hand menu to highlight
		$view->currentPage=$request->getActionName();
		       
		// Make the User Info in the session available easily everywhere
		if(($currentUser=Zend_Registry::get('defSession')->currentUser))
			$view->currentUser=Zend_Registry::get('defSession')->currentUser;
		
		
		$view->configuration = Zend_Registry::get('configuration');
  
		$view->loginForm=new Form_Login();
		// Set the default page title - can be changed elsewhere
		$view->headTitle("Mob4Hire: Mobile Testing, Usability, Market Research");
    }
}