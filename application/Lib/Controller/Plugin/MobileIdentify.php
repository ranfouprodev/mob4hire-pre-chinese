<?php

class Lib_Controller_Plugin_MobileIdentify extends Zend_Controller_Plugin_Abstract
{
    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback identifies if the request is coming from a mobile device. 
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view;

		if(isset(Zend_Registry::get('defSession')->mobile)) {
			// We have already done identification. No need to do it again
			if($request->getActionName()=='fullsite') {
				Zend_Registry::get('defSession')->mobile=0; // This is to prevent mobile devices from redirecting to mobile site when full site selected.
				$request->setModuleName('index')->setControllerName('index')->setActionName('index');
			}
//			elseif(Zend_Registry::get('defSession')->mobile) {
//				$request->setModuleName('mobile')->setControllerName($request->getControllerName())->setActionName($request->getActionName());
//			}
		}
		else
		{
			if(!empty($_SERVER['HTTP_USER_AGENT'])){

				$devices = new Devices();
				if($device = $devices->identifyDevice($_SERVER['HTTP_USER_AGENT'])){
					Zend_Registry::get('defSession')->mobile=$view->mobile=$device;
					$request->setModuleName('mobile')->setControllerName('index')->setActionName('index');
				}else{
					Zend_Registry::get('defSession')->mobile=0;
				}
			}
		}
	}
}