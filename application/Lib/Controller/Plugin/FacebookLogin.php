<?php
/*
 * Facebook Connect Section
 */
require_once 'facebook-platform/php/facebook.php';


class Lib_Controller_Plugin_FacebookLogin extends Zend_Controller_Plugin_Abstract
{

    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback allows for checking the facebook login and injecting the required scripts
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $facebook = new Facebook('3417a1664e1f729761c8b6014fc7f61f', '1edbf13ee84f91669fcbdf41ba653d67');

        // Get the dynamic values to output in the header
        $view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view;

        $fb_user = $facebook->get_loggedin_user();
		
		

        // Ignore if the user is logged in
        if (!Zend_Registry::get('defSession')->currentUser && $fb_user ) {

			$hasLogin = $this->login($fb_user);

            if (!$hasLogin) {
                // We have a logged in user but no account created?
                $usersTable = new Users();
                $user_details = $facebook->api_client->users_getInfo($fb_user, array ('last_name', 'first_name', 'proxied_email'));

                $username = $user_details[0]['first_name'].$user_details[0]['last_name'];
                // Check if the username exists
                while ($usersTable->hasUsername($username)) {
                    $username = $user_details[0]['first_name'].$user_details[0]['last_name'].rand(1000, 9999);
                }

                try {
                    $newUserId = $usersTable->addFB($fb_user, $username, $user_details[0]['first_name'], $user_details[0]['last_name'], $user_details[0]['proxied_email']);
                }
                catch(Zend_Db_Statement_Exception $e) {
                    // Don't know how to handle this...hmm.
                }

				$this->login($fb_user);
            }
			
			$view->facebookUser = Zend_Registry::get('defSession')->facebookUser;

        }


    } // end logged in if





    protected function login($fb_user) {

        // check to see if account id already exists, if not redirect
        $auth = Zend_Auth::getInstance();

        $db = Zend_Registry::get('db');
        $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'idfacebook', 'idfacebook');
        $authAdapter->setIdentity($fb_user)->setCredential($fb_user);
        $authResult = $auth->authenticate($authAdapter);
        if ($authResult->isValid()) {
		//valid username and password
		$userInfo = $authAdapter->getResultRowObject();
		if ($userInfo->active) {
			//save userinfo in session
			$userInfo->password = '';
			Zend_Registry::get('defSession')->currentUser = $userInfo;
			Zend_Registry::get('defSession')->facebookUser = $fb_user;
			// Write last_login info to db
			$users = new Users(); 
			$now = new Zend_Date(); 
							
			$userData = array('last_login' =>  $now->toString('YYYY-MM-dd HH:mm:ss') );
			$users->editProfile($userInfo->id, $userData);
		
			return true;

		} else {
			return false;
		}
        }else{
		return false;
        }
		

    }

}
