<?php

class Lib_Controller_Plugin_ACL extends Zend_Controller_Plugin_Abstract
{
	/**
	* Called before an action is dispatched by Zend_Controller_Dispatcher.
	*
	* This callback allows for proxy or filter behavior.  By altering the
	* request and resetting its dispatched flag (via
	* {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
	* the current action may be skipped.
	*
	* In this version we have only one rule - for access to 'admin' module we require 'isAdmin'
	* flag of the user to be set to true (this is very simple, but for our current needs is enough)
	*
	* @param  Zend_Controller_Request_Abstract $request
	* @return void
	*/
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$module = $request->getModuleName();
		$controller = $request->getControllerName(); 
		$action = $request->getActionName(); 
	
		if ($module == 'admin'  && (!Zend_Registry::get('defSession')->currentUser || !Zend_Registry::get('defSession')->currentUser->isAdmin))
		{
			// redirect to index if do not have access
			// (possible to redirect to some 'access denied' page if needed)
			$request->setModuleName('default')->setControllerName('index')->setActionName('index');
		}

		if($module == 'app' && !(Zend_Registry::get('defSession')->currentUser))
		{
			$request->setModuleName('default')->setControllerName('index')->setActionName('index');
		}
		else if($module == 'survey' && !(Zend_Registry::get('defSession')->currentUser))
		{
			
			$resourceName = $controller .":". $action;
			
			$openAccess = array('survey:vanilla');
				
			$openController = array('vanilla');
				
			//if(!in_array($resourceName, $openAccess)) && 
			if(!in_array($controller,$openController)){
				$request->setModuleName('mobile')->setControllerName('user')->setActionName('login'); // todo: switch this for mobile login
			}
			
		}
	
		// Mobile ACL
		else if($module == 'mobile' && !(Zend_Registry::get('defSession')->currentUser))
		{
			$resourceName = $controller .":". $action;
		
			$openAccess = array('index:terms','index:index', 'user:login','user:register','user:forgotpassword','user:logout','user:activate','user:justRegistered');  
			
			$openController = array('services');
			
			if(!in_array($resourceName, $openAccess) && !in_array($controller,$openController)){
				$request->setModuleName('mobile')->setControllerName('user')->setActionName('login'); // todo: switch this for mobile login
			}
		}
	
		else 
		{	
			// handle redirects to give correct views of projects/surveys depending who we are
		
		}
	}
}