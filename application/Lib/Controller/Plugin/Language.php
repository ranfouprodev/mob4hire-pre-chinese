<?php

class Lib_Controller_Plugin_Language extends Zend_Controller_Plugin_Abstract
{


	public function routeStartup(Zend_Controller_Request_Abstract $request)
	{
		$lang = $request->getParam('lang', null);

		if(isset($lang)){
			$session = Zend_Registry::get('defSession');
			$session->language = $lang;
		}

	}


  	/**
	 * Called before Zend_Controller_Front enters its dispatch loop.
	 *
	 * @param  Zend_Controller_Request_Abstract $request
	 * @return void
	 */
	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		$session = Zend_Registry::get('defSession');

		$translate = Zend_Registry::get('Zend_Translate');

		$configuration = Zend_Registry::get('configuration');

		// default language when requested language is not available
		$defaultlanguage = $configuration->general->language;

		if (isset($session->language) && Zend_Locale::isLocale($session->language)) {
			// change locale for the application
			$locale = new Zend_Locale($session->language);

			if($translate->getAdapter()->isAvailable($locale->getLanguage())){
				$translate->getAdapter()->setLocale($locale->getLanguage());
			}else{
				$translate->getAdapter()->setLocale($defaultlanguage);
			}

			Zend_Registry::set(
				'Zend_Locale',
				$locale
			);

			// change language for the translator
			Zend_Registry::get('Zend_Translate')->setLocale($locale);
		} else {

			$locale = new Zend_Locale($defaultlanguage);


			// check if user language is translated
			if($translate->getAdapter()->isAvailable($locale->getLanguage())){
				// change language for the translator
				$translate->getAdapter()->setLocale($defaultlanguage);
			}
		}
	}
}