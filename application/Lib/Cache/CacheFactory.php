<?php
require_once "Lib/Cache/CacheManager.php";

class CacheFactory {
	
	static public function getCache( $objectType ) {

		$cache = Zend_Registry::get('cache');
		
		if ( !is_null($cache) ) {
			// Register this object type with the cache list
			if ( !$cache_list = $cache->load('cache_list') ) {
				$cache_list = array();
			}
			if ( !in_array($objectType, $cache_list )) {
				$cache_list[] = $objectType;
			}
			
			$cache->save($cache_list, 'cache_list' );
		}		
		$cacheManager = new CacheManager( $objectType );		
		
		return $cacheManager;
	}
}