<?php

class CacheManager {
	
	private $objectType; 
	private $cache;
	
	public function __construct( $objectType ) {
		$this->objectType = $objectType;
		$this->cache = Zend_Registry::get('cache');
	}
	
	public function get( $element ) {
		
		$element = preg_replace('/[^A-Za-z0-9]/', '_', $element);
		
		if ( !is_null($this->cache) ) {
			$value = NULL;
			if ( !$value = $this->cache->load($this->objectType . '_' . $element) ) {
				// Increment cache-misses
				$this->incrementMisses($this->objectType, $element);

			} else {
				// Increment cache-hits
				$this->incrementHits($this->objectType, $element );
			}
			
			return $value;
		}
		
		return false;
	}
	
	public function setE($element, $value, $elapsedTime ) {

		$element = preg_replace('/[^A-Za-z0-9]/', '_', $element);
		
		if ( !is_null($this->cache) ) {		
			$this->cache->save($value, $this->objectType . '_' . $element );
			$cacheKey = 'cache_' . $this->objectType . '_miss_time';
			if ( !$missTimes = $this->cache->load($cacheKey) ) {
				$missTimes = array();
			}
			$missTimes[$element] = $elapsedTime;
			$this->cache->save( $missTimes, $cacheKey );
		}
	}
	
	public function set( $element, $value ) {
		if ( !is_null($this->cache) ) {
			$element = preg_replace('/[^A-Za-z0-9]/', '_', $element);
			$this->cache->save($value, $this->objectType . '_' . $element );
		}
	}

	public function remove( $element ) {
		if ( !is_null($this->cache) ) {
			$element = preg_replace('/[^A-Za-z0-9]/', '_', $element);
			$this->cache->remove($this->objectType . '_' . $element );
		}
	}
	
	
	private function incrementMisses( $objectType, $element ) {
		
		$cacheKey = 'cache_' .$objectType . '_misses';
		if ( !$misses = $this->cache->load($cacheKey ) ) {
			$misses = array();	
		}
		if ( !isset($misses[$element] ) ) {
			$misses[$element] = 0;
		}
		$misses[$element] = $misses[$element] + 1;
		$this->cache->save($misses, $cacheKey);
	}
	
	private function incrementHits( $objectType, $element ) {
		
		$cacheKey = 'cache_' . $objectType . '_hits';
		
		if ( !$hits = $this->cache->load($cacheKey) ) {			
			$hits = array();
		}
		
		if ( !isset($hits[$element])) {
			$hits[$element] = 0;
		}
		$hits[$element] = $hits[$element] + 1;

		$this->cache->save($hits, $cacheKey );		
	}	
	
	public function clean(){
		if ( !is_null($this->cache) ) {
			//$this->cache(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,array($this->objectType."_"));	
			$this->cache(Zend_Cache::CLEANING_MODE_ALL);
		}
	}
}