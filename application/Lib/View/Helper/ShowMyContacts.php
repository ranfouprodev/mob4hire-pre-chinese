<?php
class Zend_View_Helper_ShowMyContacts
{
	public $view;

	function showMyContacts( $pageNumber=1, $resultsPerPage=4, $options=array()) 
	{

		$translate = Zend_Registry::get('Zend_Translate');

		if(!array_key_exists("showcomments", $options))
			$options['showcomments'] = false;
			
		if(!array_key_exists("navigation", $options))
			$options['navigation'] = 'none';
			
		if(!array_key_exists("selector", $options))
			$options['selector'] = 'none';
			
		if(!array_key_exists("layout", $options))
			$options['layout'] = "standard";

		$this->view->headScript()->appendScript('var page='.$pageNumber.';var resultsPerPage = '.$resultsPerPage.';var selector = "'.$options['selector'].'";var showcomment = "'.$options['showcomments'].'";var showcomment = "'.$options['showcomments'].'";var layout = "'.$options['layout'].'";');	
			
		// Contact details	
		$contacts=new Contacts();
		$contactList=$contacts->listContactsByUser(Zend_Registry::get('defSession')->currentUser->id, $pageNumber, $resultsPerPage);	
		
		// Shows all the projects based on the listprojectsajax
		$return='<div class="contact-listing"><fieldset id="group">';
				
		
		if($options['selector'] != 'none')
			$return .=$this->view->partial('contacts/_contacts_selector.phtml','app',array('options'=>$options));
		
		if(count($contactList)==0){
			// Render the no results page
			$return .=$this->view->partial('contacts/_contacts_error.phtml','app');
		}else{
		
			//if($options['navigation'] == "top")
			//	$return .= $this->view->partial('partial/_project_navigation.phtml', array("order"=>$orderBy));
		
			foreach($contactList as $contact){	
				// 	Handset Details
				$regdevices=new Regdevices();
				$handsets=$regdevices->getHandsetsByUser($contact->iduser);
				
				$return .=$this->view->partial('contacts/_layout.phtml','app', array('contact'=>$contact,'handsets'=>$handsets, 'options'=>$options) );
			}
			 
			// Add pagination
			if($resultsPerPage != 0){
				$posts = (int)($contacts->count(Zend_Registry::get('defSession')->currentUser->id));
				
				$pages = ceil($posts / $resultsPerPage);
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
				
				$return .= $this->view->partial('contacts/_contacts_pagination.phtml','app', array("pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious));
			}
		}
		$return.='</fieldset></div>';
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}