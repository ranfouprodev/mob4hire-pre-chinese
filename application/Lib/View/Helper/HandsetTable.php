<?php

class Lib_View_Helper_HandsetTable extends Zend_View_Helper_Abstract
{
	function handsetTable($idproject)
	{
		$projects=new Projects();
		$handsettable=$projects->getSupportedDevices($idproject);
		$output="<table class='subtable'>
					<thead>
					</thead>
					<tfoot>
					</tfoot>
					<tbody>";
		$i=0; // used to swap background colour of table row 
		if(!$handsettable->count())
		{
			$output.="<tr class='".($i%2 ? 'row_even' : 'row_odd')."'>";
			$output.="<td>Any</td>";
			$output.="</tr>";
		}
		else
		{
			foreach($handsettable as $handset) 
			{
				$output.="<tr class='".($i%2 ? 'row_even' : 'row_odd')."'>";
				$output.="<td>".substr($handset->platform,2).' '.(($handset->vendor)? $handset->vendor : '').' '.(($handset->model)? $handset->model : 'Any')."</td>";
				$output.="</tr>";
				$i++;
			}
		}
		$output.="	</tbody>
				</table>";

		return $output;
	}
}

?>