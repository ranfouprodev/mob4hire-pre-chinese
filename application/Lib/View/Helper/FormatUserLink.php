<?php
// This should eventually be moved into the model! Why?

class Zend_View_Helper_FormatUserLink
{
	function formatUserLink($username,$layout="standard") 
	{
		if($layout == "compact"){
			$link="<a rel='external' style='color:black' href='/mobile/profile/view/user/$username'>$username</a>";
		}else{
			$link="<a href='/app/profile/view/user/$username'>$username</a>";	
		}
		
		return $link;
	}
}