<?php

class Zend_View_Helper_ShowDailyDigestWall
{
	public $view;
	
	function showDailyDigestWall($iduser, $idtarget,$postType,$pageNumber=1, $resultsPerPage=100, $timestamp=0,$where="",$filter="none") 
	{
		
	  	$return='<div id="wall">';
		
		$posts = 0;

		$wallDto = new Wall();

		if($postType == 4){

		
			$walls = $wallDto->getUnifiedWall($iduser, $pageNumber, $resultsPerPage, $timestamp,$where);
			
			$return.='<div id="wallpost-thread-top-3-'.$this->view->currentUser->id.'"></div>';
			
			foreach($walls as $wall){			
							
					if($wall['idproject'] <> ''){
					$idtarget = $wall->idproject;
					$postType = 1;
	
				}elseif($wall['idtest'] <> ''){
					$idtarget = $wall['idtest'];
					$postType = 2;
				}else{
					$idtarget = Zend_Registry::get('defSession')->currentUser->id;
					$postType = 3;
				}
				$return.=$this->view->partial('wall/_email_layout.phtml','app', array('wall'=>$wallDto->getWallThread($iduser,$wall['idparent']),'postType'=>$postType,'idtarget'=>$idtarget));
						
			}
			
			$posts = (int)$wallDto->countThreads($iduser, 4, $timestamp,$iduser,$where);
		
			if($filter == "new")
				$posts = $this->view->unread;
			
			$idtarget = 0;
			$postType = 4;
			
			
		}elseif($postType == 5){
			
			
			$return.='<div id="wallpost-thread-top-'.$postType.'-'.$idtarget.'"></div>';
			$return.=$this->view->partial('wall/_email_layout.phtml','app', array('wall'=>$wallDto->getWallThread($iduser,$idtarget),'postType'=>$postType,'idtarget'=>$idtarget));
			
			$posts = 1;
			
		}else{
			
					
			$walls= $wallDto->getWall($idtarget, $postType, $iduser, $pageNumber, $resultsPerPage, $timestamp, $where);
			$posts = (int)$wallDto->countThreads($idtarget, $postType, $timestamp,$iduser,$where);
			
			$return.='<div id="wallpost-thread-top-'.$postType.'-'.$idtarget.'"></div>';
			 
			foreach($walls as $wall){			
				$return.=$this->view->partial('wall/_email_layout.phtml','app', array('wall'=>$wallDto->getWallThread($iduser,$wall['idparent']),'postType'=>$postType,'idtarget'=>$idtarget));
			}
		
		}

			
		$return.='</div>';
			
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}