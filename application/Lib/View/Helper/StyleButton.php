<?php

class Zend_View_Helper_StyleButton
{

	function styleButton($text,$href="javascript:void(0);",$onclick='',$id="sbutton",$class="sbutton-active") 
	{
		
		$return ="<div class='".$class."' id='".$id."'>";
		$return .="<a href='".$href."'";
		
		if(isset($onclick) && $onclick != "")
			$return .=" onclick='".$onclick."'";
		
		$return .=">";
		
		$return .= $text;

		$return .="</a><span></span></div>";
		
		return $return;
	}


}