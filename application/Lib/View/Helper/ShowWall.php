<?php

class Zend_View_Helper_ShowWall
{

	public $view;
	// showcomment, where and filter should be moved into the options array 
	function showWall($idtarget, $postType, $showcomment=false, $pageNumber=1, $resultsPerPage=4, $timestamp=0,$where="",$filter="none", $options=array()) 
	{

        $translate = Zend_Registry::get('Zend_Translate');

		// Add the javascript to initialize the wall
		$name = $postType .'_'. $idtarget;
		
		$this->view->headScript()->appendScript("var wall".$name." = {'container':'wall-container-".$name."'
						,'page':".$pageNumber.",'resultsPerPage':".$resultsPerPage.",'type':".$postType."
						,'target':".$idtarget.",'comment':".($showcomment?"true":"false")."
						,'loadingText':'".$translate->_('app_wall_getting_posts')."'
						,'filter':'".$filter."'}", $type = 'text/javascript', $attrs = array());
		

		if(!array_key_exists("layout", $options))
			$options['layout'] = "standard";
		
		$return='<div id="wall">';
		
		$posts = 0;

		$wallDto = new Wall();

		if($postType == 4){

			// Unified wall comments go to the private posting
			if($showcomment==true){
				$return.=$this->view->partial('wall/_newmessage.phtml','app', array('options'=>$options,'postType'=>3,'idtarget'=>$this->view->currentUser->id));
			}
			
			$walls = $wallDto->getUnifiedWall(Zend_Registry::get('defSession')->currentUser->id, $pageNumber, $resultsPerPage, $timestamp,$where);

			// fb(print_r($walls, true));
			$return.='<div id="wallpost-thread-top-3-'.$this->view->currentUser->id.'"></div>';
			
			foreach($walls as $wall){			
							
					if($wall['idproject'] <> ''){
					$idtarget = $wall->idproject;
					$postType = 1;
	
				}elseif($wall['idtest'] <> ''){
					$idtarget = $wall['idtest'];
					$postType = 2;
				}else{
					$idtarget = Zend_Registry::get('defSession')->currentUser->id;
					$postType = 3;
				}
				$return.=$this->view->partial('wall/_layout.phtml','app', array('options'=>$options,'wall'=>$wallDto->getWallThread($this->view->currentUser->id,$wall['idparent']),'postType'=>$postType,'idtarget'=>$idtarget));
						
			}
			
			$posts = (int)$wallDto->countThreads($this->view->currentUser->id, 4, $timestamp,$this->view->currentUser->id,$where);
		
			if($filter == "new")
				$posts = $this->view->unread;
			
			$idtarget = 0;
			$postType = 4;
			
			
		}elseif($postType == 5){
			
			// thread wall comments
			if($showcomment==true){
				$return.=$this->view->partial('wall/_newmessage.phtml','app', array('options'=>$options,'postType'=>$postType,'idtarget'=>$idtarget));
			}
			
			$return.='<div id="wallpost-thread-top-'.$postType.'-'.$idtarget.'"></div>';
			$return.=$this->view->partial('wall/_layout.phtml','app', array('wall'=>$wallDto->getWallThread($this->view->currentUser->id,$idtarget),'postType'=>$postType,'idtarget'=>$idtarget,'options'=>$options));
			
			$posts = 1;
			
		}else{
			
			if($showcomment==true){
				$return.=$this->view->partial('wall/_newmessage.phtml','app', array('options'=>$options,'postType'=>$postType,'idtarget'=>$idtarget));
			}
			
			$walls= $wallDto->getWall($idtarget, $postType, $this->view->currentUser->id, $pageNumber, $resultsPerPage, $timestamp, $where);
			$posts = (int)$wallDto->countThreads($idtarget, $postType, $timestamp,$this->view->currentUser->id,$where);
			
			$return.='<div id="wallpost-thread-top-'.$postType.'-'.$idtarget.'"></div>';
			 
			foreach($walls as $wall){			
				$return.=$this->view->partial('wall/_layout.phtml','app', array('wall'=>$wallDto->getWallThread($this->view->currentUser->id,$wall['idparent']),'postType'=>$postType,'idtarget'=>$idtarget,'options'=>$options));
			}
		
		}
				
	// Add pagination
			if($resultsPerPage != 0){
				
				$pages = ($posts / $resultsPerPage);
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
				
				$return .= $this->view->partial('wall/_wall_pagination.phtml','app', array("pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious,"wallname"=>$name));
			}
		
		
		$return.='</div>';
			
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}