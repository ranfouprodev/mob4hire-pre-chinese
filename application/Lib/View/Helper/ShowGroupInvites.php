<?php
class Zend_View_Helper_ShowGroupInvites
{
	public $view;

	function ShowGroupInvites( $idgroup, $order='', $pageNumber=1, $resultsPerPage=4, $options=array()) 
	{
		// sets the defaults for the options
		if(!array_key_exists("showcomments", $options))
			$options['showcomments'] = false;

		if(!array_key_exists("selector", $options))
			$options['selector'] = 'none';
			
		if(!array_key_exists("layout", $options))
			$options['layout'] = "standard";
			
			
		$container = "invites-container";	
			
		$groups = new Groups(); 
		
		$group = $groups->getGroup($idgroup);
			
		$groupList = $groups->listUsersForGroup($idgroup,"=100");
		
		$contactList = array(); 

		if($groupList->current()){
			$whereClause = "u.id in (";
					
			foreach ($groupList as $result) {
				
				$whereClause .= $result->iduser;
				$whereClause .= ',';
			}
				
			$whereClause = substr($whereClause,0,-1);
			$whereClause .= ")";

			// Contact details	
			$users=new Users();
			$contactList=$users->listUsersForSearch($whereClause, 'MONTH(last_login) desc',$pageNumber, $resultsPerPage);	
			
		}	
		
		
		// Shows all the projects based on the listprojectsajax
		$return='<div class="contact-listing"><fieldset id="group-inv">';
		
		if(count($contactList)==0){
			// Render the no results page
			$return .=$this->view->partial('contacts/_contacts_error.phtml',array('mode'=>'invites'));
		}else{
		
			if($options['selector'] != 'none')
				$return .=$this->view->partial('contacts/_contacts_group_invite_selector.phtml', array('group'=>$group));
		
			foreach($contactList as $contact){	
				// 	Handset Details
				$regdevices=new Regdevices();
				$handsets=$regdevices->getHandsetsByUser($contact->iduser);
				
				$return .=$this->view->partial('contacts/_layout.phtml', array('contact'=>$contact,'handsets'=>$handsets, 'options'=>$options) );
			}
			 
			// Add pagination
			if($resultsPerPage != 0){
								
				$posts = (int)$group->members;
						
				$pages = ($posts / $resultsPerPage);
				
				if($pages > 20)
					$pages = 20; 
				
				$showing = $pageNumber * $resultsPerPage;
						
				$hasNext = $posts > $showing;
				$hasPrevious = ($pageNumber > 1);
		
				$return .= $this->view->partial('contacts/_contacts_pagination.phtml', array("pages"=>$pages,"active"=>$pageNumber,"hasNext"=>$hasNext,"hasPrevious"=>$hasPrevious,"container"=>$container));
			}
		}
		$return.='</fieldset></div>';
		
		return $return;
		
	}

	public function setView(Zend_View_Interface $view)
	{
		$this->view = $view;
	}

}