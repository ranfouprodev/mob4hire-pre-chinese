<?php

class Lib_View_Helper_NetworkTable extends Zend_View_Helper_Abstract
{
	function networkTable($idproject)
	{
		$projects=new Projects();
		$networktable=$projects->getSupportedNetworks($idproject);
		$output="<table class='subtable'>
					<thead>
					</thead>
					<tfoot>
					</tfoot>
					<tbody>";
		$i=0; // used to swap background colour of table row 
		if(!$networktable->count())
		{
			$output.="<tr class='".($i%2 ? 'row_even' : 'row_odd')."'>";
			$output.="<td>Any</td>";
			$output.="</tr>";
		}
		else
		{
			foreach($networktable as $network) 
			{
				$output.="<tr class='".($i%2 ? 'row_even' : 'row_odd')."'>";
				$output.="<td>".$network->country.' '.(($network->network)? $network->network : 'Any')."</td>";
				$output.="</tr>";
				$i++;
			}
		}
		$output.="	</tbody>
				</table>";

		return $output;
	}
}

?>