<?php

class Zend_View_Helper_GetProfileImage
{
	// Returns no if argument is zero or NULL, yes otherwise
	function getProfileImage($profile) 
	{
		if($profile['image']){
		    $img_path = $profile['image'];
		}else{
			$img_path="/images/image-not-found.gif";
		}
		return '<img src=' . $img_path . ' alt="' . $profile['username'] . '" />';
 	}
}