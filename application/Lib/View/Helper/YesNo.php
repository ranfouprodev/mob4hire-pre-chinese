<?php

class Zend_View_Helper_YesNo
{
	// Returns no if argument is zero or NULL, yes otherwise
	function yesNo($arg) 
	{
		return (((int)$arg==1)? 'Yes' : 'No');
	}
}