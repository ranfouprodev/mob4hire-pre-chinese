<?php

define('TMP_FOLDER', realpath($_SERVER['DOCUMENT_ROOT'].'/../tmp'));


// For our dev environment we will report all errors to the screen    
if(strpos($_SERVER['HTTP_HOST'],"mob4hire.com") != FALSE)
{
	$production=true;
}
else
{
	$production=false;
}

// Set our timezone
date_default_timezone_set('UTC');


// Add /library and /application directory to our include path 
if($_SERVER['HTTP_HOST'] == "mob4hire.com" || $_SERVER['HTTP_HOST'] == "app.mob4hire.com")
{
	$siteRootDir = dirname($_SERVER['DOCUMENT_ROOT']). '/app/' ;
}
else
{
	$siteRootDir = dirname($_SERVER['DOCUMENT_ROOT']) ;
}



set_include_path(
    $siteRootDir . '/library' . PATH_SEPARATOR 
    . $siteRootDir . '/application' . PATH_SEPARATOR
    . $siteRootDir . '/application/models' . PATH_SEPARATOR 
    . $siteRootDir . '/application/Form' . PATH_SEPARATOR 
    . get_include_path()
);


require_once 'Zend/Cache.php';
// Turn on autoloading, so we do not include each Zend Framework class
require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Lib_');
$loader->setFallbackAutoloader(true);


// Create registry object and setting it as the static instance in the Zend_Registry class
$registry = new Zend_Registry();
Zend_Registry::setInstance($registry);

//save $siteRootDir in registry:
$registry->set('siteRootDir', $siteRootDir);

$_SERVER['SERVER_ENV'] = 'local';
// Default to development configurations
$config_path = '/configuration/development/config.ini';
if ( isset($_SERVER['SERVER_ENV']) ) {
	$config_path = '/configuration/' . $_SERVER['SERVER_ENV'] . '/config.ini';
}

// Load configuration file and store the data in the registry
$configuration = new Zend_Config_Ini($siteRootDir . $config_path, 'main');
Zend_Registry::set('configuration', $configuration);

// Construct the database adapter class, connect to the database and store the db object in the registry
$db = Zend_Db::factory($configuration->db);


$db->query("SET NAMES 'utf8'");
Zend_Registry::set('db', $db);
// set this adapter as default for use with Zend_Db_Table
Zend_Db_Table_Abstract::setDefaultAdapter($db);

/*
 * Add a second database connection to the sms database
 */
$dbsms = Zend_Db::factory($configuration->dbsms);
$dbsms->query("SET NAMES 'utf8'");
Zend_Registry::set('dbsms', $dbsms);


/*facebook auth stuff
$registry->set('facebook_client_id', $configuration->facebook->client_id);
$registry->set('facebook_client_secret', $configuration->facebook->client_secret);
$registry->set('facebook_redirect_uri', $configuration->facebook->redirect_uri);
*/
if ( $configuration->has->memcache ) {
	// Construct the list of memcache servers to use as our backend cache.  This will be the list of app servers in the farm
	$results = array( 'host'=>$configuration->memcache->url, 'port'=>$configuration->memcache->port, 'persistence'=> true);
	$backendOptions = array('servers'=> $results );
	
	// Only setup the cache if we found the app servers
	if ( count($backendOptions) > 0 ) {
		$frontendOptions = array('lifeTime' => 1800, 'automatic_serialization' => true );
		$cache = Zend_Cache::factory('Core', 'Memcached', $frontendOptions, $backendOptions);
		
		Zend_Registry::set('cache', $cache);
	} else {
		Zend_Registry::set('cache', NULL );
	}
} else {
	Zend_Registry::set('cache', NULL);
}
// Now set session save handler to our custom class which saves the data in MySQL database  
$sessionManager = new My_Session_Manager(); 
Zend_Session::setOptions(array( 
	 	    'gc_probability' => 1, 
	 	    'gc_divisor' => 5000 
	 	    )); 
Zend_Session::setSaveHandler($sessionManager); 
/* Aborted attempt to make sessions persist
$config = array(
'name'           => 'session',      //table name as per Zend_Db_Table
'primary'        => 'session_id',   //the sessionID given by php
'modifiedColumn' => 'modified',     //time the session should expire
'dataColumn'     => 'session_data', //serialized data
'lifetimeColumn' => 'lifetime'      //end of life for a specific record
);
$savehandler = new Zend_Session_SaveHandler_DbTable($config);
//cookie persist for 30 days
Zend_Session::rememberMe($seconds = (60 * 60 * 24 * 30));

//make the session persist for 30 days
$savehandler->setLifetime($seconds)
    ->setOverrideLifetime(true);

Zend_Session::setSaveHandler($savehandler);
Zend_Session::start();
*/

// This is a horrible hack to work with Pauls (ps4) gigantic user
ini_set('max_execution_time', 300);

// we will always use session, so this is good place to create this and save it to the registry
$defSession = new Zend_Session_Namespace('Default', true);
Zend_Registry::set('defSession', $defSession);


$logger = new Zend_Log();
if (!$production) {
	error_reporting(E_ALL | E_STRICT);    
	ini_set('display_startup_errors', 1);    
	ini_set('display_errors', 1);
	//$writer = new Zend_Log_Writer_Stream('php://output');
	//$writer = new Zend_Log_Writer_Syslog(array('application' => 'Mob4Hire'));
	$writer = new Zend_Log_Writer_Stream($siteRootDir.'/log/debug.log');
}else{
	$writer = new Zend_Log_Writer_Stream($siteRootDir.'/log/production.log');
}
$logger->addWriter($writer);
Zend_Registry::set('logger',$logger);

// Setup translation adapter
// Languages are fixed in plugin/language.php
$locale = new Zend_Locale('browser');
Zend_Registry::set('Zend_Locale', $locale);

$translate = new Zend_Translate('gettext', $siteRootDir . '/languages/en.mo', 'en');
$translate->addTranslation($siteRootDir . '/languages/zh_TW.mo','zh');
Zend_Registry::set('Zend_Translate', $translate);

Zend_Form::setDefaultTranslator($translate);



/*
 * S3 File Persistance Stuff
 */
$s3 = new Zend_Service_Amazon_S3($configuration->S3->key, $configuration->S3->secret);
Zend_Registry::set('S3', $s3 );

// Setup the Front Controller, disable the error handler, set our controller directories 
$frontController = Zend_Controller_Front::getInstance();   
  
$frontController->addModuleDirectory($siteRootDir . '/application/modules');
//we want the front controller to return the response, instead of emitting it automatically
$frontController->returnResponse(true);


// define some routes (URLs)
$router = $frontController->getRouter();
$captchaRoute = new Zend_Controller_Router_Route('captcha/get/:namespace/:captchaId', 
    array('controller'=>'captcha', 'action'=>'get')
    );
$router->addRoute('captcha', $captchaRoute);

$activateUserRoute = new Zend_Controller_Router_Route('index/activate/:userId/:activationCode',
    array('controller'=>'index', 'action'=>'activate')
    );    
$router->addRoute('activateUser', $activateUserRoute);    

$projectViewRoute = new Zend_Controller_Router_Route('app/project/view/:idproject',
    array('module'=>'app', 'controller'=>'project', 'action'=>'view')  );    
$router->addRoute('projectViewRoute', $projectViewRoute); 


$projectEditRoute = new Zend_Controller_Router_Route('app/project/definition/:idproject',
    array('module'=>'app', 'controller'=>'project', 'action'=>'definition')  );    
$router->addRoute('projectEditRoute', $projectEditRoute);

$surveyEditRoute = new Zend_Controller_Router_Route('app/survey/definition/:idproject',
    array('module'=>'app', 'controller'=>'survey', 'action'=>'definition')  );    
$router->addRoute('surveyEditRoute', $surveyEditRoute);

$experienceEditRoute = new Zend_Controller_Router_Route('app/experience/definition/:idproject',
    array('module'=>'app', 'controller'=>'experience', 'action'=>'definition')  );    
$router->addRoute('experienceEditRoute', $experienceEditRoute);



$downloadAppFileRoute = new Zend_Controller_Router_Route('app/project/download/:id',
    array('module'=>'app', 'controller'=>'project', 'action'=>'download')  );    
$router->addRoute('downloadAppFileRoute', $downloadAppFileRoute);

$downloadFileRoute = new Zend_Controller_Router_Route('content/download/:id',
    array('module'=>'index', 'controller'=>'content', 'action'=>'download')  );    
$router->addRoute('downloadFileRoute', $downloadFileRoute);

$downloadImageRoute = new Zend_Controller_Router_Route('content/image/:id',
    array('module'=>'index', 'controller'=>'content', 'action'=>'image')  );    
$router->addRoute('downloadImageRoute', $downloadImageRoute);


$showMessageRoute = new Zend_Controller_Router_Route('/app/inbox/view/:id',
    array('module'=>'app', 'controller'=>'inbox', 'action'=>'view')  );    
$router->addRoute('showMessageRoute', $showMessageRoute);

$deleteProjectRoute = new Zend_Controller_Router_Route('/app/project/delete/:id',
    array('module'=>'app', 'controller'=>'project', 'action'=>'delete')  );    
$router->addRoute('deleteProjectRoute', $deleteProjectRoute);

$adminEditProjectRoute = new Zend_Controller_Router_Route('/admin/project/edit/:id',
    array('module'=>'admin', 'controller'=>'project', 'action'=>'edit')  );    
$router->addRoute('adminEditProjectRoute', $adminEditProjectRoute);


$profileViewRoute = new Zend_Controller_Router_Route('/app/profile/view/:id',
    array('module'=>'app', 'controller'=>'profile', 'action'=>'view')  );    
$router->addRoute('profileViewRoute', $profileViewRoute); 


$gmrRoute = new Zend_Controller_Router_Route('/services/global-mobile-research/:status',
    array('module'=>'default', 'controller'=>'services', 'action'=>'global-mobile-research')  );    
$router->addRoute('gmrRoute', $gmrRoute);

$surveyRoute = new Zend_Controller_Router_Route('/survey/:idsurvey',
    array('module'=>'survey', 'controller'=>'index', 'action'=>'index')  );    
$router->addRoute('surveyRoute', $surveyRoute);

$surveyRoute2 = new Zend_Controller_Router_Route('/survey/:idsurvey/:count',
    array('module'=>'survey', 'controller'=>'index', 'action'=>'index')  );    
$router->addRoute('surveyRoute2', $surveyRoute2);

$surveyRouteVanilla = new Zend_Controller_Router_Route('/survey/vanilla/:idsurvey/:iduser',
		array('module'=>'survey', 'controller'=>'vanilla', 'action'=>'index')  );
$router->addRoute('surveyRouteVanilla', $surveyRouteVanilla);

$surveyRouteVanillaCount = new Zend_Controller_Router_Route('/survey/vanilla/:idsurvey/:iduser/:count',
		array('module'=>'survey', 'controller'=>'vanilla', 'action'=>'index')  );
$router->addRoute('surveyRouteVanillaCount', $surveyRouteVanillaCount);

$surveyRouteVanillaReset = new Zend_Controller_Router_Route('/survey/vanilla/reset/:idsurvey/:iduser',
		array('module'=>'survey', 'controller'=>'vanilla', 'action'=>'reset')  );
$router->addRoute('surveyRouteVanillaReset', $surveyRouteVanillaReset);


$groupViewRoute = new Zend_Controller_Router_Route('/app/group/view/:id',
    array('module'=>'app', 'controller'=>'group', 'action'=>'view')  );    
$router->addRoute('groupViewRoute', $groupViewRoute); 

$groupManageRoute = new Zend_Controller_Router_Route('/app/group/manage/:id',
    array('module'=>'app', 'controller'=>'group', 'action'=>'manage')  );    
$router->addRoute('groupManageRoute', $groupManageRoute); 

$groupCloseRoute = new Zend_Controller_Router_Route('/app/group/close/:id',
    array('module'=>'app', 'controller'=>'group', 'action'=>'close')  );    
$router->addRoute('groupCloseRoute', $groupCloseRoute); 

$groupResumeRoute = new Zend_Controller_Router_Route('/app/group/resume/:id',
    array('module'=>'app', 'controller'=>'group', 'action'=>'resume')  );    
$router->addRoute('groupResumeRoute', $groupResumeRoute); 


/*
 * Routes for API testing
 */
$baseApiRoute = new Zend_Controller_Router_Route('/api/:method', array('module' => 'api', 'controller' => 'index', 'action' => 'index'));
$router->addRoute('baseApiRoute', $baseApiRoute);

$contextApiRoute = new Zend_Controller_Router_Route('/api/:context/:method', array('module' => 'api','controller' => 'index', 'action' => 'index'));
$router->addRoute('contextApiRoute', $contextApiRoute);



// Register front controller plugins
// $this->bootstrap('autoloaders');
// $this->bootstrap('frontController');
$plugin = new Lib_Controller_Plugin_Modularlayout();
$frontController->registerPlugin($plugin);

$frontController->registerPlugin(new Lib_Controller_Plugin_ACL());
$frontController->registerPlugin(new  Lib_Controller_Plugin_Header());
$frontController->registerPlugin(new  Lib_Controller_Plugin_Language());

/*
// Facebook login controller
$frontController->registerPlugin(new  Lib_Controller_Plugin_FacebookLogin());
*/

//Check for mobile and populate view->mobile variable
$frontController->registerPlugin(new  Lib_Controller_Plugin_MobileIdentify());  


// Error Handlers 

if (!$production) {
	$frontController->throwExceptions(true);  // This should be removed for production
}
else {
	$frontController->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array(
		'module'     => 'default',
		'controller' => 'error',
		'action'     => 'error'
	)));
}


/*
 * We want to set the encoding to UTF-8, so we won't rely on the ViewRenderer action helper by default, 
 * but will construct view object and deliver it to the ViewRenderer after setting some options. 
 */ 
$view = new Zend_View(array('encoding'=>'UTF-8'));
$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);

$view->addHelperPath('My/View/Helper', 'My_View_Helper_');
$view->addHelperPath('Lib/View/Helper', 'Lib_View_Helper_');
$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
$view->jQuery()->addStylesheet('/js/jquery/css/blitzer/jquery-ui-1.7.2.custom.css')
        ->setLocalPath('/js/jquery.js')
        ->setUiLocalPath('/js/jquery/js/jquery-ui-1.7.2.custom.min.js');

$viewRenderer->setView($view);
Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
Zend_Controller_Action_HelperBroker::addPath('My/Controller/Action/Helper', 'My_Controller_Action_Helper');
$authUsersHelper = new My_Controller_Action_Helper_AuthUsers();
Zend_Controller_Action_HelperBroker::addHelper($authUsersHelper);

 
// Now we initialize the Zend_Layout object with MVC support
Zend_Layout::startMvc(
    array(
        'layoutPath' => $siteRootDir . '/application/layouts',
        'layout' => 'main'
    )
);



// run the dispatch, get the response and send it to the client   
$response = $frontController->dispatch();
$response->sendResponse();

function __($string)
{
    return $string; // This was added for translation purposes
}

function fb($message, $label=null)
{
	if ($label!=null) {
		$message = array($label,$message);
	}
	try {
		Zend_Registry::get('logger')->debug($message);
	}
	catch (Exception $e)
	{
	 // This will be triggered if on production server which has no logger
	 // Just in case anybody leaves debug stuff in the code
	}
}
