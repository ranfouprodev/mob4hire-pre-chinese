<?php
class Form_ViewProjectProfile extends My_Form
{
	protected $idproject=0;
	
	public function __construct($idproject, $options=NULL)
	{
		$this->idproject = $idproject;
		$this->setAttrib('enctype', 'multipart/form-data');
		
		parent::__construct($options);
	
	}
	
	public function init()
	{   
		$viewProject = new Zend_Form_Element_Button('viewProject');
		$viewProject->setLabel('VIEW PROJECT PUBLIC PROFILE')
					->setAttrib('onClick', 'window.open("/app/project/view/idproject/'.$this->idproject.'/popup/1", "Project Profile", "width=800, height=600,scrollbars=yes");');;
		$this->addElement($viewProject);	
	}
}
	