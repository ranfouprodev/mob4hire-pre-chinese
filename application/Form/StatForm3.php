<?php
class Form_StatForm3 extends My_Form
{
	public function init()
	{
		$this->setAction('/admin/project/dog')->setMethod('POST')->setAttrib('id','frmStatForm');
		$projects=new Projects();
		//$parameters = $this->_getAllParams();
  		$idproject=$_POST['select1'];
  		$regdevices=new Regdevices();
  		$this->view->testerList=$regdevices->getCompatibleHandsets($idproject);
  		$users=array();
  		foreach($this->view->testerList as $tester) // get an array of unique user ids
  		{
   			if(!in_array($tester->userid, $users)) 
   			{
    			array_push($users, $tester->userid);
   			}
  		}
		$select2 = new Zend_Form_Element_Select('select2');
		$total=0;
		foreach($users as $user)
		{
			$total++;
			$select1->addMultiOption($user->userid,$user->userid);
			
		}
		
        
        $select2->setLabel('Send out '.$total.' emails?');
		$select2->setDecorators(array('MobForm'));	
		$select2->setValue(50)->setRequired(true);
		$select2->helpText = 'From this list select a project that you wish to send an invite email out for.';

		
        
        $goButton = new Zend_Form_Element_Submit('button');
        $goButton->setLabel('Find the dudes');
		
        $this->addElement($select2);
        $this->addElement($goButton);
        
	}
}