<?php
class Form_ProfileDeveloperApply extends My_Form
{
	public function init(){
	
	
		$this->addPrefixPath('My_Decorator',
	                        'My/Decorator/',
	                        'decorator');
		
		
		$title = new My_Form_Element_myXhtml('applyhdr');
		$title->setContent('<div class="form_element clearfix"><div class="form_label"><label for="mobster">Apply To Be A Developer</label>
		<div class="form_description"></div></div><div class="form_helptip"><span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>Since a citizen of the Mob4Hire community can do several things, use this to apply to participate as a Mobster. </p></span></div>
		<div class="form_input"><p>Do you have mobile projects thats you wish to have tested by the community?</p></div></div>');
		
	    
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
	
		$hidden = new Zend_Form_Element_Hidden('apply');
		
		$submit = new Zend_Form_Element_Submit('applybutton');
		$submit->setLabel  ('')
				->setDecorators($this->submitDecorators);


	
		$this->addElement($title);    
		$this->addElement($start_row);
		$this->addElement($hidden);
		$this->addElement($submit);
		$this->addElement($end_row);
		
		
	}
}
?>
