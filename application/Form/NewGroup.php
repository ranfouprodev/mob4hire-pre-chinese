<?php 
class Form_NewGroup extends My_Form {
	public function init() {

		$filterTrim = new Zend_Filter_StringTrim();
        
		/*
		*  Type
		*/
		$type = new Zend_Form_Element_Radio('type');
		$type->addMultiOptions(array(1=>'Public (Visible on the groups listing)', 2=>'Private (Hidden on the groups listing)'))
				->setLabel('Group Type')
		  		->setDecorators(array('MobForm'))	
				->setDescription('Private groups are not visible on the main search index')
				->setValue(1);
		$type->helpText = 'Public groups are indexed and visible on the main search';

		
		/*
		 * Group Name
		 */
		$groupName = new Zend_Form_Element_Text('name');    
		$groupName->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Group Name')
				 ->setAttrib('size', '60')
				 ->setRequired(true)
				 ->setDescription('Choose a group name');			 
 		$groupName->setValue('');
		$groupName->helpText = 'The name you will be assigning to your group. This will be shown in searches so make sure it is relevant';
        
		/*
		 * Group Short Description
		 */
		$groupShort = new Zend_Form_Element_Text('short');    
		$groupShort->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Short Description')
				 ->setAttrib('size', '60')
				 ->setRequired(true)
				 ->setDescription('This is the description shown in the listing. Should be under 140 characters');			 
 		$groupShort->setValue('');
		
		/*
		 * Group Description
		 */   	 
		$groupDesc = new My_Form_Element_TinyMce('description');
		$groupDesc->addFilter($filterTrim)
					//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setDecorators(array('MobForm'))
					->setRequired(true)
					->setLabel('Description')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50')
					->setDescription('Give enough information to describe the groups mission');
		$groupDesc->helpText='This is a longer description of the purpose of the group';
		
		
		
		/*
		* Tags
		*/
		$tags = new Zend_Form_Element_Text('tags');
		$tags->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setRequired(false)
			->setLabel('Tags')
			->setAttrib('size', '50')
			->setDescription('Enter tags separated by commas.');
		$tags->helpText='Tags are useful for Mobsters to find your project through search results';
		
		/*
		 * Image Thumbnail
		 */
		$imageName= new Zend_Form_Element_Hidden('image');
		$imageName->setValue('/images/image-not-found.gif') ;
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak->setContent('<hr />');
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$next = new Zend_Form_Element_Submit('submit');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$this->addElement($groupName);
		$this->addElement($type);
		$this->addElement($groupShort);
		$this->addElement($groupDesc);
		$this->addElement($tags);
        $this->addElement($imageName);
        
		$this->addElement($start_row);
		$this->addElement($next);
		$this->addElement($end_row);
	}
}