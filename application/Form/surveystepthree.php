<?php
class Form_surveystepthree extends My_Form
{
    public function init()
    {
                $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
        
        $uploadSurvey = new My_Form_Element_FileUpload('survey');
		$uploadSurvey->setDecorators(array('MobForm'))
				->setLabel('Upload Survey')
				->setDescription('See Flyover ? button for pop-up help on the format of the file.');
		$uploadSurvey->helpText='Your .CSV or .XLS file must be uploaded in the following format: Column 1 is the question text. Column 2 is the type of question; you must enter ONE of [Text, Checkbox, Radiobutton, Yes, Skip]. Column 3 is the default value (if any). Column 4 is the # of attributes; the subsequent columns are the different choices for the Checkbox (multiple choices can be selected) or the Radiobutton (only one choice can be selected). If the type is "Skip", it\'s a Yes/No question that allows simple branching in the survey. Column 3 is the # of question to skip to if a No is answered, Column 4 is the Yes alternative. Anything more complicated? (or perhaps you need some assistance?) Please select "Managed Services" when choosing the MobSurvey package, and we\'ll quote on the job. After submission, we\'ll configure the survey, and then give you one chance at edits before it goes live. Any additional edits will cost you at our going rate for survey design. Ok?';
				
		$divuploadSurvey=new My_Form_Element_myXhtml('divuploadSurvey');
		$divuploadSurvey_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadSurvey_str .= '<div id="divsurvey"></div>';
		$divuploadSurvey_str .= '</div>';
		$divuploadSurvey_str .= '</div>';	
		$divuploadSurvey->setContent($divuploadSurvey_str);
		
		$uploadSurveyScript = new My_Form_Element_FileUpload('surveyScript');
		$uploadSurveyScript->setDecorators(array('MobForm'))
				->setDescription('Tell Mobsters step by step exactly what you want them to do to complete a survey.')
				->setLabel('Upload Survey Script')
				->helpText='The Survey Script is one of the key elements of a successful survey; as they say "Garbage In Garbage Out". The secret to a good one is to think like a user. What steps do Mobsters need to do in order to accomplish your goals?';
			
		$divuploadSurveyScript=new My_Form_Element_myXhtml('divtestOutput');
		$divuploadSurveyScript_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadSurveyScript_str .= '<div id="divsurveyScript"></div>';
		$divuploadSurveyScript_str .= '</div>';
		$divuploadSurveyScript_str .= '</div>';	
		$divuploadSurveyScript->setContent($divuploadSurveyScript_str);
		
		
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak_str = '<hr /><br />';
		$lineBreak_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$lineBreak_str .= 'Use the following section to provide the links to the resources that you want the surveyors to evaluate';
		$lineBreak_str .= '</div>';	
		$lineBreak->setContent($lineBreak_str);
		
		
		
		
		$genFiles = new My_Form_Element_FileUpload('genFiles');
		$genFiles->setDecorators(array('MobForm'))
				->setLabel('Generic Mobile App Files')
				->setDescription('Upload files that will be used by everyone\'s handset in the project.');
		$genFiles->helpText='Optional. This would be your mobile app. Or, mobile ad campaign. Or, whatever you want to install on the Mobsters handsets for them to try out before they complete a survey.';
		
		$divgenFiles=new My_Form_Element_myXhtml('divgenFiles');
		$divgenFiles_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divgenFiles_str .= '<div id="divgenFiles"></div>';
		$divgenFiles_str .= '</div>';
		$divgenFiles_str .= '</div>';	
		$divgenFiles->setContent($divgenFiles_str);
		
		$mobURL= new Zend_Form_Element_Text('mobURL');
		$mobURL->setLabel('Mobile Website')
				->setAttrib('size','50')
				->setDescription('Or, do you need a web resource that the Mobsters can access. Use this field if you want to test your mobile web site')
				->setDecorators(array('MobForm'));
		$mobURL->helpText='Optional. Handsets and browsers vary so much that you\'ll need to test your website on many different versions. Enter the URL of your mobile website here.';
		
		$appStoreURL= new Zend_Form_Element_Text('appStoreURL');
		$appStoreURL->setLabel('App Store URL')
				->setAttrib('size','50')
				->setDescription('Or, have the mobile user download it from an app store.')
				->setDecorators(array('MobForm'));
		$appStoreURL->helpText='If you already have an application in market that you wish to survey, you can direct mobile users to app stores. Remember: if you wish them to download a paid app, make sure to include the cost in the fee that you\'re willing to give for completing the survey.';
		
	
		
		$screenShots = new My_Form_Element_FileUpload('screenShots');
		$screenShots->setLabel('Screenshots')
				->setDecorators(array('MobForm'))
				->setDescription('Upload screenshots, other images or graphics.');	
		$screenShots->helpText='Your survey may be easier to explain if you include graphics. Or, maybe you want do some A-B testing, and show several screen samples for the mobile user to compare and comment on.';	
			
		
		$divscreenShots=new My_Form_Element_myXhtml('divscreenShots');
		$divscreenShots_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divscreenShots_str .= '<div id="divscreenShots"></div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots->setContent($divscreenShots_str);
		
		$documentation = new My_Form_Element_FileUpload('documentation');
		$documentation->setLabel('Other Documentation')
				->setDecorators(array('MobForm'))
				->setDescription('Upload supporting documentation, user guides or help.');	
		$documentation->helpText='Are there any other documents you need reviewed?';		
			
		$divdocumentation=new My_Form_Element_myXhtml('divdocumentation');
		$divdocumentation_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divdocumentation_str .= '<div id="divdocumentation"></div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation->setContent($divdocumentation_str);
			
		
		$this->addElement($uploadSurvey);
		$this->addElement($divuploadSurvey);
		$this->addElement($uploadSurveyScript);	
		$this->addElement($divuploadSurveyScript);
	
		$this->addElement($lineBreak);
		$this->addElement($genFiles);
		$this->addElement($divgenFiles);
		$this->addElement($mobURL);
	//	$this->addElement($appStoreURL);
	
		$this->addElement($screenShots);
		$this->addElement($divscreenShots);		
		$this->addElement($documentation);
		$this->addElement($divdocumentation);
	    
			$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('                       ')
				->setDecorators($this->submitDecorators);
				
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$mobtest_logo = new My_Form_Element_myXhtml('mobsurvey-logo');
		$mobtest_logo->setContent('<div id="mobsurvey-logo"><span>MobSurvey Mobile surveys. Global Panels.</span></div>');

		
		$this->addElement($start_row);
		$this->addElement($mobtest_logo);
		
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);
		
    }    
} //end class

?>