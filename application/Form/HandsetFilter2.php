<?php
class Form_HandsetFilter2 extends My_Form
{
	public function init()
	{	

		$countries=new Countries();
		$countrylist=array_merge(array(0=>'Any'),$countries->listCountries()); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country');
		
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$country->addMultiOption($id, $countryname);
				else
					$country->addMultiOption(substr($id,7), $countryname);
		}
		
		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$networktable=new My_Form_Element_myXhtml('networktable');
		$networktable_str = '<div id="networktable"></div>';
		$networktable_str .= '</div>';
		$networktable_str .= '</div>';
		$networktable->setContent($networktable_str);

		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel('Platform')
				->addMultiOptions(array(0=>'Any', 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(0=>'Any'), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel('Handset')
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically
		
				
		$project = new Zend_Form_Element_Text('idproject');
		$project->setLabel('idproject');

		$user = new Zend_Form_Element_Text('userlike');
		$user->setLabel('userlike');
		
		
		$submit= new Zend_Form_Element_Button('filter');
		$submit->setLabel('Filter');
		
		$this->addElement($country);
		$this->addElement($carrier);
		//$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($project);
		$this->addElement($user);
		$this->addElement($submit);
        }
}
?>