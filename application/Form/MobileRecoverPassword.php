<?php
class Form_MobileRecoverPassword extends My_Form
{
	public function init()
	{

		
		$this->setAction('/mobile/user/forgotpassword')
			->setMethod('post')
			->setAttrib('id', 'login');

		$translator = $this->getTranslator();

		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' " . $translator->translate('form_recoverpassword_IP_ADDRESS_NOT_ALLOWED')),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' " . $translator->translate('form_recoverpassword_UNKNOWN_TLD')),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_DASH')),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME_SCHEMA') . "'%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' " . $translator->translate('form_recoverpassword_UNDECIPHERABLE_TLD')),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME0')),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID_LOCAL_NAME')),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' " . $translator->translate('form_recoverpassword_LOCAL_NAME_NOT_ALLOWED'))
			)
		);

		$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' " . $translator->translate('form_recoverpassword_INVALID')),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' " . $translator->translate('form_recoverpassword_INVALID_HOSTNAME') . " '%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' " . $translator->translate('form_recoverpassword_INVALID_MX_RECORD') . " '%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' " . $translator->translate('form_recoverpassword_DOT_ATOM')),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' " . $translator->translate('form_recoverpassword_QUOTED_STRING')),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' " . $translator->translate('form_recoverpassword_INVALID_LOCAL_PART') . " '%value%'")
			)
		);
 
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel($translator->translate('form_recoverpassword_email'))
				->addValidator($validatorEmail)
				->setRequired(true)
				->setAttrib('size','50');      
				
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel  ($translator->translate('form_recoverpassword_submit'));
		$submit->setAttrib('rel','external');
		$submit->setAttrib('data-inline','true');
		$submit->setAttrib('data-icon','arrow-r');
		$submit->setAttrib('data-iconpos','right');
		$submit->setAttrib('data-theme', 'a');
		
		$this->addElement($email);
		$this->addElement($submit);				 
		
 	}
}
