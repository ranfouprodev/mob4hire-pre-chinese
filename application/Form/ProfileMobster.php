<?php
class Form_ProfileMobster extends My_Form
{
	public function init(){
	
	$translator = $this->getTranslator();	
	$filterTrim = new Zend_Filter_StringTrim();

	$validatorNotEmpty = new Zend_Validate_NotEmpty();
	$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));

	
	$notifyEmail = new Zend_Form_Element_MultiCheckbox('notify_email');
	$notifyEmail->setLabel($translator->translate('form_profileMobster_byEmail'))
				->setDecorators(array('MobForm'));
	$notifyEmail->setDescription($translator->translate('form_profileMobster_byEmailDesc'));
	$notifyEmail->helpText=$translator->translate('form_profileMobster_byEmailHelp');
	$notifyEmail->addMultiOptions(array(1 => $translator->translate('form_profileMobster_byEmailOpt1'),2 => $translator->translate('form_profileMobster_byEmailOpt2') ,3 => $translator->translate('form_profileMobster_byEmailOpt3'),4 => $translator->translate('form_profileMobster_byEmailOpt4'),5 => $translator->translate('form_profileMobster_byEmailOpt5')));
	$notifyEmail->setValue(array(1,2,3,4,5));
	
	$notifySMS = new Zend_Form_Element_MultiCheckbox('notify_sms');
	$notifySMS->setLabel($translator->translate('form_profileMobster_bySms'))->setDecorators(array('MobForm'));
	$notifySMS->setDescription($translator->translate('form_profileMobster_bySmsDesc'));
	$notifySMS->helpText=$translator->translate('form_profileMobster_bySmsHelp');
	$notifySMS->addMultiOptions(array(1 => $translator->translate('form_profileMobster_bySmsOpt1'),2 => $translator->translate('form_profileMobster_bySmsOpt2'),3 => $translator->translate('form_profileMobster_bySmsOpt3'),4 => $translator->translate('form_profileMobster_bySmsOpt4')));
	
	$this->addElement($notifyEmail);
	//$this->addElement($notifySMS);
	
	$this->addDisplayGroup(array('notify_email'),
                                       'notifications',
                                       array('legend' => $translator->translate('form_profileMobster_notifications')));
	
	// begin Heading
	$devicesHeading = new My_Form_Element_myXhtml('devicesHeading');
	$devicesHeading_str = '<span class="mobblack">';
	$devicesHeading_str .= "<h3>" . $translator->translate('form_profileMobster_mandatoryHeader') . "</h3>" . $translator->translate('form_profileMobster_mandatory1') . "<br /><br />" . $translator->translate('form_profileMobster_mandatory2') . " <a href='mailto:support@mob4hire.com?subject=I%20would%20like%20to%20become%20a%20MobPro!'><u>" . $translator->translate('form_profileMobster_mandatoryContactUs') . " </u></a>" . $translator->translate('form_profileMobster_mandatory3') . "</span><br/>";
	$devicesHeading->setContent($devicesHeading_str);
	$this->addElement($devicesHeading);
	
	/*$this->addDisplayGroup(array('devicesHeading'),
			       'Devices Heading',
				array('legend' => 'Add Devices To Your Account'));
	*/
	$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element clearfix">';
		$heading1_str .= '<div class="form_label"><label>' . $translator->translate('form_profileMobster_heading1Label') . '</label>';
		$heading1_str .= '<div class="form_description">';
		$heading1_str .= '<p>' . $translator->translate('form_profileMobster_heading1Desc') . '</p>';
		$heading1_str .= '</div></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>' . $translator->translate('form_profileMobster_heading1Help') . '</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);

		$countries=new Countries();
		$countrylist=$countries->listCountries(); 
		
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel($translator->translate('form_profileMobster_heading1Country'))
				->setDecorators(array('MobForm'));
		foreach($countrylist as $id=>$countryname) {
				$country->addMultiOption(substr($id,7), $countryname);
		}

		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel($translator->translate('form_profileMobster_heading1Network'))
				->setDecorators(array('MobForm'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically


		$devices=new Devices();	
		$vendors=$devices->listVendors();
		
		$manufacturer = new Zend_Form_Element_Select('vendor');	
		$manufacturer->setLabel($translator->translate('form_profileMobster_heading1Handset'))
				->setDecorators(array('MobForm'))
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically


		//$modelList = $devices->listModels(current($vendors));	
		$model = new Zend_Form_Element_Select('model');
		$model->setLabel($translator->translate('form_profileMobster_heading1Model'))
				->setDecorators(array('MobForm'))
		//		->addMultiOptions($modelList)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addmodel = new Zend_Form_Element_Button('addmodelBtn');
		$addmodel->setLabel($translator->translate('form_button_add'));
		$addmodel->setAttrib('class', 'text_button');
        $addmodel->setDecorators($this->buttonDecorators);

		$endbtn=new My_Form_Element_myXhtml('end_button');
		$endbtn->setContent('</div></div>');
		
		
		
		$handsettable=new My_Form_Element_myXhtml('ajaxdiv');
		$handsettable->setContent('<div id="ajaxtable"></div>');
		
		$devicesHeading2 = new My_Form_Element_myXhtml('devicesHeading2');
		$devicesHeading2_str = "<br /><em>" . $translator->translate('form_profileMobster_asterisk') . "</em><br /><br /><span class='mobblack'>";
		$devicesHeading2_str .= "<p><h2 class='profile-mobster'>" . $translator->translate('form_profileMobster_pick') . "</h2></p></span>";
		$devicesHeading2->setContent($devicesHeading2_str);
		$this->addElement($devicesHeading2);
			
		
		$step1 = new My_Form_Element_myXhtml('step1');
		$step1_str = '	<div class="form_element clearfix">';
		$step1_str .='		<div class="form_label"><label>' . $translator->translate('form_profileMobster_step1Label') . '</label>';
		$step1_str .=' 		<div class="form_description">';
		$step1_str .= '			<p>' . $translator->translate('form_profileMobster_step1Desc') . '</p>';
		$step1_str .= '		</div>';
		$step1_str .='		</div>';
		$step1_str .= '	<div class="form_helptip">';
		$step1_str .= '		<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$step1_str .= '			<p style="display: none;">'. $translator->translate('form_profileMobster_step1Help') . '</p>';
		$step1_str .= '		</span>';
		$step1_str .= '	</div>';
		$step1_str .= '	<div class="form_input">' . $translator->translate('form_profileMobster_step1Form1') . ': <a href="https://app.mob4hire.com/mobile"><span class="mobred">https://app.mob4hire.com/mobile</span></a>' . $translator->translate('form_profileMobster_step1Form2');
						'</div>';	
		$step1_str .= '</div>';

		$step1->setContent($step1_str);
		$this->addElement($step1);

		// Removed - Don't Update
		$manual = new My_Form_Element_myXhtml('manualsms');
		$manual_str = '<div class="form_element clearfix">';
		$manual_str .= '	<div class="form_label"><label> 2. Send an SMS to your Phone</label>';
		$manual_str .=' 		<div class="form_description">';
		$manual_str .= '			<p>Enter your mobile number here, including international area codes, and we can send you an sms to identify your device.</p>';
		$manual_str .= '		</div>'.
					'	</div>';
		$manual_str .= '	<div class="form_helptip">';
		$manual_str .= '		<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$manual_str .= '			<p style="display: none;">Use this method if you cannot find your phone on the list. By entering your mobile phone number, we will send you an sms to your device to identify it with our system. Devices will automatically be added into your account. The following are examples of international phone numbers, with area code: N. America: 13035556666. UK: 447737013261.</p>';
		$manual_str .= '		</span>';
		$manual_str .= '	</div>';
		$manual_str .= '	<div class="form_input">';		
		$manual_str .= '		<input type="text" value="" id="manualsms" name="manualsms">';
		$manual_str .= '		<button type="button" id="sendbutton" name="sendbutton">Send SMS</button>';
		$manual_str .= '		<br/><div id="smsresult">'.
						'</div>';
		$manual_str .= '</div>';
				
		//$manual->setContent($manual_str);
		
		$heading2 = new My_Form_Element_myXhtml('heading2');
		$heading2_str = '<div class="form_element clearfix">';
		$heading2_str .= '<div class="form_label"><label>' . $translator->translate('form_profileMobster_heading2Label') . '</label>';
		$heading2_str .= '<div class="form_description">';
		$heading2_str .= '<p>' . $translator->translate('form_profileMobster_heading2Desc') . '</p>';
		$heading2_str .= '</div></div>';
		$heading2_str .= '<div class="form_helptip">';
		$heading2_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading2_str .= '<p>' . $translator->translate('form_profileMobster_heading2Help') . '</p>';
		$heading2_str .= '</span>';
		$heading2_str .= '</div>';
		$heading2_str .= '<div class="form_input">';
		$heading2->setContent($heading2_str);
		
		$country2 = new Zend_Form_Element_Select('country2');
		$country2->setLabel($translator->translate('form_profileMobster_heading2Country'))
				->setDecorators(array('MobForm'));
		foreach($countrylist as $id=>$countryname) {
				$country2->addMultiOption(substr($id,7), $countryname);
		}
		
		$carrier2 = new Zend_Form_Element_Select('network2');
		$carrier2->setLabel($translator->translate('form_profileMobster_heading1Network'))
				->setDecorators(array('MobForm'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$manualHandset = new Zend_Form_Element_Text('manualHandset');
		$manualHandset->addFilter($filterTrim)->setLabel($translator->translate('form_profileMobster_heading1Handset'))->setDecorators(array('MobForm'));
		$manualModel = new Zend_Form_Element_Text('manualModel');
		$manualModel->addFilter($filterTrim)->setLabel($translator->translate('form_profileMobster_heading1Model'))->setDecorators(array('MobForm'));
				
		$addmodel2 = new Zend_Form_Element_Button('addmodel2Btn');
        $addmodel2->setLabel($translator->translate('form_button_add'));
        $addmodel2->setAttrib('class', 'text_button');
        $addmodel2->setDecorators($this->buttonDecorators);
		
		$endbtn2=new My_Form_Element_myXhtml('end_button2');
		$endbtn2->setContent('</div></div>');

		
		
	
		$this->addElement($handsettable);
		$this->addElement($heading1);
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($heading2);
		$this->addElement($country2);
		$this->addElement($carrier2);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		$this->addElement($addmodel2);
		$this->addElement($manualHandset);
		$this->addElement($manualModel);
		$this->addElement($endbtn);
		$this->addElement($endbtn2);
		
		$this->addElement($manual);
		
		$this->addDisplayGroup(array('devicesHeading','ajaxdiv','devicesHeading2','step1'),
                                       'Top',
                                       array('legend' => $translator->translate('form_profileMobster_devicesLegend')));
		
		$this->addDisplayGroup(array('manualsms'),
				       'sms',
				       array('legend' => ''));
		
		$this->addDisplayGroup(array('heading1', 'country', 'network','vendor','model', 'platform', 'addmodelBtn','end_button'),
                                       'Add Devices To Your Account',
                                       array('legend' => ''));
		
		$this->addDisplayGroup(array('heading2', 'country2', 'network2','manualHandset','manualModel','addmodel2Btn','end_button2'),
				       'cant find it',
                                       array('legend' => ''));
		
	


	
	$browser = new Zend_Form_Element_MultiCheckbox('browser');
	$browser->setLabel('What type of web browser do you have on your computer?')->setDecorators(array('MobForm'));
	$browser->setDescription('Do you wish to get paid to test projects and surveys?');
	$browser->helpText='Many projects may also require you to interact with a web site with a specific browser in order to test mobile and web compatibility and integration. Having this information in advance allows us to filter projects to you that you can specifically handle.';
	$browser->addMultiOptions(array(1 => 'I don\'t have a computer with an internet connection.',2 => 'Internet Explorer',3 => 'Opera',4 => 'Firefox',5 => 'Safari',6 => 'Chrome',7 => 'My browser is not listed.'));
	$browser->setvalue(1);
	
	$testType = new Zend_Form_Element_MultiCheckbox('test_type');
	$testType->addMultiOptions(array('Functional'=>'Functional', 'Usability'=>'Usability'))
			->setDecorators(array('MobForm'))
//			->setRequired(true)
			->setLabel('Type of projects you\'d like to work on')
			->setDescription('Select one or both');
	$testType->setValue(1);
	$testType->helpText ='With functional testing, you get answers to questions like "Does it crash?" and "Does it take advantage of the device special features like trackballs, touchscreens and high-resolution cameras?" In usability testing, you get answers to questions like "Does the menu structure make sense?" or "Is it fun to play?" and "Would you give it a good review on an app store?"  Many projects require both types of feedback, but many also do not!';
		
	$apps = new Zend_Form_Element_MultiCheckbox('apps');
	$apps->setLabel('Test Categories')->setDecorators(array('MobForm'));
	$apps->setDescription('Select them all if you want to.');
	$apps->helpText='Most people select all of these, but if you want us to try to match you to projects you are more suited for, do it here.';
	$apps->addMultiOptions(array(1 => 'News',2 => 'Medical',3 => 'Reference',4 => 'Productivity',5 => 'Navigation',6 => 'Health and Fitness',7 => 'Education',8 => 'Weather',9 => 'Business',10 => 'Music',11 => 'Finance',12 => 'Sports',13 => 'Travel',14 => 'Utilities',15 => 'Games',16 => 'Social Networking',17 => 'Books',18 => 'Entertainment',19 => 'Lifestyle'));
	

	//$this->addElement($browser);
	//$this->addElement($testType);
	//$this->addElement($apps);

	$hack=new My_Form_Element_myXhtml('hack'); // fix for IE7
	$hack->setContent("<br/><br/><br/>");
	$this->addElement($hack);
	
	/*$this->addDisplayGroup(array('handsetPicker','browser','test_type','apps'),
                                       'Devices And Technologies',
                                       array('legend' => 'Devices And Technologies'));*/
	
	//SKILLS AND EXPERIENCE
	$experience = new My_Form_Element_TinyMce('experience');
		$experience->addFilter($filterTrim)
				// ->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				 ->setDecorators(array('MobForm'))
				 ->setRequired(true)
				 ->setLabel('Experience Description')
				 ->setAttrib('rows', '7')
				 ->setAttrib('cols', '50')
				 ->setDescription('Your areas of expertise ... a small resume to apply for the job.');
		$experience->helpText='This is your chance to shine! This description appears on your public profile, and is used by mobile developers to help determine if they\'d like to use you in a project you want to work on. Be as general or specific as you want.';				 
	
			
	$education = new Zend_Form_Element_Select('education');
	$education->setLabel('Highest Level Of Education')->setDecorators(array('MobForm'));
	$education->helpText = 'Please be honest. Projects require a broad range of skill sets and education, so a developer or market researcher may be looking for someone in exactly your position.'; 
	$education->addMultiOptions(array(1 => 'Less than high school.',2 => 'High school graduate.',3 => 'Some college or university.',4 => 'College graduate with a 2 year degree.',5 => 'College graduate with a 4 year degree.',6 => 'Advanced degree.'));
	
	$previousTesting = new Zend_Form_Element_Select('previous_testing');
	$previousTesting->setLabel('Have You Tested Software Before?')->setDecorators(array('MobForm'));
	$previousTesting->helpText = '&nbsp'; 
	$previousTesting->addMultiOptions(array(1 => 'Not really.',2 => 'A little.',3 => 'Lots.',4 => 'I used to work in Q/A.',5 => 'I work in Q/A right now.'));
	
	$nda = new Zend_Form_Element_Radio('nda');
	$nda->setLabel('Would you consider signing an NDA when doing a project?')->setDecorators(array('MobForm'));
	$nda->setDescription('Some projects require a non-disclosure agreement to be signed.');
	$nda->helpText='The mobile business is moving very fast. Sometimes, developers or market researchers don\'t want to let the word out that THE NEXT and BIGGEST NEXT THING is in the testing phase. They may require you to sign an NDA to further legally protect their assets. Of course, by using Mob4Hire, you are always legally bound to abide by our Terms and Conditions, no matter whether you sign an NDA or not.';
	$nda->addMultiOptions(array(1 => 'Yes', 0 => 'No'));
	$nda->setvalue(1);
	
	//$this->addElement($experience);
	//$this->addElement($education);
	//$this->addElement($previousTesting);
	//$this->addElement($nda);
	
	/*$this->addDisplayGroup(array('experience', 'education','previous_testing','nda'),
                                       'skills and experience',
                                       array('legend' => 'Skills And Experience'));
	*/
	$whoSummary = new My_Form_Element_myXhtml('who_summary');
	$whoSummary->setContent('These demographics are used when developers or market researchers want to target a "focus group" ... i.e. someone who is part of their potential customer base they want to learn more about regarding use of their app, product or service.<br />The more you complete, the more you\'ll be selected for highly valuable and targeted surveys.<br />This information is PRIVATE. It can never be used to individually identify you. We don\'t share it one a 1:1 basis with anyone.');
	
	$gender = new Zend_Form_Element_Select('gender');
	$gender->setLabel('Gender')->setDecorators(array('MobForm'));
	$gender->helpText='In addition to age, gender is one of the most requested attributes in a survey. Make sure you fill this one out.';
	$gender->addMultiOptions(array(1 => 'Rather not say.',2 => 'Male',3 => 'Female'));

	
	$bdate =  new My_Form_Element_DateSelects('bdate');
        $validatorDate = new Zend_Validate_Date();
        $validatorDate->setMessages(
            array(
                Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
    $bdate->setLabel('Date Of Birth')->addValidator($validatorDate)->setDecorators(array('MobForm'));
	$bdate->helpText='In addition to gender, age is one of the most requested attributes in a survey. Make sure you fill this one out.';
    $bdate->setStartEndYear(1933, date("Y"))->setReverseYears(true);
	
	$maritalStatus = new Zend_Form_Element_Select('martial_status'); // Yes, I know. The same typo exists in the database
	$maritalStatus->setLabel('Marital Status')->setDecorators(array('MobForm'));
	$maritalStatus->helpText='In addition to age, gender is one of the most requested attributes in a survey. Make sure you fill this one out.';
	$maritalStatus->addMultiOptions(array(1 => 'Rather not say.',2 => 'Single / Never Married',3 => 'Married',4 => 'Divorced',5 => 'Widowed.',6 => 'Separated.'));
	
	$children = new Zend_Form_Element_Select('children');
	$children->setLabel('How many children under the age of 18 live at home?')->setDecorators(array('MobForm'));
	$children->helpText='In addition to age, gender is one of the most requested attributes in a survey. Make sure you fill this one out.';
	$children->addMultiOptions(array(1 => 'Rather not say',2 => '0',3 => '1',4 => '2',5 => '3',6 => '4',7 => '5 or more.'));
	
	$annualIncome = new Zend_Form_Element_Select('annual_income');
	$annualIncome->setLabel('What is your total annual household income?')->setDecorators(array('MobForm'));
	$annualIncome->helpText='In addition to age, gender is one of the most requested attributes in a survey. Make sure you fill this one out.';
	$annualIncome->addMultiOptions(array(1 => 'Rather not say',2 => '$0 - $10,000',3 => '$10,001 - $23,000',4 => '$23,001 - $46,000',5 => '$46,001 - $69,000',6 => '$69,001 - $115,000',7 => '$115,001 - $161,000',8 => '$161,001 or more'));

	
	/*$this->addElement($whoSummary);
	$this->addElement($gender);
	$this->addElement($bdate);
	$this->addElement($maritalStatus);
	$this->addElement($children);
	$this->addElement($annualIncome);
	
	$this->addDisplayGroup(array('who_summary','gender', 'bdate','martial_status','children','annual_income'),
                                       'who are you',
                                       array('legend' => 'Who Are You?'));*/
	
	$mobLife = new Zend_Form_Element_Select('mob_life');
	$mobLife->setLabel('Describe your mobile lifestyle.')->setDecorators(array('MobForm'));
	$mobLife->helpText='In addition to age, gender is one of the most requested attributes in a survey. Make sure you fill this one out.';
	$mobLife->addMultiOptions(array(1 => 'Rather not say',2 => 'Rarely use my mobile handset.',3 => 'It\'s part of my day.',4 => 'I get nervous when I don\'t have my phone.',5 => 'I love mobile!',6 => 'I\'m the first person to try everything new.',7 => 'It\'s an integral part of both my job and my home life.'));
	
	$phoneUse = new Zend_Form_Element_MultiCheckbox('phone_use');
	$phoneUse->setLabel('Other than talking ...')->setDecorators(array('MobForm'));
	$phoneUse->setDescription('... what other activities do you do on your mobile phone?');
	$phoneUse->helpText='&nbsp';
	$phoneUse->addMultiOptions(array(1 => 'Web browsing',2 => 'Game playing',3 => 'Email',4 => 'Instant messaging',5 => 'Photo/photo sharing',6 => 'Music and Video Stream',7 => 'Text messaging/SMS',8 => 'MMS messaging',9 => 'Photography',10 => 'Check weather',11 => 'Check time (clock)',12 => 'Navigation / maps',13 => 'Video calls',14 => 'Watch television',15 => 'Social networking like Facebook',16 => 'Books',17 => 'Organizing a calendar',18 => 'Other (please specify)'));
	$phoneUseOther = new Zend_Form_Element_Text('phone_use_other');
	$phoneUseOther->addFilter($filterTrim)->setLabel('&nbsp;')->setDecorators(array('MobForm'));
	
	/*$this->addElement($mobLife);
	$this->addElement($phoneUse);
	$this->addElement($phoneUseOther);
	
	$this->addDisplayGroup(array('mob_life','phone_use','phone_use_other'),
                                       'mob life',
                                       array('legend' => 'Mob Life'));*/
	
	
	
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$cancel = new Zend_Form_Element_Submit('cancel');
		$cancel->setDecorators($this->submitDecorators)
				->setLabel('');

		$submit = new Zend_Form_Element_Submit('submitBtn');
		$submit->setAttrib('class', 'text_button_red');
		$submit->setLabel($translator->translate('form_button_submit'));
				
		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
	
	   
	    $this->addElement($start_row);
		$this->addElement($submit);
		$this->addElement($end_row);
	
	}
}
?>
