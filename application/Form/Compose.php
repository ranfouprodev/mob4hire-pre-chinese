<?php
class Form_Compose extends My_Form
{
	public function init()
	{
		$filterTrim = new Zend_Filter_StringTrim(); 
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));
		
		$to=new Zend_Form_Element_Text('contactsSelect');  
		$to->setLabel('To:');
		$to->setAttrib('size','60');
		$to->setAttrib('rel','#contactpop');
		
		
		$button=new Zend_Form_Element_Button('addcontact');
		$button->setLabel('Select From Contacts');
	//	$button->setAttribs(array('onclick'=>"showSelect(); return false;"));
		
		
		$validatorNotEmptySubject = new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::STRING);
		$validatorNotEmptySubject->setMessage(__('Subject cannot be black'));
		
		$subject= new Zend_Form_Element_Text('subject');
		$subject->setLabel('Subject:');
		$subject->setAttrib('size','80');
		$subject->addValidator($validatorNotEmptySubject);
		
		/*$message =  new Zend_Form_Element_Textarea('message');
		$message->setLabel('Message:');*/
		
		$message = new My_Form_Element_TinyMce('message');
		$message->addFilter($filterTrim)
					//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setAttrib('rows', '20')
					->setAttrib('cols', '80');
		
		$send = new Zend_Form_Element_Submit('sendbutton');
		$send->setLabel('Send')
		->setDecorators($this->buttonDecorators);


		$this->addElement($to);
		$this->addElement($subject);
		$this->addElement($message);
		$this->addElement($send);
		
	}
}
