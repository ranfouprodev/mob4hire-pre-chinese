<?php
class Form_MobileAddHandset extends My_Form
{
	
    public function init()
    {
        $this->addPrefixPath('My_Decorator',
                'My/Decorator/',
                'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
  

		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country');


		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Select Country First'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$manufacturer = new Zend_Form_Element_Select('vendor');
		$manufacturer->setLabel('Handset')
	//			->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->addMultiOptions(array(0=>'Select Model First'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically


		$addmodel = new Zend_Form_Element_Button('addmodel');
		$addmodel->setLabel('    Add    ');
		
	
		
		$this->addElement($country);
		$this->addElement($carrier);
			
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		
	
		
    }    
} //end class

?>