<?php
class Form_SubmitTestCase extends ZendX_JQuery_Form
{
	protected $idtest = 0;
	public $elementDecorators = array(
								'ViewHelper',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
							
	public $fileDecorators = array(
								'File',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
	public $buttonDecorators = array('ViewHelper', array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_input')));
	public $submitDecorators = array(
								'ViewHelper',
								array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'button')),
							);
	public $jQueryDecorators = array(
								array('UiWidgetElement', array('tag' => '')), // it necessary to include for jquery elements
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);

	
	public function __construct($idtest,  $options=NULL)
	{
		$this->idtest = $idtest;
		$this->addElementPrefixPath('My_Form_Decorator', 'My/Form/Decorator/', 'decorator');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		parent::__construct($options);
	
	}
	
	public function init()
	{
		
		$tests = new Tests(); 
		$test = $tests->getTest($this->idtest);
		
		$this->setName('testcase');

		$filterTrim = new Zend_Filter_StringTrim();
 
		$comments = new My_Form_Element_TinyMce('comments');
		$comments->addFilter($filterTrim)
					->setDecorators(array('MobForm'))
					->setRequired(true)
					->setLabel('Comments')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50')
					->setDescription('Please enter comments for the developer here');
		$comments->helpText='Please enter comments for the developer here';
		
		
		$uploadTestPlan = new My_Form_Element_FileUpload('tsc'.$test['idtest']);
		$uploadTestPlan->setDecorators(array('MobForm'))
				->setLabel('Upload Test Results')
				->setDescription('You can upload your test results here. Once the developer has had a chance to review the files they will release the payment to you');
				
		$uploadTestPlan->helpText='If you have any questions at any time feel free to contact us at support@mob4hire.com';
				
		$divuploadPlan=new My_Form_Element_myXhtml('divtsc'.$test['idtest']);
		$divuploadPlan_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadPlan_str .= '<div id="divtsc'.$test['idtest'].'"></div>';
		$divuploadPlan_str .= '</div>';
		$divuploadPlan_str .= '</div>';	
		$divuploadPlan->setContent($divuploadPlan_str);	


		 
		// Upload Test Plan
		$this->addElement($uploadTestPlan);
		$this->addElement($divuploadPlan);
		
		$id= new Zend_Form_Element_Hidden('id');
		$id->setValue($test->idproject);
		
		$file = new Zend_Form_Element_Hidden('file');
		$file->setValue($test->idproject);
		
		$ratingbar=new My_Form_Element_RatingBar('ratingbar');
		$ratingbar->setDecorators(array('MobForm'))
				->setLabel('Rate developer')
				->setDescription('Please rate the developer for their conduct during this test. This will help other mobsters in future to decide which projects to work on');
		$ratingbar->helpText='If you have any questions at any time feel free to contact us at support@mob4hire.com';
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$previous = new Zend_Form_Element_Submit('cancel');
		$previous->setLabel('')
				->setDecorators($this->submitDecorators);
		
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')
				->setDecorators($this->submitDecorators);

		$this->addElement($comments);
		$this->addElement($ratingbar);
		$this->addElement($start_row);
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);

		$this->addElement('hidden', 'return', array(
			'value' => Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),                         
                ));

		
		
        }
}
