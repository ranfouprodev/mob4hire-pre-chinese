<?php
class Form_AcceleratorStepFour extends My_Form
{
    public function init()
    {
        $this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$filterTrim = new Zend_Filter_StringTrim();
		
		$start_div_form_element = '<div class="form_element clearfix">';
		$start_div_helptip = '<div class="form_helptip">';
		$close_div = '</div>';
			
		$start_form_label = '<div class="form_label"><label>';
		$end_form_label = '</label></div>';
			
		$start_span_whats_this = '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a><p>';
		$close_span = '</p></span>';
			
		$start_td = '<td>';
		$start_label_td = '<td class="label">';
		$end_td = '</td>';
		$empty_td = '<td>&nbsp;</td>';
			
		$start_table_label = $start_label_td . $start_div_form_element . $start_form_label;
		$helptip = $end_form_label. $start_div_helptip. $start_span_whats_this;
		$close_label_tags = $close_span . $close_div . $close_div . $end_td;
	
		$projReview = new My_Form_Element_myXhtml('projReview');
		$projReview_str = $start_table_label;
		$projReview_str .= 'Project Review';
		$projReview_str .= $helptip;
		$projReview_str .= 'To see what your project will look like to a Mobster. review it. Go back and make edits to the project if you need to before submitting.';
		$projReview_str .= $close_label_tags;
		$projReview->setContent($projReview_str);
	
		$projReview2 = new My_Form_Element_myXhtml('projReview2');
		$review_desc = $start_td;
		$review_desc .= '<span class="mobblack">This open, flat fee project needs 20 Mobsters and involves 1 country and 2 carriers, with Nokia, Motorola and Samsung handsets running the Mobile Windows Platform.</span>';
		$review_desc .= $end_td;
		$projReview2->setContent($review_desc);
	
			
		$view_project_start_td = new My_Form_Element_myXhtml('view_project_start_td');
		$view_project_start_td->setContent('<td>');
	    
		$viewProject = new Zend_Form_Element_Button('viewProject');
		$viewProject->setLabel('VIEW PROJECT PUBLIC PROFILE')
					->setAttrib('onClick', 'window.open("/app/project/view/idproject/'.Zend_Registry::get('defSession')->project->idproject.'/popup/1", "Project Profile", "width=800, height=600,scrollbars=yes");');;
					//->setDecorators($this->buttonDecorators);
	    
		$view_project_end_td = new My_Form_Element_myXhtml('view_project_end_td');
		$view_project_end_td->setContent('</td>');
	    
		$currency = new My_Form_Element_myXhtml('currency');
		$currency_str = $start_table_label;
		$currency_str .= 'Currency';
		$currency_str .= $helptip;
		$currency_str .= 'Only one currency is currently supported. Multi-currency is coming in future releases of Mob4hire.';
		$currency_str .= $close_label_tags;
		$currency->setContent($currency_str);
	
		$currency2 = new My_Form_Element_myXhtml('currency2');
		$currency_desc = $start_td;
		$currency_desc .= '<span class="mobblack">All currency and money amounts are in United States Dollars (U.S.D.)</span>';
		$currency_desc .= $end_td;
		$currency_desc .= $empty_td;
		$currency2->setContent($currency_desc);
		
		$payMethod = new Zend_Form_Element_Radio('payMethod');
		$payMethod->addMultiOptions(array(1=>'Paypal', 2=>'Pay by cheque or wire transfer	(9% service charge will be added for manual processing)'))
					->setDecorators(array('MobFormTable'))
					->setLabel('Method Of Payment');
		$payMethod->setValue('1');
		$payMethod->helpText = 'All monies must be paid in full before testing can begin. In the case of Mobster bidding, additional funds will need to be deposited once bids are collected and accepted';
		
		
		
		$start_table = new My_Form_Element_myXhtml('start_table');
		$start_table->setContent('<table>');
		$end_table = new My_Form_Element_myXhtml('end_table');
		$end_table->setContent('</table>');
			
		$start_review_row = new My_Form_Element_myXhtml('start_review_row');
		$start_review_row->setContent('<tr>');
		$end_review_row = new My_Form_Element_myXhtml('end_review_row');
		$end_review_row->setContent('</tr>');
		
	
		$this->addElement($start_table);
	  	$this->addElement($start_review_row);
		$this->addElement($projReview);
		$this->addElement($projReview2);
		$this->addElement($view_project_start_td);	
		$this->addElement($viewProject);	
		$this->addElement($view_project_end_td);
		$this->addElement($end_review_row);
		
		$this->addElement($currency);
		$this->addElement($currency2);
		$this->addElement($payMethod);
		$this->addElement($end_table);
		
	
    }    
} //end class

?>