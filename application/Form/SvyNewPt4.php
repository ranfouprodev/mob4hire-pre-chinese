<?php
class Form_SvyNewPt4
{ 
    public $title ="<h3>Determine Costs</h3>";
    public $summary ="<p>Determine the cost of the survey. This will depend upon the number of desired respondents and how much you are willing to pay for each respondent to complete your survey.<br /><br />Below we have provided a way for you to determine the costs:<br /></p>";
    
    public function getForm(){
        
        $filterTrim = new Zend_Filter_StringTrim();
        
        $form = new Zend_Form;
        $form->setAction('/app/survey/rsrchrnew')->setMethod('post')->setAttrib('id','frmrschrnewPt4');
        
        $nores = new Zend_Form_Element_Text('nores');        
        $nores->setLabel('No. of Desired Respondents(')->addFilter($filterTrim);
        $nores->setDescription('X');
        $survcost = new Zend_Form_Element_Text('survcost');        
        $survcost->setLabel('Costs Per Survey')->addFilter($filterTrim);
        $survcost->setDescription(')+');       
        $survpkg = new Zend_Form_Element_Text('survpkg');  
        $survpkg->setLabel('MobViral Survey Package')->addFilter($filterTrim);
        $survpkg->setDescription('=');
        $total = new Zend_Form_Element_Text('total');        
        $total->setLabel('Grand Total')->addFilter($filterTrim);
        
        $form->addElement($nores);
        $form->addElement($survcost);
        $form->addElement($survpkg);
        $form->addElement($total);
        
        return $form;
    }//end getForm        
} //end class
?>