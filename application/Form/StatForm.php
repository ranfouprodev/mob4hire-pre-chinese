<?php
class Form_StatForm extends My_Form
{
	public function init()
	{
		$this->setAction('/admin/project/dog')->setMethod('POST')->setAttrib('id','frmStatForm');
		$projects=new Projects();
		$whereClause = "projects.Status = 201";
		$projectList=$projects->listProjects($whereClause,'StartDate desc',1,0);
		$selectArray = array();
		$select1 = new Zend_Form_Element_Select('select1');
		foreach($projectList as $project)
		{
			$select1->addMultiOption($project->idproject,$project->Name);
			
		}
		
		$flag= new Zend_Form_Element_Hidden('selectedFlag');
		$flag->setValue(1);
        
        $select1->setLabel('Select the project to send an email too.');
		$select1->setDecorators(array('MobForm'));	
		$select1->setValue(50)->setRequired(true);
		$select1->helpText = 'From this list select a project that you wish to send an invite email out for.';

		
        
        $goButton = new Zend_Form_Element_Submit('button');
        $goButton->setLabel('Find the dudes');
		
        $this->addElement($select1);
        $this->addElement($goButton);
        $this->addElement($flag);
        
	}
}