<?php
class Form_ProfileResearcher extends My_Form
{
	public function init(){
        
  // $this->setAction('/app/profile/researcher')->setMethod('post')->setAttrib('id','frmProfileResearcher');
	$filterTrim = new Zend_Filter_StringTrim();
	
	$notifyEmail = new Zend_Form_Element_MultiCheckbox('notify_email');
	$notifyEmail->setLabel('By Email.')->setDecorators(array('MobForm'));
	$notifyEmail->setDescription('What type of messages do you wish to receive on email?');
	$notifyEmail->helpText='We can send as much or as little email as you want / need to work with the Mob4Hire community. Please note that Mob4Hire Alerts is mandatory as per the T\'s and C\'s of Mob4Hire.';
	$notifyEmail->addMultiOptions(array(1 => 'Everytime a message is submitted to a project I posted.',2 => 'Daily digest of emails.',3 => 'Mob4Hire Newsletters.'));
	
	$notifySMS = new Zend_Form_Element_MultiCheckbox('notify_sms');
	$notifySMS->setLabel('By SMS.')->setDecorators(array('MobForm'));
	$notifySMS->setDescription('What type of messages do you wish to receive on SMS?');
	$notifySMS->helpText='Your setting to receive notifications by SMS';
	$notifySMS->addMultiOptions(array(1 => 'Daily digest of emails.',2 => 'As a final reminder for things I should be taking care of!'));
	
	$this->addElement($notifyEmail);
	$this->addElement($notifySMS);
	
	$this->addDisplayGroup(array('notify_email','notify_sms'),
                                       'Notifications',
                                       array('legend' => 'Notifications'));
	
	$stDate =  new My_Form_Element_DateSelects('start_date');
        $validatorDate = new Zend_Validate_Date();
        $validatorDate->setMessages(
            array(
                 Zend_Validate_Date::INVALID        => __("'%value%' does not appear to be a valid date"),
                Zend_Validate_Date::FALSEFORMAT    => __("'%value%' does not fit given date format")
            )
        );
	$stDate->setLabel('When Did The Company Start?')->addValidator($validatorDate)->setDecorators(array('MobForm'));
	$stDate->helpText='Please enter the date your comapany started.';
        $stDate->setStartEndYear(1957, date("Y"))->setReverseYears(true);
	
	$numEmployees = new Zend_Form_Element_Select('num_employees');
	$numEmployees->setLabel('How Many People Are Working In Your Company?')->setDecorators(array('MobForm'));
	$numEmployees->helpText='&nbsp';
	$numEmployees->addMultiOptions(array(1 => 'Rather Not Say.',2 => '1 (me!)',3 => '2-5',4 => '6-10',5 => '11-25',6 => '26-100',7 => '101-500',8 => '500+'));
	
	$this->addElement($stDate);
	$this->addElement($numEmployees);
	
	$this->addDisplayGroup(array('start_date','num_employees'),
                                       'about company',
                                       array('legend' => 'Tell Us About Your Company'));
        /*
        $company = new Zend_Form_Element_Text('company_name');
	$company->setLabel('Company Name:')->addFilter($filterTrim);
    
	$position = new Zend_Form_Element_Text('title');        
	$position->setLabel('Your Job Title:')->addFilter($filterTrim);
	
	$platform = new Zend_Form_Element_Select('platform');
	$platform->setLabel('Targeted Mobile Platform:');
	$platform->addMultiOptions(array('blackberry' => 'Blackberry'));

	$numEmployees = new Zend_Form_Element_Select('employees_number');
	$numEmployees->setLabel('Number Of Employees:');
	$numEmployees->addMultiOptions(array('0_10' => '0-10', '11_30' => '11-30'));
	
	$interest = new Zend_Form_Element_Select('software_type');
	$interest->setLabel('Type of Software Development:');
	$interest->addMultiOptions(array('1' => 'Games','2' => 'Entertainment','3' => 'Utilities','4' => 'Social Networking','5' => 'Productivity','6' => 'Reference','7' => 'Sports','8' => 'Navigation','9' => 'Health & Fitness','10' => 'Other'));
	
	$notes =  new Zend_Form_Element_Textarea('profile_notes');
	$notes->setLabel('Profile Notes / Highlights:');
        
        $this->addElement($company);
	$this->addElement($position);
	$this->addElement($platform);	
	$this->addElement($numEmployees);
	$this->addElement($interest);
	$this->addElement($notes);
        
        $this->addDisplayGroup(array('company_name', 'title', 'platform', 'employees_number', 'software_type', 'profile_notes'),
                                       'Company Info',
                                       array('legend' => 'Company Info'));
        
	
	$campaign_complete = new Zend_Form_Element_Radio('campaign_complete');
	$campaign_complete->setLabel('Please notify me when a survey campaign has been completed by all participating mobsters:');
	$campaign_complete->addMultiOptions(array(1 => 'Yes',2 => 'No'));
	$campaign_complete->setvalue(1);
        
        $this->addElement($campaign_complete);
        $this->addDisplayGroup(array('campaign_complete'),
                                       'Researcher Notification Setting',
                                       array('legend' => 'Researcher Notification Setting'));
	*/
        
	$start_row = new My_Form_Element_myXhtml('start_single_row');
	$start_row->setContent('<div class="submit_buttons clearfix">');

	$cancel = new Zend_Form_Element_Submit('cancel');
	$cancel->setLabel('')
			->setDecorators($this->submitDecorators);
         
	$save = new Zend_Form_Element_Submit('submit');
	$save->setLabel('')
			->setDecorators($this->submitDecorators);
		
	$end_row = new My_Form_Element_myXhtml('end_single_row');
	$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
	
	
	$this->addElement($start_row);
	$this->addElement($save);
	$this->addElement($cancel);
	$this->addElement($end_row);
            
        }//end init
}//end class


?>