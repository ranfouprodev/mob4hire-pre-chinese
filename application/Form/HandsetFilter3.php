<?php
class Form_HandsetFilter3 extends My_Form
{
	public function init()
	{	

		//$countries=new Countries();
		//$countrylist=array_merge(array(0=>'Any'),$countries->listCountries()); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country');
		
//		foreach($countrylist as $id=>$countryname) {
//				if ($id=='0')
//					$country->addMultiOption($id, $countryname);
//				else
//					$country->addMultiOption(substr($id,7), $countryname);
//		}
		
		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$networktable=new My_Form_Element_myXhtml('networktable');
		$networktable_str = '<div id="networktable"></div>';
		$networktable_str .= '</div>';
		$networktable_str .= '</div>';
		$networktable->setContent($networktable_str);

		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel('Platform')
				->addMultiOptions(array(0=>'Any', 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(0=>'Any'), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel('Handset');
				//->addMultiOptions($vendors)
				//->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically
		
				
		//$user = new Zend_Form_Element_Text('userlike');
		//$user->setLabel('User Match');
		
		$filterdata = new Zend_Form_Element_Text('filterdata');
		$filterdata->setLabel('Search Data');
		
		$filterArray = array('u.username'=>'Username','p.city'=>'City','p.state'=>'State','p.company'=>'Company');
		$filtertype = new Zend_Form_Element_Select('filtertype');
		$filtertype->setLabel('Filter Select')
				->addMultiOptions($filterArray)
				->setRegisterInArrayValidator(false); 
		
		$invite = new Zend_Form_Element_Select('showinvites');
		$invite->setLabel('Show Invited')
				->addMultiOptions(array('2'=>'Yes','1'=>'No'))
				->setRegisterInArrayValidator(false)
				->setValue('1'); 
		
		$submit= new Zend_Form_Element_Button('filter');
		$submit->setLabel('FILTER')
			->setAttrib("class","button_single_height");
		
		$this->addElement($country);
		$this->addElement($carrier);
		//$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($invite);
		//$this->addElement($user);
		$this->addElement($submit);
		$this->addElement($filtertype);
		$this->addElement($filterdata);
        }
}
?>