<?php
class Form_Comments extends My_Form
{
	public function init()
	{
		$this->addPrefixPath('My_Decorator',
	                        'My/Decorator/',
	                        'decorator');
	
		$filterTrim = new Zend_Filter_StringTrim();
	
		$comment = new My_Form_Element_TinyMce('comment');
		// adding the MobForm decorator makes this not work in some places. leave out for now
		$comment->addFilter($filterTrim)
//					->setDecorators(array('MobForm'))
					//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setAttrib('rows', '2')
					->setAttrib('cols', '40');	
			
				 
		$button = new Zend_Form_Element_Button('addcomment');
	        $button->setLabel('      Post      ')
				->setDecorators($this->buttonDecorators)
				->setOptions(array('onclick'=>'postComment();'));		 
				 
		$divcomment=new My_Form_Element_myXhtml('commenttable');
		$divcomment_str = '<div id="commentstable"></div>';
		$divcomment->setContent($divcomment_str);				 
	
				      
		$this->addElement($comment);
		$this->addElement($button);
		//$this->addElement($divcomment);
	
	
 	}
}
