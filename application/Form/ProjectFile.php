<?php 
class Form_ProjectFile extends ZendX_JQuery_Form {
	
	
	protected $idproject = 0;
	public $elementDecorators = array(
								'ViewHelper',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
							
	public $fileDecorators = array(
								'File',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
	public $buttonDecorators = array('ViewHelper', array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_input')));
	public $submitDecorators = array(
								'ViewHelper',
								array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'button')),
							);
	public $jQueryDecorators = array(
								array('UiWidgetElement', array('tag' => '')), // it necessary to include for jquery elements
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);

	
	public function __construct($idproject, $options=NULL)
	{
		$this->idproject = $idproject;
		$this->addElementPrefixPath('My_Form_Decorator', 'My/Form/Decorator/', 'decorator');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		parent::__construct($options);
	
	}
	
	public function init() {
		
		
		$this->addPrefixPath('My_Decorator',
                      'My/Decorator/',
                      'decorator'); // todo: Add this is My_Form instead of each form which extends it
		
	
		$filterTrim = new Zend_Filter_StringTrim();
        
	
		$projects = new Projects(); 
		$project = $projects->getProject($this->idproject);
	
	
				
	
		$divuploadPlan=new My_Form_Element_myXhtml('divuploadPlan');
		$divuploadPlan_str = '<div class="form_element clearfix"><div class="form_label"><label>Test Plans</label><div class="form_description"><p>These are the directions to complete the test</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadPlan_str .= '<div id="divuploadPlan"><img src="/images/working.gif"/> Loading..</div>';
		$divuploadPlan_str .= '</div>';
		$divuploadPlan_str .= '</div>';	
		$divuploadPlan->setContent($divuploadPlan_str);	

    

	

		   
		$divtestOutput=new My_Form_Element_myXhtml('divtestOutput');
		$divtestOutput_str = '<div class="form_element clearfix"><div class="form_label"><label>Instructions for Reporting</label><div class="form_description"><p>This is how the results should be displayed</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divtestOutput_str .= '<div id="divtestOutput"><img src="/images/working.gif"/> Loading..</div>';
		$divtestOutput_str .= '</div>';
		$divtestOutput_str .= '</div>';	
		$divtestOutput->setContent($divtestOutput_str);	
		

		
		
  
		$divgenFiles=new My_Form_Element_myXhtml('divgenFiles');
		$divgenFiles_str = '<div class="form_element clearfix"><div class="form_label"><label>Application Files</label><div class="form_description"><p>These files are to be installed on your handset</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divgenFiles_str .= '<div id="divgenFiles"><img src="/images/working.gif"/> Loading..</div>';
		$divgenFiles_str .= '</div>';
		$divgenFiles_str .= '</div>';	
		$divgenFiles->setContent($divgenFiles_str);	
		
		
	
		// Get the table for supported devices
		$supporteddevices= new SupportedDevices();
		$specificuploads=array();
		if ($devices=$supporteddevices->getSupportedDevices($this->idproject))
		{
			foreach($devices as $device)
			{
					
				$elementname='dev'.$device['platform'].$device['vendor'].$device['tid'];
		
				$tablediv=$specificuploads[][1]=new My_Form_Element_myXhtml('div'.$elementname);
				$tablediv_str = '<div class="form_element clearfix"><div class="form_label"><label>Specific Files For ';
				$tablediv_str .= substr($device->platform,2).'/'.(($device->vendor)? $device->vendor : '').'/'.(($device->model)? $device->model : 'Any');
				$tablediv_str .= '</label><div class="form_description"><p>These files are to be installed on your handset</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
				$tablediv_str .= '<div id="div'.$elementname.'"><img src="/images/working.gif"/> Loading..</div>';
				$tablediv_str .= '</div>';
				$tablediv_str .= '</div>';			
				$tablediv->setContent($tablediv_str);
				
					

			}
		}

		
		
		
			
		$divndaFile=new My_Form_Element_myXhtml('divndaFile');
		$divndaFile_str = '<div class="form_element clearfix"><div class="form_label"><label>NDA Files</label><div class="form_description"><p>This NDAs are required by the developer before work can be started</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divndaFile_str .= '<div id="divndaFile"><img src="/images/working.gif"/> Loading..</div>';
		$divndaFile_str .= '</div>';
		$divndaFile_str .= '</div>';
		$divndaFile->setContent($divndaFile_str);	
	
		
		
		
		$divscreenShots=new My_Form_Element_myXhtml('divscreenShots');
		$divscreenShots_str = '<div class="form_element clearfix"><div class="form_label"><label>Screenshots</label><div class="form_description"><p></p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divscreenShots_str .= '<div id="divscreenShots"><img src="/images/working.gif"/> Loading..</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots->setContent($divscreenShots_str);
			
		
		$divdocumentation=new My_Form_Element_myXhtml('divdocumentation');
		$divdocumentation_str = '<div class="form_element clearfix"><div class="form_label"><label>Additional Support Documentaion</label><div class="form_description"><p>These files will help with your testing</p></div></div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divdocumentation_str .= '<div id="divdocumentation"><img src="/images/working.gif"/> Loading..</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation->setContent($divdocumentation_str);
		 
		// Upload Test Plan

		
		$this->addElement($divuploadPlan);
		
		
		$this->addElement($divtestOutput);
		
		$this->addElement($divgenFiles);
		
		
		//create dynamic text fields here based on results from getSupportedDevices(projectID);
		foreach($specificuploads as $upload)
		{
			foreach($upload as $element)
			{			
					$this->addElement($element);	
					
		
			}
		}
		
		
		$this->addElement($divndaFile);

		$this->addElement($divscreenShots);
		
		$this->addElement($divdocumentation);
		
  
	}
}