<?php
class Form_ExtendDateForm extends My_Form
{
	public function init()
	{
		$validatorDate2 = new Zend_Validate_Date();
		$validatorDate2->setMessages(array(Zend_Validate_Date::INVALID=>__("'%value%' does not appear to be a valid date"), Zend_Validate_Date::FALSEFORMAT=>__("'%value%' does not fit given date format")));

		$dateFuture=new My_Validate_CompareDate(); // if no argument passed then CompareDate compares with today's date
		$dateFuture->setMessage(__('Latest end date is today.'));
        
		$testenddate = new ZendX_JQuery_Form_Element_DatePicker('testenddate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$testenddate->addValidator($validatorDate2)
				->addValidator($dateFuture)
				->setDecorators(array('MobLabel'
						, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
						, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
						,'Errors')
				->setRequired(false)
				->setLabel('Select new bidding end date')
				->setAttrib("readonly","readonly");
		$testenddate->helpText='New end date for project bidding.'; 
		$testenddate->setValue(date("Y-m-d", strtotime("+14 days")));
		$goButton = new Zend_Form_Element_Submit('goButton');
		$goButton->setLabel('EXTEND BIDDING')
				 ->setAttrib("class","button_double_height")
				->setDecorators($this->submitDecorators);
		$this->addElement($testenddate);
		$this->addElement($goButton);
	}
}