<?php 
class Form_AdminProject extends ZendX_JQuery_Form {
	
	
	protected $idproject = 0;
	public $elementDecorators = array(
								'ViewHelper',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
							
	public $fileDecorators = array(
								'File',
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);
	public $buttonDecorators = array('ViewHelper', array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_input')));
	public $submitDecorators = array(
								'ViewHelper',
								array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'button')),
							);
	public $jQueryDecorators = array(
								array('UiWidgetElement', array('tag' => '')), // it necessary to include for jquery elements
								'Errors',
								array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
								array('Label', array('tag' => 'td', 'escape' => false)),
								array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
							);

	
	public function __construct($idproject, $options=NULL)
    {
        $this->idproject = $idproject;
		$this->addElementPrefixPath('My_Form_Decorator', 'My/Form/Decorator/', 'decorator');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		parent::__construct($options);
	
    }
	
	public function init() {
		
		
		$this->addPrefixPath('My_Decorator',
                      'My/Decorator/',
                      'decorator'); // todo: Add this is My_Form instead of each form which extends it
		
		$this->setName('admin');
		
		$filterTrim = new Zend_Filter_StringTrim();
        
		
				
		/*
		* Test Class 
		*/
		$status = new Zend_Form_Element_Select('status');
		$status->addMultiOptions(Projects::$status)
				->setDecorators(array('MobForm'))
				->setLabel('Status');
		
		
		$type = new Zend_Form_Element_Text('projecttype');
		$type->addFilter($filterTrim)
					->setDecorators(array('MobForm'))
					->setLabel('Type')
					->setDescription('test = Mobtest, survey = Mobsurvey');
		
		
		
		/*
		*  Test Package
		*/
		$testPkg = new Zend_Form_Element_Radio('testPkg');
		$testPkg->addMultiOptions(array(1=>'Basic ', 2=>'Premium ', 3=>'Managed'))
				->setLabel('<span class="mobblack">MOB</span><span class="mobred">TEST</span> Testing Package')
		  		->setDecorators(array('MobForm'))	
				->setDescription('Choose a package that suits your needs')
				->setValue(1);
		$testPkg->helpText = '<img src="/images/banner-product-info-mobtest.gif"/>';
        
		/*
		 * Project Name
		 */
		$projName = new Zend_Form_Element_Text('projName');
        
		$projects = new Projects();
		$idproject=$this->idproject;

	// Won't work if we use the page for updates
	//	$validatorUniqueProject = new My_Validate_ProjectUnique($projects, 'Name', $idproject);
	//		$validatorUniqueProject->setMessage(__('This project name has already been used, please choose another one.'));
        
		$projName->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Project Name')
				 ->setAttrib('size', '50')
	//			 ->addValidator($validatorUniqueProject)
				 ->setDescription('Choose a unique project name');			 
 		$projName->setValue('Project Name');
		$projName->helpText = 'The name of your project is how it’s identified throughout the system. It cannot be changed once your project goes live. Use your app name, your company name, the phase of the moon of whatever it needs to be to attract Mobster’s attention.';
        
		/*
		 * Project Image Uploader
		 
		$projImg = new My_Form_Element_ImageUpload('projImg');
		$projImg->setDecorators(array('MobForm'))
				->setLabel('Project Image')
				->setDescription('Market your project! Upload an image that will be associated with it.');
		$projImg->helpText='Upload an image that will be associated with your project. Maximum 300kb and should be png, gif or jpeg';
*/
		
		
		/*
		 * Project Descriptions
		 */    
		$projDesc = new Zend_Form_Element_Text('projDesc');
		$projDesc->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel('Short Description(140 chars max)')
				 ->setAttrib('size', '50')
				 ->setAttrib('maxLength', 140)
				 ->setDescription('Give enough information to describe the test so someone can decide whether they want to help you with it or not.');
		$projDesc->helpText='A short description of your project. It should be compelling, descriptive and to the point. Make it short and snappy; it’s used for project listings and Twitter / SMS promotions.';				 
				 
				 
		$projDescFull = new My_Form_Element_TinyMce('$projDetail');
		$projDescFull->addFilter($filterTrim)
				//	->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setDecorators(array('MobForm'))
					->setLabel('Description')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50')
					->setDescription('For the project profile. Give enough information to describe the test');
		$projDescFull->helpText='This description is shown publicly to anybody that wants to read it. It’s an overview of your project, including what you’re trying to accomplish and a summary of what you want the Mobster to do. This ISN’T your full test plan / script … you’ll take care of that in step 3 “Resources.”   Does your project description have enough information AND is it compelling enough for Mobsters to want to complete this project?';
        
		/*
		* Invitation Type
		*/
		$inviteType = new Zend_Form_Element_Radio('inviteType');
		$inviteType->addMultiOptions(array(0=>'Open (anybody that qualifies)', 1=>'Closed to specific mobsters I\'ll select. (Requires Premium or Managed Project)'))
					->setDecorators(array('MobForm'))
					->setLabel('Type Of Invitation')
					->setDescription('Who gets to participate in your project? (Select a premium or managed project to be able to choose “Closed”)');
		$inviteType->setValue(0);
		$inviteType->helpText="An Open invitation means that anybody who qualifies for the project can apply to participate. To “qualify,” it means they must match the criteria you select in Step 2: Panel Selection. A Closed invitation assumes you’ve built a list of Mobsters in your Contact list … those are the people you can hand-pick to do the test. Closed Invitations are useful for regression testing, or if you’ve built trust and rapport with Mobsters who have done work for you before.";
		
		/*
		* Mobster Invitation Email 
		*/
		$mobInvite = new Zend_Form_Element_Textarea('mobInvite');
		$mobInvite->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel('Mobster Invitation')
				->setOptions(array('cols'=>'50'))
				->setDescription('This is add to the email that is sent to Mobsters that have elected to receive project information');
		$mobInvite->setValue('Thank you for agreeing to be a mobile tester for Mob4hire. Our test matches your handset(s) and we would like for you to help us out');
		$mobInvite->helpText="This is the email that is sent to the mobsters who qualify to do the project.";
		
		
		/*
		* Transaction Type and Bidding Amount
		*/
		$transType = new Zend_Form_Element_Radio('transType');
		$transType->addMultiOptions(array(0=>'Open Bids with Suggested Amounts', 1=>'Flat fee. How much will you pay per completed test? (Requires Premium or Managed Project)'))
					->setDecorators(array('MobForm'))
					->setLabel('Type Of Transaction')
					->setDescription('Choose a type of payment exchange and processing. (Select a premium or managed project to be able to choose “Flat Fee”)');
		$transType->setValue(0);
		$transType->helpText='Flat fee means you will pay a specified amount per completed test and it’s non-negotiable. Bidding means that you would like Mobsters to bid on your project; you will be expecting to get a range of bids that you must then accept, negotiate or reject. Bidding can vary depending on region, demographic of person, the cost of their data and voice plans, etc…  You can optionally put a “suggested bid” in the next field to help guide the Mobsters so that you don’t receive $500 bids on a $50 project. Bidding takes more Mobster management than Flat fee projects. Be careful you don’t under price your project or you won’t get any Mobsters interested in helping you!';
        
		$amtMobsters = new Zend_Form_Element_Text('amtMobsters');
		$amtMobsters	->addFilter($filterTrim)
  						->setDecorators(array('MobForm'))
						->setLabel(' How Many Mobsters')
						->setAttrib('size','10')
						->setValue("5")
						->addValidator(new Zend_Validate_Int())
						->addValidator(new Zend_Validate_LessThan('500'))
						->setDescription('Select the number of Mobsters you will need to complete your project.');
		$amtMobsters->helpText = 'What is the total number of people/handsets you need to run the test? Open to Premium Tests Only';
		$amtMobsters->setValue('5');
		
		
		$bidAmount = new Zend_Form_Element_Text('bidAmount');
		$bidAmount->addFilter($filterTrim)
				->setDecorators(array('MobForm')
				, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> '', 'id' => 'bidamount')))
				->setLabel('Price Per Test ($USD)')
				->setDescription('If this is a Flat Fee project, then enter the amount you are willing to pay for a completed project, per person. If this is a Bidding project, you can optionally suggest an amount you think is fair to help guide the Mobster in their bid.');
        
		$bidAmount->addFilter($filterTrim);
		$bidAmount->helpText='This will give the testers an estimate of where to start bidding';
		
				
		/*
		* Dates 
		*/
		$validatorDate = new Zend_Validate_Date();
		$validatorDate->setMessages(array(Zend_Validate_Date::INVALID=>__("'%value%' does not appear to be a valid date"), Zend_Validate_Date::FALSEFORMAT=>__("'%value%' does not fit given date format")));
        
		$sDate = new ZendX_JQuery_Form_Element_DatePicker('sDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$sDate//->addValidator($validatorDate)
			// ->setDecorators($this->jQueryDecorators)
			->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
						)
			  ->setLabel('Listing Start Date')
			  ->setAttrib("readonly","readonly")
			  ->setDescription("When do you want the project to start?");
		$sDate->setValue(date("Y-m-d"));	
		$sDate->helpText='This is the date the project will start being displayed to Mobsters. If you want to start right away and set it to today’s date (or before), please note that Mob4Hire will still need to approve the project before it gets listed.';  
			  
        
		$eDate = new ZendX_JQuery_Form_Element_DatePicker('eDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$eDate//->addValidator($validatorDate)
			//->setDecorators($this->jQueryDecorators)
			->setDecorators(array('MobLabel'
									,array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
									,array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
								)
			->setLabel('Listing End Date')
			->setAttrib("readonly","readonly")
			->setDescription('These are the dates the project listing will remain active for recruiting Mobsters.');
		$eDate->setValue(date("Y-m-d", strtotime("+14 days")));	
		$eDate->helpText='These are the dates your project is listed publicly; Mobsters can apply to help with the project, can begin work and return results during this period.';
		
		
		/*
		* Project Duration
		*/
		$projDur = new Zend_Form_Element_Text('projDur');
		$projDur->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel('Project Duration (days)')
				->setDescription('The number of days the Mobsters have to complete the project AND return results to you after the Listing End Date.');
		$projDur->helpText="Once your project listing is done, and Mobsters can no longer apply, they should still be given a bit of time to complete the project and return their results to you. This can be as short as 1 day. If left blank, we assume that you expect the Mobsters to finish by the Listing End Date. If Mobster results aren’t returned by the end of their duration, they do not get paid.";

        
		/*
		* Test Type
		*/
		$testType = new Zend_Form_Element_MultiCheckbox('testType');
		$testType->addMultiOptions(array('Functional'=>'Functional', 'Usability'=>'Usability'))
				->setDecorators(array('MobForm'))
				->setLabel('Type Of Test')
				->setDescription('Select one or both');
		$testType->setValue(1);
		$testType->helpText ='With functional testing, you get answers to questions like “Does it crash?” and “Does it take advantage of the device special features like trackballs, touchscreens and high-resolution camera’s?” In usability testing, you get answers to questions like “Does the menu structure make sense?” or “Is it fun to play?” and “Would you give it a good review on an app store?”  Many projects require both types of feedback, but many also do not!';
		
		/*
		* Test Category
		*/
		$testCategory = new Zend_Form_Element_MultiCheckbox('testCategory');
		$testCategory->addMultiOptions(array('News'=>'News', 'Medical'=>'Medical', 'Reference'=>'Reference', 'Productivity'=>'Productivity',
			 'Navigation'=>'Navigation', 'Health and Fitness'=>'Health and Fitness', 'Education'=>'Education',
			  'Weather'=>'Weather', 'Education'=>'Education', 'Business'=>'Business', 'Music'=>'Music', 'Finance'=>'Finance'
			  , 'Sports'=>'Sports', 'Travel'=>'Travel', 'Utilities'=>'Utilities', 'Games'=>'Games', 'Social Networking'=>'Social Networking'
			  , 'Books'=>'Books', 'Entertainment'=>'Entertainment', 'Lifestyle'=>'Lifestyle'))
				->setDecorators(array('MobForm'))
				->setLabel('Test Category')
				->setDescription('Check all that apply. Try to select just the major functions of your program.');
		$testCategory->helpText ='These common categories help Mobsters understand what your project is about, and helps them filter projects when looking for specific types.';
		


		/*
		* Test Class 
		*/
		$testClass = new Zend_Form_Element_Select('testClass');
		$testClass->addMultiOptions(array('Easy'=>'Easy', 'Moderate'=>'Moderate', 'Average'=>'Average', 'Advanced'=>'Advanced', 'Difficult'=>'Difficult'))
				->setDecorators(array('MobForm'))
				->setLabel('Test Classification')
				->setDescription('The classification should identify the skill level you think will be required to complete the test.');
		$testClass->helpText='These classifications help to give guidance to Mobsters whether they think they could complete the project. Use your best judgment: “Easy” should mean it’s a quick test, and doesn’t require a lot of technical expertise. “Difficult” means it’s an involved test, with lots of steps and permutations, and requires a very experienced tester with technical background.';

        
		/*
		* MobStar Rating
		*/
		$mobRating = new Zend_Form_Element_Radio('mobRating');
		$mobRating->addMultiOptions(array(1=>'Yes', 2=>'No'))
				->setDecorators(array('MobForm'))
				->setLabel('<span class="mobblack">MOB</span><span class="mobred">STAR</span> Rating?')
				->setDescription('The Mobsters are happy to provide this service free of charge.');
		$mobRating->helpText='A MOBSTAR rating will give you a preview of how the Mobster would rate your application on an appstore if they were to download it. This can be very helpful in choosing the features to launch with';
		$mobRating->setValue(1);
        
		/*
		* Time Estimate
		*/
		$estTime = new Zend_Form_Element_Text('estTime');
		$estTime->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel('Estimated Time to Complete The Test')
				->setDescription('How long will the test(s) take to run from start to finish?');
		$estTime->helpText='A Mobster rating will give you a preview of how the Mobster would rate your application on an app store if they were to download it. It includes both a 5 star rating and a short review statement. This can be very helpful in choosing the features to launch with or fixing some fundamental problems BEFORE launching if you get only one or two stars. ';
			
			

		/*
		* Estimated Data Usage
		*/
		$estData = new Zend_Form_Element_Text('estData');
		$estData->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setLabel('Estimated Data Usage for the test (kb)')
			->setDescription('How much data (kb) will be used during the test?');
		$estData->helpText='Since data plans vary SO much from person to person, carrier to carrier and country to country, you need to tell the Mobster how much data they can expect will be transferred during the project. This helps them estimate how much the data will cost for the test so they can price themselves correctly. This includes app download, as well as data Tx and Rx. Examples: “100 Kbytes,”  “2 Mbytes,” “More than 5 Mbytes; unlimited data plan is required.”';
			
		/*
		* Other Requirements
		*/
		$otherReq = new Zend_Form_Element_Textarea('otherReq');
		$otherReq->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel('Other Requirements')
				->setOptions(array('cols'=>'50'))
				->setDescription('What other requirements will the Mobster need to complete the test');
		$otherReq->helpText='What other things does the user need to run the test? Selection of the specific carriers, handsets and networks will take place on the next page';
		
		
		/*
		* Tags
		*/
		$tags = new Zend_Form_Element_Text('tags');
		$tags->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setLabel('Tags')
			->setAttrib('size', '50')
			->setDescription('Enter tags separated by commas.');
		$tags->helpText='Tags are useful for Mobsters to find your project through search results';
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak->setContent('<hr />');
		
		
		/*
		 * Number of Mobsters
		 */
		$amtMobsters = new Zend_Form_Element_Text('amtMobsters');
		$amtMobsters	->addFilter($filterTrim)
  						->setDecorators(array('MobForm'))
						->setLabel(' How Many Mobsters')
						->setAttrib('size','10')
						->setValue("5")
						->setDescription('Select the number of Mobsters you will need to complete your project.');
		$amtMobsters->helpText = 'What is the total number of people/handsets you need to run the test? Basic projects are limited to 10 Mobsters per test';
		$amtMobsters->setValue('5');


		$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element clearfix">';
		$heading1_str .= '<div class="form_label"><label>Network Carriers</label></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>Select the network carriers on which you want to test. Add as many as you want.</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);

		$countries=new Countries();
		$countrylist=array_merge(array(0=>'Any'),$countries->listCountries()); 
		$country = new Zend_Form_Element_Select('country');
		$country->setLabel('Country')
				->setDecorators(array('MobForm'));
		foreach($countrylist as $id=>$countryname) {
				if ($id=='0')
					$country->addMultiOption($id, $countryname);
				else
					$country->addMultiOption(substr($id,7), $countryname);
		}

		$carrier = new Zend_Form_Element_Select('network');
		$carrier->setLabel('Network')
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addcarrier = new Zend_Form_Element_Button('addcarrier');
		$addcarrier->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);

		$networktable=new My_Form_Element_myXhtml('networktable');
		$networktable_str = '</div>';
		$networktable_str .= '</div>';
		$networktable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$networktable_str .= '<div id="networktable"></div>';
		$networktable_str .= '</div>';
		$networktable_str .= '</div>';
		$networktable->setContent($networktable_str);

		$heading2 = new My_Form_Element_myXhtml('heading2');
		$heading2_str = '<div class="form_element clearfix">';
		$heading2_str .= '<div class="form_label"><label>Platforms & Handsets</label></div>';
		$heading2_str .= '<div class="form_helptip">';
		$heading2_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading2_str .= '<p>Are you targetting specific devices with your test?</p>';
		$heading2_str .= '</span>';
		$heading2_str .= '</div>';
		$heading2_str .= '<div class="form_input">';
		$heading2->setContent($heading2_str);

		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel('Platform')
				->setdecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any', 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(0=>'Any'), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel('Handset')
				->setDecorators(array('MobForm'))
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->setDecorators(array('MobForm'))
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$addmodel = new Zend_Form_Element_Button('addmodel');
		$addmodel->setLabel('    Add    ')
				->setDecorators($this->buttonDecorators);
		
		
		$handsettable=new My_Form_Element_myXhtml('handsettable');
		$handsettable_str = '</div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$handsettable_str .= '<div id="handsettable"></div>';
		$handsettable_str .= '</div>';
		$handsettable_str .= '</div>';
		
		$handsettable->setContent($handsettable_str);
				
		
		
		

		$region = new Zend_Form_Element_Textarea('region');
		$region->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel('Other Search Criteria')
		    ->setOptions(array('cols' => '50'));
		$region->setValue('Looking for testers in San Francisco, Chicago, New York City and in Uzbekistan.');
		$region->helpText = 'Use this field to target a specific region like a city, state, province, territory or ...?';

 		
		/*
		 * Other Selection Criteria: Why do we have two of these?
		 */
		$region = new My_Form_Element_TinyMce('$region');	
		$region->addFilter($filterTrim)
			->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
			->setDecorators(array('MobForm'))
			->setLabel('Other Selection Criteria')
			->setAttrib('rows', '10')
			->setAttrib('cols', '50')
			->setDescription('You can specify any other demographic or geographic constraints here. ')
			->setValue('None');
		$region->helpText='Use this free form field to target a specific region like a city or state or select Mobsters by demographic such as age or income level';


$uploadTestPlan = new My_Form_Element_FileUpload('uploadPlan');
		$uploadTestPlan->setDecorators(array('MobForm'))
				->setLabel('Upload Test Plan')
				->setDescription('The test plan is one of the key elements of a successful test; as they say "Garbage In Garbage Out." The secret to a good one is to think like a user. What steps do Mobsters need to do in order to accomplish your goals?');
		$uploadTestPlan->helpText='Tell Mobsters step-by-step exactly what you want them to do.';
				
	
		$divuploadPlan=new My_Form_Element_myXhtml('divuploadPlan');
		$divuploadPlan_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadPlan_str .= '<div id="divuploadPlan"></div>';
		$divuploadPlan_str .= '</div>';
		$divuploadPlan_str .= '</div>';	
		$divuploadPlan->setContent($divuploadPlan_str);	

    

		$reportInstructions = new Zend_Form_Element_Textarea('reportInstructions');
		$reportInstructions->setLabel('Test Results & Output')
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setOptions(array('cols' => '50'))
				->setDescription('How do you want the Mobsters to report the test results? Do you have any files or forms you want to upload to collect information?');
		$reportInstructions->helpText='How do you want the Mobsters to report the test results? Do you have any files or forms you want to upload to collect information?';
		

		$testOutput = new My_Form_Element_FileUpload('testOutput');
		$testOutput->setDecorators(array('MobForm'))
				->setLabel('Or include instruction document(s)');	
			
		
	    
		$divtestOutput=new My_Form_Element_myXhtml('divtestOutput');
		$divtestOutput_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divtestOutput_str .= '<div id="divtestOutput"></div>';
		$divtestOutput_str .= '</div>';
		$divtestOutput_str .= '</div>';	
		$divtestOutput->setContent($divtestOutput_str);	
		

		
		$genFiles = new My_Form_Element_FileUpload('genFiles');
		$genFiles->setDecorators(array('MobForm'))
				->setLabel('Generic App Files')
				->setDescription('Upload files that will be used by everyone\'s handset in the project.');
		$genFiles->helpText='Upload files that will be used by everyone\'s handset in the project.';
		
  
		$divgenFiles=new My_Form_Element_myXhtml('divgenFiles');
		$divgenFiles_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divgenFiles_str .= '<div id="divgenFiles"></div>';
		$divgenFiles_str .= '</div>';
		$divgenFiles_str .= '</div>';	
		$divgenFiles->setContent($divgenFiles_str);	
		
		
	
		// Get the table for supported devices
		$supporteddevices= new SupportedDevices();
		$specificuploads=array();
		if ($devices=$supporteddevices->getSupportedDevices($idproject))
		{
			foreach($devices as $device)
			{
					
				$elementname='dev'.$device['platform'].$device['vendor'].$device['tid'];
				$element=$specificuploads[][0]= new My_Form_Element_FileUpload($elementname);
				$element->setDecorators(array('MobForm'))
						->setLabel('Specific Files for '.substr($device->platform,2).'/'.(($device->vendor)? $device->vendor : '').'/'.(($device->model)? $device->model : 'Any'))
						->setDescription('These are files will only be available to testers with this handset.');
					
		
				$tablediv=$specificuploads[][1]=new My_Form_Element_myXhtml('div'.$elementname);
				$tablediv_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
				$tablediv_str .= '<div id="div'.$elementname.'"></div>';
				$tablediv_str .= '</div>';
				$tablediv_str .= '</div>';			
				$tablediv->setContent($tablediv_str);
				
					

			}
		}

		// More admin pieces, add/update/remove test reports. 
		
		
	


		
		$mobURL= new Zend_Form_Element_Text('mobURL');
		$mobURL->setLabel('Mobile / Website URL')
				->setDescription('Want to test a mobile website? Or, send the mobster to this URL for further install or download instructions?')
				->setDecorators(array('MobForm'));
		$mobURL->helpText='Want to test a mobile website? Or, send the mobster to this URL for further install or download instructions?';
		
		//nda
		$nda = new Zend_Form_Element_Radio('nda');
		$nda->addMultiOptions(array(0 => 'No',1 => 'Yes'))
			->setDecorators(array('MobForm'))
			->setLabel( 'Non-Disclosure Agreement?')
			->setDescription('Do you require proprietary or more stringent security for your test?');
		$nda->helpText='Do you require proprietary or more stringent security for your test?';
		$nda->setValue(0);	
		
		
		
		$ndaFile = new My_Form_Element_FileUpload('ndaFile');
		$ndaFile->setLabel('Custom NDA')
				->setDecorators(array('MobForm'))
				->setDescription('If you require a specific NDA file please upload it here.');
			
		$divndaFile=new My_Form_Element_myXhtml('divndaFile');
		$divndaFile_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divndaFile_str .= '<div id="divndaFile"></div>';
		$divndaFile_str .= '</div>';
		$divndaFile_str .= '</div>';
		$divndaFile->setContent($divndaFile_str);	
	
		
		$screenShots = new My_Form_Element_FileUpload('screenShots');
		$screenShots->setLabel('Screenshots etc.')
				->setDecorators(array('MobForm'))
				->setDescription('Upload screenshots, other images or graphics.');	
		$screenShots->helpText='Upload screenshots, other images or graphics.';	
			
		
		$divscreenShots=new My_Form_Element_myXhtml('divscreenShots');
		$divscreenShots_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divscreenShots_str .= '<div id="divscreenShots"></div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots->setContent($divscreenShots_str);
			
		$documentation = new My_Form_Element_FileUpload('documentation');
		$documentation->setLabel('Support Documentation')
				->setDecorators(array('MobForm'))
				->setDescription('Upload supporting documentation, user guides or help.');	
		$documentation->helpText='Upload supporting documentation, user guides or help.';		
			
	$divdocumentation=new My_Form_Element_myXhtml('divdocumentation');
		$divdocumentation_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divdocumentation_str .= '<div id="divdocumentation"></div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation->setContent($divdocumentation_str);
		

		$projImg = new My_Form_Element_FileUpload('projImg');
		$projImg->setLabel('Project Image.')
				->setDecorators(array('MobForm'))
				->setDescription('Image Associated With the Project.');	
	
			
		
		$divprojImg=new My_Form_Element_myXhtml('divprojImg');
		$divprojImg_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divprojImg_str .= '<div id="divprojImg"></div>';
		$divprojImg_str .= '</div>';
		$divprojImg_str .= '</div>';
		$divprojImg->setContent($divprojImg_str);

		$idproj = new Zend_Form_Element_Hidden('projectid');
		$idproj->setValue($this->idproject);
		
		$this->addElement($idproj);

		$this->addElement($status);				
		$this->addElement($type);	
		$this->addElement($testPkg);
		$this->addElement($lineBreak);
		$this->addElement($inviteType);
		$this->addElement($transType);
		$this->addElement($bidAmount);
		$this->addElement($amtMobsters);

		$lineBreak = new My_Form_Element_myXhtml('linebreak2'); // What a pain in the arse. Shouldn't we be able to add these tags as decorators to the fields where we want them?
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
		$this->addElement($projName);
	
		//$this->addElement($projImg);	
		
		
		$this->addElement($projDesc);
		$this->addElement($projDescFull);
		$this->addElement($tags);
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak3');
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
	
		$this->addElement($sDate);
		$this->addElement($eDate);
		$this->addElement($projDur);
		$this->addElement($testType);
		$this->addElement($testCategory);
		$this->addElement($testClass);
		$this->addElement($estTime);
		$this->addElement($estData);
	
		$lineBreak = new My_Form_Element_myXhtml('linebreak4');
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
		$this->addElement($mobRating);		
		$this->addElement($mobInvite);
		
				
		$this->addElement($heading1);
		$this->addElement($country);
		$this->addElement($carrier);
		$this->addElement($addcarrier);
		$this->addElement($networktable);
		$this->addElement($heading2);
		$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($addmodel);
		$this->addElement($handsettable);
		$this->addElement($region);
		
			 
		// Upload Test Plan
		$this->addElement($uploadTestPlan);
		$this->addElement($divuploadPlan);
		
		// Test Instructions
		$this->addElement($reportInstructions);
		
		$this->addElement($testOutput);	
		$this->addElement($divtestOutput);
		
		// Generic App Files
		$this->addElement($genFiles);
		$this->addElement($divgenFiles);
		
		
		//create dynamic text fields here based on results from getSupportedDevices(projectID);
		foreach($specificuploads as $upload)
		{
			foreach($upload as $element)
			{
				$this->addElement($element);
			}
		}
		 
		$this->addElement($mobURL);
		
		$this->addElement($nda);
		$this->addElement($ndaFile);
		$this->addElement($divndaFile);

		$this->addElement($screenShots);
		$this->addElement($divscreenShots);

		$this->addElement($projImg);
		$this->addElement($divprojImg);
		
		$this->addElement($documentation);
		$this->addElement($divdocumentation);
		
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');
		
		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')
			->setDecorators($this->submitDecorators);
			
		$this->addElement($start_row);
		$this->addElement($next);
		$this->addElement($end_row);
		
  
	}
}