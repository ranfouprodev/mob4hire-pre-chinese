<?php 
class Form_NewTestDefinition extends My_Form {
	public function init() {

		$translator = $this->getTranslator();

		$saveDraft= new Zend_Form_Element_Submit('saveDraft');
		$saveDraft->setAttrib('class', 'text_button');
		$saveDraft->setLabel($translator->translate('form_NewTestDefinition_saveDraftButton'));
		$this->addElement($saveDraft);
		
		$filterTrim = new Zend_Filter_StringTrim();
        
		/*
		*  Test Package
		*/
		$testPkg = new Zend_Form_Element_Radio('testPkg');
		$testPkg->addMultiOptions(array(1=> $translator->translate('form_NewTestDefinition_testPkgOpt1'), 2=> $translator->translate('form_NewTestDefinition_testPkgOpt2'), 3=> $translator->translate('form_NewTestDefinition_testPkgOpt3')))
				->setLabel('<span class="mobblack">' . $translator->translate('form_NewTestDefinition_testPkgLabel1') . '</span>' . $translator->translate('form_NewTestDefinition_testPkgLabel2') . '<span class="mobred"></span>' . $translator->translate('form_NewTestDefinition_testPkgLabel3'))
		  		->setDecorators(array('MobForm'))	
				->setDescription($translator->translate('form_NewTestDefinition_testPkgDesc'))
				->setValue(1);
		// $testPkg->helpText = '<img src="/images/banner-product-info-mobtest.gif"/>';
        
		/*
		 * Project Name
		 */
		$projName = new Zend_Form_Element_Text('projName');
        
		$projects = new Projects();
		if (isset( Zend_Registry::get('defSession')->project) && property_exists(Zend_Registry::get('defSession')->project, 'idproject'))
			$idproject= Zend_Registry::get('defSession')->project->idproject;
		else 
			$idproject=0;

	// Won't work if we use the page for updates - Actually it would have - that is why we passed idproject to the validator function
	//	$validatorUniqueProject = new My_Validate_ProjectUnique($projects, 'Name', $idproject);
	//		$validatorUniqueProject->setMessage(__('This project name has already been used, please choose another one.'));
        
		$projName->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setLabel($translator->translate('form_NewTestDefinition_projNameLabel'))
				 ->setAttrib('size', '50')
				 ->setRequired(true)
	//			 ->addValidator($validatorUniqueProject)
				 ->setDescription($translator->translate('form_NewTestDefinition_projNameDesc'));			 
 		$projName->setValue('Project Name');
		$projName->helpText = $translator->translate('form_NewTestDefinition_projNameHelp');
        
		/*
		 * Project Image Uploader
		 */
		$projImg = new My_Form_Element_ImageUpload('projImg');
		$projImg->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestDefinition_projImgLabel'))
				->setDescription($translator->translate('form_NewTestDefinition_projImgDesc'));
		$projImg->helpText=$translator->translate('form_NewTestDefinition_projImgHelp');
		
		
		/*
		 * Project Descriptions
		 */    
		$projDesc = new Zend_Form_Element_Text('projDesc');
		$projDesc->addFilter($filterTrim)
				 ->setDecorators(array('MobForm'))
				 ->setRequired(true)
				 ->setLabel($translator->translate('form_NewTestDefinition_projDescLabel'))
				 ->setAttrib('size', '50')
				 ->setAttrib('maxLength', 140)
				 ->setDescription($translator->translate('form_NewTestDefinition_projDescDesc'));
		$projDesc->helpText=$translator->translate('form_NewTestDefinition_projDescHelp');				 
				 
				 
		$projDescFull = new My_Form_Element_TinyMce('projDetail');
		$projDescFull->addFilter($filterTrim)
					//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setDecorators(array('MobForm'))
					->setRequired(true)
					->setLabel($translator->translate('form_NewTestDefinition_projDescFullLabel'))
					->setAttrib('rows', '10')
					->setAttrib('cols', '50')
					->setDescription($translator->translate('form_NewTestDefinition_projDescFullDesc'));
		$projDescFull->helpText=$translator->translate('form_NewTestDefinition_projDescFullHelp');
		
		
		/*
		* Invitation Type
		*/
		$inviteType = new Zend_Form_Element_Radio('inviteType');
		$inviteType->addMultiOptions(array(0=> $translator->translate('form_NewTestDefinition_inviteTypeOpt0'), 1=> $translator->translate('form_NewTestDefinition_inviteTypeOpt1')))
					->setDecorators(array('MobForm'))
					->setLabel($translator->translate('form_NewTestDefinition_inviteTypeLabel'))
					->setDescription($translator->translate('form_NewTestDefinition_inviteTypeDesc'));
		$inviteType->setValue(0);
		$inviteType->helpText=$translator->translate('form_NewTestDefinition_inviteTypeHelp');
		
		/*
		* Mobster Invitation Email 
		*/
		$mobInvite = new Zend_Form_Element_Textarea('mobInvite');
		$mobInvite->addFilter($filterTrim)
				->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestDefinition_mobInviteLabel'))
				->setOptions(array('cols'=>'50'))
				->setDescription($translator->translate('form_NewTestDefinition_mobInviteDesc'));
		$mobInvite->setValue($translator->translate('form_NewTestDefinition_mobInviteValue'));
		$mobInvite->helpText=$translator->translate('form_NewTestDefinition_mobInviteHelp');
		
		
		/*
		* Transaction Type and Bidding Amount
		*/
		$transType = new Zend_Form_Element_Radio('transType');
		$transType->addMultiOptions(array(0=> $translator->translate('form_NewTestDefinition_transTypeOpt0')
, 1=> $translator->translate('form_NewTestDefinition_transTypeOpt1')
))
					->setDecorators(array('MobForm'))
					->setLabel($translator->translate('form_NewTestDefinition_transTypeLabel'))
					->setDescription($translator->translate('form_NewTestDefinition_transTypeDesc'));
		$transType->setValue(0);
		$transType->helpText=$translator->translate('form_NewTestDefinition_transTypeHelp');
 
		$validatorMobsters=new Zend_Validate_LessThan('501');
		$validatorMobsters->setMessage($translator->translate('form_NewTestDefinition_amtMobstersMessage'));
		$amtMobsters = new Zend_Form_Element_Text('amtMobsters');
		$amtMobsters	->addFilter($filterTrim)
  						->setDecorators(array('MobForm'))
						->setLabel($translator->translate('form_NewTestDefinition_amtMobstersLabel'))
						->setAttrib('size','10')
						->setValue("10")
						->addValidator(new Zend_Validate_Int())
						->addValidator($validatorMobsters)
						->setDescription($translator->translate('form_NewTestDefinition_amtMobstersDesc'));
		$amtMobsters->helpText = $translator->translate('form_NewTestDefinition_amtMobstersHelp');
		$amtMobsters->setValue('10');
		
		
		$bidAmount = new Zend_Form_Element_Text('bidAmount');
		$bidAmount->addFilter($filterTrim)
				->setDecorators(array('MobForm')
				, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> '', 'id' => 'bidamount')))
				->addValidator(new Zend_Validate_Int())
//				->addValidator(new Zend_Validate_GreaterThan('0'))
				->setLabel($translator->translate('form_NewTestDefinition_bidAmountLabel'))
				->setDescription($translator->translate('form_NewTestDefinition_bidAmountDesc'));
        
		$bidAmount->addFilter($filterTrim);
		$bidAmount->helpText=$translator->translate('form_NewTestDefinition_bidAmountHelp');
		
				
		/*
		* Dates 
		*/
		$validatorDate = new Zend_Validate_Date();
		
		$validatorDate->setMessages(array(Zend_Validate_Date::INVALID=>__('%value%' . $translator->translate('form_NewTestDefinition_validatorDate')), Zend_Validate_Date::FALSEFORMAT=>__('%value%' . $translator->translate('form_NewTestDefinition_validatorFormat'))));

		$dateFuture=new My_Validate_CompareDate(); // if no argument passed then CompareDate compares with today's date
		$dateFuture->setMessage(__($translator->translate('form_NewTestDefinition_dateFuture')));
		
		$sDate = new ZendX_JQuery_Form_Element_DatePicker('sDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		
			// ->setDecorators($this->jQueryDecorators)
		$sDate->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix'))
							, 'Errors')
						)
			  ->setRequired(true)
			  ->setLabel($translator->translate('form_NewTestDefinition_sDateLabel'))
			  ->setAttrib("readonly","readonly")
			  ->setDescription($translator->translate('form_NewTestDefinition_sDateDesc'))
			  ->addValidator($validatorDate)
			  ->addValidator($dateFuture);
 
		$sDate->setValue(date("Y-m-d"));	
		$sDate->helpText=$translator->translate('form_NewTestDefinition_sDateHelp');  
			  
 		$dateCmp = new My_Validate_CompareDate('sDate');
		$dateCmp->setMessage(__($translator->translate('form_NewTestDefinition_dateCmp')));       

		$eDate = new ZendX_JQuery_Form_Element_DatePicker('eDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$eDate->addValidator($validatorDate)
			->addValidator($dateCmp)
			//->setDecorators($this->jQueryDecorators)
			->setDecorators(array('MobLabel'
									, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
									, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix'))
									,'Errors')
								)
			
			->setRequired(true)
			->setLabel($translator->translate('form_NewTestDefinition_eDateLabel'))
			->setAttrib("readonly","readonly")
			->setDescription($translator->translate('form_NewTestDefinition_eDateDesc'));
		$eDate->setValue(date("Y-m-d", strtotime("+14 days")));	
		$eDate->helpText=$translator->translate('form_NewTestDefinition_eDateHelp');
		
		
		/*
		* Project Duration
		*/
		$projDur = new Zend_Form_Element_Text('projDur');
		$projDur->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setRequired(true)
				->setLabel($translator->translate('form_NewTestDefinition_projDurLabel'))
				->addValidator(new Zend_Validate_Int())
				->addValidator(new Zend_Validate_GreaterThan('0'))
				->setDescription($translator->translate('form_NewTestDefinition_projDurDesc'));
		$projDur->helpText=$translator->translate('form_NewTestDefinition_projDurHelp');

        
		/*
		* Test Type
		*/
		$testType = new Zend_Form_Element_MultiCheckbox('testType');
		$testType->addMultiOptions(array('Functional'=>'Functional', 'Usability'=>'Usability'))
				->setDecorators(array('MobForm'))
				->setRequired(true)
				->setLabel('Type Of Test')
				->setDescription('Select one or both');
		$testType->helpText ='With functional testing, you get answers to questions like “Does it crash?” and “Does it take advantage of the device special features like trackballs, touchscreens and high-resolution cameras?” In usability testing, you get answers to questions like “Does the menu structure make sense?” or “Is it fun to play?” and “Would you give it a good review on an app store?”  Many projects require both types of feedback, but many also do not!';
		
		/*
		* Test Category
		*/
		$testCategory = new Zend_Form_Element_MultiCheckbox('testCategory');
		$testCategory->addMultiOptions(array('News'=>'News', 'Medical'=>'Medical', 'Reference'=>'Reference', 'Productivity'=>'Productivity',
			 'Navigation'=>'Navigation', 'Health and Fitness'=>'Health and Fitness', 'Education'=>'Education',
			  'Weather'=>'Weather', 'Education'=>'Education', 'Business'=>'Business', 'Music'=>'Music', 'Finance'=>'Finance'
			  , 'Sports'=>'Sports', 'Travel'=>'Travel', 'Utilities'=>'Utilities', 'Games'=>'Games', 'Social Networking'=>'Social Networking'
			  , 'Books'=>'Books', 'Entertainment'=>'Entertainment', 'Lifestyle'=>'Lifestyle'))
				->setDecorators(array('MobForm'))
				->setRequired(true)
				->setLabel('Test Category')
				->setDescription('Check all that apply. Try to select just the major functions of your program.');
		$testCategory->helpText ='These common categories help Mobsters understand what your project is about, and helps them filter projects when looking for specific types.';
		


		/*
		* Test Class 
		*/
		$testClass = new Zend_Form_Element_Select('testClass');
		$testClass->addMultiOptions(array('Easy'=> $translator->translate('form_NewTestDefinition_testClassOpt1'), 'Moderate'=> $translator->translate('form_NewTestDefinition_testClassOpt2'), 'Average'=> $translator->translate('form_NewTestDefinition_testClassOpt3'), 'Advanced'=> $translator->translate('form_NewTestDefinition_testClassOpt4'), 'Difficult'=> $translator->translate('form_NewTestDefinition_testClassOpt5')))
				->setDecorators(array('MobForm'))
				->setRequired(true)
				->setLabel($translator->translate('form_NewTestDefinition_testClassLabel'))
				->setDescription($translator->translate('form_NewTestDefinition_testClassDesc'));
		$testClass->helpText=$translator->translate('form_NewTestDefinition_testClassHelp');

        
		/*
		* MobStar Rating
		*/
		$mobRating = new Zend_Form_Element_Radio('mobRating');
		$mobRating->addMultiOptions(array(1=>'Yes', 2=>'No'))
				->setDecorators(array('MobForm'))
				->setLabel('<span class="mobblack">MOB</span><span class="mobred">STAR</span> Rating?')
				->setDescription('The Mobsters are happy to provide this service free of charge.');
		$mobRating->helpText='A MOBSTAR rating will give you a preview of how the Mobster would rate your application on an appstore if they were to download it. This can be very helpful in choosing the features to launch with';
		$mobRating->setValue(1);
        
		/*
		* Time Estimate
		*/
		$estTime = new Zend_Form_Element_Text('estTime');
		$estTime->addFilter($filterTrim)
				->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestDefinition_estTimeLabel'))
				->setDescription($translator->translate('form_NewTestDefinition_estTimeDesc'));
		$estTime->helpText=$translator->translate('form_NewTestDefinition_estTimeHelp');
			
			

		/*
		* Estimated Data Usage
		*/
		$estData = new Zend_Form_Element_Text('estData');
		$estData->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setLabel($translator->translate('form_NewTestDefinition_estDataLabel'))
			->setDescription($translator->translate('form_NewTestDefinition_estDataDesc'));
		$estData->helpText=$translator->translate('form_NewTestDefinition_estDataHelp');
			
		
		/*
		* Tags
		*/
		$tags = new Zend_Form_Element_Text('tags');
		$tags->addFilter($filterTrim)
			->setDecorators(array('MobForm'))
			->setRequired(false)
			->setLabel($translator->translate('form_NewTestDefinition_tagsLabel'))
			->setAttrib('size', '50')
			->setDescription($translator->translate('form_NewTestDefinition_tagsDesc'));
		$tags->helpText=$translator->translate('form_NewTestDefinition_tagsHelp');
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak->setContent('<hr />');
		
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');
        
		$mobtest_logo = new My_Form_Element_myXhtml('mobtest_logo');
		$mobtest_logo->setContent('<div id="mobtest-logo"><span>MobTest Real world mobile testing.</span></div>');
        
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')->setDecorators($this->submitDecorators);
        
		$previous = new Zend_Form_Element_Submit('startover');
		$previous->setLabel('')
				->setDecorators($this->submitDecorators);
		
		$this->addElement($testPkg);


		$this->addElement($lineBreak);
		$this->addElement($inviteType);
		$this->addElement($transType);
		$this->addElement($bidAmount);
		$this->addElement($amtMobsters);

		
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak2'); // What a pain in the arse. Shouldn't we be able to add these tags as decorators to the fields where we want them?
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
		$this->addElement($projName);
		
		//$this->addElement($heading1);
		$this->addElement($projImg);	
		//$this->addElement($add);
		//$this->addElement($uploads);
		//$this->addElement($closediv);
		
		
		$this->addElement($projDesc);
		$this->addElement($projDescFull);
		$this->addElement($tags);
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak3');
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
	
		$this->addElement($sDate);
		$this->addElement($eDate);
		$this->addElement($projDur);
		//$this->addElement($testType);
		//$this->addElement($testCategory);
		$this->addElement($testClass);
		$this->addElement($estTime);
		$this->addElement($estData);
	
		$lineBreak = new My_Form_Element_myXhtml('linebreak4');
		$lineBreak->setContent('<hr />');
		$this->addElement($lineBreak);
		
		//$this->addElement($mobRating);
		
		//$this->addElement($otherReq); // moved to next page
				
		$this->addElement($mobInvite);
        
		$this->addElement($start_row);
		$this->addElement($mobtest_logo);
		$this->addElement($next);
		//$this->addElement($previous);  // We will need a start over step
		$this->addElement($end_row);
	}
}