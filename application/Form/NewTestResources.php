<?php
class Form_NewTestresources extends My_Form
{
	public function init()
	{
		$translator = $this->getTranslator();

		$this->addPrefixPath('My_Decorator',
                      'My/Decorator/',
                      'decorator'); // todo: Add this is My_Form instead of each form which extends it

		$saveDraft= new Zend_Form_Element_Submit('saveDraft');
		$saveDraft->setAttrib('class', 'save-draft')->setLabel('');
		$this->addElement($saveDraft);

		$filterTrim = new Zend_Filter_StringTrim();
	    
		
		$uploadTestPlan = new My_Form_Element_FileUpload('uploadPlan');
		$uploadTestPlan->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestResources_uploadTestPlan_Label'))
				->setDescription($translator->translate('form_NewTestResources_uploadTestPlan_Desc'));
		$uploadTestPlan->helpText=$translator->translate('form_NewTestResources_uploadTestPlan_Help');
				
		//$divuploadPlan = new My_Form_Element_myXhtml('divuploadPlan');
		//$divuploadPlan->setContent('<div id="divuploadPlan"></div>');	
		
		$divuploadPlan=new My_Form_Element_myXhtml('divuploadPlan');
		$divuploadPlan_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divuploadPlan_str .= '<div id="divuploadPlan"></div>';
		$divuploadPlan_str .= '</div>';
		$divuploadPlan_str .= '</div>';	
		$divuploadPlan->setContent($divuploadPlan_str);	


/*       Removed. Replace with static links. 

		$testPlans = new Zend_Form_Element_Select('testPlans');
		$testPlans->addMultiOptions(array(1 => 'SMS/MMS',2 => 'GAME',3 => 'VOIP',4 => 'GPS/LBS',5 => 'Voice Quality',6 => 'eCommerce'));
		$testPlans->setDecorators(array('ViewHelper', 'MobLabel'))
				->setLabel('Select Test Plan Template')
				->setDescription('Start off your test plan on the right foot with our pre-defined sample templates. After downloading, copy from acrobat file into your own Test Plan document for edits, then use "Upload Test Plan" above.');
		$testPlans->helpText = 'Start off your test plan on the right foot with our pre-defined sample templates. After downloading, copy from acrobat file into your own Test Plan document for edits, then use "Upload Test Plan" above.';
	    
		$getPDF = new Zend_Form_Element_Button('getPDF');
		$getPDF->setLabel('GET DOC')
				->setDecorators($this->buttonDecorators);
*/	    

		$reportInstructions = new Zend_Form_Element_Textarea('reportInstructions');
		$reportInstructions->setLabel($translator->translate('form_NewTestResources_reportInstructions_Label'))
				->setDecorators(array('MobForm'))
				//->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
				->setOptions(array('cols' => '50'))
				->setDescription($translator->translate('form_NewTestResources_reportInstructions_Desc'));
		$reportInstructions->helpText=$translator->translate('form_NewTestResources_reportInstructions_Help');
		
		
/*		$testOutput = new Zend_Form_Element_File('testOutput');
		$testOutput->setDecorators(array('MobLabel'
									, array('File', array('tag' => 'div', 'class' => 'form_input'))
									, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element')))	
									)
				->addFilter($filterTrim)
				->setLabel('&nbsp;')
				->setDescription('&nbsp;');
		
		$btntestOutput = new Zend_Form_Element_Button('btntestOutput'); // The name of the button should point to the file element it is affecting
	        $btntestOutput->setLabel('      Upload      ')
			->setDecorators($this->buttonDecorators)
			->setOptions(array('onclick'=>'buttonClick(this);'));
*/			

		$testOutput = new My_Form_Element_FileUpload('testOutput');
		$testOutput->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestResources_testOutput_Label'))
				->helpText=$translator->translate('form_NewTestResources_testOutput_Help');
			
		
	    
		$divtestOutput=new My_Form_Element_myXhtml('divtestOutput');
		$divtestOutput_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divtestOutput_str .= '<div id="divtestOutput"></div>';
		$divtestOutput_str .= '</div>';
		$divtestOutput_str .= '</div>';	
		$divtestOutput->setContent($divtestOutput_str);	
		
		
		/*
		$genFiles = new Zend_Form_Element_File('genFiles');
		$genFiles->setDecorators(array('MobLabel'
									, array('File', array('tag' => 'div', 'class' => 'form_input'))
									, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element')))	
									)
				->setLabel('Generic App Files')
				->setDescription('Upload files that will be used by everyone\'s handset in the project.');
		$genFiles->helpText='Upload files that will be used by everyone\'s handset in the project.';
		$btngenFiles = new Zend_Form_Element_Button('btngenFiles'); // The name of the button should point to the file element it is affecting
	        $btngenFiles->setLabel('      Upload      ')
			->setDecorators($this->buttonDecorators)
			->setOptions(array('onclick'=>'buttonClick(this);'));
		
		*/
		
		$genFiles = new My_Form_Element_FileUpload('genFiles');
		$genFiles->setDecorators(array('MobForm'))
				->setLabel($translator->translate('form_NewTestResources_genFiles_Label'))
				->setDescription($translator->translate('form_NewTestResources_genFiles_Desc'));
		$genFiles->helpText=$translator->translate('form_NewTestResources_genFiles_Help');
		
  
		$divgenFiles=new My_Form_Element_myXhtml('divgenFiles');
		$divgenFiles_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divgenFiles_str .= '<div id="divgenFiles"></div>';
		$divgenFiles_str .= '</div>';
		$divgenFiles_str .= '</div>';	
		$divgenFiles->setContent($divgenFiles_str);	
		
		
		// This is a real ball-ache just to get stuff to show in the right column only. There has to be a better way
		/*$heading1 = new My_Form_Element_myXhtml('heading1');
		$heading1_str = '<div class="form_element">';
		$heading1_str .= '<div class="form_label"><label>Specific Handset Files</label>';
		$heading1_str .=' <div class="form_description">';
		$heading1_str .= '<p>Upload files that can be used specifically with these handsets</p>';
		$heading1_str .= '</div></div>';
		$heading1_str .= '<div class="form_helptip">';
		$heading1_str .= '<span class="whats-this"><a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
		$heading1_str .= '<p>Upload files that can be used specifically with these handsets.</p>';
		$heading1_str .= '</span>';
		$heading1_str .= '</div>';
		$heading1_str .= '<div class="form_input">';
		$heading1->setContent($heading1_str);*/
		
		
			
		
		
		// Get the table for supported devices
		$supporteddevices= new SupportedDevices();
		$specificuploads=array();
		if ($devices=$supporteddevices->getSupportedDevices(Zend_Registry::get('defSession')->project->idproject))
		{
			foreach($devices as $device)
			{
				/*$elementname='dev'.$device['platform'].$device['vendor'].$device['tid'];
				$element=$specificuploads[][0]=new Zend_Form_Element_File($elementname);
				$element->SetLabel(substr($device->platform,2).' '.(($device->vendor)? $device->vendor : '').' '.(($device->model)? $device->model : 'Any'));
				$button=$specificuploads[][1]=new Zend_Form_Element_Button('btn'.$elementname);
				$button->setLabel('      Upload      ')
					->setDecorators($this->buttonDecorators)
					->setOptions(array('onclick'=>'buttonClick(this);'));*/
					
				$elementname='dev'.$device['platform'].$device['vendor'].$device['tid'];
				$element=$specificuploads[][0]= new My_Form_Element_FileUpload($elementname);
				$element->setDecorators(array('MobForm'))
						->setLabel('Specific Files for '.substr($device->platform,2).'/'.(($device->vendor)? $device->vendor : '').'/'.(($device->model)? $device->model : 'Any'))
						->setDescription('These are files will only be available to testers with this handset.')
						->helpText='These are files will only be available to testers with this handset.';
					
		
				$tablediv=$specificuploads[][1]=new My_Form_Element_myXhtml('div'.$elementname);
				$tablediv_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
				$tablediv_str .= '<div id="div'.$elementname.'"></div>';
				$tablediv_str .= '</div>';
				$tablediv_str .= '</div>';			
				$tablediv->setContent($tablediv_str);
				
					

			}
		}
	/*	$closeDivs=new My_Form_Element_myXhtml('closedivs');
		$closeDivs->setContent('</div></div>');*/
		
		$mobURL= new Zend_Form_Element_Text('mobURL');
		$mobURL->setLabel($translator->translate('form_NewTestResources_mobURL_Label'))
				->setDescription($translator->translate('form_NewTestResources_mobURL_Desc'))
				->setDecorators(array('MobForm'));
		$mobURL->helpText=$translator->translate('form_NewTestResources_mobURL_Help');
		
		//nda
		$nda = new Zend_Form_Element_Radio('nda');
		$nda->addMultiOptions(array(0 => $translator->translate('form_NewTestResources_nda_Opt0'),1 => $translator->translate('form_NewTestResources_nda_Opt1')))
			->setDecorators(array('MobForm'))
			->setLabel($translator->translate('form_NewTestResources_nda_Label') )
			->setDescription($translator->translate('form_NewTestResources_nda_Desc'));
		$nda->helpText=$translator->translate('form_NewTestResources_nda_Help');
		$nda->setValue(0);	
		
		/*$ndaFile = new Zend_Form_Element_File('ndaFile');
		$ndaFile->setDecorators(array('MobLabel'
							, array('File', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element')))	
									)
				->setLabel('&nbsp;')
				->setDescription('&nbsp;');
		$btnndaFile = new Zend_Form_Element_Button('btnndaFile'); // The name of the button should point to the file element it is affecting
	        $btnndaFile->setLabel('      Upload      ')
			->setDecorators($this->buttonDecorators)
			->setOptions(array('onclick'=>'buttonClick(this);'));*/
			
		
		$ndaFile = new My_Form_Element_FileUpload('ndaFile');
		$ndaFile->setLabel($translator->translate('form_NewTestResources_ndaFile_Label'))
				->setDecorators(array('MobForm'))
				->setDescription($translator->translate('form_NewTestResources_ndaFile_Desc'))
				->helpText=$translator->translate('form_NewTestResources_ndaFile_Help');
		$divndaFile=new My_Form_Element_myXhtml('divndaFile');
		$divndaFile_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divndaFile_str .= '<div id="divndaFile"></div>';
		$divndaFile_str .= '</div>';
		$divndaFile_str .= '</div>';
		$divndaFile->setContent($divndaFile_str);	
	
	    
/*		$screenShots = new Zend_Form_Element_File('screenShots');
		$screenShots->setDecorators(array('MobLabel'
								, array('File', array('tag' => 'div', 'class' => 'form_input'))
								, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element')))	
								)
					->setLabel('Screenshots etc.')
					->setDescription('Upload screenshots, other images or graphics.');
		$screenShots->helpText='Upload screenshots, other images or graphics.';
		$btnscreenShots = new Zend_Form_Element_Button('btnscreenShots'); // The name of the button should point to the file element it is affecting
	        $btnscreenShots->setLabel('      Upload      ')
			->setDecorators($this->buttonDecorators)
			->setOptions(array('onclick'=>'buttonClick(this);'));*/
		
			
		$screenShots = new My_Form_Element_FileUpload('screenShots');
		$screenShots->setLabel($translator->translate('form_NewTestResources_screenShots_Label'))
				->setDecorators(array('MobForm'))
				->setDescription($translator->translate('form_NewTestResources_screenShots_Desc'));	
		$screenShots->helpText=$translator->translate('form_NewTestResources_screenShots_Help');	
			
		
		$divscreenShots=new My_Form_Element_myXhtml('divscreenShots');
		$divscreenShots_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divscreenShots_str .= '<div id="divscreenShots"></div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots_str .= '</div>';
		$divscreenShots->setContent($divscreenShots_str);
		
		/*$documentation = new Zend_Form_Element_File('documentation');
		$documentation->setDecorators(array('MobLabel'
									, array('File', array('tag' => 'div', 'class' => 'form_input'))
									, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element')))	
										)
					->setLabel('Documentation')
					->setDescription( 'Upload supporting documentation, user guides or help.');

		$documentation->helpText = 'Upload supporting documentation, user guides or help.';
		$btndocumentation = new Zend_Form_Element_Button('btndocumentation'); // The name of the button should point to the file element it is affecting
	        $btndocumentation->setLabel('      Upload      ')
			->setDecorators($this->buttonDecorators)
			->setOptions(array('onclick'=>'buttonClick(this);'));*/

			
		$documentation = new My_Form_Element_FileUpload('documentation');
		$documentation->setLabel($translator->translate('form_NewTestResources_documentation_Label'))
				->setDecorators(array('MobForm'))
				->setDescription($translator->translate('form_NewTestResources_documentation_Desc'));	
		$documentation->helpText=$translator->translate('form_NewTestResources_documentation_Help');		
			
	$divdocumentation=new My_Form_Element_myXhtml('divdocumentation');
		$divdocumentation_str = '<div class="form_element clearfix"><div class="form_label">&nbsp;</div><div class="form_helptip">&nbsp;</div><div class="form_input">';
		$divdocumentation_str .= '<div id="divdocumentation"></div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation_str .= '</div>';
		$divdocumentation->setContent($divdocumentation_str);
		
		
		$lineBreak = new My_Form_Element_myXhtml('linebreak');
		$lineBreak->setContent('<hr />');
		
		$start_fieldset = new My_Form_Element_myXhtml('start_fieldset');
		$start_fieldset->setContent('<fieldset>');
		
		$end_fieldset = new My_Form_Element_myXhtml('end_fieldset');
		$end_fieldset->setContent('</fieldset>');
	  
		 
		// Upload Test Plan
		$this->addElement($uploadTestPlan);
		$this->addElement($divuploadPlan);
		
		// Test Instructions
		$this->addElement($reportInstructions);
		$this->addElement($testOutput);	
		$this->addElement($divtestOutput);
		
		// Generic App Files
		$this->addElement($genFiles);
		$this->addElement($divgenFiles);
		
		
		//create dynamic text fields here based on results from getSupportedDevices(projectID);
		foreach($specificuploads as $upload)
		{
			foreach($upload as $element)
			{
				$this->addElement($element);
			}
		}
		
		$this->addElement($mobURL);
		
		$this->addElement($nda);
		$this->addElement($ndaFile);
		$this->addElement($divndaFile);

		$this->addElement($screenShots);
		$this->addElement($divscreenShots);
				
		$this->addElement($documentation);
		$this->addElement($divdocumentation);
	    
		  
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');

		$mobtest_logo = new My_Form_Element_myXhtml('mobtest_logo');
		$mobtest_logo->setContent('<div id="mobtest-logo"><span>MobTest Real world mobile testing.</span></div>');
	
		$previous = new Zend_Form_Element_Submit('previous');
		$previous->setLabel('')
				->setDecorators($this->submitDecorators);
		
		$next = new Zend_Form_Element_Submit('next');
		$next->setLabel('')
				->setDecorators($this->submitDecorators);
	
		$this->addElement($start_row);
		$this->addElement($mobtest_logo);
		$this->addElement($next);
		$this->addElement($previous);
		$this->addElement($end_row);

	    $this->addElement('hidden', 'return', array(
        'value' => Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),                         
                ));

		
		
        }
}
