<?php
class Form_RegisteredManagedServices extends My_Form
{
	public function init()
	{
		$filterTrim = new Zend_Filter_StringTrim(); 
		
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));
		
		$this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$this->setAction('/app/project/newproject')->setMethod('post');
					
		$description = new Zend_Form_Element_Textarea('description');
		$description->addFilter($filterTrim)
					->addFilter(new Zend_Filter_StripTags('b','i','u','p'))
					->setDecorators(array('MobForm'))
					->setRequired(true)
					->setLabel('Description')
					->setAttrib('rows', '10')
					->setAttrib('cols', '50');
		
		$product = new Zend_Form_Element_Hidden('product');
		$product->setDecorators(array('MobForm'))
				->setValue('Experience');

				$validatorDate = new Zend_Validate_Date();
		
		$validatorDate->setMessages(array(Zend_Validate_Date::INVALID=>__("'%value%' does not appear to be a valid date"), Zend_Validate_Date::FALSEFORMAT=>__("'%value%' does not fit given date format")));

		$dateFuture=new My_Validate_CompareDate(); // if no argument passed then CompareDate compares with today's date
		$dateFuture->setMessage(__('While our Mobsters are incredible, time travel is quite out of the question. Can you choose a start date sometime in the linear future?'));
		
		$sDate = new ZendX_JQuery_Form_Element_DatePicker('sDate', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		
			// ->setDecorators($this->jQueryDecorators)
		$sDate->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix'))
							, 'Errors')
						)
			  ->setRequired(true)
			  ->setLabel('When do you need the project started?')
			  ->setAttrib("readonly","readonly")
			  ->setDescription("When do you want the project to start?")
			  ->addValidator($validatorDate)
			  ->addValidator($dateFuture);
 
		$sDate->setValue(date("Y-m-d"));	
	
					
		
		$send = new Zend_Form_Element_Submit('submit');
		$send->setLabel('Submit')
			 ->setDecorators($this->buttonDecorators);
			 
		$start_row = new My_Form_Element_myXhtml('start_single_row');
		$start_row->setContent('<div class="submit_buttons clearfix">');

		$end_row = new My_Form_Element_myXhtml('end_single_row');
		$end_row->setContent('<div class="clear">&nbsp;</div></div>');


		$this->addElement($description);
		$this->addElement($product);
		$this->addElement($sDate);
		
		
		$this->addElement($start_row);
		$this->addElement($send);
		$this->addElement($end_row);
		
	}
}
