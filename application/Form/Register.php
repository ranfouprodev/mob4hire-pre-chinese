<?php
class Form_Register extends My_Form
{
	

	public function init()
	{
		$translator = $this->getTranslator();
		$this->addPrefixPath('My_Decorator',
                        'My/Decorator/',
                        'decorator');
		$this->setAction('/index/register')->setMethod('post')->setAttrib('id', 'register');
 
		$filterTrim = new Zend_Filter_StringTrim();
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__($translator->translate('default_index_register_notEmpty')));
 
		$firstName = new Zend_Form_Element_Text('firstname');        
		$firstName->setLabel($translator->translate('default_index_register_firstName'))
							->addFilter($filterTrim)
							->setDecorators(array('MobForm'));
 
 		$lastName = new Zend_Form_Element_Text('lastname');        
		$lastName	->setLabel($translator->translate('default_index_register_lastName'))
							->addFilter($filterTrim)
							->setDecorators(array('MobForm'));

		$username = new Zend_Form_Element_Text('username');
		$validatorAlnum = new Zend_Validate_Alnum();
		$validatorAlnum->setMessage(__($translator->translate('default_index_register_useLatin')));
		$validatorStringLength = new Zend_Validate_StringLength(3, 32);
		$validatorStringLength->setMessages(array(
			Zend_Validate_StringLength::TOO_SHORT => __($translator->translate('default_index_register_stringLengthShort')),
			Zend_Validate_StringLength::TOO_LONG => __($translator->translate('default_index_register_stringLengthLong')),
			)
		);
		$users = new Users();
		$validatorUniqueUsername = new My_Validate_DbUnique($users, 'username');
		$validatorUniqueUsername->setMessage(__($translator->translate('default_index_register_alreadyRegistered')));

		$username	->addValidator($validatorNotEmpty, true)
							->setRequired(true)
							->setLabel($translator->translate('default_index_register_username'))
							->setDecorators(array('MobForm'))
							->addFilter($filterTrim)
							->addValidator($validatorAlnum)
							->addValidator($validatorStringLength)
							->addValidator($validatorUniqueUsername)
							->addDecorator(array('ajaxDiv' => 'HtmlTag'), array('tag'=>'div', 'placement'=>'append', 'id'=>'username_help', 'class'=>'errors'));

	
		$password = new Zend_Form_Element_Password('password');
		$password	->addValidator($validatorNotEmpty, true)
							->setRequired(true)
							->setLabel($translator->translate('default_index_register_password'))
							->setDecorators(array('MobForm'))
							->addValidator(new Zend_Validate_StringLength(3));
 
		$password2 = new Zend_Form_Element_Password('password2');
		$validatorPassword = new My_Validate_PasswordConfirmation('password');
		$validatorPassword->setMessage(__($translator->translate('default_index_register_passwordMatch')));
 		$password2->setLabel($translator->translate('default_index_register_confirmPassword'))
							->setDecorators(array('MobForm'))
							->addValidator($validatorNotEmpty, true)
							->setRequired(true)
							->addValidator($validatorPassword);

		/**
		* @todo Change this wired error messages to something more user friendly, or even use simple email regex matching validator
		*/
		$email = new Zend_Form_Element_Text('email');
		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' " . $translator->translate('default_index_register_IP_ADDRESS_NOT_ALLOWED')),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' " . $translator->translate('default_index_register_UNKNOWN_TLD')),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' " . $translator->translate('default_index_register_INVALID_DASH')),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' " . $translator->translate('default_index_register_INVALID_HOSTNAME_SCHEMA') . "'%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' " . $translator->translate('default_index_register_UNDECIPHERABLE_TLD')),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' " . $translator->translate('default_index_register_INVALID_DNS_HOSTNAME')),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' " . $translator->translate('default_index_register_INVALID_LOCAL_NETWORK_NAME')),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' " . $translator->translate('default_index_register_LOCAL_NETWORK_NAME_NOT_ALLOWED'))
			)
		);
 
		$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' " . $translator->translate('default_index_register_INVALID_EMAIL_ADDRESS')),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' " . $translator->translate('default_index_register_INVALID_EMAIL_HOSTNAME') . "'%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' " . $translator->translate('default_index_register_INVALID_MX_RECORD') . "'%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' " . $translator->translate('default_index_register_DOT_ATOM')),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' " . $translator->translate('default_index_register_QUOTED_STRING')),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' " . $translator->translate('default_index_register_INVALID_LOCAL_PART') . "'%value%'")
			)
		);
		$validatorUniqueEmail = new My_Validate_DbUnique($users, 'email');
		$validatorUniqueEmail->setMessage(__($translator->translate('default_index_register_emailAlreadyRegistered')));
		$email->addValidator($validatorNotEmpty, true)
			->setRequired(true)
			->setLabel($translator->translate('default_index_register_emailAddress'))
			->setDecorators(array('MobForm'))
			->addFilter($filterTrim)
			->addValidator($validatorEmail)
			->addValidator($validatorUniqueEmail)
			->addDecorator(array('ajaxDiv' => 'HtmlTag'), array('tag'=>'div', 'placement'=>'append', 'id'=>'email_help', 'class'=>'errors'));

 
		$email2 = new Zend_Form_Element_Text('email2');
		$validatorEmail = new My_Validate_PasswordConfirmation('email');
		$validatorEmail->setMessage(__($translator->translate('default_index_register_emailNotMatch')));
		$email2->setLabel($translator->translate('default_index_register_confirmEmail'))
				->setDecorators(array('MobForm'))
				->addValidator($validatorNotEmpty, true)
				->setRequired(true)
				->addValidator($validatorEmail);

		$validatorNotEmptyAgreement = new Zend_Validate_NotEmpty();
		$validatorNotEmptyAgreement->setMessage(__($translator->translate('default_index_register_acceptTerms')));        

		$agreement = new Zend_Form_Element_Checkbox('agreement');
		$agreement->setLabel($translator->translate('default_index_register_agreeTo') . " <a href= '/index/terms' target='_new'>" . $translator->translate('default_index_register_termsConditions') . "</a>")
			->setUncheckedValue('')
			->addValidator($validatorNotEmptyAgreement, true)->setRequired(true)
			->setDecorators(array('MobForm'));
		
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('class', 'text_button_red');
        $submit->setLabel($translator->translate('form_register_submit'));

		$linebreak = new My_Form_Element_myXhtml('linebreak');
		$linebreak->setContent('<hr />');
		
		$linebreak2 = new My_Form_Element_myXhtml('linebreak2');
		$linebreak2->setContent('<hr />');
		
		$linebreak3 = new My_Form_Element_myXhtml('linebreak3');
		$linebreak3->setContent('<hr />');
		
		$this->addElement($username);
		$this->addElement($password);
		$this->addElement($password2);
		$this->addElement($linebreak);
		$this->addElement($firstName);
		$this->addElement($lastName);
		$this->addElement($linebreak2);
		$this->addElement($email);
		$this->addElement($email2);
		$this->addElement($linebreak3);
		$this->addElement($agreement);
		$this->addElement($submit);
	}
}
