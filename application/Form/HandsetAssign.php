<?php
class Form_HandsetAssign extends My_Form
{
	public function init()
	{	
		$this->setAction('/admin/community/assigndevice')->setMethod('POST')->setAttrib('id','frmHandsetAssign');
		$platform= new Zend_Form_Element_Select('platform');
		$platform->setLabel('Platform')
				->addMultiOptions(array(0=>'Any', 'osSymbian'=>'Symbian', 'osLinux'=>'Linux', 'osAndroid'=>'Android', 'osWindows'=>'Windows','osRim'=>'RIM', 'osOsx'=>'Osx'));

		$devices=new Devices();	
		$vendors=array_merge(array(0=>'Any'), $devices->listVendors());
		
		$manufacturer = new Zend_Form_Element_Select('vendor');
		
		$manufacturer->setLabel('Handset')
				->addMultiOptions($vendors)
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$model = new Zend_Form_Element_Select('model');
		$model->setLabel('Model')
				->addMultiOptions(array(0=>'Any'))
				->setRegisterInArrayValidator(false); // This is required because the selector is populated dynamically

		$submit= new Zend_Form_Element_Button('filter');
		$submit->setLabel('Filter');
		
		$this->addElement($platform);
		$this->addElement($manufacturer);
		$this->addElement($model);
		$this->addElement($submit);
        }
}
?>