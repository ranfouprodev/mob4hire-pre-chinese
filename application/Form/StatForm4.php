<?php
class Form_StatForm4 extends My_Form
{
	public function init()
	{
		$this->setAction('/admin/stats/testers')->setMethod('POST')->setAttrib('id','frmStatForm2');
        $validatorDate2 = new Zend_Validate_Date();
		$validatorDate2->setMessages(array(Zend_Validate_Date::INVALID=>__("'%value%' does not appear to be a valid date"), Zend_Validate_Date::FALSEFORMAT=>__("'%value%' does not fit given date format")));
		$stDate2 = new ZendX_JQuery_Form_Element_DatePicker('startDate2', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$stDate2->addValidator($validatorDate2)
			->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
						)
			  ->setRequired(true)
			  ->setLabel('Start Date For Query')
			  ->setAttrib("readonly","readonly");
		$stDate2->setValue(date("Y-m-d"));	
		$stDate2->helpText='This is the date the query will start with.';  
	
		$endDate2 = new ZendX_JQuery_Form_Element_DatePicker('endDate2', array('jQueryParams'=>array('dateFormat'=>'yy-mm-dd')));
		$endDate2->addValidator($validatorDate2)
			->setDecorators(array('MobLabel'
							, array('UiWidgetElement', array('tag' => 'div', 'class' => 'form_input'))
							, array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form_element clearfix')))	
						)
			  ->setRequired(true)
			  ->setLabel('End Date For Query')
			  ->setAttrib("readonly","readonly");
		$endDate2->setValue(date("Y-m-d"));	
		$endDate2->helpText='This is the date the query will end with.'; 
        
        $goButton2 = new Zend_Form_Element_Submit('button');
        $goButton2->setLabel('Get The Stats');
        
        $select2 = new Zend_Form_Element_Select('select2');
		$select2->addMultiOptions(array(1=>'Yesterday', 2=>'Last Week', 3=>'Last Month',4=>'Last Quarter',
			5=>'Last Year', 6=>'Input Dates'));
		$select2->setLabel('Select Time Period to View');
		$select2->setDecorators(array('MobForm'));	
		$select2->setValue(2)->setRequired(true);
		$select2->helpText = 'This is used to select the type of interval that statistics are to be generated for';
		
        
        $this->addElement($stDate2);
        $this->addElement($endDate2);
        $this->addElement($select2);
        $this->addElement($goButton2);
        
	}
}