<?php

class Zend_View_Helper_ShowTimeRemaining
{
	public function showTimeRemaining($timestamp, $durationindays) 
	{
		$dhms=$this->calculateTimeRemaining($timestamp, $durationindays);
		if ($dhms==-1)
			return -1;
		foreach($dhms as $row=>$value) 
		{
			if ($value) 
			{
				return "$value $row";
			}
		}
	}

	private function calculateTimeRemaining($timestamp, $durationindays) 
	{
		return $this->calculateTimeElapsed(($timestamp+($durationindays*86400)),time());
	}

	private function calculateTimeElapsed($date1, $date2) {

		if( $date2 > $date1 ) {
			return -1; // timestamp is in the future
		}


		$diff = $date1-$date2;
		$seconds = 0;
		$hours = 0;
		$minutes = 0;

		if($diff % 86400 <= 0) //there are 86,400 seconds in a day
			$days = $diff / 86400;

		else {
			$rest = ($diff % 86400);
			$days = ($diff - $rest) / 86400;
			if( $rest % 3600 > 0 ) {
				$rest1 = ($rest % 3600);
				$hours = ($rest - $rest1) / 3600;
				if( $rest1 % 60 > 0 ) {
					$rest2 = ($rest1 % 60);
					$minutes = ($rest1 - $rest2) / 60;
					$seconds = $rest2;
				}
				else
				$minutes = $rest1 / 60;
			}
			else
				$hours = $rest / 3600;
			return array('days'=>$days,'hours'=>$hours,'minutes'=>$minutes,'seconds'=>$seconds);
		}
	}
}	