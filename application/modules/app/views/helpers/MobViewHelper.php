<?php

class Zend_View_Helper_MobViewHelper
{
	function mobViewHelper($label, $content, $description='',  $helptip='') 
	{
		$output='<div class="form_element clearfix">';
		$output.='	<div class="form_label">';
		$output.=		'<label>'.$label.'</label>';
		$output.='		<div class="form_description"><p>'.$description.'</p></div>';
		$output.='	</div>';
		$output.='	<div class="form_helptip">';
		$output.='		<span class="whats-this">';

		if ($helptip) {
			$output.='		<a href="javascript:void(0);"><img src="/images/icons/whats_this.gif" alt="What\'s This" /></a>';
			$output.='		<p>'.$helptip.'</p>';
		}
		$output.='		</span>';
		$output.='	</div>';

		$output.='	<div class="form_input">';
		$output.=		$content;
		$output.='	</div>';
		$output.='</div>';
		return $output;
	}
}