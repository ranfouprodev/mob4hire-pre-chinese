<?php

class App_ExperienceController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $this->_redirect('/app/project/', array ('exit'=>true));
    }

    public function clearAction() {

        Zend_Registry::get('defSession')->project = NULL;
        $this->_redirect('/app/experience/definition/');

    }

    public function viewAction()
    {
        $parameters = $this->_getAllParams();//get all passed in params

        //looking for parameter called idproject as in eg: ?idproject=1
        //if no params passed in then exit page...
        if (! isset ($parameters['idproject']))
        {
            $this->_redirect('/app/project/', array ('exit'=>true));
        }

        if ( isset ($parameters['popup'])) // When the page is to pop up in a new window, turn off the layout
        {

            $this->_helper->layout->setLayout('popup');
            $this->view->headLink()->appendStylesheet('/css/rating.css');// We have to add style sheets back in since these are included by the layout...
            $this->view->headLink()->appendStylesheet('/css/app/master.css');
            $this->view->headLink()->appendStylesheet('/css/forms.css');
            $this->view->headScript()->appendFile('/js/application.js');
        }

        //ok we have a param called idproject
        $projectId = $parameters['idproject']; //get the value and store it in $projectId variable

        $projectTable = new Projects(); //create new instance of Projects table

        $this->view->proj = $projectTable->getProject($projectId); //pass this row back to the view based on parameter passed in as idproject

        $form = new Form_Comments;

        $this->view->comments = $form;


    }

	public function definitionAction()
	{

		// Only a developer can perform this action
		$this->checkDeveloperRole();

		// This is to check if we are editing an existing project
		$parameters = $this->_getAllParams();
		if ( isset ($parameters['idproject']))
		{
			$idproject = $parameters['idproject'];
			$pj = new Projects();
			$pd = $pj->getProject($idproject);

			if($pd['projecttype'] =='survey'){
				// reroute
				$this->_redirect('/app/survey/definition/'.$idproject);	
			}elseif($pd['projecttype'] =='test'){
				// reroute
				$this->_redirect('/app/project/definition/'.$idproject);	
			}elseif($pd['projecttype'] =='accelerator'){
				// reroute
				$this->_redirect('/app/accelerator/definition/'.$idproject);	
			}
			
			// make sure it is the owner
			$canEdit = false;
        
			if(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canEdit = true;
			}elseif ($pd['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id && $pd['Status'] == 100) {
				$canEdit = true;
			}

			if (!$canEdit)
			{
				$this->_redirect('/app/experience/view/'.$idproject);
			}	

			Zend_Registry::get('defSession')->project = new stdClass ();
			Zend_Registry::get('defSession')->project->idproject = $pd['idproject'];
			Zend_Registry::get('defSession')->project->projecttype = 'experience';
		}
		$form = new Form_ExperienceStepOne;
		$this->view->zForm = $form;
		
		if ($this->getRequest()->isPost())
		{
			if(isset(Zend_Registry::get('defSession')->tempimage)){
				$projects = new Projects();
				// We will write a record to the projects table even when validation is failed
				if (!(isset(Zend_Registry::get('defSession')->project)) || !(isset(Zend_Registry::get('defSession')->project->idproject)))
				{
					$projectData=array(
									'projecttype'=>'experience',
									'iddeveloper'=>$this->view->currentUser->id,
									'Status'=>100 // Draft
								);
					Zend_Registry::get('defSession')->project->idproject = $projects->add($projectData); // write project and store its id in the session
				}

				$tempImage = Zend_Registry::get('defSession')->tempimage;
				$projectFiles = new ProjectFiles();
				$fileData = file_get_contents($tempImage['upload']);
				$fileSize = filesize($tempImage['upload']);			

				$fileid = $projectFiles->addFileToProject(Zend_Registry::get('defSession')->project->idproject, $tempImage['basefilename'], $tempImage['mimetype'], $fileSize, $fileData, "projImg", "0");
				 	
				$imagePath = '/content/image/' .$fileid; 
				$projectData = array (
					'ImagePath'=>$imagePath,
				);

				Zend_Registry::get('defSession')->tempimage=NULL; // Unset the temporary image so that we don't get duplicates
				$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);	
			}
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$formValues = $form->getValues();
				// write form values to database here
				$projects = new Projects();

				// maybe instead of the following clumsy operation, we should rename the form elements to match the database
				$projectData = array (
					'iddeveloper'=>$this->view->currentUser->id,
					'projecttype'=>'experience',
					'package'=>$formValues['svyPkg'],
					'Name'=>$formValues['projName'],
					'description'=>$formValues['projDesc'],
					'longDesc'=>$formValues['projDetail'],
					'inviteonly'=>$formValues['inviteType'],
					'invitemessage'=>$formValues['mobInvite'],
					'flatfee'=>1, // Always flat fee for experience
					'budget'=>$formValues['svyAmount'],
					'StartDate'=>$formValues['sDate'],
					'testenddate'=>$formValues['eDate'],
					'Duration'=>$formValues['projDur'],
					'estTime'=>$formValues['estTime'],
					'estData'=>$formValues['estData'],
					'tags'=>$formValues['tags'],
					'Status'=>100 // Draft
				);
				if (isset(Zend_Registry::get('defSession')->project) && Zend_Registry::get('defSession')->project->idproject)
				{
					$projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
				}
				else
				{
					Zend_Registry::get('defSession')->project->idproject = $projects->add($projectData); // write project and store its id in the session
				}
				Zend_Registry::get('defSession')->project->projecttype = 'experience';
				// redirect to next step
				$this->_redirect('/app/experience/selectpanel');
			}
		}
		else
		{
			if (Zend_Registry::get('defSession')->project && isset (Zend_Registry::get('defSession')->project->idproject) && Zend_Registry::get('defSession')->project->projecttype =='experience')
			{
				$projects = new Projects();
				$projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
		
				$formData = array (
					'svyPkg'=>$projectData['package'],
					'inviteType'=>$projectData['inviteonly'],
					'svyAmount'=>$projectData['budget'],
					'projName'=>$projectData['Name'],
					'projDesc'=>$projectData['description'],
					'projDetail'=>$projectData['longDesc'],
					'tags'=>$projectData['tags'],
					'sDate'=>$projectData['StartDate'],
					'eDate'=>$projectData['testenddate'],
					'projDur'=>$projectData['Duration'],
					'estTime'=>$projectData['estTime'],
					'estData'=>$projectData['estData'],
					'mobInvite'=>$projectData['invitemessage'],
				);
				$form->populate($formData);
			}
			else
			{
				// We need to create the session variable here to store the image
				Zend_Registry::get('defSession')->project = new stdClass ();
				Zend_Registry::get('defSession')->project->projecttype = 'experience';
	
			}
   			
		}
	}


    public function selectpanelAction() {

	
        // Ensure the user can see this page
        $this->checkRole();

        $this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
        $this->view->headScript()->appendFile('/js/devices/list.js');
        $this->view->headScript()->appendFile('/js/networks/addnetworksproject.js');
        $this->view->headScript()->appendFile('/js/handsets/addhandsetsproject.js');


        $form = new Form_ExperienceStepTwo;
        $this->view->zForm = $form;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ( isset ($formData['previous']))
            $this->_redirect('/app/experience/definition');

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                // write form values to database here
                $projects = new Projects();
                // maybe instead of the following clumsy operation, we should rename the form elements to match the database
                $projectData = array (
                'region'=>$formValues['region'],
 				'maxtests'=>$formValues['amtMobsters']
                );
                if (Zend_Registry::get('defSession')->project->idproject)
                {
                    $projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
                }

                // Check and add invited mobsters.
                $invites = new TesterInvites();
                $userlist = explode(',', $formValues['contactsSelect']);
                if (sizeof($userlist) > 0)
                $invites->addInvitesByName(Zend_Registry::get('defSession')->project->idproject, $userlist);


                if ( isset ($formData['next']))
                $this->_redirect('/app/experience/resources');
                elseif ( isset ($formData['previous']))
                $this->_redirect('/app/experience/definition');
            }
        }
        else
        {
            // if the idproject session variable is set then populate the form
            if (Zend_Registry::get('defSession')->project->idproject)
            {
                $projects = new Projects();
                $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
                $formData = array (
                'amtMobsters'=>$projectData['maxtests'],
                'region'=>$projectData['region'],
                );

                $invites = new TesterInvites();
                $inviteList = $invites->getInvites(Zend_Registry::get('defSession')->project->idproject);


                if (sizeof($inviteList) > 0) {
                    $inviteStr = " ";
                    foreach ($inviteList as $invite) {
                        $inviteStr .= $invite['username'].',';
                    }
                    $formData['contactsSelect'] = substr($inviteStr, 0, -1);
                }
                $form->populate($formData);
            }
        }


    }




    public function resourcesAction()
    {
    	
	     // Ensure the user can see this page
        $this->checkRole();

        $this->view->headScript()->appendFile('/js/files/list.js');

        $form = new Form_ExperienceStepThreeA;
        $this->view->zForm = $form;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                // write form values to database here
                $projects = new Projects();
                // maybe instead of the following clumsy operation, we should rename the form elements to match the database
                $projectData = array (
                'weburl'=>$formValues['mobURL'],
		'appurl'=>$formValues['appStoreURL']
                );
                
                if (Zend_Registry::get('defSession')->project->idproject)
                {
                	$survey = new Survey();
                	$token=new Token();
                	
                	$idproject=Zend_Registry::get('defSession')->project->idproject;
			$projects->updateProject($idproject, $projectData);
			$projectData=$projects->getProject($idproject);
			$appName=$formValues['appname'];
               		$handset=$formValues['handsetname'];
 //               	$title=$formValues['surveyname'];
			$title=$projectData['Name'];
                    if(Zend_Registry::get('defSession')->idsurvey)
                    {
                    	//We must update a survey
                    	$idsurvey=$formValues['idsurvey'];
                    	$surveyData = array('title'=>$title);
                		$survey->updateSurvey($idsurvey, $title, $idproject);
                    }
                    else
                    {
                    	//We need to make a new survey here
                    	$idsurvey=$survey->createNewSurvey($title, $idproject);
                    	Zend_Registry::get('defSession')->idsurvey=$idsurvey;
                    	$surveyQuestion = new SurveyQuestion();
                    	$questions=$surveyQuestion->getQuestionsBySurvey(1);
						foreach($questions as $q)
						{
							$surveyQuestion->addQuestionToSurvey($idsurvey, $q['idquestion'], $q['order']);
						}
						$branch = new Branch();
						$branch->copyAnotherSurveyBranch($idsurvey,1);
                    }
                    $token->updateTokenByTokenAndSurvey($idsurvey, 'HANDSET MANUFACTURER & MODEL', $handset);
                	$token->updateTokenByTokenAndSurvey($idsurvey, 'APPLICATION NAME', $appName);
                }
                
                
                //				print_r($formData);
                if ( isset ($formData['next']))
                	$this->_redirect('/app/experience/questions');
                elseif ( isset ($formData['previous']))
                	$this->_redirect('/app/experience/selectpanel');
                
            }
        }
        else
        {
            // if the idproject session variable is set then populate the form
            if (Zend_Registry::get('defSession')->project->idproject)
            {
                $projects = new Projects();
             	$survey = new Survey();
             	$token = new Token();
             		
             	$idproject=Zend_Registry::get('defSession')->project->idproject;
                $projectData = $projects->getProject($idproject);
                $check = $survey->countSurveyByProject($idproject);
                if($check>0)
                {
                	
	                $surveyRow = $survey->getSurveyByProjectID($idproject);
	                $title = $surveyRow['title'];
	                $idsurvey=$surveyRow['idsurvey'];
	                $tokenRows=$token->getTokenBySurvey($idsurvey);
	                $appName="";
	                $handset="";
	                Zend_Registry::get('defSession')->idsurvey=$idsurvey;
	                foreach($tokenRows as $t)
	                {
	                	if($t['token']=='APPLICATION NAME')
	                	{
	                		$appName=$t['value'];
	                	}
	                	if($t['token']=='HANDSET MANUFACTURER & MODEL')
	                	{
	                		$handset=$t['value'];
	                	}
	                }
	                $formData = array ('mobURL'=>$projectData['weburl'], 'surveyname'=>$title,
               		'appname'=>$appName,'handsetname'=>$handset,'idsurvey'=>$idsurvey,
                	);
                }
                else
                {
                	unset(Zend_Registry::get('defSession')->idsurvey);
                	 $formData = array (
							'mobURL'=>$projectData['weburl'],	
							'appStoreURL'=>$projectData['appurl']
							);//todo: Save all the upload paths from the session here
                }
                
                $form->populate($formData);
            }
        }

    }

	public function questionsAction()
    {
    	$this->checkRole();
    	$this->view->headScript()->appendFile('/js/jquery.js');
		$this->view->headScript()->appendFile('/js/survey/listanswers.js');
		
		$form = new Form_ExperienceStepThreeB;
        $this->view->zForm = $form;
        if ($this->getRequest()->isPost())
        {
        	$formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                if (Zend_Registry::get('defSession')->project->idproject && Zend_Registry::get('defSession')->idsurvey)
	        	{
	        		
	        	}
            }
        	if ( isset ($formData['next']))
            	$this->_redirect('/app/experience/estimates');
            elseif ( isset ($formData['previous']))
            	$this->_redirect('/app/experience/resources');
        }
        else
        {
        	if (Zend_Registry::get('defSession')->project->idproject && Zend_Registry::get('defSession')->idsurvey)
        	{
        		//The correct info is loaded lets get the info into it
        		$idsurvey=Zend_Registry::get('defSession')->idsurvey;
        		$formData = array('idsurvey'=>$idsurvey,);
        		$form->populate($formData);
        		$this->view->idsurvey=$idsurvey;
        	}
        	elseif(Zend_Registry::get('defSession')->project->idproject)
        	{
        		$this->_redirect('/app/experience/resources');
        	}
        	else
        	{
        		$this->_redirect('/app/experience/definition');
        	}
        }
        
    }

    public function estimatesAction()
    {
       // Ensure the user can see this page
        $this->checkRole();

        $form = new Form_ExperienceStepFour;
        // $this->view->headScript()->appendFile('/js/estimatesPage.js');
        $this->view->estimates = $form;

        //estimates table
        $profiletable = new ProfileContactInfo();
        $profile = $profiletable->getProfile($this->view->currentUser->id);
        $invData = array (
        "billto"=>$profile['username'],
        "est#"=>$profile['id'].'-'.Zend_Registry::get('defSession')->project->idproject,
        "billToName"=>$profile['first_name']." ".$profile['last_name'],
        "mob"=>"Mob4hire Inc.",
        "billtoAddr"=>"",
        "mobAddr"=>"",
        "billtoCity"=>$profile['city'],
        "mobCity"=>"Calgary, Alberta",
        "billtoCountry"=>$profile['country'].' '.$profile['postcode'],
        "mobCountry"=>"Canada T2E 6M7",
        "billtoInfo"=>"Att: ".$profile['first_name']." ".$profile['last_name'],
        "mobInfo"=>"Inquiries: accounting@mob4hire.com");

        $projects = new Projects();
        $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
        $this->view->projectData = $projectData;

        $subtotal = 0;
        if ($projectData['package'] == 2) {
            $this->view->projectFee = $projectFee = Zend_Registry::get('configuration')->m4h->premiumSurveyFee;
            $subtotal += $projectFee;
            $rate = Zend_Registry::get('configuration')->m4h->premiumSurveyRate;
        } else {
            $this->view->projectFee = $projectFee = Zend_Registry::get('configuration')->m4h->basicSurveyFee;
            $subtotal += $projectFee;
            $rate = Zend_Registry::get('configuration')->m4h->basicSurveyRate;

        }

        if ($projectData['inviteonly'] == 1) // open to all
        {
           $this->view->testers = $projectData['maxtests'];
		    $this->view->fixedfee = $fixedFee = $projectData['maxtests']*$projectData['budget'];
            $subtotal += $fixedFee;

            $this->view->commission = $commission = $fixedFee*($rate/100);
            $subtotal += $commission;
        } else {
            //  count the invited testers
            $ti = new TesterInvites();
            $count = $ti->count($projectData['idproject']);

			$this->view->testers = $count;
            $this->view->fixedfee = $fixedFee = $count*$projectData['budget'];
            $subtotal += $fixedFee;

            $this->view->commission = $commission = $fixedFee*($rate/100);
            $subtotal += $commission;

        }

        if ($projectData['package'] == 3) {
            $subtotal = 0;
        }

        $this->view->subtotal = $subtotal;



        $this->view->estimate = $invData;
        $form2 = new Form_ExperienceStepFour2;

        $form2->setAction('/app/index/deposit-funds')->setMethod('post');
        $this->view->estimatesPt2 = $form2;

        $testTp = array (1=>"Basic", 2=>"Premium", 3=>"Managed");
        $this->view->testType = $testTp[$this->view->projectData['package']];

        //This is really nasty, but I do not know how else to do it without a major retooling
        $form->getElement('projReview2')->setContent("<td><span class='mobblack'>".$this->view->experienceReview($this->view->projectData)."</span></td>");


        // Form is redirected to the paypal deposit page
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                // write form values to database here
                print_r($formData);

                if ( isset ($formData['next']))
                {
                    // need to unset the session variables here. Probably would be better if they were all in an array so they could be unset as one todo
                    // $this->_redirect('/app/project/estimates'); // Not sure what to do here: need to redirect to paypal if paypal selected, confirmation message otherwise


                    // If the account balance requires funds go to paypal if not back to dashboard
                    //$this->_forward('/app/index/depositfunds');

                }
                elseif ( isset ($formData['previous']))
                $this->_redirect('/app/experience/questions');
            }
        }

    }
    
    

	// All projects should inherit one model. 
    protected function checkRole() {

	if (! isset (Zend_Registry::get('defSession')->project) || Zend_Registry::get('defSession')->project->projecttype != 'experience') {
            $this->_redirect('/app/project/');
        }

    }

	protected function checkDeveloperRole(){
		
		// redirect if the user doesn't have a developer profile set 
		$profileTable = new ProfileDeveloper();
		if(!$profileTable->hasProfile($this->view->currentUser->id)){
			
			$flashMessenger = $this->_helper->getHelper('FlashMessenger');
			$flashMessenger->addMessage('permission');
	
			$this->_redirect('/app/project/error');
		}
		
	}

}

?>
