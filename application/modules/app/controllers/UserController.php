<?php

class App_UserController extends Zend_Controller_Action
{
	
	public function listAction(){ 
	
		$this->_helper->layout->disableLayout();
		$whereClause=$this->getRequest()->getParam('whereClause');
		$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
		
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly

		
		$regdevices=new Regdevices();
		$this->view->numberOfPages=((int)(($this->view->totalHandsets=$regdevices->distinctDevicesCount($whereClause))/$rowsPerPage))+1; //  Get total rows and calculate number of pages
		$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	
	}
	
	
	
	
} 