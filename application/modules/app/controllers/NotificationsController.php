<?php

/*
 * Ajax module for the notification features
 */
class App_NotificationsController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_redirect("/app/");
	}
 
 	public function listAction()
	{
		$this->_helper->layout->disableLayout();
 		
		$userid =  Zend_Registry::get('defSession')->currentUser->id;
	
	   	$notifications = new Notifications(); 
		$notificationData = $notifications->getNotificationsForUser($userid);
		$this->view->notifications = $notificationData;
		
		// Notifications are only seen once
		foreach($notificationData as $notification){
			$notifications->removeNotification($notification['idnotification']);
		}
		
 	}
	
	
	public function deleteAction()
	{
		
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
        if ( isset ($parameters['id']))
        {
	       try{
	       	
	        // Check for permission to delete
			$notificationId = $parameters['id'];
        	$notifications = new Notifications(); 
			$notificationData = $notifications->getNotification($notificationId);
			
							
			// Current User
			$canDelete = $this->canDelete($notificationData);
						
			if($canDelete)
			{
				$notifications->removeNotification($notificationId);
			}
			
			$notificationData = $notifications->getNotificationsForUser(Zend_Registry::get('defSession')->currentUser->id);			
			$this->view->notifications = $notificationData;
			
		   }catch(Exception $e){
			   	// bad input data likely. 
		   }
			
		}
	}
	
	public function addAction()
	{
		if(Zend_Registry::get('defSession')->currentUser->isAdmin){
			
			$this->_helper->layout->disableLayout();
			$parameters = $this->_getAllParams();
	        if ( isset ($parameters['id']) && isset($parameters['msg']))
	        {
		       $notifications = new Notifications(); 
	  		   $category = $parameters['id'];   	
			   $message = $parameters['msg'];
			   $userid =  Zend_Registry::get('defSession')->currentUser->id;
			   
			   $notifications->addNotification($category,$userid,$message);
				
				$notificationData = $notifications->getNotifications($category);			
				$this->view->notifications = $notificationData;
				
			}
			
		}else{
			$this->_redirect("/app/");
		}
	
		
	}
	
	protected function canDelete($notificationData){
		
		if($notificationData['iduser'] == Zend_Registry::get('defSession')->currentUser->id){
				$canDelete = true;			
			}elseif(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canDelete=true;
			}else{
				$canDelete=false;
			}
			
		return $canDelete;
	}
 
 
}//end class