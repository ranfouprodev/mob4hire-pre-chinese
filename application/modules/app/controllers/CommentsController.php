<?php

/*
 * Ajax module for the generic comment classes
 */
class App_CommentsController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_redirect("/app/");
	}
 
 	public function listAction()
	{
	$this->_helper->layout->disableLayout();
 		$parameters = $this->_getAllParams();
        if ( isset ($parameters['id']))
        {
        	$comments = new Comments(); 
			$commentData = $comments->getComments($parameters['id']);
				
			$this->view->comments = $commentData;	
			
		;
		}
 	}
	
	
	public function deleteAction()
	{
			$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
        if ( isset ($parameters['id']))
        {
	       try{
	       	
	        // Check for permission to delete
			$commentId = $parameters['id'];
        	$comments = new Comments(); 
			$commentData = $comments->getComment($commentId);
			
			$category = $commentData['category'];
				
			// Current User
			$canDelete = $this->canDelete($commentData);
			
						
			if($canDelete)
			{
				$comments->removeComment($commentId);
			}
			
			$commentData = $comments->getComments($category);			
			$this->view->comments = $commentData;
			
		   }catch(Exception $e){
			   	// bad input data likely. 
		   }
			
		}
	}
	
	public function addAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
        if ( isset ($parameters['id']) && isset($parameters['msg']))
        {
	       $comments = new Comments(); 
  		   $category = $parameters['id'];   	
		   $message = $parameters['msg'];
		   $userid =  Zend_Registry::get('defSession')->currentUser->id;
		   
		   $comments->addComment($category,$userid,$message);
			
			$commentData = $comments->getComments($category);			
			$this->view->comments = $commentData;
			
		}
	}
	
	protected function canDelete($commentData){
		
		if($commentData['iduser'] == Zend_Registry::get('defSession')->currentUser->id){
				$canDelete = true;			
			}elseif(substr($commentData['category'], 0, 3)=="pro" ){
				// project owner can delete the comment
				$idproject = substr($commentData['category'],3);
				$project = new Projects(); 
				$projectData = $project->getProject($idproject);
				
				($projectData['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id )?$canDelete=true:$canDelete=false;			
			}elseif(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canDelete=true;
			}else{
				$canDelete=false;
			}
			
			
		return $canDelete;
	}
 
 
}//end class