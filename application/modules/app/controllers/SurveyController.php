<?php

class App_SurveyController extends Zend_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }

    public function indexAction()
    {
        $this->_redirect('/app/project/', array ('exit'=>true));
    }

    public function clearAction() {

        Zend_Registry::get('defSession')->project = NULL;
        $this->_redirect('/app/survey/definition/');

    }

    public function viewAction()
    {
        $parameters = $this->_getAllParams();//get all passed in params

        //looking for parameter called idproject as in eg: ?idproject=1
        //if no params passed in then exit page...
        if (! isset ($parameters['idproject']))
        {
            $this->_redirect('/app/project/', array ('exit'=>true));
        }

        if ( isset ($parameters['popup'])) // When the page is to pop up in a new window, turn off the layout
        {

            $this->_helper->layout->setLayout('popup');
            $this->view->headLink()->appendStylesheet('/css/rating.css');// We have to add style sheets back in since these are included by the layout...
            $this->view->headLink()->appendStylesheet('/css/app/master.css');
            $this->view->headLink()->appendStylesheet('/css/forms.css');
            $this->view->headScript()->appendFile('/js/application.js');
        }

        //ok we have a param called idproject
        $projectId = $parameters['idproject']; //get the value and store it in $projectId variable

        $projectTable = new Projects(); //create new instance of Projects table

        $this->view->proj = $projectTable->getProject($projectId); //pass this row back to the view based on parameter passed in as idproject

        $form = new Form_Comments;

        $this->view->comments = $form;


    }

    public function definitionAction() {


        $this->checkResearcherRole();

        // This is to check if we are editing an existing survey
        $parameters = $this->_getAllParams();
        if ( isset ($parameters['idproject']))
        {
            $idproject = $parameters['idproject'];
            $pj = new Projects();
            $pd = $pj->getProject($idproject);

            // make sure it is the owner
            $canEdit = false;


            // make sure the id is a survey!
            if ($pd['projecttype'] != 'survey') {
                $canEdit = false;
            }
            if (Zend_Registry::get('defSession')->currentUser->isAdmin) {
                $canEdit = true;
            } elseif ($pd['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id && $pd['Status'] == 100) {
                $canEdit = true;
            }

            if (!$canEdit)
            {
                $this->_redirect('/app/survey/view/'.$idproject);
            }


            Zend_Registry::get('defSession')->project = NULL;
            Zend_Registry::get('defSession')->project = $pd;
            Zend_Registry::get('defSession')->project->projecttype = 'survey';
        }


        $form = new Form_surveystepone;
        $this->view->zForm = $form;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                // write form values to database here
                $projects = new Projects();

                // maybe instead of the following clumsy operation, we should rename the form elements to match the database
                $projectData = array (
                'iddeveloper'=>$this->view->currentUser->id,
                'projecttype'=>'survey',
                'package'=>$formValues['svyPkg'],
                'Name'=>$formValues['projName'],
                'description'=>$formValues['projDesc'],
                'longDesc'=>$formValues['projDetail'],
                'inviteonly'=>$formValues['inviteType'],
                'invitemessage'=>$formValues['mobInvite'],
                'budget'=>$formValues['svyAmount'],
                'StartDate'=>$formValues['sDate'],
                'Duration'=>$formValues['projDur'],
                'estTime'=>$formValues['estTime'],
                'estData'=>$formValues['estData'],
                'tags'=>$formValues['tags'],
                'UpLoadPath'=>Zend_Registry::get('defSession')->project->UpLoadPath,
                'Status'=>100 // Draft
                );

                if (Zend_Registry::get('defSession')->project->idproject)
                {
                    $projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
                }
                else
                {
                    Zend_Registry::get('defSession')->project->idproject = $projects->add($projectData); // write project and store its id in the session
                }

				    Zend_Registry::get('defSession')->project->projecttype = 'survey';

                // Check for temporary file in session and create the entry for it

                if(isset(Zend_Registry::get('defSession')->tempimage)){
					
				
					$tempImage = Zend_Registry::get('defSession')->tempimage;
							
					$projectFiles = new ProjectFiles();
					
					
					$fileData = file_get_contents($tempImage['upload']);
					$fileSize = filesize($tempImage['upload']);			
					
					$fileid = $projectFiles->addFileToProject(Zend_Registry::get('defSession')->project->idproject, $tempImage['basefilename'],$tempImage['mimetype'], $fileSize, $fileData, "projImg", "0");
				 	
					$imagePath = '/content/image/' .$fileid; 
					$projectData = array (
						'ImagePath'=>$imagePath,
					);


					$projects->updateProject(Zend_Registry::get('defSession')->project->idproject,$projectData);	
						
					//  We are not storing files on the server. They must be in the db because there is no san and on site updates all the temp images 
					// will be destroyed. 
					
					//$imagePath = strstr($tempImage['upload'], '/uploads'); // strip out the document root
					//echo "<h1>$imagePath</h1>";
					//$uploadPath = $tempImage['folder']; // We need to store this so that uploads on the resources page go in the same folder. maybe it doesn't matter
				}



                // redirect to next step
                $this->_redirect('/app/survey/selectpanel');

            }
        }
        else
        {

            // if the idproject session variable is set then populate the form
            //if(Zend_Registry::get('defSession')->project && property_exists(Zend_Registry::get('defSession')->project, 'idproject'))
            if (Zend_Registry::get('defSession')->project && isset (Zend_Registry::get('defSession')->project->idproject)
            && isset (Zend_Registry::get('defSession')->project->projecttype)
            && Zend_Registry::get('defSession')->project->projecttype == 'survey')
            {
                $projects = new Projects();
                $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);

                $formData = array (
                'svyPkg'=>$projectData['package'],
                'inviteType'=>$projectData['inviteonly'],
                'svyAmount'=>$projectData['budget'],
                'projName'=>$projectData['Name'],
                'projDesc'=>$projectData['description'],
                'projDetail'=>$projectData['longDesc'],
                'tags'=>$projectData['tags'],
                'sDate'=>$projectData['StartDate'],
                'projDur'=>$projectData['Duration'],
                'estTime'=>$projectData['estTime'],
                'estData'=>$projectData['estData'],
                'mobInvite'=>$projectData['invitemessage'],
                );
                $form->populate($formData);


                // occurs on temporary files + validation errors
                //if ( isset (Zend_Registry::get('defSession')->tempimage->imageurl)) {
                //    $this->view->imageurl = Zend_Registry::get('defSession')->tempimage->imageurl;
               // }


            }
            else
            {
                // We need to create the session variable here to store the image
                Zend_Registry::get('defSession')->project = new stdClass();
                Zend_Registry::get('defSession')->project->projecttype = 'survey';

            }


        }

    }


    public function selectpanelAction() {

        // Ensure the user can see this page
        $this->checkRole();

        $this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
        $this->view->headScript()->appendFile('/js/devices/list.js');
        $this->view->headScript()->appendFile('/js/networks/addnetworksproject.js');
        $this->view->headScript()->appendFile('/js/handsets/addhandsetsproject.js');


        $form = new Form_surveysteptwo;
        $this->view->zForm = $form;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ( isset ($formData['previous']))
            $this->_redirect('/app/survey/definition');

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                // write form values to database here
                $projects = new Projects();
                // maybe instead of the following clumsy operation, we should rename the form elements to match the database
                $projectData = array (
                'region'=>$formValues['region'],
				'maxtests'=>$formValues['amtMobsters']
                );
                if (Zend_Registry::get('defSession')->project->idproject)
                {
                    $projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
                }

                // Check and add invited mobsters.
                $invites = new TesterInvites();
                $userlist = explode(',', $formValues['contactsSelect']);
                if (sizeof($userlist) > 0)
                $invites->addInvitesByName(Zend_Registry::get('defSession')->project->idproject, $userlist);


                if ( isset ($formData['next']))
                $this->_redirect('/app/survey/resources');
                elseif ( isset ($formData['previous']))
                $this->_redirect('/app/survey/definition');
            }
        }
        else
        {
            // if the idproject session variable is set then populate the form
            if (Zend_Registry::get('defSession')->project->idproject)
            {
                $projects = new Projects();
                $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
                $formData = array (
                'amtMobsters'=>$projectData['maxtests'],
                'region'=>$projectData['region'],
                );

                $invites = new TesterInvites();
                $inviteList = $invites->getInvites(Zend_Registry::get('defSession')->project->idproject);


                if (sizeof($inviteList) > 0) {
                    $inviteStr = " ";
                    foreach ($inviteList as $invite) {
                        $inviteStr .= $invite['username'].',';
                    }
                    $formData['contactsSelect'] = substr($inviteStr, 0, -1);
                }
                $form->populate($formData);
            }
        }


    }




    public function resourcesAction()
    {
        // Ensure the user can see this page
        $this->checkRole();

        $this->view->headScript()->appendFile('/js/files/list.js');

        $form = new Form_surveystepthree;
        $this->view->zForm = $form;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $formValues = $form->getValues();
                // write form values to database here
                $projects = new Projects();
                // maybe instead of the following clumsy operation, we should rename the form elements to match the database
                $projectData = array (
                'weburl'=>$formValues['mobURL'],
                );
                if (Zend_Registry::get('defSession')->project->idproject)
                {
                    $projects->updateProject(Zend_Registry::get('defSession')->project->idproject, $projectData);
                }
                //				print_r($formData);
                if ( isset ($formData['next']))
                $this->_redirect('/app/survey/estimates');
                elseif ( isset ($formData['previous']))
                $this->_redirect('/app/survey/selectpanel');
            }
        }
        else
        {
            // if the idproject session variable is set then populate the form
            if (Zend_Registry::get('defSession')->project->idproject)
            {
                $projects = new Projects();
                $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
                $formData = array (
                'mobURL'=>$projectData['weburl'],
                ); //todo: Save all the upload paths from the session here
                $form->populate($formData);
            }
        }

    }


    public function estimatesAction()
    {
        // Ensure the user can see this page
        $this->checkRole();

        $form = new Form_surveystepfour;
        // $this->view->headScript()->appendFile('/js/estimatesPage.js');
        $this->view->estimates = $form;

        //estimates table
        $profiletable = new ProfileContactInfo();
        $profile = $profiletable->getProfile($this->view->currentUser->id);
        $invData = array (
        "billto"=>$profile['username'],
        "est#"=>$profile['id'].'-'.Zend_Registry::get('defSession')->project->idproject,
        "billToName"=>$profile['first_name']." ".$profile['last_name'],
        "mob"=>"Mob4hire Inc.",
        "billtoAddr"=>"",
        "mobAddr"=>"",
        "billtoCity"=>$profile['city'],
        "mobCity"=>"Calgary, Alberta",
        "billtoCountry"=>$profile['country'].' '.$profile['postcode'],
        "mobCountry"=>"Canada T2E 6M7",
        "billtoInfo"=>"Att: ".$profile['first_name']." ".$profile['last_name'],
        "mobInfo"=>"Inquiries: accounting@mob4hire.com");

        $projects = new Projects();
        $projectData = $projects->getProject(Zend_Registry::get('defSession')->project->idproject);
        $this->view->projectData = $projectData;

        $subtotal = 0;
        if ($projectData['package'] == 2) {
            $this->view->projectFee = $projectFee = Zend_Registry::get('configuration')->m4h->premiumSurveyFee;
            $subtotal += $projectFee;
            $rate = Zend_Registry::get('configuration')->m4h->premiumSurveyRate;
        } else {
            $this->view->projectFee = $projectFee = Zend_Registry::get('configuration')->m4h->basicSurveyFee;
            $subtotal += $projectFee;
            $rate = Zend_Registry::get('configuration')->m4h->basicSurveyRate;

        }
		
        if ($projectData['inviteonly'] == 0) // open to all
        {
            $this->view->testers = $projectData['maxtests'];
		    
			$fixedFee = $projectData['maxtests']*$projectData['budget'];
			$this->view->fixedFee = $fixedFee;
            $subtotal += $fixedFee;

            $this->view->commission = $commission = $fixedFee*($rate/100);
            $subtotal += $commission;
        } else {
            //  count the invited testers
            $ti = new TesterInvites();
            $count = $ti->count($projectData['idproject']);

			$this->view->testers = $count;
            $this->view->fixedfee = $fixedFee = $count*$projectData['budget'];
            $subtotal += $fixedFee;

            $this->view->commission = $commission = $fixedFee*($rate/100);
            $subtotal += $commission;

        }

        if ($projectData['package'] == 3) {
            $subtotal = 0;
        }

        $this->view->subtotal = $subtotal;



        $this->view->estimate = $invData;
        $form2 = new Form_surveystepfour2;

        $form2->setAction('/app/index/deposit-funds')->setMethod('post');
        $this->view->estimatesPt2 = $form2;

        $testTp = array (1=>"Basic", 2=>"Premium", 3=>"Managed");
        $this->view->testType = $testTp[$this->view->projectData['package']];

        //This is really nasty, but I do not know how else to do it without a major retooling
        $form->getElement('projReview2')->setContent("<td><span class='mobblack'>".$this->view->surveyReview($this->view->projectData)."</span></td>");


        // Form is redirected to the paypal deposit page
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                // write form values to database here
                //print_r($formData);

                if ( isset ($formData['next']))
                {
                    // need to unset the session variables here. Probably would be better if they were all in an array so they could be unset as one todo
                    // $this->_redirect('/app/project/estimates'); // Not sure what to do here: need to redirect to paypal if paypal selected, confirmation message otherwise


                    // If the account balance requires funds go to paypal if not back to dashboard
                    //$this->_forward('/app/index/depositfunds');

                }
                elseif ( isset ($formData['previous']))
                $this->_redirect('/app/survey/resources');
            }
        }

    }


    protected function checkRole() {

	if (! isset (Zend_Registry::get('defSession')->project) || Zend_Registry::get('defSession')->project->projecttype != 'survey') {
            $this->_redirect('/app/project/');
        }

    }
	
	protected function checkResearcherRole(){
		
		// redirect if the user doesn't have a developer profile set 
		$profileTable = new ProfileResearcher();
		if(!$profileTable->hasProfile($this->view->currentUser->id)){
			
			$flashMessenger = $this->_helper->getHelper('FlashMessenger');
			$flashMessenger->addMessage('permission_researcher');
	
			$this->_redirect('/app/project/error');
		}
		
	}

}//end class App_SurveyController

?>
