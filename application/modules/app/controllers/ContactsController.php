<?php

class App_ContactsController extends Zend_Controller_Action
{
 	
	public function init() {
        $wall = new Wall(); 
        $this->view->unread = $wall->countUnread($this->view->currentUser->id); 
        
        $this->view->headScript()->appendFile('/js/contactlist/contacts.js');

        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }
	
	
	public function indexAction()
	{
		$this->_helper->layout->setLayout('inbox');		
	}
 

	
 	public function searchAction()
	{
		
		$parameters = $this->_getAllParams();
		
		if(!isset($parameters['search']))
			$this->_redirect('/app/contacts/index');	
			
		
		if(isset($parameters['nolayout']))
			$this->_helper->layout->disableLayout();
		else	
			$this->_helper->layout->setLayout('inbox');	
	

		
		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->page=$page=1;
			
	
			$this->view->search = $parameters['search'];
		
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage=$resultsPerPage = $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
					
 	}
 	
	public function manageAction(){
		
		
		$this->_helper->layout->setLayout('inbox');
		$this->view->currentPage = "manage";

		$this->view->resultsPerPage=$resultsPerPage=5;
		$this->view->page=$page=1;
		
		$options = array('selector'=>'true');
	
		
		$this->view->options = $options;
		
	}
	
	
	
	public function listAction()
	{
		$this->_helper->layout->disableLayout();
	
		$parameters = $this->_getAllParams();
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
		if(isset($parameters['showcomment']))
			$options['showcomment'] = $parameters['showcomment'];
				
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;

 	}
 	

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
        if (isset ($parameters['remove']))
        {
	       $users = explode(",",$parameters['remove']);
			
	       $contacts=new Contacts();
	       
	       foreach($users as $user){
	       		$contacts->remove($this->view->currentUser->id,$user);
	       }
        	
		}
		
		$parameters = $this->_getAllParams();
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
		if(isset($parameters['showcomment']))
			$options['showcomment'] = $parameters['showcomment'];
				
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	

		if(isset($parameters['layout']))
			$options['layout'] = $parameters['layout'];				
			
		$this->view->options = $options;
		
		
	}
	
	public function addAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();
        if ( isset ($parameters['id']))
        {
		   $id = $parameters['id'];
			$userid =  Zend_Registry::get('defSession')->currentUser->id;
				
			$contacts=new Contacts();
			$contacts->add($userid, $id);
			
			$this->view->contacts=$contacts->listContactsByUser($userid);
	   
	   
		}
	}
	
 
 
}//end class