<?php

class App_GroupController extends Zend_Controller_Action
{
	
	public function init() {
        
        $this->view->headScript()->appendFile('/js/groups/groups.js');
    }
	
	public function indexAction()
	{
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		$options = array(); 
		$options['showcomment'] = true;
			
		$this->view->options = $options;
		$this->view->mode = "all";
		
	}
	
	
/*
	 * Plugin component to allow for selecting of multiple users
	 * 
	 * Add a mode parameter to switch between different types of selecting. Just enabled for group invites now
	 * 
	 */
	public function selectorAction(){
		
		
		$this->_helper->layout->disableLayout();
		
			
		$parameters = $this->_getAllParams();
				
		if(!isset($parameters['idgroup']))
			return "Whoa, that wasn't supposed to happen.";
		
		$this->view->idgroup = $parameters['idgroup'];	
			
		$this->view->resultsPerPage=0;
		$this->view->page=$page=1;
		
		$options = array('selector'=>'true','invite_group_selector'=>'true');
		$this->view->options = $options;
		
	}

	
	
	/*
	 * Invites users in to the group. Automatically adds them
	 */
	public function inviteAction()
	{
		
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
				
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Whoa, that wasn't supposed to happen. Couldn't add those contacts. You will have to try again.";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
		$groupInvites = new GroupInvites();
		
		$count = 0; 
        if (isset ($parameters['choose']) && ($this->view->currentUser->id == $group->idowner) )
        {
	       $users = explode(",",$parameters['choose']);

	       foreach($users as $user){
	       		$count++; 
	       		$groupInvites->addUserToGroup($group->idgroup,$user);
	       }
		}
	
		return $count." users added to your group";
		
	}
	
	
	
	public function addAction()
	{

		$this->view->headScript()->appendFile('/js/uploadify/swfobject.js');
		$this->view->headScript()->appendFile('/js/uploadify/jquery.uploadify.v2.1.0.min.js');
		$this->view->headLink()->appendStylesheet('/js/uploadify/uploadify.css');
	
		$form = new Form_NewGroup;

		$this->view->form = $form;
		
		
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData))
			{
				$groups = new Groups();
				
				$id = $groups->add($this->view->currentUser->id,$formData);
				
				// add and redirect to view	
				$this->_redirect('/app/group/view/'.$id);
			}
		}

	}
	/*
	 * Close a group down
	 */
	public function closeAction(){ 
		
		$parameters = $this->_getAllParams();
		
		if(!isset($parameters['id']))
			$this->view->error = "That group is closed or we couldn't find it";
		else{
			$idgroup = $parameters['id'];	
			$groups = new Groups();
			
			if(!$groups->isAdmin($this->view->currentUser->id, $idgroup)){
				$this->view->error = "Only the group owner can close a group.";	
			}else{
				$groups->close($idgroup);	
			}
		}
		 	
	}
	
	/*
	 * Resume a closed group
	 */
	public function resumeAction(){ 
		
		$parameters = $this->_getAllParams();
		
		if(!isset($parameters['id']))
			$this->view->error = "That group is active or we couldn't find it";
		else{
			$idgroup = $parameters['id'];	
			$groups = new Groups();
			
			if(!$groups->isAdmin($this->view->currentUser->id, $idgroup)){
				$this->view->error = "Only the group owner can resume an inactive group.";	
			}else{
				$groups->resume($idgroup);	
			}
		}
		 	
	}
	
	
	public function leaveAction(){ 
		
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
		
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Invalid Index";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
		$groups->removeFromGroup($idgroup,$this->view->currentUser->id);	
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 

		if(isset($parameters['layout']))
			$options['layout'] = $parameters['layout'];	
			
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;
		$this->view->group = $group; 
		
	}
	
	
	public function joinAction(){ 
		
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
		
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Invalid Index";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
		
		$invites = new GroupInvites();
		$invites->sendInviteToGroup($idgroup, $this->view->currentUser->id); 
		
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
			
		$options = array(); 
		if(isset($parameters['layout']))
			$options['layout'] = $parameters['layout'];
					
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;
		$this->view->group = $group; 
		
	}
	
	
	public function selfAction()
	{
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		$options = array(); 
		$options['showcomment'] = true;
			
		$this->view->options = $options;
		$this->view->mode = "self";
	}
	
	/*
	 * Group View Page
	 */
	public function viewAction()
	{
		$parameters = $this->_getAllParams();
		
		if(!isset($parameters['id'])){
				$this->_redirect('/mobile/group/index');
		}
			
		$groups = new Groups(); 
		$group = $groups->getGroup($parameters['id']);
		
		$this->view->group = $group; 
	}
	
	/*
	 * Manage your group
	 */
	public function manageAction()
	{
		$parameters = $this->_getAllParams();
		
		$this->view->headScript()->appendFile('/js/contactlist/contacts.js');
		$this->view->headLink()->appendStylesheet('/css/app/overlay.css');
		
		$groups = new Groups();
		
		if(!isset($parameters['id']))
				$this->_redirect('/app/group/index');
		
		$idgroup = $parameters['id'];
		
		if(!$groups->isAdmin($this->view->currentUser->id, $idgroup))		
		 	$this->_redirect('/app/group/index');
		
		$group = $groups->getGroup($idgroup,'all');
		
		
		$options = array(); 
		$options['selector'] = 'true';
				
		$this->view->options = $options; 
				
		$this->view->group = $group; 
		
		$this->view->resultsPerPage=2;
		$this->view->page=1;
	
	}
	
	public function rejectAction()
	{
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
				
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Invalid Index";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
		$groupInvites = new GroupInvites();
		
        if (isset ($parameters['remove']) && ($this->view->currentUser->id == $group->idowner) )
        {
	       $users = explode(",",$parameters['remove']);

	       foreach($users as $user){
	       		$groupInvites->rejectInvite($group->idgroup,$user);
	       }
		}
			
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
					
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;
		$this->view->group = $group;
	}
	
	
	public function acceptAction()
	{
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
		
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Invalid Index";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
		$groupInvites = new GroupInvites();
		
        if (isset ($parameters['remove']) && ($this->view->currentUser->id == $group->idowner) )
        {
	       $users = explode(",",$parameters['remove']);
	       foreach($users as $user){
	       		$groupInvites->acceptInvite($group->idgroup,$user);
	       }
		}
			
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
					
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;
		$this->view->group = $group;
	}
	
	/*
	 * Delete members from a group
	 */
	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		$parameters = $this->_getAllParams();
		
		$groups = new Groups();
		
		if(!isset($parameters['idgroup']))
			return "Invalid Index";
		
		$idgroup = $parameters['idgroup'];	
		$group = $groups->getGroup($idgroup);
		
        if (isset ($parameters['remove']) )//&& ($this->view->currentUser->id == $group->idowner) )
        {
	       $users = explode(",",$parameters['remove']);
	       foreach($users as $user){
	       		$groups->removeFromGroup($idgroup,$user);
	       }
		}
			
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
					
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;
		$this->view->idgroup = $group->idgroup;
		
		
	}
	
	public function listmembersAction()
	{
		$this->_helper->layout->disableLayout();
	
		$parameters = $this->_getAllParams();
		
			if(!isset($parameters['idgroup']))
				return "Invalid Index";
		
		$this->view->idgroup = $parameters['idgroup'];
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
			
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
		if(isset($parameters['showcomment']))
			$options['showcomment'] = $parameters['showcomment'];
				
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	
			
		$this->view->options = $options;

 	}
	
	
	public function listAction(){
		
		// get the list of groups
		$this->_helper->layout->disableLayout();
	
		$parameters = $this->_getAllParams();
		
		$this->view->resultsPerPage=4;
		$this->view->page=1;
		$this->view->mode= "all";
		
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage= $parameters['resultsPerPage'];
				
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		$options = array(); 
		if(isset($parameters['showcomment']))
			$options['showcomment'] = $parameters['showcomment'];
				
		if(isset($parameters['selector']))
			$options['selector'] = $parameters['selector'];	

		if(isset($parameters['layout']))
			$options['layout'] = $parameters['layout'];
			
			
		if(isset($parameters['mode']))
			$this->view->mode= $parameters['mode'];		
			
		$this->view->options = $options;
		
		$this->view->where = "1";
		$this->view->sort = "members desc";
		
			
	}
	
	// Add generic admin access here
	protected function hasAccess($user, $idgroup){
		$groups = new Groups(); 
		
		if($groups->inGroup($user,$idgroup) )//|| $this->view->currentUser->isAdmin)
			return true; 
			
		return false; 
		
	}
	
 
}//end class