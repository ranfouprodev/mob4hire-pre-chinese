<?php
class Mobile_ProjectController extends Zend_Controller_Action
{

		public function init()
		{
			$this->view->headScript()->appendFile('/js/projects/list.js');
			$this->view->translate = Zend_Registry::get('Zend_Translate');
			$this->_helper->layout->setLayout('mobile');
			$this->view->submenu = $this->view->render('partial/project_submenu.phtml');



		}
		
		
		public function indexAction(){
		}
		
		public function allAction(){
		}
		
		public function workAction(){
		}

		public function viewAction()
		{
		
			$parameters = $this->_getAllParams();
					
			if (! isset ($parameters['id']))	{
				$this->view->error=$this->view->translate->_('mobile_project_view_error');
				return false; 
			}
		
			
			
			$idproject = $parameters['id'];
			$this->view->idproject=$idproject;

			$pj = new Projects();
			$this->view->project= $pd = $pj->getProject($idproject);

			$idtester = Zend_Registry::get('defSession')->currentUser->id;
			
			$testProvider = new Tests();
			$testRow = $testProvider->listTestsByTesterAndProject($idproject, $idtester,'','t.Timestamp desc');
			
			$this->view->test = $testRow;
			
			
			if(($pd['Status'] <= 200 || $pd['Status'] > 401) && (Zend_Registry::get('defSession')->currentUser->id !=$pd['iddeveloper']) && (!Zend_Registry::get('defSession')->currentUser->isAdmin))
			{
				$this->view->error=$this->view->translate->_('mobile_project_view_error');
				return false;
			}
	
	
		}
		
		public function instructionsAction()
		{
		
			$parameters = $this->_getAllParams();
					
			if (! isset ($parameters['id']))	{
				$this->view->error=$this->view->translate->_('mobile_project_view_error');
				return false; 
			}
		
			
			
			$idproject = $parameters['id'];
			$this->view->idproject=$idproject;

			$pj = new Projects();
			$this->view->project= $pd = $pj->getProject($idproject);

			$idtester = Zend_Registry::get('defSession')->currentUser->id;
			
			$testProvider = new Tests();
			$testRow = $testProvider->listTestsByTesterAndProject($idproject, $idtester,'','t.Timestamp desc');
			
			$this->view->test = $testRow;
			
			
			if(($pd['Status'] <= 200 || $pd['Status'] > 401) && (Zend_Registry::get('defSession')->currentUser->id !=$pd['iddeveloper']) && (!Zend_Registry::get('defSession')->currentUser->isAdmin))
			{
				$this->view->error=$this->view->translate->_('mobile_project_view_error');
				return false;
			}
	
	
		}
		
		
	public function submitbidAction()
	{
		$parameters = $this->_getAllParams();
		
		$this->view->idproject=$idproject=$parameters['idproject'];
		$projecttable=new Projects();

		$this->view->project= $pd = $projecttable->getProject($idproject);

		$gidtester=$this->view->currentUser->id;
		
		if ($this->getRequest()->isPost() && isset($idproject)) { // form has been submitted
			
			$errors=array();
			$validated=true; // assume validation passed	
			
			$this->view->app=$app=new App();
			$app->load($idproject);
		
			// Check to see if the user has a mobster profile completed. 
			$profileTable = new ProfileMobster();
			if(!$profileTable->hasProfile($this->view->currentUser->id)){
				array_push($errors,4);
				$validated=false;	
			}
	
			// This only applies if the project requires specific handsets (I think now all projects will require a handset. If any was selected, then they selected it from the list of their own handsets)
			if(isset($_POST['supportedhandsets']))
				$iddevice=$_POST['supportedhandsets'];
			else{
				array_push($errors,1);
				$validated=false;
			}
	
			if(isset($_POST['supportednetworks'])){
				$idnetwork=$_POST['supportednetworks'];
					
			}
			else{
					array_push($errors,3);
					$validated=false;
			}
			if (!$app->flatfee) {
			
				$bidamount=(int) $_POST['bidAmount'];
				if ($bidamount < 1) {
					array_push($errors,2);
					$validated=false;
				}
			}
			else {
				$bidamount=$app->budget;
			}
			$comments=mysql_real_escape_string($_POST['comments']);
	
			$this->view->errors=$errors;
	
			if($validated){	
				
				$exe='';
				$tests=new Tests();
				if(($this->view->errorcode=$tests->submitBid($idproject, $gidtester, $bidamount, $idnetwork, $iddevice, $comments))==1) {						
					$this->view->bidSubmitted=true;
				}
				else {
					//something went wrong
					$this->view->errors=array('5');
					$validated=false;
				}
			}

		}elseif (isset($idproject) && !isset($this->view->bidSubmitted)) {
			// This is either the first time through or we failed validation	
		
			$this->view->app=$app=new App();
		
			if($this->view->canApply=$projecttable->canApplyForProject($this->view->currentUser->id, $this->view->idproject)) {

				if ($app->load($idproject)) { // all this stuff can be got from getProject. We need to get rid of this App object
					$compatibledevices=array();
					$compatiblenetworks=array();
					// get all compatible devices and add them to this array for selection in form
					$regdevices=new Regdevices();
					$devices=$regdevices->getHandsetsByUser($this->view->currentUser->id);  // get handsets
					foreach($devices as $device)
					{
						if($projecttable->iscompatibleDevice($device, $idproject)) {
							if (!in_array($device['vendor']." ".$device['model'], $compatibledevices)) {
								$compatibledevices[$device['id']]=$device['vendor']." ".$device['model'];
							}
							if (!in_array($device['country']." ".$device['network'], $compatiblenetworks)) {	
								$compatiblenetworks[$device['idnetwork']]=$device['country']." ".$device['network'];
							}
						}
					}
					$this->view->compatibledevices=$compatibledevices;
					$this->view->compatiblenetworks=$compatiblenetworks;
				}
			}
		}
	}


}
