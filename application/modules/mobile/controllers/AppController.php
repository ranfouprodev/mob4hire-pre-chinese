<?php

class Mobile_AppController extends Zend_Controller_Action 
{

	public function init()
	{
		$this->session = new Zend_Session_Namespace('User');
		$this->view->translate = Zend_Registry::get('Zend_Translate');
	}

	public function indexAction()
	{

		// Automatically add the device into registered devices

		$userId = Zend_Registry::get('defSession')->currentUser->id;
		$mobile = Zend_Registry::get('defSession')->mobile;

        if(!empty($mobile) && !(isset($mobile['added']))){
			$regdevices= new Regdevices();
			$deviceprops=$regdevices->addDevice($mobile,$userId,$_SERVER['HTTP_USER_AGENT'],  $ip=$this->getIP());
			if($deviceprops && (isset($deviceprops['added']))){
				$this->view->deviceAdded = true;
			}
			Zend_Registry::get('defSession')->mobile['added']=true;
		}

		// Dashboard equivalent
		//Append the scripts for the wall
		$this->view->lastLogin=$lastLogin=strtotime(Zend_Registry::get('defSession')->currentUser->last_login);

		// Just for checking if there is projects available
		$projecttable = new Projects();
		$this->view->hasProjects=$projecttable->getProjectsByDeveloper($this->view->currentUser->id, 399,1,1)->count();	
		
		$teststable= new Tests();
		$this->view->hasTests=$teststable->listTestsByTester($this->view->currentUser->id,'idproject desc',1,1)->count(); 
	}

	public function downloadAppAction()
	{
		$this->view->idtest=$this->_getParam('idtest', 0);
		
		$tests=new Tests();
		$this->view->test=$test=$tests->getTest($this->view->idtest);

		if($this->view->test->idtester==$this->view->currentUser->id && $this->view->test->Status==250) {
			$projectfiles=new ProjectFiles();
			$this->view->files=$projectfiles->getBinaryByDevice($test->idproject, $test->iddevice);	
		}
		else {
			// TODO Need to make sure they are active on the project first
			$this->_redirect('mobile/app'); // done I think
		}
		
	}
	
	public function showprojectAction()
	{
	
	}
	public function termsAction()
	{
    
	}
    
	public function partnersAction()
	{
    
	}
    
	public function privacyAction()
	{
    
	}

	private function getIP()
	{
		$headers = apache_request_headers();
		if (array_key_exists('X-Forwarded-For', $headers)){
			$hostname=$headers['X-Forwarded-For'] ;
		} else {
			$hostname=$_SERVER["REMOTE_ADDR"];
		}
		return $hostname;
	}
}
