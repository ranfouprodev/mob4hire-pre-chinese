<?php

class Mobile_WallController extends Zend_Controller_Action
{
		
	
	public function init()
		{
			$this->view->headScript()->appendFile('/js/wall/wall.js');
			
			$this->_helper->layout->setLayout('mobile');

			$this->view->translate = Zend_Registry::get('Zend_Translate');

		}
		
	
	/*
	 * Renders a single message view. Useful for webview rendering on devices
	 * id is the thread id
	 * 
	 */
	public function viewAction()
	{
	
		$parameters = $this->_getAllParams();
		if (isset($parameters['id']))
		{
			$this->view->idtarget=$idtarget=$parameters['id'];
			$this->view->postType=$postType=5;
			$options = array(); 
			$options['layout'] = 'compact';
			$this->view->options = $options;
						
		}else{
			$this->view->error="That message thread is invalid or been removed.";
		}
		
		
	}//end indexAction
	
	
	/*
	 * Gets the unified wall
	 */
	public function indexAction()
	{
		$parameters = $this->_getAllParams();
		
		
		$this->view->resultsPerPage=$resultsPerPage=10;
		$this->view->page=$page=1;
		$this->view->timestamp=$timestamp=0;
		
		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage=$resultsPerPage = $parameters['resultsPerPage'];
		
		if(isset($parameters['page']))
			$this->view->page=$page = $parameters['page'];
			
		if(isset($parameters['timestamp']))
			$this->view->timestamp=$timestamp= $parameters['timestamp'];
		
		$options = array(); 
		$options['layout'] = 'compact';
		$this->view->options = $options;
			
		$this->view->idtarget=$this->view->currentUser->id;;
		$this->view->postType=4;
  	    $this->view->showcomment = false; 	
		
	}
	
	


 
 
}//end class