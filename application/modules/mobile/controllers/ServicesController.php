<?php

class Mobile_ServicesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate');
		$this->_helper->layout->setLayout('mobile_services');	
	}
	
	public function indexAction()
	{	
		$parameters = $this->_getAllParams();
		
		$this->view->deflt = isset($_GET['product']) ?$_GET['product']:"Experience";
		
		$this->view->form= $form = new Form_MobileContact();

		$this->view->done=false;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$mailer = new Mailer(); 
				$mailer->sendGetQuoteEmails($formData);	
				$this->view->done=true;
			}
		}
	}
	
	public function whatAction(){ 
		
	}
	
	public function whoAction(){ 
		
	}
	
	

}//end class