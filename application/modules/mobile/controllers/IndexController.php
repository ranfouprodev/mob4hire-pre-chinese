<?php

class Mobile_IndexController extends Zend_Controller_Action 
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }

	
	// Login Page is now here
    public function indexAction()
    {
		$this->_helper->layout->setLayout('mobile');
    	
    	
    	if ($this->_helper->authUsers->isLoggedIn())
		{
			$this->_redirect($this->view->url(array(
                              'module'=>'mobile',
                              'controller'=>'app',
                              'action'=>'index'
                              ))
                   , array('exit'=>true));
		}
 
		$this->view->loginForm = new Form_MobileLogin(); 
		
		
	}
    
    public function termsAction()
    {
    
    }
    
}