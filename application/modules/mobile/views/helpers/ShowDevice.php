<?php

class Zend_View_Helper_ShowDevice
{
	function showDevice($currentUser) 
	{
		if(isset($currentUser->device) && isset ($currentUser->device['vendor']) && isset($currentUser->device['model']))
			return $currentUser->device['vendor'].'&nbsp;'.$currentUser->device['model'];
		else
			return "Unknown Device";
	}
}