<?php

class Zend_View_Helper_FormatDate
{
	function formatDate($mysqldate, $offset=0) 
	{
		$timestamp=strtotime("+ $offset days", strtotime($mysqldate));
		$formatteddate=date("F j, Y", $timestamp);
		return $formatteddate;
	}
}