<?php
// This should eventually be moved into the model!

class Zend_View_Helper_GetProjectImage
{
	function getProjectImage($project) 
	{
		$projectFiles = new ProjectFiles();
		$currentImages = $projectFiles->getFilesForProjectByCategory($project->idproject,"projImg");
		
				
		if(!isset($currentImages) || sizeof($currentImages)==0){
			 return '<img src="/images/icons/no_project_image.gif" height="100" width="100" alt="' . $project->Name . '" />';			
		}
		else
		{
			$currentImage = $currentImages[0];
			 return '<img src="/content/image/'.$currentImage['idfiles'].'" height="100" width="100" alt="' . $project->Name . '" />';			
		}
		
		
		
	}
}