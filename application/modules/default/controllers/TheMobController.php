<?php

class TheMobController extends Zend_Controller_Action 
{

	public function init()
	{
		$this->session = new Zend_Session_Namespace('User');
		$this->view->translate = Zend_Registry::get('Zend_Translate');
	}


	public function indexAction()
	{
		$this->_helper->layout->setLayout('main');      // Override the default behaviour since this page does not have the left menu

	}
	
	public function whatisthemobAction()
	{
 		$this->_helper->layout()->getView()->headTitle(' | Mobile Testers and Apps');        
       
	}
	
	public function projectsAction()
	{
 		$this->_helper->layout()->getView()->headTitle(' | Projects');        
		$this->view->headScript()->appendFile('/js/projects/list.js');
		$this->view->headScript()->appendScript('var resultsPerPage = 5;var showComment = false;var mode = "all";var navigation = "none";');
		
		$this->_helper->layout->setLayout('the-mob_wide');
	}

	// Public project view
	public function viewAction()
	{	
		$this->view->headLink()->appendStylesheet('/css/forms.css');
		$this->_helper->layout->setLayout('the-mob_wide');
		$parameters = $this->_getAllParams();//get all passed in params
        
		//looking for parameter called idproject as in eg: ?idproject=1
		//if no params passed in then exit page...
		if (!isset($parameters['idproject']))
		{
			$this->_redirect('/', array('exit'=>true));
		}
        
		//ok we have a param called idproject
		$projectId = $parameters['idproject']; //get the value and store it in $projectId variable
        
		// If logged in , redirect to logged in view
		if(Zend_Registry::get('defSession')->currentUser){
			$this->_redirect('/app/project/view/idproject/'.$projectId, array('exit'=>true));
		}
	
		$projectTable = new Projects(); //create new instance of Projects table
        
		$this->view->proj = $project = $projectTable->getProject($projectId);//pass this row back to the view based on parameter passed in as idproject
		
		if($project['Status'] <= 200 || $project['Status'] > 401)
		{
			$this->_redirect('/the-mob/projects', array('exit'=>true));		
		}
		
		if(!isset($project))
		{
			$this->_redirect('/', array('exit'=>true));
		}
	}
	
	public function handsetsAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Handsets');  
	    /*
	     * Platform doesn't seem to be working. I'll remove it for now. 
	     * 
	     */
		$this->view->headScript()->appendFile('/js/jquery.js');
		$this->view->headScript()->appendFile('/js/handsets/list.js');
		$this->view->headScript()->appendFile('/js/networks/list.js'); // needed for Dynamic menus to work
		$this->view->headScript()->appendFile('/js/devices/list.js');
		$this->view->headLink()->appendStylesheet('/css/rating.css');
		$this->_helper->layout->setLayout('the-mob_wide');
		$this->view->form=new Form_HandsetFilter();
		$this->view->form->populate($this->_request->getPost());
		
		// If no filters set return all devices
		$countryFilter=$this->getRequest()->getParam('country');
		$networkFilter=$this->getRequest()->getParam('network');
		//$platformFilter=$this->getRequest()->getParam('platform');
		$vendorFilter=$this->getRequest()->getParam('vendor');
		$modelFilter=$this->getRequest()->getParam('model');
		
		if(!$countryFilter && !$networkFilter   && !$vendorFilter && !$modelFilter ){ //&& !$platformFilter) {
			$this->view->whereClause='n.idcountry>0 AND regdevices.idnetwork>0 AND d.vendor>0 AND d.id>0';		
		}
		else {
			$firstQuerySet=false;			
			if($countryFilter){
				$countryFilterString = "n.idcountry='$countryFilter' ";	
				$firstQuerySet=true;
			}
			else {
				$countryFilterString = "";
			}
			$networkFilterString = "";			
			if($networkFilter){
				if ($firstQuerySet) {
					$networkFilterString.="AND ";
				} 
			
				$networkFilterString .="regdevices.idnetwork='$networkFilter' ";
				$firstQuerySet=true;
			}

			/*$platformFilterString = "";
			
			if($platformFilter){
				if ($firstQuerySet) {
					$platformFilterString.="AND ";
				}
				$platformFilterString .= "$platformFilter ";
				$firstQuerySet=true;
			}*/

			$vendorFilterString = "";
			
			if($vendorFilter){
				if ($firstQuerySet) {
					$vendorFilterString.="AND ";
				}
				$vendorFilterString .= "d.vendor='$vendorFilter' ";
				$firstQuerySet=true;
			}

			$modelFilterString="";			
			if($modelFilter){
				if ($firstQuerySet) {
					$modelFilterString.="AND " ;
				}
				$modelFilterString .= "d.id='$modelFilter' ";
			}
		
			$this->view->whereClause=$countryFilterString.$networkFilterString.$vendorFilterString.$modelFilterString;//.$platformFilterString
		}
	}
	
	public function surveysAction()
	{
	
	}
	
	public function testersAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Mobile Testers'); 
	}
	
	public function testersGettingStartedAction()
	{
	
	}
	
	public function testersFaqAction()
	{
	
	}
	
	public function developersAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Developer Developers');  
		$projects = new Projects();
		$this->view->projectList = $projects->listProjects('projects.Status > 199 and projects.Status < 499', 'StartDate desc', 1, 4);
        
		$users = new Users(); 
		$this->view->testerCount = $users->count();  
		
		$profileContact = new ProfileContactInfo(); 
		$this->view->userCountryCount = $profileContact->countCountries(); 
		
		/*
		 * Add this point we are adding a correction for litmus users and handsets.
		 * 	[8:09:43 AM] Paul Poutanen: take the static number of litnus testers and add to our count please
		 *	[8:10:49 AM] Paul Poutanen: and handsets too
		 * 
		 * So when someone does a db count and it doesn't match up to the one on the front page this is why. 
		 *  
		 *  
		 */
		$this->view->proCount = 0; // not being used
        
		$profiles = new ProfileDeveloper();
		$this->view->developerCount = $profiles->count();
		$this->view->developerCountryCount = $profiles->countCountries();

		$regdevices = new Regdevices();
		$this->view->carrierCount = $regdevices->carrierCount();
		$this->view->countryCount = $regdevices->countryCount();
	}
	
	public function developersGettingStartedAction()
	{
	
	}
	
	public function developersFaqAction()
	{
	
	}
	
	public function developerNetworksAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Developer Networks');  
	}
	
	public function marketResearchersAction()
	{
		$this->_helper->layout()->getView()->headTitle(' | Global Researchers');  
	}
	
	public function listprojectsajaxAction()
	{
		
		$this->_helper->layout->disableLayout();
			
		$projects=new Projects();
		
		$parameters = $this->_getAllParams();
		
		if(isset(Zend_Registry::get('defSession')->currentUser->id))
			$iduser=Zend_Registry::get('defSession')->currentUser->id;
		else
			$iduser=0;

		$this->view->iduser = $iduser;
			
	
			
		// set the display options
		$options = array(); 
		if(isset($parameters['showcomment'])){
			$showcomment = strtolower($parameters['showcomment']);
			if($showcomment == "true")
		 		$options['showcomment'] = true;
			else 
				$options['showcomment'] = false;
		}
		
		
		if(isset($parameters['navigation'])){
			$options['navigation'] = $parameters['navigation'];	
		}
		
		if(isset($parameters['search']))
			$options['search'] = $parameters['search'];
		
		if(isset($parameters['layout']))
			$options['layout'] = $parameters['layout'];	
			
		
		$this->view->options = $options; 
		
		
		$this->view->orderBy = $orderBy= urldecode($this->getRequest()->getParam('orderBy'));
		
	
		$this->view->mode=$mode=$this->getRequest()->getParam('mode');

		
		$this->view->resultsPerPage=$resultsPerPage=4;
		$this->view->pageNumber=$pageNumber=1;
		

		if(isset($parameters['resultsPerPage']))
			$this->view->resultsPerPage=$resultsPerPage = $parameters['resultsPerPage'];
				
		if(isset($parameters['pageNumber']))
				$this->view->pageNumber=$page = $parameters['pageNumber'];
		
	}

	public function listhandsetsajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$whereClause=$this->getRequest()->getParam('whereClause');
		$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly
		$regdevices=new Regdevices();
		$this->view->numberOfPages=((int)(($this->view->totalHandsets=$regdevices->distinctDevicesCount($whereClause))/$rowsPerPage))+1; //  Get total rows and calculate number of pages
		$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	}
	
	public function ajaxnetworkmenuAction()
	{
		$this->_helper->layout->disableLayout();
	
	}
	
	public function ajaxdevicemenuAction()
	{
		$this->_helper->layout->disableLayout();
	
	}

}