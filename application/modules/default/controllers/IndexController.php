<?php

// include_once 'facebook-platform/php/facebook.php';

class IndexController extends Zend_Controller_Action
{
    /**
     * Session namespace object for user data
     *
     * @var Zend_Session_Namespace
     */
    protected $session = NULL;

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
//	$this->fb = new Facebook('3417a1664e1f729761c8b6014fc7f61f', '1edbf13ee84f91669fcbdf41ba653d67');
    }

    public function indexAction()
    {

        if ($this->_helper->authUsers->isLoggedIn()) {
            $this->_redirect('/app/', array('exit' => true));
        }

        $this->_forward('login');
    }


    public function loginAction()
    {

        if ($this->_helper->authUsers->isLoggedIn()) {
            $this->_redirect('/app/', array('exit' => true));
        }

        $this->view->loginForm = new Form_Login;

        if ($this->getRequest()->isPost()) {
            if ($this->view->loginForm->isValid($_POST)) {
                $username = $this->view->loginForm->getValue('username');
                //todo: If username input contains a @ then strip the rest of the string. Change usernames in database (hopefully we will have no dupes)
                $password = $this->view->loginForm->getValue('password');
                if ($username && $password) {
                    $auth = Zend_Auth::getInstance();

                    $db = Zend_Registry::get('db');
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
                    $authAdapter->setIdentity($username)->setCredential(Users::computePasswordHash($password));
                    $authResult = $auth->authenticate($authAdapter);
                    if ($authResult->isValid()) {
                        //valid username and password
                        $userInfo = $authAdapter->getResultRowObject();
                        if ($userInfo->active) {
                            //save userinfo in session
                            $userInfo->password = '';
                            Zend_Registry::get('defSession')->currentUser = $userInfo;
//						Zend_Session::rememberMe(2419200); // 28 days
                            // Write last_login info to db
                            $users = new Users();
                            $now = new Zend_Date();

                            $userData = array('last_login' => $now->toString('YYYY-MM-dd HH:mm:ss'));
                            $users->editProfile($userInfo->id, $userData);
                            // if we requested a page but failed auth redirect there
                            if (isset($_POST['return'])) {
                                $return = $_POST['return'];
                                $this->_redirect($return, array('exit' => true));
                            } else {
                                //redirect to home
                                $this->_redirect('/app/', array('exit' => true));
                            }
                        } else {
                            $this->view->notActivated = true;
                            $auth->clearIdentity();
                        }
                    } else {
                        $this->view->loginError = true;
                    }
                } else {
                    $this->view->loginError = true;
                }
            }
        }
    }

    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        Zend_Registry::get('defSession')->currentUser = null;

        /*
                try {
                    $this->fb->expire_session();
                } catch(Exception $e) {
                    $this->fb->set_user(null, null);
                    $this->fb->clear_cookie_state();
                }
        */
        $this->_redirect($this->_helper->url('index', 'index'), array('exit' => true));

    }


    public function activateAction()
    {
        $parameters = $this->_getAllParams();
        if (isset ($parameters['userId']) && isset ($parameters['activationCode'])) {

            // activate the user if the activation code is valid.
            $usersTable = new Users();
            $rows = $usersTable->find($parameters['userId']);
            $user = $rows->current();
            if ($user) {
                if (!$user->active && $user->code == $parameters['activationCode']) {
                    // activate the user
                    $user->active = 1;
                    $user->save();
                    $this->view->activatedOK = true;


                    $this->view->loginForm = new Form_Login;

                } else if ($user->active) {
                    $this->view->userAlreadyActive = true;
                }
            }


            if ($this->view->mobile) {
                $params = array('userId' => $parameters['userId'], 'activationCode' => $parameters['activationCode']);
                $this->_helper->redirector('activate', 'user', 'mobile', $params);
            }
        } else if (isset ($this->session->userJustRegistered)) {
            $this->view->username = $this->session->userJustRegistered;

            // use 'user/justRegistered.phtml' template.
            $this->renderScript('index/justRegistered.phtml');
        }
    }

    public function profileAction()
    {

    }


    public function registerAction()
    {

        $this->view->headLink()->appendStylesheet('/css/forms.css');
        $usersTable = new Users();

        // Display the form if the facebook user isn't avail.
        $this->view->registerForm = new Form_Register();

        if ($this->getRequest()->isPost()) {
            if ($this->view->registerForm->isValid($_POST)) {
                $values = $this->view->registerForm->getValues();
                $badRegistration = false;
                try {
                    $newUserId = $usersTable->add($values['username'], $values['password'], $values['email']);
                } catch (Zend_Db_Statement_Exception $e) {
                    // race condition can occur between validator and actual DB operation
                    // so check if username or email is already registered
                    // NOTE: this should be very rare case, because we check this in validators just before inserting the row
                    $message = $e->getMessage();

                    if (strpos($message, $values['email']) !== false) {
                        //email already registered
                        $badRegistration = true;
                        $this->view->globalPageError = 'email';
                    } else if (strpos($message, $values['username']) !== false) {
                        //username already registered
                        $badRegistration = true;
                        $this->view->globalPageError = 'username';
                    } else {
                        // other error, rethrow the exception
                        throw $e;
                    }

                }


                if (!$badRegistration) {
                    $profileFields = array('first_name' => $values['firstname'], 'last_name' => $values['lastname'],);

                    $usersTable->editProfile($newUserId, $profileFields);


                    // delete captcha code from session:
                    $this->session->registerCaptcha = NULL;


                    /*
                     * after successfull submit of the form, we want to prevent double submit with the
                     * same data, because this will lead to error - trying to register again the same
                     * username, so we set in our session 'success flag' and redirect to activate action
                     */

                    $this->session->userJustRegistered = $values['username'];
                    $this->_redirect($this->_helper->url('activate', 'index'), array('exit' => true));
                }
            }
        }
        $this->view->headScript()->appendFile('/js/jquery.js');
        $this->view->headScript()->appendFile('/js/register/form.js');
    }


    public function forgotpasswordAction()
    {


        $this->view->headLink()->appendStylesheet('/css/forms.css');
        $parameters = $this->_getAllParams();

        if ($this->view->mobile) {
            $params = array('cc' => $parameters['cc'], 'exit' => true);
            $this->_helper->redirector('forgotpassword', 'user', 'mobile', $params);

        }


        $users = new Users();
        // Check if the token is in the parameter
        if (isset ($parameters['cc'])) {

            $code = $parameters['cc'];
            // find the user with that password
            $user = $users->getUserByCode($code);

            if (!$user)
                $this->view->tokenError = true;
            else {

                $date = new Zend_Date();
                $last = new Zend_Date(strtotime($user['last_login']));

                $age = ($date->sub($last)) / 60;

                if ($age < (60 * 12)) {

                    // Log the user in and forward them to the change password page
                    $auth = Zend_Auth::getInstance();

                    $db = Zend_Registry::get('db');
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'username');
                    $authAdapter->setIdentity($user['username'])->setCredential($user['username']);
                    $authResult = $auth->authenticate($authAdapter);

                    Zend_Registry::get('defSession')->currentUser = $user;
                    Zend_Registry::get('defSession')->resetPass = true;

                    $this->_redirect('/app/profile/changepassword', array('exit' => true));

                } else {
                    $form = new Form_RecoverPassword();

                    $this->view->form = $form;
                    $this->view->tokenExpired = true;
                }


            }


        } else {


            $form = new Form_RecoverPassword();

            $this->view->form = $form;

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                if ($form->isValid($formData)) {
                    $email = $formData['email'];

                    // Find the user related to that account

                    $user = $users->getUserByEmail($email);

                    if (!$user)
                        $this->view->emailError = true;
                    else {

                        // Generate the token and add it to the activation field in the user db
                        // We are reusing that field so I don't have to update the schema.
                        $activationCode = sha1(uniqid('xyz', true));

                        // update the account
                        $now = new Zend_Date();

                        $userData = array('code' => $activationCode, 'last_login' => $now->toString('YYYY-MM-dd HH:mm:ss'));
                        $users->editProfile($user['id'], $userData);

                        // Mail out the password
                        $mailer = new Mailer();
                        $mailer->sendForgotPassword($user, $activationCode);

                        $this->view->emailSent = true;
                    }


                }


            }
        }
    }


    // Note that these actions are also called in edit profile
    public function checkusernameajaxAction()
    {
        $this->_helper->layout->disableLayout();

        $username = $this->getRequest()->getParam('username');

        $usersTable = new Users();
        $select = $usersTable->select();
        $select->where('username = ?', $username);
        $rows = $usersTable->fetchAll($select);
        if ($rows->count()) {
            if ((!Zend_Registry::get('defSession')->currentUser) || ($rows[0]->id != Zend_Registry::get('defSession')->currentUser->id))
                $valid = false;
            else
                $valid = true; // do not fail validation if tis is our own  username and we have not changed it
        } else
            $valid = true;

        $this->_helper->viewRenderer->setNoRender();

        $data = array('valid' => $valid);
        $json = Zend_Json::encode($data);
        echo $json;
    }

    public function checkemailajaxAction()
    {
        $this->_helper->layout->disableLayout();

        $email = $this->getRequest()->getParam('email');
        if ($email) {
            $usersTable = new Users();
            $select = $usersTable->select();
            $select->where('email = ?', $email);
            $rows = $usersTable->fetchAll($select);
            if ($rows->count()) {
                if ((!Zend_Registry::get('defSession')->currentUser) || ($rows[0]->id != Zend_Registry::get('defSession')->currentUser->id))
                    $valid = false;
                else
                    $valid = true; // do not fail validation if tis is our own email address and we have not changed it
            } else
                $valid = true;
        } else $valid = true;

        $this->_helper->viewRenderer->setNoRender();

        $data = array('valid' => $valid);
        $json = Zend_Json::encode($data);
        echo $json;
    }


    public function termsAction()
    {

    }

    public function privacyAction()
    {

    }

    public function langAction(){

        $session = Zend_Registry::get('defSession');
        $language = $this->getRequest()->getParam('lang');

        $session->language = $language;

        $url = $_SERVER['REQUEST_URI'];
    
        $this->_redirect($url, array('prependBase' => false));
    }

}




