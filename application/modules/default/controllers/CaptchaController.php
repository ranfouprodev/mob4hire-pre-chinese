<?php

class CaptchaController extends Zend_Controller_Action 
{
    public function init()
    {   
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();            
    }
    
    public function getAction()
    {
        $parameters = $this->_getAllParams();
               
        $width = 150;
        $height = 40; 
        
        $image = ImageCreate($width, $height);
        
        if (isset($parameters['namespace']) && isset($parameters['captchaId']))
        {
            $session = new Zend_Session_Namespace($parameters['namespace']);
            $text = $session->{$parameters['captchaId']};
            
            if ($text)
            {
                $grey = ImageColorAllocate($image, 190, 190, 190);
                $black = ImageColorAllocate($image, 0, 0, 0);
                ImageString($image, 20, 50, 15, $text, $black);
            }            
        }
        
        $this->_response->setHeader('Content-Type', 'image/gif');        
        imagegif($image);        
    }
}