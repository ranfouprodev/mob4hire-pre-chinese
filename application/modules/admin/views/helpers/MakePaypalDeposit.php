<?php

class Zend_View_Helper_MakePayPalDeposit
{

	function makePaypalDeposit($amount, $description, $currency) 
	{
		// encode amount and description string to prevent fraud!
		$stringtoencode="".$amount.":".$description.":".$currency;
		$salt = substr($stringtoencode, 0, 2);
		$encodedcheck = crypt($stringtoencode, $salt);
		return(
				'<form action="https://www.paypal.com/cgi-bin/webscr" method="post">'.
				'<input type="hidden" name="cmd" value="_xclick">'.
				'<input type="hidden" name="business" value="paypal@mob4hire.com">'.
				'<input type="hidden" name="item_name" value="'.$description.'">'.
				'<input type= "hidden" name="amount" value="'. $amount.'">'.
				'<input type="hidden" name="shipping" value="0.00">'.
				'<input type="hidden" name="no_shipping" value="1">'.
				'<input type="hidden" name="no_note" value="1">'.
				'<input type="hidden" name="currency_code" value="'.$currency.'">'.
				'<input type="hidden" name="tax" value="0.00">'.
				'<input type="hidden" name="bn" value="PP-BuyNowBF">'.
				'<input type="hidden" name="return" value="http://v3.mob4hire.com/app/index/ppreturn">'.
				'<input type="hidden" name="cancel_return" value="http://v3.mob4hire.com/app/index/ppcancel">'.
				'<input type="hidden" name="notify_url" value="http://v3.mob4hire.com/paypal">'.
				'<input type="hidden" name="custom" value="'.$encodedcheck.'">'.
				'<div class="submit_buttons clearfix">'.
				'<div class="button">'.
				'<input type="submit" name="submit" id="pay-now" value="Pay Now"></div>'.
				'<div class="button">'.
				'<input type="submit" name="cancel" id="cancel" value="cancel"></div>'.
				'</div>'.
				'</form>'
			);
		}
}	