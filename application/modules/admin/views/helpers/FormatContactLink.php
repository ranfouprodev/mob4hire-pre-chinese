<?php

class Zend_View_Helper_FormatContactLink
{

	function formatContactLink($id) 
	{ 
		// Check if the contact is connected 
		$userid =  Zend_Registry::get('defSession')->currentUser->id;
		
		$contact = new Contacts();
		$hasContact = $contact->hasContact($userid, $id);
		
		if($hasContact)
		{
			$link = '<div id="divAddContactsLink"><a onclick="removeContact('.$id.')">Remove from Contacts</a></div>';
		}
		else
		{
			$link = '<div id="divAddContactsLink"><a onclick="addContact('.$id.')">Add to Contacts</a></div>';			
		}
		
		return $link;
	}
}