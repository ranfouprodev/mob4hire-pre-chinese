<?php

class Zend_View_Helper_CanDelete
{
	function canDelete($commentData) 
	{
				
		if($commentData['iduser'] == Zend_Registry::get('defSession')->currentUser->id){
				$canDelete = true;			
			}elseif(substr($commentData['category'], 0, 3)=="pro" ){
				// project owner can delete the comment
				$idproject = substr($commentData['category'],3);
				$project = new Projects(); 
				$projectData = $project->getProject($idproject);
				
				($projectData['iddeveloper'] == Zend_Registry::get('defSession')->currentUser->id )?$canDelete=true:$canDelete=false;			
			}elseif(Zend_Registry::get('defSession')->currentUser->isAdmin){
				$canDelete=true;
			}else{
				$canDelete=false;
			}
			
			
		return $canDelete;
			
		
	}
}