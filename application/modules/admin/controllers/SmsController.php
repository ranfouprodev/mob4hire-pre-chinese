<?php

require_once 'oauthsimple/OAuthSimple.php';

class Admin_SmsController extends Zend_Controller_Action
{
	
	public function init()
	{
		$this->dbsms = Zend_Registry::get('dbsms');
	}
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 
 		 $data = $this->dbsms->fetchAll('select * from coverage_last_24 order by country asc');
 		 
 		 $this->view->headLink()->appendStylesheet("/css/dynamictable.css");
 		 $this->view->latestCoverageTable= $data;
	}
	
	
	public function mapAction(){
		
		$this->_helper->layout->setLayout('admin_map');
		
		$data = $this->dbsms->fetchAll('select *,sum(testers) as depth from coverage_last_24 group by country order by country asc ');
		
		$scriptCode = "google.load('visualization', '1', {'packages': ['geochart']});
				google.setOnLoadCallback(drawRegionsMap);
				function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
          ['ISO', 'Depth']";
		
		foreach($data as $coverage){
			if($coverage['country'] != ''){
				$scriptCode .= ",['".$coverage['country']."',".$coverage['depth']."]";
			}
		}
		
		$scriptCode .= "]);
				var options = {backgroundColor: {stroke:'#666',strokeWidth:2}};
				var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
        chart.draw(data, options);}";
		
		
		$this->view->headScript()->appendScript($scriptCode);
		
	}
	
	public function clientAction(){
		
		$this->_helper->layout->setLayout('admin');
		
		$form=new Form_SmsClientForm();
		
		$this->view->form=$form;
		
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
	
		    $email = $formData['email'];
			
			$post_data['e'] = urlencode($email);
			$post_data['p'] = 'none';
						
			//traverse array and prepare data for posting (key1=value1)
			foreach ( $post_data as $key => $value) {
			    $post_items[] = $key . '=' . $value;
			}
 
			//create the final string to be posted using implode()
			$post_string = implode ('&', $post_items);
						
			$response = $this->doOauthRequestReport( "http://api.mob4hire.com/v1/admin/client/",$post_string, 'POST');
			
						
			$data = $this->dbsms->fetchRow("select * from user_keys where email like '%".$email."%' ");
			if(count($data) != 0){
				
				$name = $data['name'];
				$key = $data['consumer_key'];
				$secret = $data['consumer_secret'];
				
				$id = $data['iduser'];
				
				$smscode = base_convert(740000+$id,10,36);
				
				$mailer=new Mailer();
				$mailer->sendNewAPIClientEmail($email, $name, $key, $secret, $smscode);
				$this->view->results = 'Sent confirmation to '.$email.' code:'.$smscode;
				
			}else
			{
				$this->view->results = 'No message sent. No keys found';
			}
			 
			
		}
		
	}
	
	
	public function reportAction()
	{
	
		$this->_helper->layout->setLayout('admin');
		
		$parameters = $this->_getAllParams();
	
		$this->view->reportName= $parameters['name'];
	
		$report = $this->doOauthRequestReport( "http://api.mob4hire.com/v1/admin/report/".$parameters['name'].".xls" );
	
		$this->view->report = $report;
		
	
	}
	
		
	public function reportpdfAction()
	{
	
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->viewRenderer->setNoController();
		$this->_helper->layout()->disableLayout();
	
		// $this->_helper->layout->setLayout('admin');
		$response = $this->_response;
	
	
		$parameters = $this->_getAllParams();
	
		$this->view->reportName= $parameters['name'];
	
		$report = $this->doOauthRequestReport( "http://api.mob4hire.com/v1/admin/report/".$parameters['name'].".pdf" );
	
	
		$response->setHeader('Content-type', "application/pdf");
		$response->setBody($report);
	
	
	}
	
	
	public function reportxlsAction()
	{
		
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->viewRenderer->setNoController();
		$this->_helper->layout()->disableLayout();
		
		// $this->_helper->layout->setLayout('admin');
		$response = $this->_response;
		
		
		$parameters = $this->_getAllParams();
		
		$this->view->reportName= $parameters['name'];
		
		$report = $this->doOauthRequestReport( "http://api.mob4hire.com/v1/admin/report/".$parameters['name'].".xls" );
		
		
		$response->setHeader('Content-type', "application/vnd.ms-excel; charset=utf-8");
	    $response->setBody($report);
		
		
	}
	
	
	function doOauthRequestReport( $url, $params = array(), $action = 'GET' ) {
		$key = '9su9ch43ksq1pctfpv2muk35ih'; // this is your consumer key
		$secret = 'djl57hkf79g3t4l4kjtn1191n5'; // this is your secret key
	
		$oauth = new OAuthSimple($key,$secret);
	
		$oauth->setPath($url);
		$oauth->setParameters($params);
		$oauth->setAction($action);
		$result = $oauth->sign();
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $result['signed_url']);
		if( $action == 'POST' )
		{
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		$r = curl_exec($ch);
		curl_close($ch);
		return  $r ;
	}
	
	
	function doOauthRequest( $url, $params = array(), $action = 'GET' ) {
		$key = '9su9ch43ksq1pctfpv2muk35ih'; // this is your consumer key
		$secret = 'djl57hkf79g3t4l4kjtn1191n5'; // this is your secret key
	
		$oauth = new OAuthSimple($key,$secret);
	
		$oauth->setPath($url);
		$oauth->setParameters($params);
		$oauth->setAction($action);
		$result = $oauth->sign();
	
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Accept: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $result['signed_url']);
		if( $action == 'POST' )
		{
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		$r = curl_exec($ch);
		curl_close($ch);
		return json_decode( $r );
	}
	
}