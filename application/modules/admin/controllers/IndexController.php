<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace('User');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
    }

	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 
 		 $reg = new Regdevices();
 		 $developer = new ProfileDeveloper();
 		 $users = new Users();
 		 $contactinfo = new ProfileContactInfo();
 		 $projects = new Projects();
 		 $tests = new Tests();
 		 $researcher = new ProfileResearcher();
 		 
 		 $this->view->totalDevelopers=$developer->count();
 		 $this->view->totalTesters=$users->count();
 		 $this->view->network=$reg->countNetworks();	 
 		 $this->view->userCountry=$contactinfo->countCountries();
 		 $this->view->totalProjects=$projects->count();
 		 $this->view->totalTests=$tests->count();
 		 $this->view->totalResearchers=$researcher->countAllResearchers();
 		 $this->view->fundsInEscrow=$projects->getEscrowFundsForProject(0);
 		 $this->view->mobPro=$users->countMobPro();
	}

	public function canceltestAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->db=Zend_Registry::get('db');
		
	}
	public function tstcsvAction()
	{
		$this->_helper->layout->disableLayout();
		header("Expires: 0");
		header("Cache-control: private");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Description: File Transfer");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=tester.csv");		
		$db=Zend_Registry::get('db');

		$query=$db->query("select username, email, first_name,last_name, Pro, city, countries.country,  networks.network, vendor, model, mobile, count(regdevices.idregdevice) as numdevices, date_FORMAT(registered_on,'%m/%d/%Y') as date, tests.idtest ".
				    "from users LEFT JOIN profile_contactinfo ON profile_contactinfo.id=users.id LEFT JOIN profile_tester ON profile_tester.id=users.id LEFT JOIN regdevices ON users.id=regdevices.idtester ".
				    "LEFT JOIN deviceatlas ON deviceatlas.id=regdevices.deviceatlasid LEFT JOIN networks ON networks.idnetwork=regdevices.idnetwork LEFT JOIN countries ON countries.idcountry=profile_contactinfo.idcountry ".
				    "LEFT JOIN tests ON tests.idtester=users.id AND tests.Status=400 GROUP by users.id");    
				    
		$out = '';


		// Add all values in the table to $out.
		$headers=FALSE;
		while ($result = $query->fetch()) {
			if (!$headers) {
				foreach($result as $index=>$row){
					$out .='"'.$index.'"'.',';
				}
				$out .="\n";
				$headers=TRUE;
			}
			foreach ($result as $index=>$row) {
				$out .='"'.$row.'"'.',';
			}
			$out .="\n";
		}
		echo $out;
	}
	
	public function devcsvAction()
	{
		$this->_helper->layout->disableLayout();
		header("Expires: 0");
		header("Cache-control: private");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Description: File Transfer");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=developer.csv");		
		$db=Zend_Registry::get('db');

		$query=$db->query("select username, email, first_name,last_name, Pro, City, countries.Country,date_FORMAT(registered_on,'%m/%d/%Y') as date, profile_contactinfo.company AS company_name from users  INNER JOIN profile_contactinfo ON profile_contactinfo.id=users.id INNER JOIN profile_developer ON profile_developer.id=users.id LEFT JOIN countries ON countries.idcountry=profile_contactinfo.idcountry ORDER BY users.id");    
				    
		$out = '';


		// Add all values in the table to $out.
		$headers=FALSE;
		while ($result = $query->fetch()) {
			if (!$headers) {
				foreach($result as $index=>$row){
					$out .='"'.$index.'"'.',';
				}
				$out .="\n";
				$headers=TRUE;
			}
			foreach ($result as $index=>$row) {
				$out .='"'.$row.'"'.',';
			}
			$out .="\n";
		}
		echo $out;
	}

	// TODO: Make these reports generic so we can create one line this $report = new Report("SELECT..."); $report->getCSV(); // Too much copy and paste already 
	public function carriercsvAction()
	{
		$this->_helper->layout->disableLayout();
		header("Expires: 0");
		header("Cache-control: private");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Description: File Transfer");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=carriers.csv");		
		$db=Zend_Registry::get('db');


		$query=$db->query("SELECT country, network, count(*) from regdevices inner join networks on networks.idnetwork=regdevices.idnetwork inner join countries on countries.idcountry=networks.idcountry group by country, network;");    
				    
		$out = '';


		// Add all values in the table to $out.
		$headers=FALSE;
		while ($result = $query->fetch()) {
			if (!$headers) {
				foreach($result as $index=>$row){
					$out .='"'.$index.'"'.',';
				}
				$out .="\n";
				$headers=TRUE;
			}
			foreach ($result as $index=>$row) {
				$out .='"'.$row.'"'.',';
			}
			$out .="\n";
		}
		echo $out;
	}
	
	// List all inputs and outputs to/from escrow
	public function escrowAction()
	{
		 $this->_helper->layout->setLayout('admin');
		
		$db=Zend_Registry::get('db');

		$query=$db->query("SELECT accounts.transactionid, accounts.iduser, accounts.transactioncode, DATE_FORMAT(FROM_UNIXTIME(Timestamp),'%m/%d/%y') AS date, accounts.transactiontype, users.username, accounts.amount, tests.commission, tests.bidAmount, tests.salestax FROM accounts LEFT JOIN tests ON tests.idtest=accounts.idtest LEFT JOIN users ON users.id=accounts.iduser WHERE transactioncode IN (2,3,4) ORDER BY transactionid");    

		$balance=0;
		$profit=0;
		
		if (isset($_GET['csv']))
		{
			$this->_helper->layout->disableLayout();
			header("Expires: 0");
			header("Cache-control: private");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Description: File Transfer");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=escrow.csv");		
			
			$out = '';
			// Add all values in the table to $out.
			$headers=FALSE;
			while ($result = $query->fetch()) {
				$result['amount']*=-1; // reverse the sign so that funds into escrow are positive
				if (!$headers) {
					foreach($result as $index=>$row){
						$out .='"'.$index.'"'.',';
					}
					$out .=",Escrow Totals,Profit\n";
					$headers=TRUE;
				}
				foreach ($result as $index=>$row) {
					$out .='"'.$row.'"'.',';
				}
				if($result['transactioncode']==2)
				{
					//This is escrow in
					$balance+=$result['bidAmount']+$result['commission']+$result['salestax'];
				}
				else
				{
					//This is escrow out
					$tempTotal=($result['bidAmount']+$result['commission']+$result['salestax'])*-1;
					$balance+=$tempTotal;
					if($result['transactioncode']==3)
					{
						$profit+=$result['commission'];
					}
				}
				$out .=",".$balance.",".$profit."\n";
			}
			echo $out;
		}
		else {
			echo "<h1>Funds in Escrow</h1>";
			echo "<a href='/admin/index/escrow/?csv=1'?>Download csv</a><br/><br/>";
			$out="<table class=\"table-esc\">";
			$headers=FALSE;
			while ($result = $query->fetch()) {
				
				
				$result['amount']*=-1; // reverse the sign so that funds into escrow are positive
				$out.="<tr>";
				if (!$headers) {
					foreach($result as $index=>$row){
						$out .='<td><b>'.$index.'</b></td>';
					}
					$out .="<td><b>Escrow Totals</b></td><td><b>Profit</b></td></tr><tr>";
					$headers=TRUE;
				}
				foreach ($result as $index=>$row) {
					$out .='<td>'.$row.'</td>';
				}
				if($result['transactioncode']==2)
				{
					//This is escrow in
					$balance+=$result['bidAmount']+$result['commission']+$result['salestax'];
				}
				else
				{
					//This is escrow out
					$balance+=($result['bidAmount']+$result['commission']+$result['salestax'])*-1;
					if($result['transactioncode']==3)
					{
						$profit+=$result['commission'];
					}
				}
				
				$out .="<td>".$balance."</td><td>".$profit."</td></tr>";
			}	
			$out.="</table>";
			echo $out;
		}
	
	}
	
	// List all deposits and withdrawals to/from Paypal
	public function paypalAction()
	{
		 $this->_helper->layout->setLayout('admin');
		
		$db=Zend_Registry::get('db');

		// putting debt and credit in different columns 
		// Adjust the date/time
		// add all transaction types
		
		//$query=$db->query("SELECT accounts.transactionid, date_format(FROM_UNIXTIME(Timestamp),'%m/%d/%y') as date, accounts.transactiontype, accounts.transactioncode,users.username, accounts.amount FROM accounts LEFT JOIN users ON users.id=accounts.iduser WHERE transactioncode IN (0,1) ORDER BY transactionid;");    
		$query=$db->query("SELECT accounts.transactionid, date_format(FROM_UNIXTIME(Timestamp),'%m/%d/%y') as date, accounts.transactiontype, accounts.transactioncode,users.username, accounts.amount FROM accounts LEFT JOIN users ON users.id=accounts.iduser WHERE transactioncode IN (0,1) ORDER BY transactionid;");
				
		$balance=0;
		if (isset($_GET['csv']))
		{
			$this->_helper->layout->disableLayout();
			header("Expires: 0");
			header("Cache-control: private");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Description: File Transfer");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=paypal.csv");		
			
			$out = '';
			// Add all values in the table to $out.
			$headers=FALSE;
			while ($result = $query->fetch()) {
				if (!$headers) {
					foreach($result as $index=>$row){
						$out .='"'.$index.'"'.',';
					}
					$out .="\n";
					$headers=TRUE;
				}
				foreach ($result as $index=>$row) {
					$out .='"'.$row.'"'.',';
				}
				$balance+=$result['amount'];
				$out .=",".$balance."\n";
			}
			echo $out;
		}
		else {
			echo "<h1>PayPal Transactions</h1>";
			echo "<a href='/admin/index/paypal/?csv=1'?>Download csv</a><br/><br/>";
			$out="<table class=\"table-pp\">";
			$headers=FALSE;
			while ($result = $query->fetch()) {
				$out.="<tr>";
				if (!$headers) {
					foreach($result as $index=>$row){
						$out .='<td><b>'.$index.'</b></td>';
					}
					$out .="</tr><tr>";
					$headers=TRUE;
				}
				foreach ($result as $index=>$row) {
					$out .='<td>'.$row.'</td>';
				}
				$balance+=$result['amount'];
				$out .="<td>".$balance."</td></tr>";
			}	
			$out.="</table>";
			echo $out;
		}
	}
	
	public function listprojectsAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$this->view->db=Zend_Registry::get('db');
	}

	public function showprojectAction()
	{
		$this->_helper->layout->setLayout('admin');
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		$this->view->db=Zend_Registry::get('db');
		$this->view->idproject=$this->_getParam('idproject', 0);
		$projecttable=new Projects();
		$this->view->proj=$projecttable->getProject($this->view->idproject);
	}
	
	
	// Send test email
	public function testemailAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		
		if(isset($parameters['name'])){
			$users = new Users(); 
			$user = $users->getUser(0,$parameters['name']);
			
			$mailer=new Mailer();			
			$mailer->sendTestEmail($user['id']);
			
			$this->view->name = $user['email'];
		}else{
			$this->view->name = "NOT SENT";
		}
		
	
	}
	
	/*
	 * Mostly evil can come from this, be careful
	 */
	public function loginasAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
	
		if(isset($parameters['name'])){
			$users = new Users();
			$user = $users->getUser(0,$parameters['name']);
				
			
			$username = $user['username'];
			$password = $user['password'];
			if ($username && $password) {
				$auth = Zend_Auth::getInstance();

				$db = Zend_Registry::get('db');
				$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
				$authAdapter->setIdentity($username)->setCredential($password);
				$authResult = $auth->authenticate($authAdapter);
				if ($authResult->isValid()) {
					//valid username and password
					$userInfo = $authAdapter->getResultRowObject();
					if ($userInfo->active) {
						//save userinfo in session
						$userInfo->password = '';
						Zend_Registry::get('defSession')->currentUser = $userInfo;
//						Zend_Session::rememberMe(2419200); // 28 days
						// Write last_login info to db
						$users = new Users(); 
						$now = new Zend_Date(); 
							
						//redirect to home
						$this->_redirect('/app/', array ('exit'=>true));
					} else {
						$this->view->error ="User Not Activated";
					}
				} else {
					$this->view->error ="Unable to login:Credentials";
				}
			}
			else {
				$this->view->error ="Unable to login:Other";
			}
			
		
		}else{
					$this->view->error ="Unable to login:name not set";
		}
	
	
	}
	
	public function cacheStatsAction()
	{
	}
	
	public function developerinfoAction()
	{
		 $this->_helper->layout->setLayout('admin');
		
		$db=Zend_Registry::get('db');

		$revenue = array(1 => 'Original Mobile Application Development',2 => 'Branded Mobile Application Development',3 => 'Content Development',4 => 'Mobile 2.0 Web Development',5 => 'Porting Applications From Various Platforms',6 => 'Text Messaging/SMS',7 => 'MMS',8 => 'Content Aggregator',9 => 'SMS/MMS Aggregator',10 => 'Mobile Application Aggregator',11 => 'Application Storefront',12 => 'Aggregate Content For Licensing And/Or Direct Sales',13 => 'Offer Services To Other Developers (please specify)',14 => 'Other (please specify)');
		$platform = array('iphone'=>'IPhone','blackberry' => 'Blackberry','android' => 'Android','palm' => 'Palm','flash' => 'Flash','Windows' => 'Windows Mobile','j2me' => 'J2ME','symbian' => 'Symbian','mob2' => 'Mobile Web','other' => 'Other');
		$apps = array(1 => 'News',2 => 'Medical',3 => 'Reference',4 => 'Productivity',5 => 'Navigation',6 => 'Health and Fitness',7 => 'Education',8 => 'Weather',9 => 'Business',10 => 'Music',11 => 'Finance',12 => 'Sports',13 => 'Travel',14 => 'Utilities',15 => 'Games',16 => 'Social Networking',17 => 'Books',18 => 'Entertainment',19 => 'Lifestyle');
		$numEmp = array(1 => 'Rather not say.',2 => '1 (me!)',3 => '2-5',4 => '6-10',5 => '11-25',6 => '26-100',7 => '101-500',8 => '500+');

		$balance=0;
		if (isset($_GET['csv']))
		{
			$query=$db->query("SELECT u.username AS username, u.first_name AS first_name, u.last_name AS last_name, u.email AS email, 
								c.address1 AS address1, c.address2 AS address2, c.state AS state, c.city AS city,c.postcode AS postcode,
								c.zip AS zip, cc.country AS idcountry, d.revenue AS revenue, d.apps AS apps, d.num_emloyees AS num_emloyees, d.platform AS platform
								FROM profile_developer d JOIN users u ON u.id=d.id
								JOIN profile_contactinfo c ON c.id=d.id LEFT JOIN countries cc ON cc.idcountry=c.idcountry
								WHERE 1;");    
			$this->_helper->layout->disableLayout();
			header("Expires: 0");
			header("Cache-control: private");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Description: File Transfer");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=developerinfo.csv");		
			
			$out = '';
			// Add all values in the table to $out.
			$headers=FALSE;
			while ($result = $query->fetch()) {
				if (!$headers) 
				{
					$out .='username, ';
					$out .='first_name,';
					$out .='last_name,';
					$out .='email,';
					$out .='address1,';
					$out .='address2,';
					$out .='city,';
					$out .='state,';
					$out .='zip,';
					$out .='postcode,';
					$out .='idcountry,';
					foreach($revenue as $index=>$row){
						$out .='"'.$row.'"'.',';
					}
					foreach($apps as $index=>$row){
						$out .='"'.$row.'"'.',';
					}
					foreach($platform as $index=>$row){
						$out .='"'.$row.'"'.',';
					}
					$out .="\n";
					$headers=TRUE;
				}
				else 
				{
					
					$out .=str_replace(",", "-", $result['username']).','.str_replace(",", "-", $result['first_name']).','.str_replace(",", "-", $result['last_name']).','.str_replace(",", "", $result['email']).','.str_replace(",", "-", $result['address1']).','.str_replace(",", "-", $result['address2']).',';
					$out .=str_replace(",", "-", $result['city']).','.str_replace(",", "-", $result['state']).','.str_replace(",", "-", $result['zip']).','.str_replace(",", "-", $result['postcode']).','.str_replace(",", "-", $result['idcountry']).',';
					if($result['revenue'])
					{
						$textArray=explode(",", $result['revenue']);
						foreach($revenue as $index=>$row)
						{
							if(array_key_exists($index, $textArray))
								$out .='Y,';
							else
								$out .=',';
						}
					}
					else
					{
						foreach($revenue as $index=>$row){
							$out .=',';
						}
					}
					
					if($result['apps'])
					{
						$textArray=explode(",", $result['apps']);
						foreach($apps as $index=>$row)
						{
							if(array_key_exists($index, $textArray))
								$out .='Y,';
							else
								$out .=',';
						}
					}
					else
					{
						foreach($apps as $index=>$row){
							$out .=',';
						}
					}
					if($result['platform'])
					{
						$text = $result['platform'];
						foreach($platform as $index=>$row)
						{
							$match1 = strstr($text, $index);
							$match2 = strstr($text, $row);
							if($match1||$match2)
								$out .='Y,';
							else
								$out .=',';
						}
					}
					else
					{
						foreach($platform as $index=>$row)
						{
							$out .=',';
						}
					}
					$out .=','.$result['num_emloyees'];
				}
				$out .="\n";
			}
			echo $out;
		}
		else 
		{
			$query=$db->query("SELECT u.username AS username, u.first_name AS first_name, u.last_name AS last_name, u.email AS email, 
								c.address1 AS address1, c.address2 AS address2, c.state AS state, c.city AS city,c.postcode AS postcode,
								c.zip AS zip, c.idcountry AS idcountry, d.revenue AS revenue, d.apps AS apps, d.num_emloyees AS num_emloyees, d.platform AS platform
								FROM profile_developer d JOIN users u ON u.id=d.id
								JOIN profile_contactinfo c ON c.id=d.id
								WHERE c.idcountry=1;");    
			echo "<h1>Developer Information</h1>";
			echo "<a href='/admin/index/developerinfo/?csv=1'?>Download csv</a><br/><br/>";
			$out="<table class=\"table-pp\">";
			$headers=FALSE;
			while ($result = $query->fetch()) {
				$out.="<tr>";
				if (!$headers) 
				{
					$out .='<td>username</td>';
					$out .='<td>first_name</td>';
					$out .='<td>last_name</td>';
					$out .='<td>email</td>';
					$out .='<td>address1</td>';
					$out .='<td>address2</td>';
					$out .='<td>city</td>';
					$out .='<td>state</td>';
					$out .='<td>zip</td>';
					$out .='<td>postcode</td>';
					$out .='<td>idcountry</td>';
					foreach($revenue as $index=>$row){
						$out .='<td>'.$row.'</td>';
					}
					foreach($apps as $index=>$row){
						$out .='<td>'.$row.'</td>';
					}
					foreach($platform as $index=>$row){
						$out .='<td>'.$row.'</td>';
					}
					$out .='<td>num_employees</td>';
					$out .="</tr><tr>";
					$headers=TRUE;
				}
				else
				{
					$out .='<td>'.$result['username'].'</td>';
					$out .='<td>'.$result['first_name'].'</td>';
					$out .='<td>'.$result['last_name'].'</td>';
					$out .='<td>'.$result['email'].'</td>';
					$out .='<td>'.$result['address1'].'</td>';
					$out .='<td>'.$result['address2'].'</td>';
					$out .='<td>'.$result['city'].'</td>';
					$out .='<td>'.$result['state'].'</td>';
					$out .='<td>'.$result['zip'].'</td>';
					$out .='<td>'.$result['postcode'].'</td>';
					$out .='<td>'.$result['idcountry'].'</td>';
					
					if($result['revenue'])
					{
						$textArray=explode(",", $result['revenue']);
						foreach($revenue as $index=>$row)
						{
							if(array_key_exists($index, $textArray))
								$out .='<td>Y</td>';
							else
								$out .='<td></td>';
						}
					}
					else
					{
						foreach($revenue as $index=>$row){
							$out .='<td></td>';
						}
					}
					
					if($result['apps'])
					{
						$textArray=explode(",", $result['apps']);
						foreach($apps as $index=>$row)
						{
							if(array_key_exists($index, $textArray))
								$out .='<td>Y</td>';
							else
								$out .='<td></td>';
						}
					}
					else
					{
						foreach($apps as $index=>$row){
							$out .='<td></td>';
						}
					}
					if($result['platform'])
					{
						$text = $result['platform'];
						foreach($platform as $index=>$row)
						{
							$match1 = strstr($text, $index);
							$match2 = strstr($text, $row);
							if($match1||$match2)
								$out .='<td>Y</td>';
							else
								$out .='<td></td>';
						}
					}
					else
					{
						foreach($platform as $index=>$row){
							$out .='<td></td>';
						}
					}
					$out .='<td>'.$result['num_emloyees'].'</td>';
				}
				$out .="</tr>";
			}	
			$out.="</table>";
			echo $out;
		}
	}

	public function iddeviceAction()
	{
		$form=new Zend_Form();
		$ua=new Zend_Form_Element_Text('ua');
		$ua->setLabel('User Agent');
		$submit=new Zend_Form_Element_Submit('submit');
		$form->addElement($ua);
		$form->addElement($submit);
		$this->view->form=$form;
		if ($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$devices=new Devices();
				$properties=$devices->identifyDevice($formData['ua']);

				print_r($formData['ua']);
				print_r($properties);
			}
		}
	}
 }
