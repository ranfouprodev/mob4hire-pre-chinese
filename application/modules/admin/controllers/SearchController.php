<?php

class Admin_SearchController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_helper->layout->setLayout('admin');
		
		
	}

	public function userAction(){
		
		$this->_helper->layout->setLayout('admin');
		
		$parameters = $this->_getAllParams();
		
		$projects = new Search_UserSearch(); 

		
		if ($this->getRequest()->isPost())
		{
			$action = $parameters['execute'];
			switch($action){
				case "reindex":
					echo "Indexing";
					set_time_limit(90000);
					$projects->fullIndex();
					break;
				case "optimize":
					set_time_limit(7200);
					$projects->optimize();
					break;
			}
		}
		
		$this->view->count = $projects->count(); 
		
		$this->view->index = $projects->getIndex(); 
		
		
	}
	
	
	public function projectAction(){
		
		$this->_helper->layout->setLayout('admin');
		
		$parameters = $this->_getAllParams();
		
		$projects = new Search_ProjectSearch(); 

	
		
		if ($this->getRequest()->isPost())
		{
			$action = $parameters['execute'];
			switch($action){
				case "reindex":
					echo "Indexing";
					set_time_limit(90000);
					$projects->fullIndex();
					break;
				case "optimize":
					set_time_limit(3600);
					$projects->optimize();
					break;
			}
		}
		
		$this->view->count = $projects->count(); 
		
		$this->view->index = $projects->getIndex(); 
		
		
	}
		
}