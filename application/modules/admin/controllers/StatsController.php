<?php

class Admin_StatsController extends Zend_Controller_Action
{
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
	}
	
	public function testersAction()
	{
		$this->_helper->layout->setLayout('admin');
		$myForm=new Form_StatForm4();
		$this->view->blah=$myForm;
		$now=strtotime("now");
		$endDate=date('Y-m-d', $now);
		$flag="";
		//Time Formatting here
		if(isset($_POST['select2']))
		{
			$check=$_POST['select2'];
			
			if($check==1)
			{
				//Yesterday
				$flag='Yesterday';
				$startDate=date('Y-m-d', $now-86400);
				
			}
			elseif($check==2)
			{
				//Last Week
				$flag='Last Week';
				$startDate=date('Y-m-d', $now-604800);
			}
			elseif($check==3)
			{
				//Last Month
				$flag='Last Month';
				$startDate=date('Y-m-d', $now-2592000);
			}
			elseif($check==4)
			{
				//Last Quarter
				$flag='Last Quarter';
				$startDate=date('Y-m-d', $now-7776000);
			}
			elseif($check==5)
			{
				//Last Year
				$flag='Last Year';
				$startDate=date('Y-m-d', $now-31536000);
			}
			elseif($check==6)
			{
				//Manually Input Dates
				$flag='1';
				$endDate=$_POST['endDate2'];
				$startDate=$_POST['startDate2'];
			}
		}
		else
		{
				$flag='Last Week';
				$startDate=date('Y-m-d', $now-604800);
		}
		$this->view->flag=$flag;
		$timeInterval = strtotime($startDate)*2-strtotime($endDate);
 		$oldStartDate =date('Y-m-d', $timeInterval); 
 		$today = new Zend_Db_Expr('NOW()');
		$this->view->startDate=$startDate;
 		$this->view->endDate=$endDate;
 		$this->view->oldStartDate=$oldStartDate;
 		
 		
 		//Class declerations here
		$tester = new ProfileMobster();
		$regdevice = new Regdevices();
		$transactions = new Transactions();
		$project = new Projects();
		$contactinfo = new ProfileContactInfo();
		$tests = new Tests();
		$users = new Users();
		$user = new Users();
		
		$this->view->mobPro=$users->countMobPro();
		
 		//New Tester Statistics
		$this->view->newTesters=$tester->countNewTesters($startDate,$endDate);
 		$tempVar=$tester->countNewTesters($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->newTestersDelta=number_format((($this->view->newTesters-$tempVar)/$tempDiv)*100,1);
 		$this->view->avgNewTestersDaily=number_format($tester->averageDailyNewTesters($startDate,$endDate));
 		$tempVar=$tester->averageDailyNewTesters($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgNewTestersDailyDelta=number_format((($this->view->avgNewTestersDaily-$tempVar)/$tempDiv)*100,1);
 		$this->view->avgNewTestersWeekly=number_format($tester->averageWeeklyNewTesters($startDate,$endDate));
 		$tempVar=$tester->averageWeeklyNewTesters($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgNewTestersWeeklyDelta=number_format((($this->view->avgNewTestersWeekly-$tempVar)/$tempDiv)*100,1);
		$this->view->newHandSets=$regdevice->countDevicesOverRange($startDate, $endDate);
		$tempVar=$regdevice->countDevicesOverRange($oldStartDate, $startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
		$this->view->newHandSetsDelta=number_format((($this->view->newHandSets-$tempVar)/$tempDiv)*100,1);
		$tempVar = $transactions->countTesterActivityByStatus($startDate, $endDate, -1);
		//BS FUDGE FACTOR FOR BS REASONS
		$tempVar2 = $tester->countAllTesters()-$tester->countNewTesters($endDate, $today)-2044;
		if($tempVar2<=0)
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar2;
		} 
		$dog=$transactions->countTesterActivityByStatus($startDate, $endDate, -1)*100;
		$cat=$dog/$tempDiv;
		$this->view->percentTestersTesting = number_format($cat,1);
 		$tempVar3=$tempVar2-$tester->countNewTesters($oldStartDate,$startDate);
 		$dog=$transactions->countTesterActivityByStatus($oldStartDate,$startDate, -1)*100;
 		if($tempVar3<=0) $tempVar3=1;
 		$cat=$dog/$tempVar3;
 		$tempVar=number_format($cat,1);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->percentTestersTestingDelta=number_format((($this->view->percentTestersTesting-$tempVar)/$tempDiv)*100,1);
 		$dog=$transactions->countTesterActivityByStatus($startDate, $endDate, 200)*100;
 		if($tempVar2<=0)
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar2;
		} 
 		$cat=$dog/$tempDiv;
		$this->view->percentTestersBid=number_format($cat,1);
		$dog=$transactions->countTesterActivityByStatus($oldStartDate,$startDate, 200)*100;
		if($tempVar3<=0)
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar3;
		} 
		$cat=$dog/$tempDiv;
 		$tempVar=number_format($cat,1);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->percentTestersBidDelta=number_format((($this->view->percentTestersBid-$tempVar)/$tempDiv)*100,1);
 		$this->view->avgTestersPerProject =number_format($project->averageTestsPerProject($startDate,$endDate));
 		$tempVar=$project->averageTestsPerProject($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgTestersPerProjectDelta = number_format((($this->view->avgTestersPerProject-$tempVar)/$tempDiv)*100,1);
 		
        //Total Mobster Statistics
        $this->view->totalMobsters=$users->count();
        $this->view->totalTesters=$tester->countAllTesters();
        $this->view->totalCountries=$contactinfo->countCountries();
        $this->view->totalHandSets=$regdevice->count();
        $this->view->totalNetworks=$regdevice->countNetworks();
        $this->view->percentTestersWithHandset = number_format($regdevice->percentageTestersWithHandsets(),1);
        
        //Pay Results
        $cat=$tests->getBidSumForRange($startDate, $endDate);
        $dog=$tests->getBidSumForRange($oldStartDate, $startDate);
        $this->view->avgPayPerTest=number_format($cat['average'],2);
        if($dog['average']==0)
        {
        	$tempVar=$cat['average'];
        }
        else
        {
        	$tempVar = ($cat['average']-$dog['average'])/$dog['average'];
        }
        $this->view->avgPayPerTestDelta=number_format($tempVar,1);
 		
        //NEW USERS
        $this->view->newUsers=$user->countNewUsers($startDate,$endDate);
 		$tempVar=$users->countNewUsers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->newUsersDelta=number_format((($this->view->newUsers-$tempVar)/$tempDiv)*100,1);
 		$this->view->avgNewUsersDaily=number_format($users->averageDailyNewUsers($startDate,$endDate));
 		$tempVar=$users->averageDailyNewUsers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgNewUsersDailyDelta=number_format((($this->view->avgNewUsersDaily-$tempVar)/$tempDiv)*100,1);
 		$this->view->avgNewUsersWeekly=number_format($users->averageWeeklyNewUsers($startDate,$endDate));
 		$tempVar=$users->averageWeeklyNewUsers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgNewUsersWeeklyDelta=number_format((($this->view->avgNewUsersWeekly-$tempVar)/$tempDiv)*100,1);
 		
	}
	
	public function developerAction()
	{
		$this->_helper->layout->setLayout('admin');
		$myForm=new Form_StatForm2();
		$this->view->blah=$myForm;
		$now=strtotime("now");
		$endDate=date('Y-m-d', $now);
			$flag="";
		//Time Formatting here
		if(isset($_POST['select2']))
		{
			$check=$_POST['select2'];
			
			if($check==1)
			{
				//Yesterday
				$flag='Yesterday';
				$startDate=date('Y-m-d', $now-86400);
				
			}
			elseif($check==2)
			{
				//Last Week
				$flag='Last Week';
				$startDate=date('Y-m-d', $now-604800);
			}
			elseif($check==3)
			{
				//Last Month
				$flag='Last Month';
				$startDate=date('Y-m-d', $now-2592000);
			}
			elseif($check==4)
			{
				//Last Quarter
				$flag='Last Quarter';
				$startDate=date('Y-m-d', $now-7776000);
			}
			elseif($check==5)
			{
				//Last Year
				$flag='Last Year';
				$startDate=date('Y-m-d', $now-31536000);
			}
			elseif($check==6)
			{
				//Manually Input Dates
				$flag='1';
				$endDate=$_POST['endDate2'];
				$startDate=$_POST['startDate2'];
			}
		}
		else
		{
				$flag='Last Week';
				$startDate=date('Y-m-d', $now-604800);
		}
		$this->view->flag=$flag;
		$this->view->startDate=$startDate;
 		$this->view->endDate=$endDate;
 		
 		//This is combined into one step.
 		$timeInterval = strtotime($startDate)*2-strtotime($endDate);
 		$oldStartDate =date('Y-m-d', $timeInterval); 
		
		//Classes Used
		$developer = new ProfileDeveloper();
		$project = new Projects();
		$reg = new Regdevices();
		$transactions = new Transactions();
		$statusType='Blah';
		$tester = new ProfileMobster();
		$tests = new Tests();
		$researcher = new ProfileResearcher();
 	
		//New PROJECTS
		$this->view->newProjects=$project->countNewProjects($startDate,$endDate);
 		$oldProjects=$project->countNewProjects($oldStartDate,$startDate);
		if($oldProjects==0) 
		{
			$tempDiv=1;
			$oldProjects=1;
		}
		else
		{
			$tempDiv=$oldProjects;
		}
 		$this->view->newProjectsDelta=number_format((($this->view->newProjects-$oldProjects)/$tempDiv)*100,1);
 		$newProjects=$this->view->newProjects;
 		if($newProjects==0) $newProjects=1;
 		 		
 		//Average Bids Per Project
 		$this->view->avgBidsPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 100)/$newProjects);
 		$tempVar=$transactions->countTransactionsByStatus($oldStartDate, $startDate, 100);
 		$tempVar=$tempVar/$oldProjects;
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgBidsPerProjectDelta=number_format((($this->view->avgBidsPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//Average Invited Per Project
 		$this->view->avgInvitedPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 50)/$newProjects);
 		$tempVar=number_format($transactions->countTransactionsByStatus($oldStartDate, $startDate, 50)/$oldProjects);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgInvitedPerProjectDelta=number_format((($this->view->avgInvitedPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//Average Accepted Per Project
 		$this->view->avgAcceptedPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 200)/$newProjects);
 		$tempVar=number_format($transactions->countTransactionsByStatus($oldStartDate, $startDate, 200)/$oldProjects);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgAcceptedPerProjectDelta=number_format((($this->view->avgAcceptedPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//Average Submitted Per Project
 		$this->view->avgSubmittedPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 300)/$newProjects);
 		$tempVar=number_format($transactions->countTransactionsByStatus($oldStartDate, $startDate, 300)/$oldProjects);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgSubmittedPerProjectDelta=number_format((($this->view->avgSubmittedPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//Average Paid Per Test
 		$this->view->avgPaidPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 400)/$newProjects);
 		$tempVar=number_format($transactions->countTransactionsByStatus($oldStartDate, $startDate, 400)/$oldProjects);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgPaidPerProjectDelta=number_format((($this->view->avgPaidPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//Average Accepted Bid Value
        $cat=$tests->getBidSumForRange($startDate, $endDate);
        $dog=$tests->getBidSumForRange($oldStartDate, $startDate);
        $this->view->avgPayPerTest=number_format($cat['average'],2);
        if($dog['average']==0)
        {
        	$tempVar=$cat['average'];
        }
        else
        {
        	$tempVar = ($cat['average']-$dog['average'])/$dog['average'];
        }
        $this->view->avgPayPerTestDelta=number_format($tempVar,1);
        
        //Average Outstanding Per Project
 		$this->view->avgOutstandingPerProject=number_format($transactions->countTransactionsByStatus($startDate, $endDate, 250)/$newProjects);
 		$tempVar=number_format($transactions->countTransactionsByStatus($oldStartDate, $startDate, 250)/$oldProjects);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgOutstandingPerProjectDelta=number_format((($this->view->avgOutstandingPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		
 		//New Developers
 		$this->view->newDeveloper=$developer->countNewDevelopers($startDate,$endDate);
 		$tempVar=$developer->countNewDevelopers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->newDeveloperDelta=number_format((($this->view->newDeveloper-$tempVar)/$tempDiv)*100,1);
 		//Average New Developers Daily
 		$this->view->avgDailyNewDeveloper=number_format($developer->averageDailyNewDevelopers($startDate,$endDate),2);
 		$tempVar=$developer->averageDailyNewDevelopers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgDailyNewDeveloperDelta=number_format((($this->view->avgDailyNewDeveloper-$tempVar)/$tempDiv)*100,1);
 		//Average New Developers Weekly
 		$this->view->avgWeeklyNewDeveloper=number_format($developer->averageWeeklyNewDevelopers($startDate,$endDate),2);
 		$tempVar=$developer->averageWeeklyNewDevelopers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgWeeklyNewDeveloperDelta=number_format((($this->view->avgWeeklyNewDeveloper-$tempVar)/$tempDiv)*100,1);
 		
 		//AVERAGE TESTS STATS
 		$this->view->avgTestersPerProject =number_format($project->averageTestsPerProject($startDate,$endDate));
 		$tempVar=$project->averageTestsPerProject($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgTestersPerProjectDelta = number_format((($this->view->avgTestersPerProject-$tempVar)/$tempDiv)*100,1);
 		
 		//TOTAL DEVELOPERS
 		$this->view->totalDevelopers=$developer->count();
 		
 		//Total Researchers
 		$this->view->totalResearchers=$researcher->countAllResearchers();
 		
 		//New Researchers
 		$this->view->newResearcher=$researcher->countNewResearchers($startDate,$endDate);
 		$tempVar=$researcher->countNewResearchers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->newResearcherDelta=number_format((($this->view->newResearcher-$tempVar)/$tempDiv)*100,1);
 		//Average New Researchers Daily
 		$this->view->avgDailyNewResearcher=number_format($researcher->averageDailyNewResearchers($startDate,$endDate),2);
 		$tempVar=$researcher->averageDailyNewResearchers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgDailyNewResearcerDelta=number_format((($this->view->avgDailyNewResearcher-$tempVar)/$tempDiv)*100,1);
 		//Average New Researchers Weekly
 		$this->view->avgWeeklyNewResearcher=number_format($researcher->averageWeeklyNewResearchers($startDate,$endDate),2);
 		$tempVar=$researcher->averageWeeklyNewResearchers($oldStartDate,$startDate);
		if($tempVar==0) 
		{
			$tempDiv=1;
		}
		else
		{
			$tempDiv=$tempVar;
		}
 		$this->view->avgWeeklyNewResearcherDelta=number_format((($this->view->avgWeeklyNewResearcher-$tempVar)/$tempDiv)*100,1);
 		
 		
	}
	
	public function statdogAction()
	{
		$this->_helper->layout->setLayout('admin');
		//require_once '/library/Mobi/DA/Api.php';
		$mobi = new Mobi_Mtld_DA_Api();	
		$tree = $mobi->getTreeFromFile('C:\mob\trunk/library/Mobi/json/DeviceAtlas.json');
		$blah= $mobi->listProperties($tree);
		$reg = new Regdevices();
		$idregdevice=91;
		$row = $reg->getRegdevice($idregdevice);
		$agent = $row['http_user_agent'];
		$blah=$mobi->getProperties($tree, $agent);
		$property="id";
		$what=$mobi->getProperty($tree, $agent, $property);
		echo "<h1>$what</h1>";
		$out="<table class='dynamicTable' align=center border=1>";
		$headers=FALSE;
		foreach($blah as $index=>$row) {
			$out.="<tr>";
			$out .='<th>'.$index.'</th>';

			$out .='<td>'.$row.'</td>';
			
			$out .="</tr>";
		}	
		$out.="</table>";
		echo $out;
	}
	
	public function catAction()
	{
		$this->_helper->layout->setLayout('admin');
		$parameters = $this->_getAllParams();
		$this->view->headLink()->appendStylesheet("/css/dynamictable.css");
		if(isset($parameters['query']))
		{
			$sql=$parameters['queryArea'];
			$db=Zend_Registry::get('db');
			$query=$db->query($sql);
			echo "QUERY IS :: <br/>".$sql;
			$out="<table class='dynamicTable' align=center border=1>";
			$headers=FALSE;
			while ($result = $query->fetch()) {
				$out.="<tr>";
				if (!$headers) {
					foreach($result as $index=>$row){
						$out .='<th>'.$index.'</th>';
					}
					$out .="</tr><tr>";
					$headers=TRUE;
				}
				foreach ($result as $index=>$row) {
					$out .='<td>'.$row.'</td>';
				}
				$out .="</tr>";
			}	
			$out.="</table>";
			echo $out;
		}
		elseif(isset($parameters['insert']))
		{
			$sql=$parameters['queryArea'];
			$db=Zend_Registry::get('db');
			$db->query($sql);
			echo "Query done";
		}
		
	
		
	}
	
	public function dogAction()
	{
		$this->_helper->layout->setLayout('admin');	
		$parameters = $this->_getAllParams();
		//Going to test some reCAPTCHA stuff here
		if(isset($parameters['message']))
		{
			$this->view->message="INVALID ENTRY";
		}
		else
		{
			$this->view->message="";
		}
		
	}
	
	public function doggyAction()
	{
		$this->_helper->layout->setLayout('admin');		
		//GOING to use this for more Captcha stuff
		require_once('Lib/Captcha/recaptchalib.php');
		$privatekey = "6LeUKb4SAAAAAGvKRTqijA81f18MvNdVSjlZ4LkL";
		$resp = recaptcha_check_answer ($privatekey,
		                              $_SERVER["REMOTE_ADDR"],
		                              $_POST["recaptcha_challenge_field"],
		                              $_POST["recaptcha_response_field"]);
		if (!$resp->is_valid) 
		{
			// What happens when the CAPTCHA was entered incorrectly
			$this->_redirect('/admin/stats/dog/message/1');
			die ("The reCAPTCHA wasn't entered correctly. Go back and try it again.".
			"(reCAPTCHA said: " . $resp->error . ")");
		} 
		else
		{
			echo "<h1>GOOD DOGGY!!!</h1>";
			// Your code here to handle a successful verification
		}
		
	}
}