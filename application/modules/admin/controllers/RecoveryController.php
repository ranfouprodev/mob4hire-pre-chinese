<?php

class Admin_RecoveryController extends Zend_Controller_Action
{
	public function indexAction()
	{
 		 $this->_helper->layout->setLayout('admin');
 		 
	}

	/*
	 * This will recovery and resend emails from the date specified forward in the system.
	 * This is a recovery method to ensure that lost emails are still sent
	 */
	public function emailAction()
	{
		$this->_helper->layout->setLayout('admin');
		$mailer = new Mailer();
		
		$statusFlag=0;
		$sent=0;
		$failed=0;
		$this->view->show=0;
		$name="";
		$email="";
		$templateTxt="";
		$subject="";
		$parameters = $this->_getAllParams();
		
		if(isset($parameters['datefrom']))
		{
			$this->view->show=1;
			$datestring=$parameters['datefrom'];
			$datefrom=strtotime($datestring);
			$file_name="/tmp/mailerLog.txt";
			$file_array=file($file_name);
			foreach($file_array as $line)
			{
				switch ($statusFlag)
				{
					case 0: //This means we've not even found the first line of the email
						$subjectMatch = strstr($line, 'INFO (6): Inside the send function.');
						if($subjectMatch!=FALSE)
						{
							//Reset the parameters for this we still have to verify that the date is still OK
							$messageDateString=substr($line,0,10);
							$messageDate=strtotime($messageDateString);
							$name="";
							$email="";
							$templateTxt="";
							$subject="";
							if($messageDate>=$datefrom)
							{
								//go to next stage in the email process
								$statusFlag=1;
							}
							else
							{
								//The mail message was before our datefrom so we reset
								$statusFlag=0;
							}							
						}
						break;
					case 1://This means that the first line of the mail has been read now we get email address
						$subjectMatch = strstr($line, 'INFO (6): emailAddress');
						if($subjectMatch!=FALSE)
						{
							//Set the email address
							$statusFlag=2;
							$email=substr($line,50);
						}
						break;
					case 2://This means that the email address is set now we need name
						$subjectMatch = strstr($line, 'INFO (6): name');
						if($subjectMatch!=FALSE)
						{
							//set the user's name
							$statusFlag=3;
							$name=substr($line,42);
						}
						break;
					case 3://This means that the name is set now we need subject
						$subjectMatch = strstr($line, 'INFO (6): subject');
						if($subjectMatch!=FALSE)
						{
							//set the subject
							$statusFlag=4;
							$subject=substr($line,45);
						}
						break;
					case 4://This means that the subject is set now we need templateTxt
						$subjectMatch = strstr($line, 'INFO (6): templateTxt');
						if($subjectMatch!=FALSE)
						{
							//Begin the templateTxt
							$statusFlag=5;
							$templateTxt=substr($line,49);
						}
						break;
					case 5://now we keep appending to the templateTxt until we find a new email
						$subjectMatch = strstr($line, 'INFO (6): Inside the send function.');
						if($subjectMatch!=FALSE)
						{
							//Send the email and reset the parameters
							try 
							{
								$mailer->recoverySend($email, $name, $subject, $templateTxt);
								$sent++;
							}
							catch(Exception $e)
							{
								$failed++;
							}
							
							//Reset the parameters for this we still have to verify that the date is still OK
							$messageDateString=substr($line,0,10);
							$messageDate=strtotime($messageDateString);
							$name="";
							$email="";
							$templateTxt="";
							$subject="";
							if($messageDate>=$datefrom)
							{
								//go to next stage in the email process
								$statusFlag=1;
							}
							else
							{
								//The mail message was before our datefrom so we reset
								$statusFlag=0;
							}
						}
						else
						{
							//Append this line to the end of templateTxt
							$templateTxt=$templateTxt.$line;
						}
						break;
				}//End switch
			}//End foreach
			$this->view->sent=$sent;
			$this->view->failed=$failed;
		}//End if
	}//End emailAction
	
 }