<?php

class Zend_View_Helper_CanEditHandset
{
	/*
	 * Determines if a user can edit the file
	 */
	function canEditHandset($idregdevice) 
	{
				
		try{
		
		$regdevices=new Regdevices();
		$row = $regdevices->fetchRow('idregdevice = ' . $idregdevice);		
		
		if(Zend_Registry::get('defSession')->currentUser->isAdmin){
			$canEdit = true; // Admin access
		}elseif($row && $row->idtester == Zend_Registry::get('defSession')->currentUser->id){
			// Users Can only edit the files in draft
			$canEdit = true;
		}else{
			$canEdit = false;
		}
	
		}catch(Exception $e){
			$canEdit = false;
		}
			
		return $canEdit;
			
		
	}
}