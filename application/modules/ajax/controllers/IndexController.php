<?php
/* This class is a repository for all Ajax actions that can be called from any page */
require_once 'Zend/Service/Amazon/S3.php';
	
class Ajax_IndexController  extends Zend_Controller_Action
{

	protected $s3;

	public function init() {
		$this->s3 = Zend_Registry::get('S3');
		$this->imagebucket = Zend_Registry::get('configuration')->S3->imagebucket;
		$this->securebucket = Zend_Registry::get('configuration')->S3->securebucket;
		$this->imageurl = Zend_Registry::get('configuration')->S3->imageurl;
	}
	
	/* Populate the vendor select based on platform */
	public function platformselectAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$platform=$this->getRequest()->getParam('id');
		$devices=new Devices();
		$vendors=$devices->listVendors($platform);
		// json encode the array for output in a select control
		$json= "[{\"optionValue\":\"\", \"optionDisplay\":\"Any\"},";
		foreach ($vendors as $id=>$vendor)
		{
			$json.= "{\"optionValue\":\"$id\", \"optionDisplay\":\"$vendor\"},";
		}
		$json = substr($json, 0, -1); // remove final comma		
		$json.= "]";
		header("Expires: 0");
		header("Content-Type: application/json");
		echo $json;
	}
	
	/* Populate the model select based on the selection in the vendor select */
	public function modelselectAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$vendor= $this->getRequest()->getParam('id');
		$platform=$this->getRequest()->getParam('platform');
		$idproject=$this->getRequest()->getParam('idproject');
		if ($platform=='undefined')
			$platform='';
		$devices=new Devices();
		$models=$devices->listModels($vendor, $platform, $idproject);
		// json encode the array for output in a select control
		$json= "[{\"optionValue\":\"0\", \"optionDisplay\":\"Any\"},";
		foreach ($models as $id=>$model)
		{
			$json.= "{\"optionValue\":\"$id\", \"optionDisplay\":\"$model\"},";
		}
		$json = substr($json, 0, -1); // remove final comma		
		$json.= "]";
		header("Expires: 0");
		header("Content-Type: application/json");
		echo $json;
	}
	
	/* Populate the carrier select based on the selection in the country select */
	public function networkselectAction()
	{		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$network= $this->getRequest()->getParam('id');
		$networks=new Networks();
		$networklist=$networks->listNetworks($network);
		$json= "[{\"optionValue\":\"0\", \"optionDisplay\":\"Any\"},";
		foreach ($networklist as $id=>$network)
		{
			$json.= "{\"optionValue\":\"$id\", \"optionDisplay\":\"$network\"},";
		}
		$json = substr($json, 0, -1); // remove final comma		
		$json.= "]";
		header("Expires: 0");
		header("Content-Type: application/json");
		
		echo $json;
	}

	public function getcountrycodeAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$country=$this->getRequest()->getParam('id');
		$countries=new Countries();
		$row=$countries->find($country);
		echo $row[0]->dialingcode;
	}
	
	public function listhandsetsajaxAction()
	{
		$this->_helper->layout->disableLayout();
		
		// Uh oh where is this used? TheMobController has the method where you can pass this in. 
		//$whereClause=$this->getRequest()->getParam('whereClause');
		
		$whereClause="regdevices.idtester=".$this->view->currentUser->id;
		//$this->view->whereClause=mysql_real_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly

		$regdevices=new Regdevices();
		$this->view->numberOfPages=((int)($regdevices->distinctDevicesCount($whereClause)/$rowsPerPage))+1; //  Get total rows and calculate number of pages
		$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	}

	
	public function addhandsetsajaxAction()
	{
		$this->_helper->layout->disableLayout();
		if($network=$this->getRequest()->getParam('network')) {
			if($model=$this->getRequest()->getParam('model')) 
			{	
				$regdevices=new Regdevices();
				$regdevices->insert(array('idtester'=>$this->view->currentUser->id, 'idnetwork'=>$network, 'deviceatlasid'=>$model));
				
				$whereClause="regdevices.idtester=".$this->view->currentUser->id;
				//$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
				$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
				$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
				$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly
				$this->view->numberOfPages=((int)($regdevices->distinctDevicesCount($whereClause)/$rowsPerPage))+1; //  Get total rows and calculate number of pages
				$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
			
			}
			elseif($manualid=$this->getRequest()->getParam('manualid'))
			{
				// hopefully we have the make and model from the text boxes
				$regdevices=new Regdevices();
				$regdevices->insert(array('idtester'=>$this->view->currentUser->id, 'idnetwork'=>$network, 'deviceatlasid'=>0, 'manualid'=>$manualid));

				$whereClause="regdevices.idtester=".$this->view->currentUser->id;
				//$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
				$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
				$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
				$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly
				$this->view->numberOfPages=((int)($regdevices->distinctDevicesCount($whereClause)/$rowsPerPage))+1; //  Get total rows and calculate number of pages
				$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
			}
		}

		else{
			$this->view->error = "Please enter all the details (network, carrier, handset and model) to add the device";
		}
	}
	
	
	public function removehandsetsajaxAction()
	{
		$this->_helper->layout->disableLayout();
		
		if($id=$this->getRequest()->getParam('id'))
		{
			$regdevices=new Regdevices();
			$row = $regdevices->fetchRow('idregdevice = ' . $id);
			
			if(isset(Zend_Registry::get('defSession')->currentUser) && $row && ($row->idtester == Zend_Registry::get('defSession')->currentUser->id || Zend_Registry::get('defSession')->currentUser->isAdmin )){
					$regdevices->removeDevice($id);				
			}
		
		}
		
		
		$whereClause="regdevices.idtester=".$this->view->currentUser->id;
		//$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly

		$regdevices=new Regdevices();
		$this->view->numberOfPages=((int)($regdevices->distinctDevicesCount($whereClause)/$rowsPerPage))+1; //  Get total rows and calculate number of pages
		$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	}
	
	public function addmacidajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$doit=0;
		$regdevices=new Regdevices();
		if($this->getRequest()->getParam('id')&&$this->getRequest()->getParam('macID'))
		{
			$id=$this->getRequest()->getParam('id');
			$mac=$this->getRequest()->getParam('macID');
			$row = $regdevices->fetchRow('idregdevice = ' . $id);
			
			if(isset(Zend_Registry::get('defSession')->currentUser) && $row && ($row->idtester == Zend_Registry::get('defSession')->currentUser->id || Zend_Registry::get('defSession')->currentUser->isAdmin ))
			{
				$data=array('mac_id'=>$mac);
				$regdevices->updateDevice($id, $data);			
			}
		
		}
		
		
		$whereClause="regdevices.idtester=".$this->view->currentUser->id;
		//$this->view->whereClause=mysql_escape_string(stripslashes($whereClause)); // pain in the arse getting the escaping just right here;
		$this->view->orderBy = $orderBy = $this->getRequest()->getParam('orderBy');
		$rowsPerPage=12; // This is currently hardcoded. Eventually we should try to make these tables more generic and configurable
		$this->view->pageNumber=$pageNumber= $this->getRequest()->getParam('pageNumber'); // The view needs access to the page number to render the table properly

		$this->view->numberOfPages=((int)($regdevices->distinctDevicesCount($whereClause)/$rowsPerPage))+1; //  Get total rows and calculate number of pages
		$this->view->handsetList=$regdevices->listRegdevices($whereClause, $orderBy, $pageNumber,$rowsPerPage); 
	}

	public function addhandsetsprojectajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params
		if(isset($parameters['id'])){
			$idproject = $parameters['id'];
		}else{
			$idproject = Zend_Registry::get('defSession')->project->idproject;
		}
		
		$supporteddevices=new SupportedDevices();
		
		if(isset($parameters['platform']) && isset($parameters['vendor'])&& isset($parameters['model']))
		{
			$platform=$parameters['platform'];
			$vendor=$parameters['vendor'];
			$model=$parameters['model'];
			$supporteddevices->addSupportedDevice($idproject, $platform, $vendor, $model, '');
		}
		$this->view->handsettable=$supporteddevices->getSupportedDevices($idproject);
	}

	public function addnetworksprojectajaxAction()
	{
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params
		
		if(isset($parameters['id'])){
			$idproject = $parameters['id'];
		}else{
			$idproject = Zend_Registry::get('defSession')->project->idproject;
		}
		
		$supportednetworks=new SupportedNetworks();
		
		if(isset($parameters['country']) && isset($parameters['network']))
		{
			$country=$parameters['country'];
			$network=$parameters['network'];
			$supportednetworks->addSupportedNetwork($idproject, $country, $network);
		}
		$this->view->networktable=$supportednetworks->getSupportedNetworks($idproject);
	}

	public function removehandsetprojectAction()
	{
	
		$this->_helper->layout->disableLayout();
		$supporteddevices=new SupportedDevices();
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params
		
		if(isset($parameters['id'])){
			$idproject = $parameters['id'];
		}else{
			$idproject = Zend_Registry::get('defSession')->project->idproject;
		}
		
		if(isset($parameters['platform']) && isset($parameters['vendor'])&& isset($parameters['model']))
		{
			$platform=$parameters['platform'];
			$vendor=$parameters['vendor'];
			$model=$parameters['model'];
			$supporteddevices->removeSupportedDevice($idproject, $platform, $vendor, $model);
		
		}
		$this->view->handsettable=$supporteddevices->getSupportedDevices($idproject);
	
	}
	
	public function removenetworkprojectAction()
	{
		$this->_helper->layout->disableLayout();
		$supportednetworks=new SupportedNetworks();
		$parameters = $this->_getAllParams();//get all passed in params
		
		if(isset($parameters['id'])){
			$idproject = $parameters['id'];
		}else{
			$idproject = Zend_Registry::get('defSession')->project->idproject;
		}
		
		if(isset($parameters['country']) && isset($parameters['network']))
		{
			$country=$parameters['country'];
			$network=$parameters['network'];
			$supportednetworks->removeSupportedNetwork($idproject, $country, $network);
		}
		$this->view->networktable=$supportednetworks->getSupportedNetworks($idproject);
	
	}
	
	
	// Deprecated moved to /app/contacts/list
	public function listcontactsAction()
	{
		$this->_helper->layout->disableLayout();
		$contacts=new Contacts();
		$this->view->contacts= $contacts->listContactsByUser($this->view->currentUser->id);
	}

	/*
	* This works only for the image upload to user profile
	* 
	* Images will have to be pushed in the
	* 
	*/
	public function uploadimageAction()
	{ 
	
		$this->_helper->layout->disableLayout();
		
		if (isset($_FILES['projImg']))
		{
			$tempName = $_FILES['projImg']['tmp_name'];
				
			$randName = time().mt_rand(100, 999);
				
			$extension = strtolower(strrchr($_FILES['projImg']['name'], '.'));
			
			$randName.= ".jpg";
				
			$tempImage['basefilename'] = $randName;
		
			try{
				$tempImage['mimetype'] = $this->getMimeType($extension);
			}catch(Exception $e){
				$tempImage['mimetype'] = $extension;
			}
		
			$fileData =  (file_get_contents($_FILES['projImg']['tmp_name']));
			$fileSize = getimagesize($_FILES['projImg']['tmp_name']);
		
			$imageType = exif_imagetype($tempName);
			if($imageType == false || ($imageType != IMAGETYPE_GIF && $imageType != IMAGETYPE_JPEG && $imageType != IMAGETYPE_PNG))
			{
				throw new Exception('Only png, gif or jpeg images are allowed.');
			}
		
				
			// Resize the image
			switch(image_type_to_extension($imageType,false)){
				case "gif":
					$image = imagecreatefromgif($tempName);
					break;
				case "jpeg":
					$image = imagecreatefromjpeg($tempName);
					break;
				case "png":
					$image = imagecreatefrompng($tempName);
					break;
			}
				
			list($w, $h) = getimagesize($tempName);
				
			if($w > $h){
				$ratio = 100 / $w;
				$width = 100;
				$height = (int) ($h * $ratio);
			}else{
				$ratio = 100 / $h;
				$height = 100;
				$width = (int) ($w * $ratio);
			}
				
				
			$newimage = imagecreatetruecolor($width, $height);
				
			$tempDir = "$_SERVER[DOCUMENT_ROOT]/uploads/";

			imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $w, $h);
				
			imagejpeg($newimage,$tempDir.$randName ,100);
				
			$newData = file_get_contents($tempDir.$randName);
				
			imagedestroy($image);
			unlink($tempDir.$randName);

			$this->s3->putObject($this->imagebucket.'/'.$randName, $newData,
			array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
			Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));
				
			$imageurl = $this->imageurl.$this->imagebucket."/".$randName;

			$projectFiles = new ProjectFiles();
			$fileid=$projectFiles->addFileToProject(0, $tempImage['basefilename'], $tempImage['mimetype'],$fileSize[3],'', "profImg", "0");
				
			$projectFiles->updateProjectFile($fileid, array("physical_filename"=>$imageurl));
			
			$tempImage['imageurl'] = $imageurl;
			$this->view->imageurl = $imageurl;
			
			// Write the image URL to the database here
			$profile= new ProfileContactInfo();
			$profile->updateProfile($this->view->currentUser->id, array('image'=>$imageurl));
		}
		
		
		
	}

	
	public function uploadprojectimageAction()
	{

		$this->_helper->layout->disableLayout();
	
		if(isset($parameters['id'])){
			$idproject = $parameters['id'];
		}else{
			$idproject = Zend_Registry::get('defSession')->project->idproject;
		}
		
		
		if (isset($_FILES['projImg']))
		{
				
			$tempName = $_FILES['projImg']['tmp_name'];
	
			$randName = time().mt_rand(100, 999);
	
			$extension = strtolower(strrchr($_FILES['projImg']['name'], '.'));
				
			$randName.= ".jpg";
	
			$tempImage['basefilename'] = $randName;
	
			try{
				$tempImage['mimetype'] = $this->getMimeType($extension);
			}catch(Exception $e){
				$tempImage['mimetype'] = $extension;
			}
	
			$fileData =  (file_get_contents($_FILES['projImg']['tmp_name']));
			$fileSize = getimagesize($_FILES['projImg']['tmp_name']);
	
			$imageType = exif_imagetype($tempName);
			if($imageType == false || ($imageType != IMAGETYPE_GIF && $imageType != IMAGETYPE_JPEG && $imageType != IMAGETYPE_PNG))
			{
				throw new Exception('Only png, gif or jpeg images are allowed.');
			}
	
	
			// Resize the image
			switch(image_type_to_extension($imageType,false)){
				case "gif":
					$image = imagecreatefromgif($tempName);
					break;
				case "jpeg":
					$image = imagecreatefromjpeg($tempName);
					break;
				case "png":
					$image = imagecreatefrompng($tempName);
					break;
			}
	
			list($w, $h) = getimagesize($tempName);
	
			if($w > $h){
				$ratio = 100 / $w;
				$width = 100;
				$height = (int) ($h * $ratio);
			}else{
				$ratio = 100 / $h;
				$height = 100;
				$width = (int) ($w * $ratio);
			}
	
	
			$newimage = imagecreatetruecolor($width, $height);
	
			$tempDir = "$_SERVER[DOCUMENT_ROOT]/uploads/";
	
	
			imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $w, $h);
	
			imagejpeg($newimage,$tempDir.$randName ,100);
	
			$newData = file_get_contents($tempDir.$randName);
	
			imagedestroy($image);
			unlink($tempDir.$randName);
				
			$this->s3->putObject($this->imagebucket.'/'.$randName, $newData,
			array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
			Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));

			$imageurl = $this->imageurl.$this->imagebucket."/".$randName;

			$projectFiles = new ProjectFiles();
			
			// replace if necessary (Only one profile image)
			$currentImages = $projectFiles->getFilesForProjectByCategory($idproject,"projImg");
			foreach($currentImages as $image){
				$projectFiles->removeFileFromProject($image['idfiles']);
			}
			
			$fileid=$projectFiles->addFileToProject($idproject, $tempImage['basefilename'], $tempImage['mimetype'],$fileSize[3],'', "projImg", "0");
	
			$projectFiles->updateProjectFile($fileid, array("physical_filename"=>$imageurl));
				
			$tempImage['imageurl'] = $imageurl;
			$this->view->imageurl = $imageurl;
				
			// Write the image URL to the database here
			$projects = new Projects();
			
			$projectData = array(
				'ImagePath'=>$imageurl,
			);
			
			$projects->updateProject($idproject,$projectData );
			
		}
	
	
	
	}
	

	/*
	 * This action does not store the file as a ProjectFile. Only sets some session variables so that it can be stored later. 
	 * Only should be used when there is no project id assigned to the file yet. 
	 *
	 * I wish you hadn't fucked with this. It used to work perfectly well. Now it doesn't.
	 * 
	 * 
	 */
	public function uploadtemporaryAction()
	{

		$parameters = $this->_getAllParams();//get all passed in params
		
		$this->_helper->layout->disableLayout();
		if (isset($_FILES['projImg']))
		{

			$tempName = $_FILES['projImg']['tmp_name'];
			
			$randName = time().mt_rand(100, 999);						
			
			$extension = strtolower(strrchr($_FILES['projImg']['name'], '.'));
			//$randName.= $extension;
			$randName.= ".jpg";
			
			$tempImage['basefilename'] = $randName;
	
			try{
				$tempImage['mimetype'] = $this->getMimeType($extension);
			}catch(Exception $e){
				$tempImage['mimetype'] = $extension;
			}
				
			$fileData =  (file_get_contents($_FILES['projImg']['tmp_name']));
			$fileSize = getimagesize($_FILES['projImg']['tmp_name']);			
						
			$imageType = exif_imagetype($tempName);
			if($imageType == false || ($imageType != IMAGETYPE_GIF && $imageType != IMAGETYPE_JPEG && $imageType != IMAGETYPE_PNG))
			{
				throw new Exception('Only png, gif or jpeg images are allowed.');
			}
	
			
			// Resize the image
			switch(image_type_to_extension($imageType,false)){
				case "gif":
					$image = imagecreatefromgif($tempName);
					break;
				case "jpeg":
					$image = imagecreatefromjpeg($tempName);
					break;
				case "png":
					$image = imagecreatefrompng($tempName);
					break;
			}
			
			list($w, $h) = getimagesize($tempName);
			
			if($w > $h){
				$ratio = 100 / $w; 
				$width = 100; 
				$height = (int) ($h * $ratio);
			}else{
				$ratio = 100 / $h; 
				$height = 100; 
				$width = (int) ($w * $ratio);
			}
			
			
			$newimage = imagecreatetruecolor($width, $height);
			
			$tempDir = "$_SERVER[DOCUMENT_ROOT]/uploads/";
			
			
			imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $w, $h);
			
			imagejpeg($newimage,$tempDir.$randName ,100);
			
			$newData = file_get_contents($tempDir.$randName);
			
			imagedestroy($image);
			unlink($tempDir.$randName);
			
			$this->s3->putObject($this->imagebucket.'/'.$randName, $newData,
			    array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
			          Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));


			$imageurl = $this->imageurl.$this->imagebucket."/".$randName;
						
			$tempImage['imageurl'] = $imageurl; 
					
			Zend_Registry::get('defSession')->tempimage=$tempImage;
			$this->view->imageurl = $imageurl;
				    
		}
	
	}
	
	/*
	 * Upload to s3 and resizes an image to 100x100
	 * 
	 * returns the url of the image
	 */
	public function uploadavatarAction(){

		$parameters = $this->_getAllParams();//get all passed in params
		
		$this->_helper->layout->disableLayout();
		if (isset($_FILES['Filedata']))
		{

			$tempName = $_FILES['Filedata']['tmp_name'];
			
			$randName = time().mt_rand(100, 999);						
			
			$extension = strtolower(strrchr($_FILES['Filedata']['name'], '.'));
			//$randName.= $extension;
			$randName.= ".jpg";
			
			$tempImage['basefilename'] = $randName;
	
			try{
				$tempImage['mimetype'] = $this->getMimeType($extension);
			}catch(Exception $e){
				$tempImage['mimetype'] = $extension;
			}
				
			$fileData =  (file_get_contents($_FILES['Filedata']['tmp_name']));
			$fileSize = getimagesize($_FILES['Filedata']['tmp_name']);			
						
			$imageType = exif_imagetype($tempName);
			if($imageType == false || ($imageType != IMAGETYPE_GIF && $imageType != IMAGETYPE_JPEG && $imageType != IMAGETYPE_PNG))
			{
				throw new Exception('Only png, gif or jpeg images are allowed.');
			}
	
			
			// Resize the image
			switch(image_type_to_extension($imageType,false)){
				case "gif":
					$image = imagecreatefromgif($tempName);
					break;
				case "jpeg":
					$image = imagecreatefromjpeg($tempName);
					break;
				case "png":
					$image = imagecreatefrompng($tempName);
					break;
			}
			
			list($w, $h) = getimagesize($tempName);
			
			if($w > $h){
				$ratio = 100 / $w; 
				$width = 100; 
				$height = (int) ($h * $ratio);
			}else{
				$ratio = 100 / $h; 
				$height = 100; 
				$width = (int) ($w * $ratio);
			}
			
			
			$newimage = imagecreatetruecolor($width, $height);
			
			imagecopyresampled($newimage, $image, 0, 0, 0, 0, $width, $height, $w, $h);
			
			$tempDir = "$_SERVER[DOCUMENT_ROOT]/uploads/";
			
			imagejpeg($newimage,$tempDir.$randName ,100);
			
			$newData = file_get_contents($tempDir.$randName);
			
			imagedestroy($image);
			unlink($tempDir.$randName);

			$this->s3->putObject($this->imagebucket.'/'.$randName, $newData,
				array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
					Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));


			$imageurl = $this->imageurl.$this->imagebucket."/".$randName;

			$tempImage['imageurl'] = $imageurl; 
			$this->view->imageurl = $imageurl;
				    
		}        
	
		
	}
	
	
	
	/*
	 * File System Below Here
	 */
	public function uploadAction()
	{
		$this->_helper->layout->disableLayout();
		
	
		//todo: check mime types to avoid fake extensions
		$allowedfiletypes=array(
							'survey' => array(".csv",".xls",".xlsx",".txt"),
							'surveyScript' => array(".csv",".xls",".xlsx",".txt"),
							'uploadPlan' => array(".pdf",".doc",".xls",".docx",".xlsx",".txt"),
							'testOutput' => array(".pdf",".doc",".xls",".docx",".xlsx",".txt"),
							'genFiles' => array(".jar",".jad",".sis",".sisx",".apk",".cab",".zip",".rar"),
							'ndaFile' => array(".pdf",".doc",".xls",".docx",".xlsx",".txt",".zip",".rar"),
							'screenShots' => array(".gif",".jpg",".jpeg",".png"),
							'documentation' => array(".pdf",".doc",".xls",".docx",".xlsx",".txt",".zip",".rar"),
							'dev' => array(".jar",".jad",".sis", ".sisx", ".apk",".cab",".zip",".rar"),
							'projImg' => array(".gif",".jpg",".jpeg",".png"),
							'tsc' => array(".pdf",".doc",".xls",".docx",".xlsx",".rtf",".txt",".zip",".rar",".jpg",".jpeg",".gif",".png"),
							'wal' => array(".pdf",".doc",".xls",".docx",".xlsx",".rtf",".txt",".zip",".rar",".jpg",".jpeg",".gif",".png")
				
							); // This validation has already been done on the client side but better safe than sorry
	
		$parameters = $this->_getAllParams();//get all passed in params
		
		try{
		
	
			if(isset($parameters['id'])){
				$idproject = $parameters['id'];
			}else{
				$idproject = Zend_Registry::get('defSession')->project->idproject;
			}
			
			$projects = new Projects();
			$project = $projects->getProject($idproject);
		
			if(!isset($project))
				throw new Exception("Invalid project specified.");
		
		
			if(($filecontrolname=$this->getRequest()->getParam('file')))
			{
				if(substr($filecontrolname, 0, 3)=='dev')
				{
					$match='dev';
				}
				elseif(substr($filecontrolname, 0, 3)=='tsc')
				{
					$match='tsc';
				}
				else
				{
					$match=$filecontrolname;
				}
				if (isset($allowedfiletypes[$match])) // check that the file control name is valid
				{
					$extension= strtolower(strrchr($_FILES[$filecontrolname]['name'],'.'));

					//if (in_array($extension, $allowedfiletypes[$match]))
					//{
															
							$tempName = $_FILES[$filecontrolname]['tmp_name'];
							$randName = time().mt_rand(100, 999).$extension;
				
	
							try{
								$mimeType = $this->getMimeType(substr($extension,1));
							}catch(Exception $e){
								$mimeType = substr($extension,1);
							}
						
						    $fileData = file_get_contents($_FILES[$filecontrolname]['tmp_name']);
							$fileSize = filesize($_FILES[$filecontrolname]['tmp_name']);			
																	
							$baseFilename = basename( $_FILES[$filecontrolname]['name']);
					
							$projectFiles = new ProjectFiles();
								
							$this->s3->putObject($this->securebucket.'/'.$idproject.'/'.$baseFilename, $fileData,
							array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
							Zend_Service_Amazon_S3::S3_ACL_PRIVATE));
								
							$fileurl = $this->imageurl.$this->securebucket."/".$idproject."/".$baseFilename;

							$fileid=$projectFiles->addFileToProject($idproject, $baseFilename, $mimeType,$fileSize,'', $filecontrolname, "0");
							
							$projectFiles->updateProjectFile($fileid, array("physical_filename"=>$fileurl));
							 
							
					
					//}else{
					//	$this->view->error="That filetype is not supported at this time";
					//}
					
					$this->view->filecontrolname=$filecontrolname;
	
					$files = new ProjectFiles();
					$this->view->projectFiles=$files->getFilesForProjectByCategory($idproject,$filecontrolname);
					
				}else{
					$this->view->error="Unknown file control type";
				}
			}
		}catch(Exception $e){
		//	echo "exception:".$e->getMessage();
			$this->view->error = "We are unable to upload that file at this time";
		}
	}
	
	
	public function changevisibilityAction(){
		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params
		
		try{
		
			if(isset($parameters['id']))
			{
				$files = new ProjectFiles();		
				$projectFile=$files->getFile($parameters['id']);
				
				$projects = new Projects(); 
				$project = $projects->getProject($projectFile['idproject']);
							
				
				if( Zend_Registry::get('defSession')->currentUser->id == $project['iddeveloper'] || ( Zend_Registry::get('defSession')->currentUser->isAdmin)   ){
					$files->changePermission($projectFile['idfiles'],($projectFile['permissions'] =="0")?"1":"0" );
				}else{
					$this->view->error = "You do not have sufficient privileges to modify that file";
				}


				if(isset($parameters['fcn']) && isset($projectFile))
				{		
					$this->view->projectFiles=$files->getFilesForProjectByCategory($projectFile['idproject'],$parameters['fcn']);				
					$this->view->filecontrolname=$parameters['fcn'];
				}
				else
				{
					$this->view->projectFiles=$files->getAllFilesForProject(Zend_Registry::get('defSession')->project->idproject);
				}

			}
			
		}catch(Exception $e){
			$this->view->error = "We are unable to change the settings on that view at this time";
		}
	}
	
	public function deleteAction(){

		$this->_helper->layout->disableLayout();
		$parameters = $this->_getAllParams();//get all passed in params
		
		
		try{
		
			if(isset($parameters['id']))
			{
				$files = new ProjectFiles();		
				$projectFile=$files->getFile($parameters['id']);
				
				$projects = new Projects(); 
				$project = $projects->getProject($projectFile['idproject']);
							
				
				if( Zend_Registry::get('defSession')->currentUser->id == $project['iddeveloper'] || ( Zend_Registry::get('defSession')->currentUser->isAdmin)   ){
					$files->removeFileFromProject($projectFile['idfiles']);
				}else{
					$this->view->error = "You do not have sufficient privileges to modify that file";
				}


				if(isset($parameters['fcn']) && isset($projectFile))
				{		
					$this->view->projectFiles=$files->getFilesForProjectByCategory($projectFile['idproject'],$parameters['fcn']);				
					$this->view->filecontrolname=$parameters['fcn'];
				}
				else
				{
					$this->view->projectFiles=$files->getAllFilesForProject(Zend_Registry::get('defSession')->project->idproject);
				}

			}
			
		}catch(Exception $e){
			$this->view->error = "We are unable to delete the file at this time";
		}
		
	}
	
	/*
	 * Simply returns a list of the files for a filecontrolname 
	 */
	public function listfilesAction(){
		
		$this->_helper->layout->disableLayout();
		
		$parameters = $this->_getAllParams();//get all passed in params

		$files = new ProjectFiles();
		
		if(isset($parameters['id']))
		{
			$id = $parameters['id'];
		}elseif (isset(Zend_Registry::get('defSession')->project->idproject)) {
			$id = Zend_Registry::get('defSession')->project->idproject;
		}
		
		
		
		if(isset($id)){
			if(isset($parameters['fcn']) )
			{		
				$this->view->projectFiles=$files->getFilesForProjectByCategory($id,$parameters['fcn']);				
				$this->view->filecontrolname=$parameters['fcn'];
			}
			else
			{
				$this->view->projectFiles=$files->getAllFilesForProject($id);
			}
		}else{
			$this->view->error = "No Files Selected";
		}
		
	}
	
	
	protected function getMimeType($ext){
	

		
		$mimetypes = array(
		 "doc" => "application/msword", 
         "pdf" => "application/pdf", 
         "bmp" => "image/bmp", 
         "gif" => "image/gif", 
         "jpeg" => "image/jpeg", 
         "jpg" => "image/jpeg", 
         "jpe" => "image/jpeg", 
         "png" => "image/png", 
         "txt" => "text/plain", 
     	);
		
		
	  if (isset($mimetypes[$ext])) { 
         return $mimetypes[$ext]; 
    
      } else { 
         return 'application/octet-stream'; 
      }
		
	}
	
	public function showanswergroupAction()
	{
		$this->_helper->layout->disableLayout();
		$answer = new Answer();
		$whereClause=$this->getRequest()->getParam('whereClause');
		$this->view->groupData=$answer->getAnswersByGroup($whereClause);
	}
	
	public function showsurveyquestionsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->view->headScript()->appendFile('/js/survey/listanswers.js');
		$surveyQuestion = new SurveyQuestion();
		$parameters = $this->_getAllParams();
		$idsurvey=$parameters['idsurvey'];
		$this->view->idsurvey=$idsurvey;
		if(isset($parameters['add']))
		{
			$answer = new Answer();
			$question = new Question();
			$text = $parameters['text'];
			$mandatory = $parameters['mandatory'];
			$group = $parameters['answergroup'];
			if($group=='Entered Text')
			{
				$type=2;
				$answerArray= array();
			}
			else
			{
				$type=1;
				$answerArray=$answer->getAnswersByGroup($group);
			}
			$custom=1;
			$multiselect=0;
			$idquestion=$question->addNewQuestion($text, $answerArray, $type, $mandatory, $multiselect, $custom);
			$surveyQuestion->addQuestionToSurvey($idsurvey, $idquestion);
		}
		elseif(isset($parameters['remove']))
		{
			$idsurvey_question = $parameters['remove'];
			$surveyQuestion->removeQuestionFromSurvey($idsurvey_question);
		}
		
		$this->view->surveyQuestionList = $surveyQuestion->getQuestionsBySurvey($idsurvey,1);	
	}
	
	public function addsurveyquestionAction()
	{
		$this->_helper->layout->disableLayout();
		$surveyQuestion = new SurveyQuestion();
		$answer = new Answer();
		$question = new Question();
		
		$parameters = $this->_getAllParams();
		$idsurvey=$parameters['idsurvey'];		
		$text = $parameters['text'];
		$mandatory = $parameters['mandatory'];
		$group = $parameters['answergroup'];
		if($group=='Entered Text')
		{
			$type=2;
			$answerArray= array();
		}
		else
		{
			$type=1;
			$answerArray=$answer->getAnswersByGroup($group);
		}
		$custom=1;
		$multiselect=0;
		$idquestion=$question->addNewQuestion($text, $answerArray, $type, $mandatory, $multiselect, $custom);
		$surveyQuestion->addQuestionToSurvey($idsurvey, $idquestion);
		
	}
	
	
}