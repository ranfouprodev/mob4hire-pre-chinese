<?php

class Profile_IndexController extends Zend_Controller_Action
{
	function preDispatch()
	{
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
			$this->_redirect('user/login');
		}
	}
    
	public function indexAction()
	{
 
	}
    
	public function editAction()
	{
		$profileTable = new Profiles();
		$this->view->editForm = $this->getProfileForm($profileTable);
	}
	
	private function getProfileForm(Profiles $profiles)
	{
		$users= new Users();
		$form = new Zend_Form();
 
		$form->setAction('/profile/index/edit')->setMethod('post')->setAttrib('id', 'editprofile');
 
		$filterTrim = new Zend_Filter_StringTrim();
		$validatorNotEmpty = new Zend_Validate_NotEmpty();
		$validatorNotEmpty->setMessage(__('This field is required, you cannot leave it empty'));
 
		$firstName = new Zend_Form_Element_Text('firstname');        
		$firstName->setLabel('First Name')->addFilter($filterTrim);
		$form->addElement($firstName);
 
 		$lastName = new Zend_Form_Element_Text('lastname');        
		$lastName->setLabel('Last Name')->addFilter($filterTrim);
		$form->addElement($lastName);

/**
		* @todo Change this wired error messages to something more user friendly, or even use simple email regex matching validator
		*/
		$email = new Zend_Form_Element_Text('email');
		$validatorHostname = new Zend_Validate_Hostname();
		$validatorHostname->setMessages(
			array(
				Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => __("'%value%' appears to be an IP address, but IP addresses are not allowed"),
				Zend_Validate_Hostname::UNKNOWN_TLD             => __("'%value%' appears to be a DNS hostname but cannot match TLD against known list"),
				Zend_Validate_Hostname::INVALID_DASH            => __("'%value%' appears to be a DNS hostname but contains a dash (-) in an invalid position"),
				Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA => __("'%value%' appears to be a DNS hostname but cannot match against hostname schema for TLD '%tld%'"),
				Zend_Validate_Hostname::UNDECIPHERABLE_TLD      => __("'%value%' appears to be a DNS hostname but cannot extract TLD part"),
				Zend_Validate_Hostname::INVALID_HOSTNAME        => __("'%value%' does not match the expected structure for a DNS hostname"),
				Zend_Validate_Hostname::INVALID_LOCAL_NAME      => __("'%value%' does not appear to be a valid local network name"),
				Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => __("'%value%' appears to be a local network name but local network names are not allowed")
			)
		);
 
		$validatorEmail = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, false, $validatorHostname);
		$validatorEmail->setMessages(
			array(
				Zend_Validate_EmailAddress::INVALID            => __("'%value%' is not a valid email address"),
				Zend_Validate_EmailAddress::INVALID_HOSTNAME   => __("'%hostname%' is not a valid hostname for email address '%value%'"),
				Zend_Validate_EmailAddress::INVALID_MX_RECORD  => __("'%hostname%' does not appear to have a valid MX record for the email address '%value%'"),
				Zend_Validate_EmailAddress::DOT_ATOM           => __("'%localPart%' not matched against dot-atom format"),
				Zend_Validate_EmailAddress::QUOTED_STRING      => __("'%localPart%' not matched against quoted-string format"),
				Zend_Validate_EmailAddress::INVALID_LOCAL_PART => __("'%localPart%' is not a valid local part for email address '%value%'")
			)
		);
		$validatorUniqueEmail = new My_Validate_DbUnique($users, 'email');
		$validatorUniqueEmail->setMessage(__('This email address is already registered, please choose another one.'));
		$email->addValidator($validatorNotEmpty, true)->setRequired(true)->setLabel('Email Address')
			->addFilter($filterTrim)
			->addValidator($validatorEmail)
			->addValidator($validatorUniqueEmail)
			->addDecorator(array('ajaxDiv' => 'HtmlTag'), array('tag'=>'div', 'placement'=>'append', 'id'=>'email_help', 'class'=>'errors'));

		$form->addElement($email);
 		
		$password = new Zend_Form_Element_Password('password');
		$password->addValidator($validatorNotEmpty, true)->setRequired(true)->setLabel('Password')
			->addValidator(new Zend_Validate_StringLength(3));
		$form->addElement($password);
 
		$password2 = new Zend_Form_Element_Password('password2');
		$validatorPassword = new My_Validate_PasswordConfirmation('password');
		$validatorPassword->setMessage(__('Passwords do not match'));
 		$password2->setLabel('Confirm Password')->addValidator($validatorPassword);
		$form->addElement($password2);

		$form->addDisplayGroup(array('firstname', 'lastname', 'email'),'basic', array('legend' => 'Basic Information'));
		$basic = $form->getDisplayGroup('basic');
		$basic->setDecorators(array(
                    'FormElements',
                    'Fieldset',
                    array('HtmlTag',array('tag'=>'div','style'=>'width:50%;;float:left;'))
		));

		$form->addDisplayGroup(array('password', 'password2'), 'pwd', array('legend' => 'Password'));
		$pwd = $form->getDisplayGroup('pwd');
		$pwd->setDecorators(array(
			'FormElements',
			'Fieldset',
			array('HtmlTag',array('tag'=>'div','openOnly'=>true,'style'=>'width:48%;;float:right'))
		));

		$submit = new Zend_Form_Element_Submit('editprofile');
		$submit->setLabel('Save All');
		$form->addElement($submit);

		$cancel = new Zend_Form_Element_Submit('cancelprofile');
		$cancel->setLabel('Save All');
		$form->addElement($cancel);


		$form->addDisplayGroup(array('submit', 'cancel'), 'buttons', array());
		$buttons=$form->getDisplayGroup('buttons');
		$buttons->setDecorators(array(
			'FormElements',
			'Fieldset',
			array('HtmlTag',array('tag'=>'div','openOnly'=>true,'style'=>'width:98%;;float:left'))
		));
		return $form;

	}
}