var gettextParser = require("gettext-parser");
var fs = require('fs');
var googleTranslate = require('google-translate')('AIzaSyBbHAO98D4a7ZQV_wfkeBPaim0RZuMY85k');
var async = require('async');

var en_input = fs.readFileSync('en.po');
var zh_input = fs.readFileSync('zh_TW.po');

var en_po = gettextParser.po.parse(en_input);
var zh_po = gettextParser.po.parse(zh_input);

var en_translations = en_po.translations[Object.keys(en_po.translations)[0]];
var zh_translations = zh_po.translations[Object.keys(zh_po.translations)[0]];

var appendedString = '';
var asyncTasks = [];

for(var msg in en_translations){
  if(!zh_translations.hasOwnProperty(msg)){
    console.log("Missing translation:"+msg);
    var en_msgstr = en_translations[msg].msgstr;
    console.log("English Version:"+en_msgstr);

    queueTranslate(en_translations[msg]);
  }
}

function queueTranslate(msg){
  asyncTasks.push(function(callback){
      translate(msg,callback);
  });
}

function translate(msg,callback){
  console.log("Translating:"+msg.msgstr);
  //appendedString += "\n#Automated Translation\n"
  //appendedString += "msgid \""+msg.msgid+"\"\nmsgstr \""+msg.msgstr+"\"\n\n";
  //callback(null);
  googleTranslate.translate(msg.msgstr, 'zh', function(err, translation) {
    console.log("Translated:"+translation.translatedText);
    appendedString +="\n#Automated Translation\n"
    appendedString += "msgid \""+msg.msgid+"\"\nmsgstr \""+translation.translatedText+"\"\n\n";
    callback(null);
  });
}


async.series(asyncTasks,function(){
  fs.appendFile('zh_TW.po',appendedString,function(err){
    console.log("Unable to write file:"+err);
  });
});
