var gettextParser = require("gettext-parser");
var fs = require('fs');
var googleTranslate = require('google-translate')('AIzaSyBbHAO98D4a7ZQV_wfkeBPaim0RZuMY85k');
var async = require('async');

compile('en');
compile('zh_TW');

function compile(filename){
  var input = fs.readFileSync(filename+".po");
  var po = gettextParser.po.parse(input);

  var output = gettextParser.mo.compile(po);
  fs.writeFileSync(filename+".mo",output);

}
