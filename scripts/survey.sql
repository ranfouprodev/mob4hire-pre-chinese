--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey` (
  `idsurvey` int(11) NOT NULL AUTO_INCREMENT,
  `idproject` int(10) unsigned DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsurvey`),
  KEY `fk_survey_projects1` (`idproject`),
  CONSTRAINT `fk_survey_projects1` FOREIGN KEY (`idproject`) REFERENCES `projects` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey`
--

LOCK TABLES `survey` WRITE;
/*!40000 ALTER TABLE `survey` DISABLE KEYS */;
INSERT INTO `survey` VALUES (1,1,'Vanilla MobExperience Survey'),(2,599,'Mobster Profile Survey'),(3,556,'Evaluate our mobile site'),(4,NULL,'User Experience Demonstration');
/*!40000 ALTER TABLE `survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_question`
--

DROP TABLE IF EXISTS `survey_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_question` (
  `idsurvey_question` int(11) NOT NULL AUTO_INCREMENT,
  `idsurvey` int(11) DEFAULT NULL,
  `idquestion` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`idsurvey_question`),
  KEY `fk_survey_question_survey1` (`idsurvey`),
  KEY `fk_survey_question_question1` (`idquestion`),
  CONSTRAINT `fk_survey_question_question1` FOREIGN KEY (`idquestion`) REFERENCES `question` (`idquestion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_survey_question_survey1` FOREIGN KEY (`idsurvey`) REFERENCES `survey` (`idsurvey`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_question`
--

LOCK TABLES `survey_question` WRITE;
/*!40000 ALTER TABLE `survey_question` DISABLE KEYS */;
INSERT INTO `survey_question` VALUES (1,1,1,NULL,1),(2,1,2,NULL,1),(3,1,3,NULL,1),(4,1,4,NULL,1),(5,1,5,NULL,1),(6,1,6,NULL,1),(7,1,7,NULL,1),(8,1,8,NULL,1),(9,1,9,NULL,1),(10,1,10,NULL,1),(11,1,11,NULL,1),(12,1,12,NULL,1),(13,1,13,NULL,1),(14,1,14,NULL,1),(15,1,15,NULL,1),(16,1,16,NULL,1),(17,1,17,NULL,1),(18,1,18,NULL,1),(19,1,19,NULL,1),(20,1,20,NULL,1),(21,1,21,NULL,1),(22,1,22,NULL,1),(23,1,23,NULL,1),(24,1,24,NULL,1),(25,1,25,NULL,1),(26,1,26,NULL,1),(27,1,27,NULL,1),(28,1,28,NULL,1),(29,1,29,NULL,1),(30,1,30,NULL,1),(31,1,31,NULL,1),(32,1,32,NULL,1),(33,1,33,NULL,1),(34,1,34,NULL,1),(35,1,35,NULL,1),(36,1,36,NULL,1),(37,1,37,NULL,1),(38,1,38,NULL,1),(39,1,39,NULL,1),(40,1,40,NULL,1),(41,1,41,NULL,1),(42,1,42,NULL,1),(43,1,43,NULL,1),(44,1,44,NULL,1),(45,1,45,NULL,1),(46,1,46,NULL,1),(47,1,47,NULL,1),(48,1,48,NULL,1),(49,1,49,NULL,1),(50,1,50,NULL,1),(51,1,51,NULL,1),(52,1,52,NULL,1),(53,1,53,NULL,1),(54,1,54,NULL,1),(55,2,55,NULL,1),(56,2,56,NULL,1),(57,2,57,NULL,1),(58,2,58,NULL,1),(60,2,60,NULL,1),(61,2,61,NULL,1),(62,2,62,NULL,1),(63,2,63,NULL,1),(64,2,64,NULL,1),(66,2,66,NULL,1),(67,2,67,NULL,1),(68,2,68,NULL,1),(69,2,69,NULL,1),(70,2,70,NULL,1),(71,2,71,NULL,1),(72,2,72,NULL,1),(73,2,73,NULL,1),(74,3,1,NULL,1),(75,3,2,NULL,1),(76,3,3,NULL,1),(77,3,4,NULL,1),(78,3,5,NULL,1),(79,3,6,NULL,1),(80,3,7,NULL,1),(81,3,8,NULL,1),(82,3,9,NULL,1),(83,3,10,NULL,1),(84,3,11,NULL,1),(85,3,12,NULL,1),(86,3,13,NULL,1),(87,3,14,NULL,1),(88,3,15,NULL,1),(89,3,16,NULL,1),(90,3,17,NULL,1),(91,3,18,NULL,1),(92,3,19,NULL,1),(93,3,20,NULL,1),(94,3,21,NULL,1),(95,3,22,NULL,1),(96,3,23,NULL,1),(97,3,24,NULL,1),(98,3,25,NULL,1),(99,3,26,NULL,1),(100,3,27,NULL,1),(101,3,28,NULL,1),(102,3,29,NULL,1),(103,3,30,NULL,1),(104,3,31,NULL,1),(105,3,32,NULL,1),(106,3,33,NULL,1),(107,3,34,NULL,1),(108,3,35,NULL,1),(109,3,36,NULL,1),(110,3,37,NULL,1),(111,3,38,NULL,1),(112,3,39,NULL,1),(113,3,40,NULL,1),(114,3,41,NULL,1),(115,3,42,NULL,1),(116,3,43,NULL,1),(117,3,44,NULL,1),(118,3,45,NULL,1),(119,3,46,NULL,1),(120,3,47,NULL,1),(121,3,48,NULL,1),(122,3,49,NULL,1),(123,3,50,NULL,1),(124,3,51,NULL,1),(125,3,52,NULL,1),(126,3,53,NULL,1),(127,3,54,NULL,1),(128,4,4,NULL,1),(129,4,16,NULL,1);
/*!40000 ALTER TABLE `survey_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `idsurvey` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iduser` int(10) unsigned DEFAULT NULL,
  `survey_type` int(10) unsigned DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `description` text,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `nores` int(10) unsigned DEFAULT NULL,
  `survcost` int(10) unsigned DEFAULT NULL,
  `survpkg` int(10) unsigned DEFAULT NULL,
  `total` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idsurvey`),
  UNIQUE KEY `idsurvey` (`idsurvey`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `idanswer` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(45) DEFAULT NULL,
  `answergroup` varchar(45) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idanswer`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (0,'Dummy. Used for string responses','Entered Text',NULL),(1,'Just installed it; less than an hour','Length Of Use','3600'),(2,'Less than one day','Length Of Use','86400'),(3,'One day to less than one week','Length Of Use','604800'),(4,'One week to less than one month','Length Of Use','2419200'),(5,'One month to less than three months','Length Of Use','7257600'),(6,'Three months to less than six months - Six mo','Length Of Use','14515200'),(7,'A year to three years','Length Of Use','29030400'),(8,'Three years or more','Length Of Use','87091200'),(9,'I have not used this application','Length Of Use','0'),(10,'Beginner','User Level of Experience','1'),(11,'Average','User Level of Experience','2'),(12,'Proficient','User Level of Experience','3'),(13,'Expert','User Level of Experience','4'),(14,'0 - Extremely Dissatisfied','Satisfaction Level 0 to 10','0'),(15,'1','Satisfaction Level 0 to 10','1'),(16,'2','Satisfaction Level 0 to 10','2'),(17,'3','Satisfaction Level 0 to 10','3'),(18,'4','Satisfaction Level 0 to 10','4'),(19,'5 - Neither Satisfied nor Dissatisfied','Satisfaction Level 0 to 10','5'),(20,'6','Satisfaction Level 0 to 10','6'),(21,'7','Satisfaction Level 0 to 10','7'),(22,'8','Satisfaction Level 0 to 10','8'),(23,'9','Satisfaction Level 0 to 10','9'),(24,'10 - Extremely Satisfied','Satisfaction Level 0 to 10','10'),(25,'1 Star','Star Rating','1'),(26,'2 Stars','Star Rating','2'),(27,'3 Stars','Star Rating','3'),(28,'4 Stars','Star Rating','4'),(29,'5 Stars','Star Rating','5'),(30,'0 - Not at all Likely','Likelihood of Use 0 to 10','0'),(31,'1','Likelihood of Use 0 to 10','1'),(32,'2','Likelihood of Use 0 to 10','2'),(33,'3','Likelihood of Use 0 to 10','3'),(34,'4','Likelihood of Use 0 to 10','4'),(35,'5','Likelihood of Use 0 to 10','5'),(36,'6','Likelihood of Use 0 to 10','6'),(37,'7','Likelihood of Use 0 to 10','7'),(38,'8','Likelihood of Use 0 to 10','8'),(39,'9','Likelihood of Use 0 to 10','9'),(40,'10 - Extremely Likely','Likelihood of Use 0 to 10','10'),(41,'None','How Many 0 to 10','0'),(42,'1','How Many 0 to 10','1'),(43,'2','How Many 0 to 10','2'),(44,'3','How Many 0 to 10','3'),(45,'4','How Many 0 to 10','4'),(46,'5','How Many 0 to 10','5'),(47,'6','How Many 0 to 10','6'),(48,'7','How Many 0 to 10','7'),(49,'8','How Many 0 to 10','8'),(50,'9','How Many 0 to 10','9'),(51,'10','How Many 0 to 10','10'),(52,'More than 10','How Many 0 to 10','11'),(53,'G','Censor Rating','1'),(54,'PG','Censor Rating','2'),(55,'15+','Censor Rating','3'),(56,'18+','Censor Rating','4'),(57,'R18+','Censor Rating','5'),(58,'X18+','Censor Rating','6'),(59,'1 - Never','Frequency 1 to 5','1'),(60,'2 - Rarely','Frequency 1 to 5','2'),(61,'3 - Sometimes','Frequency 1 to 5','3'),(62,'4 - Often','Frequency 1 to 5','4'),(63,'5 - Always','Frequency 1 to 5','5'),(64,'1 - Strongly Disagree','Agree 1 to 5','1'),(65,'2 - Disagree','Agree 1 to 5','2'),(66,'3 - Neither Agree nor Disagree','Agree 1 to 5','3'),(67,'4 - Agree','Agree 1 to 5','4'),(68,'5 - Strongly Agree','Agree 1 to 5','5'),(69,'Don\'t know','Agree 1 to 5',NULL),(70,'0 - Hated it!','Rating 1 to 10','0'),(71,'1','Rating 1 to 10','1'),(72,'2','Rating 1 to 10','2'),(73,'3','Rating 1 to 10','3'),(74,'4','Rating 1 to 10','4'),(75,'5','Rating 1 to 10','5'),(76,'6','Rating 1 to 10','6'),(77,'7','Rating 1 to 10','7'),(78,'8','Rating 1 to 10','8'),(79,'9','Rating 1 to 10','9'),(80,'10 - Loved it!','Rating 1 to 10','10'),(81,'Male','Gender','1'),(82,'Female','Gender','2'),(83,'Rather not say','Gender',NULL),(84,'Confidential','Age',NULL),(85,'18-25','Age','1'),(86,'26-30','Age','2'),(87,'31-35','Age','3'),(88,'36-40','Age','4'),(89,'41-45','Age','5'),(90,'46-50','Age','6'),(91,'51-55','Age','7'),(92,'56-60','Age','8'),(93,'61-65','Age','9'),(94,'66-70','Age','10'),(95,'71-75','Age','11'),(96,'76-80','Age','12'),(97,'Over 80','Age','13'),(98,'HIgh School','Education','0'),(99,'Techincal Degree','Education','1'),(100,'2 Year Degree','Education','2'),(101,'4 Year Degree','Education','4'),(102,'Master\'s Degree','Education','6'),(103,'Doctorate Degree','Education','8'),(104,'No answer','Education',NULL),(105,'English','Language','1'),(106,'French','Language','2'),(107,'Spanish','Language','3'),(108,'Italian','Language','4'),(109,'Afar','Language','5'),(110,'Abkhazian','Language','6'),(111,'Afrikaans','Language','7'),(112,'Amharic','Language','8'),(113,'Arabic','Language','9'),(114,'Assamese','Language','10'),(115,'Aymara','Language','11'),(116,'Azerbaijani','Language','12'),(117,'Bashkir','Language','13'),(118,'Byelorussian','Language','14'),(119,'Bulgarian','Language','15'),(120,'Bihari','Language','16'),(121,'Bislama','Language','17'),(122,'Bengali','Language','18'),(123,'Tibetan','Language','19'),(124,'Breton','Language','20'),(125,'Catalan','Language','21'),(126,'Corsican','Language','22'),(127,'Czech','Language','23'),(128,'Welsh','Language','24'),(129,'Danish','Language','25'),(130,'German','Language','26'),(131,'Bhutani','Language','27'),(132,'Greek','Language','28'),(133,'Esperanto','Language','29'),(134,'Estonian','Language','30'),(135,'Basque','Language','31'),(136,'Persian','Language','32'),(137,'Finnish','Language','33'),(138,'Fiji','Language','34'),(139,'Faeroese','Language','35'),(140,'Frisian','Language','36'),(141,'Irish','Language','37'),(142,'Gaelic','Language','38'),(143,'Galician','Language','39'),(144,'Guarani','Language','40'),(145,'Gujarati','Language','41'),(146,'Hausa','Language','42'),(147,'Hindi','Language','43'),(148,'Croatian','Language','44'),(149,'Hungarian','Language','45'),(150,'Armenian','Language','46'),(151,'Interlinguae','Language','47'),(152,'Inupiak','Language','48'),(153,'Indonesian','Language','49'),(154,'Icelandic','Language','50'),(155,'Hebrew','Language','51'),(156,'Japanese','Language','52'),(157,'Yiddish','Language','53'),(158,'Javanese','Language','54'),(159,'Georgian','Language','55'),(160,'Kazakh','Language','56'),(161,'Greenlandic','Language','57'),(162,'Cambodian','Language','58'),(163,'Kannada','Language','59'),(164,'Korean','Language','60'),(165,'Kashmiri','Language','61'),(166,'Kurdish','Language','62'),(167,'Kirghiz','Language','63'),(168,'Latin','Language','64'),(169,'Lingala','Language','65'),(170,'Laothian','Language','66'),(171,'Lithuanian','Language','67'),(172,'Latvian Lettish','Language','68'),(173,'Malagasy','Language','69'),(174,'Maori','Language','70'),(175,'Macedonian','Language','71'),(176,'Malayalam','Language','72'),(177,'Mongolian','Language','73'),(178,'Moldavian','Language','74'),(179,'Marathi','Language','75'),(180,'Malay','Language','76'),(181,'Maltese','Language','77'),(182,'Burmese','Language','78'),(183,'Nauru','Language','79'),(184,'Nepali','Language','80'),(185,'Dutch','Language','81'),(186,'Norwegian','Language','82'),(187,'Occitan','Language','83'),(188,'Oromo Afan','Language','84'),(189,'Oriya','Language','85'),(190,'Punjabi','Language','86'),(191,'Polish','Language','87'),(192,'Pashto Pushto','Language','88'),(193,'Portuguese','Language','89'),(194,'Romanian','Language','92'),(195,'Russian','Language','93'),(196,'Kinyarwanda','Language','94'),(197,'Sanskrit','Language','95'),(198,'Sindhi','Language','96'),(199,'Sangro','Language','97'),(200,'Serbo-Croatian','Language','98'),(201,'Singhalese','Language','99'),(202,'Slovak','Language','100'),(203,'Slovanian','Language','101'),(204,'Samoan','Language','102'),(205,'Shona','Language','103'),(206,'Somali','Language','104'),(207,'Albanian','Language','105'),(208,'Serbian','Language','106'),(209,'Siswati','Language','107'),(210,'Sesotho','Language','108'),(211,'Sudanese','Language','109'),(212,'Swedish','Language','110'),(213,'Swahili','Language','111'),(214,'Tamil','Language','112'),(215,'Tegulu','Language','113'),(216,'Tajik','Language','114'),(217,'Thai','Language','115'),(218,'Tigrinya','Language','116'),(219,'Turkmen','Language','117'),(220,'Tagalog','Language','118'),(221,'Setswana','Language','119'),(222,'Tonga','Language','120'),(223,'Turkish','Language','121'),(224,'Tsonga','Language','122'),(225,'Tatar','Language','123'),(226,'Twi','Language','124'),(227,'Ukrainian','Language','125'),(228,'Urdu','Language','126'),(229,'Uzbek','Language','127'),(230,'Vietnamese','Language','128'),(231,'Volapuk','Language','129'),(232,'Wolof','Language','130'),(233,'Xhosa','Language','131'),(234,'Yoruba','Language','132'),(235,'Chinese','Language','133'),(236,'Zulu','Language','134'),(237,'Other','Language',NULL),(238,'No computer with internet connection','browser','1'),(239,'Internet Explorer','browser','2'),(240,'Opera','browser','3'),(241,'Firefox','browser','4'),(242,'Safari','browser','5'),(243,'Chrome','browser','6'),(244,'Not listed.','browser','7'),(245,'Functional','test_type','1'),(246,'Usability','test_type','2'),(247,'News','apps','1'),(248,'Medical','apps','2'),(249,'Reference','apps','3'),(250,'Productivity','apps','4'),(251,'Navigation','apps','5'),(252,'Health and fitness','apps','6'),(253,'Education','apps','7'),(254,'Weather','apps','8'),(255,'Business','apps','9'),(256,'Music','apps','10'),(257,'Finance','apps','11'),(258,'Sports','apps','12'),(259,'Travel','apps','13'),(260,'Utilities','apps','14'),(261,'Games','apps','15'),(262,'Social networking','apps','16'),(263,'Books','apps','17'),(264,'Entertainment','apps','18'),(265,'Lifestyle','apps','19'),(266,'Less than high school','education','1'),(267,'High school graduate','education','2'),(268,'Some college or university','education','3'),(269,'College graduate with a 2 year degree','education','4'),(270,'College graduate with a 4 year degree','education','5'),(271,'Advanced degree','education','6'),(272,'Not really','experience','1'),(273,'A little','experience','2'),(274,'Lots','experience','3'),(275,'I used to work in Q/A.','experience','4'),(276,'I work in Q/A right now.','experience','5'),(277,'Rather not say.','gender','1'),(278,'Male','gender','2'),(279,'Female','gender','3'),(280,'Rather not say.','martial_status','1'),(281,'Single / Never Married','martial_status','2'),(282,'Married','martial_status','3'),(283,'Divorced','martial_status','4'),(284,'Widowed','martial_status','5'),(285,'Separated','martial_status','6'),(286,'Rather not say.','children','1'),(287,'1','children','3'),(288,'2','children','4'),(289,'3','children','5'),(290,'4','children','6'),(291,'5 or more','children','7'),(292,'Rather not say.','annual_income','8'),(293,'$0 - $10,000','annual_income','9'),(294,'$10,001 - $23,000','annual_income','10'),(295,'$23,001 - $46,000','annual_income','11'),(296,'$46,001 - $69,000','annual_income','12'),(297,'$69,001 - $115,000','annual_income','13'),(298,'$115,001 - $161,000','annual_income','14'),(299,'$161,001 or more','annual_income','15'),(300,'Rather not say','mob_life','1'),(301,'Rarely use handset','mob_life','2'),(302,'It\'s part of my day.','mob_life','3'),(303,'Nervous when I don\'t have it','mob_life','4'),(304,'I love mobile!','mob_life','5'),(305,'I\'m the first to try everything new.','mob_life','6'),(306,'An integral part of job and home life','mob_life','7'),(307,'Web browsing','phone_use','1'),(308,'Games','phone_use','2'),(309,'E-mail','phone_use','3'),(310,'IM','phone_use','4'),(311,'Photography/photo sharing','phone_use','5'),(312,'Music and video','phone_use','6'),(313,'Texting/SMS','phone_use','7'),(314,'MMS','phone_use','8'),(315,'Check weather','phone_use','9'),(316,'Clock','phone_use','10'),(317,'Navigation/maps','phone_use','11'),(318,'Video calls','phone_use','12'),(319,'TV','phone_use','13'),(320,'Social networking','phone_use','14'),(321,'Books','phone_use','15'),(322,'Schedule/to do','phone_use','16'),(323,'Other','phone_use','17'),(324,'None','children','2');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `idbranch` int(11) NOT NULL AUTO_INCREMENT,
  `idsurvey_question` int(11) DEFAULT NULL,
  `idanswer` int(11) DEFAULT NULL,
  `branchto` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbranch`),
  KEY `fk_branch_survey_question1` (`idsurvey_question`),
  KEY `fk_branch_answer1` (`idanswer`),
  KEY `fk_branch_survey_question2` (`branchto`),
  CONSTRAINT `fk_branch_answer1` FOREIGN KEY (`idanswer`) REFERENCES `answer` (`idanswer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_branch_survey_question1` FOREIGN KEY (`idsurvey_question`) REFERENCES `survey_question` (`idsurvey_question`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_branch_survey_question2` FOREIGN KEY (`branchto`) REFERENCES `survey_question` (`idsurvey_question`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,1,9,51),(2,74,9,124);
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `idquestion` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `mandatory` tinyint(4) DEFAULT NULL,
  `multiselect` tinyint(4) DEFAULT NULL,
  `custom` int(1) DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idquestion`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'How long have you been using {APPLICATION NAME} on your mobile phone?',1,1,0,0,'Verification'),(2,'What is your level of expertise using {APPLICATION NAME}?',1,1,0,0,'Verification'),(3,'What is your level of expertise using {HANDSET MANUFACTURER & MODEL}?',1,1,0,0,'Verification'),(4,'Overall, how satisfied are you with {APPLICATION NAME}?',1,1,0,0,'Customer Satisfaction'),(5,'If {APPLICATION NAME} was available for download at an app store, how would you rate it?',1,1,0,0,'Customer Satisfaction'),(6,'If {APPLICATION NAME} was available for download at an app store, what would you say about it in the \'twitter-length\' user review?',2,1,0,0,'Customer Satisfaction'),(7,'If you were selecting a similar mobile application for the first time, how likely is it that you would choose {APPLICATION NAME}?',1,1,0,0,'Customer Satisfaction'),(8,'How likely are you to recommend {APPLICATION NAME} to your friends/colleagues?',1,1,0,0,'Customer Satisfaction'),(9,'How likely are you to use other applications that are created by the developers of {APPLICATION NAME}?',1,1,0,0,'Customer Satisfaction'),(10,'How likely are you to increase the frequency with which you use {APPLICATION NAME}?',1,1,0,0,'Customer Satisfaction'),(11,'How likely are you to stop using {APPLICATION NAME} on your mobile phone in the future?',1,1,0,0,'Customer Satisfaction'),(12,'How likely are you to use a similar application by a different company?',1,1,0,0,'Customer Satisfaction'),(13,'How many friends/colleagues have you recommended {APPLICATION NAME} to in the past 12 months?',1,1,0,0,'Customer Satisfaction'),(14,'If {APPLICATION NAME} were a movie, what age-appropriate rating would you give it?',1,1,0,0,'Customer Satisfaction'),(15,'How frequently does {APPLICATION NAME} crash on your mobile phone?',1,1,0,0,'Customer Satisfaction'),(16,'What is the most important area for improvement for this application?',2,1,0,0,'Customer Satisfaction'),(17,'{APPLICATION NAME} did what it is designed to do.',1,1,0,0,'Customer Experience: Overall'),(18,'{APPLICATION NAME} met my expectations.',1,1,0,0,'Customer Experience: Overall'),(19,'Why did you give these ratings for your Overall experience?',2,0,0,0,'Customer Experience: Overall'),(20,'{APPLICATION NAME} does not consume battery excessively.',1,1,0,0,'Customer Experience: Stability'),(21,'{APPLICATION NAME} behaved the same way each time I opened it.',1,1,0,0,'Customer Experience: Stability'),(22,'My mobile phone froze up when I used {APPLICATION NAME}.',1,1,0,0,'Customer Experience: Stability'),(23,'When {APPLICATION NAME} is running on my phone, I\'m able to use the camera, bluetooth devices, plugging in my handset, media player, etc ...',1,1,0,0,'Customer Experience: Stability'),(24,'Why did you give these ratings for the Stability of {APPLICATION NAME}?',2,0,0,0,'Customer Experience: Stability'),(25,'{APPLICATION NAME} installed easily on my mobile phone.',1,1,0,0,'Customer Experience: Application Launch'),(26,'{APPLICATION NAME} opened consistently.',1,1,0,0,'Customer Experience: Application Launch'),(27,'{APPLICATION NAME} was available immediately when I opened it.',1,1,0,0,'Customer Experience: Application Launch'),(28,'Why did you give these ratings for the installation and launching of {APPLICATION NAME}?',2,0,0,0,'Customer Experience: Application Launch'),(29,'{APPLICATION NAME} was easy to use.',1,1,0,0,'Customer Experience: User Interface Requirement'),(30,'The graphics on {APPLICATION NAME} were clear.',1,1,0,0,'Customer Experience: User Interface Requirement'),(31,'I became proficient with {APPLICATION NAME} quickly.',1,1,0,0,'Customer Experience: User Interface Requirement'),(32,'The output of {APPLICATION NAME} was easy to understand.',1,1,0,0,'Customer Experience: User Interface Requirement'),(33,'{APPLICATION NAME}\'s menu system was easy to understand.',1,1,0,0,'Customer Experience: User Interface Requirement'),(34,'Why did you give these ratings for the {APPLICATION NAME}\'s User Interface?',2,0,0,0,'Customer Experience: User Interface Requirement'),(35,'The text in {APPLICATION NAME} was clear and understandable.',1,1,0,0,'Customer Experience: Language'),(36,'The grammar and spelling of the text is correct.',1,1,0,0,'Customer Experience: Language'),(37,'Why did you give these ratings for the primary language in {APPLICATION NAME}?',2,0,0,0,'Customer Experience: Language'),(38,'I was able to perform functions with precision.',1,1,0,0,'Customer Experience: Usability'),(39,'{APPLICATION NAME} responded quickly to my commands.',1,1,0,0,'Customer Experience: Usability'),(40,'{APPLICATION NAME} did not interfere with other applications.',1,1,0,0,'Customer Experience: Usability'),(41,'Inputting data into {APPLICATION NAME} was easy.',1,1,0,0,'Customer Experience: Usability'),(42,'The {APPLICATION NAME}\'s documentation and help screens were useful.',1,1,0,0,'Customer Experience: Usability'),(43,'I could find help on {APPLICATION NAME} when I needed it.',1,1,0,0,'Customer Experience: Usability'),(44,'Making changes to {APPLICATION NAME} was easy to do.',1,1,0,0,'Customer Experience: Usability'),(45,'Why did you give these ratings for the usability of {APPLICATION NAME}?',2,0,0,0,'Customer Experience: Usability'),(46,'When {APPLICATION NAME} is running on my phone, I am able to make and get phone calls, and also able to send and receive messages such as SMS/MMS, Twitter feeds, email, etc...',1,1,0,0,'Customer Experience: Connectivity'),(47,'Limited wireless connectivity (two bars or less) did not affect {APPLICATION NAME}\'s performance.',1,1,0,0,'Customer Experience: Connectivity'),(48,'Why did you give these ratings for the {APPLICATION NAME}\'s connectivity?',2,0,0,0,'Customer Experience: Connectivity'),(49,'Overall, how did you like completing this survey?',1,1,0,0,'Survey About the Survey'),(50,'What could we do to improve the survey process?',2,1,0,0,'Survey About the Survey'),(51,'What is your gender?',1,1,0,0,'About You'),(52,'What is your age (in years)?',1,1,0,0,'About You'),(53,'What is the highest level of education that you attained?',1,1,0,0,'About You'),(54,'What is your primary language?',1,1,0,0,'About You'),(55,'Please select your country.',4,1,0,1,'Your handset'),(56,'Please select your mobile carrier / Network provider.',5,1,0,1,'Your handset'),(57,'Please select your device manufacturer.',6,1,0,1,'Your handset'),(58,'Please select your device model.',7,1,0,1,'Your handset'),(59,'Your handset\'s ID (UUID, IMEI etc.)',2,1,0,1,'Your handset'),(60,'Which desktop browsers do you use?',1,1,1,1,'Your browser'),(61,'What type of projects interest you?',1,1,1,1,'About You'),(62,'Which test categories interest you?',1,1,1,1,'About You'),(63,'Do you have past testing experience?',1,1,0,1,'About You'),(64,'What is your highest level of education',1,1,0,1,'About You'),(66,'Would you sign an NDA?',3,1,0,1,'About You'),(67,'What is your gender?',1,1,0,1,'About You'),(68,'Please enter your date of birth.',8,1,0,1,'About You'),(69,'What is your marital status?',1,1,0,1,'About You'),(70,'How many children under 18 do you have living with you.',1,1,0,1,'About You'),(71,'What is you total annual household income',1,1,0,1,'About You'),(72,'Which of these statements best describes your mobile lifestyle?',1,1,0,1,'About You'),(73,'Phone use other',1,1,1,1,'About You');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_answer` (
  `idquestion_answer` int(11) NOT NULL AUTO_INCREMENT,
  `idquestion` int(11) DEFAULT NULL,
  `idanswer` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`idquestion_answer`),
  KEY `fk_question_answer_question1` (`idquestion`),
  KEY `fk_question_answer_answer1` (`idanswer`),
  CONSTRAINT `fk_question_answer_answer1` FOREIGN KEY (`idanswer`) REFERENCES `answer` (`idanswer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_answer_question1` FOREIGN KEY (`idquestion`) REFERENCES `question` (`idquestion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=527 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
INSERT INTO `question_answer` VALUES (1,1,1,NULL),(2,1,2,NULL),(3,1,3,NULL),(4,1,4,NULL),(5,1,5,NULL),(6,1,6,NULL),(7,1,7,NULL),(8,1,8,NULL),(9,1,9,NULL),(10,2,10,NULL),(11,2,11,NULL),(12,2,12,NULL),(13,2,13,NULL),(14,3,10,NULL),(15,3,11,NULL),(16,3,12,NULL),(17,3,13,NULL),(18,4,14,NULL),(19,4,15,NULL),(20,4,16,NULL),(21,4,17,NULL),(22,4,18,NULL),(23,4,19,NULL),(24,4,20,NULL),(25,4,21,NULL),(26,4,22,NULL),(27,4,23,NULL),(28,4,24,NULL),(29,5,25,NULL),(30,5,26,NULL),(31,5,27,NULL),(32,5,28,NULL),(33,5,29,NULL),(34,7,30,NULL),(35,7,31,NULL),(36,7,32,NULL),(37,7,33,NULL),(38,7,34,NULL),(39,7,35,NULL),(40,7,36,NULL),(41,7,37,NULL),(42,7,38,NULL),(43,7,39,NULL),(44,7,40,NULL),(45,8,30,NULL),(46,8,31,NULL),(47,8,32,NULL),(48,8,33,NULL),(49,8,34,NULL),(50,8,35,NULL),(51,8,36,NULL),(52,8,37,NULL),(53,8,38,NULL),(54,8,39,NULL),(55,8,40,NULL),(56,9,30,NULL),(57,9,31,NULL),(58,9,32,NULL),(59,9,33,NULL),(60,9,34,NULL),(61,9,35,NULL),(62,9,36,NULL),(63,9,37,NULL),(64,9,38,NULL),(65,9,39,NULL),(66,9,40,NULL),(67,10,30,NULL),(68,10,31,NULL),(69,10,32,NULL),(70,10,33,NULL),(71,10,34,NULL),(72,10,35,NULL),(73,10,36,NULL),(74,10,37,NULL),(75,10,38,NULL),(76,10,39,NULL),(77,10,40,NULL),(78,11,30,NULL),(79,11,31,NULL),(80,11,32,NULL),(81,11,33,NULL),(82,11,34,NULL),(83,11,35,NULL),(84,11,36,NULL),(85,11,37,NULL),(86,11,38,NULL),(87,11,39,NULL),(88,11,40,NULL),(89,12,30,NULL),(90,12,31,NULL),(91,12,32,NULL),(92,12,33,NULL),(93,12,34,NULL),(94,12,35,NULL),(95,12,36,NULL),(96,12,37,NULL),(97,12,38,NULL),(98,12,39,NULL),(99,12,40,NULL),(100,13,41,NULL),(101,13,42,NULL),(102,13,43,NULL),(103,13,44,NULL),(104,13,45,NULL),(105,13,46,NULL),(106,13,47,NULL),(107,13,48,NULL),(108,13,49,NULL),(109,13,50,NULL),(110,13,51,NULL),(111,13,52,NULL),(112,14,53,NULL),(113,14,54,NULL),(114,14,55,NULL),(115,14,56,NULL),(116,14,57,NULL),(117,14,58,NULL),(118,15,59,NULL),(119,15,60,NULL),(120,15,61,NULL),(121,15,62,NULL),(122,15,63,NULL),(123,17,64,NULL),(124,17,65,NULL),(125,17,66,NULL),(126,17,67,NULL),(127,17,68,NULL),(128,17,69,NULL),(129,18,64,NULL),(130,18,65,NULL),(131,18,66,NULL),(132,18,67,NULL),(133,18,68,NULL),(134,18,69,NULL),(135,20,64,NULL),(136,20,65,NULL),(137,20,66,NULL),(138,20,67,NULL),(139,20,68,NULL),(140,20,69,NULL),(141,21,64,NULL),(142,21,65,NULL),(143,21,66,NULL),(144,21,67,NULL),(145,21,68,NULL),(146,21,69,NULL),(147,22,64,NULL),(148,22,65,NULL),(149,22,66,NULL),(150,22,67,NULL),(151,22,68,NULL),(152,22,69,NULL),(153,23,64,NULL),(154,23,65,NULL),(155,23,66,NULL),(156,23,67,NULL),(157,23,68,NULL),(158,23,69,NULL),(159,25,64,NULL),(160,25,65,NULL),(161,25,66,NULL),(162,25,67,NULL),(163,25,68,NULL),(164,25,69,NULL),(165,26,64,NULL),(166,26,65,NULL),(167,26,66,NULL),(168,26,67,NULL),(169,26,68,NULL),(170,26,69,NULL),(171,27,64,NULL),(172,27,65,NULL),(173,27,66,NULL),(174,27,67,NULL),(175,27,68,NULL),(176,27,69,NULL),(177,29,64,NULL),(178,29,65,NULL),(179,29,66,NULL),(180,29,67,NULL),(181,29,68,NULL),(182,29,69,NULL),(183,30,64,NULL),(184,30,65,NULL),(185,30,66,NULL),(186,30,67,NULL),(187,30,68,NULL),(188,30,69,NULL),(189,31,64,NULL),(190,31,65,NULL),(191,31,66,NULL),(192,31,67,NULL),(193,31,68,NULL),(194,31,69,NULL),(195,32,64,NULL),(196,32,65,NULL),(197,32,66,NULL),(198,32,67,NULL),(199,32,68,NULL),(200,32,69,NULL),(201,33,64,NULL),(202,33,65,NULL),(203,33,66,NULL),(204,33,67,NULL),(205,33,68,NULL),(206,33,69,NULL),(207,35,64,NULL),(208,35,65,NULL),(209,35,66,NULL),(210,35,67,NULL),(211,35,68,NULL),(212,35,69,NULL),(213,36,64,NULL),(214,36,65,NULL),(215,36,66,NULL),(216,36,67,NULL),(217,36,68,NULL),(218,36,69,NULL),(219,38,64,NULL),(220,38,65,NULL),(221,38,66,NULL),(222,38,67,NULL),(223,38,68,NULL),(224,38,69,NULL),(225,39,64,NULL),(226,39,65,NULL),(227,39,66,NULL),(228,39,67,NULL),(229,39,68,NULL),(230,39,69,NULL),(231,40,64,NULL),(232,40,65,NULL),(233,40,66,NULL),(234,40,67,NULL),(235,40,68,NULL),(236,40,69,NULL),(237,41,64,NULL),(238,41,65,NULL),(239,41,66,NULL),(240,41,67,NULL),(241,41,68,NULL),(242,41,69,NULL),(243,42,64,NULL),(244,42,65,NULL),(245,42,66,NULL),(246,42,67,NULL),(247,42,68,NULL),(248,42,69,NULL),(249,43,64,NULL),(250,43,65,NULL),(251,43,66,NULL),(252,43,67,NULL),(253,43,68,NULL),(254,43,69,NULL),(255,44,64,NULL),(256,44,65,NULL),(257,44,66,NULL),(258,44,67,NULL),(259,44,68,NULL),(260,44,69,NULL),(261,46,64,NULL),(262,46,65,NULL),(263,46,66,NULL),(264,46,67,NULL),(265,46,68,NULL),(266,46,69,NULL),(267,47,64,NULL),(268,47,65,NULL),(269,47,66,NULL),(270,47,67,NULL),(271,47,68,NULL),(272,47,69,NULL),(273,49,70,NULL),(274,49,71,NULL),(275,49,72,NULL),(276,49,73,NULL),(277,49,74,NULL),(278,49,75,NULL),(279,49,76,NULL),(280,49,77,NULL),(281,49,78,NULL),(282,49,79,NULL),(283,49,80,NULL),(284,51,81,NULL),(285,51,82,NULL),(286,51,83,NULL),(287,52,85,NULL),(288,52,86,NULL),(289,52,87,NULL),(290,52,88,NULL),(291,52,89,NULL),(292,52,90,NULL),(293,52,91,NULL),(294,52,92,NULL),(295,52,93,NULL),(296,52,94,NULL),(297,52,95,NULL),(298,52,96,NULL),(299,52,97,NULL),(300,53,98,NULL),(301,53,99,NULL),(302,53,100,NULL),(303,53,101,NULL),(304,53,102,NULL),(305,53,103,NULL),(306,53,104,NULL),(307,54,105,NULL),(308,54,106,NULL),(309,54,107,NULL),(310,54,108,NULL),(311,54,109,NULL),(312,54,110,NULL),(313,54,111,NULL),(314,54,112,NULL),(315,54,113,NULL),(316,54,114,NULL),(317,54,115,NULL),(318,54,116,NULL),(319,54,117,NULL),(320,54,118,NULL),(321,54,119,NULL),(322,54,120,NULL),(323,54,121,NULL),(324,54,122,NULL),(325,54,123,NULL),(326,54,124,NULL),(327,54,125,NULL),(328,54,126,NULL),(329,54,127,NULL),(330,54,128,NULL),(331,54,129,NULL),(332,54,130,NULL),(333,54,131,NULL),(334,54,132,NULL),(335,54,133,NULL),(336,54,134,NULL),(337,54,135,NULL),(338,54,136,NULL),(339,54,137,NULL),(340,54,138,NULL),(341,54,139,NULL),(342,54,140,NULL),(343,54,141,NULL),(344,54,142,NULL),(345,54,143,NULL),(346,54,144,NULL),(347,54,145,NULL),(348,54,146,NULL),(349,54,147,NULL),(350,54,148,NULL),(351,54,149,NULL),(352,54,150,NULL),(353,54,151,NULL),(354,54,152,NULL),(355,54,153,NULL),(356,54,154,NULL),(357,54,155,NULL),(358,54,156,NULL),(359,54,157,NULL),(360,54,158,NULL),(361,54,159,NULL),(362,54,160,NULL),(363,54,161,NULL),(364,54,162,NULL),(365,54,163,NULL),(366,54,164,NULL),(367,54,165,NULL),(368,54,166,NULL),(369,54,167,NULL),(370,54,168,NULL),(371,54,169,NULL),(372,54,170,NULL),(373,54,171,NULL),(374,54,172,NULL),(375,54,173,NULL),(376,54,174,NULL),(377,54,175,NULL),(378,54,176,NULL),(379,54,177,NULL),(380,54,178,NULL),(381,54,179,NULL),(382,54,180,NULL),(383,54,181,NULL),(384,54,182,NULL),(385,54,183,NULL),(386,54,184,NULL),(387,54,185,NULL),(388,54,186,NULL),(389,54,187,NULL),(390,54,188,NULL),(391,54,189,NULL),(392,54,190,NULL),(393,54,191,NULL),(394,54,192,NULL),(395,54,193,NULL),(396,54,194,NULL),(397,54,195,NULL),(398,54,196,NULL),(399,54,197,NULL),(400,54,198,NULL),(401,54,199,NULL),(402,54,200,NULL),(403,54,201,NULL),(404,54,202,NULL),(405,54,203,NULL),(406,54,204,NULL),(407,54,205,NULL),(408,54,206,NULL),(409,54,207,NULL),(410,54,208,NULL),(411,54,209,NULL),(412,54,210,NULL),(413,54,211,NULL),(414,54,212,NULL),(415,54,213,NULL),(416,54,214,NULL),(417,54,215,NULL),(418,54,216,NULL),(419,54,217,NULL),(420,54,218,NULL),(421,54,219,NULL),(422,54,220,NULL),(423,54,221,NULL),(424,54,222,NULL),(425,54,223,NULL),(426,54,224,NULL),(427,54,225,NULL),(428,54,226,NULL),(429,54,227,NULL),(430,54,228,NULL),(431,54,229,NULL),(432,54,230,NULL),(433,54,231,NULL),(434,54,232,NULL),(435,54,233,NULL),(436,54,234,NULL),(437,54,235,NULL),(438,54,236,NULL),(439,54,237,NULL),(440,60,238,NULL),(441,60,239,NULL),(442,60,240,NULL),(443,60,241,NULL),(444,60,242,NULL),(445,60,243,NULL),(446,60,244,NULL),(447,61,245,NULL),(448,61,246,NULL),(449,62,247,NULL),(450,62,248,NULL),(451,62,249,NULL),(452,62,250,NULL),(453,62,251,NULL),(454,62,252,NULL),(455,62,253,NULL),(456,62,254,NULL),(457,62,255,NULL),(458,62,256,NULL),(459,62,257,NULL),(460,62,258,NULL),(461,62,259,NULL),(462,62,260,NULL),(463,62,261,NULL),(464,62,262,NULL),(465,62,263,NULL),(466,62,264,NULL),(467,62,265,NULL),(468,63,272,NULL),(469,63,273,NULL),(470,63,274,NULL),(471,63,275,NULL),(472,63,276,NULL),(473,64,266,NULL),(474,64,267,NULL),(475,64,268,NULL),(476,64,269,NULL),(477,64,270,NULL),(478,64,271,NULL),(479,67,277,NULL),(480,67,278,NULL),(481,67,279,NULL),(482,69,280,NULL),(483,69,281,NULL),(484,69,282,NULL),(485,69,283,NULL),(486,69,284,NULL),(487,69,285,NULL),(488,70,286,200),(489,70,287,NULL),(490,70,288,NULL),(491,70,289,NULL),(492,70,290,NULL),(493,70,291,NULL),(494,71,292,NULL),(495,71,293,NULL),(496,71,294,NULL),(497,71,295,NULL),(498,71,296,NULL),(499,71,297,NULL),(500,71,298,NULL),(501,71,299,NULL),(502,72,300,NULL),(503,72,301,NULL),(504,72,302,NULL),(505,72,303,NULL),(506,72,304,NULL),(507,72,305,NULL),(508,72,306,NULL),(509,73,307,NULL),(510,73,308,NULL),(511,73,309,NULL),(512,73,310,NULL),(513,73,311,NULL),(514,73,312,NULL),(515,73,313,NULL),(516,73,314,NULL),(517,73,315,NULL),(518,73,316,NULL),(519,73,317,NULL),(520,73,318,NULL),(521,73,319,NULL),(522,73,320,NULL),(523,73,321,NULL),(524,73,322,NULL),(525,73,323,NULL),(526,70,324,100);
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `idtoken` int(11) NOT NULL AUTO_INCREMENT,
  `idsurvey` int(11) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtoken`),
  KEY `fk_token_survey1` (`idsurvey`),
  CONSTRAINT `fk_token_survey1` FOREIGN KEY (`idsurvey`) REFERENCES `survey` (`idsurvey`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,1,'APPLICATION NAME','Angry Birds'),(2,1,'HANDSET MANUFACTURER & MODEL','Apple iPhone'),(3,3,'HANDSET MANUFACTURER & MODEL','Webkit browser'),(4,3,'APPLICATION NAME','Mob4Hire Mobile Survey');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;