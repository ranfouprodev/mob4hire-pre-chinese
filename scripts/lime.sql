
--
-- Table structure for table `lime_answers`
--

DROP TABLE IF EXISTS `lime_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_answers` (
  `qid` int(11) NOT NULL DEFAULT '0',
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `default_value` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `assessment_value` int(11) NOT NULL DEFAULT '0',
  `sortorder` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`qid`,`code`,`language`),
  KEY `answers_idx2` (`sortorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_answers`
--

LOCK TABLES `lime_answers` WRITE;
/*!40000 ALTER TABLE `lime_answers` DISABLE KEYS */;
INSERT INTO `lime_answers` VALUES (1,'1','Overall Satisfaction','N',1,1,'en'),(2,'1','Choose Again','N',1,1,'en'),(3,'1','Recommend','N',1,1,'en'),(4,'1','Continue purchasing','N',1,1,'en'),(5,'1','Purchase different types','N',1,1,'en'),(6,'1','Purchase more expensive','N',1,1,'en'),(7,'1','Increase amount','N',1,1,'en'),(8,'1','Make larger purchases','N',1,1,'en'),(9,'1','Purchase from competitors','N',1,1,'en'),(10,'1','Stop purchasing','N',1,1,'en'),(11,'1','Switch to different provider','N',1,1,'en'),(12,'1','None','N',0,1,'en'),(12,'10','9','N',0,10,'en'),(12,'11','10','N',0,11,'en'),(12,'12','More than 10','N',0,12,'en'),(12,'2','1','N',0,2,'en'),(12,'3','2','N',0,3,'en'),(12,'4','3','N',0,4,'en'),(12,'5','4','N',0,5,'en'),(12,'6','5','N',0,6,'en'),(12,'7','6','N',0,7,'en'),(12,'8','7','N',0,8,'en'),(12,'9','8','N',0,9,'en'),(13,'1','Less than one year','N',0,1,'en'),(13,'2','One to less than three years','N',0,2,'en'),(13,'3','Three to less than five years','N',0,3,'en'),(13,'4','Five to less than ten years','N',0,4,'en'),(13,'5','Ten years or more','N',0,5,'en'),(15,'1','{INSERTANS:89893X39X122}\'s products are excellent.','N',1,1,'en'),(15,'10','{INSERTANS:89893X39X122} representatives understand my needs.','N',10,10,'en'),(15,'11','{INSERTANS:89893X39X122} representatives are always there when I need them.','N',11,11,'en'),(15,'12','{INSERTANS:89893X39X122}\'s products and/or services are a good value for the price.','N',12,12,'en'),(15,'2','{INSERTANS:89893X39X122}\'s services are excellent. ','N',2,2,'en'),(15,'3','{INSERTANS:89893X39X122} has the best reputation in their industry.','N',3,3,'en'),(15,'4','{INSERTANS:89893X39X122} has good coverage in my area of interest.','N',4,4,'en'),(15,'5','{INSERTANS:89893X39X122} has reliable service (e.g., few dropped calls).','N',5,5,'en'),(15,'6','{INSERTANS:89893X39X122} offers the handsets (e.g., phones) that I want.','N',6,6,'en'),(15,'7','{INSERTANS:89893X39X122} representatives respond to my needs in a timely manner.','N',7,7,'en'),(15,'8','{INSERTANS:89893X39X122} representatives have the knowledge to answer my questions.','N',8,8,'en'),(15,'9','{INSERTANS:89893X39X122} representatives are courteous.','N',9,9,'en'),(17,'1','Frequency of crashes','N',1,0,'en'),(18,'1','Satisfaction','N',1,1,'en'),(19,'1','Recommend Mob4Hire','N',1,1,'en'),(20,'1','Continue being tester','N',1,1,'en'),(21,'1','AT&T / Cingular','N',0,0,'en'),(21,'10','T-Mobile US','N',0,9,'en'),(21,'11','TracFone','N',0,10,'en'),(21,'12','Unicel','N',0,11,'en'),(21,'13','US Cellular','N',0,12,'en'),(21,'14','Verizon','N',0,13,'en'),(21,'15','Virgin Mobile','N',0,14,'en'),(21,'16','Don\'t know','N',0,15,'en'),(21,'2','BOOST Mobile','N',0,1,'en'),(21,'3','Cellular One','N',0,2,'en'),(21,'4','Centennial','N',0,3,'en'),(21,'5','Cricket','N',0,4,'en'),(21,'6','Metro PCS','N',0,5,'en'),(21,'7','nTelos','N',0,6,'en'),(21,'8','Sprint/Nextel','N',0,7,'en'),(21,'9','SunCom','N',0,8,'en'),(22,'1','POSTPAID','N',0,1,'en'),(22,'2','PREPAID/PAY-AS-YOU-GO; you pay for usage in advance','N',0,2,'en'),(22,'3','Don\'t know','N',0,3,'en'),(24,'1','Confidential','N',0,1,'en'),(24,'18','18','N',0,2,'en'),(24,'19','19','N',0,3,'en'),(24,'20','20','N',0,4,'en'),(24,'21','21','N',0,5,'en'),(24,'22','22','N',0,6,'en'),(24,'23','23','N',0,7,'en'),(24,'24','24','N',0,8,'en'),(24,'25','25','N',0,9,'en'),(24,'26','26','N',0,10,'en'),(24,'27','27','N',0,11,'en'),(24,'28','28','N',0,12,'en'),(24,'29','29','N',0,13,'en'),(24,'30','30','N',0,14,'en'),(24,'31','31','N',0,15,'en'),(24,'32','32','N',0,16,'en'),(24,'33','33','N',0,17,'en'),(24,'34','34','N',0,18,'en'),(24,'35','35','N',0,19,'en'),(24,'36','36','N',0,20,'en'),(24,'37','37','N',0,21,'en'),(24,'38','38','N',0,22,'en'),(24,'39','39','N',0,23,'en'),(24,'40','40','N',0,24,'en'),(24,'41','41','N',0,25,'en'),(24,'42','42','N',0,26,'en'),(24,'43','43','N',0,27,'en'),(24,'44','44','N',0,28,'en'),(24,'45','45','N',0,29,'en'),(24,'46','46','N',0,30,'en'),(24,'47','47','N',0,31,'en'),(24,'48','48','N',0,32,'en'),(24,'49','49','N',0,33,'en'),(24,'50','50','N',0,34,'en'),(24,'51','51','N',0,35,'en'),(24,'52','52','N',0,36,'en'),(24,'53','53','N',0,37,'en'),(24,'54','54','N',0,38,'en'),(24,'55','55','N',0,39,'en'),(24,'56','56','N',0,40,'en'),(24,'57','57','N',0,41,'en'),(24,'58','58','N',0,42,'en'),(24,'59','59','N',0,43,'en'),(24,'60','60','N',0,44,'en'),(24,'61','61','N',0,45,'en'),(24,'62','62','N',0,46,'en'),(24,'63','63','N',0,47,'en'),(24,'64','64','N',0,48,'en'),(24,'65','65','N',0,49,'en'),(24,'66','66','N',0,50,'en'),(24,'67','67','N',0,51,'en'),(24,'68','68','N',0,52,'en'),(24,'69','69','N',0,53,'en'),(24,'70','70','N',0,54,'en'),(24,'71','71','N',0,55,'en'),(24,'72','72','N',0,56,'en'),(24,'73','73','N',0,57,'en'),(24,'74','74','N',0,58,'en'),(24,'75','75','N',0,59,'en'),(24,'76','76','N',0,60,'en'),(24,'77','77','N',0,61,'en'),(24,'78','78','N',0,62,'en'),(24,'79','79','N',0,63,'en'),(24,'80','80','N',0,64,'en'),(24,'81','81','N',0,65,'en'),(24,'82','82','N',0,66,'en'),(24,'83','Older than 82','N',0,67,'en'),(25,'1','High School','N',0,1,'en'),(25,'2','Technical Degree','N',0,2,'en'),(25,'3','2-year Degree','N',0,3,'en'),(25,'4','4-year Degree','N',0,4,'en'),(25,'5','Master\'s Degree','N',0,5,'en'),(25,'6','Doctorate Degree','N',0,6,'en');
/*!40000 ALTER TABLE `lime_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_assessments`
--

DROP TABLE IF EXISTS `lime_assessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_assessments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL DEFAULT '0',
  `scope` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gid` int(11) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `minimum` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `maximum` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`id`,`language`),
  KEY `assessments_idx2` (`sid`),
  KEY `assessments_idx3` (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_assessments`
--

LOCK TABLES `lime_assessments` WRITE;
/*!40000 ALTER TABLE `lime_assessments` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_assessments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_conditions`
--

DROP TABLE IF EXISTS `lime_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_conditions` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `qid` int(11) NOT NULL DEFAULT '0',
  `scenario` int(11) NOT NULL DEFAULT '1',
  `cqid` int(11) NOT NULL DEFAULT '0',
  `cfieldname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `method` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`cid`),
  KEY `conditions_idx2` (`qid`),
  KEY `conditions_idx3` (`cqid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_conditions`
--

LOCK TABLES `lime_conditions` WRITE;
/*!40000 ALTER TABLE `lime_conditions` DISABLE KEYS */;
INSERT INTO `lime_conditions` VALUES (1,14,1,5,'89893X2X51','<=','4');
/*!40000 ALTER TABLE `lime_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_groups`
--

DROP TABLE IF EXISTS `lime_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_groups` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_order` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`gid`,`language`),
  KEY `groups_idx2` (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_groups`
--

LOCK TABLES `lime_groups` WRITE;
/*!40000 ALTER TABLE `lime_groups` DISABLE KEYS */;
INSERT INTO `lime_groups` VALUES (1,89893,'Overall Perceptions',1,'<div><span style=\"font-size:10.0pt;font-family:Verdana;\nmso-bidi-font-family:Arial\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Using the rating scales below, please answer the following questions regarding {INSERTANS:89893X39X122}.</span></span><o:p></o:p></span></div>','en'),(2,89893,'Overall Perceptions - Purchasing',2,'','en'),(3,89893,'Overall Perceptions - Retention',3,'','en'),(4,89893,'Reasons for not purchasing more',4,'&nbsp;Please complete the following question.','en'),(5,89893,'Customer Experience',5,'<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">The following questions are about your experience with&nbsp;<span contenteditable=\"false\">{INSERTANS:89893X39X122}</span>\'s products and services.</span></span></div>','en'),(6,89893,'Mob4Hire Testing Experience',6,'<div style=\"text-align: left; \">&nbsp;<span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Next, we would like you to provide your feedback regarding your experience as a tester.&nbsp;</span></span></div>','en'),(7,89893,'Wireless Service Provider Information',0,'&nbsp;','en'),(8,89893,'About You',7,'<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">The following questions will help us better understand our community of testers. Please answer each question.</span></span></div>','en');
/*!40000 ALTER TABLE `lime_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_labels`
--

DROP TABLE IF EXISTS `lime_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_labels` (
  `lid` int(11) NOT NULL DEFAULT '0',
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` text COLLATE utf8_unicode_ci,
  `sortorder` int(11) NOT NULL,
  `assessment_value` int(11) NOT NULL DEFAULT '0',
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`lid`,`sortorder`,`language`),
  KEY `ixcode` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_labels`
--

LOCK TABLES `lime_labels` WRITE;
/*!40000 ALTER TABLE `lime_labels` DISABLE KEYS */;
INSERT INTO `lime_labels` VALUES (1,'1','Strongly Disagree',1,0,'en'),(1,'2','Disagree',2,0,'en'),(1,'3','Neither Agree nor Disagree',3,0,'en'),(1,'4','Agree',4,0,'en'),(1,'5','Strongly Agree',5,0,'en'),(1,'6','Don\'t Know',6,0,'en'),(2,'0','0 - Not at all Likely',1,0,'en'),(2,'1','1',2,0,'en'),(2,'10','10 - Extremely Likely',11,0,'en'),(2,'2','2',3,0,'en'),(2,'3','3',4,0,'en'),(2,'4','4',5,0,'en'),(2,'5','5',6,0,'en'),(2,'6','6',7,0,'en'),(2,'7','7',8,0,'en'),(2,'8','8',9,0,'en'),(2,'9','9',10,0,'en'),(3,'0','0 - Extremely Dissatisfied',1,0,'en'),(3,'1','1',2,0,'en'),(3,'10','10 - Extremely Satisfied',11,0,'en'),(3,'2','2',3,0,'en'),(3,'3','3',4,0,'en'),(3,'4','4',5,0,'en'),(3,'5','5 - Neither Dissatisfied nor Satisfied',6,0,'en'),(3,'6','6',7,0,'en'),(3,'7','7',8,0,'en'),(3,'8','8',9,0,'en'),(3,'9','9',10,0,'en'),(4,'1','Never',1,0,'en'),(4,'2','Rarely',2,0,'en'),(4,'3','Sometimes',3,0,'en'),(4,'4','Often',4,0,'en'),(4,'5','Always',5,0,'en');
/*!40000 ALTER TABLE `lime_labels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_labelsets`
--

DROP TABLE IF EXISTS `lime_labelsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_labelsets` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `label_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `languages` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'en',
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_labelsets`
--

LOCK TABLES `lime_labelsets` WRITE;
/*!40000 ALTER TABLE `lime_labelsets` DISABLE KEYS */;
INSERT INTO `lime_labelsets` VALUES (1,'5-point agreement','en'),(2,'Likelihood','en'),(3,'Satisfaction','en'),(4,'Frequency','en');
/*!40000 ALTER TABLE `lime_labelsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_question_attributes`
--

DROP TABLE IF EXISTS `lime_question_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_question_attributes` (
  `qaid` int(11) NOT NULL AUTO_INCREMENT,
  `qid` int(11) NOT NULL DEFAULT '0',
  `attribute` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`qaid`),
  KEY `question_attributes_idx2` (`qid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_question_attributes`
--

LOCK TABLES `lime_question_attributes` WRITE;
/*!40000 ALTER TABLE `lime_question_attributes` DISABLE KEYS */;
INSERT INTO `lime_question_attributes` VALUES (1,8,'answer_width','20'),(2,6,'answer_width','20'),(3,7,'answer_width','20'),(4,4,'answer_width','20'),(5,3,'answer_width','20'),(6,5,'answer_width','20'),(7,2,'answer_width','20'),(8,1,'answer_width','20'),(9,9,'answer_width','20'),(10,10,'answer_width','20'),(11,11,'answer_width','20');
/*!40000 ALTER TABLE `lime_question_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_questions`
--

DROP TABLE IF EXISTS `lime_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_questions` (
  `qid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL DEFAULT '0',
  `gid` int(11) NOT NULL DEFAULT '0',
  `type` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'T',
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `preg` text COLLATE utf8_unicode_ci,
  `help` text COLLATE utf8_unicode_ci,
  `other` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `mandatory` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lid` int(11) NOT NULL DEFAULT '0',
  `lid1` int(11) NOT NULL DEFAULT '0',
  `question_order` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`qid`,`language`),
  KEY `questions_idx2` (`sid`),
  KEY `questions_idx3` (`gid`),
  KEY `questions_idx4` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_questions`
--

LOCK TABLES `lime_questions` WRITE;
/*!40000 ALTER TABLE `lime_questions` DISABLE KEYS */;
INSERT INTO `lime_questions` VALUES (1,89893,1,'F','0001','<p style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Overall how satisfied are you with&nbsp;{INSERTANS:89893X39X122}?</span></span></span></p>','','&nbsp;','N','Y',3,0,0,'en'),(2,89893,1,'F','0002','<p><span style=\"font-size:10pt;font-family:Verdana;\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">If you were selecting a wireless service provider for the <u>first time</u>, how likely is it that you would choose&nbsp;{INSERTANS:89893X39X122}?</span></span></span></p>','','&nbsp;','N','Y',2,0,1,'en'),(3,89893,1,'F','0003','<div style=\"text-align: left; \"><span style=\"font-family: Verdana; \"><span style=\"font-size: 10pt; \"><span style=\"font-size: medium; \">How likely are you to recommend&nbsp;{INSERTANS:89893X39X122}&nbsp;to your friends/colleagues?</span></span></span></div>','','&nbsp;','N','Y',2,0,2,'en'),(4,89893,1,'F','0004','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to continue purchasing the <u>same</u> product and/or service from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,3,'en'),(5,89893,2,'F','005','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>purchase different types</u> of services from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,0,'en'),(6,89893,2,'F','0006','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>purchase more expensive services</u> from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,1,'en'),(7,89893,2,'F','0007','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>increase the amount</u> of products and/or services you purchase from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,2,'en'),(8,89893,2,'F','0008','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>make larger purchases</u> from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,3,'en'),(9,89893,3,'F','0009','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>purchase services from&nbsp;{INSERTANS:89893X39X122}\'s competitor(s)</u>?</span></span></span></div>','','&nbsp;','N','Y',2,0,0,'en'),(10,89893,3,'F','0010','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to <u>stop purchasing</u> services from&nbsp;{INSERTANS:89893X39X122}?</span></span></span></div>','','&nbsp;','N','Y',2,0,1,'en'),(11,89893,3,'F','0011','<div style=\"text-align: left; \"><span style=\"font-size:10pt;font-family:Verdana;mso-fareast-font-family:\'TimesNewRoman\';mso-bidi-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to switch to a different wireless service provider within the next 12 months?</span></span></span></div>','','&nbsp;','N','Y',2,0,2,'en'),(12,89893,3,'!','0021','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How many friends/colleagues have you recommended&nbsp;{INSERTANS:89893X39X122}<span contenteditable=\"false\">&nbsp;</span>to in the past 12 months?&nbsp;</span></span></div>','','&nbsp;','N','N',0,0,3,'en'),(13,89893,3,'!','0022','<div style=\"text-align: left; \"><span style=\"font-family: Verdana; \"><span style=\"font-size: medium; \">How long have you been a customer of {INSERTANS:89893X39X122}?</span></span><span style=\"font-size: medium; \" /></div>','','&nbsp;','N','N',0,0,4,'en'),(14,89893,4,'T','0012','<div style=\"text-align: left; \"><span style=\"font-size:10.0pt;font-family:Verdana;\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Why&nbsp;you are not likely to purchase additional service from {INSERTANS:89893X39X122}? Please be specific.</span></span><span style=\"font-family: Verdana; \" /><span style=\"font-size:10.0pt;font-family:Verdana;\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\" /></span></div>','','&nbsp;','N','N',0,0,0,'en'),(15,89893,5,'F','0013','<div style=\"text-align: left; \"><span style=\"font-size:10.0pt;font-family:Verdana;\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Please rate the extent to which you agree or disagree with each of the following statements regarding your experience with {INSERTANS:89893X39X122}.</span></span></span></div>','','&nbsp;','N','Y',1,0,0,'en'),(16,89893,5,'T','0014','<div style=\"text-align: left; \"><span style=\"font-size:10.0pt;font-family:Verdana;\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">What is the most important area for improvement for {INSERTANS:89893X39X122}?&nbsp;</span></span></span></div>','','&nbsp;','N','N',0,0,1,'en'),(17,89893,6,'F','0014','<span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \"><span class=\"Apple-style-span\" style=\"color: rgb(0, 0, 128); white-space: pre; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; \">How frequently do applications you are testing crash on your mobile phone?</span></span></span><span style=\"font-family: Verdana; \" />','','&nbsp;','N','N',4,0,0,'en'),(18,89893,6,'F','0015','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">Overall, how satisfied are you with Mob4Hire?&nbsp;</span></span></div>','','&nbsp;','N','Y',3,0,1,'en'),(19,89893,6,'F','0016','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to recommend Mob4Hire to your friends?&nbsp;</span></span></div>','','&nbsp;','N','Y',2,0,2,'en'),(20,89893,6,'F','0017','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">How likely are you to continue being a tester for Mob4Hire?&nbsp;</span></span></div>','','&nbsp;','N','Y',2,0,3,'en'),(21,89893,7,'!','0019','<p><big><span lang=\"NL\" style=\"font-size:8.0pt;line-height:115%;\nfont-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;color:midnightblue;\nmso-ansi-language:NL;mso-fareast-language:NL;mso-bidi-language:HI\" /><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \"><span lang=\"NL\" style=\"line-height: 115%; color: rgb(25, 25, 112); \">Which of the following companies is your <b>current</b> (primary) cell phone provider?</span>&nbsp;</span></span></big></p>','','&nbsp;','Y','Y',0,0,0,'en'),(22,89893,7,'!','0020','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \"><span lang=\"NL\" style=\"line-height: 115%; color: rgb(25, 25, 112); \">Which type of contract / plan do you currently have with your wireless carrier?</span></span></span><span style=\"font-family: Verdana; \" />&nbsp;</div>','','&nbsp;','N','N',0,0,1,'en'),(23,89893,8,'G','0023','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">What is your gender?</span></span>&nbsp;</div>','','&nbsp;','N','N',0,0,0,'en'),(24,89893,8,'!','0024','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">What is your age (in years)?&nbsp;</span></span></div>','','&nbsp;','N','N',0,0,1,'en'),(25,89893,8,'!','0025','<div style=\"text-align: left; \"><span style=\"font-size: medium; \"><span style=\"font-family: Verdana; \">What is the highest level of education that you attained?&nbsp;</span></span></div>','','&nbsp;','N','N',0,0,2,'en');
/*!40000 ALTER TABLE `lime_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_quota`
--

DROP TABLE IF EXISTS `lime_quota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_quota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qlimit` int(8) DEFAULT NULL,
  `action` int(2) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `autoload_url` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `quota_idx2` (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_quota`
--

LOCK TABLES `lime_quota` WRITE;
/*!40000 ALTER TABLE `lime_quota` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_quota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_quota_languagesettings`
--

DROP TABLE IF EXISTS `lime_quota_languagesettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_quota_languagesettings` (
  `quotals_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotals_quota_id` int(11) NOT NULL DEFAULT '0',
  `quotals_language` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `quotals_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotals_message` text COLLATE utf8_unicode_ci NOT NULL,
  `quotals_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotals_urldescrip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`quotals_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_quota_languagesettings`
--

LOCK TABLES `lime_quota_languagesettings` WRITE;
/*!40000 ALTER TABLE `lime_quota_languagesettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_quota_languagesettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_quota_members`
--

DROP TABLE IF EXISTS `lime_quota_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_quota_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  `quota_id` int(11) DEFAULT NULL,
  `code` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sid` (`sid`,`qid`,`quota_id`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_quota_members`
--

LOCK TABLES `lime_quota_members` WRITE;
/*!40000 ALTER TABLE `lime_quota_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_quota_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_saved_control`
--

DROP TABLE IF EXISTS `lime_saved_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_saved_control` (
  `scid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL DEFAULT '0',
  `srid` int(11) NOT NULL DEFAULT '0',
  `identifier` text COLLATE utf8_unicode_ci NOT NULL,
  `access_code` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` text COLLATE utf8_unicode_ci NOT NULL,
  `saved_thisstep` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `saved_date` datetime NOT NULL,
  `refurl` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`scid`),
  KEY `saved_control_idx2` (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_saved_control`
--

LOCK TABLES `lime_saved_control` WRITE;
/*!40000 ALTER TABLE `lime_saved_control` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_saved_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_settings_global`
--

DROP TABLE IF EXISTS `lime_settings_global`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_settings_global` (
  `stg_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stg_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`stg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_settings_global`
--

LOCK TABLES `lime_settings_global` WRITE;
/*!40000 ALTER TABLE `lime_settings_global` DISABLE KEYS */;
INSERT INTO `lime_settings_global` VALUES ('DBVersion','138'),('SessionName','ls89619911878652999666');
/*!40000 ALTER TABLE `lime_settings_global` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_surveys`
--

DROP TABLE IF EXISTS `lime_surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_surveys` (
  `sid` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `expires` date DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `adminemail` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `private` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faxto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'default',
  `language` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datestamp` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `usecookie` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `notification` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `allowregister` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `allowsave` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `autonumber_start` bigint(11) DEFAULT '0',
  `autoredirect` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `allowprev` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `printanswers` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `ipaddr` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `refurl` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `datecreated` date DEFAULT NULL,
  `publicstatistics` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `publicgraphs` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `listpublic` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `htmlemail` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `tokenanswerspersistence` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `assessments` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `usecaptcha` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `usetokens` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `bounce_email` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributedescriptions` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_surveys`
--

LOCK TABLES `lime_surveys` WRITE;
/*!40000 ALTER TABLE `lime_surveys` DISABLE KEYS */;
INSERT INTO `lime_surveys` VALUES (89893,1,'Bob E. Hayes','N',NULL,NULL,'bob@businessoverbroadway.com','Y','','G','BOB_Template','en','','N','N','0','N','Y',0,'Y','Y','N','N','N','2008-08-21','N','N','N','Y','N','N','D','N','bob@businessoverbroadway.com','');
/*!40000 ALTER TABLE `lime_surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_surveys_languagesettings`
--

DROP TABLE IF EXISTS `lime_surveys_languagesettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_surveys_languagesettings` (
  `surveyls_survey_id` int(10) unsigned NOT NULL DEFAULT '0',
  `surveyls_language` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `surveyls_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `surveyls_description` text COLLATE utf8_unicode_ci,
  `surveyls_welcometext` text COLLATE utf8_unicode_ci,
  `surveyls_endtext` text COLLATE utf8_unicode_ci,
  `surveyls_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_urldescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_email_invite_subj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_email_invite` text COLLATE utf8_unicode_ci,
  `surveyls_email_remind_subj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_email_remind` text COLLATE utf8_unicode_ci,
  `surveyls_email_register_subj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_email_register` text COLLATE utf8_unicode_ci,
  `surveyls_email_confirm_subj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveyls_email_confirm` text COLLATE utf8_unicode_ci,
  `surveyls_dateformat` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`surveyls_survey_id`,`surveyls_language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_surveys_languagesettings`
--

LOCK TABLES `lime_surveys_languagesettings` WRITE;
/*!40000 ALTER TABLE `lime_surveys_languagesettings` DISABLE KEYS */;
INSERT INTO `lime_surveys_languagesettings` VALUES (89893,'en','Wireless Service Provider Survey','<div style=\"text-align: left; \">\n<div style=\"text-align: left; \">&nbsp;</div>\n</div>','<p style=\"text-align: left; \"><span style=\"font-size: small; \"><span style=\"font-family: Arial; \"><span contenteditable=\"false\">{TOKEN:FIRSTNAME}</span>,<br />\n<br />\nThank your taking the time to complete this survey. We are working with Mob4Hire to understand your satisfaction with your current wireless service provider and experience with Mob4Hire. &nbsp;In return for completing this survey, you will receive $10.00 USD in your Mob4Hire account.&nbsp;<br />\n<br />\nOn the following pages, you will be asked a series of question about your wireless service provider and your experience with Mob4Hire. Please be honest. There are no right or wrong answers.&nbsp;<br />\n<br />\nIf you have any questions about this survey, please feel free to contact me.&nbsp;<br />\n<br />\nSincerely,<br />\n<br />\nBob E. Hayes, Ph.D.<br />\n</span></span><a href=\"mailto:bob@businessoverbroadway.com?subject=Wireless%20Service%20Provider%20Survey%20-%20Mob4Hire\"><span style=\"font-size: small; \"><span style=\"font-family: Arial; \">bob@businessoverbroadway.com</span></span></a><span style=\"font-size: small; \"><span style=\"font-family: Arial; \"><br />\nBusiness Over Broadway</span></span></p>','&nbsp;','http://www.businessoverbroadway.com/tym4h.htm','BOB - Wireless Service Provider Survey Thank You Page','Invitation to participate in survey','Dear {FIRSTNAME},<br /><br />You have been invited to participate in a survey.<br /><br />The survey is titled:<br />\"{SURVEYNAME}\"<br /><br />\"{SURVEYDESCRIPTION}\"<br /><br />To participate, please click on the link below.<br /><br />Sincerely,<br /><br />{ADMINNAME} ({ADMINEMAIL})<br /><br />----------------------------------------------<br />Click here to do the survey:<br />{SURVEYURL}','Reminder to participate in survey','Dear {FIRSTNAME},<br /><br />Recently we invited you to participate in a survey.<br /><br />We note that you have not yet completed the survey, and wish to remind you that the survey is still available should you wish to take part.<br /><br />The survey is titled:<br />\"{SURVEYNAME}\"<br /><br />\"{SURVEYDESCRIPTION}\"<br /><br />To participate, please click on the link below.<br /><br />Sincerely,<br /><br />{ADMINNAME} ({ADMINEMAIL})<br /><br />----------------------------------------------<br />Click here to do the survey:<br />{SURVEYURL}','Survey Registration Confirmation','Dear {FIRSTNAME},<br /><br />You, or someone using your email address, have registered to participate in an online survey titled {SURVEYNAME}.<br /><br />To complete this survey, click on the following URL:<br /><br />{SURVEYURL}<br /><br />If you have any questions about this survey, or if you did not register to participate and believe this email is in error, please contact {ADMINNAME} at {ADMINEMAIL}.','Confirmation of completed survey','Dear {FIRSTNAME},<br /><br />This email is to confirm that you have completed the survey titled {SURVEYNAME} and your response has been saved. Thank you for participating.<br /><br />If you have any further questions about this email, please contact {ADMINNAME} on {ADMINEMAIL}.<br /><br />Sincerely,<br /><br />{ADMINNAME}',1);
/*!40000 ALTER TABLE `lime_surveys_languagesettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_surveys_rights`
--

DROP TABLE IF EXISTS `lime_surveys_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_surveys_rights` (
  `sid` int(10) unsigned NOT NULL DEFAULT '0',
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_survey_property` tinyint(1) NOT NULL DEFAULT '0',
  `define_questions` tinyint(1) NOT NULL DEFAULT '0',
  `browse_response` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `delete_survey` tinyint(1) NOT NULL DEFAULT '0',
  `activate_survey` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_surveys_rights`
--

LOCK TABLES `lime_surveys_rights` WRITE;
/*!40000 ALTER TABLE `lime_surveys_rights` DISABLE KEYS */;
INSERT INTO `lime_surveys_rights` VALUES (89893,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `lime_surveys_rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_templates`
--

DROP TABLE IF EXISTS `lime_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_templates` (
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator` int(11) NOT NULL,
  PRIMARY KEY (`folder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_templates`
--

LOCK TABLES `lime_templates` WRITE;
/*!40000 ALTER TABLE `lime_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_templates_rights`
--

DROP TABLE IF EXISTS `lime_templates_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_templates_rights` (
  `uid` int(11) NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use` int(1) NOT NULL,
  PRIMARY KEY (`uid`,`folder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_templates_rights`
--

LOCK TABLES `lime_templates_rights` WRITE;
/*!40000 ALTER TABLE `lime_templates_rights` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_templates_rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_user_groups`
--

DROP TABLE IF EXISTS `lime_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_user_groups` (
  `ugid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ugid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_user_groups`
--

LOCK TABLES `lime_user_groups` WRITE;
/*!40000 ALTER TABLE `lime_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_user_in_groups`
--

DROP TABLE IF EXISTS `lime_user_in_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_user_in_groups` (
  `ugid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  KEY `user_in_groups_idx1` (`ugid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_user_in_groups`
--

LOCK TABLES `lime_user_in_groups` WRITE;
/*!40000 ALTER TABLE `lime_user_in_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `lime_user_in_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lime_users`
--

DROP TABLE IF EXISTS `lime_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lime_users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `users_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` blob NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `lang` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_survey` tinyint(1) NOT NULL DEFAULT '0',
  `create_user` tinyint(1) NOT NULL DEFAULT '0',
  `delete_user` tinyint(1) NOT NULL DEFAULT '0',
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `configurator` tinyint(1) NOT NULL DEFAULT '0',
  `manage_template` tinyint(1) NOT NULL DEFAULT '0',
  `manage_label` tinyint(1) NOT NULL DEFAULT '0',
  `htmleditormode` varchar(7) COLLATE utf8_unicode_ci DEFAULT 'default',
  `one_time_pw` blob,
  `dateformat` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_name` (`users_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lime_users`
--

LOCK TABLES `lime_users` WRITE;
/*!40000 ALTER TABLE `lime_users` DISABLE KEYS */;
INSERT INTO `lime_users` VALUES (1,'admin','068be298f238d888dfdfdacd6669d3e240e0feb5419f85642a1d718a9176018a','Your Name',0,'en','your@email.org',1,1,1,1,1,1,1,'default',NULL,1);
/*!40000 ALTER TABLE `lime_users` ENABLE KEYS */;
UNLOCK TABLES;
