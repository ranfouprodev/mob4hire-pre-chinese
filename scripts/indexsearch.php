<?php
// Set our timezone
date_default_timezone_set('Europe/London');
// Define path to application directory
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define('APPLICATION_ENV', getenv('APPLICATION_ENV'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
realpath(APPLICATION_PATH . '/../library'),
realpath(APPLICATION_PATH . '/../application'),
realpath(APPLICATION_PATH . '/../application/models'),
get_include_path(),
)));
$siteRootDir =dirname(realpath(APPLICATION_PATH));
// Turn on autoloading, so we do not include each Zend Framework class
require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Lib_');
$loader->setFallbackAutoloader(true);

// Create registry object and setting it as the static instance in the Zend_Registry class
$registry = new Zend_Registry();
Zend_Registry::setInstance($registry);

//save $siteRootDir in registry:
$registry->set('siteRootDir', $siteRootDir);

if ( isset($_ENV['SERVER_ENV']) ) {
	$config_path =realpath(APPLICATION_PATH . '/../configuration/'. $_ENV['SERVER_ENV'] .'/config.ini'); 
}
else {
	$config_path =realpath(APPLICATION_PATH . '/../configuration/config.ini');
}
// Load configuration file and store the data in the registry
$configuration = new Zend_Config_Ini($config_path, 'main');
Zend_Registry::set('configuration', $configuration);

// Construct the database adapter class, connect to the database and store the db object in the registry
$db = Zend_Db::factory($configuration->db);
$db->query("SET NAMES 'utf8'");
Zend_Registry::set('db', $db);
// set this adapter as default for use with Zend_Db_Table
Zend_Db_Table_Abstract::setDefaultAdapter($db);

if ( $configuration->has->memcache ) {
	// Construct the list of memcache servers to use as our backend cache.  This will be the list of app servers in the farm
	$results = array( 'host'=>'127.0.0.1', 'port'=>11211, 'persistence'=> true);
	$backendOptions = array('servers'=> $results );
	
	// Only setup the cache if we found the app servers
	if ( count($backendOptions) > 0 ) {
		$frontendOptions = array('lifeTime' => 180, 'automatic_serialization' => true );
		$cache = Zend_Cache::factory('Core', 'Memcached', $frontendOptions, $backendOptions);
		
		Zend_Registry::set('cache', $cache);
	} else {
		Zend_Registry::set('cache', NULL );
	}
} else {
	Zend_Registry::set('cache', NULL);
}

// Setup translation adapter
// Check if language is set in session - if not - use english as default
$lang = 'en';
$translate = new Zend_Translate('gettext', realpath(APPLICATION_PATH .  '/../languages/'.$lang.'.mo'), $lang);
Zend_Registry::set('Zend_Translate', $translate);
Zend_Form::setDefaultTranslator($translate);

// Index the user search
$usersearch = new Search_UserSearch(); 
$usersearch->fullIndex();

$projectsearch = new Search_ProjectSearch(); 
$projectsearch->fullIndex();

