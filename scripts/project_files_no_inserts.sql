--
-- Table structure for table `project_files`
--

DROP TABLE IF EXISTS `project_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_files` (
  `idfiles` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idproject` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `physical_filename` varchar(255) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `permissions` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `filesize` int(10) DEFAULT NULL,
  `filedata` longblob,
  PRIMARY KEY (`idfiles`),
  KEY `idproject_ndx` (`idproject`,`category`,`status`)
) ENGINE=MyISAM AUTO_INCREMENT=4212 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;