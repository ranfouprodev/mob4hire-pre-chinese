<?php
require '../application/Lib/Util/Timer.php';

$timer = Timer::start();
require '../application/bootstrap.php';
$elapsed = Timer::stop($timer);
?>