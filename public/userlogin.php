<?php 
// PHP Proxy for Mob4Hire Web Services.
//
// Wraps requests with callback = functions
//
// Author: John Carpenter
//

// All calls are though userlogin.php with go to
$url = 'http://api.mob4hire.com/APIAuth/api/v1/user/authenticate/json/';
$callback = $_GET['callback'];

$encoded = '';
// include GET as well as POST variables; your needs may vary.
foreach ($_GET as $name=>$value) {
	if( $name != 'callback' )
    	$encoded .= urlencode($name).'='.urlencode($value).'&';
}
// chop off last ampersand
$encoded = substr($encoded, 0, strlen($encoded) - 1);


$fullUrl = $url . $user . '?' . $encoded;

// Open the Curl session
$ch = curl_init($fullUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

$xml = curl_exec($ch);


header('Content-type: text/javascript');
header("Cache-Control: no-cache, must-revalidate");


// OUTPUT JSONP:
echo $callback .'('.$xml.');';
curl_close($ch);
?>
