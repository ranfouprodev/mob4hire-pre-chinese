$(function(){
	$("select#vendor").change(function(){
		idproject=$("#idproject").val();
		if (idproject==undefined)
		{
			idproject=0;
		}
		$.getJSON("/ajax/index/modelSelect",{id: $(this).val(), platform: $("#platform").val(),idproject: idproject}, function(j){
			var options = '';
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
			}
			$("#model").html(options);
			$('#model option:first').attr('selected', 'selected');
		});
	})			
})

$(function(){
	$("select#platform").change(function(){
		$.getJSON("/ajax/index/platformSelect",{id: $(this).val()}, function(j){
			var options = '';
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
			}
			$("#vendor").html(options);
			$('#vendor option:first').attr('selected', 'selected');
			$('#model').html('<option value=\'0\'>Any</option>');
		});
	})
})