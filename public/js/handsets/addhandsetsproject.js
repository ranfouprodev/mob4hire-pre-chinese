$(document).ready(function() {
	postObject= new Object; // make sure that the table shows on page load
	$.post("/ajax/index/addhandsetsprojectajax/", postObject, function(data){
		$("#handsettable").html(data);
	});

});

$(function(){
	$("button#addmodel").click(function(){

		// Show working if available
		$('#workinghandset').show();
		
		postObject= new Object;
		postObject.platform=$('#platform').val();
		postObject.vendor=$('#vendor').val();
		postObject.model=$('#model').val();
		$.post("/ajax/index/addhandsetsprojectajax/", postObject, function(data){
			$("#handsettable").html(data);
			$('#workinghandset').hide();
		});
	
	})
})


function removeHandset(platformId, vendorId, modelId){
	$('#workinghandset').show();
	
	postObject= new Object;
		postObject.platform=platformId;
		postObject.vendor=vendorId;
		postObject.model=modelId;
		$.post("/ajax/index/removehandsetproject/", postObject, function(data){
			$("#handsettable").html(data);
			$('#workinghandset').hide();
		});
	
}