function showPage(idproject)
{
	var countryFilter=$('#country').val();
	var networkFilter=$('#network').val();
//	platformFilter=$('#platform').val();
	var platformFilter='0';
	var vendorFilter=$('#vendor').val();
	var modelFilter=$('#model').val();
	var showInvited=$('#showinvites').val();
	var filterType=$('#filtertype').val();
	var filterData=$('#filterdata').val();
	
	
	var whereClause='';

	var firstQuerySet=0;
	
	if(!(countryFilter=='0' && networkFilter=='0'   && platformFilter=='0'  && vendorFilter=='0'  && modelFilter=='0' && filterData=='') ) 
	{
		countryFilterString = "";
		if(countryFilter!='0' ){
			countryFilterString = "n.idcountry='"+countryFilter+"'";	
			firstQuerySet=1;
		}
		
		networkFilterString = "";			
		if(networkFilter!='0' ){
			if (firstQuerySet) {
				networkFilterString+=" AND ";
			} 
			
			networkFilterString +="regdevices.idnetwork='"+ networkFilter+"'";
			firstQuerySet=1;
		}

		platformFilterString = "";
			
		if(platformFilter!='0' ){
			if (firstQuerySet) {
				platformFilterString+=" AND ";
			}
			platformFilterString += platformFilter;
			firstQuerySet=1;
		}

		vendorFilterString = "";
			
		if(vendorFilter!='0' ){
			if (firstQuerySet) {
				vendorFilterString+=" AND ";
			}
			vendorFilterString += "d.vendor='"+vendorFilter+"'";
			firstQuerySet=1;
		}

		modelFilterString="";			
		if(modelFilter!='0' ){
			if (firstQuerySet) {
				modelFilterString+=" AND " ;
			}
			modelFilterString += "d.id='"+modelFilter+"'";
			firstQuerySet=1;
		}

		
		userFilterString="";
		if(filterData != ''){
			if (firstQuerySet) {
				userFilterString+=" AND " ;
			}
			userFilterString += "LOWER("+filterType+") LIKE LOWER('%"+filterData+"%')";
			firstQuerySet=1;
			
		}

		whereClause=countryFilterString+networkFilterString+platformFilterString+vendorFilterString+modelFilterString+userFilterString;
		//+" AND n.idcountry>0 AND regdevices.idnetwork>0 AND d.id>0";
	}
	
	postObject = new Object;
	postObject.showit=$('#showinvites').val();
	postObject.id = idproject; 
	
	postObject.whereClause = whereClause;
	$("#ajaxtable").html('<img src="/images/working.gif"/>');
	$.post("/app/developer/invitetableajax/", postObject, function(data){
		$("#ajaxtable").html(data);
	});
}
