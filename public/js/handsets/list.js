$(function(){
	$("button#filter").click(function(){
		showPage("n.idcountry,n.network,d.vendor,d.model",'1');
	})
})
		

function showPage(orderBy, pageNumber)
{
	countryFilter=$('#country').val();
	networkFilter=$('#network').val();
	//platformFilter=$('#platform').val();
	platformFilter='0';
	vendorFilter=$('#vendor').val();
	modelFilter=$('#model').val();

	if(countryFilter=='0' && networkFilter=='0'   && platformFilter=='0'  && vendorFilter=='0'  && modelFilter=='0' ) {
		whereClause="n.idcountry>0 AND regdevices.idnetwork>0 AND d.id>0";		
	}
	else {
		firstQuerySet=0;			
		if(countryFilter!='0' ){
			countryFilterString = "n.idcountry='"+countryFilter+"'";	
			firstQuerySet=1;
		}
		else {
			countryFilterString = "";
		}
		networkFilterString = "";			
		if(networkFilter!='0' ){
			if (firstQuerySet) {
				networkFilterString+=" AND ";
			} 
			
			networkFilterString +="regdevices.idnetwork='"+ networkFilter+"'";
			firstQuerySet=1;
		}

		platformFilterString = "";
			
		if(platformFilter!='0' ){
			if (firstQuerySet) {
				platformFilterString+=" AND ";
			}
			platformFilterString += platformFilter;
			firstQuerySet=1;
		}

		vendorFilterString = "";
			
		if(vendorFilter!='0' ){
			if (firstQuerySet) {
				vendorFilterString+=" AND ";
			}
			vendorFilterString += "d.vendor='"+vendorFilter+"'";
			firstQuerySet=1;
		}

		modelFilterString="";			
		if(modelFilter!='0' ){
			if (firstQuerySet) {
				modelFilterString+=" AND " ;
			}
			modelFilterString += "d.id='"+modelFilter+"'";
		}
		
		whereClause=countryFilterString+networkFilterString+platformFilterString+vendorFilterString+modelFilterString+" AND n.idcountry>0 AND regdevices.idnetwork>0 AND d.id>0";
	}
	postObject = new Object;
	postObject.whereClause = whereClause;
	postObject.orderBy = orderBy;
	postObject.pageNumber = pageNumber;
	$.post("/the-mob/listhandsetsajax/", postObject, function(data){
			$("#ajaxtable").html(data);
	});
}
