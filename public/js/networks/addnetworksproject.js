$(document).ready(function() {
	postObject= new Object; // make sure that the table shows on page load
		$.post("/ajax/index/addnetworksprojectajax/", postObject, function(data){
			$("#networktable").html(data);
	});

});

$(function(){
	$("button#addcarrier").click(function(){
		
		// Show working if available
		$('#workingnetwork').show();
		
		postObject= new Object;
		postObject.country=$('#country').val();
		postObject.network=$('#network').val();
		$.post("/ajax/index/addnetworksprojectajax/", postObject, function(data){
			$("#networktable").html(data);
			$('#workingnetwork').hide();
		});
	
	})
})


function removeNetwork(countryId, networkId){
	$('#workingnetwork').show();
	
	postObject= new Object;
		postObject.country=countryId;
		postObject.network=networkId;
		$.post("/ajax/index/removenetworkproject/", postObject, function(data){
			$("#networktable").html(data);
			$('#workingnetwork').hide();
		});
	
}
