$(function(){
	$("select#country").change(function(){
		
		// disable the networks until the countries are populated
		$('#network').attr('disabled', 'disabled');
		$('#workingnetwork').show();
		$.getJSON("/ajax/index/networkselect",{id: $(this).val()}, function(j){
			var options = '';
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
			}
			$("#network").html(options);
//			$('#network option:first').attr('selected', 'selected');

			// Show the working gif if available
			$('#network').removeAttr("disabled");
			$('#workingnetwork').hide();
		})
		$.post("/ajax/index/getcountrycode/", {id: $(this).val()}, function(data){
			$("#code").val(data);
		});

	})
	
})
