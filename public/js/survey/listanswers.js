$(document).ready(function(){
	$("select#answergroup").change(function(){
		answergroup=$('#answergroup').val();
		postObject = new Object;
		postObject.whereClause = answergroup;
		$.post("/ajax/index/showanswergroup", postObject, function(data){
				$("#answerajaxtable").html(data);
		});
	});
	
	$("#addquestion").click(function(){
		
		postObject= new Object;
		postObject.text=$('#questiontext').val();
		postObject.answergroup=$('select#answergroup').val();
		postObject.mandatory=$('select#mandatory').val();
		postObject.idsurvey=$('#idsurvey').val();
		postObject.add='1';
		$.post("/ajax/index/showsurveyquestions/", postObject, function(data){
			$("#questionlistajaxtable").html(data);
		});
	
	})
	
	
	
	
})


function loadQuestionList(idsurvey){
	
	postObject= new Object; // make sure that the table shows on page load
	postObject.idsurvey = idsurvey;
	$.post("/ajax/index/showsurveyquestions/", postObject, function(data){
			$("#questionlistajaxtable").html(data);
	});
	
}

function removeQuestion(idquestion, idsurvey)
{
	postObject= new Object; // make sure that the table shows on page load
	postObject.remove=idquestion;
	postObject.idsurvey = idsurvey;
	$.post("/ajax/index/showsurveyquestions/", postObject, function(data){
			$("#questionlistajaxtable").html(data);
	});
}

