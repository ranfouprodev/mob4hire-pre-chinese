$(document).ready(function(){
	
if($('.anythingSlider').length > 0) {
		$(function () {

			$('.anythingSlider').anythingSlider({
				easing: "easeInOutExpo",        // Anything other than "linear" or "swing" requires the easing plugin
				autoPlay: false,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
				delay: 12000,                    // How long between slide transitions in AutoPlay mode
				startStopped: false,            // If autoPlay is on, this can force it to start stopped
				animationTime: 600,             // How long the slide transition takes
				hashTags: true,                 // Should links change the hashtag in the URL?
				buildNavigation: false,          // If true, builds and list of anchor links to link to each slide
				pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
				startText: "Go",             // Start text
				stopText: "Stop"               // Stop text
			});


			$("#slide-jump").click(function(){
				$('.anythingSlider').anythingSlider(6);
			});

		});
	}


	//HOMEPAGE TESTIMONIALS
	if($("#home_testimonials").length > 0 ){
		if(home_testimonial = $("#home_testimonials")) {
			home_testimonial.css('background', 'none');
			homeTestimonial = {
				i:0,
				 showTestimonials: function(i) {
					var testimonials = new Array('#testimonial-red_herring_100', '#testimonial-navteq','#testimonial-o2', '#testimonial-immi');
					home_testimonial.children('span').hide();
					home_testimonial.children('div').filter(":visible").hide();
					$(testimonials[homeTestimonial.i]).show();
					if (homeTestimonial.i==3) {homeTestimonial.i=0;}else{homeTestimonial.i=homeTestimonial.i+1;}
					setTimeout(homeTestimonial.showTestimonials,5000);
				}
			}
			homeTestimonial.showTestimonials();
		}
	}

	//HOMEPAGE VIDEO OVERLAY
	if($('#m4h_work_video').length >0) {

	   // install flowplayer into flowplayer container 
	   //var player = $f("player", "http://www.youtube.com/v/YJY1Fkl4ZnI&hl=en&fs=1&rel=0&color1=0x5d1719&color2=0xcd311b"); 

	   // setup button action. it will fire our overlay  
	   $("a[rel]").overlay({ 
		            expose: '#333',

	       // use the Apple effect for overlay 
	       effect: 'apple', 

	     /*  // when overlay is opened, load our player 
	       onLoad: function() { 
	           //player.load(); 
	       }, 

	       // when overlay is closed, unload our player 
	       onClose: function() { 
	           //player.unload(); 
	       } */
	   });
	}

	// HEADER LOGIN FORM
	if($(".zend_form #login")) {
			$(".zend_form #login").val('');
	}


	// DASHBOARD OVERLAYS
	if($("#dashboard-services").length > 0) {
		// if the function argument is given to overlay, it is assumed to be the onBeforeLoad event listener
				$(function() { 
				    // if the function argument is given to overlay, 
				    // it is assumed to be the onBeforeLoad event listener 
				    $("a[rel]").overlay({
			            expose: '#333', 
			            opacity: 0.7, 
			            closeSpeed: 1000
				    }); 
				});
	}
	
	if($('.whats-this a').length > 0) {
		$('.whats-this a').mouseover(function() {
//			setTimeout($(this).siblings("p").show(), 1500); // I don't think this ever worked
			$(this).siblings("p").fadeIn(600);

		});
	
	
		$('.whats-this a').mouseout(function() {
			$(this).siblings("p").fadeOut(600);
		});
	}
	
	/*if($("input[type=file]").length > 0) {
		$("input[type=file]").filestyle({ 

		 buttonText: "browsee",
	     imageheight : 24,
	     imagewidth : 59,
	     width : 200
	 });
	}*/

	// calculate timezone offset for this user
	var tzo=((new Date().getTimezoneOffset())/60)*(-1);	
});

/*
 * jQuery.fn.sortElements
 * --------------
 * @param Function comparator:
 *   Exactly the same behaviour as [1,2,3].sort(comparator)
 *   
 * @param Function getSortable
 *   A function that should return the element that is
 *   to be sorted. The comparator will run on the
 *   current collection, but you may want the actual
 *   resulting sort to occur on a parent or another
 *   associated element.
 *   
 *   E.g. $('td').sortElements(comparator, function(){
 *      return this.parentNode; 
 *   })
 *   
 *   The <td>'s parent (<tr>) will be sorted instead
 *   of the <td> itself.
 */
jQuery.fn.sortElements = (function(){
 
    var sort = [].sort;
 
    return function(comparator, getSortable) {
 
        getSortable = getSortable || function(){return this;};
 
        var placements = this.map(function(){
 
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
 
                // Since the element itself will change position, we have
                // to have some way of storing its original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
 
            return function() {
 
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
 
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
 
            };
 
        });
 
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
 
    };
 
})();	