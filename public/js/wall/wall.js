function toggleProjectWall(id, type) {
    div = 'wall-container-' + type + '_' + id;
    $("#" + div).toggle();

    if ($("#" + div).is(':visible') && !$("#" + div + " #wall").length) {
        var wall = {'resultsPerPage': 2, 'type': type, 'target': id, 'comment': true};
        showWall(wall, div);
    }
}


function showWallPage(newPage, options, container) {

    options.page = newPage;

    showWall(options, container);

}


function showWall(options, container) {

    var postObject = new Object;

    postObject.idtarget = options.target;

    postObject.postType = options.type;

    postObject.page = options.page;

    postObject.resultsPerPage = options.resultsPerPage;

    postObject.timestamp = options.timestamp;

    postObject.showcomment = options.comment;

    postObject.filter = options.filter;

    $("#" + container).prepend('<div class="loading-wall">' + options.loadingText + '</div>').fadeIn(600);


    $.post("/app/wall/getwall/", postObject, function (data) {
        $("#" + container).html(data);

        $.get("/app/wall/getunread", function (data) {
            $("#unread .unread-count").html("(" + data + ")");
        })

    });

    scroll(0, 0);

    return true;


}

function _addComment(messageContainer, idtarget, postType, idparent, id) {

    var message = $("#" + messageContainer).val();

    // Fade out the file container if it is open
    if (idparent == 0) {
        div = postType + '-' + idtarget;
    } else {
        div = idparent;
    }


    if (message != "") {

        $("#file-" + div).fadeOut(600);

        var postObject = new Object;
        postObject.idtarget = idtarget;
        postObject.message = message;
        postObject.postType = postType;
        postObject.idparent = idparent;

        if (id != undefined)
            postObject.id = id;

        divId = "wallpost-thread-" + idparent;
        loadingDiv = "loadingMsg" + idparent;

        $("#" + loadingDiv).fadeIn(600);

        $.post("/app/wall/add/", postObject, function(data) {
            $("#" + loadingDiv).fadeOut(600,function(){
                location.reload();
                // Returns the whole thread
               /* if (idparent == 0) {
                    // New Thread
                    $("div#wallpost-thread-top-"+div).prepend(data).fadeIn(600);
                } else {
                    $("#" + divId).html(data).fadeIn(600);
                }*/
            });
        });


    } else {
        alert("Please add a description if you are uploading a file");
    }
    return false;
}

function _removeComment(id, idtarget, postType) {

    if (confirm("This cannot be undone. Are you sure you want to delete this comment?")) {

        var postObject = new Object;
        postObject.idmessage = id;
        postObject.postType = postType;
        postObject.idtarget = idtarget;

        $("#wallpost-thread-" + id).fadeOut(600);

        $.post("/app/wall/delete/", postObject, function (data) {
        });
        return false;
    }
}

function _preaddComment(message, idtarget, postType, parent) {

    var filediv;

    var postObject = new Object;

    postObject.message = message;
    postObject.idtarget = idtarget;
    postObject.postType = postType;
    postObject.idparent = parent;

    if (parent == 0) {
        filediv = postType + '-' + idtarget;
    } else {
        filediv = parent;
    }

    $.post("/app/wall/preadd/", postObject, function (data) {
        $("#uploadFile-" + filediv).fadeOut(600);
        $("#fileinput-" + filediv).show();
        $("#fileinput-" + filediv).html(data);

    });
    return false;
}

function _attachDoc(idtarget, postType, parent) {
    var filediv;

    if (parent == 0) {
        filediv = postType + '-' + idtarget;
    } else {
        filediv = parent;
    }


    if ($("#file-" + filediv).is(':visible')) {
        $("#file-" + filediv).hide();
    } else {
        $("#fileinput-" + filediv).hide();
        $("#uploadFile-" + filediv).html(" preparing....").fadeIn(600);
        $("#file-" + filediv).show();
        _preaddComment($('#par-' + filediv + ' .comment').val(), idtarget, postType, parent);
    }


}


function _submitFile(element, postType, idtarget, parent) {

    var filediv;

    if (parent == 0) {
        filediv = postType + '-' + idtarget;
    } else {
        filediv = parent;
    }

    $("#uploadFile-" + filediv).html(" uploading....").fadeIn(600);

    _buttonClick(element, filediv);

}


function _buttonClick(element, filediv) {
    // remove the label

    var allowedfiletypes = new Object;
    allowedfiletypes['wal'] = ".pdf,.doc,.xls,.docx,.xlsx,.rtf,.txt,.zip,.rar,.jpg,.jpeg,.gif,.png";

    // infer the name of the file control based on the name of the button that
    // we clicked
    filecontrolname = element.name.substring(3);
    // check if this is an allowed value for file control
    if ((filecontrolname.substring(0, 3) != 'wal')
        && (allowedfiletypes[filecontrolname] == undefined)) {
        $('#div' + filecontrolname)
            .prepend(
            "<div id='divError'><img src='/images/icons/form_error.png'/>Unknown Error</div>");

    } else {
        // get the file control
        filecontrol = document.getElementById(filecontrolname);
        // and the filename from it
        filename = filecontrol.value;
        strpos = filename.lastIndexOf(".");
        extension = filename.substr(strpos).toLowerCase();
        allowedextensions = allowedfiletypes['wal'];

        if (!allowedextensions.match(extension)) {
            $('#div' + filecontrolname).prepend(
                "<div id='divError'><img src='/images/icons/form_error.png'/> filetype "
                + extension + " not allowed. Only files of type "
                + allowedextensions + " are allowed</div>");
            setTimeout(function () {
                $('#divError').fadeOut('slow');
            }, 2000);

        } else {
            // the second argument of setForm is crucial,
            // which tells Connection Manager this is a file upload form
            YAHOO.util.Connect.setForm(element.form, true);

            var uploadHandler = {
                upload: function (o) {
                    $('#div' + filecontrolname).html(o.responseText);
                    filecontrol.value = ""; // reset the file control
                    $("#uploadFile-" + filediv).hide();
                }
            };


            var url = "/app/wall/upload/?file=" + filecontrolname;

            YAHOO.util.Connect.asyncRequest("GET", url, uploadHandler);
        }
    }
}


function removeFile(fcn, filename) {

    if (confirm("This cannot be undone. Are you sure you want to delete this file?")) {

        var postObject = new Object;
        postObject.id = filename;

        divid = "div" + fcn;

        $('#' + divid).hide();
        $('#divWorkToggle' + filename).fadeIn(600);

        $.post("/app/wall/removefile/", postObject, function (data) {
            $('#divWorkToggle' + filename).fadeOut(600);
            $('#' + divid).html(data).fadeIn(200);
        });
    }
    return false;

}

function _clickComment(id) {
    $("#" + id).toggle();
}
