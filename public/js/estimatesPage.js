$(document).ready(calcRows);





function calcRows()
{
   document.getElementById("rate3").onblur=additionalFunds;
   
 
   //calculate the sheet 
   //calc row 1
   //check to see if this is a Basic project 
   
   if (document.getElementById("desc").innerHTML =='MOB<span class="mobred">TEST</span> Basic Project Fee')
   {  
      document.getElementById("total1").innerHTML ="0.00";
      document.getElementById("rate1").innerHTML='Free';
      document.getElementById("quantity1").innerHTML='Free';
   }
   else
   {
   document.getElementById("total1").innerHTML =(parseFloat(document.getElementById("rate1").innerHTML) * parseFloat(document.getElementById("quantity1").innerHTML)).toFixed(2);
   document.getElementById("rate1").innerHTML= parseFloat(document.getElementById("rate1").innerHTML).toFixed(2);
   }
   
   calcRow2();
   calcRow3();
   updateTotals();
   addDollarSigns();  
}

function additionalFunds(){
   removeDollarSigns();
   calcRow2();
   calcRow3();
   updateTotals();
   addDollarSigns();    
}

function calcRow2()
{
   //check for legal number   
   //calc row 2
   var value = (parseInt(document.getElementById("quantity2").innerHTML) * parseFloat(document.getElementById("rate2").innerHTML)).toFixed(2);
   document.getElementById("rate2").innerHTML= parseFloat(document.getElementById("rate2").innerHTML).toFixed(2);
   document.getElementById("total2").innerHTML =isNaN(value)?'0.00':value;
}

function calcRow3()
{
   //check for legal number
   //calc row 3
   //if its a 4 line calc then...
   if(document.getElementById("feeTotal").innerHTML != "")
   {
      document.getElementById("feeTotal").innerHTML = parseFloat(document.getElementById("total2").innerHTML *.19).toFixed(2);
   }
   var value =(parseFloat(document.getElementById("rate3").value)).toFixed(2);
   document.getElementById("total3").innerHTML =isNaN(value)?'0.00':value.toString();
}

function updateTotals()
{
   //calc subtotal
   document.getElementById("subtotal").innerHTML= (parseFloat(document.getElementById("total1").innerHTML)+parseFloat(document.getElementById("total2").innerHTML)+parseFloat(document.getElementById("total3").innerHTML)).toFixed(2);
   //get state of feeTotal
   if (document.getElementById("feeTotal").innerHTML != "")
   {
      document.getElementById("subtotal").innerHTML = (parseFloat(document.getElementById("subtotal").innerHTML) + parseFloat(document.getElementById("feeTotal").innerHTML)).toFixed(2);
   }
   
   //calc balance 
   var bal = parseFloat((document.getElementById("subtotal").innerHTML)-parseFloat(document.getElementById("lessfunds").innerHTML)).toFixed(2);
   document.forms[1].amount.value=document.getElementById("balance").innerHTML=((bal<0)? '0.00': bal);
}

function addDollarSigns()
{
   if(document.getElementById("rate1").innerHTML!="Free")
   {
   document.getElementById("rate1").innerHTML = "$" + document.getElementById("rate1").innerHTML;
   }
   
   if(document.getElementById("feeTotal").innerHTML!="")
   {  
      document.getElementById("feeTotal").innerHTML = "$" + document.getElementById("feeTotal").innerHTML;
   }
   
   document.getElementById("total1").innerHTML = "$" + document.getElementById("total1").innerHTML;
   document.getElementById("rate2").innerHTML = "$" + document.getElementById("rate2").innerHTML;
   document.getElementById("total2").innerHTML = "$" + document.getElementById("total2").innerHTML;
   document.getElementById("total3").innerHTML = "$" + document.getElementById("total3").innerHTML;
   document.getElementById("subtotal").innerHTML = "$" + document.getElementById("subtotal").innerHTML;
   document.getElementById("lessfunds").innerHTML = "$" + document.getElementById("lessfunds").innerHTML;
   document.getElementById("balance").innerHTML = "$" + document.getElementById("balance").innerHTML;
}

function removeDollarSigns()
{
   if(document.getElementById("rate1").innerHTML!="Free")
   {
   document.getElementById("rate1").innerHTML=document.getElementById("rate1").innerHTML.substring(1,document.getElementById("rate1").innerHTML.length);
   }
   
   document.getElementById("rate2").innerHTML =document.getElementById("rate2").innerHTML.substring(1,document.getElementById("total1").innerHTML.length);
   document.getElementById("total1").innerHTML = document.getElementById("total1").innerHTML.substring(1,document.getElementById("total1").innerHTML.length);
   document.getElementById("total2").innerHTML =document.getElementById("total2").innerHTML.substring(1,document.getElementById("total2").innerHTML.length);
   document.getElementById("total3").innerHTML = document.getElementById("total3").innerHTML.substring(1,document.getElementById("total3").innerHTML.length);
   
   if (document.getElementById("feeTotal").innerHTML.substr(0,1)=="$")
   {
      document.getElementById("feeTotal").innerHTML = document.getElementById("feeTotal").innerHTML.substring(1,document.getElementById("feeTotal").innerHTML.length);
   }
      
   document.getElementById("subtotal").innerHTML =document.getElementById("subtotal").innerHTML.substring(1,document.getElementById("subtotal").innerHTML.length);
   document.getElementById("lessfunds").innerHTML =document.getElementById("lessfunds").innerHTML.substring(1,document.getElementById("lessfunds").innerHTML.length);
   document.getElementById("balance").innerHTML =document.getElementById("balance").innerHTML.substring(1,document.getElementById("balance").innerHTML.length);  
}