$(document).ready(function() {
    $("#username").bind('blur', usernameCheck);
    $("#email").bind('blur', emailCheck);
 
});
 
function usernameCheck()
{
    var username = $("#username").val();
 
    if (username.length < 3)
    {
        $("#username_help").html('This field should be at least 3 characters long');
    }
    else if (username.length > 32)
    {
        $("#username_help").html('This field should be maximum 32 characters long');
    }
    else
    {
	    postObject = new Object;
	    postObject.username = username;
	    $.post('/user/checkusernameajax/', postObject,
	      function(data){
	        if (data.valid)
	        {
	            $("#username_help").html('');
	        }
	        else
	        {
	            $("#username_help").html('This username is already taken');
	        }
	      }, "json" );
	 }
}
 
function emailCheck()
{
    var email = $("#email").val();
 
    {
        postObject = new Object;
        postObject.email = email;
        $.post('/user/checkemailajax/', postObject,
          function(data){
            if (data.valid)
            {
                $("#email_help").html('');
            }
            else
            {
                $("#email_help").html('This email is already registered');
            }
          }, "json" );
     }
}