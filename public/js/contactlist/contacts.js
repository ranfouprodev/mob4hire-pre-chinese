function showContacts(pageNumber,container)
{
	
	if(container == undefined)
		container = "contacts-container"
		
	postObject = new Object;
	postObject.page = pageNumber;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.showcomment = showcomment; 
	postObject.selector = selector;
	postObject.layout = layout;
	
	if(search != undefined){ 
		postObject.search = search;
		postObject.nolayout = true;
		$.post("/app/contacts/search/", postObject, function(data){
			$("#"+container).html(data);
		});
	}else{
		$.post("/app/contacts/list/", postObject, function(data){
			$("#"+container).html(data);
		});	
	}
		
		
	
}


function removeSelectedContacts()
{
	var checked = $("#group INPUT[type='checkbox']:checked");
	var checkedIds = new Array;
		
	for( var i=0; i<checked.length; i++ ) {
		checkedIds[i] = checked[i].getAttribute('value');
	}
	
	postObject = new Object;
	postObject.remove = checkedIds.join(",");
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.showcomment = showcomment; 
	postObject.selector = selector;
	postObject.layout = layout;
	
	$.post("/app/contacts/delete/", postObject, function(data){
			$("#contacts-container").html(data);
	});

}


function removeContact(id)
{
	
	postObject = new Object;
	postObject.remove = id;
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.showcomment = showcomment; 
	postObject.selector = selector; 
	postObject.layout = layout;
	
	$.post("/app/contacts/delete/", postObject, function(data){
			$("#contacts-container").html(data);
	});

}

function addContact(element,id)
{ 	
	postObject = new Object;
	postObject.id = id;
	
	$.post("/app/contacts/add/", postObject, function(data){
			$(element).parent().removeClass("sbutton-active");	
			$(element).parent().html('<span class="info"><img src="/images/icons/tick.png"/>Added</span>');
			
			setTimeout(function(){
				$(element).parent().hide();},3000);
	});
}




