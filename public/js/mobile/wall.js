

function showUnifiedWall(divid,page,resultsPerPage,timestamp){
	
	var postObject = new Object;
	
	if(!page == undefined)
		postObject.page = page;
		
	if(!resultsPerPage == undefined)
		postObject.resultsPerPage = resultsPerPage;
			
	if(!timestamp == undefined)
		postObject.timestamp = timestamp;
	

	$.post("/mobile/wall/unified/", postObject, function(data){
			$("#"+divid).html(data);
	});
}

