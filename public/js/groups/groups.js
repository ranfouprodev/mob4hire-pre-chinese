function showGroups(pageNumber)
{
	
	postObject = new Object;
	postObject.page = pageNumber;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode;
	postObject.layout = layout; 
	
	
	$.post("/app/group/list/", postObject, function(data){
			$("#groups-container").html(data);
			$("#groups-container").page();
	});
}

function showGroupContacts(idgroup, pageNumber,container)
{
	
	if(container == undefined)
		container = "contacts-container"
		
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = pageNumber;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	
	$.post("/app/group/listmembers/", postObject, function(data){
			$("#"+container).html(data);
	});
}

function sendInvites(idgroup)
{
	var checked = $("#group INPUT[type='checkbox']:checked");
	
	var checkedIds = new Array;
		
	for( var i=0; i<checked.length; i++ ) {
		checkedIds[i] = checked[i].getAttribute('value');
	}
	
	postObject = new Object;
	postObject.idgroup = idgroup; 
	postObject.choose = checkedIds.join(",");
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	
	// set loading gif
	$(".content-wrap").html("<img src='/images/working.gif'/> Sending invites...");
		
	$.post("/app/group/invite/", postObject, function(data){
		$(".content-wrap").html("<img src='/images/working.gif'/> Updating invite list...");
		// update selector page
		var href = "http://"+window.location.hostname + "/app/group/selector?idgroup="+idgroup;
		$(".content-wrap").load(href);
		
		// update the current users list underneath the popup
		showGroupContacts(idgroup,1);
	});

}



function rejectInvites(idgroup)
{
	var checked = $("#group-inv INPUT[type='checkbox']:checked");
	
	var checkedIds = new Array;
		
	for( var i=0; i<checked.length; i++ ) {
		checkedIds[i] = checked[i].getAttribute('value');
	}
	
	postObject = new Object;
	postObject.idgroup = idgroup; 
	postObject.remove = checkedIds.join(",");
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	
	$.post("/app/group/reject/", postObject, function(data){
			$("#invites-container").html(data);		
			//location.reload(); 
	});

}

function acceptInvites(idgroup)
{
	var checked = $("#group-inv INPUT[type='checkbox']:checked");
	
	var checkedIds = new Array;
		
	for( var i=0; i<checked.length; i++ ) {
		checkedIds[i] = checked[i].getAttribute('value');
	}
	
	postObject = new Object;
	postObject.idgroup = idgroup; 
	postObject.remove = checkedIds.join(",");
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	
	$.post("/app/group/accept/", postObject, function(data){
			$("#invites-container").html(data);
			//location.reload(); 
	});

}


function removeSelectedContactsFromGroup(idgroup)
{
	var checked = $("#group-grp INPUT[type='checkbox']:checked");
	
	var checkedIds = new Array;
		
	for( var i=0; i<checked.length; i++ ) {
		checkedIds[i] = checked[i].getAttribute('value');
	}
	
	postObject = new Object;
	postObject.idgroup = idgroup; 
	postObject.remove = checkedIds.join(",");
	postObject.page = 1;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	
	$.post("/app/group/delete/", postObject, function(data){
			$("#contacts-container").html(data);
	});

}

function leaveGroup(idgroup){
	
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = page;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode; 
	
	$.post("/app/group/leave/", postObject, function(data){
			$("#groups-container").html(data);
	});
	
}

function joinGroup(idgroup){
	
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = page;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode; 
	
	$.post("/app/group/join/", postObject, function(data){
			$("#groups-container").html(data);
	});
	
}


