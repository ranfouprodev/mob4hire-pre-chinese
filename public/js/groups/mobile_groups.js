function showGroups(pageNumber)
{
	
	postObject = new Object;
	postObject.page = pageNumber;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode;
	postObject.layout = layout; 
	
	
	$.post("/app/group/list/", postObject, function(data){
			$("#groups-container").html(data);
			$("#groups-container").page();
	});
}

function leaveGroup(idgroup){
	
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = page;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode; 
	postObject.layout = layout; 
		
	$.post("/app/group/leave/", postObject, function(data){
			$("#groups-container").html(data);
			$("#groups-container").page();
	});
	
}

function joinGroupRequest(element,idgroup){
	
	
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = 1;
	postObject.resultsPerPage = 1; 
	postObject.selector = "none"; 
	postObject.mode = "self"; 
	postObject.layout = "compact"; 
	
	$.post("/app/group/join/", postObject, function(data){
		$(element).parent().removeClass("sbutton-active");	
		$(element).parent().html('<span class="info"><img src="/images/icons/tick.png"/> You have requested to join the group. Once the moderator has approved your request you will be added into the discussion</span>');
		
		setTimeout(function(){
			$(element).parent().hide();},3000);
	});
	
	
}

function joinGroup(idgroup){
	
	postObject = new Object;
	postObject.idgroup = idgroup;
	postObject.page = page;
	postObject.resultsPerPage = resultsPerPage; 
	postObject.selector = selector; 
	postObject.mode = mode; 
	postObject.layout = layout; 
	
	$.post("/app/group/join/", postObject, function(data){
			$("#groups-container").html(data);
			$("#groups-container").page();
	});
	
}


