master.css
used site wide and imports the following css files:
reset.css-- resets all styles to default values
typography.css-- defines default typography
navigation.css-- top main navigation bar positioning, including left nav submenu
layout.css-- controls positioning of form elements and other pieces of the page such as footer
common.css -- commonly used styles within the app

---------------------------------------------

reset.css
This stylesheet, by Eric Meyer, resets all values so that browser inconsistencies are gone. For more info, visit http://meyerweb.com/eric/tools/css/reset/index.html

---------------------------------------------

typography.css
This sets all of the default typography for the site, including the marketing (default) and webapp. All font sizes are specified in relative units (em's).

---------------------------------------------

navigation.css
This sets the positioning and background graphics for the top navigation and left column navigation menus.

---------------------------------------------

layout.css
This controls positioning of all of the outer containers, site header, and footer.

---------------------------------------------

common.css
This controls and sets positioning for commonly used miscellaneous items, such as graphics, tables, graphic links, columns, etc.

---------------------------------------------

csshover.htc
Not sure what this file is for. It currently is not being called and implemented

---------------------------------------------

buzz.css
This controls the buzz landing page elements, such as the Twitter feed.
NOTE: this file is being called inside the <body> of the page(inside the view of the page, index.phtml).
TODO: Maybe move this call to the Controller of the page

---------------------------------------------

contacts.css
This controls the contacts pop-up overlay in the webapp.
NOTE: this file is being called inside the <body> of the page(inside the view of the page, compose.phtml)
TODO: Maybe move this call to the Controller of the page

---------------------------------------------

default.css
Needs to be removed! - Doublecheck first!

---------------------------------------------

dynamictable.css
Styles for the Project Listing page in the admin module.

---------------------------------------------

forms.css
Styles for all zend forms, including dropdown lists, textfields, and (handset) tables in the website, including default/marketing and webapp.

---------------------------------------------

imageselect.css
Styles for the image edit pop-up inside the Webapp Profile landing page (application/modules/app/views/scripts/profile/index.phtml)

---------------------------------------------

mobile.css
Styles for the mobile version of the M4H website.

---------------------------------------------

rating.css
Controls the display and styling for the user rating.  Written ryan masuga, masugadesign.com
 
---------------------------------------------

style.css
Styles for o2litmus, original M4H website, and unknown.
TODO: Verify with Richard and John the components of this stylesheet that is used and remove.  Doublecheck first!

---------------------------------------------

Styles.css
Styles for various pages within marketing/default and also for the webapp.  Need to verify pages that using this stylesheet.
TODO: Cleanup, organize, and place styles into other files.

---------------------------------------------

webapp.css
Styles for all the app pages. This is currently not being used.
TODO: Remove this file. Webapp styles are used by app/webapp.css  Verify first!

---------------------------------------------

/app/layout.css
Styles for positioning that is specific to the webapp

---------------------------------------------

/app/master.css
Contains imports to site wide css and styles specific to the webapp.

---------------------------------------------

/app/navigation.css
Styles for webapp top navigation menu and the left column side menu

---------------------------------------------

/app/style.css
Specific page styles for the webapp?  Maybe not necessary
TODO: Currently, this is a copy of the styles.css file.  Extraneous styles not specific to the webapp needs to be removed.

---------------------------------------------

/app/webapp.css
Styles specific to the webapp.